<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="illustration_page">330</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1824" rank="genus">cercocarpus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">betuloides</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 427. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus cercocarpus;species betuloides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100036</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 10–50 (–80) dm, sparsely to moderately branched, young stems, leaves, pedicels, and hypanthia sericeous (hairs straight, antrorse, appressed, or ± ascending), or hirsute-pilose (hairs erect, straight), or ± villous (hairs wavy or wavy-crinkled), or hairs tightly coiled or 1/2 coiled, or 1/2 coiled at base and erect-ascending, wavy distally, 0.2–0.8 (–1.3) mm.</text>
      <biological_entity id="o7001" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o7003" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o7004" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o7005" name="pedicel" name_original="pedicels" src="d0_s0" type="structure" />
      <biological_entity id="o7006" name="hypanthium" name_original="hypanthia" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute-pilose" value_original="hirsute-pilose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute-pilose" value_original="hirsute-pilose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hirsute-pilose" value_original="hirsute-pilose" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o7007" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="tightly" name="architecture" src="d0_s0" value="coiled" value_original="coiled" />
        <character name="quantity" src="d0_s0" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="coiled" value_original="coiled" />
        <character name="quantity" src="d0_s0" value="1/2" value_original="1/2" />
        <character constraint="at base" constraintid="o7008" is_modifier="false" name="architecture" src="d0_s0" value="coiled" value_original="coiled" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s0" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s0" value="wavy" value_original="wavy" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s0" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7008" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: long-shoot internodes 3–20 (–55) mm, ± sparsely hairy, glabrescent;</text>
      <biological_entity id="o7009" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="long-shoot" id="o7010" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="55" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="more or less sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>short-shoots 3–30 (–55) × 1–2.5 mm.</text>
      <biological_entity id="o7011" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o7012" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="55" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent or ± drought-deciduous;</text>
      <biological_entity id="o7013" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s3" value="drought-deciduous" value_original="drought-deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules (1.5–) 3.5–6 (–10) mm, 1.5–2.5 mm in s California;</text>
      <biological_entity id="o7014" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="in california" constraintid="o7015" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7015" name="california" name_original="california" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5–9 (–16) mm;</text>
      <biological_entity id="o7016" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblong-obovate, rhombic, suborbiculate to broadly ovate, rarely oblongelliptic, (8–) 12–50 (–78) × (4–) 8–35 (–64) mm, membranous to coriaceous, base cuneate to rounded, rarely ± cordate, margins ± revolute or flat, dentate, serrate, or, in broader leaves, crenate in distal 1/5–3/4, apex rounded, sometimes acute, abaxial surface sericeous, pilose, villous, or with curled hairs on veins, areoles canescent or glabrous, adaxial sparsely hairy, sometimes glabrate.</text>
      <biological_entity id="o7017" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-obovate" value_original="oblong-obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s6" to="broadly ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s6" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="78" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s6" to="50" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s6" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="64" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s6" to="coriaceous" />
      </biological_entity>
      <biological_entity id="o7018" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" modifier="rarely more or less" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o7019" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character constraint="in distal 1/5-3/4" constraintid="o7021" is_modifier="false" name="shape" notes="" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="broader" id="o7020" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o7021" name="1/5-3/4" name_original="1/5-3/4" src="d0_s6" type="structure" />
      <biological_entity id="o7022" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7023" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o7024" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="curled" value_original="curled" />
      </biological_entity>
      <biological_entity id="o7025" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o7026" name="areole" name_original="areoles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7027" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o7019" id="r424" name="in" negation="false" src="d0_s6" to="o7020" />
      <relation from="o7023" id="r425" name="with" negation="false" src="d0_s6" to="o7024" />
      <relation from="o7024" id="r426" name="on" negation="false" src="d0_s6" to="o7025" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 1–18 per short-shoot;</text>
      <biological_entity id="o7028" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per short-shoot" constraintid="o7029" from="1" name="quantity" src="d0_s7" to="18" />
      </biological_entity>
      <biological_entity id="o7029" name="short-shoot" name_original="short-shoot" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthial tubes sericeous, woolly, or villous or with coiled or spreading hairs;</text>
      <biological_entity constraint="hypanthial" id="o7030" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with coiled or spreading hairs" />
      </biological_entity>
      <biological_entity id="o7031" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="coiled" value_original="coiled" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o7030" id="r427" name="with" negation="false" src="d0_s8" to="o7031" />
    </statement>
    <statement id="d0_s9">
      <text>hypanthial cups 2–4 × 4–7 mm;</text>
      <biological_entity constraint="hypanthial" id="o7032" name="cup" name_original="cups" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, broadly deltate, 1–2.5 mm, acute-acuminate;</text>
      <biological_entity id="o7033" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute-acuminate" value_original="acute-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20–61, anthers 1–1.5 mm, hirsute.</text>
      <biological_entity id="o7034" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="61" />
      </biological_entity>
      <biological_entity id="o7035" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 7–11 (–13.5) × 1.5–2.5 (–2.9) mm;</text>
    </statement>
    <statement id="d0_s13">
      <text>fruiting pedicels (1.5–) 3–11 (–21) mm;</text>
      <biological_entity id="o7036" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="13.5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="21" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7037" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o7036" id="r428" name="fruiting" negation="false" src="d0_s13" to="o7037" />
    </statement>
    <statement id="d0_s14">
      <text>hypanthial tubes (5–) 7–13 (–17) mm;</text>
      <biological_entity constraint="hypanthial" id="o7038" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="17" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pedicel/tube ratio (15–) 35–90 (–126) %;</text>
      <biological_entity id="o7039" name="tube" name_original="pedicel/tube" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>fruit awns 5–8.5 (–12.5) cm, proximal setae 2.7–5 mm.</text>
      <biological_entity constraint="fruit" id="o7040" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s16" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s16" to="8.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7041" name="seta" name_original="setae" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Oreg.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>The western, spring-flowering Cercocarpus betuloides in the broad sense, centered in California, is well supported by molecular data. A total of five taxa have been recognized in recent floras; molecular data provide no support for these taxa, but instead support recognition of a single inclusive taxon.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves, young stems, and hypanthial tubes appressed-sericeous or hypanthial tubes also with erect-ascending hairs (hairs not coiled); flowers 1–3(–10) per short shoot; transmontane s, c California, c Arizona, and sw Oregon.</description>
      <determination>1a Cercocarpus betuloides var. betuloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves, young stems, and hypanthial tubes commonly with tightly coiled hairs (except in some insular plants where sericeous); flowers (1–)3–6(–18) per short shoot; cismontane s California</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades sparsely to moderately hairy, subcoriaceous to coriaceous, margins serrate or dentate or crenate in distal 1/5–3/4; coastal s California and adjacent islands.</description>
      <determination>1b Cercocarpus betuloides var. blancheae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades abaxially densely woolly, strongly coriaceous, margins (often strongly revolute, usually obscuring teeth), crenate, dentate-serrate in distal 1/2–3/4; sw Santa Catalina Island.</description>
      <determination>1c Cercocarpus betuloides var. traskiae</determination>
    </key_statement>
  </key>
</bio:treatment>