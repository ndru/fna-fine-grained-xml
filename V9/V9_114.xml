<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="(Cockerell) Cockerell ex Rehder" date="1940" rank="subgenus">hesperhodos</taxon_name>
    <taxon_name authority="Crépin" date="1889" rank="section">Minutifoliae</taxon_name>
    <place_of_publication>
      <publication_title>J. Roy. Hort. Soc.</publication_title>
      <place_in_publication>11: 226. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus hesperhodos;section Minutifoliae</taxon_hierarchy>
    <other_info_on_name type="fna_id">318043</other_info_on_name>
  </taxon_identification>
  <number>7a.1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, forming dense thickets, xerophytic;</text>
      <biological_entity id="o9444" name="thicket" name_original="thickets" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o9443" id="r591" name="forming" negation="false" src="d0_s0" to="o9444" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o9443" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, rarely arching, 2.5–12 (–15) dm;</text>
      <biological_entity id="o9445" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="15" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s2" to="12" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches densely pubescent or glabrous, with or without stellate hairs;</text>
      <biological_entity constraint="distal" id="o9446" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9447" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s3" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o9446" id="r592" name="with or without" negation="false" src="d0_s3" to="o9447" />
    </statement>
    <statement id="d0_s4">
      <text>prickles erect, rarely curved, infrastipular prickles present, mixed with internodal prickles and aciculi.</text>
      <biological_entity id="o9448" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s4" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o9449" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="with internodal prickles; with internodal prickles and aciculi" constraintid="o9451" is_modifier="false" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o9450" name="prickle" name_original="prickles" src="d0_s4" type="structure" />
      <biological_entity constraint="internodal" id="o9451" name="prickle" name_original="prickles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, 1.5–3 × 1.5–2.5 cm;</text>
      <biological_entity id="o9452" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipule margins entire or dentate, glandular or eglandular, auricles flared;</text>
      <biological_entity constraint="stipule" id="o9453" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o9454" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flared" value_original="flared" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 3–7, terminal: petiolule 0.5–4 mm, blade oval, suborbiculate, obovate, or deltate, 6–18 × 4–13 mm, abaxial surfaces usually pubescent or tomentulose, rarely glabrous.</text>
      <biological_entity id="o9455" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9456" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9457" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s7" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9458" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 1 (–3) -flowered.</text>
      <biological_entity id="o9459" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1(-3)-flowered" value_original="1(-3)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, stout, 2–16 mm, mostly setose, often pubescent, rarely glabrous;</text>
      <biological_entity id="o9460" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s9" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 0 or 1 or 2, margins serrate to deeply incised.</text>
      <biological_entity id="o9461" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" unit="or or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" unit="or or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9462" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s10" to="deeply incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 2.5–5 cm;</text>
      <biological_entity id="o9463" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s11" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium thick-walled, setose or not;</text>
      <biological_entity id="o9464" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="thick-walled" value_original="thick-walled" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="setose" value_original="setose" />
        <character name="pubescence" src="d0_s12" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals ovatelanceolate, 8–20 × (1–) 2–5 mm, margins pinnatifid, abaxial surfaces glabrous or pubescent, sessile or stipitate-glandular, sometimes eglandular;</text>
      <biological_entity id="o9465" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9466" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9467" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, pink to rose-pink, sometimes white;</text>
      <biological_entity id="o9468" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="rose-pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 10–110, styles free, stylar orifice 2–5 mm diam., rims 0.5–1 mm wide, hypanthial disc absent.</text>
      <biological_entity id="o9469" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s15" to="110" />
      </biological_entity>
      <biological_entity id="o9470" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="stylar" id="o9471" name="orifice" name_original="orifice" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9472" name="rim" name_original="rims" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="hypanthial" id="o9473" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Hips dull red to dark reddish purple, subglobose or cupulate, 5–13 (–18) × 5–16 (–18) mm, hard, leathery, pubescent or glabrous, setose, stipitate-glandular or eglandular;</text>
      <biological_entity id="o9474" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dull" value_original="dull" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s16" to="dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s16" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="13" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s16" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s16" to="16" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s16" value="hard" value_original="hard" />
        <character is_modifier="false" name="texture" src="d0_s16" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals persistent, erect.</text>
      <biological_entity id="o9475" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basal.</text>
      <biological_entity id="o9476" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw, sc United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 5–7, terminal blades 3–7 × 2–6 mm; bracts 1 or 2; flowers 2.5–3(–4) cm diam.; hypanthia subglobose to globose, densely pubescent and setose; distal branches pubescent or glabrous, without stellate hairs; sepals 8–12 × 2–4 mm, abaxial surfaces pubescent; hips eglandular, densely setose.</description>
      <determination>1 Rosa minutifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 3–5, terminal blades 8–18 × 5–13 mm; bracts 0; flowers 4–5 cm diam.; hypanthia cupulate, puberulent or glabrous; distal branches densely pubescent, with or without stellate hairs; sepals 12–20 × 4–5 mm, abaxial surfaces glabrous, sometimes puberulent; hips stipitate-glandular, densely or sparsely setose.</description>
      <determination>2 Rosa stellata</determination>
    </key_statement>
  </key>
</bio:treatment>