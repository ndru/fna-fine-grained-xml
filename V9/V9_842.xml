<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">500</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="(Beadle) C. K. Schneider" date="1906" rank="section">brevispinae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Brevispinae</taxon_name>
    <place_of_publication>
      <publication_title>Man. Cult. Trees ed.</publication_title>
      <place_in_publication>2, 366. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section brevispinae;series Brevispinae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318055</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Beadle" date="unknown" rank="unranked">Brevispinae</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Fl. S.E. U.S.,</publication_title>
      <place_in_publication>532, 534. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;unranked Brevispinae</taxon_hierarchy>
  </taxon_identification>
  <number>64b.1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 60–100 (–150) dm.</text>
      <biological_entity id="o12848" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="dm" />
        <character char_type="range_value" from="60" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="dm" />
        <character char_type="range_value" from="60" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: trunks 1–few, erect, bark plated;</text>
      <biological_entity id="o12850" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o12851" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s1" to="few" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o12852" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="relief" src="d0_s1" value="plated" value_original="plated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>compound thorns on trunks present when older;</text>
      <biological_entity id="o12853" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o12854" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o12855" name="trunk" name_original="trunks" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when older" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o12854" id="r799" name="on" negation="false" src="d0_s2" to="o12855" />
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs absent or determinate, recurved, short, 1–1.5 cm.</text>
      <biological_entity id="o12856" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o12857" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" name="development" src="d0_s3" value="determinate" value_original="determinate" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12858" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o12857" id="r800" name="on" negation="false" src="d0_s3" to="o12858" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade elliptic, 2–3 cm, coriaceous, lobes 0, veins 5 or 6 (–8) per side, absent to sinuses, glossy;</text>
      <biological_entity id="o12859" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12860" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o12861" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12862" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character constraint="per side" constraintid="o12863" name="quantity" src="d0_s4" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o12863" name="side" name_original="side" src="d0_s4" type="structure">
        <character constraint="to sinuses" constraintid="o12864" is_modifier="false" name="presence" notes="" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12864" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" notes="" src="d0_s4" value="glossy" value_original="glossy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades on extension shoots often much larger (more than 6 cm) and variously lobed, to deeper sinuses.</text>
      <biological_entity id="o12865" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12866" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="variously" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="extension" id="o12867" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often much" name="size" src="d0_s5" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="deeper" id="o12868" name="sinuse" name_original="sinuses" src="d0_s5" type="structure" />
      <relation from="o12866" id="r801" name="on" negation="false" src="d0_s5" to="o12867" />
      <relation from="o12866" id="r802" name="to" negation="false" src="d0_s5" to="o12868" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: branches glabrous;</text>
      <biological_entity id="o12869" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o12870" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>symmetric bracteoles present, basal stipuliform, falcate bracteoles absent.</text>
      <biological_entity id="o12871" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o12872" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12873" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="stipuliform" value_original="stipuliform" />
      </biological_entity>
      <biological_entity id="o12874" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: post-mature petals ± orange;</text>
      <biological_entity id="o12875" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12876" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="post-mature" value_original="post-mature" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s8" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20, anthers cream to orange.</text>
      <biological_entity id="o12877" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12878" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o12879" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s9" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pomes black to bluish black, heavily pruinose mature;</text>
      <biological_entity id="o12880" name="pome" name_original="pomes" src="d0_s10" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s10" to="bluish black" />
        <character is_modifier="false" modifier="heavily" name="coating" src="d0_s10" value="pruinose" value_original="pruinose" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pyrene sides plane.</text>
      <biological_entity constraint="pyrene" id="o12881" name="side" name_original="sides" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc, se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc" establishment_means="native" />
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Series Brevispinae is centered in Louisiana and occurs in neighboring states, a distribution unique among hawthorns; the series is disjunct in Georgia. Formerly, Crataegus saligna (ser. Cerrones) was placed in ser. Brevispinae due to similarities of fruit, flower, and foliage. Section Brevispinae is distinct because of its short, recurved thorns, relatively large plant size (in some specimens), narrow, glossy leaves with lobes absent, more or less orange post-mature petals, and black fruit with plane-sided pyrenes. The names Brachyacanthae Sargent and sect. Brachyacanthae E. J. Palmer were not validly published.</discussion>
  
</bio:treatment>