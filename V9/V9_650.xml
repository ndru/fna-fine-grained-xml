<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James Henrickson,Alan S. Weakley</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Focke in H. G. A. Engler and K. Prantl" date="1888" rank="tribe">kerrieae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="genus">KERRIA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>12: 156. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe kerrieae;genus KERRIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For William Kerr, d. 1814 collector in the far east, sponsored by Royal Botanic Gardens, Kew, and superintendent of Botanic Garden, Peradinaya, Sri Lanka</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">117070</other_info_on_name>
  </taxon_identification>
  <number>39.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, spreading, open, rounded, 10–20 (–30) dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, suckering freely.</text>
      <biological_entity id="o19513" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="freely" name="growth_form" src="d0_s1" value="suckering" value_original="suckering" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–30+, widely arcuate, sparingly branched;</text>
      <biological_entity id="o19514" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="30" upper_restricted="false" />
        <character is_modifier="false" modifier="widely" name="course_or_shape" src="d0_s2" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bark (periderm) not forming, epidermis green, striate, glabrous;</text>
      <biological_entity id="o19515" name="bark" name_original="bark" src="d0_s3" type="structure" />
      <biological_entity id="o19516" name="epidermis" name_original="epidermis" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s3" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>short-shoots absent;</text>
    </statement>
    <statement id="d0_s5">
      <text>unarmed;</text>
      <biological_entity id="o19517" name="short-shoot" name_original="short-shoots" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bud-scales imbricate.</text>
      <biological_entity id="o19518" name="bud-scale" name_original="bud-scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves winter-deciduous, cauline, alternate;</text>
      <biological_entity id="o19519" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="winter-deciduous" value_original="winter-deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o19520" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules caducous, linear-subulate, thin, margins entire ± strigose-ciliate;</text>
      <biological_entity id="o19521" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o19522" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_shape" src="d0_s8" value="strigose-ciliate" value_original="strigose-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole present;</text>
      <biological_entity id="o19523" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade ovate to lanceovate, 2–8.5 cm, membranous, margins flat, coarsely doubly serrate, abaxial surface sparsely sericeous-strigose along veins.</text>
      <biological_entity id="o19524" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="8.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o19525" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="coarsely doubly" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19526" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character constraint="along veins" constraintid="o19527" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="sericeous-strigose" value_original="sericeous-strigose" />
      </biological_entity>
      <biological_entity id="o19527" name="vein" name_original="veins" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences terminal on vernal, leaf-bearing lateral branches, flowers solitary, sparsely strigose or glabrous;</text>
      <biological_entity id="o19528" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character constraint="on lateral branches" constraintid="o19529" is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19529" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="duration_or_growth_form" src="d0_s11" value="vernal" value_original="vernal" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="leaf-bearing" value_original="leaf-bearing" />
      </biological_entity>
      <biological_entity id="o19530" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracts absent;</text>
      <biological_entity id="o19531" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bracteoles absent or present, leaflike.</text>
      <biological_entity id="o19532" name="bracteole" name_original="bracteoles" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels present.</text>
      <biological_entity id="o19533" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Flowers 20–50 mm diam.;</text>
      <biological_entity id="o19534" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s15" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>epicalyx bractlets 0;</text>
      <biological_entity constraint="epicalyx" id="o19535" name="bractlet" name_original="bractlets" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>hypanthium saucer-shaped, 3–4 mm diam., glabrous;</text>
      <biological_entity id="o19536" name="hypanthium" name_original="hypanthium" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals (4 or) 5 (or 6), spreading to reflexed, oblong-ovate to ± orbiculate;</text>
      <biological_entity id="o19537" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s18" to="reflexed" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s18" to="more or less orbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>petals (4 or) 5 (or 6), to 34 in multi-petaled cultivars, spreading, strong yellow to slightly orangish or yellow-cream (some petals white in multi-petaled cultivars), oblong-ovate to orbiculate, base short-clawed, apex rounded to emarginate;</text>
      <biological_entity id="o19538" name="petal" name_original="petals" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
        <character char_type="range_value" constraint="in base, apex" constraintid="o19539, o19540" from="0" name="quantity" src="d0_s19" to="34" />
      </biological_entity>
      <biological_entity id="o19539" name="base" name_original="base" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="multi-petaled" value_original="multi-petaled" />
        <character is_modifier="true" name="orientation" src="d0_s19" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="coloration" src="d0_s19" value="strong" value_original="strong" />
        <character char_type="range_value" from="yellow" is_modifier="true" name="coloration" src="d0_s19" to="slightly orangish or yellow-cream" />
        <character char_type="range_value" from="oblong-ovate" is_modifier="true" name="shape" src="d0_s19" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s19" value="short-clawed" value_original="short-clawed" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s19" to="emarginate" />
      </biological_entity>
      <biological_entity id="o19540" name="apex" name_original="apex" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="multi-petaled" value_original="multi-petaled" />
        <character is_modifier="true" name="orientation" src="d0_s19" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="coloration" src="d0_s19" value="strong" value_original="strong" />
        <character char_type="range_value" from="yellow" is_modifier="true" name="coloration" src="d0_s19" to="slightly orangish or yellow-cream" />
        <character char_type="range_value" from="oblong-ovate" is_modifier="true" name="shape" src="d0_s19" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s19" value="short-clawed" value_original="short-clawed" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s19" to="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stamens 80–130 (0 or few in multi-petaled cultivars), shorter than petals;</text>
      <biological_entity id="o19541" name="stamen" name_original="stamens" src="d0_s20" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s20" to="130" />
        <character constraint="than petals" constraintid="o19542" is_modifier="false" name="height_or_length_or_size" src="d0_s20" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o19542" name="petal" name_original="petals" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>torus absent;</text>
      <biological_entity id="o19543" name="torus" name_original="torus" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>carpels (4–) 5 (–8), globose, glabrous, styles lateral, linear, ± exceeding filaments;</text>
      <biological_entity id="o19544" name="carpel" name_original="carpels" src="d0_s22" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s22" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s22" to="8" />
        <character name="quantity" src="d0_s22" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s22" value="globose" value_original="globose" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19545" name="style" name_original="styles" src="d0_s22" type="structure">
        <character is_modifier="false" name="position" src="d0_s22" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s22" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o19546" name="filament" name_original="filaments" src="d0_s22" type="structure" />
      <relation from="o19545" id="r1270" modifier="more or less" name="exceeding" negation="false" src="d0_s22" to="o19546" />
    </statement>
    <statement id="d0_s23">
      <text>ovules 2.</text>
      <biological_entity id="o19547" name="ovule" name_original="ovules" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Fruits aggregated nutlets, (1–) 5 in 1 whorl, tan, obliquely obovoid-globose, 4.5–5 mm, with distinct adaxial ridge, glabrous except for coarse basal hypanthial hairs;</text>
      <biological_entity id="o19548" name="fruit" name_original="fruits" src="d0_s24" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s24" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s24" value="obovoid-globose" value_original="obovoid-globose" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s24" to="5" to_unit="mm" />
        <character constraint="except-for basal hypanthial, hairs" constraintid="o19552, o19553" is_modifier="false" name="pubescence" notes="" src="d0_s24" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19549" name="nutlet" name_original="nutlets" src="d0_s24" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s24" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s24" to="5" to_inclusive="false" />
        <character constraint="in whorl" constraintid="o19550" name="quantity" src="d0_s24" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o19550" name="whorl" name_original="whorl" src="d0_s24" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s24" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19551" name="ridge" name_original="ridge" src="d0_s24" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s24" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="basal" id="o19552" name="hypanthial" name_original="hypanthial" src="d0_s24" type="structure">
        <character is_modifier="true" name="relief" src="d0_s24" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity constraint="basal" id="o19553" name="hair" name_original="hairs" src="d0_s24" type="structure">
        <character is_modifier="true" name="relief" src="d0_s24" value="coarse" value_original="coarse" />
      </biological_entity>
      <relation from="o19548" id="r1271" name="with" negation="false" src="d0_s24" to="o19551" />
    </statement>
    <statement id="d0_s25">
      <text>hypanthium persistent;</text>
      <biological_entity id="o19554" name="hypanthium" name_original="hypanthium" src="d0_s25" type="structure">
        <character is_modifier="false" name="duration" src="d0_s25" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>sepals persistent (withering), reflexed;</text>
      <biological_entity id="o19555" name="sepal" name_original="sepals" src="d0_s26" type="structure">
        <character is_modifier="false" name="duration" src="d0_s26" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s26" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>exocarp thin, wrinkled, mesocarp thin, dry, endocarp ± cartilaginous.</text>
      <biological_entity id="o19556" name="exocarp" name_original="exocarp" src="d0_s27" type="structure">
        <character is_modifier="false" name="width" src="d0_s27" value="thin" value_original="thin" />
        <character is_modifier="false" name="relief" src="d0_s27" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity id="o19557" name="mesocarp" name_original="mesocarp" src="d0_s27" type="structure">
        <character is_modifier="false" name="width" src="d0_s27" value="thin" value_original="thin" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s27" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>x = 9.</text>
      <biological_entity id="o19558" name="endocarp" name_original="endocarp" src="d0_s27" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence_or_texture" src="d0_s27" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity constraint="x" id="o19559" name="chromosome" name_original="" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; e Asia (China, Japan); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="introduced" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Kerria is a distinctive mesophytic shrub, native to montane forests and valleys at 200–3000 m in China and Japan, now widely cultivated. Morphologically, it stands closest to Neviusia.</discussion>
  
</bio:treatment>