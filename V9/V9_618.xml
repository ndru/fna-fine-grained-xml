<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="illustration_page">372</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="species">pumila</taxon_name>
    <taxon_name authority="(L. H. Bailey) Waugh" date="1899" rank="variety">besseyi</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Vermont Agric. Exp. Sta.</publication_title>
      <place_in_publication>12: 239. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species pumila;variety besseyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100710</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prunus</taxon_name>
    <taxon_name authority="L. H. Bailey" date="unknown" rank="species">besseyi</taxon_name>
    <place_of_publication>
      <publication_title>Cornell Univ. Exp. Sta. Bull.</publication_title>
      <place_in_publication>70: 261, plate 1. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Prunus;species besseyi</taxon_hierarchy>
  </taxon_identification>
  <number>26b.</number>
  <other_name type="common_name">Western or Bessey’s sandcherry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually erect-ascending, sometimes sprawling with ascending tips, 1–7 (–15) dm;</text>
      <biological_entity id="o1660" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect-ascending" value_original="erect-ascending" />
        <character constraint="with tips" constraintid="o1661" is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o1661" name="tip" name_original="tips" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>twigs usually glabrous, sometimes sparsely puberulent.</text>
      <biological_entity id="o1662" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades elliptic, oblanceolate, or obovate, 2.5–8 × 0.8–2.2 (–2.9) cm, lengths ca. 2.9 times widths, base cuneate, apex short-acuminate, acute, or obtuse.</text>
      <biological_entity id="o1663" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="2.9" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s2" to="2.2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="2.9" value_original="2.9" />
      </biological_entity>
      <biological_entity id="o1664" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o1665" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="short-acuminate" value_original="short-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Drupes subglobose, 8–12 × 5–12 mm;</text>
      <biological_entity id="o1666" name="drupe" name_original="drupes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stones subglobose to ovoid, 7–9 × 5–7 mm.</text>
      <biological_entity id="o1667" name="stone" name_original="stones" src="d0_s4" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s4" to="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy prairies, oak savannas, rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy prairies" />
        <character name="habitat" value="oak savannas" />
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Sask.; Colo., Ill., Ind., Iowa, Kans., Minn., Mont., Nebr., N.Dak., Ohio, Oreg., S.Dak., Utah, Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety besseyi has been introduced west of its native range as an ornamental and fruit bearer for the home garden, where its adaptation to cold winters and hot, dry summers is a valuable asset. Cultivars were developed by breeders at the Morden Experimental Station in Manitoba and at the University of South Dakota.</discussion>
  
</bio:treatment>