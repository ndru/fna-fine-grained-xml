<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">360</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="illustration_page">372</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Linnaeus" date="1767" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>1: 75. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100401</other_info_on_name>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Sandcherry</other_name>
  <other_name type="common_name">cerisier des sables</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, sometimes suckering, 1–15 (–25) dm, not thorny.</text>
      <biological_entity id="o10743" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with terminal end buds, glabrous or sparsely to densely puberulent (var. susquehanae).</text>
      <biological_entity id="o10744" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character char_type="range_value" from="glabrous or" name="pubescence" notes="" src="d0_s1" to="sparsely densely puberulent" />
      </biological_entity>
      <biological_entity constraint="end" id="o10745" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="terminal end" />
      <relation from="o10744" id="r655" name="with" negation="false" src="d0_s1" to="o10745" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o10746" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–10 (–13) mm, glabrous or hairy only when young, sometimes glandular distally or on margins at bases of blades, glands 1–2;</text>
      <biological_entity id="o10747" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="on margins" value_original="on margins" />
      </biological_entity>
      <biological_entity id="o10748" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o10749" name="base" name_original="bases" src="d0_s3" type="structure" />
      <biological_entity id="o10750" name="blade" name_original="blades" src="d0_s3" type="structure" />
      <biological_entity id="o10751" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
      <relation from="o10747" id="r656" name="on" negation="false" src="d0_s3" to="o10748" />
      <relation from="o10748" id="r657" name="at" negation="false" src="d0_s3" to="o10749" />
      <relation from="o10749" id="r658" name="part_of" negation="false" src="d0_s3" to="o10750" />
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic, oblanceolate, or obovate, 2.5–8 × 0.8–3 cm, base obtuse, cuneate, or long-attenuate, margins crenulate-serrulate to serrate in distal 1/2–2/3, teeth sharp or blunt, callus-tipped, sometimes glandular, apex short-acuminate to rounded, surfaces glabrous.</text>
      <biological_entity id="o10752" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10753" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-attenuate" value_original="long-attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-attenuate" value_original="long-attenuate" />
      </biological_entity>
      <biological_entity id="o10754" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="in distal 1/2-2/3" constraintid="o10755" is_modifier="false" name="shape_or_architecture" src="d0_s4" value="crenulate-serrulate to serrate" value_original="crenulate-serrulate to serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10755" name="1/2-2/3" name_original="1/2-2/3" src="d0_s4" type="structure" />
      <biological_entity id="o10756" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="callus-tipped" value_original="callus-tipped" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10757" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="short-acuminate" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o10758" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–5-flowered, umbellate fascicles.</text>
      <biological_entity id="o10759" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-5-flowered" value_original="2-5-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 3–19 mm, glabrous.</text>
      <biological_entity id="o10760" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="19" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers blooming before leaf emergence;</text>
      <biological_entity id="o10761" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="before leaf" constraintid="o10762" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o10762" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium campanulate, 1.7–3 mm, glabrous externally;</text>
      <biological_entity id="o10763" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect to reflexed, semicircular, 1.3–2.8 mm, margins glandular-toothed, surfaces glabrous;</text>
      <biological_entity id="o10764" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="reflexed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="semicircular" value_original="semicircular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10765" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o10766" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, oblanceolate, oblong, or suborbiculate, 3–9 mm;</text>
      <biological_entity id="o10767" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries glabrous.</text>
      <biological_entity id="o10768" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes dark purple to nearly black, subglobose or broadly ellipsoid, 6–12 mm, glabrous;</text>
      <biological_entity id="o10769" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark purple" name="coloration" src="d0_s12" to="nearly black" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mesocarps fleshy;</text>
      <biological_entity id="o10770" name="mesocarp" name_original="mesocarps" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stones subglobose, ovoid, or fusiform, not flattened.</text>
      <biological_entity id="o10771" name="stone" name_original="stones" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Ont., Que., Sask.; Ark., Colo., Conn., Del., Ill., Ind., Iowa, Kans., Maine, Mass., Md., Mich., Minn., Mont., N.C., N.Dak., N.H., N.J., N.Y., Nebr., Ohio, Oreg., Pa., R.I., S.Dak., Tenn., Utah, Va., Vt., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Opinion has varied as to whether Prunus pumila is best treated as one variable species (for example, H. Groh and H. A. Senn 1940; H. A. Gleason 1952; J. R. Rohrer 2000) or as two, three, or four separate species (for example, W. F. Wight 1915; M. L. Fernald 1923b; P. M. Catling et al. 1999). The plants vary in stem posture, twig indument, leaf shape, fruit size and taste, pit size and shape, and ecologic preference. Even though the morphologic characters show almost continuous variation, four varieties are recognized here based largely on differences in ecologic habitat and geographic range.</discussion>
  <references>
    <reference>Catling, P. M., S. M. McKay-Kuja, and G. Mitrow. 1999. Rank and typification in North American dwarf cherries, and a key to the taxa. Taxon 48: 483–488.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Twigs densely puberulent (10× magnification); leaf apices usually obtuse, sometimes acute or rounded, blade lengths ca. 2.6 times widths; sandy pine-oak woods or barrens with open canopy, or adjacent fields and lakeshores.</description>
      <determination>26d Prunus pumila var. susquehanae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Twigs usually glabrous, sometimes sparsely puberulent; leaf apices short-acuminate, acute, or obtuse, blade lengths ca. 2.9–3.7 times widths; usually in open habitats</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems prostrate to decumbent; leaf blades oblanceolate, lengths ca. 3.7 times widths; stones ovoid to fusiform, 4.5–5 mm wide.</description>
      <determination>26c Prunus pumila var. depressa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems usually erect-ascending, sometimes decumbent or sprawling; leaf blades elliptic, obovate, or oblanceolate, lengths ca. 2.9–3.3 times widths; stones subglobose to ovoid, 5–7 mm wide</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades usually oblanceolate, lengths ca. 3.3 times widths; shores of the Great Lakes on sandy, gravelly, or rocky beaches, dunes, and interdunal flats.</description>
      <determination>26a Prunus pumila var. pumila</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades elliptic, oblanceolate, or obovate, lengths ca. 2.9 times widths; sandy prairies, oak savannas, rock outcrops.</description>
      <determination>26b Prunus pumila var. besseyi</determination>
    </key_statement>
  </key>
</bio:treatment>