<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="illustration_page">285</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="Rydberg" date="1898" rank="species">cuneifolia</taxon_name>
    <taxon_name authority="(D. D. Keck) Ertter" date="2007" rank="variety">ewanii</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 44. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species cuneifolia;variety ewanii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100619</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">glandulosa</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">ewanii</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>520: 47. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species glandulosa;subspecies ewanii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drymocallis</taxon_name>
    <taxon_name authority="(Lindley) Rydberg" date="unknown" rank="species">glandulosa</taxon_name>
    <taxon_name authority="(D. D. Keck) Soják" date="unknown" rank="subspecies">ewanii</taxon_name>
    <taxon_hierarchy>genus Drymocallis;species glandulosa;subspecies ewanii</taxon_hierarchy>
  </taxon_identification>
  <number>15b.</number>
  <other_name type="common_name">Ewan’s drymocallis or wood beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.5–2 (–2.5) dm.</text>
      <biological_entity id="o17709" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 2–10 cm, leaflet pairs 3–5;</text>
      <biological_entity constraint="basal" id="o17710" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17711" name="leaflet" name_original="leaflet" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>terminal leaflet broadly cuneate to nearly round, 0.6–1.5 cm.</text>
      <biological_entity constraint="terminal" id="o17712" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s2" to="nearly round" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Epicalyx bractlets linear-elliptic to lanceolate, 1 × 0.5 mm.</text>
      <biological_entity constraint="epicalyx" id="o17713" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-elliptic" name="shape" src="d0_s3" to="lanceolate" />
        <character name="length" src="d0_s3" unit="mm" value="1" value_original="1" />
        <character name="width" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Achenes 0.8–1 mm.</text>
      <biological_entity id="o17714" name="achene" name_original="achenes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges of seeps, springs, and waterways, in lower montane coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of seeps , springs , and waterways" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="waterways" />
        <character name="habitat" value="lower montane coniferous" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety ewanii is known from only four occurrences in the Dawson Saddle area of the San Gabriel Mountains, Los Angeles County.</discussion>
  
</bio:treatment>