<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Ehrhart" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">serotina</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species serotina;variety serotina</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100716</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prunus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="(Small) McVaugh" date="unknown" rank="subspecies">eximia</taxon_name>
    <taxon_hierarchy>genus Prunus;species serotina;subspecies eximia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">eximia</taxon_name>
    <taxon_hierarchy>genus P.;species serotina;variety eximia</taxon_hierarchy>
  </taxon_identification>
  <number>6a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Twigs usually glabrous, sometimes hairy, hairs rusty brown.</text>
      <biological_entity id="o9895" name="twig" name_original="twigs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9896" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="rusty brown" value_original="rusty brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (7–) 10–23 (–30) mm, glabrous, glandular distally, glands 1–6;</text>
      <biological_entity id="o9897" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o9898" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="23" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9899" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade usually narrowly elliptic to elliptic or lanceolate, rarely obovate, 4–13.5 × 1.7–5.8 cm, membranous or slightly leathery, apex acute to acuminate, sometimes abruptly so, abaxial surface glabrous or midribs sparsely to densely hairy proximally.</text>
      <biological_entity id="o9900" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9901" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="to apex, surface" constraintid="o9902, o9903" is_modifier="false" modifier="usually narrowly" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9902" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="rarely" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="cm" is_modifier="true" name="length" src="d0_s2" to="13.5" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" is_modifier="true" name="width" src="d0_s2" to="5.8" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
        <character is_modifier="true" modifier="slightly" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9903" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="rarely" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="cm" is_modifier="true" name="length" src="d0_s2" to="13.5" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" is_modifier="true" name="width" src="d0_s2" to="5.8" to_unit="cm" />
        <character is_modifier="true" name="texture" src="d0_s2" value="membranous" value_original="membranous" />
        <character is_modifier="true" modifier="slightly" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9904" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o9905" name="midrib" name_original="midribs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to densely; proximally" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: rachises 35–140 mm. 2n = 32.</text>
      <biological_entity id="o9906" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o9907" name="rachis" name_original="rachises" src="d0_s3" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s3" to="140" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9908" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun; fruiting Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, edges of fields, secondary forests, open woodlands, ravines, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="edges" constraint="of fields , secondary forests , open woodlands , ravines , along" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="secondary forests" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="ravines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., Wash., W.Va., Wis.; Mexico (Chiapas, Chihuahua, Coahuila, Colima [Socorro Island], Durango, Hidalgo, Jalisco, Michoacán, Nayarit, Nuevo León, Oaxaca, Puebla, San Luis Potosí, Tamaulipas, Veracruz); Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico (Chiapas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Colima [Socorro Island])" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Hidalgo)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Michoacán)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Oaxaca)" establishment_means="native" />
        <character name="distribution" value="Mexico (Puebla)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Prunus serotina from the Edwards Plateau in central Texas have often been recognized as subsp. or var. eximia. If it were not for their geographic isolation from the rest of var. serotina by 200 km, it is doubtful that taxonomists would even think about segregating these outliers from the plants of eastern North America. All of the suggested distinctive characteristics of var. eximia (glabrous leaves, branchlets, and inflorescences; coarsely toothed leaf margins; long petioles) can be found both separately and combined in specimens from other areas within the main range of var. serotina. The trees of the Edwards Plateau seem to be merely a homogeneous subset of the wide-ranging, heterogeneous var. serotina.</discussion>
  
</bio:treatment>