<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">438</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">452</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="(Ledebour) Loddiges" date="unknown" rank="species">melanocarpus</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Nat. Syn. Monogr.</publication_title>
      <place_in_publication>3: 223. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species melanocarpus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200010751</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotoneaster</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="variety">melanocarpus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Altaica</publication_title>
      <place_in_publication>2: 219. 1830 (as melanocarpa)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotoneaster;species vulgaris;variety melanocarpus</taxon_hierarchy>
  </taxon_identification>
  <number>34.</number>
  <other_name type="common_name">Dark cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2–2.5 m.</text>
      <biological_entity id="o2608" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems loosely erect;</text>
      <biological_entity id="o2609" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches spiraled, yellow-brown to red brown, lenticellate, shiny, initially densely pilose.</text>
      <biological_entity id="o2610" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character char_type="range_value" from="yellow-brown" name="coloration" src="d0_s2" to="red brown" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s2" value="lenticellate" value_original="lenticellate" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="initially densely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o2611" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 4–7 mm, tomentose-villous;</text>
      <biological_entity id="o2612" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose-villous" value_original="tomentose-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic to ovate, 33–45 × 20–32 mm, chartaceous, base rounded, margins flat, veins 5–7, superficial, apex acute or obtuse, abaxial surfaces densely silvery-pilose-villous, adaxial green to dark green, dull to slightly shiny, not glaucous, flat between lateral-veins, rugose, sparsely pilose;</text>
      <biological_entity id="o2613" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="33" from_unit="mm" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s5" to="32" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o2614" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o2615" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o2616" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o2617" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2618" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="silvery-pilose-villous" value_original="silvery-pilose-villous" />
      </biological_entity>
      <biological_entity id="o2620" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves lacking notable color.</text>
      <biological_entity constraint="adaxial" id="o2619" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="dark green" />
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s5" to="slightly shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character constraint="between lateral-veins" constraintid="o2620" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" notes="" src="d0_s5" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o2621" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o2619" id="r159" name="fall" negation="false" src="d0_s6" to="o2621" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 25–30 mm with 3 or 4 leaves, 5–13-flowered, pendent, lax.</text>
      <biological_entity id="o2622" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o2623" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with 3 or 4leaves , 5-13-flowered , pendent" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <relation from="o2622" id="r160" name="on" negation="false" src="d0_s7" to="o2623" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 3–8 mm, sometimes sparsely villous.</text>
      <biological_entity id="o2624" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers erect, 6–7 mm, open;</text>
      <biological_entity id="o2625" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium cupulate, dark reddish-brown, glabrous;</text>
      <biological_entity id="o2626" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins erose, apex acute or obtuse, surfaces glabrous, often apically villous;</text>
      <biological_entity id="o2627" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o2628" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o2629" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o2630" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often apically" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals erect, greenish white with pink, red, base slightly darkened, margins white, glabrous;</text>
      <biological_entity id="o2631" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o2632" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white with pink" value_original="greenish white with pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o2633" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="coloration" src="d0_s12" value="darkened" value_original="darkened" />
      </biological_entity>
      <biological_entity id="o2634" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 20 (–22), filaments white, anthers white;</text>
      <biological_entity id="o2635" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o2636" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="22" />
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o2637" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o2638" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2–4.</text>
      <biological_entity id="o2639" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o2640" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes purple-black, obovoid or globose, 7–9 × 7–9 mm, dull, glaucous with blue tinge, glabrous;</text>
      <biological_entity id="o2641" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple-black" value_original="purple-black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s15" to="9" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character constraint="with blue tinge" is_modifier="false" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals suberect, glabrous or apically villous;</text>
    </statement>
    <statement id="d0_s17">
      <text>navel open;</text>
      <biological_entity id="o2642" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants 2/3 from base.</text>
      <biological_entity constraint="style" id="o2643" name="remnant" name_original="remnants" src="d0_s18" type="structure">
        <character constraint="from base" constraintid="o2644" name="quantity" src="d0_s18" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o2644" name="base" name_original="base" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes 2–4.2n = 48–52, 68 (Russia).</text>
      <biological_entity id="o2645" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s19" to="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2646" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character char_type="range_value" from="48" name="quantity" src="d0_s19" to="52" />
        <character name="quantity" src="d0_s19" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forested ravines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forested ravines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Man.; Eurasia (Russia, Ukraine).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" value="Eurasia (Russia)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Ukraine)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">melanocarpa</other_name>
  <discussion>Cotoneaster melanocarpus is said to hybridize spontaneously with Sorbus aucuparia in Siberia, forming x\Sorbocotoneaster Pojarkova. The actual Cotoneaster parent is more likely to be a diploid and needs investigation.</discussion>
  
</bio:treatment>