<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1824" rank="genus">cercocarpus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">ledifolius</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">ledifolius</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus cercocarpus;species ledifolius;variety ledifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100545</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cercocarpus</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">hypoleucus</taxon_name>
    <taxon_hierarchy>genus Cercocarpus;species hypoleucus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ledifolius</taxon_name>
    <taxon_name authority="C. K. Schneider" date="unknown" rank="variety">intercedens</taxon_name>
    <taxon_hierarchy>genus C.;species ledifolius;variety intercedens</taxon_hierarchy>
  </taxon_identification>
  <number>4a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 10–30 dm.</text>
      <biological_entity id="o7479" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: long-shoot internodes 10–17 mm, sparsely villous, sericeous, or hirsute;</text>
      <biological_entity id="o7481" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="long-shoot" id="o7482" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="17" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>short-shoots 1–15 × 1.7–3 mm.</text>
      <biological_entity id="o7483" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o7484" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole (1.5–) 2–4.5 mm;</text>
      <biological_entity id="o7485" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7486" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly lanceolate to linear, sometimes lance-elliptic, (7–) 13–29 (–48) × (1.5–) 2–5 (–7) mm, margins revolute (leaving much of abaxial surface exposed), abaxial surface usually densely villous, sometimes sericeous, adaxial usually moderately, often loosely, villous or sericeous, rarely pilose, usually glabrescent.</text>
      <biological_entity id="o7487" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7488" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate to linear" value_original="lanceolate to linear" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s4" value="lance-elliptic" value_original="lance-elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s4" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="29" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="48" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s4" to="29" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7489" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7490" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7491" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often loosely; loosely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 1–5 per short-shoot;</text>
      <biological_entity id="o7492" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per short-shoot" constraintid="o7493" from="1" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o7493" name="short-shoot" name_original="short-shoot" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>hypanthial tubes strongly villous;</text>
      <biological_entity constraint="hypanthial" id="o7494" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals 2–2.5 mm.</text>
      <biological_entity id="o7495" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 6.5–9 × 1.2–1.4 mm;</text>
      <biological_entity id="o7496" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>fruit awns 4–7.5 cm, proximal setae 1.6–2.7 mm.</text>
      <biological_entity constraint="fruit" id="o7497" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7498" name="seta" name_original="setae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s9" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky outcrops, talus slopes, with Artemisia, Purshia, Gambel oak, pinyon pine and juniper</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="gambel oak" />
        <character name="habitat" value="pinyon pine" />
        <character name="habitat" value="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(200–)1300–2300(–3000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety ledifolius has narrowly lanceolate to linear leaves that are initially strongly villous adaxially and often strongly villous-woolly abaxially, obscuring the veins between the revolute margins. The adaxial surfaces are usually glabrescent; vestiture is not very dense in sericeous plants.</discussion>
  <discussion>Here, var. ledifolius comprises intermediates between var. intermontanus and var. intricatus representing hybrids and introgressants that together form breeding populations in zones of parental overlap and in regions outside the range of either parent. Variety ledifolius is considered to be polyphyletic and is much more variable than either parent. Some specimens have relatively small leaves approaching those in var. intricatus. Some specimens have larger long-shoot leaves as in var. intermontanus (with diminished [glabrescent] vestiture and narrower margins) and short-shoot leaves as in var. ledifolius.</discussion>
  
</bio:treatment>