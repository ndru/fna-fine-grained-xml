<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="Pojarkova" date="1955" rank="species">rehderi</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mater. Gerb. Bot. Inst. Komarova Akad. Nauk S.S.S.R.</publication_title>
      <place_in_publication>17: 184. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species rehderi</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242315142</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotoneaster</taxon_name>
    <taxon_name authority="Bois" date="unknown" rank="species">bullatus</taxon_name>
    <taxon_name authority="Rehder &amp; E. H. Wilson" date="unknown" rank="variety">macrophyllus</taxon_name>
    <place_of_publication>
      <publication_title>in C. S. Sargent, Pl. Wilson.</publication_title>
      <place_in_publication>1: 164. 1912 (as bullata var. macrophylla)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotoneaster;species bullatus;variety macrophyllus</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <other_name type="common_name">Bullate cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2–5 m.</text>
      <biological_entity id="o9501" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, arching;</text>
      <biological_entity id="o9502" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distichous or spiraled, maroon to brown, lenticellate, initially pilose-strigose.</text>
      <biological_entity id="o9503" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character char_type="range_value" from="maroon" name="coloration" src="d0_s2" to="brown" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s2" value="lenticellate" value_original="lenticellate" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s2" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o9504" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–3 mm, pilose-strigose;</text>
      <biological_entity id="o9505" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, sometimes ovate-elliptic, rarely obovate, (50–) 70–124 (–210) x (24–) 30–55 (–90) mm, subcoriaceous, base cuneate, obtuse, truncate, or auriculate, margins flat, not revolute, veins 8–11, deeply sunken, apex long-acuminate, sometimes acute on short-shoots, abaxial surfaces light green, initially yellowish strigose, adaxial green, shiny, not glaucous, strongly wrinkled or bulging between lateral-veins (bullate), initially sparsely pilose;</text>
      <biological_entity id="o9506" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="texture" src="d0_s5" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o9507" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o9508" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o9509" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="11" />
        <character is_modifier="false" modifier="deeply" name="prominence" src="d0_s5" value="sunken" value_original="sunken" />
      </biological_entity>
      <biological_entity id="o9510" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
        <character constraint="on short-shoots" constraintid="o9511" is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9511" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o9512" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="initially" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o9514" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves intense butter yellow and reddish purple.</text>
      <biological_entity constraint="adaxial" id="o9513" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s5" value="wrinkled" value_original="wrinkled" />
        <character constraint="between lateral-veins" constraintid="o9514" is_modifier="false" name="pubescence_or_shape" src="d0_s5" value="bulging" value_original="bulging" />
        <character is_modifier="false" modifier="initially sparsely" name="pubescence" notes="" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow and reddish purple" value_original="yellow and reddish purple" />
      </biological_entity>
      <biological_entity id="o9515" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="intense" value_original="intense" />
      </biological_entity>
      <relation from="o9513" id="r597" name="fall" negation="false" src="d0_s6" to="o9515" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 60–120 mm with 4 leaves, 8–32-flowered, lax.</text>
      <biological_entity id="o9516" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="8-32-flowered" value_original="8-32-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o9517" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with leaves" constraintid="o9518" from="60" from_unit="mm" name="some_measurement" src="d0_s7" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9518" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <relation from="o9516" id="r598" name="on" negation="false" src="d0_s7" to="o9517" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–4 mm, pilose-strigose.</text>
      <biological_entity id="o9519" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers (5–) 7–9 mm, closed;</text>
      <biological_entity id="o9520" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" name="condition" src="d0_s9" value="closed" value_original="closed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium shallowly cupulate, shiny, sparsely pilose-strigose;</text>
      <biological_entity id="o9521" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins villous, apex acute, surfaces shiny, glabrous;</text>
      <biological_entity id="o9522" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o9523" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o9524" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9525" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals erect-incurved, pink, red, greenish pink, or maroon, margins pink;</text>
      <biological_entity id="o9526" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o9527" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect-incurved" value_original="erect-incurved" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish pink" value_original="greenish pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish pink" value_original="greenish pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity id="o9528" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 20–23, filaments pink, white distally, anthers white;</text>
      <biological_entity id="o9529" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o9530" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="23" />
      </biological_entity>
      <biological_entity id="o9531" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o9532" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles (4 or) 5.</text>
      <biological_entity id="o9533" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o9534" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes bright to dark red, obovoid, broadly obovoid, or obconic, rarely globose or depressed-globose, 8–12.4 × 7–11.3 mm, shiny, not glaucous, sparsely pilose;</text>
      <biological_entity id="o9535" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s15" value="bright" value_original="bright" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-globose" value_original="depressed-globose" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-globose" value_original="depressed-globose" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s15" to="12.4" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s15" to="11.3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals flat, glabrous;</text>
    </statement>
    <statement id="d0_s17">
      <text>navel closed;</text>
      <biological_entity id="o9536" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="condition" src="d0_s17" value="closed" value_original="closed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants 3/4 from base.</text>
      <biological_entity constraint="style" id="o9537" name="remnant" name_original="remnants" src="d0_s18" type="structure">
        <character constraint="from base" constraintid="o9538" name="quantity" src="d0_s18" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o9538" name="base" name_original="base" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes (4 or) 5.</text>
      <biological_entity id="o9539" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Sep–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Dec" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, disturbed forests, flood plains, lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="disturbed forests" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Alaska, Wash.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reports of Cotoneaster bullatus (treated as C. bullatus var. bullatus by L. Lingdi and A. R. Brach 2003) from British Columbia (J. Pojar 1999) are here referred to C. rehderi.</discussion>
  
</bio:treatment>