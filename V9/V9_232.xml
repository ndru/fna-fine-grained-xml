<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">160</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson" date="1899" rank="section">Graciles</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="species">bruceae</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 342. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section graciles;species bruceae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100312</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="(Rydberg) D. D. Keck" date="unknown" rank="subspecies">bruceae</taxon_name>
    <taxon_hierarchy>genus Potentilla;species drummondii;subspecies bruceae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="(Rydberg) N. H. Holmgren" date="unknown" rank="variety">bruceae</taxon_name>
    <taxon_hierarchy>genus P.;species drummondii;variety bruceae</taxon_hierarchy>
  </taxon_identification>
  <number>28.</number>
  <other_name type="common_name">Bruce’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Glands absent or inconspicuous, uncolored.</text>
      <biological_entity id="o34647" name="gland" name_original="glands" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s0" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="uncolored" value_original="uncolored" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to nearly erect, (1–) 2–5 (–6) dm.</text>
      <biological_entity id="o34648" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="nearly erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves not in ranks, ± subpalmate to subpinnate (proximal leaflets sometimes doubled, distal leaflets confluent and/or decurrent), (2–) 5–20 (–28) cm;</text>
      <biological_entity constraint="basal" id="o34649" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="less subpalmate" name="architecture" notes="" src="d0_s2" to="subpinnate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="28" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34650" name="rank" name_original="ranks" src="d0_s2" type="structure" />
      <relation from="o34649" id="r2281" name="in" negation="false" src="d0_s2" to="o34650" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (1–) 3–15 (–20) cm, long hairs abundant, appressed to ascending-spreading, 1–2.5 mm, weak often flattened and twisted, sometimes grading to crisped or cottony hairs, short hairs absent, glands absent or sparse;</text>
      <biological_entity id="o34651" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34652" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s3" to="ascending-spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o34653" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o34654" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o34655" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o34652" id="r2282" modifier="sometimes" name="to" negation="false" src="d0_s3" to="o34653" />
    </statement>
    <statement id="d0_s4">
      <text>leaflets 5–7, on distal 1/10–1/4 of leaf axis, ± overlapping, largest ones usually ± obovate, (1–) 1.5–5 (–6) × 1–3.5 (–4) cm, margins flat, distal 2/3–3/4 evenly to unevenly incised ± 1/2 to midvein (often with additional incisions nearly to midvein), undivided medial blade 4–20 mm wide, teeth (2–) 3–6 (–8) per side (sometimes secondarily toothed), ± lanceolate, 4–11 mm, surfaces ± similar, abaxial ± lighter and hairier, gray-green to grayish, rarely green, not glaucous, long hairs ± abundant, short-crisped hairs absent or sparse to common, cottony hairs usually absent, glands absent or sparse.</text>
      <biological_entity id="o34656" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character is_modifier="false" modifier="usually more or less" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o34657" name="1/10-1/4" name_original="1/10-1/4" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o34658" name="axis" name_original="axis" src="d0_s4" type="structure" />
      <biological_entity id="o34659" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s4" to="3/4" />
        <character is_modifier="false" modifier="evenly to unevenly; more or less" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character constraint="to midvein" constraintid="o34660" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o34660" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity constraint="medial" id="o34661" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="undivided" value_original="undivided" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34662" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character char_type="range_value" constraint="per side" constraintid="o34663" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34663" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o34664" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o34665" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s4" value="lighter" value_original="lighter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairier" value_original="hairier" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s4" to="grayish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o34666" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o34667" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="short-crisped" value_original="short-crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
      </biological_entity>
      <biological_entity id="o34668" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o34669" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o34656" id="r2283" name="on" negation="false" src="d0_s4" to="o34657" />
      <relation from="o34657" id="r2284" name="part_of" negation="false" src="d0_s4" to="o34658" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 1–2.</text>
      <biological_entity constraint="cauline" id="o34670" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 5–50-flowered.</text>
      <biological_entity id="o34671" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-50-flowered" value_original="5-50-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–3 (–4) cm.</text>
      <biological_entity id="o34672" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: epicalyx bractlets broadly lanceolate to elliptic, 3–6 (–7.5) × 1.5–2.5 (–3) mm, hairs ± common to abundant, loosely appressed to ascending, glands absent or inconspicuous;</text>
      <biological_entity id="o34673" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="epicalyx" id="o34674" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34675" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="less common" name="quantity" src="d0_s8" to="abundant" />
        <character char_type="range_value" from="loosely appressed" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
      <biological_entity id="o34676" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 3.5–5.5 mm diam.;</text>
      <biological_entity id="o34677" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34678" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s9" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5–10 mm, apex acute to acuminate;</text>
      <biological_entity id="o34679" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34680" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34681" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5–10 × 5–10 mm;</text>
      <biological_entity id="o34682" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34683" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 1–2.5 mm, anthers 0.7–1.2 mm;</text>
      <biological_entity id="o34684" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34685" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34686" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>carpels 15–30, styles filiform-tapered, papillate-swollen proximally, 2–3 mm.</text>
      <biological_entity id="o34687" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34688" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s13" to="30" />
      </biological_entity>
      <biological_entity id="o34689" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform-tapered" value_original="filiform-tapered" />
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s13" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 1.4–1.6 mm. 2n = 63–73, 79, 87, 98, 129.</text>
      <biological_entity id="o34690" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34691" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character char_type="range_value" from="63" name="quantity" src="d0_s14" to="73" />
        <character name="quantity" src="d0_s14" value="79" value_original="79" />
        <character name="quantity" src="d0_s14" value="87" value_original="87" />
        <character name="quantity" src="d0_s14" value="98" value_original="98" />
        <character name="quantity" src="d0_s14" value="129" value_original="129" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry montane meadows and adjacent slopes, in conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane meadows" modifier="dry" />
        <character name="habitat" value="adjacent slopes" />
        <character name="habitat" value="conifer woodlands" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As redefined by B. Ertter and D. Mansfield (2007), Potentilla bruceae is characterized by irregularly subpalmate, weakly villose leaves. Such plants form relatively consistent and extensive populations in dry meadows centered near Lake Tahoe in California and Nevada, extending to the central Sierra Nevada and to the Warner Mountains in Oregon. This interpretation differs from that previously suggested by Ertter (1992), in which P. bruceae was interpreted as the catchall category for hybrids between P. breweri (sect. Multijugae) and P. drummondii. All three taxa were accordingly treated as varieties of P. drummondii. D. D. Keck (in J. Clausen et al. 1940) treated P. bruceae as a subspecies of P. drummondii; B. C. Johnston (1980) concluded that its placement as a variety of P. breweri was more justified. The three entities unquestionably intergrade (with the type of P. anomalofolia M. Peck possibly one result), but not beyond what is the norm for facultatively apomictic species of Potentilla.</discussion>
  <discussion>Chromosome numbers reported by J. Clausen et al. (1940) require verification as this species; the irregular behavior observed in meiotic material suggests that at least some of the individuals were hybrids.</discussion>
  <discussion>See discussion of 29. Potentilla drummondii regarding leaflet dissection and the inclusion of P. bruceae in sect. Graciles.</discussion>
  
</bio:treatment>