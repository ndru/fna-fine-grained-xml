<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Potentilla</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Potentilla</taxon_hierarchy>
    <other_info_on_name type="fna_id">304105</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">Tormentilla</taxon_name>
    <taxon_hierarchy>genus Potentilla;section Tormentilla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="section">Tormentilla</taxon_name>
    <taxon_hierarchy>section Tormentilla</taxon_hierarchy>
  </taxon_identification>
  <number>8d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, openly matted or ± tufted, often stoloniferous;</text>
      <biological_entity id="o34921" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="openly" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="more or less; more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots usually replaced by thick rootstocks;</text>
      <biological_entity id="o34922" name="taproot" name_original="taproots" src="d0_s1" type="structure" />
      <biological_entity id="o34923" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o34922" id="r2309" name="replaced by" negation="false" src="d0_s1" to="o34923" />
    </statement>
    <statement id="d0_s2">
      <text>vestiture mostly of long hairs, glands absent or sparse, rarely common, sometimes reddish.</text>
      <biological_entity id="o34924" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o34925" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o34926" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s2" value="common" value_original="common" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o34924" id="r2310" name="part_of" negation="false" src="d0_s2" to="o34925" />
    </statement>
    <statement id="d0_s3">
      <text>Stems usually becoming ± prostrate, sometimes ascending to erect, often flagelliform, often rooting at nodes, lateral or central to persistent or ephemeral basal rosettes, 0.3–12+ dm, lengths (1–) 2–10+ times basal leaves.</text>
      <biological_entity id="o34927" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually becoming more or less" name="growth_form_or_orientation" src="d0_s3" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="ascending" modifier="sometimes" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o34928" is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character constraint="to basal rosettes" constraintid="o34929" is_modifier="false" name="position" src="d0_s3" value="central" value_original="central" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="12" to_unit="dm" upper_restricted="false" />
        <character constraint="leaf" constraintid="o34930" is_modifier="false" name="length" src="d0_s3" value="(1-)2-10+ times basal leaves" value_original="(1-)2-10+ times basal leaves" />
      </biological_entity>
      <biological_entity id="o34928" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o34929" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="duration" src="d0_s3" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
      <biological_entity constraint="basal" id="o34930" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal and cauline not in ranks;</text>
      <biological_entity id="o34931" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="in ranks" constraintid="o34932" is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o34932" name="rank" name_original="ranks" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>cauline (proximal to flowering and/or branching nodes) 0–3 (–7);</text>
      <biological_entity id="o34933" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o34934" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves ternate or palmate, 2–20 (–30) cm;</text>
      <biological_entity id="o34935" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o34936" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="ternate" value_original="ternate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="palmate" value_original="palmate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs appressed to spreading, weak to stiff, glands absent or sparse, sometimes common;</text>
      <biological_entity id="o34937" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o34938" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s7" to="spreading" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s7" to="stiff" />
      </biological_entity>
      <biological_entity id="o34939" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s7" value="common" value_original="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets 3–5 (–7), at tip of leaf axis, separate to slightly overlapping, obovate to narrowly elliptic, cuneate, or oblanceolate, margins flat or slightly revolute, distal 1/2–3/4 evenly incised 1/4–1/2 to midvein, teeth 2–13 per side, surfaces similar to ± dissimilar, abaxial usually green, sometimes silvery white, cottony hairs absent, adaxial green, not glaucous, long hairs usually ± stiff, sometimes weak or absent.</text>
      <biological_entity id="o34940" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o34941" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="separate" name="arrangement" notes="" src="d0_s8" to="slightly overlapping" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="narrowly elliptic cuneate or oblanceolate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="narrowly elliptic cuneate or oblanceolate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="narrowly elliptic cuneate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o34942" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o34943" name="axis" name_original="axis" src="d0_s8" type="structure" />
      <biological_entity id="o34944" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s8" to="3/4" />
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o34945" from="1/4" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity id="o34945" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o34946" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o34947" from="2" name="quantity" src="d0_s8" to="13" />
      </biological_entity>
      <biological_entity id="o34947" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o34948" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o34949" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less; usually" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="silvery white" value_original="silvery white" />
      </biological_entity>
      <biological_entity id="o34950" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34951" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o34952" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" modifier="usually more or less" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s8" value="weak" value_original="weak" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o34941" id="r2311" name="at" negation="false" src="d0_s8" to="o34942" />
      <relation from="o34942" id="r2312" name="part_of" negation="false" src="d0_s8" to="o34943" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences solitary flowers at stolon nodes or 3–30-flowered, cymose, open.</text>
      <biological_entity id="o34953" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o34954" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-30-flowered" value_original="3-30-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="stolon" id="o34955" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <relation from="o34954" id="r2313" name="at" negation="false" src="d0_s9" to="o34955" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels straight or slightly curved in fruit, (1–) 2–12 (–17) cm, proximal not longer than distal.</text>
      <biological_entity id="o34956" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="in fruit" constraintid="o34957" is_modifier="false" modifier="slightly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="17" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34957" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o34958" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o34959" is_modifier="false" name="length_or_size" src="d0_s10" value="not longer" value_original="not longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o34959" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 4–5 (–10) -merous;</text>
      <biological_entity id="o34960" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="4-5(-10)-merous" value_original="4-5(-10)-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 1.5–5 (–7) mm diam.;</text>
      <biological_entity id="o34961" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals usually bright-yellow, rarely cream, ± obcordate or obovate to round, (2–) 4–9 (–12) mm, usually longer than sepals, apex rounded to retuse;</text>
      <biological_entity id="o34962" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="cream" value_original="cream" />
        <character char_type="range_value" from="less obcordate or obovate" name="shape" src="d0_s13" to="round" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character constraint="than sepals" constraintid="o34963" is_modifier="false" name="length_or_size" src="d0_s13" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o34963" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o34964" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 15–20;</text>
      <biological_entity id="o34965" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, columnar-clavate to ± filiform, not papillate-swollen proximally, 0.6–1.5 mm.</text>
      <biological_entity id="o34966" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character char_type="range_value" from="columnar-clavate" name="shape" src="d0_s15" to="more or less filiform" />
        <character is_modifier="false" modifier="not; proximally" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth or rugose.</text>
      <biological_entity id="o34967" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s16" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, Eurasia, n Africa, Atlantic Islands; introduced in w North America, Mexico, West Indies, Bermuda, Central America, South America, c Africa (Ethiopia), Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="in w North America" establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Bermuda" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="c Africa (Ethiopia)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7 or 8 (5 in the flora).</discussion>
  <discussion>The species of sect. Potentilla comprise a monophyletic clade that includes Duchesnea, diverging basally to the species placed here in Horkelia, Horkeliella, and Ivesia (C. Dobeš and J. Paule 2010; M. H. Töpel et al. 2011). There are at least some morphological features by which this and other clades basal to the core Potentilla could be treated as separate genera. However, since the type of the genus (P. reptans) is in this section, doing so would require either a massive renaming of most Potentilla, or else conserving the type of the genus on a different species. If the latter course were taken, the species in this section would comprise Tormentilla.</discussion>
  <discussion>Among the distinctive features of sect. Potentilla are the high percentage of stoloniferous species (shared with Duchesnea), a tuberous rootstock in many species, and the presence of tetramerous-flowered species (P. anglica, P. erecta). The section is also distinctive in having an amphi-Atlantic distribution, with native species in both eastern North America and Europe. The native material has sometimes been treated as a single species, or with confused nomenclature (M. L. Fernald 1931), leading to much unreliability in older herbarium annotations.</discussion>
  <discussion>When leaves are palmate, the lateral leaflet pairs are usually more or less fused at the base, suggesting a ternate origin. Distal cauline leaves and inflorescence bracts are sometimes opposite. For comparison with other sections, counts of cauline leaves are restricted to nodes proximal to the first flowering stolon node, but descriptions of cauline leaves otherwise include all well-developed foliar structures at stolon nodes (until such time as these root and form new basal rosettes).</discussion>
  <references>
    <reference>Fernald, M. L. 1931. Potentilla canadensis and P. simplex. Rhodora 33: 180–191.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers 4(–5)-merous; stems usually openly branched; leaflets 3–5; hypanthia 1.5–4 mm diam</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers 5(–10)-merous; stems not branched; leaflets (3–)5(–7); hypanthia 2.5–7 mm diam</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems soon becoming prostrate, ± flagelliform, eventually rooting at some nodes; inflorescences mostly solitary flowers at stolon nodes; cauline leaves: petioles 0.3–4(–8) cm; basal leaves ± persistent.</description>
      <determination>7 Potentilla anglica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems mostly ascending to erect, not flagelliform, not rooting at nodes; inflorescences cymose, 3–30-flowered; cauline leaves: petioles usually 0 cm; basal leaves ephemeral.</description>
      <determination>8 Potentilla erecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals 7–9(–12) × 6–9(–11) mm; anthers (1–)1.3–2 mm; epicalyx bractlets elliptic or oblong to ovate, 4–10 × 1.5–3.5 mm, often much larger than sepals (especially in fruit); petiole hairs tightly to loosely appressed, 0.5–1.5 mm, leaflet surfaces similar, abaxial green, sparsely to moderately hairy; cauline leaves 2–3(–4) proximal to 1st flowering node; achenes ± rugose; introduced.</description>
      <determination>6 Potentilla reptans</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals 4–7(–8) × 3–6.5(–8) mm; anthers 0.4–1 mm; epicalyx bractlets linear to narrowly lanceolate or lanceolate-elliptic, 2–5(–6) × 0.8–1.5(–2) mm, slightly smaller than to larger than sepals (especially in bud); petiole hairs appressed to spreading, (0.5–)1–3 mm, leaflet surfaces similar to ± dissimilar, abaxial green to silvery white, sparsely to densely hairy; cauline leaves 0–1 proximal to 1st flowering node; achenes smooth or faintly rugose; native</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cauline leaves not fully expanded at anthesis, 0–1 proximal to 1st flowering node, leaflets ± resembling those of basal leaves, apex rounded to obtuse, teeth 2–7 per side on distal 1/2 of leaflet; rootstocks erect, stout, 0.5–2 cm.</description>
      <determination>4 Potentilla canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cauline leaves well expanded at anthesis, (0–)1 proximal to 1st flowering node, leaflets often more elongate than those of basal leaves, apex acute to obtuse, teeth 4–8(–13) per side on distal 1/2–3/4 of leaflet; rootstocks horizontal, irregularly thickened or moniliform, 1–8 cm.</description>
      <determination>5 Potentilla simplex</determination>
    </key_statement>
  </key>
</bio:treatment>