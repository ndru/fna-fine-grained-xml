<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Rubricaules</taxon_name>
    <taxon_name authority="A. E. Porsild" date="1951" rank="species">furcata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Natl. Mus. Canada</publication_title>
      <place_in_publication>121: 224, plate 18. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section rubricaules;species furcata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100326</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">hookeriana</taxon_name>
    <taxon_name authority="(A. E. Porsild) Hultén" date="unknown" rank="variety">furcata</taxon_name>
    <taxon_hierarchy>genus Potentilla;species hookeriana;variety furcata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">rubricaulis</taxon_name>
    <taxon_name authority="(A. E. Porsild) Soják" date="unknown" rank="variety">furcata</taxon_name>
    <taxon_hierarchy>genus P.;species rubricaulis;variety furcata</taxon_hierarchy>
  </taxon_identification>
  <number>85.</number>
  <other_name type="common_name">Forked cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches usually not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o14855" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="with leaves" constraintid="o14856" is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o14856" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (0.5–) 1–3 dm.</text>
      <biological_entity id="o14857" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves often both ternate and palmate on same plant, sometimes subpalmate, 4–10 cm;</text>
      <biological_entity constraint="basal" id="o14858" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="ternate" value_original="ternate" />
        <character constraint="on plant" constraintid="o14859" is_modifier="false" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s2" value="subpalmate" value_original="subpalmate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14859" name="plant" name_original="plant" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 2.5–8 cm, long hairs common to abundant, loosely appressed to ascending-spreading, 1–2 mm, weak to ± stiff, ± verrucose, short and/or crisped hairs sparse to abundant, cottony hairs absent, glands sparse to common;</text>
      <biological_entity id="o14860" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14861" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s3" to="abundant" />
        <character char_type="range_value" from="loosely appressed" name="orientation" src="d0_s3" to="ascending-spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="more or less stiff" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s3" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o14862" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="abundant" />
      </biological_entity>
      <biological_entity id="o14863" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14864" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3–5, proximalmost separated by 0 (–2) mm, central oblong to obovate, 1.4–2.5 (–5) × 0.6–1.3 (–2) cm, petiolules 1–3 (–5) mm, distal 2/3–3/4 of margin incised 1/2–3/4+ to midvein, teeth 3–5 per side, 3.5–6 mm, apical tufts 0.5–1 mm, abaxial surfaces grayish white to white, long hairs common to abundant, cottony-crisped hairs usually dense, short hairs and glands absent or obscured, adaxial green to grayish green, long hairs sparse to common, 0.5–1 (–1.5) mm, ± stiff, short and/or crisped hairs sparse to common, sometimes abundant, cottony hairs absent, glands sparse to abundant.</text>
      <biological_entity id="o14865" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o14866" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character constraint="by central leaflets" constraintid="o14867" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" notes="" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" notes="" src="d0_s4" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o14867" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="true" name="some_measurement" src="d0_s4" unit="mm" value="0" value_original="0" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="obovate" />
      </biological_entity>
      <biological_entity id="o14868" name="petiolule" name_original="petiolules" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="of margin" constraintid="o14869" from="2/3" name="quantity" src="d0_s4" to="3/4" />
        <character char_type="range_value" constraint="to midvein" constraintid="o14870" from="1/2" name="quantity" src="d0_s4" to="3/4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14869" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o14870" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o14871" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o14872" from="3" name="quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14872" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="apical" id="o14873" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14874" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="grayish white" name="coloration" src="d0_s4" to="white" />
      </biological_entity>
      <biological_entity id="o14875" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
      <biological_entity id="o14877" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="true" modifier="usually" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o14878" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="true" modifier="usually" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14879" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="grayish green" />
      </biological_entity>
      <biological_entity id="o14880" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o14881" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o14882" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14883" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 1–3.</text>
      <biological_entity constraint="cauline" id="o14884" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 7–12 (–20) -flowered, ± open, branch angle 5–30 (–50) °.</text>
      <biological_entity id="o14885" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="7-12(-20)-flowered" value_original="7-12(-20)-flowered" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="branch" id="o14886" name="angle" name_original="angle" src="d0_s6" type="structure">
        <character name="degree" src="d0_s6" value="5-30(-50)°" value_original="5-30(-50)°" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels (0.5–) 1–2 cm, proximal to 3 cm.</text>
      <biological_entity id="o14887" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14888" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: epicalyx bractlets linear to narrowly lanceolate, 2–3.5 × 0.6–1 mm;</text>
      <biological_entity id="o14889" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="epicalyx" id="o14890" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 2.5–3.5 mm diam.;</text>
      <biological_entity id="o14891" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14892" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 2.5–4 mm, apex subacute to acute, glands ± common, usually not obscured;</text>
      <biological_entity id="o14893" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14894" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14895" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
      <biological_entity id="o14896" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s10" value="common" value_original="common" />
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pale-yellow, not overlapping, 3–5 (–6) × 3–4 (–5) mm, slightly longer than sepals;</text>
      <biological_entity id="o14897" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14898" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s11" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character constraint="than sepals" constraintid="o14899" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o14899" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 0.8–1.8 mm, anthers 0.4 mm;</text>
      <biological_entity id="o14900" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14901" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14902" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>carpels 30–60, styles 0.7–0.9 mm.</text>
      <biological_entity id="o14903" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14904" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
      <biological_entity id="o14905" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 1.2 mm.</text>
      <biological_entity id="o14906" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy bluffs, dry riverbanks, limestone outcrops, grassy openings in dry forests, dry mountain slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy bluffs" />
        <character name="habitat" value="dry riverbanks" />
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="grassy openings" constraint="in dry forests" />
        <character name="habitat" value="dry forests" />
        <character name="habitat" value="dry mountain slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla furcata is a characteristic species of the steppe bluffs of interior and south-central Alaska, Yukon, and northern British Columbia, mainly within the unglaciated Beringian region. It differs from P. rubricaulis in having less open inflorescences, smaller flowers, narrower petals, more papillae on styles, and denser glands on epicalyx and calyx.</discussion>
  
</bio:treatment>