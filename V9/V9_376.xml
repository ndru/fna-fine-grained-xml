<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens" date="1959" rank="section">unguiculatae</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">unguiculata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 339. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section unguiculatae;species unguiculata;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100281</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(A. Gray) Hooker f." date="unknown" rank="species">unguiculata</taxon_name>
    <taxon_hierarchy>genus Potentilla;species unguiculata</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Yosemite ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± grayish, often purple-tinged;</text>
      <biological_entity id="o25865" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="purple-tinged" value_original="purple-tinged" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>glands sparse to abundant.</text>
      <biological_entity id="o25866" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s1" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending, 1–3.5 dm.</text>
      <biological_entity id="o25867" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="3.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (4–) 7–15 cm;</text>
      <biological_entity constraint="basal" id="o25868" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheathing base ± glabrous abaxially;</text>
      <biological_entity id="o25869" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="more or less; abaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules linear, 3–6 mm;</text>
      <biological_entity id="o25870" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole (0.3–) 0.5–4 (–5) cm, hairs sparse to abundant, ascending to spreading, 1–2 mm;</text>
      <biological_entity id="o25871" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25872" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s6" to="abundant" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 15–20 (–25) per side, loosely overlapping, 3–6 mm, lobes 3–8, linear to oblanceolate, hairs sparse to abundant, spreading, 1–2 mm.</text>
      <biological_entity id="o25873" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="25" />
        <character char_type="range_value" constraint="per side" constraintid="o25874" from="15" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="false" modifier="loosely" name="arrangement" notes="" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25874" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o25875" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="8" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o25876" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s7" to="abundant" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 3–6.</text>
      <biological_entity constraint="cauline" id="o25877" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (15–) 30–100 (–200) -flowered, (1–) 1.5–4 (–8) cm diam., flowers mostly arranged in 1–several loose to tight glomerules of 5–10 flowers.</text>
      <biological_entity id="o25878" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(15-)30-100(-200)-flowered" value_original="(15-)30-100(-200)-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s9" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s9" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25879" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character constraint="in glomerules" constraintid="o25880" is_modifier="false" modifier="mostly" name="arrangement" src="d0_s9" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o25880" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="several" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s9" value="loose" value_original="loose" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s9" value="tight" value_original="tight" />
      </biological_entity>
      <biological_entity id="o25881" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <relation from="o25880" id="r1691" name="part_of" negation="false" src="d0_s9" to="o25881" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1–3 mm.</text>
      <biological_entity id="o25882" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 6–9 mm diam.;</text>
      <biological_entity id="o25883" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets linear or narrowly lanceolate to elliptic or narrowly oblong, 1.2–2 (–3) mm;</text>
      <biological_entity constraint="epicalyx" id="o25884" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s12" to="elliptic or narrowly oblong" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium shallowly turbinate, 1.5–2.5 × 2–3 (–3.5) mm, often nearly as deep as wide;</text>
      <biological_entity id="o25885" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="often nearly; nearly" name="length" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" modifier="often nearly; nearly" name="atypical_width" src="d0_s13" to="3.5" to_unit="mm" />
        <character char_type="range_value" constraint="as-deep-as wide" from="2" from_unit="mm" modifier="often nearly; nearly" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals heavily purple-mottled, (1.5–) 2–3 (–3.5) mm, acute;</text>
      <biological_entity id="o25886" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="heavily" name="coloration" src="d0_s14" value="purple-mottled" value_original="purple-mottled" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, often tinged with pink, oblanceolate to spatulate or obovate, 3–4 mm;</text>
      <biological_entity id="o25887" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s15" value="tinged with pink" value_original="tinged with pink" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="spatulate or obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 10–15, filaments filiform, 0.6–1.1 mm, anthers maroon, 0.3–0.5 mm;</text>
      <biological_entity id="o25888" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s16" to="15" />
      </biological_entity>
      <biological_entity id="o25889" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25890" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels (1–) 3–9, styles 1.4–2 mm.</text>
      <biological_entity id="o25891" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="9" />
      </biological_entity>
      <biological_entity id="o25892" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 1.2–1.5 mm.</text>
      <biological_entity id="o25893" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows and slopes, in montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" constraint="in montane conifer woodlands" />
        <character name="habitat" value="slopes" constraint="in montane conifer woodlands" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia unguiculata is found in mid-elevation meadows of the central Sierra Nevada, mostly north of the Kings River. The distinctive deep red to purplish coloration of the inflorescence, and the plant in general, can make patches of this species conspicuous as a smoky purplish haze in meadows. The shape and color of the inflorescences are similar to those of the sympatric Horkelia fusca var. parviflora, suggesting shared pollinators.</discussion>
  <discussion>The description and illustration by J. D. Hooker (1881), supposedly of Potentilla (Ivesia) unguiculata, were based actually on material grown from seed of I. sericoleuca due to initial confusion of the two species (W. H. Brewer et al. 1876–1880, vol. 1).</discussion>
  <discussion>The type (Kellogg s.n., CAS) of Potentilla ciliata Greene (not Rafinesque) is unquestionably this species; however, the purported locality (Owens Valley, Inyo County) is dubious and most likely an error in the labeling of the specimen by the collector.</discussion>
  
</bio:treatment>