<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">aphanes</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="species">australis</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 380. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus aphanes;species australis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100034</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphanes</taxon_name>
    <taxon_name authority="W. Lippert" date="unknown" rank="species">inexspectata</taxon_name>
    <taxon_hierarchy>genus Aphanes;species inexspectata</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, appressed-hairy throughout, hairs usually less than 0.8 mm.</text>
      <biological_entity id="o17455" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="appressed-hairy" value_original="appressed-hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17456" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, simple or branched, (1–) 3–10 (–22) cm.</text>
      <biological_entity id="o17457" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="22" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 5–10 mm;</text>
      <biological_entity id="o17458" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules overlapping, (2.5–) 4–6 (–7) mm, divided ca. 1/2 their length, lobes (5–) 7–10 (–12), on distal nodes oblong, 2–4 (–5) times as long as wide;</text>
      <biological_entity id="o17459" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character name="length" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o17460" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s3" to="7" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="12" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="l_w_ratio" notes="" src="d0_s3" value="2-4(-5)" value_original="2-4(-5)" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17461" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
      </biological_entity>
      <relation from="o17460" id="r1124" name="on" negation="false" src="d0_s3" to="o17461" />
    </statement>
    <statement id="d0_s4">
      <text>petiole free from stipules in proximal leaves, adnate in distal ones;</text>
      <biological_entity id="o17462" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="from stipules" constraintid="o17463" is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character constraint="in distal leaves" constraintid="o17465" is_modifier="false" name="fusion" notes="" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o17463" name="stipule" name_original="stipules" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o17464" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o17465" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <relation from="o17463" id="r1125" name="in" negation="false" src="d0_s4" to="o17464" />
    </statement>
    <statement id="d0_s5">
      <text>blade 5 mm, prominently divided into 3 segments, each (1–) 2–3 (–4) -lobed.</text>
      <biological_entity id="o17466" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="5" value_original="5" />
        <character constraint="into segments" constraintid="o17467" is_modifier="false" modifier="prominently" name="shape" src="d0_s5" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="(1-)2-3(-4)-lobed" value_original="(1-)2-3(-4)-lobed" />
      </biological_entity>
      <biological_entity id="o17467" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences opposing leaves, dense, flowers tending to remain hidden.</text>
      <biological_entity id="o17468" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o17469" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o17470" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o17468" id="r1126" name="opposing" negation="false" src="d0_s6" to="o17469" />
      <relation from="o17468" id="r1127" name="opposing" negation="false" src="d0_s6" to="o17470" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels less than 1 mm.</text>
      <biological_entity id="o17471" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (0.8–) 1.2–1.3 (–1.4) × 0.7–0.8 mm, 1–1.5 mm in fruit;</text>
      <biological_entity id="o17472" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_area" src="d0_s8" to="1.2-1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2-1.3" from_inclusive="false" from_unit="mm" name="atypical_area" src="d0_s8" to="1.4" to_unit="mm" />
        <character name="area" src="d0_s8" unit="mm" value="1.2-1.3" value_original="1.2-1.3" />
        <character char_type="range_value" constraint="in fruit" constraintid="o17473" from="1" from_unit="mm" name="area" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17473" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlets 0–0.1 mm;</text>
      <biological_entity constraint="epicalyx" id="o17474" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium subglobose to ellipsoid, not contracted at apex, not distinctly 8-ribbed, hairy between ribs 2/3–3/4 of length, sometimes glabrescent;</text>
      <biological_entity id="o17475" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s10" to="ellipsoid" />
        <character constraint="at apex" constraintid="o17476" is_modifier="false" modifier="not" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="not distinctly" name="architecture_or_shape" notes="" src="d0_s10" value="8-ribbed" value_original="8-ribbed" />
        <character constraint="between ribs" constraintid="o17477" is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o17476" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o17477" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character char_type="range_value" from="2/3" name="length" src="d0_s10" to="3/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals connivent, 0.2 mm, long-ciliate.</text>
      <biological_entity id="o17478" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits (0.9–) 1.4–1.6 (–1.7) mm.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 16.</text>
      <biological_entity id="o17479" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17480" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, sandy places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="sandy places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Ala., Ark., Del., D.C., Fla., Ga., Ky., La., Md., Miss., Mo., N.Y., N.C., Okla., Oreg., S.C., Tenn., Tex., Va., Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Aphanes australis have commonly been misidentified as A. microcarpa (Boisser &amp; Reuter) Rothmaler (or Alchemilla microcarpa Boisser &amp; Reuter), an endemic to the western Mediterranean region. Plants native to other parts of Europe, but misidentified as A. microcarpa were described in 1984 as a new species, A. inexspectata, but this taxon had previously been described by Rydberg as A. australis from plants introduced to the eastern United States (P. Frost-Olsen 1998). Aphanes microcarpa is not present in North America.</discussion>
  
</bio:treatment>