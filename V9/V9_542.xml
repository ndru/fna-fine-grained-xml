<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">331</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="illustration_page">330</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dryas</taxon_name>
    <taxon_name authority="Vahl" date="1798" rank="species">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Skr. Naturhist.-Selsk.</publication_title>
      <place_in_publication>4(2): 171. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus dryas;species integrifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100205</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Entire-leaved mountain-avens</other_name>
  <other_name type="common_name">dryade a feuilles entières</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–14 cm.</text>
      <biological_entity id="o11682" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="14" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear to narrowly ovate or oblong to oval, 2–38 × 0.5–11 mm, base cordate to truncate or cuneate to rounded, margins usually entire or dentate to crenate, sometimes serrate in proximal 1/2, sinuses 5–15 (–20) % to midvein, apex usually obtuse or acute, sometimes acuminate, surfaces usually smooth, sometimes rugulose, abaxial slightly hairy to tomentose, adaxial glabrous or sparsely hairy proximally on midvein, feathery hairs and stipitate-glands absent.</text>
      <biological_entity id="o11683" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="narrowly ovate or oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s1" to="38" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11684" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s1" to="truncate or cuneate" />
      </biological_entity>
      <biological_entity id="o11685" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s1" value="dentate to crenate" value_original="dentate to crenate" />
        <character constraint="in proximal 1/2" constraintid="o11686" is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11686" name="1/2" name_original="1/2" src="d0_s1" type="structure" />
      <biological_entity id="o11687" name="sinuse" name_original="sinuses" src="d0_s1" type="structure" />
      <biological_entity id="o11688" name="midvein" name_original="midvein" src="d0_s1" type="structure" />
      <biological_entity id="o11689" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o11690" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s1" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11691" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character char_type="range_value" from="slightly hairy" name="pubescence" src="d0_s1" to="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11692" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="on midvein" constraintid="o11693" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11693" name="midvein" name_original="midvein" src="d0_s1" type="structure" />
      <biological_entity id="o11694" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11695" name="stipitate-gland" name_original="stipitate-glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o11687" id="r731" modifier="5-15(-20)%" name="to" negation="false" src="d0_s1" to="o11688" />
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 10–150 mm.</text>
      <biological_entity id="o11696" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="150" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers erect at flowering;</text>
      <biological_entity id="o11697" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals linear-lanceolate, 3.5–9 × 1.5–2.5 mm;</text>
      <biological_entity id="o11698" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 8 (–9), spreading, usually white or cream, sometimes yellow, 9–14 × 5–11 mm;</text>
      <biological_entity id="o11699" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="9" />
        <character name="quantity" src="d0_s5" value="8" value_original="8" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments glabrous.</text>
      <biological_entity id="o11700" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Achenes 0.8–3.5 mm;</text>
      <biological_entity id="o11701" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles 8–29 mm.</text>
      <biological_entity id="o11702" name="style" name_original="styles" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Ont., Que., Yukon; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Hybrid swarms between the two subspecies of Dryas integrifolia are common (E. Hultén 1968).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades linear to narrowly ovate, margins usually strongly revolute, bases cordate to truncate, apices acute.</description>
      <determination>7a Dryas integrifolia subsp. integrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades oblong to oval, margins usually flat, sometimes narrowly revolute, bases cuneate to rounded, apices obtuse.</description>
      <determination>7b Dryas integrifolia subsp. sylvatica</determination>
    </key_statement>
  </key>
</bio:treatment>