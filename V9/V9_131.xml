<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="de Candolle ex Seringe" date="1818" rank="section">Caninae</taxon_name>
    <place_of_publication>
      <publication_title>Mus. Helv. Bot.</publication_title>
      <place_in_publication>1: 3. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section Caninae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">318045</other_info_on_name>
  </taxon_identification>
  <number>7b.5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, compact, usually forming thickets;</text>
      <biological_entity id="o22148" name="thicket" name_original="thickets" src="d0_s0" type="structure" />
      <relation from="o22147" id="r1438" modifier="usually" name="forming" negation="false" src="d0_s0" to="o22148" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes absent or relatively short.</text>
      <biological_entity id="o22147" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o22149" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, arching, or spreading, 7–30 (–50) dm;</text>
      <biological_entity id="o22150" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="50" to_unit="dm" />
        <character char_type="range_value" from="7" from_unit="dm" name="some_measurement" src="d0_s2" to="30" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches glabrous, eglandular;</text>
    </statement>
    <statement id="d0_s4">
      <text>infrastipular prickles paired or single, curved or appressed, rarely erect, flattened, stout, internodal prickles rare, smaller, or aciculi, sometimes absent.</text>
      <biological_entity constraint="distal" id="o22151" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o22152" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o22153" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="rare" value_original="rare" />
        <character is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, rarely persistent, 4–11 (–18) cm, membranous to leathery;</text>
      <biological_entity id="o22154" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s5" to="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules persistent, adnate to petiole, auricles flared, margins entire, rarely slightly crenate, sometimes densely glandular-ciliate, stipitate-glandular or eglandular;</text>
      <biological_entity id="o22155" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o22156" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o22156" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o22157" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flared" value_original="flared" />
      </biological_entity>
      <biological_entity id="o22158" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="sometimes densely" name="pubescence" src="d0_s6" value="glandular-ciliate" value_original="glandular-ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 5–7 (–9), terminal: petiolule 5–17 (–40) mm, blade ovate, obovate, orbiculate, or elliptic, sometimes oval or ovatelanceolate, 10–45 (–65) × 8–30 (–50) mm, abaxial surfaces pubescent or tomentose (sometimes on midveins) or glabrous, eglandular or glandular, with or without resinous glands, adaxial pale, light, or dark green, sometimes glaucous, lustrous to dull, tomentulose or pubescent, sometimes glabrous.</text>
      <biological_entity id="o22159" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o22160" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22161" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="65" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="45" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22162" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o22163" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="true" name="coating" src="d0_s7" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22164" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="lustrous" name="reflectance" src="d0_s7" to="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o22162" id="r1439" name="with or without" negation="false" src="d0_s7" to="o22163" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences panicles, sometimes corymbs, 1–4 (–7) -flowered.</text>
      <biological_entity constraint="inflorescences" id="o22165" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-4(-7)-flowered" value_original="1-4(-7)-flowered" />
      </biological_entity>
      <biological_entity id="o22166" name="corymb" name_original="corymbs" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect or reflexed, stout, (5–) 11–20 (–35) mm, glabrous, eglandular or stipitate-glandular;</text>
      <biological_entity id="o22167" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts persistent, 1 or 2, margins glandular-serrate or stipitate or ciliate-glandular.</text>
      <biological_entity id="o22168" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o22169" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="glandular-serrate" value_original="glandular-serrate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ciliate-glandular" value_original="ciliate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 2.5–5 cm diam.;</text>
      <biological_entity id="o22170" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s11" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium globose, ovoid, obovoid, oblong, or urceolate, glabrous, usually eglandular, base sometimes stipitate or setose-glandular;</text>
      <biological_entity id="o22171" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o22172" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="setose-glandular" value_original="setose-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals persistent or deciduous, appressed-reflexed, spreading, or erect, lanceolate or ovatelanceolate, 10–25 × 2–5 mm, margins (outer) often deeply pinnatifid, abaxial surfaces glabrous, eglandular, densely glandular, or stipitate-glandular;</text>
      <biological_entity id="o22173" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="appressed-reflexed" value_original="appressed-reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22174" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="often deeply" name="shape" src="d0_s13" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22175" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, pink or rose, rarely white;</text>
      <biological_entity id="o22176" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="rose" value_original="rose" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 25–65, styles free, lanate or villous, rarely glabrous, stylar orifice 1–2.5 (–3.5) mm diam., hypanthial disc flat or conic, 2–5 mm diam.</text>
      <biological_entity id="o22177" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s15" to="65" />
      </biological_entity>
      <biological_entity id="o22178" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="stylar" id="o22179" name="orifice" name_original="orifice" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="hypanthial" id="o22180" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Hips red to purplish or orange, globose, depressed-globose, obovoid, ovoid, oblong, urceolate, ellipsoid, or pyriform, 10–25 × 6–22 mm, glabrous, sometimes setose, eglandular or stipitate-glandular;</text>
      <biological_entity id="o22181" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s16" to="purplish or orange" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="depressed-globose" value_original="depressed-globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s16" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s16" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s16" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals deciduous or persistent, spreading, reflexed, or erect.</text>
      <biological_entity id="o22182" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basiparietal.</text>
      <biological_entity id="o22183" name="achene" name_original="achenes" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, n Africa; introduced also in Mexico, Central America, South America, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 20 (5 in the flora).</discussion>
  <discussion>All species of sect. Caninae are polyploid, mostly pentaploid (2n = 35), but also triploid (2n = 21) and tetraploid (2n = 28) or all three polyploid levels for a single species, with a hemisexual, matroclinal (maternal) reproduction unlike that known for any other flowering plant (H. Nybom et al. 2000). In addition, unknown elsewhere in Rosa, apomixis appears to occur to a limited extent among Caninae species, which complicates further their reproductive anomaly (G. Werlemark 2000). Even though over 350 taxa have been described in the section, the relatively few species now recognized are highly complex groups of microspecies, and the authors agree with the editors of W. J. Bean (1970–1988, vol. 4) that no attempt should be made in modern floristic works to take account of every character-state combination. Further, those editors believed that the species of Caninae are cryptohybrids, that is, they are all products of ancient hybridizations between species that no longer exist, at least not in their primitive states.</discussion>
  <discussion>In addition to the species treated here, Rosa obtusifolia Desvaux is marginally naturalized in California, New Mexico, and Virginia; it differs from R. canina in having hairy leaflets that are usually glandular abaxially.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals abaxially eglandular; prickle lengths ± uniform; leaflets abaxially glabrous, rarely pubescent or tomentose on midveins, eglandular; bracts 6–18 × 4–5 mm, surfaces glabrous.</description>
      <determination>9 Rosa canina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals abaxially densely glandular or stipitate-glandular; prickle lengths varying or ± uniform; leaflets abaxially usually tomentose, rarely glabrous or pubescent, usually glandular; bracts 13–21 × 4–7 mm, surfaces usually tomentose, rarely glabrous or pubescent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets abaxially usually densely viscid-glandular, glabrous or pubescent, glands with ripe apple scent; prickles curved or falcate.</description>
      <determination>10 Rosa rubiginosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets abaxially usually resinous-glandular, tomentose, glands resin-scented; prickles all erect or some curved</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hips: sepals spreading or reflexed, deciduous after anthesis; stylar orifices 1 mm diam.</description>
      <determination>11 Rosa tomentosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hips: sepals usually erect, persistent after anthesis; stylar orifices 2.5–3.5 mm diam</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stipules 10–14 × 3–5 mm, auricles 3–4 mm; bracts 16–21 mm; sepals lanceolate, 15–20 × 3–3.5 mm; stylar orifices 2.5 mm diam.</description>
      <determination>12 Rosa sherardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stipules 15–20 × 5–10 mm, auricles 5–8 mm; bracts 10–12 mm; sepals ovate-lanceolate, 20–25 × (4–)5 mm; stylar orifices 3.5 mm diam.</description>
      <determination>13 Rosa mollis</determination>
    </key_statement>
  </key>
</bio:treatment>