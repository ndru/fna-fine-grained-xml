<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="Bois" date="unknown" rank="species">adpressus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Bot. France</publication_title>
      <place_in_publication>51(sess. extraord.): cxlix, fig. [p. cl.] 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species adpressus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200010713</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Creeping cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, mound-forming, to 0.3 m.</text>
      <biological_entity id="o20752" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mound-forming" value_original="mound-forming" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="0.3" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems divaricate, prostrate to procumbent, rooting, to 0.8 m long, stiff;</text>
      <biological_entity id="o20753" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="prostrate" name="growth_form" src="d0_s1" to="procumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="0" from_unit="m" name="length" src="d0_s1" to="0.8" to_unit="m" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distichous, maroon, initially strigose-villous.</text>
      <biological_entity id="o20754" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="maroon" value_original="maroon" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s2" value="strigose-villous" value_original="strigose-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o20755" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–2 mm, glabrescent;</text>
      <biological_entity id="o20756" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic or ovate, rarely suborbiculate, 8–15 × 5–12 mm, chartaceous, base rounded, margins slightly undulate, not revolute, veins 2 or 3, superficial, apex acute or obtuse, abaxial surfaces green, glabrate, midrib strigose, adaxial green, dull to slightly shiny, not glaucous, flat between lateral-veins, glabrous.</text>
      <biological_entity id="o20757" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o20758" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20759" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o20760" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o20761" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20762" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o20763" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20764" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character char_type="range_value" from="dull" name="reflectance" src="d0_s5" to="slightly shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character constraint="between lateral-veins" constraintid="o20765" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20765" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences on fertile shoots 6–10 mm with 2 or 3 leaves, 1 (or 2) -flowered, subsessile.</text>
      <biological_entity id="o20766" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o20767" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with 2 or 3leaves" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <relation from="o20766" id="r1356" name="on" negation="false" src="d0_s6" to="o20767" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0–1.5 mm, glabrate.</text>
      <biological_entity id="o20768" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 4–7 mm;</text>
      <biological_entity id="o20769" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium funnelform, maroon, glabrate;</text>
      <biological_entity id="o20770" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals sometimes recurved, margins villous, apex lingulate or acute, surfaces glabrous;</text>
      <biological_entity id="o20771" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o20772" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o20773" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lingulate" value_original="lingulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o20774" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals erect, dark red;</text>
      <biological_entity id="o20775" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark red" value_original="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10 (–13), filaments pink to red, anthers white;</text>
      <biological_entity id="o20776" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="13" />
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o20777" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s12" to="red" />
      </biological_entity>
      <biological_entity id="o20778" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 2 (or 3).</text>
      <biological_entity id="o20779" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pomes bright red, broadly obovoid to globose, 6–7 mm, shiny, not glaucous, succulent, glabrous;</text>
      <biological_entity id="o20780" name="pome" name_original="pomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright red" value_original="bright red" />
        <character char_type="range_value" from="broadly obovoid" name="shape" src="d0_s14" to="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s14" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="texture" src="d0_s14" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals suberect, glabrous;</text>
    </statement>
    <statement id="d0_s16">
      <text>navel open;</text>
      <biological_entity id="o20781" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>style remnants 4/5 from base.</text>
      <biological_entity constraint="style" id="o20782" name="remnant" name_original="remnants" src="d0_s17" type="structure">
        <character constraint="from base" constraintid="o20783" name="quantity" src="d0_s17" value="4/5" value_original="4/5" />
      </biological_entity>
      <biological_entity id="o20783" name="base" name_original="base" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Pyrenes 2 (or 3).</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 34 (Sweden).</text>
      <biological_entity id="o20784" name="pyrene" name_original="pyrenes" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20785" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed ground, gravel pits</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed ground" />
        <character name="habitat" value="gravel pits" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mich.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">adpressa</other_name>
  
</bio:treatment>