<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="illustration_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">gymnocarpa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 461. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species gymnocarpa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100416</other_info_on_name>
  </taxon_identification>
  <number>30.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs, usually loosely clustered.</text>
      <biological_entity id="o20425" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually loosely" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" modifier="usually loosely" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sometimes spreading, slender, (1–) 3–15 (–25) dm, sparsely or densely branched;</text>
      <biological_entity id="o20427" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="15" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely; densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark sometimes glaucous, reddish-brown with age, glabrous;</text>
      <biological_entity id="o20429" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>infrastipular prickles 0–2, erect, subulate, 2–8 (–10) × 1.5 mm, terete, internodal prickles similar or smaller, sparse to dense, sometimes absent on distal stems, mixed with aciculi, base terete, eglandular.</text>
      <biological_entity id="o20428" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character constraint="with age" constraintid="o20429" is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20430" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o20431" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="dense" />
        <character constraint="on distal stems" constraintid="o20432" is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character constraint="with aciculi" constraintid="o20433" is_modifier="false" name="arrangement" notes="" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20432" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o20433" name="aciculus" name_original="aciculi" src="d0_s3" type="structure" />
      <biological_entity id="o20434" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves (2–) 4–10 (–17) cm;</text>
      <biological_entity id="o20435" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules 5–15 × 2–5 mm, auricles flared, 2–3 mm, margins entire, stipitate-glandular, surfaces glabrous, eglandular;</text>
      <biological_entity id="o20436" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20437" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flared" value_original="flared" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20438" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20439" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and rachis with pricklets, glabrous, rarely finely puberulent, sparsely stipitate-glandular;</text>
      <biological_entity id="o20440" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="with pricklets" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely finely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20441" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="with pricklets" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely finely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 5–9 (–11), terminal: petiolule (2–) 5–12 (–20) mm, blade elliptic to obovate or ovate to nearly orbiculate, (4–) 10–40 (–60) × (4–) 10–20 (–40) mm, membranous to ± leathery, margins 2+-serrate, teeth 7–13 per side, obtuse to acute, gland-tipped, apex obtuse, sometimes nearly acute, rounded, or truncate, abaxial surfaces pale green, glabrous, eglandular, adaxial green, dull, glabrous.</text>
      <biological_entity id="o20442" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="11" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o20443" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20444" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="obovate or ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s7" to="more or less leathery" />
      </biological_entity>
      <biological_entity id="o20445" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="2+-serrate" value_original="2+-serrate" />
      </biological_entity>
      <biological_entity id="o20446" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o20447" from="7" name="quantity" src="d0_s7" to="13" />
        <character char_type="range_value" from="obtuse" name="shape" notes="" src="d0_s7" to="acute" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o20447" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o20448" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes nearly" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20449" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20450" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences corymbs, usually 1–3-flowered, rarely in multiflowered candelabras.</text>
      <biological_entity constraint="inflorescences" id="o20451" name="corymb" name_original="corymbs" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
      <biological_entity id="o20452" name="candelabra" name_original="candelabras" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="multiflowered" value_original="multiflowered" />
      </biological_entity>
      <relation from="o20451" id="r1338" modifier="rarely" name="in" negation="false" src="d0_s8" to="o20452" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, ± curved as hips mature, slender, 10–25 (–35) mm, glabrous, stipitate-glandular, rarely eglandular (except var. serpentina);</text>
      <biological_entity id="o20453" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="as hips" constraintid="o20454" is_modifier="false" modifier="more or less" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o20454" name="hip" name_original="hips" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts some early caducous, 1 or 2, attached near pedicel bases, ovate or lanceolate, 4–12 × 2–8 mm, margins entire, short-stipitate-glandular, apex acute or rounded, surfaces glabrous, eglandular.</text>
      <biological_entity id="o20455" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or" value="2" value_original="2" />
        <character constraint="near pedicel bases" constraintid="o20456" is_modifier="false" name="fixation" src="d0_s10" value="attached" value_original="attached" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="pedicel" id="o20456" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o20457" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o20458" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20459" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1.5–3 cm diam.;</text>
      <biological_entity id="o20460" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s11" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium narrowly ovoid-urceolate, 2–4 × 1.5–2 mm, glabrous, eglandular, neck 0–1 × 1.5 mm;</text>
      <biological_entity id="o20461" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ovoid-urceolate" value_original="ovoid-urceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o20462" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s12" to="1" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals ascending to reflexed, lanceolate, 5–10 × 2–3 mm, tip 0.1–5 × 1 mm, margins entire, abaxial surfaces glabrous, eglandular, sometimes stipitate-glandular;</text>
      <biological_entity id="o20463" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s13" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20464" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o20465" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20466" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, deep pink, 8–15 × 6–13 mm;</text>
      <biological_entity id="o20467" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character is_modifier="false" name="depth" src="d0_s14" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s14" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 57;</text>
      <biological_entity id="o20468" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="57" value_original="57" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>carpels 3–12 (–16), styles exsert 1–1.5 mm beyond stylar orifice (1 mm diam.) of hypanthial disc (2–4 mm diam.).</text>
      <biological_entity id="o20469" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="16" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s16" to="12" />
      </biological_entity>
      <biological_entity id="o20470" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exsert" value_original="exsert" />
        <character char_type="range_value" constraint="beyond stylar, orifice" constraintid="o20471, o20472" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20471" name="stylar" name_original="stylar" src="d0_s16" type="structure" />
      <biological_entity id="o20472" name="orifice" name_original="orifice" src="d0_s16" type="structure" />
      <biological_entity id="o20473" name="hypanthial" name_original="hypanthial" src="d0_s16" type="structure" />
      <biological_entity id="o20474" name="disc" name_original="disc" src="d0_s16" type="structure" />
      <relation from="o20471" id="r1339" name="part_of" negation="false" src="d0_s16" to="o20473" />
      <relation from="o20471" id="r1340" name="part_of" negation="false" src="d0_s16" to="o20474" />
      <relation from="o20472" id="r1341" name="part_of" negation="false" src="d0_s16" to="o20473" />
      <relation from="o20472" id="r1342" name="part_of" negation="false" src="d0_s16" to="o20474" />
    </statement>
    <statement id="d0_s17">
      <text>Hips scarlet, irregularly ellipsoid or ellipsoid to nearly globose, 7–15 × 5–13 mm, fleshy, glabrous, eglandular, neck 0–2 × 1.5–2.5 mm;</text>
      <biological_entity id="o20475" name="hip" name_original="hips" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="scarlet" value_original="scarlet" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s17" to="nearly globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s17" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s17" to="13" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o20476" name="neck" name_original="neck" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s17" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals, styles, and distal receptacle collectively deciduous at fruit maturity along well-defined, circumscissile line, erect to reflexed.</text>
      <biological_entity id="o20477" name="sepal" name_original="sepals" src="d0_s18" type="structure" />
      <biological_entity id="o20478" name="style" name_original="styles" src="d0_s18" type="structure" />
      <biological_entity constraint="distal" id="o20479" name="receptacle" name_original="receptacle" src="d0_s18" type="structure">
        <character constraint="at fruit" constraintid="o20480" is_modifier="false" modifier="collectively" name="duration" src="d0_s18" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s18" to="reflexed" />
      </biological_entity>
      <biological_entity id="o20480" name="fruit" name_original="fruit" src="d0_s18" type="structure">
        <character constraint="along line" constraintid="o20481" is_modifier="false" name="life_cycle" src="d0_s18" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o20481" name="line" name_original="line" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="well-defined" value_original="well-defined" />
        <character is_modifier="true" name="dehiscence" src="d0_s18" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Achenes basiparietal, (1–) 4–10 (–12), cream to pale-brown, (3–) 4.5–7 × 2–4 mm.</text>
      <biological_entity id="o20482" name="achene" name_original="achenes" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s19" to="4" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="12" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s19" to="10" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s19" to="pale-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s19" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s19" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s19" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Rosa gymnocarpa is most easily distinguished in mature fruit, in that the sepals collectively and cleanly separate from the hip together with styles and, sometimes, associated hypanthia. Other diagnostic features include solitary to few, small flowers, stipitate-glandular pedicels, hypanthia extremely small in bud, and glabrous leaflets doubly glandular-toothed. The species is also one of the relatively few roses that flourishes in partial shade. Rosa gymnocarpa occurs in forested areas from British Columbia and Montana to central California, with disjunct populations in southern California; it skirts the Great Basin, with the possible exception of the type of R. leucopsis Greene, a likely synonym, purportedly from central Oregon’s sagebrush steppe, which is otherwise devoid of the species (A. Cronquist and N. H. Holmgren 1997).</discussion>
  <discussion>The distinctive features of Rosa gymnocarpa have been used as the basis for sect. Gymnocarpae Crépin. Recognition of that section is not supported by the molecular analysis by A. Bruneau et al. (2007), in which R. gymnocarpa occurs in a clade with R. pinetorum and one sample of R. californica, separate from a distinct clade that groups diverse specimens of R. bridgesii, R. spithamea, and some Asian species of sect. Rosa.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets (5–)7–9(–11), terminal blades elliptic to narrowly obovate or ovate, (5–)10–30(–60) mm, apices usually obtuse, sometimes nearly acute or rounded; pedicels (10–)15–25(–35) mm; stems 3–15(–25) dm; light shade (rarely full sun) in open woodlands, forest edges, often near streams or rocky sites; s British Columbia to s California, e to Montana.</description>
      <determination>30a Rosa gymnocarpa var. gymnocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 5(–7), terminal blades broadly elliptic to obovate or ovate to nearly orbiculate, 4–20 mm, apices usually broadly obtuse to rounded, sometimes nearly truncate; pedicels 10–15 mm; stems (1–)3–6(–13) dm; full sun in roadsides, ridges, and other openings in chaparral and stunted forests on ultramafic substrates; Siskiyou Mountains, sw Oregon and nw California.</description>
      <determination>30b Rosa gymnocarpa var. serpentina</determination>
    </key_statement>
  </key>
</bio:treatment>