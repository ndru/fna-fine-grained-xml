<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">324</other_info_on_meta>
    <other_info_on_meta type="illustration_page">320</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus" date="1771" rank="genus">acaena</taxon_name>
    <taxon_name authority="Ruiz &amp; Pavón" date="1798" rank="species">pinnatifida</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Peruv.</publication_title>
      <place_in_publication>1: 68, plate 104, fig. b. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus acaena;species pinnatifida</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250040993</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acaena</taxon_name>
    <taxon_name authority="Bitter" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Acaena;species californica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pinnatifida</taxon_name>
    <taxon_name authority="(Bitter) Jepson" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus A.;species pinnatifida;variety californica</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants from branched woody caudex.</text>
      <biological_entity id="o26749" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26750" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o26749" id="r1734" name="from" negation="false" src="d0_s0" to="o26750" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, becoming suberect, 1.5–5 dm (in fruit), 4 mm diam.;</text>
      <biological_entity id="o26751" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character name="diameter" src="d0_s1" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches erect or leafy.</text>
      <biological_entity id="o26752" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (crowded at bases of stems);</text>
      <biological_entity id="o26753" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o26754" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole to 4 cm;</text>
      <biological_entity id="o26755" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade 3–15 cm, leaflets 6–10 per side, gray-green (at least on dried specimens), ovate, 4–10 mm, pinnatisect, lobes 3–7 (–11), linear, margins revolute, apex penicillate (having a little brush or tuft of hairs at tips), abaxial surface sericeous, adaxial sparsely pilose.</text>
      <biological_entity id="o26756" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26757" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o26758" from="6" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity id="o26758" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o26759" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="11" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o26760" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o26761" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="penicillate" value_original="penicillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26762" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26763" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Scapes terminal, to 50 cm, 1.5–3 mm diam., sparsely pilose, proximal scape and leaf-sheaths with multicellular hairs among 1-celled hairs, with (0–) 2–3 cauline leaves to 10 cm.</text>
      <biological_entity id="o26764" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="50" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o26765" name="scape" name_original="scape" src="d0_s7" type="structure" />
      <biological_entity constraint="proximal" id="o26766" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <biological_entity id="o26767" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o26768" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="1-celled" value_original="1-celled" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o26769" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="atypical_quantity" src="d0_s7" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o26765" id="r1735" name="with" negation="false" src="d0_s7" to="o26767" />
      <relation from="o26766" id="r1736" name="with" negation="false" src="d0_s7" to="o26767" />
      <relation from="o26767" id="r1737" name="among" negation="false" src="d0_s7" to="o26768" />
      <relation from="o26765" id="r1738" name="with" negation="false" src="d0_s7" to="o26769" />
      <relation from="o26766" id="r1739" modifier="to 10 cm" name="with" negation="false" src="d0_s7" to="o26769" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 15–35-flowered, interrupted spikes, (flowers crowded distally, 2–15 cm in flower, elongating to 50 cm in fruit);</text>
      <biological_entity id="o26770" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="15-35-flowered" value_original="15-35-flowered" />
      </biological_entity>
      <biological_entity id="o26771" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracteoles leaflike to linear, ciliate.</text>
      <biological_entity id="o26772" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character char_type="range_value" from="leaflike" name="shape" src="d0_s9" to="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 6 mm diam.;</text>
      <biological_entity id="o26773" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character name="diameter" src="d0_s10" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–5, elliptic, 3 mm, abaxially pilose, adaxially glabrous;</text>
      <biological_entity id="o26774" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens usually 4, filaments 2–3 mm, anthers dark red;</text>
      <biological_entity id="o26775" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o26776" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26777" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark red" value_original="dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma rose, laciniate.</text>
    </statement>
    <statement id="d0_s14">
      <text>Fruiting hypanthia ovoid, 3–4 mm diam., surfaces tomentose, obscurely 4-angled, bearing 10–20 spines in longitudinal ranks, to 3 [–4] mm, spines with retrorse hairs or barbs.</text>
      <biological_entity id="o26778" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose" value_original="rose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o26779" name="hypanthium" name_original="hypanthia" src="d0_s14" type="structure" />
      <biological_entity id="o26780" name="whole-organism" name_original="" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26781" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26782" name="spine" name_original="spines" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
      <biological_entity id="o26783" name="rank" name_original="ranks" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o26784" name="spine" name_original="spines" src="d0_s14" type="structure" />
      <biological_entity id="o26785" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity id="o26786" name="barb" name_original="barbs" src="d0_s14" type="structure" />
      <relation from="o26778" id="r1740" name="fruiting" negation="false" src="d0_s14" to="o26779" />
      <relation from="o26781" id="r1741" name="bearing" negation="false" src="d0_s14" to="o26782" />
      <relation from="o26781" id="r1742" name="in" negation="false" src="d0_s14" to="o26783" />
      <relation from="o26784" id="r1743" name="with" negation="false" src="d0_s14" to="o26785" />
      <relation from="o26784" id="r1744" name="with" negation="false" src="d0_s14" to="o26786" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal grasslands, open rocky slopes, sand dunes, old roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal grasslands" />
        <character name="habitat" value="open rocky slopes" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="old roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; South America (Argentina, Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Herbarium records indicate occurrences of Acaena pinnatifida between Santa Barbara and Sonoma counties.</discussion>
  
</bio:treatment>