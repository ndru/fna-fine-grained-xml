<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Multijugae</taxon_name>
    <taxon_name authority="Greene" date="1887" rank="species">arizonica</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 104. 1887</place_in_publication>
      <other_info_on_pub>not Potentilla pinnatifida C. Presl 1822</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section multijugae;species arizonica;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100304</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">pinnatifida</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>20: 364. 1885,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ivesia;species pinnatifida</taxon_hierarchy>
  </taxon_identification>
  <number>36.</number>
  <other_name type="common_name">Garland Prairie cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rosetted to tufted;</text>
      <biological_entity id="o13346" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="rosetted" name="arrangement" src="d0_s0" to="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots ± fleshy-thickened.</text>
      <biological_entity id="o13347" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending, sometimes prostrate, 0.6–2.2 dm, lengths 1.5–3 times basal leaves.</text>
      <biological_entity id="o13348" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s2" to="2.2" to_unit="dm" />
        <character constraint="leaf" constraintid="o13349" is_modifier="false" name="length" src="d0_s2" value="1.5-3 times basal leaves" value_original="1.5-3 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13349" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves pinnate with distal leaflets ± distinct, 4–10 × 1–2 (–3) cm;</text>
      <biological_entity constraint="basal" id="o13350" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with distal leaflets" constraintid="o13351" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" notes="" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13351" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–3 cm, straight hairs dense, ± appressed, 1–2 mm, stiff, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o13352" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13353" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o13354" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13355" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lateral leaflets 5–7 (–9) per side (sometimes with additional interspersed leaflets), on distal (1/2–) 2/3–3/4 of leaf axis, ± overlapping, largest ones oblanceolate to obovate-oblong, 0.5–1.5 × 0.4–0.8 cm, distal 3/4 to whole margin pinnately incised nearly to midvein, teeth (5–) 7–9, linear-oblanceolate, 2–6 × 0.5–1 mm, apical tufts 1 mm, surfaces grayish green, straight hairs sparse (adaxially) to common, ± appressed, 1–2 mm, stiff, cottony hairs absent, glands absent or obscured.</text>
      <biological_entity constraint="primary lateral" id="o13356" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="9" />
        <character char_type="range_value" constraint="per side" constraintid="o13357" from="5" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="obovate-oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character constraint="to margin" constraintid="o13360" name="quantity" src="d0_s5" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o13357" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o13358" name="(1/2-)2/3-3/4" name_original="(1/2-)2/3-3/4" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o13359" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o13360" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character constraint="to midvein" constraintid="o13361" is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o13361" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o13362" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s5" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="9" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o13363" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o13364" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
      </biological_entity>
      <biological_entity id="o13365" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o13366" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13367" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscured" value_original="obscured" />
      </biological_entity>
      <relation from="o13356" id="r849" name="on" negation="false" src="d0_s5" to="o13358" />
      <relation from="o13358" id="r850" name="part_of" negation="false" src="d0_s5" to="o13359" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (1–) 2–3 (–4).</text>
      <biological_entity constraint="cauline" id="o13368" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–15-flowered, ± compactly cymose, opening in fruit.</text>
      <biological_entity id="o13369" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-15-flowered" value_original="3-15-flowered" />
        <character is_modifier="false" modifier="more or less compactly" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
      </biological_entity>
      <biological_entity id="o13370" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <relation from="o13369" id="r851" name="opening in" negation="false" src="d0_s7" to="o13370" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.7–2 (–2.5) cm, straight in fruit.</text>
      <biological_entity id="o13371" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character constraint="in fruit" constraintid="o13372" is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o13372" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets narrowly elliptic, sometimes doubled, 2–3.5 × 1–1.5 mm;</text>
      <biological_entity id="o13373" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o13374" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s9" value="doubled" value_original="doubled" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 3–4 mm diam.;</text>
      <biological_entity id="o13375" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13376" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–6 mm, apex ± acute;</text>
      <biological_entity id="o13377" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13378" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13379" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 4–5 (–6) × 3–4 (–5.5) mm;</text>
      <biological_entity id="o13380" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13381" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 1–2 mm, anthers 1–1.5 mm, often as long as filaments;</text>
      <biological_entity id="o13382" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o13383" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13384" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13385" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <relation from="o13384" id="r852" modifier="often" name="as long as" negation="false" src="d0_s13" to="o13385" />
    </statement>
    <statement id="d0_s14">
      <text>carpels 8–20, styles 2 mm.</text>
      <biological_entity id="o13386" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o13387" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
      <biological_entity id="o13388" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.8 mm, ± smooth, not carunculate.</text>
      <biological_entity id="o13389" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.8" value_original="1.8" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernally wet clay of rocky basaltic meadows, openings in pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vernally wet clay" constraint="of rocky basaltic meadows , openings in pine woodlands" />
        <character name="habitat" value="rocky basaltic meadows" />
        <character name="habitat" value="openings" constraint="in pine woodlands" />
        <character name="habitat" value="pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla arizonica is known only from the Garland Prairie area in Coconino County. Although commonly included within P. plattensis (for example, N. H. Holmgren 1997b; B. C. Johnston 1980; T. H. Kearney and R. H. Peebles 1951), P. arizonica differs in its longer hairs, more erect habit, more condensed inflorescences, pedicels that remain straight in fruit, and significantly larger anthers.</discussion>
  
</bio:treatment>