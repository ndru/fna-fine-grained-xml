<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Th. Wolf) Juzepczuk in V. L. Komarov et al." date="1941" rank="section">Rectae</taxon_name>
    <place_of_publication>
      <publication_title>in V. L. Komarov et al., Fl. URSS</publication_title>
      <place_in_publication>10: 160. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Rectae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">318036</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Th. Wolf" date="unknown" rank="genus">Rectae</taxon_name>
    <place_of_publication>
      <publication_title>in P. F. A. Ascherson et al., Syn. Mitteleur. Fl.</publication_title>
      <place_in_publication>6(1): 671, 750. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;genus Rectae</taxon_hierarchy>
  </taxon_identification>
  <number>8g.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1-stemmed to ± tufted, not stoloniferous;</text>
      <biological_entity id="o1432" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="1-stemmed" value_original="1-stemmed" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened, often giving rise to ± thickened lateral roots;</text>
      <biological_entity id="o1433" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
      <biological_entity id="o1434" name="rise" name_original="rise" src="d0_s1" type="structure" />
      <biological_entity constraint="lateral" id="o1435" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="more or less" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o1433" id="r93" modifier="often" name="giving" negation="false" src="d0_s1" to="o1434" />
      <relation from="o1433" id="r94" modifier="often" name="to" negation="false" src="d0_s1" to="o1435" />
    </statement>
    <statement id="d0_s2">
      <text>vestiture of well-differentiated long and short hairs, glands common to abundant, not red.</text>
      <biological_entity id="o1436" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o1437" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="variability" src="d0_s2" value="well-differentiated" value_original="well-differentiated" />
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o1438" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="common" name="quantity" src="d0_s2" to="abundant" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <relation from="o1436" id="r95" name="part_of" negation="false" src="d0_s2" to="o1437" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, not flagelliform, not rooting at nodes, from centers of ephemeral basal rosettes, 1.5–7 dm, lengths (2–) 3–5 times basal or proximal cauline leaves.</text>
      <biological_entity id="o1439" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o1440" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="7" to_unit="dm" />
        <character constraint="leaf" constraintid="o1443" is_modifier="false" name="length" src="d0_s3" value="(2-)3-5 times basal or proximal cauline leaves" />
      </biological_entity>
      <biological_entity id="o1440" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o1441" name="center" name_original="centers" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o1442" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1443" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o1439" id="r96" name="from" negation="false" src="d0_s3" to="o1441" />
      <relation from="o1441" id="r97" name="part_of" negation="false" src="d0_s3" to="o1442" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not in ranks;</text>
      <biological_entity id="o1444" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o1445" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1446" name="rank" name_original="ranks" src="d0_s4" type="structure" />
      <relation from="o1445" id="r98" name="in" negation="false" src="d0_s4" to="o1446" />
    </statement>
    <statement id="d0_s5">
      <text>cauline (3–) 5–9;</text>
      <biological_entity id="o1447" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o1448" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves palmate, proximal ones 5–18 cm;</text>
      <biological_entity id="o1449" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o1450" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1451" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs spreading, stiff, glands common to abundant;</text>
      <biological_entity id="o1452" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o1453" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1454" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character char_type="range_value" from="common" name="quantity" src="d0_s7" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets 5–7, at tip of leaf axis, separate, oblanceolate, margins flat or ± revolute, ± whole length ± evenly incised 1/3–1/2 to midvein, teeth 7–17 per side, surfaces ± similar, green, not glaucous, long hairs stiff, cottony and/or crisped hairs absent.</text>
      <biological_entity id="o1455" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o1456" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="7" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s8" value="separate" value_original="separate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o1457" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o1458" name="axis" name_original="axis" src="d0_s8" type="structure" />
      <biological_entity id="o1459" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="more or less" name="character" src="d0_s8" value="length" value_original="length" />
        <character is_modifier="false" modifier="more or less evenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o1460" from="1/3" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity id="o1460" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o1461" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o1462" from="7" name="quantity" src="d0_s8" to="17" />
      </biological_entity>
      <biological_entity id="o1462" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o1463" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o1464" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o1465" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o1456" id="r99" name="at" negation="false" src="d0_s8" to="o1457" />
      <relation from="o1457" id="r100" name="part_of" negation="false" src="d0_s8" to="o1458" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (3–) 7–60+-flowered, cymose, ± open.</text>
      <biological_entity id="o1466" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(3-)7-60+-flowered" value_original="(3-)7-60+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels straight in fruit, 0.5–2 (–3) cm, proximal often much longer than distal.</text>
      <biological_entity id="o1467" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o1468" is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1468" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o1469" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o1470" is_modifier="false" name="length_or_size" src="d0_s10" value="often much longer" value_original="often much longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1470" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o1471" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 5–9 mm diam.;</text>
      <biological_entity id="o1472" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals pale [to bright] yellow, obcordate, (4–) 7–10 (–13) mm, equal to or longer than sepals, apex retuse, rarely rounded;</text>
      <biological_entity id="o1473" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale yellow" value_original="pale yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character constraint="than sepals" constraintid="o1474" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1474" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o1475" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 25–30;</text>
      <biological_entity id="o1476" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s14" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, columnar-tapered, not or slightly papillate-swollen in proximal 1/3, 0.8–1.3 mm.</text>
      <biological_entity id="o1477" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="columnar-tapered" value_original="columnar-tapered" />
        <character constraint="in proximal 1/3" constraintid="o1478" is_modifier="false" modifier="not; slightly" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1478" name="1/3" name_original="1/3" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Achenes strongly rugose.</text>
      <biological_entity id="o1479" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s16" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia, n Africa; introduced also in South America, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 8–14 (1 in the flora).</discussion>
  
</bio:treatment>