<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">566</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Tenuifoliae</taxon_name>
    <taxon_name authority="Ashe" date="1901" rank="species">schuettei</taxon_name>
    <taxon_name authority="(Ashe) Kruschke" date="1965" rank="variety">ferrissii</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Bot. Milwaukee Public Mus.</publication_title>
      <place_in_publication>3: 176. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series tenuifoliae;species schuettei;variety ferrissii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100597</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="species">ferrissii</taxon_name>
    <place_of_publication>
      <publication_title>J. Elisha Mitchell Sci. Soc.</publication_title>
      <place_in_publication>17: 11. 1901 (as ferrissi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;species ferrissii</taxon_hierarchy>
  </taxon_identification>
  <number>61c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades ovate to ovate-oblong, 3–6 cm, base rounded to subtruncate, sinuses deep (max LII 25–40%), proximal lobe most deeply separated, spreading at 60º–80º.</text>
      <biological_entity id="o6659" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s0" to="ovate-oblong" />
        <character char_type="range_value" from="3" from_unit="cm" name="distance" src="d0_s0" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6660" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s0" to="subtruncate" />
      </biological_entity>
      <biological_entity id="o6661" name="sinuse" name_original="sinuses" src="d0_s0" type="structure">
        <character is_modifier="false" name="depth" src="d0_s0" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6662" name="lobe" name_original="lobe" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="deeply" name="arrangement" src="d0_s0" value="separated" value_original="separated" />
        <character constraint="at 60º-80º" is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescence branches glabrous.</text>
      <biological_entity constraint="inflorescence" id="o6663" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 15–20 mm diam.</text>
      <biological_entity id="o6664" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pomes 9–12 mm diam.</text>
      <biological_entity id="o6665" name="pome" name_original="pomes" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brush, field margins, woodland edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brush" />
        <character name="habitat" value="field margins" />
        <character name="habitat" value="woodland edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety ferrissii is a very distinct form from northern Illinois that has recently been rediscovered near Tinley Park; it should perhaps be referred to another species.</discussion>
  
</bio:treatment>