<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">302</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">alchemilla</taxon_name>
    <taxon_name authority="Buser" date="1893" rank="species">glomerulans</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Herb. Boissier</publication_title>
      <place_in_publication>1(app. 2): 30. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus alchemilla;species glomerulans</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100011</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alchemilla</taxon_name>
    <taxon_name authority="Buser" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_hierarchy>genus Alchemilla;species obtusa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Böcher" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_name authority="Brenner" date="unknown" rank="variety">comosa</taxon_name>
    <taxon_hierarchy>genus A.;species obtusa;variety comosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pseudomicans</taxon_name>
    <taxon_hierarchy>genus A.;species pseudomicans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vulgaris</taxon_name>
    <taxon_name authority="(Brenner) Fernald &amp; Wiegand" date="unknown" rank="variety">comosa</taxon_name>
    <taxon_hierarchy>genus A.;species vulgaris;variety comosa</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Clustered lady's mantle</other_name>
  <other_name type="common_name">alchémille à glomérules</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, grass green or glaucous, often becoming reddish orange when young changing to dark brownish especially on margins of leaves and flowers (young flowers yellowish), often coarse, 30–40 cm.</text>
      <biological_entity id="o1081" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grass green" value_original="grass green" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="when young changing to dark brownish especially on margins of leaves and flowers" name="coloration" src="d0_s0" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" modifier="often" name="relief" src="d0_s0" value="coarse" value_original="coarse" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems appressed-hairy throughout (hairs becoming looser and ± ascending distally).</text>
      <biological_entity id="o1082" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules translucent to pale green, appearing brownish upon drying;</text>
      <biological_entity id="o1083" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1084" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="translucent" name="coloration" src="d0_s2" to="pale green" />
        <character constraint="upon drying" is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole thickly, usually densely appressed-hairy throughout, rarely glabrous or sparsely hairy (on spring leaves);</text>
      <biological_entity id="o1085" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1086" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually densely; throughout" name="pubescence" src="d0_s3" value="appressed-hairy" value_original="appressed-hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade reniform to orbiculate, 7–9-lobed, margins undulate, basal sinuses narrow, middle lobes equal to longer than their half-widths;</text>
      <biological_entity id="o1087" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1088" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s4" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="7-9-lobed" value_original="7-9-lobed" />
      </biological_entity>
      <biological_entity id="o1089" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1090" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="middle" id="o1091" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="longer than their half-widths" value_original="longer than their half-widths" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>incisions absent;</text>
      <biological_entity id="o1092" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o1093" name="incision" name_original="incisions" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>teeth: sometimes proximal sides at least slightly connivent, slightly concave near apex, slightly asymmetric, apex subobtuse to acute, abaxial surface with nerves hairy throughout, internerve regions ± hairy throughout, adaxial light to grass green, sometimes glaucous, margins and folds usually turning reddish orange, sparsely to densely appressed-hairy throughout or only on folds.</text>
      <biological_entity id="o1094" name="tooth" name_original="teeth" src="d0_s6" type="structure" />
      <biological_entity id="o1095" name="side" name_original="sides" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s6" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="at-least slightly" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
        <character constraint="near apex" constraintid="o1096" is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" notes="" src="d0_s6" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o1096" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o1097" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="subobtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1098" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o1099" name="nerve" name_original="nerves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o1100" name="region" name_original="regions" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less; throughout" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1101" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s6" to="grass green" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o1102" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o1103" name="fold" name_original="folds" src="d0_s6" type="structure" />
      <biological_entity id="o1104" name="fold" name_original="folds" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="true" modifier="sparsely; densely" name="pubescence" src="d0_s6" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
      <relation from="o1098" id="r64" name="with" negation="false" src="d0_s6" to="o1099" />
      <relation from="o1102" id="r65" name="turning" negation="false" src="d0_s6" to="o1104" />
      <relation from="o1103" id="r66" name="turning" negation="false" src="d0_s6" to="o1104" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences: primary branches densely appressed to ascending-hairy;</text>
      <biological_entity id="o1105" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="primary" id="o1106" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ascending-hairy" value_original="ascending-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles appressed to ascending-hairy or glabrous.</text>
      <biological_entity id="o1107" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o1108" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="ascending-hairy" value_original="ascending-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels mostly glabrous or some of the proximal hairy.</text>
      <biological_entity id="o1109" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s9" value="some" value_original="some" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1110" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o1109" id="r67" name="part_of" negation="false" src="d0_s9" to="o1110" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: epicalyx bractlet lengths 0.5 times to almost equal to sepals (narrower);</text>
      <biological_entity id="o1111" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="epicalyx" id="o1112" name="bractlet" name_original="bractlet" src="d0_s10" type="structure">
        <character constraint="sepal" constraintid="o1113" is_modifier="false" name="length" src="d0_s10" value="0.5 times to almost equal to sepals" />
      </biological_entity>
      <biological_entity id="o1113" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>hypanthium glabrous or sparsely appressed-hairy (on proximalmost flowers).</text>
      <biological_entity id="o1114" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1115" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes not exserted.</text>
      <biological_entity id="o1116" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist herb slopes, willow scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist herb slopes" />
        <character name="habitat" value="willow scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.), Que.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In contrast to Alchemilla wichurae, A. glomerulans occurs throughout the southern (unglaciated) portion of Greenland.</discussion>
  
</bio:treatment>