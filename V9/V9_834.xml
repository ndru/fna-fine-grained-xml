<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">497</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Crataegus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="series">Crataegus</taxon_name>
    <taxon_name authority="Jacquin" date="1775" rank="species">monogyna</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">monogyna</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section crataegus;series crataegus;species monogyna;variety monogyna;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100576</other_info_on_name>
  </taxon_identification>
  <number>2a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 40–100 [–120] dm.</text>
      <biological_entity id="o14032" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="dm" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="dm" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth glabrous;</text>
      <biological_entity id="o14034" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o14035" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o14036" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules of flowering shoots ± entire;</text>
      <biological_entity id="o14037" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o14038" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14039" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
      </biological_entity>
      <relation from="o14038" id="r902" name="part_of" negation="false" src="d0_s2" to="o14039" />
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs few to numerous, usually indeterminate and terminal, 0.7–1 cm, when determinate and lateral ± stout, to 2.4 cm.</text>
      <biological_entity id="o14040" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o14041" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="development" notes="" src="d0_s3" value="indeterminate" value_original="indeterminate" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="when determinate lateral more or less stout; when determinate lateral more or less stout" name="some_measurement" src="d0_s3" to="2.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14042" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="numerous" />
      </biological_entity>
      <relation from="o14041" id="r903" name="on" negation="false" src="d0_s3" to="o14042" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole slender, 1–3 cm, glabrous, eglandular;</text>
      <biological_entity id="o14043" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14044" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to deltate, 1.2–5.5 cm, base cuneate to truncate, lobes 1–3 per side, sinuses usually deep, lobe apex acute, margins weakly and often few-toothed at lobe apices, veins 2–5 per side, apex acute, abaxial surface paler green, villous on major veins, adaxial ± lustrous dark green, glabrous or mainly villous.</text>
      <biological_entity id="o14045" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o14046" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="deltate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s5" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14047" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="truncate" />
      </biological_entity>
      <biological_entity id="o14048" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o14049" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o14049" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o14050" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="depth" src="d0_s5" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o14051" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14052" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character constraint="at lobe apices" constraintid="o14053" is_modifier="false" modifier="often" name="shape" src="d0_s5" value="few-toothed" value_original="few-toothed" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o14053" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o14054" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o14055" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity id="o14055" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o14056" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14057" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="paler green" value_original="paler green" />
        <character constraint="on veins" constraintid="o14058" is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o14058" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14059" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s5" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="mainly" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 5–15-flowered, ± lax;</text>
      <biological_entity id="o14060" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-15-flowered" value_original="5-15-flowered" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches glabrous [to villous];</text>
      <biological_entity id="o14061" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteole margins entire or bearing few to numerous glands on small irregular protuberances or on small pegs.</text>
      <biological_entity constraint="bracteole" id="o14062" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s8" value="bearing few to numerous glands" />
      </biological_entity>
      <biological_entity id="o14063" name="peg" name_original="pegs" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="small" value_original="small" />
      </biological_entity>
      <relation from="o14062" id="r904" name="on small irregular protuberances or on" negation="false" src="d0_s8" to="o14063" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers 8–16 mm diam.;</text>
      <biological_entity id="o14064" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium glabrous [to lanate];</text>
      <biological_entity id="o14065" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 1.5–2.5 mm, margins entire, apex acute or obtuse;</text>
      <biological_entity id="o14066" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14067" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14068" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 15–20, anthers pink-purple;</text>
      <biological_entity id="o14069" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <biological_entity id="o14070" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="pink-purple" value_original="pink-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1.</text>
      <biological_entity id="o14071" name="style" name_original="style" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pomes sometimes bright, usually dark red, orbicular to ± cylindric, 6–11 mm diam.;</text>
      <biological_entity id="o14072" name="pome" name_original="pomes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="reflectance" src="d0_s14" value="bright" value_original="bright" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="dark red" value_original="dark red" />
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s14" to="more or less cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals reflexed;</text>
      <biological_entity id="o14073" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pyrene 1.2n = 34, 51.</text>
      <biological_entity id="o14074" name="pyrene" name_original="pyrene" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14075" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
        <character name="quantity" src="d0_s16" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brush, woodland edges and old pastures in medium rainfall areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brush" />
        <character name="habitat" value="woodland edges" />
        <character name="habitat" value="old pastures" constraint="in medium rainfall" />
        <character name="habitat" value="medium rainfall" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., N.S., Ont., P.E.I., Que.; Calif., Colo., Conn., Del., Ill., Ind., Maine, Mass., Mich., Minn., N.H., N.J., N.Y., Ohio, Oreg., Pa., R.I., Vt., Wash., Wis.; Europe; sw Asia; n Africa; introduced also in South America (Argentina, Chile), s Africa, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value=" also in South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value=" also in South America (Chile)" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety monogyna differs from var. lasiocarpa (Lange) K. I. Christensen in the absence of villous abaxial leaf surfaces and inflorescences. Variety monogyna is distributed throughout the area of the species; var. lasiocarpa primarily occurs in the Balkan, Iranian, and Mediterranean areas, and is most common in Greece.</discussion>
  <discussion>Variety monogyna is locally common in the Pacific Northwest and adjacent Canada, as well as from the Great Lakes to the Atlantic. The variety hybridizes in North America with the diploid species Crataegus suksdorfii and rarely with C. punctata. The hybrid with C. laevigata, C. ×media Bechstein, is well known in the wild in Europe; only its cultivars are seen in North America.</discussion>
  
</bio:treatment>