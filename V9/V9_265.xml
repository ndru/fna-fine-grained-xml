<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Concinnae</taxon_name>
    <taxon_name authority="Ertter" date="1992" rank="species">morefieldii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>44: 432, fig. 1. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section concinnae;species morefieldii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100342</other_info_on_name>
  </taxon_identification>
  <number>50.</number>
  <other_name type="common_name">Morefield’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.4–1.5 (–1.7) dm, lengths 2–3 times basal leaves.</text>
      <biological_entity id="o13052" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1.7" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o13053" is_modifier="false" name="length" src="d0_s0" value="2-3 times basal leaves" value_original="2-3 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13053" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves pinnate, usually at least 2 pairs of leaflets separate from terminal leaflets, distal leaflets usually distinct, 2–6 cm;</text>
      <biological_entity constraint="basal" id="o13054" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character constraint="of leaflets" constraintid="o13055" modifier="usually at-least; at least" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o13055" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character constraint="from terminal leaflets" constraintid="o13056" is_modifier="false" name="arrangement" src="d0_s1" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13056" name="leaflet" name_original="leaflets" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o13057" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s1" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.2–1.5 cm, straight hairs abundant, ± appressed, 1–1.5 mm, stiff, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o13058" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13059" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o13060" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13061" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 5–9, on distal (1/4–) 1/2–3/4+ of leaf axis, overlapping, proximal pair separated from others by 2–10 mm of leaf axis, central leaflets oblanceolate to narrowly obovate, (0.5–) 1–2 (–2.5) × 0.5–1.2 cm, petiolules 0–1 mm, distal 3/4 of margins incised 3/4 to midvein (sometimes medially cleft as well), teeth 3–4 per side, separate to slightly overlapping, (1–) 2–5 mm, surfaces ± to strongly dissimilar, abaxial grayish to white, straight hairs ± abundant, ± appressed, 1 mm, weak to stiff (especially on veins), cottony hairs abundant to dense, glands absent or obscured, adaxial greenish to grayish, straight hairs ± abundant, appressed, 0.5–1.5 mm, stiff, cottony hairs absent, rarely sparse, glands sparse.</text>
      <biological_entity id="o13062" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="9" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13063" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="of leaf axis" constraintid="o13064" from="1/4" name="quantity" src="d0_s3" to="1/2-3/4" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o13064" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o13065" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character constraint="from others" constraintid="o13066" is_modifier="false" name="arrangement" src="d0_s3" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o13066" name="other" name_original="others" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o13067" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity constraint="central" id="o13068" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13069" name="petiolule" name_original="petiolules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character constraint="of margins" constraintid="o13070" name="quantity" src="d0_s3" value="3/4" value_original="3/4" />
        <character constraint="to midvein" constraintid="o13071" name="quantity" src="d0_s3" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o13070" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o13071" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o13072" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o13073" from="3" name="quantity" src="d0_s3" to="4" />
        <character char_type="range_value" from="separate" name="arrangement" notes="" src="d0_s3" to="slightly overlapping" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13073" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o13074" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o13075" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="grayish" modifier="strongly" name="coloration" src="d0_s3" to="white" />
      </biological_entity>
      <biological_entity id="o13076" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="stiff" />
      </biological_entity>
      <biological_entity id="o13077" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o13078" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13079" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s3" to="grayish" />
      </biological_entity>
      <biological_entity id="o13080" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o13081" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o13082" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o13062" id="r818" name="on" negation="false" src="d0_s3" to="o13063" />
      <relation from="o13066" id="r819" modifier="by 2-10 mm" name="part_of" negation="false" src="d0_s3" to="o13067" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–15-flowered.</text>
      <biological_entity id="o13083" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-15-flowered" value_original="3-15-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1–2.5 (–3) cm.</text>
      <biological_entity id="o13084" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: epicalyx bractlets ± ovate-elliptic, 2–4 × 0.8–2 mm;</text>
      <biological_entity id="o13085" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="epicalyx" id="o13086" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium 2.5–5 mm diam.;</text>
      <biological_entity id="o13087" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13088" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 3.5–5 mm, apex ± acute;</text>
      <biological_entity id="o13089" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13090" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13091" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 4–6 × 3–5 mm;</text>
      <biological_entity id="o13092" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13093" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 1–2 mm, anthers 0.5–1 mm;</text>
      <biological_entity id="o13094" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13095" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13096" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels 15–20, styles 2 mm.</text>
      <biological_entity id="o13097" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13098" name="carpel" name_original="carpels" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
      <biological_entity id="o13099" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 1.5–2 mm, smooth to faintly rugose.</text>
      <biological_entity id="o13100" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="faintly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra and fellfields, mostly on dolomite substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" modifier="alpine" />
        <character name="habitat" value="fellfields" />
        <character name="habitat" value="dolomite substrates" modifier="mostly on" />
        <character name="habitat" value="alpine" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3500–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla morefieldii is known only from the White Mountains and adjacent Sierra Nevada. Plants with similar aspect but lacking strigose vestiture occur in White Pine County, Nevada; their disposition is uncertain. Potentilla morefieldii is sometimes confused with P. breweri (sect. Multijugae), but the latter differs in having more or less similar leaflet surfaces with cottony hairs nearly as abundant adaxially as abaxially. Potentilla breweri also lacks the stiff adaxial leaflet hairs that characterize most species in sect. Concinnae, including P. morefieldii.</discussion>
  <discussion>W. L. Jepson (1909–1943, vol. 2) misapplied the name Potentilla pseudosericea var. grandiflora Th. Wolf, which is a synonym of an unresolved species from the Rocky Mountains (B. Ertter 1992; Ertter et al. 2013), to P. morefieldii.</discussion>
  
</bio:treatment>