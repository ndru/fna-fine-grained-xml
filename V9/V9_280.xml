<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">Subviscosae</taxon_name>
    <taxon_name authority="N. H. Holmgren" date="1987" rank="species">cottamii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>39: 340, fig. 1. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section subviscosae;species cottamii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100315</other_info_on_name>
  </taxon_identification>
  <number>60.</number>
  <other_name type="common_name">Cottam’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Short hairs not well differentiated from long hairs, absent or sparse throughout.</text>
      <biological_entity id="o4529" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character constraint="from hairs" constraintid="o4530" is_modifier="false" modifier="not well" name="variability" src="d0_s0" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="throughout" name="quantity" src="d0_s0" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o4530" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.4–1.7 dm.</text>
      <biological_entity id="o4531" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s1" to="1.7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves ternate, (1.5–) 2–6 (–8) cm;</text>
      <biological_entity constraint="basal" id="o4532" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="ternate" value_original="ternate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.8–5 (–7) cm, long hairs sparse to abundant, usually ± spreading, sometimes ascending, 0.5–1 mm, weak to ± stiff, glands sparse to common;</text>
      <biological_entity id="o4533" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4534" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="abundant" />
        <character is_modifier="false" modifier="usually more or less" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="more or less stiff" />
      </biological_entity>
      <biological_entity id="o4535" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3, central obovate-cuneate, (0.2–) 0.5–1 (–1.5) × 0.2–1 cm, scarcely petiolulate, distal 1/2–2/3 of margins evenly incised 1/3–1/2 to midvein, teeth (1–) 2–3 (–4) per side, surfaces green, long hairs sparse to common, 0.5–1 mm, glands sparse to abundant.</text>
      <biological_entity id="o4536" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="central" id="o4537" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate-cuneate" value_original="obovate-cuneate" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_length" src="d0_s4" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="scarcely" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="of margins" constraintid="o4538" from="1/2" name="quantity" src="d0_s4" to="2/3" />
        <character char_type="range_value" constraint="to midvein" constraintid="o4539" from="1/3" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o4538" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o4539" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o4540" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character char_type="range_value" constraint="per side" constraintid="o4541" from="2" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o4541" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o4542" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o4543" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4544" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–4-flowered.</text>
      <biological_entity id="o4545" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-4-flowered" value_original="1-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.5–2 (–4) cm.</text>
      <biological_entity id="o4546" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets sometimes paired, narrowly elliptic, 1.5–2 (–2.5) × 0.5–1 mm 1/2–3/4 as long as sepals;</text>
      <biological_entity id="o4547" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o4548" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as sepals" constraintid="o4549" from="1/2" name="quantity" src="d0_s7" to="3/4" />
      </biological_entity>
      <biological_entity id="o4549" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 2–3 mm diam.;</text>
      <biological_entity id="o4550" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4551" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 2.5–4 mm, apex broadly acute;</text>
      <biological_entity id="o4552" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4553" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4554" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals ± paler abaxially, bright-yellow adaxially, ± obcordate to broadly obovate, (2–) 3–4 × 2–3 mm;</text>
      <biological_entity id="o4555" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4556" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less; abaxially" name="coloration" src="d0_s10" value="paler" value_original="paler" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="less obcordate" name="shape" src="d0_s10" to="broadly obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–1.5 mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o4557" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4558" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4559" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 10–25, styles 1–1.5 mm.</text>
      <biological_entity id="o4560" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4561" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
      <biological_entity id="o4562" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1.3 mm, smooth to lightly rugose.</text>
      <biological_entity id="o4563" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="1.3" value_original="1.3" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s13" to="lightly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Protected rock crevices, near vertical faces of quartzite and granite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="protected rock crevices" constraint="near vertical faces of quartzite and granite" />
        <character name="habitat" value="vertical faces" constraint="of quartzite and granite" />
        <character name="habitat" value="quartzite" />
        <character name="habitat" value="granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla cottamii is known only from the Pilot Range, Elko County, Nevada, and in northwestern Utah from the Deep Creek, Raft River, and Stansbury mountains. Plants of P. cottamii can form dense clumps to 4 dm across. Except for the short style, which led Holmgren to associate the species with P. hyparctica and P. robbinsiana in sect. Aureae, P. cottamii fits well in sect. Subviscosae.</discussion>
  
</bio:treatment>