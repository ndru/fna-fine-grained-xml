<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James B. Phipps</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="mention_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="M. Roemer" date="unknown" rank="genus">HETEROMELES</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Nat. Syn. Monogr.</publication_title>
      <place_in_publication>3: 100, 105. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus HETEROMELES</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek heteros, different, and melon, apple, alluding to low stamen number</other_info_on_name>
    <other_info_on_name type="fna_id">115321</other_info_on_name>
  </taxon_identification>
  <number>55.</number>
  <other_name type="common_name">Christmas berry</other_name>
  <other_name type="common_name">toyon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, canopies dense, 20–100 dm.</text>
      <biological_entity id="o8080" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o8082" name="canopy" name_original="canopies" src="d0_s0" type="structure">
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1;</text>
      <biological_entity id="o8083" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark (trunk) grayish, with fine tan to dark gray striations, ± smooth;</text>
      <biological_entity id="o8084" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" notes="" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8085" name="striation" name_original="striations" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="fine" value_original="fine" />
        <character char_type="range_value" from="tan" is_modifier="true" name="coloration" src="d0_s2" to="dark gray" />
      </biological_entity>
      <relation from="o8084" id="r517" name="with" negation="false" src="d0_s2" to="o8085" />
    </statement>
    <statement id="d0_s3">
      <text>short-shoots absent;</text>
    </statement>
    <statement id="d0_s4">
      <text>unarmed;</text>
      <biological_entity id="o8086" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>young stems puberulent.</text>
      <biological_entity id="o8087" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves persistent, cauline, simple;</text>
      <biological_entity id="o8088" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o8089" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stipules variably persistent, free, minute, margins unknown, apex gland-tipped;</text>
      <biological_entity id="o8090" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="variably" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o8091" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o8092" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole present;</text>
      <biological_entity id="o8093" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade ± elliptic to narrowly elliptic or narrowly oblong, 5–15 (–20) cm, leathery, margins revolute, sharply, remotely serrate, venation pinnate (craspedodromous-brochidodromous), surfaces tomentose when young, glabrescent.</text>
      <biological_entity id="o8094" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="less elliptic" name="shape" src="d0_s9" to="narrowly elliptic" />
        <character is_modifier="false" name="texture" src="d0_s9" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o8095" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="sharply; remotely" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o8096" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal, 20–150-flowered, panicles, ± dome-shaped, white-tomentose;</text>
      <biological_entity id="o8097" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="20-150-flowered" value_original="20-150-flowered" />
      </biological_entity>
      <biological_entity id="o8098" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="dome--shaped" value_original="dome--shaped" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts present on proximal nodes, leaflike, plus numerous, scalelike appendages on axes;</text>
      <biological_entity id="o8099" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character constraint="on proximal nodes" constraintid="o8100" is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s11" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="quantity" src="d0_s11" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8100" name="node" name_original="nodes" src="d0_s11" type="structure" />
      <biological_entity id="o8101" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o8102" name="axis" name_original="axes" src="d0_s11" type="structure" />
      <relation from="o8101" id="r518" name="on" negation="false" src="d0_s11" to="o8102" />
    </statement>
    <statement id="d0_s12">
      <text>bracteoles present caducous, delicate.</text>
      <biological_entity id="o8103" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s12" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="fragility" src="d0_s12" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicels present.</text>
      <biological_entity id="o8104" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers: perianth and androecium epigynous, 10 mm diam.;</text>
      <biological_entity id="o8105" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8106" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="epigynous" value_original="epigynous" />
        <character name="diameter" src="d0_s14" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o8107" name="androecium" name_original="androecium" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="epigynous" value_original="epigynous" />
        <character name="diameter" src="d0_s14" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>hypanthium urceolate, 2–4 mm, glabrous or weakly floccose;</text>
      <biological_entity id="o8108" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o8109" name="hypanthium" name_original="hypanthium" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="weakly" name="pubescence" src="d0_s15" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals 5, suberect, triangular;</text>
      <biological_entity id="o8110" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o8111" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 5, white, irregularly round, base weakly clawed;</text>
      <biological_entity id="o8112" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o8113" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s17" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o8114" name="base" name_original="base" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s17" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stamens 10, shorter than petals;</text>
      <biological_entity id="o8115" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o8116" name="stamen" name_original="stamens" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
        <character constraint="than petals" constraintid="o8117" is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8117" name="petal" name_original="petals" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>carpels 2 or 3, distinct, basally adnate to hypanthium, styles 2 or 3, lateral, distinct;</text>
      <biological_entity id="o8118" name="flower" name_original="flowers" src="d0_s19" type="structure" />
      <biological_entity id="o8119" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s19" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
        <character constraint="to hypanthium" constraintid="o8120" is_modifier="false" modifier="basally" name="fusion" src="d0_s19" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o8120" name="hypanthium" name_original="hypanthium" src="d0_s19" type="structure" />
      <biological_entity id="o8121" name="style" name_original="styles" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s19" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s19" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 2.</text>
      <biological_entity id="o8122" name="flower" name_original="flowers" src="d0_s20" type="structure" />
      <biological_entity id="o8123" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits pomes, usually bright red, sometimes yellow, ellipsoid, 5–10 mm, glabrous or glabrate;</text>
      <biological_entity constraint="fruits" id="o8124" name="pome" name_original="pomes" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s21" value="bright red" value_original="bright red" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s21" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s21" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>flesh mealy;</text>
      <biological_entity id="o8125" name="flesh" name_original="flesh" src="d0_s22" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s22" value="mealy" value_original="mealy" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>hypanthium persistent;</text>
      <biological_entity id="o8126" name="hypanthium" name_original="hypanthium" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>sepals persistent, accrescent over hypanthial opening.</text>
      <biological_entity id="o8127" name="sepal" name_original="sepals" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
        <character constraint="over hypanthial" constraintid="o8128" is_modifier="false" name="size" src="d0_s24" value="accrescent" value_original="accrescent" />
      </biological_entity>
      <biological_entity id="o8128" name="hypanthial" name_original="hypanthial" src="d0_s24" type="structure" />
    </statement>
    <statement id="d0_s25">
      <text>Pyrenes 2 or 3 per fruit, carpel walls thin, soft;</text>
      <biological_entity id="o8129" name="pyrene" name_original="pyrenes" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" unit="or per" value="2" value_original="2" />
        <character name="quantity" src="d0_s25" unit="or per" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8130" name="fruit" name_original="fruit" src="d0_s25" type="structure" />
      <biological_entity constraint="carpel" id="o8131" name="wall" name_original="walls" src="d0_s25" type="structure">
        <character is_modifier="false" name="width" src="d0_s25" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s25" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>styles not persistent;</text>
      <biological_entity id="o8132" name="style" name_original="styles" src="d0_s26" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s26" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>seeds 1 per pyrene, large.</text>
      <biological_entity id="o8133" name="seed" name_original="seeds" src="d0_s27" type="structure">
        <character constraint="per pyrene" constraintid="o8134" name="quantity" src="d0_s27" value="1" value_original="1" />
        <character is_modifier="false" name="size" notes="" src="d0_s27" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o8134" name="pyrene" name_original="pyrene" src="d0_s27" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Heteromeles resembles Aronia and Photinia; it differs by having 10 stamens instead of 20, nearly free carpels (versus fully connate), and fruit flesh lacking stone cells (versus often possessing some). Its fruit is of the coreless type (J. R. Rohrer et al. 1991). Molecular evidence (D. Potter et al. 2007) shows a relationship to the Asiatic Eriobotrya and Rhaphiolepis Lindley miniclade; fruit type, notched petals, and other characteristics agree.</discussion>
  <references>
    <reference>Phipps, J. B. 1992. Heteromeles and Photinia (Rosaceae subfam. Maloideae) of Mexico and Central America. Canad. J. Bot. 70: 2138–2162.</reference>
  </references>
  
</bio:treatment>