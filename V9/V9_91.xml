<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">radiatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 300. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species radiatum</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100226</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sieversia</taxon_name>
    <taxon_name authority="(Michaux) G. Don" date="unknown" rank="species">radiata</taxon_name>
    <taxon_hierarchy>genus Sieversia;species radiata</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Appalachian or spreading avens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subscapose.</text>
      <biological_entity id="o9798" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 10–45 (–60) cm, densely hirsute proximally to puberulent distally.</text>
      <biological_entity id="o9799" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 10–30 cm, blade strongly lyrate-pinnate, sometimes simple, major leaflet 1, minor leaflets 1–6, terminal leaflet much larger than minor laterals;</text>
      <biological_entity id="o9800" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9801" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9802" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s2" value="lyrate-pinnate" value_original="lyrate-pinnate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o9803" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="minor" id="o9804" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o9805" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than minor laterals" constraintid="o9806" is_modifier="false" name="size" src="d0_s2" value="much larger" value_original="much larger" />
      </biological_entity>
      <biological_entity constraint="minor" id="o9806" name="lateral" name_original="laterals" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 1.2–4 cm, stipules not evident, blade bractlike, not resembling basal, simple.</text>
      <biological_entity id="o9807" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o9808" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9809" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o9810" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9811" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o9810" id="r608" name="resembling" negation="true" src="d0_s3" to="o9811" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–10-flowered.</text>
      <biological_entity id="o9812" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-10-flowered" value_original="3-10-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels glandular-hairy.</text>
      <biological_entity id="o9813" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o9814" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 2–5 mm;</text>
      <biological_entity constraint="epicalyx" id="o9815" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green;</text>
      <biological_entity id="o9816" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals spreading in flower, erect in fruit, 6–10 mm;</text>
      <biological_entity id="o9817" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="in flower" constraintid="o9818" is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character constraint="in fruit" constraintid="o9819" is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9818" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity id="o9819" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, yellow, orbiculate to obcordate, 9–16 mm, longer than sepals, apex emarginate.</text>
      <biological_entity id="o9820" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s10" to="obcordate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character constraint="than sepals" constraintid="o9821" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o9821" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile, glabrous.</text>
      <biological_entity id="o9823" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <relation from="o9822" id="r609" name="fruiting" negation="false" src="d0_s11" to="o9823" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles wholly persistent, not geniculate-jointed, 8–12 mm, apex not hooked, pilose and stipitate-glandular in basal 1/3.2n = 42.</text>
      <biological_entity id="o9822" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9824" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o9825" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="wholly" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9826" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
        <character constraint="in basal 1/3" constraintid="o9827" is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9827" name="1/3" name_original="1/3" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o9828" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
      <relation from="o9822" id="r610" name="fruiting" negation="false" src="d0_s12" to="o9824" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky cliffs and ledges, montane balds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="montane balds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Geum radiatum is closely related to G. calthifolium and G. peckii. The morphologic differences among them are slight compared to the discontinuities separating other Geum species. On the basis of morphology it would be possible to combine all three in a single species. Yet they occupy distinct ranges separated by a minimum of 1200 km. Traditionally, they have been treated as separate species, and recently I. G. Paterson and M. Snyder (1999) reported molecular genetic evidence for continuing to recognize G. peckii and G. radiatum as separate species.</discussion>
  <discussion>Geum radiatum is listed as an endangered species by the U.S. Fish and Wildlife Service.</discussion>
  
</bio:treatment>