<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Multijugae</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">hickmanii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 77. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section multijugae;species hickmanii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100330</other_info_on_name>
  </taxon_identification>
  <number>42.</number>
  <other_name type="common_name">Hickman’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± rosetted;</text>
      <biological_entity id="o1773" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots fleshy-thickened.</text>
      <biological_entity id="o1774" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually prostrate to decumbent, sometimes ± ascending in supporting vegetation, 0.5–2.5 dm, lengths 1–2 times basal leaves.</text>
      <biological_entity id="o1775" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation_or_growth_form" src="d0_s2" value="prostrate to decumbent" value_original="prostrate to decumbent" />
        <character constraint="in vegetation" constraintid="o1776" is_modifier="false" modifier="sometimes more or less" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="2.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o1777" is_modifier="false" name="length" src="d0_s2" value="1-2 times basal leaves" value_original="1-2 times basal leaves" />
      </biological_entity>
      <biological_entity id="o1776" name="vegetation" name_original="vegetation" src="d0_s2" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s2" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1777" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves pinnate with distal leaflets ± confluent, 3–17 × 1–3.5 cm;</text>
      <biological_entity constraint="basal" id="o1778" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with distal leaflets" constraintid="o1779" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" notes="" src="d0_s3" to="17" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1779" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–5 cm, straight hairs sparse to common, appressed, 0.5–1.5 (–2) mm, stiff, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o1780" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1781" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1782" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1783" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lateral leaflets 3–6 per side, on distal 1/2–2/3 of leaf axis, ± overlapping to nearly separate, largest ones cuneate to flabellate, 0.5–2 × 0.5–1.5 cm, distal 1/2 to whole margin ± palmately incised 1/2–2/3 (+) to midvein, ultimate teeth or segments 2–5, narrowly elliptic to oblanceolate, 2–10 (–15) × (1–) 1.5–3 mm, apical tufts 0.5 mm, surfaces green, not glaucous, straight hairs sparse to common (often sparser to glabrate adaxially), appressed, 1–1.5 mm, stiff, cottony hairs absent, glands absent or inconspicuous.</text>
      <biological_entity constraint="primary lateral" id="o1784" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o1785" from="3" name="quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="less overlapping" name="arrangement" notes="" src="d0_s5" to="nearly separate" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="flabellate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character constraint="to margin" constraintid="o1788" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="2/3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" unit="1/2-2/3[+]" upper_restricted="false" />
        <character char_type="range_value" constraint="to segments" constraintid="o1792" from="1/2" name="quantity" src="d0_s5" to="2/3" unit="1/2-2/3[+]" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" notes="" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s5" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1785" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1786" name="1/2-2/3" name_original="1/2-2/3" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o1787" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o1788" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less palmately" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o1789" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o1790" name="tooth" name_original="teeth" src="d0_s5" type="structure" />
      <biological_entity constraint="ultimate" id="o1791" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o1792" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1793" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o1794" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o1795" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1796" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1797" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o1784" id="r118" name="on" negation="false" src="d0_s5" to="o1786" />
      <relation from="o1786" id="r119" name="part_of" negation="false" src="d0_s5" to="o1787" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 2.</text>
      <biological_entity constraint="cauline" id="o1798" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 2–5-flowered, very openly cymose, sometimes racemiform.</text>
      <biological_entity id="o1799" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-5-flowered" value_original="2-5-flowered" />
        <character is_modifier="false" modifier="very openly" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–2 (–4) cm, ± recurved in fruit.</text>
      <biological_entity id="o1800" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character constraint="in fruit" constraintid="o1801" is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o1801" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets ± elliptic, 3.5–6 × (1–) 1.5–2.5 mm;</text>
      <biological_entity id="o1802" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o1803" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s9" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 3–5 mm diam.;</text>
      <biological_entity id="o1804" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1805" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 3.5–7 (–9) mm, apex acute;</text>
      <biological_entity id="o1806" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1807" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1808" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 6–12 × 5–9 mm;</text>
      <biological_entity id="o1809" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1810" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments (1.5–) 2–3 (–4) mm, anthers 0.7–1.2 mm;</text>
      <biological_entity id="o1811" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1812" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1813" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels (2–) 5–15, styles 2.5–3.5 mm.</text>
      <biological_entity id="o1814" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1815" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s14" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s14" to="15" />
      </biological_entity>
      <biological_entity id="o1816" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 2 mm, smooth, ± carunculate.</text>
      <biological_entity id="o1817" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernally saturated coastal meadows, openings in Monterey Pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal meadows" modifier="vernally saturated" />
        <character name="habitat" value="openings" constraint="in monterey pine forests" />
        <character name="habitat" value="monterey pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">hickmani</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla hickmanii is known from only two extant populations in Monterey and San Mateo counties. The species is a federally listed endangered species (U.S. Fish and Wildlife Service, ecos.fws.gov/docs/recovery_plan/050617a.pdf). Plants from Sonoma County formerly included in P. hickmanii now constitute P. uliginosa. Although some morphologic overlap occurs with P. millefolia, P. hickmanii generally differs in having less dissected leaflets occupying less of the leaf axis, as well as somewhat larger flowers bearing fewer carpels.</discussion>
  
</bio:treatment>