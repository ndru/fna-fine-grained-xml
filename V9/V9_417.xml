<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">CAPITATAE</taxon_name>
    <taxon_name authority="Lindley" date="1837" rank="species">fusca</taxon_name>
    <taxon_name authority="(Nuttall ex Hooker &amp; Arnott) Wawra" date="1883" rank="variety">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Itin. Princ. S. Coburgi</publication_title>
      <place_in_publication>1: 17. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section capitatae;species fusca;variety parviflora;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100659</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="Nuttall ex Hooker &amp; Arnott" date="unknown" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>338. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Horkelia;species parviflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">fusca</taxon_name>
    <taxon_name authority="(Nuttall ex Hooker &amp; Arnott) D. D. Keck" date="unknown" rank="subspecies">parviflora</taxon_name>
    <taxon_hierarchy>genus H.;species fusca;subspecies parviflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">andersonii</taxon_name>
    <taxon_hierarchy>genus Potentilla;species andersonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_name authority="(Nuttall ex Hooker &amp; Arnott) J. T. Howell" date="unknown" rank="variety">parviflora</taxon_name>
    <taxon_hierarchy>genus P.;species douglasii;variety parviflora</taxon_hierarchy>
  </taxon_identification>
  <number>15f.</number>
  <other_name type="common_name">Small-flowered horkelia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–4.5 dm.</text>
      <biological_entity id="o17035" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves green, 4–15 (–18) cm;</text>
      <biological_entity constraint="basal" id="o17036" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="18" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets 4–8 per side, cuneate to broadly obovate, 5–15 (–20) × (2–) 5–10 (–15) mm, 1/2 as wide to wider than long, divided 1/4–1/2 to midrib into 4–6 (–10) teeth, surfaces not obscured, sparsely to moderately hirsute or villous, sometimes glabrate.</text>
      <biological_entity id="o17037" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o17038" from="4" name="quantity" src="d0_s2" to="8" />
        <character char_type="range_value" from="cuneate" name="shape" notes="" src="d0_s2" to="broadly obovate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character name="quantity" src="d0_s2" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="width" src="d0_s2" value="wide to wider than long" />
        <character is_modifier="false" name="shape" src="d0_s2" value="divided" value_original="divided" />
        <character char_type="range_value" constraint="to midrib" constraintid="o17039" from="1/4" name="quantity" src="d0_s2" to="1/2" />
      </biological_entity>
      <biological_entity id="o17038" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o17039" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o17040" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s2" to="10" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
      <biological_entity id="o17041" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s2" value="obscured" value_original="obscured" />
        <character is_modifier="false" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o17039" id="r1090" name="into" negation="false" src="d0_s2" to="o17040" />
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves 1–3 (or 4);</text>
      <biological_entity constraint="cauline" id="o17042" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets of proximalmost 2–4 (or 5) per side.</text>
      <biological_entity id="o17043" name="leaflet" name_original="leaflets" src="d0_s4" type="structure" />
      <biological_entity constraint="proximalmost" id="o17044" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o17045" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o17045" name="side" name_original="side" src="d0_s4" type="structure" />
      <relation from="o17043" id="r1091" name="consist_of" negation="false" src="d0_s4" to="o17044" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences green to purplish, congested to open, usually comprising less than 1/4 of stem, composed of 5–20-flowered glomerules, glandular-hairs not red-septate;</text>
      <biological_entity id="o17046" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="purplish" />
        <character char_type="range_value" from="congested" name="architecture" src="d0_s5" to="open" />
      </biological_entity>
      <biological_entity id="o17047" name="stem" name_original="stem" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s5" to="1/4" />
      </biological_entity>
      <biological_entity id="o17048" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="5-20-flowered" value_original="5-20-flowered" />
      </biological_entity>
      <biological_entity id="o17049" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually; not" name="architecture" src="d0_s5" value="red-septate" value_original="red-septate" />
      </biological_entity>
      <relation from="o17046" id="r1092" modifier="usually" name="comprising" negation="false" src="d0_s5" to="o17047" />
      <relation from="o17046" id="r1093" modifier="usually" name="composed of" negation="false" src="d0_s5" to="o17048" />
    </statement>
    <statement id="d0_s6">
      <text>bracts acute-lobed, not obscuring pedicels and flowers at maturity.</text>
      <biological_entity id="o17050" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute-lobed" value_original="acute-lobed" />
      </biological_entity>
      <biological_entity id="o17051" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o17052" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <relation from="o17050" id="r1094" name="obscuring" negation="true" src="d0_s6" to="o17051" />
      <relation from="o17050" id="r1095" name="obscuring" negation="false" src="d0_s6" to="o17052" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets 1–2 (–3) mm;</text>
      <biological_entity id="o17053" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o17054" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 1.5–2 × 2–3.5 mm;</text>
      <biological_entity id="o17055" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17056" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 2–4.5 (–6.5) mm;</text>
      <biological_entity id="o17057" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17058" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 0.2–1 mm, usually longer than wide, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o17059" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17060" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="usually longer than wide" value_original="usually longer than wide" />
      </biological_entity>
      <biological_entity id="o17061" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 0.9–1.1 mm.</text>
      <biological_entity id="o17062" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17063" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 1–1.2 mm. 2n = 28.</text>
      <biological_entity id="o17064" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17065" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry meadow edges, in conifer woodlands, mainly on volcanic or granitic soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry meadow edges" />
        <character name="habitat" value="conifer woodlands" />
        <character name="habitat" value="volcanic" />
        <character name="habitat" value="granitic soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety parviflora is the most widespread and polymorphic in the species and might represent the repository for variation that is not accommodated by the other, more tightly circumscribed varieties. Population clusters occur in three discrete areas: the Sierra Nevada of eastern California and adjacent Nevada, the mountains of northern California extending north in the Cascade Range to central Oregon, and the mountains of central Idaho. Each of these three areas has plants that differ slightly from those of the other two.</discussion>
  <discussion>The most distinct phase is found in the Cascade Range of Oregon, with larger-than-average petals and highly branched inflorescences. Plants from Lost Prairie in eastern Linn County are particularly anomalous, with petals to 6.5 mm; further studies may indicate that these deserve separate recognition.</discussion>
  <discussion>Plants from the Sierra Nevada tend to have smaller petals, and those in the higher southern Sierra Nevada tend to have capitate or subcapitate inflorescences. From about Lake Tahoe northward, inflorescences tend to be somewhat more open but still congested compared to the Oregon phase.</discussion>
  <discussion>The Idaho phase, represented by the type of var. parviflora, has petals that run the full range from 2–4 mm, with most individuals having a slightly branched inflorescence. Unlike those of the other two phases, both the hypanthia and sepals are often purple; the hypanthium is otherwise typically greenish with only the sepals themselves a dark purple. The sole Wyoming record (D. R. Goddard 1027, F, UC) has yet to be confirmed by more recent collections in Yellowstone National Park. These plants have pale inflorescences, relatively blunt sepals, and short epicalyx bractlets, differing in these features from the closest populations in central Idaho. For reports from Montana, see species discussion.</discussion>
  
</bio:treatment>