<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">47</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Mociño ex Seringe in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="species">nutkanus</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>2: 566. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species nutkanus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100441</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rubus</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="species">parviflorus</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 308. 1818</place_in_publication>
      <other_info_on_pub>not Weston 1770</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Rubus;species parviflorus</taxon_hierarchy>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Thimbleberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–20 (–30) dm, unarmed.</text>
      <biological_entity id="o26287" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems biennial, erect, sparsely hairy, glabrescent, moderately stipitate-glandular, glands yellowish to reddish, not pruinose.</text>
      <biological_entity id="o26288" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26289" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s1" to="reddish" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, simple;</text>
      <biological_entity id="o26290" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate to ovate, 5–15 mm;</text>
      <biological_entity id="o26291" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade orbiculate to reniform, 5–20 × 5–25 cm, base cordate, palmately, shallowly to moderately deeply, (3–) 5 (–7) -lobed, margins coarsely, irregularly serrate to doubly serrate, apex shortly acuminate to obtuse, abaxial surfaces glabrate to densely hairy, sparsely to moderately stipitate-glandular, glands yellowish to reddish.</text>
      <biological_entity id="o26292" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26293" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="palmately; shallowly to moderately; moderately deeply; deeply" name="shape" src="d0_s4" value="(3-)5(-7)-lobed" value_original="(3-)5(-7)-lobed" />
      </biological_entity>
      <biological_entity id="o26294" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to doubly" value_original="serrate to doubly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o26295" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="shortly acuminate" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26296" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s4" to="densely hairy" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s4" to="densely hairy" />
      </biological_entity>
      <biological_entity id="o26297" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s4" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, (1–) 3–7 (–15), cymiform to thyrsiform.</text>
      <biological_entity id="o26298" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="cymiform" name="architecture" src="d0_s5" to="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels sparsely to moderately hairy, moderately to densely stipitate-glandular, glands yellowish to reddish.</text>
      <biological_entity id="o26299" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="hairy" modifier="moderately" name="pubescence" src="d0_s6" to="moderately densely stipitate-glandular" />
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s6" to="moderately densely stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26300" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s6" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o26301" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, broadly obovate, (10–) 14–22 (–28) mm;</text>
      <biological_entity id="o26302" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="28" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments filiform;</text>
      <biological_entity id="o26303" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovaries distally densely hairy, styles clavate, glabrous.</text>
      <biological_entity id="o26304" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally densely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o26305" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits red, hemispheric, 1–1.8 cm;</text>
      <biological_entity id="o26306" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>drupelets 50–60, coherent, separating from torus.</text>
      <biological_entity id="o26308" name="torus" name_original="torus" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o26307" name="drupelet" name_original="drupelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o26308" is_modifier="false" name="arrangement" src="d0_s12" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26309" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist areas, open woods, thickets, clearings, stream banks, canyons, grassy meadows, rocky cliffs, sand dunes, upper beaches, dry sandy areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="grassy meadows" />
        <character name="habitat" value="rocky cliffs" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="upper beaches" />
        <character name="habitat" value="dry sandy areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont.; Alaska, Ariz., Calif., Colo., Idaho, Ill., Iowa, Mich., Minn., Mont., Nev., N.Mex., Oreg., S.Dak., Utah, Wash., Wis., Wyo.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus nutkanus is distinguished from other flowering raspberries by its erect, unarmed stems, simple leaves, large flowers, white petals, glabrous, clavate styles, and yellowish to reddish stipitate glands covering most plant parts. A hybrid with R. odoratus (R. ×fraseri Rehder) is thought to occur in areas of overlap in northern Michigan (E. G. Voss 1972–1996, vol. 2). Varieties and forms have been described based on indument and glandularity of stems, leaves, petioles, pedicels, and sepals (for example, M. L. Fernald 1950). Different variants often occur mixed in the same population and generally do not correspond to definite geographic regions (N. C. Fassett 1941). Plants from coastal California (considered as var. velutinus) tend to have densely hairy leaf abaxial surfaces; this variation may be environmentally induced; other Rubus species show the same pattern (for example, R. spectabilis var. franciscanus) in that area. Wild thimbleberries are collected in Keweenaw Peninsula, Michigan, and are used in beverages and especially for jam.</discussion>
  <discussion>Rubus nutkanus is typically associated with western North America but was originally described from plants at its eastern range limit in northern Michigan. The Great Lakes populations are disjunct from western mountain populations by around 600 miles.</discussion>
  <discussion>The name Rubus parviflorus var. grandiflorus Farwell, which applies here, is illegitimate.</discussion>
  
</bio:treatment>