<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
    <other_info_on_meta type="illustration_page">486</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="genus">chaenomeles</taxon_name>
    <taxon_name authority="(Sweet) Nakai" date="unknown" rank="species">speciosa</taxon_name>
    <place_of_publication>
      <publication_title>Jap. J. Bot.</publication_title>
      <place_in_publication>4: 331. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus chaenomeles;species speciosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200010698</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cydonia</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="species">speciosa</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Suburb. Lond.,</publication_title>
      <place_in_publication>113. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cydonia;species speciosa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Common flowering-quince</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2–20 dm.</text>
      <biological_entity id="o28493" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branches purplish brown or blackish brown, smooth (not verrucose with age).</text>
      <biological_entity id="o28494" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules of vegetative branches usually reniform or suborbiculate, rarely ovate, to 1 cm, margins serrate, teeth triangular, apex acute;</text>
      <biological_entity id="o28495" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o28496" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28497" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o28498" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o28499" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o28500" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o28496" id="r1863" name="part_of" negation="false" src="d0_s2" to="o28497" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 5–10 mm;</text>
      <biological_entity id="o28501" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28502" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, elliptic, or narrowly elliptic, 3–9 × 1.5–5 cm, base cuneate to broadly cuneate, margins serrate, apex obtuse to acute, abaxial surface glabrous or hairy on veins.</text>
      <biological_entity id="o28503" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28504" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28505" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o28506" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o28507" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28508" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o28509" is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o28509" name="vein" name_original="veins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 30–50 mm diam.;</text>
      <biological_entity id="o28510" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="diameter" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals suborbiculate, rarely ovate, 3–4 mm;</text>
      <biological_entity id="o28511" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals scarlet red, rarely pinkish or white, ovate or suborbiculate, 13–24 mm;</text>
      <biological_entity id="o28512" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="scarlet red" value_original="scarlet red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 40–50, equal to petals.</text>
      <biological_entity id="o28513" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="50" />
        <character constraint="to petals" constraintid="o28514" is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o28514" name="petal" name_original="petals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Pomes yellow or yellowish green, globose or ovoid, 40–60 mm diam. 2n = 34 (China).</text>
      <biological_entity id="o28515" name="pome" name_original="pomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="40" from_unit="mm" name="diameter" src="d0_s9" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28516" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vacant lots, old fields, fencerows, wastelands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="wastelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Conn., D.C., Ky., La., Mass., Mo., N.Y., N.C., Ohio, Pa., Tenn., W.Va., Wis.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Choenomeles</other_name>
  <discussion>Chaenomeles speciosa is cultivated as an ornamental for its showy spring flowers and, occasionally, for medicinal use. Putative hybrids of C. japonica and C. speciosa are called C. ×superba (Frahm) Rehder. They also are cultivated and may be expected to spread occasionally through the dumping of garden waste. The putative hybrids are difficult to distinguish from C. speciosa.</discussion>
  
</bio:treatment>