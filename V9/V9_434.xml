<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="illustration_page">263</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="(Rydberg) Rydberg in N. L. Britton et al." date="1908" rank="genus">horkeliella</taxon_name>
    <taxon_name authority="(S. Watson) Rydberg in N. L. Britton et al." date="1908" rank="species">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 282. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkeliella;species purpurascens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220006512</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">purpurascens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 148. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Horkelia;species purpurascens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="(S. Watson) D. D. Keck" date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_hierarchy>genus Ivesia;species purpurascens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_hierarchy>genus Potentilla;species purpurascens</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Kern Plateau horkeliella or false horkelia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green or grayish green to gray, nonglandular hairs sparse to dense, often obscuring stipitate-glandular ones.</text>
      <biological_entity id="o33689" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s0" to="gray" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o33690" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s0" to="dense" />
      </biological_entity>
      <biological_entity id="o33691" name="one" name_original="ones" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o33690" id="r2215" modifier="often" name="obscuring" negation="false" src="d0_s0" to="o33691" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves (5–) 7–17 (–20) cm;</text>
      <biological_entity constraint="basal" id="o33692" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–5 cm;</text>
      <biological_entity id="o33693" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 15–30 (–35) per side, (2–) 3–10 (–20) mm, divided into 3–6 ultimate segments, hirsute (sometimes densely so) and villous (especially marginally), infrequently viscid-stipitate-glandular, or nearly glabrous except for villous tuft at apex.</text>
      <biological_entity id="o33694" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="35" />
        <character char_type="range_value" constraint="per side" constraintid="o33695" from="15" name="quantity" src="d0_s3" to="30" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="10" to_unit="mm" />
        <character constraint="into ultimate segments" constraintid="o33696" is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s3" value="viscid-stipitate-glandular" value_original="viscid-stipitate-glandular" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s3" value="viscid-stipitate-glandular" value_original="viscid-stipitate-glandular" />
        <character constraint="except-for tuft" constraintid="o33697" is_modifier="false" modifier="nearly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o33695" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="ultimate" id="o33696" name="segment" name_original="segments" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o33697" name="tuft" name_original="tuft" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o33698" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <relation from="o33697" id="r2216" name="at" negation="false" src="d0_s3" to="o33698" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 2–4, 2–10 cm.</text>
      <biological_entity constraint="cauline" id="o33699" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences ± open.</text>
      <biological_entity id="o33700" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels (1–) 2–10 (–15) mm.</text>
      <biological_entity id="o33701" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 10–15 mm diam., epicalyx bractlets linear to narrowly ovate, 1.5–3 mm;</text>
      <biological_entity id="o33702" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="epicalyx" id="o33703" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="narrowly ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 3–6 mm, hirsute, sparsely villous, sometimes minutely glandular or stipitate-glandular;</text>
      <biological_entity id="o33704" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s8" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals oblanceolate to narrowly spatulate or oblong, 3–7 mm;</text>
      <biological_entity id="o33705" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="narrowly spatulate or oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 1–2.5 mm, filament opposite center of sepal shorter than those on either side, anthers 0.5–0.8 (–1) mm;</text>
      <biological_entity id="o33706" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33707" name="filament" name_original="filament" src="d0_s10" type="structure" />
      <biological_entity id="o33708" name="center" name_original="center" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o33709" name="sepal" name_original="sepal" src="d0_s10" type="structure" />
      <biological_entity id="o33710" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" modifier="on either side" name="atypical_some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="on either side" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o33708" id="r2217" name="part_of" negation="false" src="d0_s10" to="o33709" />
    </statement>
    <statement id="d0_s11">
      <text>styles 2.5–4 mm.</text>
      <biological_entity id="o33711" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry granitic meadows at edges of conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry granitic meadows" constraint="at edges of conifer woodlands" />
        <character name="habitat" value="edges" constraint="of conifer woodlands" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Typical Horkeliella purpurascens occurs on the Kern Plateau on the western slope of the southern Sierra Nevada in Kern and Tulare counties. More densely hairy populations farther south in the Piute, Scodie, and Tehachapi mountains of Kern County tend to have more congested inflorescences, shorter leaves, and fewer carpels; they may merit taxonomic recognition.</discussion>
  
</bio:treatment>