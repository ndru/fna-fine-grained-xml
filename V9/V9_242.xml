<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">166</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Subjugae</taxon_name>
    <taxon_name authority="Rydberg" date="1896" rank="species">subjuga</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 397, plate 274. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section subjugae;species subjuga;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100366</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Th. Wolf" date="unknown" rank="species">osterhoutiana</taxon_name>
    <taxon_hierarchy>genus Potentilla;species osterhoutiana</taxon_hierarchy>
  </taxon_identification>
  <number>34.</number>
  <other_name type="common_name">Colorado cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (0.8–) 1–2.5 (–3.5) dm.</text>
      <biological_entity id="o12431" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.8" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves usually palmate with additional lateral leaflets, sometimes pinnate, 3–10 (–14) cm;</text>
      <biological_entity constraint="basal" id="o12432" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="with lateral leaflets" constraintid="o12433" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="palmate" value_original="palmate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="14" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o12433" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="additional" value_original="additional" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1.5–5 cm, vestiture seasonally dimorphic, long hairs abundant, spreading on first-formed leaves, tightly appressed to ascending on later-formed leaves, 1–2 mm, ± stiff (especially on later-formed leaves), cottony and crisped hairs usually absent, glands absent or sparse;</text>
      <biological_entity id="o12434" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12435" name="vestiture" name_original="vestiture" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="seasonally" name="growth_form" src="d0_s2" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o12436" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="abundant" value_original="abundant" />
        <character constraint="on first-formed leaves" constraintid="o12437" is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" constraint="on leaves" constraintid="o12438" from="tightly appressed" name="orientation" notes="" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity constraint="first-formed" id="o12437" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12438" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12439" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12440" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets (3–) 5 at tip of leaf axis plus 1 (–2) additional pair (s) separated from tip by 3–20 mm, on distal 1/10–1/3 (–1/2) of leaf axis, largest leaflets oblanceolate-oblong, (0.5–) 1.5–2.5 (–3) × 0.3–1 cm, ± whole margin incised 1/2–2/3 (–3/4) to midvein, teeth (2–) 4–9 per side, usually touching to strongly overlapping, sometimes separate, 2–6 mm, surfaces usually strongly dissimilar (less so on first-formed leaves), abaxial usually white, straight hairs ± abundant (mostly on veins), 1–2 mm, cottony or crisped/cottony hairs ± dense (sparser on first-formed leaves), glands absent or obscured, adaxial green (to grayish), straight hairs sparse to common, 0.5–1.5 mm, cottony and crisped hairs absent, glands sparse.</text>
      <biological_entity id="o12441" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s3" to="5" to_inclusive="false" />
        <character constraint="at tip" constraintid="o12442" name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="additional" value_original="additional" />
        <character constraint="from tip" constraintid="o12444" is_modifier="false" name="arrangement" src="d0_s3" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o12442" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o12443" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o12444" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o12445" name="1/10-1/3(-1/2)" name_original="1/10-1/3(-1/2)" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o12446" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity constraint="largest" id="o12447" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate-oblong" value_original="oblanceolate-oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12448" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o12449" from="1/2" name="quantity" src="d0_s3" to="2/3-3/4" />
      </biological_entity>
      <biological_entity id="o12449" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o12450" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="4" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o12451" from="4" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity id="o12451" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o12452" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="strongly" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character is_modifier="true" modifier="sometimes" name="arrangement" src="d0_s3" value="separate" value_original="separate" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" modifier="usually" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12453" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; usually" name="coloration" src="d0_s3" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o12454" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o12455" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o12456" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12457" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12458" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o12459" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12460" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o12442" id="r774" name="part_of" negation="false" src="d0_s3" to="o12443" />
      <relation from="o12441" id="r775" modifier="by 3-20 mm" name="on" negation="false" src="d0_s3" to="o12445" />
      <relation from="o12445" id="r776" name="part_of" negation="false" src="d0_s3" to="o12446" />
      <relation from="o12450" id="r777" modifier="usually; usually strongly; strongly" name="touching to" negation="false" src="d0_s3" to="o12452" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 1–3.</text>
      <biological_entity constraint="cauline" id="o12461" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–20 (–30) -flowered.</text>
      <biological_entity id="o12462" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-20(-30)-flowered" value_original="3-20(-30)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.5–2 cm (proximal to 3 cm).</text>
      <biological_entity id="o12463" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets narrowly to broadly lanceolate, 2–5 (–6) × 1–1.5 mm;</text>
      <biological_entity id="o12464" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o12465" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 4–7 mm, apex acute to acuminate;</text>
      <biological_entity id="o12466" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12467" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12468" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 4–8 × 4–8 mm;</text>
      <biological_entity id="o12469" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12470" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments (0.5–) 1–2 mm, anthers 0.3–0.8 mm;</text>
      <biological_entity id="o12471" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12472" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12473" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels 15–30, styles filiform to filiform-tapered, ± papillate-swollen in less than proximal 1/5, 1.5–2 mm.</text>
      <biological_entity id="o12474" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12475" name="carpel" name_original="carpels" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
      <biological_entity id="o12476" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s11" to="filiform-tapered" />
        <character is_modifier="false" modifier="more or less; less" name="shape" src="d0_s11" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12477" name="1/5" name_original="1/5" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Achenes 1.2–1.6 mm.</text>
      <biological_entity id="o12478" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra and meadows, boulder piles, gravelly slopes, stabilized talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine tundra" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="boulder piles" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="talus" modifier="stabilized" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3400–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla subjuga is centered in the high mountains of Colorado and barely enters New Mexico in the Sangre de Cristo Mountains. Collections from Alberta also apparently belong to this species, but all known collections from Wyoming have been identified as different taxa, at least one currently undescribed. At its most distinctive, P. subjuga is easily recognized by its unique leaf division, with five palmate leaflets subtended by an additional pair (or two) of lateral leaflets. Southern populations, however, are more likely to have only three apical leaflets. The leaflets tend to be strongly bicolored with overlapping teeth, in contrast to most sympatric pinnate species. Petiole vestiture is also distinctive in being seasonally dimorphic, with long hairs on first-formed leaves spreading to ascending and those on later formed leaves tightly appressed, as well as more conspicuously verrucose. Unresolved infraspecific variation exists, and field observations suggest that P. subjuga readily hybridizes with sympatric species, creating a swarm of intermediate specimens.</discussion>
  
</bio:treatment>