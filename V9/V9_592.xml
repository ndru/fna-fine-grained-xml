<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Ehrhart" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) McVaugh" date="1951" rank="variety">rufula</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>7: 307. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species serotina;variety rufula</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100715</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Padus</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">rufula</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 132. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Padus;species rufula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prunus</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) Shreve" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) McVaugh" date="unknown" rank="subspecies">virens</taxon_name>
    <taxon_hierarchy>genus Prunus;species serotina;subspecies virens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">serotina</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) McVaugh" date="unknown" rank="variety">virens</taxon_name>
    <taxon_hierarchy>genus P.;species serotina;variety virens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virens</taxon_name>
    <taxon_hierarchy>genus P.;species virens</taxon_hierarchy>
  </taxon_identification>
  <number>6c.</number>
  <other_name type="common_name">Southwestern black cherry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Twigs glabrous or hairy, hairs rusty brown or gray.</text>
      <biological_entity id="o3126" name="twig" name_original="twigs" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3127" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="rusty brown" value_original="rusty brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (2–) 4–10 (–15) mm, glabrous or sparsely to densely hairy, sometimes glandular at petiole-blade junction;</text>
      <biological_entity id="o3128" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o3129" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s1" to="sparsely densely hairy" />
        <character constraint="at petiole-blade junction" constraintid="o3130" is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="petiole-blade" id="o3130" name="junction" name_original="junction" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade usually elliptic or oblong, rarely ovate or obovate, 2–5.2 (–7.4 on long-shoots) × 1.1–2.6 (–3.8) cm, leathery, apex usually acute, rarely abruptly acute, abaxial surface usually densely hairy along midribs proximally, sometimes glabrous.</text>
      <biological_entity id="o3131" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3132" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="5.2" to_unit="cm" />
        <character char_type="range_value" from="2.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="3.8" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s2" to="2.6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o3133" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely abruptly" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3134" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character constraint="along midribs" constraintid="o3135" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3135" name="midrib" name_original="midribs" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: rachises (25–) 35–70 (–102) mm.</text>
      <biological_entity id="o3136" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o3137" name="rachis" name_original="rachises" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="102" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams, moist slopes in canyons, mixed oak-pine-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="canyons" modifier="slopes in" />
        <character name="habitat" value="mixed oak-pine-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Baja California, Chihuahua, Coahuila, Durango, Guanajuato, Guerrero, Jalisco, San Luis Potosí, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Guanajuato)" establishment_means="native" />
        <character name="distribution" value="Mexico (Guerrero)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Black cherries of the semiarid southwestern mountains and canyons (var. rufula) have generally smaller, thicker leaf blades on shorter petioles than those of the eastern United States and southeastern Canada. The southwestern plants vary in the degree of indument (hairs often rusty, sometimes lighter on the branchlets and axes of the racemes, and on petioles and along midribs abaxially). The glabrous plants have been segregated by some botanists as var. virens; the variation in indument is nearly continuous, and the plants are otherwise similar to hairy specimens.</discussion>
  
</bio:treatment>