<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">602</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Rotundifoliae</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="species">margarettae</taxon_name>
    <taxon_name authority="(Britton) Sargent" date="unknown" rank="variety">brownii</taxon_name>
    <place_of_publication>
      <place_in_publication>3: 199. 1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series rotundifoliae;species margarettae;variety brownii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100568</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">brownii</taxon_name>
    <place_of_publication>
      <place_in_publication>1: 447. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;species brownii</taxon_hierarchy>
  </taxon_identification>
  <number>106c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: blade broadly elliptic, obovate to ovate, or suborbiculate, 3–5 cm, base broadly cuneate, lobe sinuses shallow, apex obtuse to subacute.</text>
      <biological_entity id="o21946" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o21947" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s0" to="ovate or suborbiculate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s0" to="ovate or suborbiculate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21948" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o21949" name="sinuse" name_original="sinuses" src="d0_s0" type="structure">
        <character is_modifier="false" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o21950" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s0" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers 8–10 mm diam.</text>
      <biological_entity id="o21951" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pomes ellipsoid.</text>
      <biological_entity id="o21952" name="pome" name_original="pomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hedgerows, thickets, wood margins, dry areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hedgerows" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="wood margins" />
        <character name="habitat" value="dry areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., Mo., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">margaretta</other_name>
  <discussion>Variety brownii is sporadic in occurrence. It is notable for having small flowers. Reports from Indiana and West Virginia need validation.</discussion>
  
</bio:treatment>