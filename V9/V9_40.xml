<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="mention_page">35</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">arcticus</taxon_name>
    <taxon_name authority="(Smith) B. Boivin" date="1955" rank="subspecies">stellatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Bot. France</publication_title>
      <place_in_publication>102: 235. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species arcticus;subspecies stellatus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100521</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rubus</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">stellatus</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Icon. Ined.</publication_title>
      <place_in_publication>3: plate 64. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rubus;species stellatus</taxon_hierarchy>
  </taxon_identification>
  <number>2c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 1.6 dm.</text>
      <biological_entity id="o26945" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.6" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often with soft, spreading hairs.</text>
      <biological_entity id="o26946" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o26947" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o26946" id="r1770" name="with" negation="false" src="d0_s1" to="o26947" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves simple, usually 3-lobed, rarely unlobed;</text>
      <biological_entity id="o26948" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole hairy;</text>
      <biological_entity id="o26949" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ± rotund, 2–4.5 cm (usually broader than long), base cordate to subcordate, margins doubly serrate, lobe apices usually obtuse.</text>
      <biological_entity id="o26950" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="rotund" value_original="rotund" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26951" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o26952" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o26953" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: flowers solitary.</text>
      <biological_entity id="o26954" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o26955" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels sparsely short-stipitate-glandular, sometimes eglandular.</text>
      <biological_entity id="o26956" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals caudate, hairy, sparsely to moderately stipitate-glandular;</text>
      <biological_entity id="o26957" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26958" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals pink to magenta, oblanceolate to obovate, 10–22 mm, apex emarginate.</text>
      <biological_entity id="o26959" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26960" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s8" to="magenta" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 14.</text>
      <biological_entity id="o26961" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26962" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine stream banks and meadows, lake shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine stream banks" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="lake shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>