<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>W. Wayt Thomas</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">3</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">3</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Fernando &amp; Quinn" date="unknown" rank="family">Picramniaceae</taxon_name>
    <taxon_hierarchy>family Picramniaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20479</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, rarely subshrubs, dioecious.</text>
      <biological_entity id="o21495" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" notes="" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="tree" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually single, wood dense.</text>
      <biological_entity id="o21498" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o21499" name="wood" name_original="wood" src="d0_s1" type="structure">
        <character is_modifier="false" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually persistent, cauline, alternate, imparipinnately compound;</text>
      <biological_entity id="o21500" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o21501" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="imparipinnately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o21502" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o21503" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually chartaceous, margins entire, leaflets alternate or subopposite (often darkening upon drying), bases of lateral leaflets often oblique.</text>
      <biological_entity id="o21504" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o21505" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21506" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="subopposite" value_original="subopposite" />
      </biological_entity>
      <biological_entity id="o21507" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21508" name="leaflet" name_original="leaflets" src="d0_s5" type="structure" />
      <relation from="o21507" id="r1397" name="part_of" negation="false" src="d0_s5" to="o21508" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or lateral, simple racemes (either of separate flowers or glomerules of flowers), pendent, or compound thyrses.</text>
      <biological_entity id="o21509" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o21510" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o21511" name="thyrse" name_original="thyrses" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, perianth and androecium hypogynous;</text>
      <biological_entity id="o21512" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o21513" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o21514" name="androecium" name_original="androecium" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>epicalyx bractlet absent;</text>
      <biological_entity constraint="epicalyx" id="o21515" name="bractlet" name_original="bractlet" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium absent;</text>
      <biological_entity id="o21516" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals [3–] 5, basally connate or distinct;</text>
      <biological_entity id="o21517" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals [3–] 5, sometimes 0, distinct, stramineous to maroon or red, usually ligulate;</text>
      <biological_entity id="o21518" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character modifier="sometimes" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s11" to="maroon or red" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="ligulate" value_original="ligulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens or staminodia (if present) (3–) 5, distinct, antipetalous, anthers longitudinally dehiscent;</text>
      <biological_entity id="o21519" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="position" src="d0_s12" value="antipetalous" value_original="antipetalous" />
      </biological_entity>
      <biological_entity id="o21520" name="staminodium" name_original="staminodia" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="position" src="d0_s12" value="antipetalous" value_original="antipetalous" />
      </biological_entity>
      <biological_entity id="o21521" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminate flowers: (filaments unappendaged) intrastaminal discs or, rarely, column present, pistillodia usually conic and hairy or vestigial;</text>
      <biological_entity id="o21522" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="intrastaminal" id="o21523" name="disc" name_original="discs" src="d0_s13" type="structure" />
      <biological_entity id="o21524" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21525" name="pistillodium" name_original="pistillodia" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="conic" value_original="conic" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistillate flowers: staminodia present or absent;</text>
      <biological_entity id="o21526" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21527" name="staminodium" name_original="staminodia" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 1, 2–3 (–4) -carpellate, ovary borne on disc or gynophore, placentation apical, styles 2–3 (–4), usually ± lacking, stigmas sessile and divergent on apices of ovaries;</text>
      <biological_entity id="o21528" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21529" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-3(-4)-carpellate" value_original="2-3(-4)-carpellate" />
      </biological_entity>
      <biological_entity id="o21530" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s15" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o21531" name="disc" name_original="disc" src="d0_s15" type="structure" />
      <biological_entity id="o21532" name="gynophore" name_original="gynophore" src="d0_s15" type="structure" />
      <biological_entity id="o21533" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="3" />
        <character is_modifier="false" modifier="usually more or less" name="quantity" src="d0_s15" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o21534" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character constraint="on apices" constraintid="o21535" is_modifier="false" name="arrangement" src="d0_s15" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o21535" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o21536" name="ovary" name_original="ovaries" src="d0_s15" type="structure" />
      <relation from="o21530" id="r1398" name="borne on" negation="false" src="d0_s15" to="o21531" />
      <relation from="o21530" id="r1399" name="borne on" negation="false" src="d0_s15" to="o21532" />
      <relation from="o21535" id="r1400" name="part_of" negation="false" src="d0_s15" to="o21536" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 2 or 3 per locule.</text>
      <biological_entity id="o21537" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21538" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit="or per" value="2" value_original="2" />
        <character name="quantity" src="d0_s16" unit="or per" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21539" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits berries or samaras;</text>
      <biological_entity constraint="fruits" id="o21540" name="berry" name_original="berries" src="d0_s17" type="structure" />
      <biological_entity constraint="fruits" id="o21541" name="samara" name_original="samaras" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>sepals often persistent;</text>
      <biological_entity id="o21542" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas persistent.</text>
      <biological_entity id="o21543" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1, not arillate.</text>
      <biological_entity id="o21544" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s20" value="arillate" value_original="arillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 3, species 52 (2 genera, 2 species in the flora).</discussion>
  <discussion>The length of the pedicel is taken from fruiting material for Alvaradoa and Picramnia. The third genus, Nothotalisia W. W. Thomas, is known from Panama and northwestern South America with three species.</discussion>
  <references>
    <reference>Thomas, W. W. 1990. The American genera of Simaroubaceae and their distribution. Acta Bot. Brasil. 4: 11–18.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits samaras; leaflets elliptic, 8–33 × 3–9 mm, apices rounded.</description>
      <determination>1 Alvaradoa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits berries; leaflets ovate to narrowly elliptic, 14–300 × 8–160 mm, apices usually acute to acuminate.</description>
      <determination>2 Picramnia</determination>
    </key_statement>
  </key>
</bio:treatment>