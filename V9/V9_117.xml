<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="illustration_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="(Cockerell) Cockerell ex Rehder" date="1940" rank="subgenus">hesperhodos</taxon_name>
    <taxon_name authority="Crépin" date="1889" rank="section">Minutifoliae</taxon_name>
    <taxon_name authority="Wooton" date="1898" rank="species">stellata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">stellata</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus hesperhodos;section minutifoliae;species stellata;subspecies stellata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100512</other_info_on_name>
  </taxon_identification>
  <number>2a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 4–15 dm;</text>
      <biological_entity id="o2483" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>distal branches tomentose-woolly, with stellate hairs, sessile and stipitate-glandular;</text>
      <biological_entity id="o2485" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o2484" id="r154" name="with" negation="false" src="d0_s1" to="o2485" />
    </statement>
    <statement id="d0_s2">
      <text>infrastipular prickles single or paired, largest flattened, 11–13 × 2.5–6 mm, internodal prickles and aciculi sparse, larger, eglandular.</text>
      <biological_entity constraint="distal" id="o2484" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose-woolly" value_original="tomentose-woolly" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o2486" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
        <character is_modifier="false" name="size" src="d0_s2" value="largest" value_original="largest" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s2" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o2487" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="size" src="d0_s2" value="larger" value_original="larger" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules 4–8 × 1–2 mm, margins ± entire, eglandular or glands sparse, auricles foliaceous;</text>
      <biological_entity id="o2488" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2489" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2490" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o2491" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o2492" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole and rachis pubescent, sometimes glabrous;</text>
      <biological_entity id="o2493" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2494" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2495" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3, terminal: petiolule 0.5–1.5 mm, blade obovate, margins broadly 1-crenate or multi-crenate, teeth 3–6 per side, abaxial surfaces tomentulose on midveins, otherwise glabrous.</text>
      <biological_entity id="o2496" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2497" name="petiolule" name_original="petiolule" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2498" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o2499" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="1-crenate" value_original="1-crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="multi-crenate" value_original="multi-crenate" />
      </biological_entity>
      <biological_entity id="o2500" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o2501" from="3" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o2501" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o2502" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character constraint="on midveins" constraintid="o2503" is_modifier="false" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2503" name="midvein" name_original="midveins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 4.5 cm diam.;</text>
      <biological_entity id="o2504" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character name="diameter" src="d0_s6" unit="cm" value="4.5" value_original="4.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium densely setose, setae 1–3 mm;</text>
      <biological_entity id="o2505" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o2506" name="seta" name_original="setae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 12–17 × 5 mm;</text>
      <biological_entity id="o2507" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pink.</text>
      <biological_entity id="o2508" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Hips subglobose or cupulate, 7–13 mm diam., sparsely or densely setose.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 14.</text>
      <biological_entity id="o2509" name="hip" name_original="hips" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s10" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; densely" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2510" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streams, roadsides, openings in pine-juniper woods, dry rocky hillsides and slopes, overhangs in rocky canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="openings" constraint="in pine-juniper woods" />
        <character name="habitat" value="pine-juniper woods" />
        <character name="habitat" value="dry rocky hillsides" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="overhangs" constraint="in rocky canyons" />
        <character name="habitat" value="rocky canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies stellata is known from the Organ and San Andres mountains in Doña Ana County of south-central New Mexico (W. H. Lewis 1965).</discussion>
  
</bio:treatment>