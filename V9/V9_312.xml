<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Rubricaules</taxon_name>
    <taxon_name authority="Lehmann" date="1830" rank="species">rubricaulis</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Stirp. Pug.</publication_title>
      <place_in_publication>2: 11. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section rubricaules;species rubricaulis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100359</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">dissecta</taxon_name>
    <taxon_name authority="(Lehmann) Rydberg" date="unknown" rank="variety">rubricaulis</taxon_name>
    <taxon_hierarchy>genus Potentilla;species dissecta;variety rubricaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nivea</taxon_name>
    <taxon_name authority="(Lehmann) Hiitonen" date="unknown" rank="subspecies">rubricaulis</taxon_name>
    <taxon_hierarchy>genus P.;species nivea;subspecies rubricaulis</taxon_hierarchy>
  </taxon_identification>
  <number>83.</number>
  <other_name type="common_name">Red-stemmed cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o574" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="with leaves" constraintid="o575" is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o575" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to nearly erect, 1.5–4 dm.</text>
      <biological_entity id="o576" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="nearly erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves often both ternate and palmate on same plant, rarely subpalmate, 4–10 cm;</text>
      <biological_entity constraint="basal" id="o577" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="ternate" value_original="ternate" />
        <character constraint="on plant" constraintid="o578" is_modifier="false" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s2" value="subpalmate" value_original="subpalmate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o578" name="plant" name_original="plant" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 2.5–7 cm, long hairs sparse to common, loosely appressed to ascending-spreading, 1–2 mm, ± weak to stiff, verrucose, short and/or ± crisped hairs common to abundant, cottony hairs absent, glands usually sparse;</text>
      <biological_entity id="o579" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o580" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character char_type="range_value" from="loosely appressed" name="orientation" src="d0_s3" to="ascending-spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="less weak" name="fragility" src="d0_s3" to="stiff" />
        <character is_modifier="false" name="relief" src="d0_s3" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o581" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s3" to="abundant" />
      </biological_entity>
      <biological_entity id="o582" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o583" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3–5, proximalmost separated by 0 (–1) mm, central oblong to obovate, 1.5–4 × 1–2.5 cm, petiolules 0–5 mm, distal 2/3–3/4 of margin incised 1/2–3/4, rarely +, to midvein, teeth (4–) 5–8 per side, 4–5 mm, apical tufts 1 mm, abaxial surfaces gray to grayish white, long hairs abundant, cottony-crisped hairs usually dense, short hairs and glands absent or obscured, adaxial green to grayish green, long hairs sparse to common, 0.5–1.5 mm, stiff, short hairs absent or sparse, rarely common, crisped and cottony hairs absent, glands absent or sparse, rarely common.</text>
      <biological_entity id="o584" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o585" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character constraint="by central leaflets" constraintid="o586" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" notes="" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o586" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="true" name="some_measurement" src="d0_s4" unit="mm" value="0" value_original="0" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="obovate" />
      </biological_entity>
      <biological_entity id="o587" name="petiolule" name_original="petiolules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="of margin" constraintid="o588" from="2/3" name="quantity" src="d0_s4" to="3/4" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o588" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o589" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o590" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o591" from="5" name="quantity" src="d0_s4" to="8" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o591" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="apical" id="o592" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o593" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s4" to="grayish white" />
      </biological_entity>
      <biological_entity id="o594" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o595" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o596" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o597" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o598" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="grayish green" />
      </biological_entity>
      <biological_entity id="o599" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o600" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s4" value="common" value_original="common" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o601" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o602" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s4" value="common" value_original="common" />
      </biological_entity>
      <relation from="o587" id="r32" modifier="rarely" name="to" negation="false" src="d0_s4" to="o589" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 2–3.</text>
      <biological_entity constraint="cauline" id="o603" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 4–20-flowered, open, branch angle (10–) 20–45°.</text>
      <biological_entity id="o604" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="4-20-flowered" value_original="4-20-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="branch" id="o605" name="angle" name_original="angle" src="d0_s6" type="structure">
        <character name="degree" src="d0_s6" value="(10-)20-45°" value_original="(10-)20-45°" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–3 cm, proximal to 5 cm.</text>
      <biological_entity id="o606" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o607" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: epicalyx bractlets linear to narrowly lanceolate, 3–4 × 0.8–1.2 mm;</text>
      <biological_entity id="o608" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="epicalyx" id="o609" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="narrowly lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 4–6 mm diam.;</text>
      <biological_entity id="o610" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o611" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4–5 mm, apex subacute to acute, glands usually ± sparse, not obscured;</text>
      <biological_entity id="o612" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o613" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o614" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
      <biological_entity id="o615" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pale-yellow, not overlapping, 5–7 × (4–) 5–6.5 mm, distinctly longer than sepals;</text>
      <biological_entity id="o616" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o617" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s11" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s11" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="6.5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o618" is_modifier="false" name="length_or_size" src="d0_s11" value="distinctly longer" value_original="distinctly longer" />
      </biological_entity>
      <biological_entity id="o618" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 0.5–1.5 mm, anthers 0.4 mm;</text>
      <biological_entity id="o619" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o620" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o621" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>carpels 30–60, styles 0.9–1.1 mm.</text>
      <biological_entity id="o622" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o623" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
      <biological_entity id="o624" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 1.2 mm. 2n = 56.</text>
      <biological_entity id="o625" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.2" value_original="1.2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o626" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy lake and stream shores, open sandy forests, dry grassy slopes, sandy and loamy bluffs, rock crevices, scree</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy lake" />
        <character name="habitat" value="stream shores" />
        <character name="habitat" value="open sandy forests" />
        <character name="habitat" value="dry grassy slopes" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="loamy bluffs" />
        <character name="habitat" value="rock crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Sask., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As addressed by B. Ertter et al. (2013), the name Potentilla rubricaulis is here restricted to relatively large plants with open inflorescences occurring mainly in glaciated parts of subarctic northwestern Canada and Alaska. Plants from the Chugach Mountains of southern Alaska tend to be more conspicuously glandular than elsewhere.</discussion>
  <discussion>The distinction between Potentilla rubricaulis and large forms of P. arenosa with supernumerary leaflets is problematic. Although both species have somewhat similar petiole vestiture (long, straight, verrucose hairs and a layer of short, stiff, or curly hairs), the latter species tends to have more stiffly spreading petiole hairs and prominently petiolulate central leaflets.</discussion>
  <discussion>The octoploid chromosome count (P. M. Dansereau and E. Steiner 1956) from Great Bear Lake area, Northwest Territories, probably belongs to Potentilla rubricaulis in the narrow sense, since that is its type locality.</discussion>
  
</bio:treatment>