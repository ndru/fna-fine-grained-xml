<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">spiraea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">tomentosa</taxon_name>
    <taxon_name authority="(Rafinesque) Fernald" date="1912" rank="variety">rosea</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>14: 190. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus spiraea;species tomentosa;variety rosea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065736</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">rosea</taxon_name>
    <place_of_publication>
      <publication_title>New Fl.</publication_title>
      <place_in_publication>3: 62. 1838 (as Spirea)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species rosea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">ferruginea</taxon_name>
    <taxon_hierarchy>genus S.;species ferruginea</taxon_hierarchy>
  </taxon_identification>
  <number>3b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences 150–10,000-flowered, flowers or fruits 6–11 per cm of branches.</text>
      <biological_entity id="o23223" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s0" to="10" />
      </biological_entity>
      <biological_entity id="o23224" name="flower" name_original="flowers" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="000-flowered" value_original="000-flowered" />
      </biological_entity>
      <biological_entity id="o23225" name="fruit" name_original="fruits" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="000-flowered" value_original="000-flowered" />
        <character char_type="range_value" constraint="per cm" constraintid="o23226" from="6" name="quantity" src="d0_s0" to="11" />
      </biological_entity>
      <biological_entity id="o23226" name="cm" name_original="cm" src="d0_s0" type="structure" />
      <biological_entity id="o23227" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <relation from="o23226" id="r1509" name="part_of" negation="false" src="d0_s0" to="o23227" />
    </statement>
    <statement id="d0_s1">
      <text>Pedicels 0.5–1.5 mm.</text>
      <biological_entity id="o23228" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep; fruiting late Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="late Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wetlands, poor fens, wet meadows, bogs, woodland borders with moist acidic soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wetlands" />
        <character name="habitat" value="poor fens" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="borders" modifier="woodland" constraint="with moist acidic soil" />
        <character name="habitat" value="moist acidic soil" modifier="with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., P.E.I., Que.; Ala., Ark., Ga., Ill., Ind., Kans., Ky., La., Md., Mich., Minn., Miss., Mo., N.J., N.Y., N.C., Ohio, Oreg., Pa., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>