<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="illustration_page">204</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Niveae</taxon_name>
    <taxon_name authority="Jurtzev in A. I. Tolmatchew" date="1984" rank="species">subvahliana</taxon_name>
    <place_of_publication>
      <publication_title>in A. I. Tolmatchew, Fl. Arct. URSS</publication_title>
      <place_in_publication>9(1): 319. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section niveae;species subvahliana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100367</other_info_on_name>
  </taxon_identification>
  <number>80.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually cushion-forming.</text>
      <biological_entity id="o7141" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cushion-forming" value_original="cushion-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudex branches stout, columnar, sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o7142" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s1" value="columnar" value_original="columnar" />
        <character constraint="with leaves" constraintid="o7143" is_modifier="false" name="architecture" src="d0_s1" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o7143" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, 0.2–0.6 (–1.1) dm, lengths 2–4 times basal leaves.</text>
      <biological_entity id="o7144" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.1" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="0.6" to_unit="dm" />
        <character constraint="leaf" constraintid="o7145" is_modifier="false" name="length" src="d0_s2" value="2-4 times basal leaves" value_original="2-4 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7145" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 0.5–2.5 (–3) cm;</text>
      <biological_entity constraint="basal" id="o7146" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–1.5 (–2) cm, long hairs common to abundant, spreading to ascending, 1 (–2) mm, ± weak, rarely stiff, smooth, crisped/short-cottony hairs usually absent, sometimes sparse, glands sparse to common;</text>
      <biological_entity id="o7147" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7148" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="ascending" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s4" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="rarely" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o7149" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="count_or_density" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o7150" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets separate to slightly overlapping, central obovate, 0.5–1.2 (–1.5) × 0.4–0.9 (–1) cm, subsessile to short-petiolulate, base cuneate, margins revolute, distal 1/2 incised 1/2–3/4 to midvein, teeth (1–) 2–3 (–4) per side, ± distant, surfaces ± dissimilar, abaxial grayish or yellowish white, long hairs 0.5–1 mm, cottony-crisped hairs ± dense (often obscured by long hairs), adaxial dark green to greenish gray, long hairs ± abundant, other hairs absent.</text>
      <biological_entity id="o7151" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="separate" name="arrangement" src="d0_s5" to="slightly overlapping" />
      </biological_entity>
      <biological_entity constraint="central" id="o7152" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="0.9" to_unit="cm" />
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s5" to="short-petiolulate" />
      </biological_entity>
      <biological_entity id="o7153" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7154" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o7155" from="1/2" name="quantity" src="d0_s5" to="3/4" />
      </biological_entity>
      <biological_entity id="o7155" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o7156" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s5" to="2" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" constraint="per side" constraintid="o7157" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="distant" value_original="distant" />
      </biological_entity>
      <biological_entity id="o7157" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o7158" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o7159" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish white" value_original="yellowish white" />
      </biological_entity>
      <biological_entity id="o7160" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7161" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7162" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s5" to="greenish gray" />
      </biological_entity>
      <biological_entity id="o7163" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s5" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o7164" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (0–) 1 (–2).</text>
      <biological_entity constraint="cauline" id="o7165" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1 (–2) -flowered.</text>
      <biological_entity id="o7166" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1(-2)-flowered" value_original="1(-2)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–2 (–3) cm in flower, to 5.5 cm in fruit.</text>
      <biological_entity id="o7167" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" constraint="in flower" constraintid="o7168" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o7169" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7168" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o7169" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets lanceolate to elliptic-ovate, rarely linear, 3–6 (–7) × 0.8–2 (–2.3) mm, (1/2–) 2/3 to nearly as wide as sepals, margins flat or ± revolute, red-glands absent;</text>
      <biological_entity id="o7170" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o7171" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="elliptic-ovate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" constraint="to sepals" constraintid="o7172" from="1/2" name="quantity" src="d0_s9" to="2/3" />
      </biological_entity>
      <biological_entity id="o7172" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o7173" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o7174" name="red-gland" name_original="red-glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 2.5–4 mm diam.;</text>
      <biological_entity id="o7175" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7176" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 3–5 (–6) mm, apex obtuse to subacute;</text>
      <biological_entity id="o7177" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7178" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7179" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 4–8 (–9) × 4–9 mm, longer than sepals;</text>
      <biological_entity id="o7180" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7181" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
        <character constraint="than sepals" constraintid="o7182" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7182" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 1–1.3 mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o7183" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7184" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7185" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 25–35, apical hairs absent, styles conic-columnar, ± papillate-swollen on less than proximal 1/5, 0.9–1.1 mm.</text>
      <biological_entity id="o7186" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7187" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s14" to="35" />
      </biological_entity>
      <biological_entity constraint="apical" id="o7188" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7189" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic-columnar" value_original="conic-columnar" />
        <character is_modifier="false" modifier="more or less; less" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7190" name="1/5" name_original="1/5" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.2–1.6 mm. 2n = 28 (Russian Far East).</text>
      <biological_entity id="o7191" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7192" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring to summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky alpine heaths, rock outcrops and crevices, scree and talus, dry tundra, coastal bluffs, stabilized sand dunes, usually on calcareous bedrock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky alpine heaths" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="scree" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="dry tundra" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="sand dunes" modifier="stabilized" />
        <character name="habitat" value="calcareous bedrock" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.W.T., Nunavut, Que., Yukon; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The major part of the range assigned to Potentilla vahliana by E. Hultén (1968) belongs to P. subvahliana.</discussion>
  <discussion>Two morphologic types are present within what is accepted here as Potentilla subvahliana. Plants corresponding to the type of P. subvahliana (Wrangel Island) are widespread throughout northeastern Asia (Chukotka) and arctic North America to northwestern Greenland. Some plants in alpine Alaska, Yukon, and east to Amundsen Gulf, Nunavut, are redder, more densely columnar, and less hairy, with smaller leaves having fewer and narrower lobes, more slender one-flowered stems, narrow and entire bracts, narrower sepals, and much narrower epicalyx bractlets. Potentilla subvahliana is tetraploid, perhaps an allopolyploid, and could consist of lineages from different parental combinations.</discussion>
  
</bio:treatment>