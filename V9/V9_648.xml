<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">388</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Focke in H. G. A. Engler and K. Prantl" date="1888" rank="tribe">kerrieae</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="genus">neviusia</taxon_name>
    <taxon_name authority="A. Gray" date="1859" rank="species">alabamensis</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>6: 374, plate 30. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe kerrieae;genus neviusia;species alabamensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220009234</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Alabama snow-wreath</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole 2–9 mm;</text>
      <biological_entity id="o322" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o323" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s0" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade ovate to ovatelanceolate, 3–7 × 2–4.5 cm, base attenuate to obtuse, margins doubly serrate, some distal leaves only finely serrate, apex acuminate to acute, surfaces subsericeous.</text>
      <biological_entity id="o324" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o325" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="ovatelanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s1" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o326" name="base" name_original="base" src="d0_s1" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o327" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o328" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="only finely" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o329" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s1" to="acute" />
      </biological_entity>
      <biological_entity id="o330" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="subsericeous" value_original="subsericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels 13–32 mm.</text>
      <biological_entity id="o331" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="32" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals ovate to lanceolate or elliptic, 5–9 (–12) × 3–5 mm;</text>
      <biological_entity id="o332" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o333" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate or elliptic" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 0;</text>
      <biological_entity id="o334" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o335" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 100+;</text>
      <biological_entity id="o336" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o337" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>carpels 2–5, styles 4.5–6 mm. 2n = 18.</text>
      <biological_entity id="o338" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o339" name="carpel" name_original="carpels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o340" name="style" name_original="styles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o341" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–mid spring; fruiting late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid spring" from="early" />
        <character name="fruiting time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Steep limestone, sandstone, and shale slopes with little soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="steep limestone" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="shale" />
        <character name="habitat" value="little soil" modifier="slopes with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Miss., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Neviusia alabamensis has been recommended for federal listing under the Endangered Species Act. The species is known currently from five southeastern states; a population known earlier from Poplar Bluff, Missouri, has been extirpated (D. M. Moore 1956).</discussion>
  <discussion>The disjunct populations appear to be relicts from a former more widespread distribution and perhaps were associated with mesophytic forests during the Tertiary (A. A. Long 1989). It also has been suggested that Neviusia alabamensis is an epibiotic taxon that survived the Mississippi embayment since the late Paleozoic or early Mesozoic (D. M. Moore 1956; D. D. Horn and P. Somers 1981).</discussion>
  <discussion>Neviusia alabamensis has been cultivated as far north as Boston and Chicago since its discovery (J. R. Shevock et al. 1992). It has been misidentified as Physocarpus opulifolius (D. M. Moore 1956).</discussion>
  
</bio:treatment>