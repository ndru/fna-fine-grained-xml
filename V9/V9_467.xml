<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="(Rydberg) Rydberg" date="1898" rank="species">pseudorupestris</taxon_name>
    <taxon_name authority="Ertter" date="2007" rank="variety">saxicola</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 37, figs. 1M–R. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species pseudorupestris;variety saxicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100628</other_info_on_name>
  </taxon_identification>
  <number>9b.</number>
  <other_name type="common_name">Cliff drymocallis or wood beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.6–2.5 dm, base 1–2 (–3) mm diam., short hairs sparse to moderately abundant, sometimes absent.</text>
      <biological_entity id="o9829" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o9830" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9831" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s0" to="moderately abundant" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves 3–9 (–15) cm, sparsely to densely hairy (hairs to 1.5 mm), usually densely peglike-glandular, not bristly;</text>
      <biological_entity constraint="basal" id="o9832" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="9" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually densely" name="architecture_or_function_or_pubescence" src="d0_s1" value="peglike-glandular" value_original="peglike-glandular" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflet pairs (2–) 3 (–4);</text>
      <biological_entity id="o9833" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="4" />
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet broadly obovate-cuneate, 0.8–2 (–4) × 0.7–2 (–3) cm, teeth single or ± double, 3–8 (–12) per side.</text>
      <biological_entity constraint="terminal" id="o9834" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="obovate-cuneate" value_original="obovate-cuneate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9835" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character name="quantity" src="d0_s3" value="more or less" value_original="more or less" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="12" />
        <character char_type="range_value" constraint="per side" constraintid="o9836" from="3" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
      <biological_entity id="o9836" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 3–15 (proximal to 20) mm, usually not bristly, short hairs sparse to moderately abundant (sometimes absent).</text>
      <biological_entity id="o9837" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="usually not" name="pubescence" src="d0_s4" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o9838" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="moderately abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers (2–) 3–12 (–20);</text>
      <biological_entity id="o9839" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="20" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hypanthia and sepals not bristly or bristles less than 1 mm;</text>
      <biological_entity id="o9840" name="hypanthium" name_original="hypanthia" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o9841" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o9842" name="bristle" name_original="bristles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets linear-lanceolate to broadly elliptic, 2–5 × 1–1.5 mm;</text>
      <biological_entity constraint="epicalyx" id="o9843" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="broadly elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 4–6 (–7) mm;</text>
      <biological_entity id="o9844" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals ± overlapping, not red-tinged, narrowly to broadly obovate, 4–8 (–9) × 3–6 (–8) mm;</text>
      <biological_entity id="o9845" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="red-tinged" value_original="red-tinged" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 1–2.5 mm;</text>
      <biological_entity id="o9846" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles usually golden brown, rarely reddish.</text>
      <biological_entity id="o9847" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden brown" value_original="golden brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs, ledges, outcrops, ridges, talus slopes, lava beds, other rocky habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="lava beds" />
        <character name="habitat" value="other rocky habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety saxicola accommodates the bulk of specimens previously placed in Potentilla glandulosa var. pseudorupestris, minus the extremes at the northeastern and southern ends of the species range. The circumscription here encompasses significant heterogeneity, which might be resolved into additional taxa upon further analysis [for example, the dwarfed high elevation form of P. glandulosa noted by N. H. Holmgren (1997b)]. Some collections from Alberta and Washington have petals as large as those of var. pseudorupestris but are here included in var. saxicola on the basis of stature. Plants from southwestern Idaho and southeastern Oregon combine features of D. lactea and D. pseudorupestris, some being atypically tall (to 5.5 dm) but with the vestiture and saxicolous preference of var. saxicola. In contrast, populations on Steens Mountain, Oregon, including the type of D. pumila Rydberg, have the typical habit of var. saxicola but the vestiture of D. lactea; their optimal placement is unresolved. In California, var. saxicola occurs in the Cascade Range (Mount Lassen, Mount Shasta) and extends sporadically south through the Sierra Nevada to Tulare County, intergrading with var. crumiana and D. lactea.</discussion>
  
</bio:treatment>