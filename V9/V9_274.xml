<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">Subviscosae</taxon_name>
    <taxon_name authority="(Munz &amp; I. M. Johnston) Ertter" date="1992" rank="species">rimicola</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>71: 420. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section subviscosae;species rimicola;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250100356</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">wheeleri</taxon_name>
    <taxon_name authority="Munz &amp; I. M. Johnston" date="unknown" rank="variety">rimicola</taxon_name>
    <place_of_publication>
      <publication_title>Bull. S. Calif. Acad. Sci.</publication_title>
      <place_in_publication>24: 19. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species wheeleri;variety rimicola</taxon_hierarchy>
  </taxon_identification>
  <number>56.</number>
  <other_name type="common_name">Cliff cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Short hairs not well differentiated from long hairs, absent or sparse throughout.</text>
      <biological_entity id="o11047" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character constraint="from hairs" constraintid="o11048" is_modifier="false" modifier="not well" name="variability" src="d0_s0" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="throughout" name="quantity" src="d0_s0" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o11048" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.4–2 (–3) dm.</text>
      <biological_entity id="o11049" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves palmate, 3–10 cm;</text>
      <biological_entity constraint="basal" id="o11050" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–7 cm, long hairs abundant, spreading to ascending, 1–3 mm, weak, glands abundant;</text>
      <biological_entity id="o11051" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11052" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o11053" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 5, central oblanceolate to broadly obovate-cuneate or nearly orbiculate, 1–3 × 0.8–2 cm, scarcely to distinctly petiolulate, distal ± 1/3 of margins evenly incised ± 1/4 to midvein, teeth 2–4 per side, surfaces green, long hairs sparse to abundant, 1 mm, glands ± abundant.</text>
      <biological_entity id="o11054" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="central" id="o11055" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="broadly obovate-cuneate or nearly orbiculate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="scarcely to distinctly" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11056" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character constraint="of margins" constraintid="o11057" name="quantity" src="d0_s4" value="1/3" value_original="1/3" />
        <character constraint="to midvein" constraintid="o11058" name="quantity" src="d0_s4" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o11057" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="evenly; more or less" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o11058" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o11059" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o11060" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o11060" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o11061" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o11062" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="abundant" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11063" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–20-flowered.</text>
      <biological_entity id="o11064" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-20-flowered" value_original="3-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.5–2.5 (–4) cm.</text>
      <biological_entity id="o11065" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets lanceolate-elliptic, rarely linear, 1–2 (–5) × 0.5–1.5 mm;</text>
      <biological_entity id="o11066" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o11067" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 2–3.5 mm diam.;</text>
      <biological_entity id="o11068" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11069" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 2–4 (–5) mm, apex ± acute;</text>
      <biological_entity id="o11070" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11071" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11072" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals ± paler abaxially, bright-yellow adaxially, ± obcordate, (3–) 4–7 × 3–6 mm;</text>
      <biological_entity id="o11073" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11074" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less; abaxially" name="coloration" src="d0_s10" value="paler" value_original="paler" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1.5–2.5 mm, anthers 0.5–1 mm;</text>
      <biological_entity id="o11075" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11076" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11077" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 5–20, styles 1.5–2.5 mm.</text>
      <biological_entity id="o11078" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11079" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <biological_entity id="o11080" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1.5 mm, smooth to faintly rugose.</text>
      <biological_entity id="o11081" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s13" to="faintly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices on protected faces of granitic outcrops in conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="on protected faces of granitic outcrops in conifer woodlands" />
        <character name="habitat" value="protected faces" constraint="of granitic outcrops in conifer woodlands" />
        <character name="habitat" value="granitic outcrops" constraint="in conifer woodlands" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2400–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla rimicola is known only from Tahquitz Peak in the San Jacinto Mountains, Riverside County, California, and the Sierra San Pedro Mártir in Baja California, Mexico. Plants are rooted in rock crevices, often on vertical surfaces, in contrast to P. wheeleri, which is rooted in the ground.</discussion>
  
</bio:treatment>