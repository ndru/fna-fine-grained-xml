<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">412</other_info_on_meta>
    <other_info_on_meta type="illustration_page">407</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) Rydberg" date="unknown" rank="genus">petrophytum</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="unknown" rank="species">caespitosum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 206. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus petrophytum;species caespitosum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100290</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">caespitosa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 418. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species caespitosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogynia</taxon_name>
    <taxon_name authority="(Nuttall) S. Watson" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_hierarchy>genus Eriogynia;species caespitosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luetkea</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze" date="unknown" rank="species">caespitosa</taxon_name>
    <taxon_hierarchy>genus Luetkea;species caespitosa</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–10+ dm diam.</text>
      <biological_entity id="o24189" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="diameter" src="d0_s0" to="10" to_unit="dm" upper_restricted="false" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate or decumbent, loosely intertwined, 1–4+ cm, and erect or ascending, tightly coalesced, 0.2–0.5 cm, internodes 0.1–1 cm.</text>
      <biological_entity id="o24190" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s1" value="intertwined" value_original="intertwined" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="tightly" name="fusion" src="d0_s1" value="coalesced" value_original="coalesced" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s1" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24191" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade spatulate, 0.4–1.8 × 0.2–0.4 cm, venation rarely visible except on long-shoot leaves, 1 (–3) -veined, apex acute, abaxial surface pilose to sericeous, rarely sparsely strigose on lamina and veins.</text>
      <biological_entity id="o24192" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24193" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s2" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="0.4" to_unit="cm" />
        <character constraint="except " constraintid="o24194" is_modifier="false" modifier="rarely" name="prominence" src="d0_s2" value="visible" value_original="visible" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="1(-3)-veined" value_original="1(-3)-veined" />
      </biological_entity>
      <biological_entity constraint="long-shoot" id="o24194" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24195" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24196" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s2" to="sericeous" />
        <character constraint="on veins" constraintid="o24198" is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o24197" name="lamina" name_original="lamina" src="d0_s2" type="structure" />
      <biological_entity id="o24198" name="vein" name_original="veins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Panicles widely branched to compact and racemose, [0.5–] 1–15 [–20] × 0.5–6 cm, sericeous;</text>
      <biological_entity id="o24199" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="widely" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched to compact" value_original="branched to compact" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts subulate to narrowly obtrullate, 3–8 mm, pilose to sericeous.</text>
      <biological_entity id="o24200" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s4" to="narrowly obtrullate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s4" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0.5–2.5 mm;</text>
      <biological_entity id="o24201" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles 1, extending from middle to well beyond apex of sepals.</text>
      <biological_entity id="o24202" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24203" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o24204" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="position" src="d0_s6" value="middle" value_original="middle" />
      </biological_entity>
      <relation from="o24202" id="r1570" name="extending from" negation="false" src="d0_s6" to="o24203" />
      <relation from="o24202" id="r1571" name="extending from" negation="false" src="d0_s6" to="o24204" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–6 mm diam.;</text>
      <biological_entity id="o24205" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 1 mm, densely sericeous;</text>
      <biological_entity id="o24206" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect, ovatelanceolate, 1–1.5 mm, margins sericeous, abaxial surface sericeous to tomentose;</text>
      <biological_entity id="o24207" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24208" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24209" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s9" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals narrowly oblanceolate, 1–2.5 mm, apex obtuse to slightly cleft or acute to acuminate;</text>
      <biological_entity id="o24210" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24211" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="slightly cleft or acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20, lengths 1.5–2 (–2.5) times petals (1.5–3 times sepals);</text>
      <biological_entity id="o24212" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
        <character constraint="petal" constraintid="o24213" is_modifier="false" name="length" src="d0_s11" value="1.5-2(-2.5) times petals" value_original="1.5-2(-2.5) times petals" />
      </biological_entity>
      <biological_entity id="o24213" name="petal" name_original="petals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>carpels (3–) 5 (–6), adaxially connate.</text>
      <biological_entity id="o24214" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="adaxially" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Follicles 2 mm. 2n = 18.</text>
      <biological_entity id="o24215" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24216" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nev., S.Dak., Tex., Utah, Wash.; ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Petrophyton</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Petrophytum caespitosum primarily inhabits arid rocky outcrops and talus slopes at high elevations in mountain ranges of the western United States and northeastern Mexico.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 0.4–1.4 cm, abaxial surfaces pilose to sericeous; petal apices obtuse to slightly cleft.</description>
      <determination>3a Petrophytum caespitosum subsp. caespitosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 1–1.8 cm, abaxial surfaces sparsely strigose; petal apices acute to acuminate.</description>
      <determination>3b Petrophytum caespitosum subsp. acuminatum</determination>
    </key_statement>
  </key>
</bio:treatment>