<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="(Sargent) Rehder" date="1940" rank="series">Aestivales</taxon_name>
    <taxon_name authority="Crataegus ×rufula Sargent" date="1920" rank="species">×rufula</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>1: 251. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series aestivales;species ×rufula;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100197</other_info_on_name>
  </taxon_identification>
  <number>45.</number>
  <other_name type="common_name">Rufous mayhaw</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 30–50 dm.</text>
      <biological_entity id="o28616" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth rusty-tomentose, 1-year old chestnut-brown;</text>
      <biological_entity id="o28618" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28619" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o28620" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="rusty-tomentose" value_original="rusty-tomentose" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="chestnut-brown" value_original="chestnut-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs 2-years old dark gray to black, 1–3 (–5) cm.</text>
      <biological_entity id="o28621" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o28622" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark gray" name="coloration" src="d0_s2" to="black" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28623" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="old" value_original="old" />
      </biological_entity>
      <relation from="o28622" id="r1870" name="on" negation="false" src="d0_s2" to="o28623" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole length 10–15% blade;</text>
      <biological_entity id="o28624" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28625" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o28626" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to ovate, 2.5–4.5 cm, lobes 0 or sinuate (those on rapidly elongating shoots larger, usually proportionately wider than those on short-shoots, sinuately lobed), margins entire or barely serrate or crenate in distal 1/3 or 1/2, teeth gland-tipped, veins 3–5 per side, apex subacute to obtuse, abaxial surface densely rufous-tomentose, especially on veins, adaxial densely white or rufous-tomentose young, scabrous mature.</text>
      <biological_entity id="o28627" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28628" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28629" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o28630" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="barely serrate or crenate" value_original="barely serrate or crenate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28631" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o28632" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character constraint="in " constraintid="o28634" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28633" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o28634" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o28635" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o28636" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o28636" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o28637" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28638" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="rufous-tomentose" value_original="rufous-tomentose" />
      </biological_entity>
      <biological_entity id="o28639" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o28640" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="rufous-tomentose" value_original="rufous-tomentose" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o28630" id="r1871" name="in" negation="false" src="d0_s4" to="o28631" />
      <relation from="o28630" id="r1872" name="in" negation="false" src="d0_s4" to="o28632" />
      <relation from="o28638" id="r1873" modifier="especially" name="on" negation="false" src="d0_s4" to="o28639" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–5-flowered umbels;</text>
      <biological_entity id="o28641" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o28642" name="umbel" name_original="umbels" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous or rufous-tomentose;</text>
      <biological_entity id="o28643" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="rufous-tomentose" value_original="rufous-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles narrow, margins glandular.</text>
      <biological_entity id="o28644" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o28645" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers ± precocious, 15–27.5 mm diam.;</text>
      <biological_entity id="o28646" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="maturation" src="d0_s8" value="precocious" value_original="precocious" />
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s8" to="27.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium glabrous or rufous-tomentose;</text>
      <biological_entity id="o28647" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="rufous-tomentose" value_original="rufous-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepal margins entire or slightly glandular-serrate;</text>
      <biological_entity constraint="sepal" id="o28648" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s10" value="glandular-serrate" value_original="glandular-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 12 mm;</text>
      <biological_entity id="o28649" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers red;</text>
      <biological_entity id="o28650" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles (4 or) 5.</text>
      <biological_entity id="o28651" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pomes red, 10 mm diam.;</text>
      <biological_entity id="o28652" name="pome" name_original="pomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character name="diameter" src="d0_s14" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals short-triangular;</text>
      <biological_entity id="o28653" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="short-triangular" value_original="short-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pyrenes 5.2n = 34, 51.</text>
      <biological_entity id="o28654" name="pyrene" name_original="pyrenes" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28655" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
        <character name="quantity" src="d0_s16" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Mar; fruiting May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally inundated depressions, ditches, sink holes, streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="inundated depressions" modifier="seasonally" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="sink holes" />
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus ×rufula is restricted to the Florida panhandle and adjacent Georgia and southeastern Alabama.</discussion>
  <discussion>Crataegus ×rufula is a presumed hybrid between the other two mayhaws or descendants thereof (J. B. Phipps 1988b) as shown by morphometric analyses. Although in some characteristics (for example, the usually sinuate leaf margin) C. ×rufula is like C. opaca, it lacks the characteristic elongate leaves of the latter. Crataegus ×rufula also tends to intergrade with C. aestivalis, with which it is fully sympatric. The frequency of C. aestivalis-like intermediates could well be accounted for by pure C. aestivalis being reasonably common through the C. ×rufula range and C. opaca being scarce there. Some, not all, individuals of C. ×rufula have relatively large flowers. Crataegus ×rufula is locally common and conveniently treated as a species.</discussion>
  
</bio:treatment>