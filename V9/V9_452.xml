<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Joshua C. Springer,Bruce D. Parfitt†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Bunge in C. F. von Ledebour" date="1829" rank="genus">Chamaerhodos</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. von Ledebour, Fl. Altaica</publication_title>
      <place_in_publication>1: 429. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus Chamaerhodos</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chamai, dwarf, and rhodon, rose, alluding to appearance of plants</other_info_on_name>
    <other_info_on_name type="fna_id">106465</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Little rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, biennial or short-lived [long-lived] perennial, [0.3–] 0.6–3 dm, hispid-hirsute, soft-hairy, and stipitate-glandular [eglandular];</text>
      <biological_entity id="o3690" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="0.6" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hispid-hirsute" value_original="hispid-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="soft-hairy" value_original="soft-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots woody, scaly.</text>
      <biological_entity id="o3691" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 (–10), reddish tinged [green], simple or branched throughout, branches ascending or erect.</text>
      <biological_entity id="o3692" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="10" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish tinged" value_original="reddish tinged" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="throughout" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o3693" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal winter-persistent, basal and cauline, rosulate, alternate, pinnately compound or simple and deeply pinnatifid;</text>
      <biological_entity id="o3694" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="duration" src="d0_s3" value="winter-persistent" value_original="winter-persistent" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o3695" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present;</text>
      <biological_entity id="o3696" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade obovate, 1.5–4 cm, herbaceous, 2–4-ternate, cauline sessile or petioles to 2 mm, blade 1–2-pinnate or pinnatifid, lobes linear-oblong, margins flat, entire, venation pinnate, 1 vein per lobe (lateral-veins not seen), surfaces hairy, stipitate [or sessile] glandular.</text>
      <biological_entity id="o3697" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-4-ternate" value_original="2-4-ternate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o3698" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3699" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3700" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="1-2-pinnate" value_original="1-2-pinnate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o3701" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <biological_entity id="o3702" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o3703" name="vein" name_original="vein" src="d0_s6" type="structure" />
      <biological_entity id="o3704" name="lobe" name_original="lobe" src="d0_s6" type="structure" />
      <biological_entity id="o3705" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o3703" id="r232" name="per" negation="false" src="d0_s6" to="o3704" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal or often axillary, 10–many-flowered, crowded [sparse], flat-topped panicles;</text>
      <biological_entity id="o3706" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="10-many-flowered" value_original="10-many-flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o3707" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="flat-topped" value_original="flat-topped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles present;</text>
      <biological_entity id="o3708" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts present;</text>
      <biological_entity id="o3709" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles absent.</text>
      <biological_entity id="o3710" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels present or nearly absent [long-pedicellate].</text>
      <biological_entity id="o3711" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="nearly" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 2–4 [–4.5] mm diam.;</text>
      <biological_entity id="o3712" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>epicalyx bractlets 5, reduced to bristles proximal to sepal bases;</text>
      <biological_entity constraint="epicalyx" id="o3713" name="bractlet" name_original="bractlets" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character constraint="to bristles" constraintid="o3714" is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3714" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character constraint="to sepal bases" constraintid="o3715" is_modifier="false" name="position" src="d0_s13" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity constraint="sepal" id="o3715" name="base" name_original="bases" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>hypanthium campanulate, 1.5–2.5 [–3.5] mm, exterior shiny-bristled, adaxially sparsely villous at rim;</text>
      <biological_entity id="o3716" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3717" name="exterior" name_original="exterior" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="shiny-bristled" value_original="shiny-bristled" />
        <character constraint="at rim" constraintid="o3718" is_modifier="false" modifier="adaxially sparsely" name="pubescence" src="d0_s14" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o3718" name="rim" name_original="rim" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, erect-ascending [recurved], ovatelanceolate [triangular], bristle-tipped;</text>
      <biological_entity id="o3719" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="bristle-tipped" value_original="bristle-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, white, obovate or cuneate-oblong, apex emarginate;</text>
      <biological_entity id="o3720" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cuneate-oblong" value_original="cuneate-oblong" />
      </biological_entity>
      <biological_entity id="o3721" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 5 (or 6), shorter than petals;</text>
      <biological_entity id="o3722" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character constraint="than petals" constraintid="o3723" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3723" name="petal" name_original="petals" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>torus absent;</text>
      <biological_entity id="o3724" name="torus" name_original="torus" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>carpels 5–10 (–20), glabrous, styles lateral;</text>
      <biological_entity id="o3725" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="20" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s19" to="10" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3726" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovule 1.</text>
      <biological_entity id="o3727" name="ovule" name_original="ovule" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits aggregated achenes, 1–6, olivaceous to blackish [reddish to purplish at base], ovoid-pyriform, 1.1–1.4 [–1.6] mm, glabrous;</text>
      <biological_entity id="o3728" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character char_type="range_value" from="olivaceous" name="coloration" notes="" src="d0_s21" to="blackish" />
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid-pyriform" value_original="ovoid-pyriform" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3729" name="achene" name_original="achenes" src="d0_s21" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s21" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s21" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hypanthium persistent;</text>
      <biological_entity id="o3730" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, erect-ascending (recurved);</text>
      <biological_entity id="o3731" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s23" value="erect-ascending" value_original="erect-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>styles persistent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 7.</text>
      <biological_entity id="o3732" name="style" name_original="styles" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3733" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7 (1 in the flora).</discussion>
  
</bio:treatment>