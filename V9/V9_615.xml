<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="unknown" rank="species">fremontii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 442. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species fremontii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100390</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prunus</taxon_name>
    <taxon_name authority="S. C. Mason" date="unknown" rank="species">eriogyna</taxon_name>
    <taxon_hierarchy>genus Prunus;species eriogyna</taxon_hierarchy>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Desert apricot</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, suckering unknown, much branched, 10–40 dm, thorny.</text>
      <biological_entity id="o14582" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with axillary end buds, glabrous.</text>
      <biological_entity id="o14583" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="end" id="o14584" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="axillary end" />
      <relation from="o14583" id="r942" name="with" negation="false" src="d0_s1" to="o14584" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o14585" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–7 mm, glabrous, eglandular;</text>
      <biological_entity id="o14586" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic, ovate, or suborbiculate, 0.6–3 × 0.5–2 cm, base obtuse to rounded, subcordate, or truncate, margins obscurely crenulate, crenulate-serrulate, or serrate, teeth blunt, glandular, apex usually obtuse to rounded, sometimes emarginate, surfaces glabrous.</text>
      <biological_entity id="o14587" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14588" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded subcordate or truncate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded subcordate or truncate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded subcordate or truncate" />
      </biological_entity>
      <biological_entity id="o14589" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s4" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenulate-serrulate" value_original="crenulate-serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o14590" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o14591" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o14592" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3-flowered, umbellate fascicles.</text>
      <biological_entity id="o14593" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 2–12 mm, glabrous.</text>
      <biological_entity id="o14594" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers blooming at leaf emergence;</text>
      <biological_entity id="o14595" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="at leaf" constraintid="o14596" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o14596" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium campanulate, 2–4 mm, glabrous externally;</text>
      <biological_entity id="o14597" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect-spreading, semicircular to ovate, 1.2–4 mm, margins glandular-toothed, ciliate, abaxial surface glabrous, adaxial hairy;</text>
      <biological_entity id="o14598" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="semicircular" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14599" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="glandular-toothed" value_original="glandular-toothed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14600" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14601" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually white, sometimes pinkish rose, elliptic, obovate, or suborbiculate, 3–10 mm;</text>
      <biological_entity id="o14602" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pinkish rose" value_original="pinkish rose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries hairy.</text>
      <biological_entity id="o14603" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes yellowish, ellipsoid-ovoid, 8–15 mm, densely puberulent;</text>
      <biological_entity id="o14604" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid-ovoid" value_original="ellipsoid-ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mesocarps leathery to dry (splitting);</text>
      <biological_entity id="o14605" name="mesocarp" name_original="mesocarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="leathery" name="texture" src="d0_s13" to="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stones ovoid, ± flattened.</text>
      <biological_entity id="o14606" name="stone" name_original="stones" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Mar; fruiting Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Jan" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy or rocky slopes, canyons, desert, chaparral, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="dry" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="desert" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">fremonti</other_name>
  <discussion>Prunus fremontii is known only from the western edge of the Sonoran Desert.</discussion>
  
</bio:treatment>