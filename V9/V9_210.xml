<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reidar Elven,Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Th. Wolf) Juzepczuk in V. L. Komarov et al." date="1941" rank="section">Chrysanthae</taxon_name>
    <place_of_publication>
      <publication_title>in V. L. Komarov et al., Fl. URSS</publication_title>
      <place_in_publication>10: 180. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Chrysanthae</taxon_hierarchy>
    <other_info_on_name type="fna_id">318027</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Th. Wolf" date="unknown" rank="unranked">Chrysanthae</taxon_name>
    <place_of_publication>
      <publication_title>in P. F. A. Ascherson et al., Syn. Mitteleur. Fl.</publication_title>
      <place_in_publication>6(1): 671, 775. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;unranked Chrysanthae</taxon_hierarchy>
  </taxon_identification>
  <number>8i.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, rosetted or tufted, not stoloniferous;</text>
      <biological_entity id="o7634" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened;</text>
      <biological_entity id="o7635" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vestiture mostly of long hairs (and crisped hairs in inflorescence), glands absent or sparse to abundant, sometimes red (P. rubella).</text>
      <biological_entity id="o7636" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o7637" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o7638" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="abundant" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <relation from="o7636" id="r473" name="part_of" negation="false" src="d0_s2" to="o7637" />
    </statement>
    <statement id="d0_s3">
      <text>Stems ascending to erect, not flagelliform, not rooting at nodes, lateral to persistent basal rosettes, (0.2–) 1–5 (–7) dm, lengths usually 2–5 times basal leaves, rarely shorter than leaves.</text>
      <biological_entity id="o7639" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o7640" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="7" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="5" to_unit="dm" />
        <character constraint="leaf" constraintid="o7642" is_modifier="false" modifier="usually" name="length" src="d0_s3" value="2-5 times basal leaves" value_original="2-5 times basal leaves" />
        <character constraint="than leaves" constraintid="o7643" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="rarely shorter" value_original="rarely shorter" />
      </biological_entity>
      <biological_entity id="o7640" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o7641" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o7642" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7643" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not in ranks;</text>
      <biological_entity id="o7644" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o7645" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7646" name="rank" name_original="ranks" src="d0_s4" type="structure" />
      <relation from="o7645" id="r474" name="in" negation="false" src="d0_s4" to="o7646" />
    </statement>
    <statement id="d0_s5">
      <text>cauline 1–3;</text>
      <biological_entity id="o7647" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o7648" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves palmate, 3–25 (–35) cm;</text>
      <biological_entity id="o7649" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o7650" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="palmate" value_original="palmate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs spreading to appressed, weak to stiff, glands absent or sparse to common;</text>
      <biological_entity id="o7651" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o7652" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="appressed" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s7" to="stiff" />
      </biological_entity>
      <biological_entity id="o7653" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s7" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets (4–) 5–11, at tip of leaf axis, ± overlapping or not, narrowly oblanceolate or oblanceolate-elliptic to obovate, margins flat, distal 1/2–3/4+ evenly incised 1/4–1/3 (–1/2) to midvein, teeth 1–10 (–13) per side, or distal 1/4 or less incised less than 1/10 (–1/4) to midvein, teeth 1–2 (–5) per side (P. stipularis), surfaces ± similar, green to pale green or reddish, sometimes glaucous, long hairs absent or weak to stiff, cottony hairs absent.</text>
      <biological_entity id="o7654" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o7655" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="11" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s8" value="overlapping" value_original="overlapping" />
        <character name="arrangement" src="d0_s8" value="not" value_original="not" />
        <character char_type="range_value" from="oblanceolate-elliptic" name="shape" src="d0_s8" to="obovate" />
      </biological_entity>
      <biological_entity id="o7656" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o7657" name="axis" name_original="axis" src="d0_s8" type="structure" />
      <biological_entity id="o7658" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s8" to="3/4" upper_restricted="false" />
        <character is_modifier="false" modifier="evenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o7659" from="1/4" name="quantity" src="d0_s8" to="1/3-1/2" />
      </biological_entity>
      <biological_entity id="o7659" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o7660" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="13" />
        <character char_type="range_value" constraint="per side" constraintid="o7661" from="1" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s8" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s8" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="less" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o7662" from="0" name="quantity" src="d0_s8" to="1/10-1/4" />
      </biological_entity>
      <biological_entity id="o7661" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o7662" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o7663" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character char_type="range_value" constraint="per side" constraintid="o7664" from="1" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o7664" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o7665" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="pale green or reddish" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o7666" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" value="weak to stiff" value_original="weak to stiff" />
      </biological_entity>
      <biological_entity id="o7667" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o7655" id="r475" name="at" negation="false" src="d0_s8" to="o7656" />
      <relation from="o7656" id="r476" name="part_of" negation="false" src="d0_s8" to="o7657" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (1–) 2–20-flowered, usually cymose, open.</text>
      <biological_entity id="o7668" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(1-)2-20-flowered" value_original="(1-)2-20-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels usually ± straight in fruit, sometimes ± curved (P. thuringiaca), 1–3 (–6) cm, proximal often much longer than distal.</text>
      <biological_entity id="o7669" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o7670" is_modifier="false" modifier="usually more or less" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes more or less" name="course" notes="" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7670" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o7671" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o7672" is_modifier="false" name="length_or_size" src="d0_s10" value="often much longer" value_original="often much longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o7672" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: 5-merous;</text>
      <biological_entity id="o7673" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 2.5–5 mm diam.;</text>
      <biological_entity id="o7674" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7675" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals yellow, ± obcordate, (4–) 5–10 mm, longer than sepals, apex retuse;</text>
      <biological_entity id="o7676" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7677" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="than sepals" constraintid="o7678" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o7678" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o7679" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens ca. 20;</text>
      <biological_entity id="o7680" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7681" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, ± columnar, rarely columnar-filiform, to tapered, not or ± papillate-swollen proximally, 0.7–1.3 mm.</text>
      <biological_entity id="o7682" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o7683" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="rarely columnar-filiform" name="shape" src="d0_s15" to="tapered" />
        <character char_type="range_value" from="rarely columnar-filiform" name="shape" src="d0_s15" to="tapered" />
        <character is_modifier="false" modifier="not; more or less; proximally" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth to faintly rugose.</text>
      <biological_entity id="o7684" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s16" to="faintly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>n North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="n North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 8 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves: stipules fused with less than 1/3 of petiole, free portion longer than fused portion; basal leaves: distal 2/3–3/4+ of leaflet margins with (4–)6–10(–13) teeth per side; petiole hairs ± spreading, 1–2.5 mm; introduced; Quebec and Rhode Island.</description>
      <determination>18 Potentilla thuringiaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves: stipules fused with all or most of petiole, free portion shorter than fused portion; basal leaves: distal 1/10–1/2 or less of leaflet margins with 1–2(–5) teeth per side; petiole hairs absent or appressed to spreading, 0.5–1.5 mm; native; Alaska and Greenland</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 7–11, margins incised in distal 1/4 or less; glands mostly absent.</description>
      <determination>19 Potentilla stipularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets (4–)5(–6), margins incised in distal ± 1/2; glands sparse to abundant (especially on pedicels, epicalyx bractlets, and sepals).</description>
      <determination>20 Potentilla rubella</determination>
    </key_statement>
  </key>
</bio:treatment>