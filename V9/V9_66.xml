<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">32</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">parvifolius</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1197. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species parvifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011513</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rubus</taxon_name>
    <taxon_name authority="Thunberg" date="unknown" rank="species">triphyllus</taxon_name>
    <taxon_hierarchy>genus Rubus;species triphyllus</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Japanese bramble</other_name>
  <other_name type="common_name">Australian raspberry or bramble</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–3 dm, armed.</text>
      <biological_entity id="o21801" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems biennial, low-mounding to creeping, sparsely to moderately hairy, glabrescent, eglandular, not pruinose;</text>
      <biological_entity id="o21802" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="low-mounding" name="growth_form" src="d0_s1" to="creeping" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>prickles sparse to moderate, slightly to strongly hooked, weak to stout, 0.8–1.5 (–2) mm, broad-based.</text>
      <biological_entity id="o21803" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="size" src="d0_s2" value="moderate" value_original="moderate" />
        <character is_modifier="false" modifier="slightly to strongly" name="shape" src="d0_s2" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s2" to="stout" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="broad-based" value_original="broad-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, ternate or pinnately compound;</text>
      <biological_entity id="o21804" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="ternate" value_original="ternate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules filiform to linear, (4–) 6–11 mm;</text>
      <biological_entity id="o21805" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3 (–5), terminal broadly ovate-rhombic to obovate, (2–) 3–5 (–9) × 2.2–5 (–7.5) cm, base cuneate to obtuse, slightly 3-lobed, margins coarsely serrate to doubly serrate, apex acute to obtuse, abaxial surfaces with slightly to strongly hooked prickles on veins, densely whitish-tomentose, eglandular or sparsely short-stipitate-glandular.</text>
      <biological_entity id="o21806" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="broadly ovate-rhombic" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21807" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o21808" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="serrate to doubly" value_original="serrate to doubly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o21809" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21810" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s5" value="whitish-tomentose" value_original="whitish-tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o21811" name="prickle" name_original="prickles" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="slightly; strongly" name="shape" src="d0_s5" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o21812" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <relation from="o21810" id="r1416" name="with" negation="false" src="d0_s5" to="o21811" />
      <relation from="o21811" id="r1417" name="on" negation="false" src="d0_s5" to="o21812" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and axillary, 2–20-flowered, cymiform to thyrsiform.</text>
      <biological_entity id="o21813" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-20-flowered" value_original="2-20-flowered" />
        <character char_type="range_value" from="cymiform" name="architecture" src="d0_s6" to="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: prickles moderate, slightly to strongly hooked, moderately hairy, eglandular or sparsely short-stipitate-glandular.</text>
      <biological_entity id="o21814" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o21815" name="prickle" name_original="prickles" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="moderate" value_original="moderate" />
        <character is_modifier="false" modifier="slightly to strongly" name="shape" src="d0_s7" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o21816" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pinkish to magenta, oblanceolate to obovate, 4–7 mm;</text>
      <biological_entity id="o21817" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s9" to="magenta" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments filiform;</text>
      <biological_entity id="o21818" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries moderately to densely hairy, styles glabrous.</text>
      <biological_entity id="o21819" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21820" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits red, round, 0.6–0.9 cm;</text>
      <biological_entity id="o21821" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s12" value="round" value_original="round" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s12" to="0.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>drupelets 10–50, strongly coherent, separating from torus.</text>
      <biological_entity id="o21823" name="torus" name_original="torus" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o21822" name="drupelet" name_original="drupelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="50" />
        <character is_modifier="false" modifier="strongly" name="fusion" src="d0_s13" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o21823" is_modifier="false" name="arrangement" src="d0_s13" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21824" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Del., Ill., Iowa, Mass., Mo., Nebr., N.J., Ohio, Va.; e Asia; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus parvifolius is distinguished from other raspberries by its broadly ovate-rhombic to obovate leaflets and relatively small flowers with pinkish to magenta petals. This species has the potential to become a very significant weed (P. M. Drobney and M. P. Widrlechner 2012).</discussion>
  
</bio:treatment>