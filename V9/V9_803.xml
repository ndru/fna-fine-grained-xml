<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="mention_page">474</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">malus</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">pumila</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Malus no. 3. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus malus;species pumila</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200010913</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">malus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="variety">paradisiaca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 479. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pyrus;species malus;variety paradisiaca</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">sylvestris</taxon_name>
    <taxon_hierarchy>genus M.;species sylvestris</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">malus</taxon_name>
    <taxon_hierarchy>genus P.;species malus</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Orchard or common or paradise crabapple</other_name>
  <other_name type="common_name">pommier commun</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 20–100 (–150) dm.</text>
      <biological_entity id="o8904" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="100" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–30 cm diam.;</text>
      <biological_entity id="o8905" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="diameter" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark dark gray or gray-brown, scaly;</text>
      <biological_entity id="o8906" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s2" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>young branches dark-brown or reddish-brown, densely tomentose, becoming glabrous;</text>
      <biological_entity id="o8907" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="young" value_original="young" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>flowering shoots becoming spurs, 40–150 mm.</text>
      <biological_entity id="o8908" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s4" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8909" name="spur" name_original="spurs" src="d0_s4" type="structure" />
      <relation from="o8908" id="r568" modifier="becoming" name="" negation="false" src="d0_s4" to="o8909" />
    </statement>
    <statement id="d0_s5">
      <text>Buds dark red or purple, ovoid, 3–4 (–5) mm, scale margins densely puberulous.</text>
      <biological_entity id="o8910" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="scale" id="o8911" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="puberulous" value_original="puberulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves convolute in bud;</text>
      <biological_entity id="o8913" name="bud" name_original="bud" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>isomorphic;</text>
      <biological_entity id="o8912" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="in bud" constraintid="o8913" is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="architecture_or_shape_or_variability" src="d0_s7" value="isomorphic" value_original="isomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules deciduous, lanceolate, 3–5 mm, apex acuminate;</text>
      <biological_entity id="o8914" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8915" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole 10–35 mm, tomentose to sparsely pubescent;</text>
      <biological_entity id="o8916" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s9" to="sparsely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade elliptic, ovate, or broadly elliptic, (2–) 5–10 × (1–) 3–6.5 cm, base broadly cuneate or rounded, margins unlobed, obtusely serrate, sometimes serrate-crenate, apex acute, abaxial surface glabrescent, adaxial densely puberulent.</text>
      <biological_entity id="o8917" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s10" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s10" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s10" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8918" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8919" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="obtusely" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="serrate-crenate" value_original="serrate-crenate" />
      </biological_entity>
      <biological_entity id="o8920" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8921" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8922" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Panicles umbellike;</text>
      <biological_entity id="o8923" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="umbel-like" value_original="umbellike" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peduncles absent;</text>
      <biological_entity id="o8924" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bracteoles deciduous, filiform, 5–7 mm.</text>
      <biological_entity id="o8925" name="bracteole" name_original="bracteoles" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels 10–25 mm, puberulous.</text>
      <biological_entity id="o8926" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulous" value_original="puberulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Flowers 30–40 mm diam.;</text>
      <biological_entity id="o8927" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="diameter" src="d0_s15" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hypanthium tomentose;</text>
      <biological_entity id="o8928" name="hypanthium" name_original="hypanthium" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals reflexed at flowering, triangular-lanceolate or triangular-ovate, 6–8 mm, equal to or longer than tube, apex acuminate, surfaces tomentose;</text>
      <biological_entity id="o8929" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character constraint="at flowering" is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s17" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character constraint="than tube" constraintid="o8930" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8930" name="tube" name_original="tube" src="d0_s17" type="structure" />
      <biological_entity id="o8931" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o8932" name="surface" name_original="surfaces" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals (rose in bud) white, sometimes pink, obovate, 15–25 mm, claws 1 mm, margins entire, apex rounded;</text>
      <biological_entity id="o8933" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s18" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s18" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8934" name="claw" name_original="claws" src="d0_s18" type="structure">
        <character name="some_measurement" src="d0_s18" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8935" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8936" name="apex" name_original="apex" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens 20, 9–10 mm, anthers yellow before dehiscence;</text>
      <biological_entity id="o8937" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s19" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8938" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 5, basally connate less than 1/2 length, 9–10 mm, slightly longer than stamens, basally gray-tomentose;</text>
      <biological_entity id="o8939" name="style" name_original="styles" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="length" src="d0_s20" to="1/2" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s20" to="10" to_unit="mm" />
        <character constraint="than stamens" constraintid="o8940" is_modifier="false" name="length_or_size" src="d0_s20" value="slightly longer" value_original="slightly longer" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s20" value="gray-tomentose" value_original="gray-tomentose" />
      </biological_entity>
      <biological_entity id="o8940" name="stamen" name_original="stamens" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>stigmas green.</text>
      <biological_entity id="o8941" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Pomes green, yellow, or red, pure, striped, or blushed, globose or depressed-globose, 20–50 (–70) mm diam., skin with bloom or wax, sometimes russetted or dotted, cores enclosed at apex;</text>
      <biological_entity id="o8942" name="pome" name_original="pomes" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="striped" value_original="striped" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="blushed" value_original="blushed" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="striped" value_original="striped" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="blushed" value_original="blushed" />
        <character is_modifier="false" name="shape" src="d0_s22" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s22" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s22" to="70" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s22" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8943" name="skin" name_original="skin" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" notes="" src="d0_s22" value="dotted" value_original="dotted" />
      </biological_entity>
      <biological_entity id="o8944" name="bloom" name_original="bloom" src="d0_s22" type="structure" />
      <biological_entity id="o8945" name="wax" name_original="wax" src="d0_s22" type="substance" />
      <biological_entity id="o8946" name="core" name_original="cores" src="d0_s22" type="structure" />
      <biological_entity id="o8947" name="apex" name_original="apex" src="d0_s22" type="structure" />
      <relation from="o8943" id="r569" name="with" negation="false" src="d0_s22" to="o8944" />
      <relation from="o8943" id="r570" name="with" negation="false" src="d0_s22" to="o8945" />
      <relation from="o8946" id="r571" name="enclosed at" negation="false" src="d0_s22" to="o8947" />
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, erect;</text>
      <biological_entity id="o8948" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s23" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>sclereids absent.</text>
      <biological_entity id="o8949" name="sclereid" name_original="sclereids" src="d0_s24" type="structure">
        <character is_modifier="false" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Seeds light-brown.</text>
    </statement>
    <statement id="d0_s26">
      <text>2n = 34 (51, 68).</text>
      <biological_entity id="o8950" name="seed" name_original="seeds" src="d0_s25" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s25" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8951" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="34" value_original="34" />
        <character name="quantity" src="d0_s26" value="[51" value_original="[51" />
        <character name="quantity" src="d0_s26" value="68]" value_original="68]" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abandoned or naturalized in thickets, forests, fields, fence edges, shores, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" modifier="abandoned or naturalized in" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="fence edges" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Alaska, Ark., Calif., Colo., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., Tenn., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; c Asia; introduced also in Mexico, Central America (El Salvador, Guatemala), South America (Argentina), Europe, e Asia, Africa (South Africa), Pacific Islands (New Zealand, Tristan da Cunha), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="c Asia" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America (El Salvador)" establishment_means="introduced" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="e Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa (South Africa)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Tristan da Cunha)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malus pumila is cultivated for its edible apple. Historically, the nomenclature of the orchard apple has been confusing, with many species names applied to cultivar groups. Based on extensive nomenclatural research, D. J. Mabberley et al. (2001) concluded that M. pumila is the correct binomial for the species of central Asia that is ancestral to orchard apples. Commonly used binomials, such as M. domestica Borkhausen (an illegitimate name), M. paradisiaca (Linnaeus) Medikus, and M. sylvestris, have been placed in synonymy. Quian G. Z. et al. (2010) have proposed to conserve M. domestica and reject M. pumila. Their proposal is still under consideration by the Nomenclature Committee for Vascular Plants.</discussion>
  <discussion>Some trees, although originally planted, may appear naturalized when found in old, overgrown areas. Naturalized trees are derived from seeds distributed by birds and mammals and from apples discarded by people. Trees grown from seeds often produce small, bitter, and sour fruit.</discussion>
  
</bio:treatment>