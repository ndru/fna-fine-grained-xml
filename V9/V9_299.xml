<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="illustration_page">193</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Niveae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nivea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 499. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section niveae;species nivea;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200011107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rottbøll" date="unknown" rank="species">prostrata</taxon_name>
    <taxon_name authority="Soják" date="unknown" rank="subspecies">floccosa</taxon_name>
    <taxon_hierarchy>genus Potentilla;species prostrata;subspecies floccosa</taxon_hierarchy>
  </taxon_identification>
  <number>73.</number>
  <other_name type="common_name">Snow cinquefoil</other_name>
  <other_name type="common_name">potentille des neiges</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± tufted.</text>
      <biological_entity id="o33962" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudex branches stout, not columnar, not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o33963" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="columnar" value_original="columnar" />
        <character constraint="with leaves" constraintid="o33964" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o33964" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, (0.3–) 0.5–3 (–4) dm, lengths 1.5–2.5 (–4) times basal leaves.</text>
      <biological_entity id="o33965" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character constraint="leaf" constraintid="o33966" is_modifier="false" name="length" src="d0_s2" value="1.5-2.5(-4) times basal leaves" value_original="1.5-2.5(-4) times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o33966" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (1–) 3–10 (–15) cm;</text>
      <biological_entity constraint="basal" id="o33967" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0.5–) 1–6 (–10) cm, long hairs usually absent, sometimes sparse to common (less so than cottony hairs), ± appressed, 1–2 mm, soft, smooth, short-crisped hairs absent or obscured, cottony hairs abundant to dense sometimes sparse with age, glands absent, sparse, or obscured;</text>
      <biological_entity id="o33968" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33969" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="sparse" modifier="sometimes" name="quantity" src="d0_s4" to="common" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o33970" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="short-crisped" value_original="short-crisped" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o33971" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character constraint="with age" constraintid="o33972" is_modifier="false" modifier="sometimes" name="count_or_density" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o33972" name="age" name_original="age" src="d0_s4" type="structure" />
      <biological_entity id="o33973" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="count_or_density" src="d0_s4" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets overlapping, central obovate, 0.5–2 (–4) × (0.2–) 0.4–1.2 (–2) cm, subsessile, base cuneate, margins slightly revolute, distal ± 3/4 incised (1/4–) 1/3–1/2 to midvein, teeth (2–) 3–5 (–6) per side, ± approximate, surfaces dissimilar, often strongly so, abaxial ± white, long hairs 0.8–1.2 mm, cottony-crisped hairs dense, adaxial usually green, sometimes grayish green, long hairs sparse to abundant, short-crisped hairs sparse to common.</text>
      <biological_entity id="o33974" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="central" id="o33975" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s5" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o33976" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o33977" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="more or less" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s5" value="3/4" value_original="3/4" />
        <character is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o33978" from="1/4" name="quantity" src="d0_s5" to="1/3-1/2" />
      </biological_entity>
      <biological_entity id="o33978" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o33979" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" constraint="per side" constraintid="o33980" from="3" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="approximate" value_original="approximate" />
      </biological_entity>
      <biological_entity id="o33980" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o33981" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o33982" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often strongly; strongly; more or less" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o33983" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33984" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33985" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
      </biological_entity>
      <biological_entity id="o33986" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="abundant" />
      </biological_entity>
      <biological_entity id="o33987" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="short-crisped" value_original="short-crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 0–1.</text>
      <biological_entity constraint="cauline" id="o33988" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1–5 (–7) -flowered.</text>
      <biological_entity id="o33989" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-5(-7)-flowered" value_original="1-5(-7)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–4 cm in flower, to 5 cm in fruit.</text>
      <biological_entity id="o33990" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in flower" constraintid="o33991" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o33992" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33991" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o33992" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets narrowly to broadly lanceolate or elliptic, (2–) 4–7 × 0.6–1.7 mm, usually 1/4–1/2 as wide as sepals, margins flat, red-glands usually absent, sometimes sparse, inconspicuous;</text>
      <biological_entity id="o33993" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o33994" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" constraint="as-wide-as sepals" constraintid="o33995" from="1/4" modifier="usually" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity id="o33995" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o33996" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o33997" name="red-gland" name_original="red-glands" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="count_or_density" src="d0_s9" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium (2–) 3–4 mm diam.;</text>
      <biological_entity id="o33998" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33999" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (2.5–) 4–8 mm, apex acute;</text>
      <biological_entity id="o34000" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34001" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34002" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (3–) 4–8 × (3–) 5–9 mm, slightly longer than sepals;</text>
      <biological_entity id="o34003" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34004" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s12" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s12" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="9" to_unit="mm" />
        <character constraint="than sepals" constraintid="o34005" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o34005" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.9–1.2 mm, anthers 0.5 mm;</text>
      <biological_entity id="o34006" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34007" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34008" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 20–40, apical hairs absent, styles narrowly columnar or columnar-tapered, strongly papillate-swollen at very base, rarely in proximal 1/5–1/3, 0.7–1.2 mm.</text>
      <biological_entity id="o34009" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o34010" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="40" />
      </biological_entity>
      <biological_entity constraint="apical" id="o34011" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o34012" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="columnar" value_original="columnar" />
        <character is_modifier="false" name="shape" src="d0_s14" value="columnar-tapered" value_original="columnar-tapered" />
        <character constraint="at base" constraintid="o34013" is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
      <biological_entity id="o34013" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.1–1.5 mm. 2n = 56, 63;</text>
      <biological_entity constraint="2n" id="o34015" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="56" value_original="56" />
        <character name="quantity" src="d0_s15" value="63" value_original="63" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>28, 42, 49, 70 (Asia, Europe).</text>
      <biological_entity id="o34014" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
        <character name="quantity" src="d0_s16" value="49" value_original="49" />
        <character name="quantity" src="d0_s16" value="70" value_original="70" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Well-drained, exposed sites, ridge crests, coarse mineral soil, scree, usually on calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="well-drained" />
        <character name="habitat" value="exposed sites" />
        <character name="habitat" value="ridge crests" />
        <character name="habitat" value="coarse mineral soil" />
        <character name="habitat" value="calcareous substrates" modifier="scree usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska, Ariz., Colo., Mont., N.Mex., Utah, Wash., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Although now restricted to plants with exclusively (or at least predominantly) cottony hairs on the petioles, the name Potentilla nivea has a long history with an often wider application, sometimes including nearly all of sect. Niveae. As further confusion, J. Soják (1989) noted that the Linnaean type of P. nivea belonged to what is here treated as P. arenosa. Although historical usage of P. nivea has been re-established as a conserved name with a conserved type (B. Eriksen et al. 1999), from 1989 to 1999 the name P. nivea was applied to P. arenosa. During this period, P. prostrata subsp. floccosa was briefly adopted as the correct name for this species (for example, W. J. Cody 1996).</discussion>
  <discussion>Molecular evidence (B. Eriksen and M. H. Töpel 2006) indicates that populations of Potentilla nivea in the Atlantic regions, including Greenland and eastern Canada, differ from those in the Beringian regions of northwestern North America, suggesting expansion from separate Pleistocene refugia. A comparable pattern was noted by R. Elven and S. G. Aiken (2007) based on morphologic characters. The conserved type of P. nivea is from northern Sweden (Eriksen et al. 1999) and belongs to the Atlantic morphologic group. The variation within each region is large, and racial recognition would accordingly be premature. Plants from sites south of the continental glaciation, which were not included in the analysis by Eriksen and Töpel, deviate in having acuminate leaflet teeth and epicalyx bractlets and in being generally more slender. Epicalyx bractlets of some Washington plants are nearly as narrow as those of P. crebridens.</discussion>
  <discussion>Additional chromosome numbers have been reported for Potentilla nivea, but it is unknown whether these apply to this species, P. crebridens, some Asian relative, or hybrids.</discussion>
  
</bio:treatment>