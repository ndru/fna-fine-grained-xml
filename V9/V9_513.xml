<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="illustration_page">315</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agrimonia</taxon_name>
    <taxon_name authority="Wallroth" date="1842" rank="species">pubescens</taxon_name>
    <place_of_publication>
      <publication_title>Beitr. Bot.</publication_title>
      <place_in_publication>1: 45, plate 1, fig. 7. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus agrimonia;species pubescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416028</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agrimonia</taxon_name>
    <taxon_name authority="(Kearney) Rydberg" date="unknown" rank="species">bicknellii</taxon_name>
    <taxon_hierarchy>genus Agrimonia;species bicknellii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">eupatoria</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">mollis</taxon_name>
    <taxon_hierarchy>genus A.;species eupatoria;variety mollis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Britton" date="unknown" rank="species">mollis</taxon_name>
    <taxon_hierarchy>genus A.;species mollis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="Wallroth" date="unknown" rank="species">mollis</taxon_name>
    <taxon_name authority="Kearney" date="unknown" rank="variety">bicknellii</taxon_name>
    <taxon_hierarchy>genus A.;species mollis;variety bicknellii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">A.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">platycarpa</taxon_name>
    <taxon_hierarchy>genus A.;species platycarpa</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Soft agrimony</other_name>
  <other_name type="common_name">aigremoine pubescente</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 5–16 dm.</text>
      <biological_entity id="o26661" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="16" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots: tubers ± fusiform.</text>
      <biological_entity id="o26662" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o26663" name="tuber" name_original="tubers" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="fusiform" value_original="fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems with short-stipitate-glandular hairs and pubescent to villous and hirsute (hairs scattered, erect, 2–3 mm, stiff).</text>
      <biological_entity id="o26664" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" notes="" src="d0_s2" to="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o26665" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o26664" id="r1722" name="with" negation="false" src="d0_s2" to="o26665" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: mid cauline stipules ± broadly 1/2-ovate, margins dentate, apical lobes sometimes attenuate;</text>
      <biological_entity id="o26666" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid cauline" id="o26667" name="stipule" name_original="stipules" src="d0_s3" type="structure" />
      <biological_entity id="o26668" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o26669" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>major leaflets 3–13 (mid cauline 5–9), minor 1 or 1–3 pairs;</text>
      <biological_entity id="o26670" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26671" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="13" />
        <character is_modifier="false" name="structure_subtype" src="d0_s4" value="minor" value_original="minor" />
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>major leaflet blades elliptic or narrowly elliptic to lanceolate, sometimes ± obovate (lateral sometimes ± falcate), terminal largest, largest of these 3.3–9.8 × 1.4–5.5 cm, margins serrate to dentate, apex obtuse to acute or acuminate, abaxial surface rarely with glistening glandular-hairs, pubescent to pilose and sparsely hirsute (hairs stiff, 1–2 mm, usually densest along major veins).</text>
      <biological_entity id="o26672" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="leaflet" id="o26673" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="major" value_original="major" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character name="arrangement_or_shape" src="d0_s5" value="narrowly elliptic to lanceolate" value_original="narrowly elliptic to lanceolate" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character constraint="of" is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="3.3" from_unit="cm" name="length" src="d0_s5" to="9.8" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s5" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26674" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="serrate to dentate" value_original="serrate to dentate" />
      </biological_entity>
      <biological_entity id="o26675" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute or acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26676" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" notes="" src="d0_s5" to="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o26677" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="reflectance" src="d0_s5" value="glistening" value_original="glistening" />
      </biological_entity>
      <relation from="o26676" id="r1723" name="with" negation="false" src="d0_s5" to="o26677" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: axes with short-stipitate-glandular hairs to villous and hirsute (hairs stiff, ± scattered, usually erect proximally and 1–3 mm to ± ascending distally and 1 mm).</text>
      <biological_entity id="o26678" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o26679" name="axis" name_original="axes" src="d0_s6" type="structure" />
      <biological_entity id="o26680" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o26679" id="r1724" name="with" negation="false" src="d0_s6" to="o26680" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers usually ± alternate.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting hypanthia turbinate to campanulate, 1.9–4.5 × 2–4.6 mm, deeply to shallowly sulcate, hooked bristles in 3–4 circumferential rows, proximal row spreading ± 90° (pressed upward on dried specimens), glandular-hairy, grooves strigose, ridges sparsely hirsute (sepal bases usually pubescent along rim).</text>
      <biological_entity id="o26681" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="arrangement" src="d0_s7" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o26682" name="hypanthium" name_original="hypanthia" src="d0_s8" type="structure" />
      <biological_entity id="o26683" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s8" to="campanulate" />
        <character is_modifier="false" modifier="deeply to shallowly" name="architecture" src="d0_s8" value="sulcate" value_original="sulcate" />
        <character is_modifier="true" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o26684" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o26685" name="row" name_original="row" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="90°" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
      <biological_entity id="o26686" name="groove" name_original="grooves" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o26687" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o26681" id="r1725" name="fruiting" negation="false" src="d0_s8" to="o26682" />
      <relation from="o26683" id="r1726" name="in" negation="false" src="d0_s8" to="o26684" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges, open spaces, thickets, deciduous or mixed deciduous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" />
        <character name="habitat" value="open spaces" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="mixed deciduous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Agrimonia pubescens includes three recognizable morphs. The principal patterns are: leaves with major leaflets more or less obovate to more or less elliptic, 5–7 leaflets on mid cauline leaves, 1 minor pair; leaves with major leaflets more or less narrowly obovate to elliptic, 7 leaflets on mid cauline leaves, 0–1 or 1–3 minor pairs; and, leaves with major leaflets elliptic to narrowly elliptic or lanceolate, 7–9 leaflets on mid cauline leaves, 0–1 or 1–3 minor pairs. Lateral major leaflets are frequently more or less falcate in morphs two and three. Stipules with attenuate apical lobes are usual in morph three and frequent in morph two. No discontinuities in the variation occur within A. pubescens, and all patterns of variation are found throughout the range. Plants resembling A. microcarpa in major leaflet shape and in the number of major and minor leaflets can usually be identified as A. pubescens by stipule shape, the presence or absence of stiff hairs on mature hypanthia ridges, and/or the lengths of stem hairs. Additional study is needed to establish the phylogeny and perhaps a more precise taxonomy of this species as well as its relationship to A. microcarpa. Eupatorium molle (Torrey &amp; A. Gray) Nieuwland, which pertains here, is illegitimate.</discussion>
  
</bio:treatment>