<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="mention_page">624</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="J. B. Phipps" date="unknown" rank="series">Lacrimatae</taxon_name>
    <taxon_name authority="Small" date="1901" rank="species">lacrimata</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>1: 97. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series lacrimatae;species lacrimata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100117</other_info_on_name>
  </taxon_identification>
  <number>129.</number>
  <other_name type="common_name">Weeping hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 50 (–80) dm, branches strongly weeping.</text>
      <biological_entity id="o26963" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="some_measurement" src="d0_s0" unit="dm" value="50" value_original="50" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="some_measurement" src="d0_s0" unit="dm" value="50" value_original="50" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o26965" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="weeping" value_original="weeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth pubescent, 1–2-years old gray or purple-gray, slender;</text>
      <biological_entity id="o26966" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o26967" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o26968" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purple-gray" value_original="purple-gray" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs straight, 1–2-years old purple-gray, fine, 1.5–3 cm.</text>
      <biological_entity id="o26969" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o26970" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple-gray" value_original="purple-gray" />
        <character is_modifier="false" name="width" src="d0_s2" value="fine" value_original="fine" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26971" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o26970" id="r1771" name="on" negation="false" src="d0_s2" to="o26971" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole slender, length 20–30% blade, pubescent, glandularity not recorded;</text>
      <biological_entity id="o26972" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o26973" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="20-30%" name="length" notes="" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o26974" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade oblong or ± narrowly obtrullate, sometimes elliptic, 1–2 cm, thin, base cuneate, lobes 0, sometimes very slightly lobed subterminally, margins subentire or finely serrate in distal 1/2, veins 1 or 2 per side (exiting in distal 1/2 of leaf), apex usually truncate to obtuse, sometimes cuspidately subacute, surfaces glabrous.</text>
      <biological_entity id="o26975" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26976" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="more or less narrowly" name="shape" src="d0_s4" value="obtrullate" value_original="obtrullate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o26977" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o26978" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes very slightly; subterminally" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o26979" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
        <character constraint="in distal 1/2" constraintid="o26980" is_modifier="false" modifier="finely" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26980" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o26981" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26982" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o26983" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually truncate" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" modifier="sometimes cuspidately" name="shape" src="d0_s4" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity id="o26984" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3-flowered;</text>
      <biological_entity id="o26985" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous;</text>
      <biological_entity id="o26986" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles ± caducous, linear, margins eglandular or nearly so, glabrous.</text>
      <biological_entity id="o26987" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s7" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o26988" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character name="architecture" src="d0_s7" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 15 mm diam.;</text>
      <biological_entity id="o26989" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character name="diameter" src="d0_s8" unit="mm" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o26990" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals triangular, 2–3 mm, margins ± entire, abaxially glabrous;</text>
      <biological_entity id="o26991" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26992" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers cream;</text>
      <biological_entity id="o26993" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3 or 4.</text>
      <biological_entity id="o26994" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s12" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes yellow, yellow blushed red, or red, suborbicular, 8 mm diam., glabrous;</text>
      <biological_entity id="o26995" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow blushed" value_original="yellow blushed" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular" value_original="suborbicular" />
        <character name="diameter" src="d0_s13" unit="mm" value="8" value_original="8" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepal remnants patent-reflexed;</text>
      <biological_entity constraint="sepal" id="o26996" name="remnant" name_original="remnants" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="patent-reflexed" value_original="patent-reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 3 or 4.</text>
      <biological_entity id="o26997" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s15" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinewoods, open scrub, sandy soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinewoods" />
        <character name="habitat" value="open scrub" />
        <character name="habitat" value="sandy soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus lacrimata is found abundantly over parts of the Florida panhandle and through adjacent Alabama and Georgia to South Carolina.</discussion>
  <discussion>Crataegus lacrimata is an upright, usually single-stemmed shrub with slender, more or less weeping branches and small, narrow, glabrous, bright green leaves. The plants are easily recognized among ser. Lacrimatae, the whole plant being almost completely glabrous, having narrow, nearly unlobed leaves, and being the only member of the series to possess more or less eglandular bracteoles. Only with poor material might the pubescent inflorescence of C. munda (dwarf, non-lacrimate) or C. crocea (tall, lacrimate) be confused. A form similar to C. lacrimata and with the same range, but apparently discontinuously larger, has longer leaves (blades 2.2–4 cm versus 1–1.7 cm) and larger flowers (petals 7–9 mm versus 5–7 mm) and more orange-colored ripe fruit.</discussion>
  
</bio:treatment>