<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">Rubeae</taxon_name>
    <place_of_publication>
      <publication_title>Anal. Fam. Pl.,</publication_title>
      <place_in_publication>39. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe Rubeae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20916</other_info_on_name>
  </taxon_identification>
  <number>a2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, subshrubs, or herbs, perennial;</text>
      <biological_entity id="o18066" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>armed or unarmed.</text>
      <biological_entity id="o18067" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="armed" value_original="armed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, imparipinnately or palmately compound or simple;</text>
      <biological_entity id="o18069" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="imparipinnately; palmately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules persistent [deciduous], free or adnate to petiole;</text>
      <biological_entity id="o18071" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>venation pinnate or palmate.</text>
      <biological_entity id="o18070" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character constraint="to petiole" constraintid="o18071" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: perianth and androecium perigynous;</text>
      <biological_entity id="o18072" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18073" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o18074" name="androecium" name_original="androecium" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>epicalyx bractlets absent;</text>
      <biological_entity id="o18075" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="epicalyx" id="o18076" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium flat to hemispheric;</text>
      <biological_entity id="o18077" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18078" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>torus flat or convex to conic;</text>
      <biological_entity id="o18079" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18080" name="torus" name_original="torus" src="d0_s8" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s8" to="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>carpels 5–150, styles apical, distinct;</text>
      <biological_entity id="o18081" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18082" name="carpel" name_original="carpels" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="150" />
      </biological_entity>
      <biological_entity id="o18083" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="apical" value_original="apical" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 2, apical, collateral, only 1 maturing.</text>
      <biological_entity id="o18084" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18085" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s10" value="apical" value_original="apical" />
        <character is_modifier="false" name="position" src="d0_s10" value="collateral" value_original="collateral" />
        <character modifier="only" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits aggregated drupelets;</text>
      <biological_entity id="o18086" name="fruit" name_original="fruits" src="d0_s11" type="structure" />
      <biological_entity id="o18087" name="drupelet" name_original="drupelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles persistent, not elongate.</text>
      <biological_entity id="o18088" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Pacific Islands, Australia; introduced nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genus 1, species 250–700 (37 in the flora).</discussion>
  <discussion>The base chromosome number for Rubeae is x = 7. The tribe is host to Phragmidium rusts.</discussion>
  
</bio:treatment>