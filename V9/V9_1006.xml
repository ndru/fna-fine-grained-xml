<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">586</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Intricatae</taxon_name>
    <taxon_name authority="J. B. Phipps &amp; O’Kennon" date="1997" rank="species">nananixonii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>17: 569, fig. 1. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series intricatae;species nananixonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100135</other_info_on_name>
  </taxon_identification>
  <number>95.</number>
  <other_name type="common_name">Nixon’s dwarf hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–15 dm.</text>
      <biological_entity id="o34216" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems intricately branched;</text>
      <biological_entity id="o34217" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs: new growth reddish green, pubescent, 1-year old gray to gray-brown, older gray;</text>
      <biological_entity id="o34218" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <biological_entity id="o34219" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="old" value_original="old" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s2" to="gray-brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs straight to slightly recurved, 2-years old dark dull gray, fine, 1–3 cm.</text>
      <biological_entity id="o34220" name="twig" name_original="twigs" src="d0_s3" type="structure" />
      <biological_entity id="o34221" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark" value_original="dark" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="width" src="d0_s3" value="fine" value_original="fine" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34222" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o34221" id="r2249" name="on" negation="false" src="d0_s3" to="o34222" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole length 20–40% blade, pubescence not recorded, gland-dotted;</text>
      <biological_entity id="o34223" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o34224" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="20-40%; not" name="length" notes="" src="d0_s4" value="pubescence" value_original="pubescence" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity id="o34225" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade rhombic-ovate, 1.5–3.5 cm, base broadly cuneate, lobes 0 or with sinuses very shallow, margins ± irregularly serrate, some teeth gland-tipped, especially proximally, veins 3 per side, apex acute, abaxial surface glabrous, veins sparsely pubescent, adaxial sparsely scabrous young, glabrescent.</text>
      <biological_entity id="o34226" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o34227" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34228" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o34229" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="with sinuses" value_original="with sinuses" />
      </biological_entity>
      <biological_entity id="o34230" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very" name="depth" src="d0_s5" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o34231" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less irregularly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o34232" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o34233" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character constraint="per side" constraintid="o34234" modifier="especially proximally; proximally" name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o34234" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o34235" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o34236" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o34237" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34238" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <relation from="o34229" id="r2250" name="with" negation="false" src="d0_s5" to="o34230" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 3–5-flowered;</text>
      <biological_entity id="o34239" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-5-flowered" value_original="3-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches pubescent;</text>
      <biological_entity id="o34240" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteole margins short-stipitate-glandular.</text>
      <biological_entity constraint="bracteole" id="o34241" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 12–15 mm diam.;</text>
      <biological_entity id="o34242" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o34243" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals lanceolate, 4–5 mm, margins glandular, abaxially glabrous;</text>
      <biological_entity id="o34244" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34245" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals slightly clawed;</text>
      <biological_entity id="o34246" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10, anthers rose-purple;</text>
      <biological_entity id="o34247" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o34248" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="rose-purple" value_original="rose-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3–5.</text>
      <biological_entity id="o34249" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes coppery red, suborbicular, 10 mm diam., glabrous;</text>
      <biological_entity id="o34250" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="coppery red" value_original="coppery red" />
        <character is_modifier="false" name="shape" src="d0_s15" value="suborbicular" value_original="suborbicular" />
        <character name="diameter" src="d0_s15" unit="mm" value="10" value_original="10" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepal remnants present or not, on collar, patent-reflexed;</text>
      <biological_entity constraint="sepal" id="o34251" name="remnant" name_original="remnants" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s16" value="not" value_original="not" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s16" value="patent-reflexed" value_original="patent-reflexed" />
      </biological_entity>
      <biological_entity id="o34252" name="collar" name_original="collar" src="d0_s16" type="structure" />
      <relation from="o34251" id="r2251" name="on" negation="false" src="d0_s16" to="o34252" />
    </statement>
    <statement id="d0_s17">
      <text>pyrenes 3–5.</text>
      <biological_entity id="o34253" name="pyrene" name_original="pyrenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sites" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus nananixonii is restricted to open sandy sites in Nacogdoches and nearby counties of eastern Texas. The species is a very dwarf and distinctive member of ser. Intricatae, similar to C. biltmoreana but with much smaller parts.</discussion>
  
</bio:treatment>