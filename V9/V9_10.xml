<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles T. Mason Jr.†,George Yatskievych</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
    <other_info_on_meta type="treatment_page">9</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">CROSSOSOMATACEAE</taxon_name>
    <taxon_hierarchy>family CROSSOSOMATACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10226</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, rarely trees.</text>
      <biological_entity id="o30394" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems intricately branched, often spinescent;</text>
      <biological_entity id="o30396" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="spinescent" value_original="spinescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark becoming finely and irregularly grooved, usually peeling in thin longitudinal plates.</text>
      <biological_entity id="o30397" name="bark" name_original="bark" src="d0_s2" type="structure" />
      <biological_entity id="o30398" name="plate" name_original="plates" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="true" modifier="becoming finely; irregularly" name="architecture" src="d0_s2" value="grooved" value_original="grooved" />
      </biological_entity>
      <relation from="o30397" id="r2004" name="peeling" negation="false" src="d0_s2" to="o30398" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous or drought-deciduous, cauline, alternate or opposite, sometimes appearing fasciculate on short-shoots, simple;</text>
      <biological_entity id="o30399" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s3" value="drought-deciduous" value_original="drought-deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o30400" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o30401" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <relation from="o30400" id="r2005" modifier="sometimes" name="appearing" negation="false" src="d0_s3" to="o30401" />
    </statement>
    <statement id="d0_s4">
      <text>stipules absent or relatively small;</text>
      <biological_entity id="o30402" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="relatively" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole absent or short, base usually slightly expanded, thickened and persistent (in Glossopetalon);</text>
      <biological_entity id="o30403" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o30404" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually slightly" name="size" src="d0_s5" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade herbaceous to slightly coriaceous, margins entire or apically 2–3-lobed (in Apacheria).</text>
      <biological_entity id="o30405" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s6" to="slightly coriaceous" />
      </biological_entity>
      <biological_entity id="o30406" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s6" value="2-3-lobed" value_original="2-3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal on short-shoots, sometimes axillary or appearing so, usually flowers solitary, occasionally 2–3-flowered clusters (in Glossopetalon), short-shoots or pedicels usually with minute, scarious or hardened bractlike structures at base.</text>
      <biological_entity id="o30407" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character constraint="on short-shoots" constraintid="o30408" is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="position" notes="" src="d0_s7" value="axillary" value_original="axillary" />
        <character name="position" src="d0_s7" value="appearing so , usually flowers" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o30408" name="short-shoot" name_original="short-shoots" src="d0_s7" type="structure" />
      <biological_entity id="o30409" name="short-shoot" name_original="short-shoots" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s7" value="2-3-flowered" value_original="2-3-flowered" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o30410" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="occasionally" name="architecture" src="d0_s7" value="2-3-flowered" value_original="2-3-flowered" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o30411" name="structure" name_original="structures" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" value_original="minute" />
        <character is_modifier="true" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s7" value="hardened" value_original="hardened" />
        <character is_modifier="true" name="shape" src="d0_s7" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity id="o30412" name="base" name_original="base" src="d0_s7" type="structure" />
      <relation from="o30409" id="r2006" name="with" negation="false" src="d0_s7" to="o30411" />
      <relation from="o30410" id="r2007" name="with" negation="false" src="d0_s7" to="o30411" />
      <relation from="o30411" id="r2008" name="at" negation="false" src="d0_s7" to="o30412" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, perianth and androecium perigynous;</text>
      <biological_entity id="o30413" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o30414" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o30415" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlet absent;</text>
      <biological_entity constraint="epicalyx" id="o30416" name="bractlet" name_original="bractlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium saucer-shaped to shallowly cupshaped [narrowly funnelform (in Velascoa)];</text>
      <biological_entity id="o30417" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="saucer-shaped" name="shape" src="d0_s10" to="shallowly cupshaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (3–) 4–5 (–6), distinct;</text>
      <biological_entity id="o30418" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (3–) 4–5 (–6), distinct, base often short-clawed;</text>
      <biological_entity id="o30419" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s12" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o30420" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="short-clawed" value_original="short-clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectar disc absent or crenately lobed, fleshy or thin;</text>
      <biological_entity constraint="nectar" id="o30421" name="disc" name_original="disc" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="crenately" name="shape" src="d0_s13" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 4–50, distinct, sometimes unequal in length, anthers attached at or near bases, yellow, dehiscing by longitudinal slits;</text>
      <biological_entity id="o30422" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s14" to="50" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes" name="length" src="d0_s14" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o30423" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="at bases" constraintid="o30424" is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="yellow" value_original="yellow" />
        <character constraint="by slits" constraintid="o30425" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o30424" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o30425" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils 1–9, distinct, free, short-stipitate or sessile, styles usually ± oblique, often indistinct, stigmas capitate or relatively short, sometimes linear, ventrally decurrent;</text>
      <biological_entity id="o30426" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="9" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="short-stipitate" value_original="short-stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o30427" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="orientation_or_shape" src="d0_s15" value="oblique" value_original="oblique" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s15" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o30428" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="ventrally" name="shape" src="d0_s15" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 1–22+.</text>
      <biological_entity id="o30429" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="22" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits follicles, angled or short-tapered at both ends, coriaceous.</text>
      <biological_entity constraint="fruits" id="o30430" name="follicle" name_original="follicles" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="angled" value_original="angled" />
        <character constraint="at ends" constraintid="o30431" is_modifier="false" name="shape" src="d0_s17" value="short-tapered" value_original="short-tapered" />
        <character is_modifier="false" name="texture" notes="" src="d0_s17" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o30431" name="end" name_original="ends" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1–22 per follicle, arillate.</text>
      <biological_entity id="o30432" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per follicle" constraintid="o30433" from="1" name="quantity" src="d0_s18" to="22" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o30433" name="follicle" name_original="follicle" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 4, species ca. 8 (3 genera, 7 species in the flora).</discussion>
  <discussion>For years, Crossosomataceae were thought to comprise only the type genus. In 1975, the circumscription was expanded with the discovery of Apacheria, and three years later it was enlarged further with the transfer of Glossopetalon from the Celastraceae (R. F. Thorne and R. Scoggin 1978). A fourth genus, the monospecific Velascoa Calderón &amp; Rzedowski, endemic to northeastern Querétaro, Mexico, was described in 1997. Velascoa recondita Calderón &amp; Rzedowski is unique in the family in its elongate, narrowly funnelform hypanthium and nearly sessile stamens.</discussion>
  <discussion>The affinities of the family have been controversial. A. Cronquist (1981) outlined the problems, noting that the presence of an aril (and some embryologic features) suggested a placement in the Dilleniales, but he chose instead to classify the family tentatively in his concept of Rosales, near the Rosaceae, based on superficial floral similarities such as perigyny. V. Sosa and M. W. Chase (2003), who studied DNA sequence variation, and M. L. Matthews and P. K. Endress (2005), who studied developmental floral morphology, independently concluded that the Crossosomatales should be segregated as a distinct order (including such morphologically diverse families as Stachyuraceae and Staphyleaceae). The circumscription and placement of this order in relation to other Rosids are still uncertain.</discussion>
  <references>
    <reference>Sosa, V. and M. W. Chase. 2003. Phylogenetics of Crossosomataceae based on rbcL sequence data. Syst. Bot. 28: 96–105.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 15–50; sepals and petals 5; petals 9–18 mm; follicles finely transversely verrucose or indistinctly reticulate; seeds 4–22 per follicle; aril deeply fimbriate.</description>
      <determination>2 Crossosoma</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 8 or 4–10; sepals and petals 4 or 3–5(–6); petals 4–5 mm or 2–9 mm; follicles longitudinally striate; seeds 1 or 2 per follicle; aril irregularly discoid or ± fimbrillate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves opposite, sometimes appearing fasciculate on short shoots, blade margins entire or apically 2–3-lobed; stigmas linear; sepals and petals 4.</description>
      <determination>1 Apacheria</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves alternate, blade margins entire; stigmas capitate, oblique; sepals and petals 3–5(–6).</description>
      <determination>3 Glossopetalon</determination>
    </key_statement>
  </key>
</bio:treatment>