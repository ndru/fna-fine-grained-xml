<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">padus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 473. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species padus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242341600</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">European bird cherry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, not suckering, 50–150 dm, not thorny.</text>
      <biological_entity id="o19806" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="50" from_unit="dm" name="some_measurement" src="d0_s0" to="150" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with terminal end buds, glabrous or puberulent.</text>
      <biological_entity id="o19807" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="end" id="o19808" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="terminal end" />
      <relation from="o19807" id="r1296" name="with" negation="false" src="d0_s1" to="o19808" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o19809" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 8–17 mm, glabrous or puberulent on adaxial surface or both surfaces, glandular distally, glands 1–4;</text>
      <biological_entity id="o19810" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="17" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="on " constraintid="o19812" is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="distally" name="architecture_or_function_or_pubescence" notes="" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19811" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <biological_entity id="o19812" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o19813" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to obovate, 5–10 (–13) × 2.5–4.5 (–7) cm, base obtuse to rounded or subcordate, margins serrate, teeth ascending to spreading, sharp, eglandular, apex acuminate to abruptly so, lateral-veins 10–18 per side, raised abaxially, surfaces glabrous or abaxial hairy in vein-axils or along midribs.</text>
      <biological_entity id="o19814" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19815" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded or subcordate" />
      </biological_entity>
      <biological_entity id="o19816" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o19817" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o19818" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o19819" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o19820" from="10" modifier="abruptly" name="quantity" src="d0_s4" to="18" />
        <character is_modifier="false" modifier="abaxially" name="prominence" notes="" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o19820" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o19821" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19822" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character constraint="in vein-axils or along midribs" constraintid="o19823" is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19823" name="midrib" name_original="midribs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 15–50-flowered, racemes;</text>
      <biological_entity id="o19824" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="15-50-flowered" value_original="15-50-flowered" />
      </biological_entity>
      <biological_entity id="o19825" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central axes 45–150 mm, leafy at bases.</text>
      <biological_entity constraint="central" id="o19826" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character char_type="range_value" from="45" from_unit="mm" name="some_measurement" src="d0_s6" to="150" to_unit="mm" />
        <character constraint="at bases" constraintid="o19827" is_modifier="false" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o19827" name="base" name_original="bases" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 3–17 mm, glabrous.</text>
      <biological_entity id="o19828" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="17" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers blooming after leaf emergence;</text>
      <biological_entity id="o19829" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="after leaf" constraintid="o19830" is_modifier="false" name="life_cycle" src="d0_s8" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o19830" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>hypanthium cupulate, 2–2.5 mm, glabrous externally;</text>
      <biological_entity id="o19831" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals broadly spreading to reflexed, oblong-ovate, 1.2–2 mm (lengths greater than widths), margins glandular-toothed, surfaces glabrous;</text>
      <biological_entity id="o19832" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly spreading" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19833" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o19834" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, obovate to suborbiculate, (5–) 6–9 mm;</text>
      <biological_entity id="o19835" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="suborbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries glabrous.</text>
      <biological_entity id="o19836" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Drupes black, globose, 6–8 mm, glabrous;</text>
      <biological_entity id="o19837" name="drupe" name_original="drupes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium deciduous, leaving discs at bases of drupes;</text>
      <biological_entity id="o19838" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o19839" name="disc" name_original="discs" src="d0_s14" type="structure" />
      <biological_entity id="o19840" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o19841" name="drupe" name_original="drupes" src="d0_s14" type="structure" />
      <relation from="o19838" id="r1297" name="leaving" negation="false" src="d0_s14" to="o19839" />
      <relation from="o19838" id="r1298" name="at" negation="false" src="d0_s14" to="o19840" />
      <relation from="o19840" id="r1299" name="part_of" negation="false" src="d0_s14" to="o19841" />
    </statement>
    <statement id="d0_s15">
      <text>mesocarps fleshy;</text>
      <biological_entity id="o19842" name="mesocarp" name_original="mesocarps" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stones subglobose, not flattened, rugulose.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 32.</text>
      <biological_entity id="o19843" name="stone" name_original="stones" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="relief" src="d0_s16" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19844" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., Ont.; Alaska, Del., Ill., Mont., N.J., N.Y., Pa., Utah, Wash.; Eurasia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Prunus padus is cultivated in North America as an ornamental prized for its long, showy racemes of white flowers and its cold hardiness. Its value in the upper Midwest is often compromised by fungal infection causing black knot disease.</discussion>
  <discussion>Prunus padus is difficult to distinguish from P. virginiana var. demissa, at least from herbarium material. Induments of hypanthium interior have been widely used in keys to separate the two: hairy in P. padus versus glabrous in P. virginiana. As noted by E. G. Voss (1972–1996, vol. 2), many of the specimens of P. virginiana have a pubescent hypanthium, especially basally. This is true throughout the range of P. virginiana, and also for some specimens of P. serotina. The hypanthia of P. padus are more hairy than those of P. virginiana. The petals of P. padus are longer than those of eastern chokecherry (P. virginiana var. virginiana), making it possible to distinguish flowering specimens from east of the Rocky Mountains. The petals are only slightly longer, on average, than those of P. virginiana var. demissa. Petals of P. padus are also a bit narrower and more elliptic. Shape of sepals may prove useful: 1.2–2 mm with lengths greater than widths in P. padus versus 0.7–1.4 mm with lengths equal to widths in P. virginiana var. demissa. In fruit, the difference between rugulose stones of P. padus and smooth stones of P. virginiana is subtle, perhaps too subtle for those unfamiliar with the range in variation of pit surfaces among these taxa.</discussion>
  
</bio:treatment>