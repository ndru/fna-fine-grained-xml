<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="illustration_page">391</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">sorbarieae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="1832" rank="genus">adenostoma</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">fasciculatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">fasciculatum</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe sorbarieae;genus adenostoma;species fasciculatum;variety fasciculatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100528</other_info_on_name>
  </taxon_identification>
  <number>1a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect-ascending, spreading (plants tall);</text>
      <biological_entity id="o11961" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>internodes 2.5–7 (–13) mm;</text>
      <biological_entity id="o11962" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>young stems glabrous, sometimes moreorless hirtellous.</text>
      <biological_entity id="o11963" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes more or less" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves oblanceolate to linear, semiterete, (2.5–) 5–13 mm (not or slightly expanded distally), apex acute-apiculate.</text>
      <biological_entity id="o11964" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="semiterete" value_original="semiterete" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity id="o11965" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="acute-apiculate" value_original="acute-apiculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11966" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jun(–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes, ridges, lower chaparral and coastal sage scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="lower chaparral" />
        <character name="habitat" value="coastal sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1800(–2100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety fasciculatum is a major component of lower chaparral, upper coastal sage scrub from northern, central, and southern cismontane California and northern Baja California.</discussion>
  
</bio:treatment>