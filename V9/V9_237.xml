<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">163</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Leucophyllae</taxon_name>
    <taxon_name authority="Douglas ex Lehmann" date="1830" rank="species">effusa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">effusa</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section leucophyllae;species effusa;variety effusa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100688</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">effusa</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="variety">filicaulis</taxon_name>
    <taxon_hierarchy>genus Potentilla;species effusa;variety filicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hippiana</taxon_name>
    <taxon_name authority="(Nuttall) B. Boivin" date="unknown" rank="variety">filicaulis</taxon_name>
    <taxon_hierarchy>genus P.;species hippiana;variety filicaulis</taxon_hierarchy>
  </taxon_identification>
  <number>31a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaflets gray to white, teeth (1–) 4–9 per side, abaxial surfaces moderately long-hairy especially on veins, moderately to densely cottony, adaxial moderately long-hairy and abundantly cottony.</text>
      <biological_entity id="o6611" name="leaflet" name_original="leaflets" src="d0_s0" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s0" to="white" />
      </biological_entity>
      <biological_entity id="o6612" name="tooth" name_original="teeth" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s0" to="4" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o6613" from="4" name="quantity" src="d0_s0" to="9" />
      </biological_entity>
      <biological_entity id="o6613" name="side" name_original="side" src="d0_s0" type="structure" />
      <biological_entity constraint="abaxial" id="o6614" name="surface" name_original="surfaces" src="d0_s0" type="structure">
        <character constraint="on veins" constraintid="o6615" is_modifier="false" modifier="moderately" name="pubescence" src="d0_s0" value="long-hairy" value_original="long-hairy" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" notes="" src="d0_s0" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o6615" name="vein" name_original="veins" src="d0_s0" type="structure" />
      <biological_entity constraint="adaxial" id="o6616" name="surface" name_original="surfaces" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s0" value="long-hairy" value_original="long-hairy" />
        <character is_modifier="false" modifier="abundantly" name="pubescence" src="d0_s0" value="cottony" value_original="cottony" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: epicalyx bractlets and sepals usually abundantly cottony at least proximally;</text>
      <biological_entity id="o6617" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity constraint="epicalyx" id="o6618" name="bractlet" name_original="bractlets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually abundantly; at-least proximally; proximally" name="pubescence" src="d0_s1" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity constraint="epicalyx" id="o6619" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually abundantly; at-least proximally; proximally" name="pubescence" src="d0_s1" value="cottony" value_original="cottony" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>anthers (0.6–) 0.7–1 mm. 2n = 56.</text>
      <biological_entity id="o6620" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o6621" name="anther" name_original="anthers" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6622" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky slopes, slightly moist meadows, grasslands, oak, aspen, and montane conifer woodlands (limestone pavement)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky slopes" />
        <character name="habitat" value="moist meadows" modifier="slightly" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="montane conifer woodlands" />
        <character name="habitat" value="limestone pavement" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Colo., Idaho, Minn., Mont., Nebr., N.Mex., N.Dak., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety effusa occurs from southern Alberta to Manitoba southward on plains and in the mountains to central Colorado. Disjunct populations from northern foothills of the Uinta Mountains in Utah are included in var. effusa by B. C. Johnston (1980), but relegated to Potentilla hippiana by N. H. Holmgren (1997b). Verification is needed for single occurrences of the variety mapped by Johnston in eastern Idaho, northwestern Nebraska, and north-central New Mexico, without citation of specimens. A single collection from Valley County, Idaho, (E. A. Christenson 42, CONN) is almost certainly an introduction of unknown persistence.</discussion>
  <discussion>The small, narrow, cottony-floccose epicalyx bractlets tend to be glabrescent at least distally, contrasting with the sharply acuminate sepals. Variety effusa, in its most distinctive form, has unevenly pinnate leaves with coarsely toothed gray leaflets that are equally cottony-floccose on both surfaces.</discussion>
  <discussion>Collections of otherwise typical var. effusa from central Wyoming and adjacent Montana are uniformly smaller than average with only one to four teeth per side of the leaflets. These occur mainly on limestone outcrops (1300–2600 m elevation) and might qualify as a distinct variety.</discussion>
  
</bio:treatment>