<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">Agrimonieae</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl. Fl. Gall.,</publication_title>
      <place_in_publication>333. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe Agrimonieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20912</other_info_on_name>
  </taxon_identification>
  <number>a6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial (annual or biennial in Poteridium) [shrubs or trees];</text>
    </statement>
    <statement id="d0_s1">
      <text>unarmed (hypanthia armed in Acaena).</text>
      <biological_entity id="o16674" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, odd-pinnately compound;</text>
      <biological_entity id="o16675" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="odd-pinnately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules persistent (absent in Acaena), adnate to petiole (free in Poterium);</text>
      <biological_entity id="o16677" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>venation pinnate.</text>
      <biological_entity id="o16676" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o16677" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: perianth and androecium perigynous;</text>
      <biological_entity id="o16678" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o16679" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o16680" name="androecium" name_original="androecium" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>epicalyx bractlets absent;</text>
      <biological_entity id="o16681" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="epicalyx" id="o16682" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium hemispheric, obconic, ovoid, urceolate, top-shaped, ellipsoid, nearly orbicular, or obtriangular;</text>
      <biological_entity id="o16683" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16684" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="top--shaped" value_original="top--shaped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s7" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtriangular" value_original="obtriangular" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s7" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtriangular" value_original="obtriangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>torus absent;</text>
      <biological_entity id="o16685" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16686" name="torus" name_original="torus" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>carpels 1 or 2 (or 3), rarely more, styles terminal, distinct;</text>
      <biological_entity id="o16687" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16688" name="carpel" name_original="carpels" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s9" unit="or" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16689" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="rarely" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovule 1, apical.</text>
      <biological_entity id="o16690" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16691" name="ovule" name_original="ovule" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s10" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits achenes, enclosed within enlarged, often hardened, sometimes armed hypanthia;</text>
      <biological_entity constraint="fruits" id="o16692" name="achene" name_original="achenes" src="d0_s11" type="structure" />
      <biological_entity id="o16693" name="hypanthium" name_original="hypanthia" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" modifier="often" name="texture" src="d0_s11" value="hardened" value_original="hardened" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s11" value="armed" value_original="armed" />
      </biological_entity>
      <relation from="o16692" id="r1076" name="enclosed within" negation="false" src="d0_s11" to="o16693" />
    </statement>
    <statement id="d0_s12">
      <text>styles deciduous, not elongate.</text>
      <biological_entity id="o16694" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies (Hispaniola), South America, Eurasia, Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Agrimoniaceae</other_name>
  <discussion>Genera 12, species ca. 270 (5 genera, 17 species in the flora).</discussion>
  <discussion>The base chromosome number for Agrimonieae is x = 7. Acaena and Sanguisorba are host to Phragmidium rusts. The tribal name Agrimonieae has priority over Sanguisorbeae, used by, among others, D. Potter et al. (2007). Agrimonieae also includes the genera Aremonia Necker ex Nestler (Europe), Cliffortia Linnaeus, Hagenia J. F. Gmelin and Leucosidea Ecklon &amp; Zeyher (Africa), Margyricarpus Ruiz &amp; Pavón and Polylepis Ruiz &amp; Pavón (South America), and Spenceria Trimen (Asia) (Potter et al.).</discussion>
  <references>
    <reference>Kerr, M. S. 2004. A Phylogenetic and Biogeographic Analysis of Sanguisorbeae (Rosaceae), with Emphasis on the Pleistocene Radiation of the High Andean Genus Polylepis. Ph.D. dissertation. University of Maryland.</reference>
    <reference>Nordborg, G. 1966. Sanguisorba L., Sarcopoterium Spach, and Bencomia Webb et Berth.: Delimitation and subdivision of the genera. Opera Bot. 11: 1–103.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems creeping or suberect; hypanthia spiny.</description>
      <determination>26 Acaena</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems ascending to erect; hypanthia not spiny (rim bristly in fruit in Agrimonia)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences racemes (simple or compound); leaf blades with minor leaflet pairs between major pairs; petals 5 (yellow); hypanthial rim with 2–5 rows of bristles.</description>
      <determination>22 Agrimonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences spikes (cylindric, globose, or headlike); leaf blades without minor leaflet pairs between major pairs; petals 0; hypanthial rim without rows of bristles</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet margins pectinately pinnatisect.</description>
      <determination>24 Poteridium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet margins crenate, serrate, or incised</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spikes headlike; flowers bisexual or pistillate (plants gynomonoecious); sepals distinct.</description>
      <determination>23 Poterium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Spikes ellipsoid to cylindric; flowers bisexual; sepals basally connate.</description>
      <determination>25 Sanguisorba</determination>
    </key_statement>
  </key>
</bio:treatment>