<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens" date="1959" rank="section">unguiculatae</taxon_name>
    <taxon_name authority="(J. T. Howell) Munz" date="1968" rank="species">aperta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">aperta</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section unguiculatae;species aperta;variety aperta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250100664</other_info_on_name>
  </taxon_identification>
  <number>23a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect.</text>
      <biological_entity id="o32755" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers (20–) 30–250, usually more than 10 per glomerule, 5–10 mm diam.;</text>
      <biological_entity id="o32756" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s1" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s1" to="250" />
        <character char_type="range_value" constraint="per glomerule" constraintid="o32757" from="10" modifier="usually" name="quantity" src="d0_s1" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" notes="" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32757" name="glomerule" name_original="glomerule" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>hypanthium 2–3 (–4) mm diam.;</text>
      <biological_entity id="o32758" name="hypanthium" name_original="hypanthium" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petals oblanceolate, 2–3 mm, shorter than sepals;</text>
      <biological_entity id="o32759" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character constraint="than sepals" constraintid="o32760" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o32760" name="sepal" name_original="sepals" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>filaments 1–1.5 (–2) mm;</text>
      <biological_entity id="o32761" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>styles 2–3 mm. 2n = 28.</text>
      <biological_entity id="o32762" name="style" name_original="styles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32763" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry meadows, edges of seeps, slopes, and flats, on vernally saturated volcanic soil, in sagebrush and grass communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry meadows" />
        <character name="habitat" value="edges" constraint="of seeps , slopes , and flats" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="vernally" />
        <character name="habitat" value="volcanic soil" modifier="saturated" constraint="in sagebrush and grass communities , conifer woodlands" />
        <character name="habitat" value="sagebrush" modifier="in" />
        <character name="habitat" value="grass communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety aperta is most abundant in Sierra Valley and its drainages in Plumas and Sierra counties, California, barely extending into Lassen County. Disjunct populations occur in the Carson Range in southern Washoe County and the Virginia Range of Storey County, Nevada.</discussion>
  <discussion>The chromosome count given here is based on a collection originally identified as Ivesia sericoleuca (D. D. Keck &amp; A. Gustafsson 4901).</discussion>
  
</bio:treatment>