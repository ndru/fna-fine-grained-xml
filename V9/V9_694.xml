<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard Lis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">412</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) Rydberg" date="unknown" rank="genus">Petrophytum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 206. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus Petrophytum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek petros, rock, and phyton, plant, alluding to habitat</other_info_on_name>
    <other_info_on_name type="fna_id">314663</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Nuttall ex Torrey &amp; A. Gray" date="unknown" rank="unranked">Petrophytum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 418. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;unranked Petrophytum</taxon_hierarchy>
  </taxon_identification>
  <number>45.</number>
  <other_name type="common_name">Rock-spiraea</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, cespitose, densely matted, 0.1–10 dm.</text>
      <biological_entity id="o20306" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1+, prostrate, decumbent, erect, or ascending;</text>
      <biological_entity id="o20307" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" upper_restricted="false" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark brown to dark-brown, aging to gray;</text>
      <biological_entity id="o20308" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s2" to="dark-brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>short and long-shoots present;</text>
      <biological_entity id="o20309" name="long-shoot" name_original="long-shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>short-shoots glabrous.</text>
      <biological_entity id="o20310" name="short-shoot" name_original="short-shoots" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent, marcescent, cauline (tightly clustered), alternate, simple;</text>
      <biological_entity id="o20311" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o20312" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole absent;</text>
      <biological_entity id="o20313" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate to narrowly obtrullate, 0.2–2.5 (–3) cm, coriaceous, margins flat, entire, venation indistinct, masked by hairs, or palmately 1–3-veined, surfaces glabrate, strigose, canescent, pilose, or sericeous.</text>
      <biological_entity id="o20314" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="narrowly obtrullate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o20315" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o20316" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity id="o20317" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <relation from="o20315" id="r1331" name="masked by" negation="false" src="d0_s7" to="o20316" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, 10–100-flowered, panicles narrow or widely branched, flowers aggregated, dense, compact, puberulent, canescent to sericeous;</text>
      <biological_entity id="o20318" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-100-flowered" value_original="10-100-flowered" />
      </biological_entity>
      <biological_entity id="o20319" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="widely" name="architecture" src="d0_s8" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o20320" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="aggregated" value_original="aggregated" />
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="compact" value_original="compact" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="canescent" name="pubescence" src="d0_s8" to="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts present, sometimes absent;</text>
      <biological_entity id="o20321" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles present.</text>
      <biological_entity id="o20322" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels present.</text>
      <biological_entity id="o20323" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 2–6 mm diam.;</text>
      <biological_entity id="o20324" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>epicalyx bractlets 0;</text>
      <biological_entity constraint="epicalyx" id="o20325" name="bractlet" name_original="bractlets" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium hemispheric, turbinate, 0.5–1 mm, canescent, pilose, or sericeous, sometimes glandular;</text>
      <biological_entity id="o20326" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="shape" src="d0_s14" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s14" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, erect or reflexed, ovate to lanceolate;</text>
      <biological_entity id="o20327" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s15" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, persistent, withering, white, oval to oblong, obovate, or oblanceolate;</text>
      <biological_entity id="o20328" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s16" value="withering" value_original="withering" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s16" to="oblong obovate or oblanceolate" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s16" to="oblong obovate or oblanceolate" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s16" to="oblong obovate or oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 20–40, equal to or longer than petals;</text>
      <biological_entity id="o20329" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s17" to="40" />
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character constraint="than petals" constraintid="o20330" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o20330" name="petal" name_original="petals" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>torus thickened basally, margins crenulate or entire;</text>
      <biological_entity id="o20331" name="torus" name_original="torus" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="basally" name="size_or_width" src="d0_s18" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o20332" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>carpels (3–) 5 (–6) [–7], abaxially or adaxially connate or free, hirsute to pilose adaxially, styles terminal, stigmas minute;</text>
      <biological_entity id="o20333" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s19" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="6" />
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
        <character is_modifier="false" modifier="abaxially; adaxially" name="fusion" src="d0_s19" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="free" value_original="free" />
        <character char_type="range_value" from="hirsute" modifier="adaxially" name="pubescence" src="d0_s19" to="pilose" />
      </biological_entity>
      <biological_entity id="o20334" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s19" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o20335" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character is_modifier="false" name="size" src="d0_s19" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 2 or 3 [or 4].</text>
      <biological_entity id="o20336" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s20" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits aggregated follicles, (3–) 5 (–6), lanceoloid, 1.5–2 mm, coriaceous, glabrous, glabrate, or sparsely pilose, dehiscent along abaxial and adaxial sutures;</text>
      <biological_entity id="o20337" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s21" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s21" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s21" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s21" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s21" value="pilose" value_original="pilose" />
        <character constraint="along abaxial adaxial sutures" constraintid="o20339" is_modifier="false" name="dehiscence" src="d0_s21" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o20338" name="follicle" name_original="follicles" src="d0_s21" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s21" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s21" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="6" />
        <character name="quantity" src="d0_s21" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o20339" name="suture" name_original="sutures" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>hypanthium persistent;</text>
      <biological_entity id="o20340" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, erect to reflexed;</text>
      <biological_entity id="o20341" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s23" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>styles deciduous or persistent.</text>
      <biological_entity id="o20342" name="style" name_original="styles" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Seeds 1 or 2, fusiform to terete.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 9.</text>
      <biological_entity id="o20343" name="seed" name_original="seeds" src="d0_s25" type="structure">
        <character is_modifier="false" name="shape" src="d0_s25" value="count" value_original="count" />
        <character is_modifier="false" name="shape" src="d0_s25" value="list" value_original="list" />
        <character name="quantity" src="d0_s25" unit="or punct fusiform to terete" value="1" value_original="1" />
        <character name="quantity" src="d0_s25" unit="or punct fusiform to terete" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="x" id="o20344" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, sc United States, ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Petrophyton</other_name>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>Taxa of Petrophytum are widely distributed in the mountains of western United States and northeastern Mexico, with P. caespitosum subsp. caespitosum the most widely distributed. The three other taxa recognized in this treatment, P. caespitosum subsp. acuminatum, P. cinerascens, and P. hendersonii, are isolated endemics with small ranges. With a single widely distributed taxon and three local endemics, it seems probable that taxa with closer geographical proximity would be likely to have greater morphological similarity. When C. Sterling (1966) examined carpel morphology in P. caespitosum, P. cinerascens, P. elatius [= P. caespitosum subsp. caespitosum], and P. hendersonii, he found that P. elatius and P. hendersonii had similar distributions of character states in five anatomical characters, yet differed as to whether carpels were fused (P. hendersonii) by their ventral bundles or distinct (P. elatius); P. caespitosum and P. cinerascens differed in only one character: carpels ventrally fused in P. caespitosum and distinct in P. cinerascens. Such incongruence of character states is seen also in leaves, flowers, and seeds; it is complicated by the reduction of these plants.</discussion>
  <discussion>In molecular phylogenetic analyses of Rosaceae (D. Potter et al. 2007), Petrophytum was sister to Kelseya, with Spiraea basal to the pair. In a more detailed study of Spiraeeae (Potter el al. 2007b), Kelseya was sister to Petrophytum and Spiraea; however, the three topologies were not well supported and additional work may show Kelseya and Petrophytum to be sister taxa.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicel bracteoles rarely extending to base of sepals; leaf abaxial surfaces glabrate, sparsely strigose, pilose to pilose-sericeous on lamina, sparsely strigose along veins, venation visible through hairs (palmately 3-veined).</description>
      <determination>1 Petrophytum hendersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicel bracteoles extending from middle to beyond apex of sepals; leaf abaxial surfaces minutely canescent to strigose, cinereous, or pilose to sericeous, venation usually not visible through hairs [3 veins may be visible through hairs (P. cinerascens) or 3+ veins may be visible in long shoot leaves (some plants of P. caespitosum)]</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicel bracteoles extending from middle to apex of sepals, rarely beyond; leaf blades: apices obtuse, abaxial surfaces minutely canescent to strigose or cinereous, venation sometimes visible through hairs, palmately 3-veined; petal apices acute or rounded.</description>
      <determination>2 Petrophytum cinerascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicel bracteoles usually extending well beyond apex of sepals; leaf blades: apices acute, abaxial surfaces pilose to sericeous or sparsely strigose, venation rarely visible except on long-shoot leaves, 1(–3+)-veined; petal apices obtuse to slightly cleft or acute to acuminate.</description>
      <determination>3 Petrophytum caespitosum</determination>
    </key_statement>
  </key>
</bio:treatment>