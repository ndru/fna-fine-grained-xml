<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator/>
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Potentilla</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">canadensis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 498. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section potentilla;species canadensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417046</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="(Poiret) Torrey &amp; A. Gray" date="unknown" rank="variety">pumila</taxon_name>
    <taxon_hierarchy>genus Potentilla;species canadensis;variety pumila</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">villosissima</taxon_name>
    <taxon_hierarchy>genus P.;species canadensis;variety villosissima</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">caroliniana</taxon_name>
    <taxon_hierarchy>genus P.;species caroliniana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pumila</taxon_name>
    <taxon_hierarchy>genus P.;species pumila</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Dwarf cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rootstocks erect, stout, 0.5–2 cm.</text>
      <biological_entity id="o8392" name="rootstock" name_original="rootstocks" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect"/>
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout"/>
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s0" to="2" to_unit="cm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems initially ascending to erect, soon becoming prostrate, flagelliform, not branched, eventually rooting at some nodes, (0.3–) 0.5–12 dm.</text>
      <biological_entity id="o8393" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="initially ascending" name="orientation" src="d0_s1" to="erect"/>
        <character is_modifier="false" modifier="soon becoming" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate"/>
        <character is_modifier="false" name="shape" src="d0_s1" value="flagelliform" value_original="flagelliform"/>
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branched" value_original="branched"/>
        <character constraint="at nodes" constraintid="o8394" is_modifier="false" modifier="eventually" name="architecture" src="d0_s1" value="rooting" value_original="rooting"/>
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s1" to="0.5" to_inclusive="false" to_unit="dm"/>
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="12" to_unit="dm"/>
      </biological_entity>
      <biological_entity id="o8394" name="node" name_original="nodes" src="d0_s1" type="structure"/>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves ± persistent, usually palmate, 2–9 (–11) cm;</text>
      <biological_entity constraint="basal" id="o8395" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s2" value="persistent" value_original="persistent"/>
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="palmate" value_original="palmate"/>
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="11" to_unit="cm"/>
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–7 (–8) cm, long hairs abundant to dense, appressed to spreading, 1.5–3 mm, mostly ± weak, glands absent or sparse;</text>
      <biological_entity id="o8396" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm"/>
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm"/>
      </biological_entity>
      <biological_entity id="o8397" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long"/>
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant"/>
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense"/>
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s3" to="spreading"/>
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm"/>
        <character is_modifier="false" modifier="mostly more or less" name="fragility" src="d0_s3" value="weak" value_original="weak"/>
      </biological_entity>
      <biological_entity id="o8398" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent"/>
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse"/>
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets (3–) 5 (–7), central mostly ± obovate to cuneate, sometimes narrowly so, (0.5–) 1–4 (–6) × (0.5–) 0.8–2 (–3) cm, distal 1/2 of margin incised 1/4–1/3 to midvein, teeth 2–7 per side, surfaces similar to ± dissimilar, abaxial green to silvery white, sparsely to densely hairy, adaxial green, sparsely to moderately hairy.</text>
      <biological_entity id="o8399" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false"/>
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7"/>
        <character name="quantity" src="d0_s4" value="5" value_original="5"/>
      </biological_entity>
      <biological_entity constraint="central" id="o8400" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="less obovate" name="shape" src="d0_s4" to="cuneate"/>
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm"/>
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="6" to_unit="cm"/>
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm"/>
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s4" to="0.8" to_inclusive="false" to_unit="cm"/>
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="3" to_unit="cm"/>
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm"/>
        <character is_modifier="false" modifier="sometimes narrowly; narrowly" name="position_or_shape" src="d0_s4" value="distal" value_original="distal"/>
        <character constraint="of margin" constraintid="o8401" name="quantity" src="d0_s4" value="1/2" value_original="1/2"/>
        <character char_type="range_value" constraint="to midvein" constraintid="o8402" from="1/4" name="quantity" src="d0_s4" to="1/3"/>
      </biological_entity>
      <biological_entity id="o8401" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised"/>
      </biological_entity>
      <biological_entity id="o8402" name="midvein" name_original="midvein" src="d0_s4" type="structure"/>
      <biological_entity id="o8403" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o8404" from="2" name="quantity" src="d0_s4" to="7"/>
      </biological_entity>
      <biological_entity id="o8404" name="side" name_original="side" src="d0_s4" type="structure"/>
      <biological_entity id="o8405" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" notes="" src="d0_s4" value="hairy" value_original="hairy"/>
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8407" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" modifier="more or less; more or less" name="coloration" src="d0_s4" to="silvery white"/>
        <character is_modifier="false" modifier="more or less; sparsely; densely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy"/>
        <character is_modifier="false" modifier="more or less; sparsely" name="coloration" src="d0_s4" value="green" value_original="green"/>
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8408" name="leaflet" name_original="leaflets" src="d0_s4" type="structure"/>
      <relation from="o8405" id="r537" name="to" negation="false" src="d0_s4" to="o8407"/>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 0–1 proximal to 1st flowering node, not fully expanded at anthesis, usually palmate, 2–7 (–9) cm;</text>
      <biological_entity constraint="cauline" id="o8409" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="1"/>
        <character constraint="to node" constraintid="o8410" is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal"/>
        <character constraint="at anthesis , usually palmate , 2-7(-9) cm" is_modifier="false" modifier="not fully" name="size" notes="" src="d0_s5" value="expanded" value_original="expanded"/>
      </biological_entity>
      <biological_entity id="o8410" name="node" name_original="node" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering"/>
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole (0.5–) 1–6 (–7.5) cm;</text>
      <biological_entity id="o8411" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="1" to_inclusive="false" to_unit="cm"/>
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="7.5" to_unit="cm"/>
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets (3–) 5, ± resembling those of basal leaves, apex rounded to obtuse.</text>
      <biological_entity id="o8412" name="leaflet" name_original="leaflets" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false"/>
        <character name="quantity" src="d0_s7" value="5" value_original="5"/>
      </biological_entity>
      <biological_entity constraint="basal" id="o8413" name="leaf" name_original="leaves" src="d0_s7" type="structure"/>
      <biological_entity id="o8414" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="obtuse"/>
      </biological_entity>
      <relation from="o8412" id="r538" modifier="more or less" name="part_of" negation="false" src="d0_s7" to="o8413"/>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences solitary flowers at stolon nodes.</text>
      <biological_entity id="o8415" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure"/>
      <biological_entity id="o8416" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary"/>
      </biological_entity>
      <biological_entity constraint="stolon" id="o8417" name="node" name_original="nodes" src="d0_s8" type="structure"/>
      <relation from="o8416" id="r539" name="at" negation="false" src="d0_s8" to="o8417"/>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels (1–) 2–5 (–9) cm.</text>
      <biological_entity id="o8418" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="cm"/>
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="9" to_unit="cm"/>
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o8419" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-merous" value_original="5-merous"/>
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>epicalyx bractlets linear to lanceolate-elliptic, (2–) 3–5 (–6) × 0.8–1.5 mm, slightly smaller than to ± equal to sepals;</text>
      <biological_entity constraint="epicalyx" id="o8420" name="bractlet" name_original="bractlets" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s11" to="lanceolate-elliptic"/>
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s11" to="3" to_inclusive="false" to_unit="mm"/>
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_unit="mm"/>
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm"/>
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm"/>
        <character constraint="than to more or less equal to sepals" constraintid="o8421" is_modifier="false" name="size" src="d0_s11" value="slightly smaller" value_original="slightly smaller"/>
      </biological_entity>
      <biological_entity id="o8421" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal"/>
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 2.5–5 mm diam.;</text>
      <biological_entity id="o8422" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals (2–) 3–5 (–6) mm, apex acute;</text>
      <biological_entity id="o8423" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_inclusive="false" to_unit="mm"/>
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6" to_unit="mm"/>
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm"/>
      </biological_entity>
      <biological_entity id="o8424" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute"/>
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 4–6 (–8) × 3.5–6.5 (–8) mm, apex rounded to slightly retuse;</text>
      <biological_entity id="o8425" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="8" to_unit="mm"/>
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm"/>
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s14" to="8" to_unit="mm"/>
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s14" to="6.5" to_unit="mm"/>
      </biological_entity>
      <biological_entity id="o8426" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s14" to="slightly retuse"/>
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens ca. 20, filaments 0.5–2 mm, anthers 0.4–1 mm;</text>
      <biological_entity id="o8427" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20"/>
      </biological_entity>
      <biological_entity id="o8428" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm"/>
      </biological_entity>
      <biological_entity id="o8429" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>carpels 20–40, styles 0.8–1.4 mm.</text>
      <biological_entity id="o8430" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s16" to="40"/>
      </biological_entity>
      <biological_entity id="o8431" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes 1.2–1.4 mm, smooth.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 28.</text>
      <biological_entity id="o8432" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="1.4" to_unit="mm"/>
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth"/>
      </biological_entity>
      <biological_entity constraint="2n" id="o8433" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28"/>
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="late Mar"/>
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry flats and slopes in lawns, pastures, roadsides, cherty slopes, dry meadows, edges of oak and conifer woodlands, often on acidic soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry flats" constraint="in lawns , pastures , roadsides , cherty slopes , dry meadows , edges of oak and conifer woodlands"/>
        <character name="habitat" value="slopes" constraint="in lawns , pastures , roadsides , cherty slopes , dry meadows , edges of oak and conifer woodlands"/>
        <character name="habitat" value="lawns"/>
        <character name="habitat" value="pastures"/>
        <character name="habitat" value="roadsides"/>
        <character name="habitat" value="cherty slopes"/>
        <character name="habitat" value="dry meadows"/>
        <character name="habitat" value="edges" constraint="of oak"/>
        <character name="habitat" value="oak"/>
        <character name="habitat" value="conifer woodlands"/>
        <character name="habitat" value="acidic soil"/>
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m"/>
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S., Ont.; Conn., Del., D.C., Ky., Maine, Md., Mass., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native"/>
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native"/>
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native"/>
      </biological_entity>
    </statement>
  </description>
  <discussion>A cream-colored form (ochroleuca) was described from a now-obliterated site in Massachusetts (M. L. Fernald 1931).</discussion>
  
</bio:treatment>