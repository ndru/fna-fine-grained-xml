<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="Ertter &amp; Reveal" date="1977" rank="species">rhypara</taxon_name>
    <taxon_name authority="Ertter" date="1989" rank="variety">shellyi</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>14: 239, fig. 4. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species rhypara;variety shellyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100681</other_info_on_name>
  </taxon_identification>
  <number>10b.</number>
  <other_name type="common_name">Shelly’s ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–10 (–15) cm diam.</text>
      <biological_entity id="o3802" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (0.1–) 0.2–0.5 (–0.9) dm, equal to or exceeding leaves by no more than 2 (–7) cm.</text>
      <biological_entity id="o3803" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.9" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s1" to="0.5" to_unit="dm" />
        <character is_modifier="false" name="variability" src="d0_s1" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o3804" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o3803" id="r239" name="exceeding" negation="false" src="d0_s1" to="o3804" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 5–30 (–60) -flowered, 0.5–2 (–5) cm diam. 2n = 28.</text>
      <biological_entity id="o3805" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-30(-60)-flowered" value_original="5-30(-60)-flowered" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="diameter" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3806" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Overtly petrophytic in cracks and crevices of pumiceous welded ash-flow tuff boulders and outcrops, in sagebrush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cracks" modifier="overtly petrophytic in" constraint="of pumiceous welded ash-flow tuff boulders and outcrops" />
        <character name="habitat" value="crevices" constraint="of pumiceous welded ash-flow tuff boulders and outcrops" />
        <character name="habitat" value="pumiceous welded ash-flow tuff boulders" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="sagebrush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In addition to their more overtly petrophytic habit, plants of var. shellyi are generally smaller and more compact than those of var. rhypara. It is known only from the Rehart and Venator canyons area along the border of Harney and Lake counties in southeastern Oregon.</discussion>
  
</bio:treatment>