<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">452</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="Exell" date="1928" rank="species">crispii</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Chron., ser.</publication_title>
      <place_in_publication>3, 83: 44. 1928</place_in_publication>
      <other_info_on_pub>as hybrid</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species crispii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100042</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Crisp’s cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1.5–6 m.</text>
      <biological_entity id="o22955" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems narrowly erect, arching;</text>
      <biological_entity id="o22957" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches spiraled, purple-black, initially silky tomentose.</text>
      <biological_entity id="o22958" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple-black" value_original="purple-black" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s2" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, rarely semipersistent (in harsh winters);</text>
      <biological_entity id="o22959" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s3" value="semipersistent" value_original="semipersistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–7 mm, tomentose;</text>
      <biological_entity id="o22960" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, sometimes ovate, 25–46 × 11–28 mm, subcoriaceous or coriaceous, base cuneate, margins flat, veins 4–7, superficial, rarely faintly sunken, apex acute, abaxial surfaces initially whitish tomentose, becoming glabrate, adaxial green to dark green, dull, slightly glaucous, flat between lateral-veins, initially sparsely pilose.</text>
      <biological_entity id="o22961" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="46" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s5" to="28" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="subcoriaceous" value_original="subcoriaceous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o22962" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22963" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o22964" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="7" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
        <character is_modifier="false" modifier="rarely faintly" name="prominence" src="d0_s5" value="sunken" value_original="sunken" />
      </biological_entity>
      <biological_entity id="o22965" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22966" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="initially" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22967" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character constraint="between lateral-veins" constraintid="o22968" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="initially sparsely" name="pubescence" notes="" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o22968" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences on fertile shoots 40–70 mm with 4 leaves, 7–25-flowered, compact to ± lax.</text>
      <biological_entity id="o22969" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="7-25-flowered" value_original="7-25-flowered" />
        <character char_type="range_value" from="compact" name="architecture_or_arrangement" src="d0_s6" to="more or less lax" />
      </biological_entity>
      <biological_entity id="o22970" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with leaves" constraintid="o22971" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22971" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o22969" id="r1493" name="on" negation="false" src="d0_s6" to="o22970" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–7 mm, silky tomentose.</text>
      <biological_entity id="o22972" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (7–) 10–11 mm diam.;</text>
      <biological_entity id="o22973" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>buds white;</text>
      <biological_entity id="o22974" name="bud" name_original="buds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium funnelform, silky tomentose;</text>
      <biological_entity id="o22975" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins villous, apex cuspidate or acuminate, surfaces silky tomentose;</text>
      <biological_entity id="o22976" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o22977" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o22978" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o22979" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals spreading, white, rarely with hair-tuft;</text>
      <biological_entity id="o22980" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o22981" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o22982" name="hair-tuft" name_original="hair-tuft" src="d0_s12" type="structure" />
      <relation from="o22981" id="r1494" modifier="rarely" name="with" negation="false" src="d0_s12" to="o22982" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 20, filaments white, anthers reddish purple;</text>
      <biological_entity id="o22983" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o22984" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o22985" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o22986" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles (1 or) 2.</text>
      <biological_entity id="o22987" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o22988" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes red to dark red, globose or subglobose, 6–8 × 6–8 mm, dull or slightly shiny, slightly glaucous, glabrescent;</text>
      <biological_entity id="o22989" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s15" to="dark red" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals flat, depressed, or incurved, densely villous;</text>
    </statement>
    <statement id="d0_s17">
      <text>navel open;</text>
      <biological_entity id="o22990" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s16" value="depressed" value_original="depressed" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants at apex.</text>
      <biological_entity constraint="style" id="o22991" name="remnant" name_original="remnants" src="d0_s18" type="structure" />
      <biological_entity id="o22992" name="apex" name_original="apex" src="d0_s18" type="structure" />
      <relation from="o22991" id="r1495" name="at" negation="false" src="d0_s18" to="o22992" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes (1 or) 2.2n = 51 (Germany).</text>
      <biological_entity id="o22993" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22994" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul; fruiting Oct–Feb.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Feb" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, building edges, cracks in pavement</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="building edges" />
        <character name="habitat" value="cracks" constraint="in pavement" />
        <character name="habitat" value="pavement" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cotoneaster crispii was collected recently wild in Yunnan. The species is often treated as a hybrid of garden origin between C. frigidus and C. pannosus. Plants of it differ from those of C. frigidus in their smaller and persistent leaves, and it has larger leaves and fruits than C. pannosus. The species was overlooked in the Flora of China (L. Lingdi and A. R. Brach 2003).</discussion>
  
</bio:treatment>