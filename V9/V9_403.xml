<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="illustration_page">245</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">HORKELIA</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="1827" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>2: 26. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section horkelia;species californica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220006509</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) Greene" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Potentilla;species californica</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± tufted, green.</text>
      <biological_entity id="o4302" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (1–) 5–10 (–12) dm, hairs ± spreading.</text>
      <biological_entity id="o4303" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="12" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o4304" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves planar, (5–) 8–40 (–50) × 1.5–11 cm;</text>
      <biological_entity constraint="basal" id="o4305" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="planar" value_original="planar" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s2" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules entire;</text>
      <biological_entity id="o4306" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3–9 per side, separate to slightly overlapping, ovate to round or broadly elliptic, 5–60 × (7–) 10–30 (–40) mm, 1/2 to as wide as long, unlobed or irregularly cleft 1/6–3/4+ to midrib into 5–15 linear or oblanceolate to obovate coarsely toothed lobes, collectively 10–60-toothed, pilose to villous.</text>
      <biological_entity id="o4307" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o4308" from="3" name="quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="separate" name="arrangement" notes="" src="d0_s4" to="slightly overlapping" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="round or broadly elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character constraint="as-wide-as long" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o4309" from="1/6" name="quantity" src="d0_s4" to="3/4" upper_restricted="false" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character is_modifier="false" modifier="collectively" name="shape" notes="" src="d0_s4" value="10-60-toothed" value_original="10-60-toothed" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s4" to="villous" />
      </biological_entity>
      <biological_entity id="o4308" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o4309" name="midrib" name_original="midrib" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" modifier="into" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o4310" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="coarsely" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 4–10.</text>
      <biological_entity constraint="cauline" id="o4311" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ± open, flowers arranged individually and in glomerules.</text>
      <biological_entity id="o4312" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4313" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="individually" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o4314" name="glomerule" name_original="glomerules" src="d0_s6" type="structure" />
      <relation from="o4313" id="r264" name="in" negation="false" src="d0_s6" to="o4314" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–20 mm.</text>
      <biological_entity id="o4315" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 8–15 mm diam.;</text>
      <biological_entity id="o4316" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlets narrowly oblong to ovate, 4–6 (–8) × 1–2.5 mm, ± equal to sepals, entire or toothed;</text>
      <biological_entity constraint="epicalyx" id="o4317" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
        <character constraint="to sepals" constraintid="o4318" is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o4318" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 3–5.5 × 4–10 mm, 1/2 to nearly as deep as wide, interior glabrous or pilose;</text>
      <biological_entity id="o4319" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
        <character name="quantity" src="d0_s10" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="nearly" name="width" src="d0_s10" value="wide" value_original="wide" />
        <character is_modifier="false" name="position" src="d0_s10" value="interior" value_original="interior" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals spreading, lanceolate, 4–6.5 (–8) mm;</text>
      <biological_entity id="o4320" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals oblanceolate to elliptic or oblong, 3–8 × 1–4 mm, apex obtuse to rounded;</text>
      <biological_entity id="o4321" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s12" to="elliptic or oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4322" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.5–3 × 0.2–1.5 mm, anthers (0.8–) 1.3–1.8 mm;</text>
      <biological_entity id="o4323" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4324" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels (50–) 80–200 (–220);</text>
      <biological_entity id="o4325" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s14" to="80" to_inclusive="false" />
        <character char_type="range_value" from="200" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="220" />
        <character char_type="range_value" from="80" name="quantity" src="d0_s14" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2–4 mm.</text>
      <biological_entity id="o4326" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes brown, 0.8–1 mm, smooth or slightly rugose.</text>
      <biological_entity id="o4327" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s16" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Plants of Horkelia californica have the largest hypanthia in the genus, and the hypanthia are significantly deeper than others that are equally wide. Other unique features include the often toothed bractlets and the slightly clawed petals that are often inwardly curved with undulate margins. These features also help to distinguish the species from Drymocallis glandulosa, with which it is often confused: both species have a similar gross morphology and overlapping ranges and habitats.</discussion>
  <discussion>The three varieties recognized here were treated as distinct species by D. D. Keck (1938). The existence of intermediate populations makes varietal status the preferred option.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral leaflets 3 or 4 per side, broadly elliptic to ovate, unlobed or cleft 1/6–1/4 to midrib.</description>
      <determination>12c Horkelia californica var. frondosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral leaflets 4–9 per side, ovate to round, cleft ± 1/2–3/4+ to midrib</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Hypanthium interior ± pilose; sepals often red-mottled adaxially; lateral leaflets cleft ± 1/2(–3/4) to midrib; styles 3–4 mm.</description>
      <determination>12a Horkelia californica var. californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Hypanthium interior glabrous; sepals not red-mottled adaxially; lateral leaflets cleft 1/2–3/4+ to midrib; styles 2–3 mm.</description>
      <determination>12b Horkelia californica var. elata</determination>
    </key_statement>
  </key>
</bio:treatment>