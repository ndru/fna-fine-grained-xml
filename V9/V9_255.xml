<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Multijugae</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">multijuga</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (Hamburg)</publication_title>
      <place_in_publication>1849: 6. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section multijugae;species multijuga;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100343</other_info_on_name>
  </taxon_identification>
  <number>44.</number>
  <other_name type="common_name">Ballona or lost cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± rosetted;</text>
      <biological_entity id="o24790" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots ± fleshy-thickened.</text>
      <biological_entity id="o24791" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending, 2–5 (–7) dm, lengths 1–2 times basal leaves.</text>
      <biological_entity id="o24792" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character constraint="leaf" constraintid="o24793" is_modifier="false" name="length" src="d0_s2" value="1-2 times basal leaves" value_original="1-2 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24793" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves pinnate with distal leaflets ± confluent, 10–22 × 1.5–3.5 cm;</text>
      <biological_entity constraint="basal" id="o24794" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with distal leaflets" constraintid="o24795" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s3" to="22" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" notes="" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24795" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (2–) 4–10 cm, straight hairs absent or sparse, appressed, 0.5–1.5 mm, stiff, cottony hairs absent, glands absent;</text>
      <biological_entity id="o24796" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24797" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o24798" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24799" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lateral leaflets 3–8 per side, on distal 1/3–2/3 of leaf axis, ± separate, largest ones cuneate to obovate, 1–2.2 × 0.8–1.5 cm, distal 1/3–2/3 of margin ± evenly incised ± 1/2 to midvein, ultimate teeth 3–6, ± ovate, 2–6 × 1.5–3 mm, apical tufts absent, surfaces green, not glaucous, straight hairs sparse to nearly absent (except on margins), ± appressed, 0.5–1 mm, stiff, cottony hairs absent, glands absent or sparse.</text>
      <biological_entity constraint="primary lateral" id="o24800" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o24801" from="3" name="quantity" src="d0_s5" to="8" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="separate" value_original="separate" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="of margin" constraintid="o24804" from="1/3" name="quantity" src="d0_s5" to="2/3" />
        <character constraint="to midvein" constraintid="o24805" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o24801" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o24802" name="1/3-2/3" name_original="1/3-2/3" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o24803" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o24804" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less evenly; more or less" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o24805" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity constraint="ultimate" id="o24806" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o24807" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24808" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o24809" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="nearly absent" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o24810" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24811" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o24800" id="r1627" name="on" negation="false" src="d0_s5" to="o24802" />
      <relation from="o24802" id="r1628" name="part_of" negation="false" src="d0_s5" to="o24803" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 1–3.</text>
      <biological_entity constraint="cauline" id="o24812" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–10 (–20) -flowered, very openly cymose, sometimes racemiform.</text>
      <biological_entity id="o24813" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-10(-20)-flowered" value_original="3-10(-20)-flowered" />
        <character is_modifier="false" modifier="very openly" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1.5–3.5 (–5) cm, ± recurved in fruit.</text>
      <biological_entity id="o24814" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3.5" to_unit="cm" />
        <character constraint="in fruit" constraintid="o24815" is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o24815" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets ovate-elliptic, 2.5–4 (–5) × 1.5–3 mm, sometimes toothed;</text>
      <biological_entity id="o24816" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o24817" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 4–6 mm diam.;</text>
      <biological_entity id="o24818" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24819" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–6 (–7) mm, apex acute, rarely acuminate;</text>
      <biological_entity id="o24820" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24821" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24822" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 4.5–10 × 4–10 mm;</text>
      <biological_entity id="o24823" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o24824" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.5–3 (–4.5) mm, anthers 0.7–1 mm;</text>
      <biological_entity id="o24825" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24826" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24827" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 5–10, styles 2–3 mm.</text>
      <biological_entity id="o24828" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o24829" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
      <biological_entity id="o24830" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.8–2 mm, smooth to faintly rugose, possibly carunculate.</text>
      <biological_entity id="o24831" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s15" to="faintly rugose" />
        <character is_modifier="false" modifier="possibly" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brackish coastal meadows or marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal meadows" modifier="brackish" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="brackish" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla multijuga is known only from a handful of collections made in the 1890s near the current site of Los Angeles International Airport. All efforts to locate extant plants have failed; the species is presumed extinct. Continued use of P. multijuga for this species required conservation of the name with a conserved type, because the type designated by Lehmann is a specimen of Horkelia cuneata Lindley var. cuneata (B. Ertter and J. L. Reveal 2008).</discussion>
  
</bio:treatment>