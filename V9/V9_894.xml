<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">533</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Virides</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">viridis</taxon_name>
    <taxon_name authority="(Sargent) E. J. Palmer" date="1935" rank="variety">lanceolata</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>22: 561. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series virides;species viridis;variety lanceolata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100610</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Sargent" date="unknown" rank="species">lanceolata</taxon_name>
    <place_of_publication>
      <publication_title>Trees &amp; Shrubs</publication_title>
      <place_in_publication>2: 65, plate 130. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;species lanceolata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Beadle" date="unknown" rank="species">interior</taxon_name>
    <taxon_hierarchy>genus C.;species interior</taxon_hierarchy>
  </taxon_identification>
  <number>32b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: trunk bark light gray, ± exfoliating;</text>
      <biological_entity id="o16875" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity constraint="trunk" id="o16876" name="bark" name_original="bark" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="light gray" value_original="light gray" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s0" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>1-year old twigs gray.</text>
      <biological_entity id="o16877" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16878" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade ± lanceolate, narrowly elliptic to narrowly obovate or oblong, 2.5–6 cm, thin, base cuneate, lobes 0 or obscure else rarely 1 per side, distinct, but not on most leaves, sinuses shallow, max LII 5 (–20) %, margins serrulate-crenate, mainly in distal 2/3, sometimes entire, teeth to 1 mm, venation craspedodromous, sometimes semicamptodromous, veins 3–7 per side (often dividing before margin), apex acute to acuminate, surfaces glabrous except abaxially with tufts of hair in vein-axils.</text>
      <biological_entity id="o16879" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16880" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s2" to="narrowly obovate or oblong" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o16881" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16882" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character constraint="per side" constraintid="o16883" modifier="rarely" name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o16883" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o16884" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16885" name="sinuse" name_original="sinuses" src="d0_s2" type="structure">
        <character is_modifier="false" name="depth" src="d0_s2" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o16886" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="5(-20)%" name="shape" src="d0_s2" value="serrulate-crenate" value_original="serrulate-crenate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16887" name="2/3" name_original="2/3" src="d0_s2" type="structure" />
      <biological_entity id="o16888" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity id="o16889" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o16890" from="3" modifier="sometimes" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity id="o16890" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o16891" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <biological_entity id="o16892" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character constraint="except tufts" constraintid="o16893" is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16893" name="tuft" name_original="tufts" src="d0_s2" type="structure" />
      <biological_entity id="o16894" name="vein-axil" name_original="vein-axils" src="d0_s2" type="structure" />
      <relation from="o16882" id="r1084" name="on" negation="true" src="d0_s2" to="o16884" />
      <relation from="o16886" id="r1085" modifier="mainly" name="in" negation="false" src="d0_s2" to="o16887" />
      <relation from="o16893" id="r1086" name="in" negation="false" src="d0_s2" to="o16894" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: branches glabrous.</text>
      <biological_entity id="o16895" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o16896" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium glabrous.</text>
      <biological_entity id="o16897" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16898" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, fertile, alluvial woodlands, agricultural derivatives of these</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fertile" modifier="moist" />
        <character name="habitat" value="alluvial woodlands" />
        <character name="habitat" value="agricultural derivatives" constraint="of these" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., Mo., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The range of var. lanceolata is poorly documented; it appears to be rather common in Louisiana and extends into Missouri, into eastern Texas, and through Alabama to South Carolina. In its most extreme expression, it is readily distinguished; it intergrades with both var. ovata and var. viridis. The foliage, with its distinctive venation, and the usually small- and few-flowered inflorescences are characteristic. Particularly large, oblong leaf forms of var. lanceolata have been referred to Crataegus interior.</discussion>
  
</bio:treatment>