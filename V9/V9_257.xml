<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Concinnae</taxon_name>
    <place_of_publication>
      <publication_title>in J. M. Coulter and A. Nelson, New Man. Bot. Rocky Mt.,</publication_title>
      <place_in_publication>255. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Concinnae</taxon_hierarchy>
    <other_info_on_name type="fna_id">318028</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Concinnae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 431. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;unranked Concinnae</taxon_hierarchy>
  </taxon_identification>
  <number>8o.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, usually rosetted to tufted, not stoloniferous;</text>
      <biological_entity id="o19178" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="usually rosetted" name="arrangement" src="d0_s0" to="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots sometimes thick, not fleshy;</text>
      <biological_entity id="o19179" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vestiture primarily of long and/or cottony hairs (and crisped hairs in inflorescence), glands absent or sparse to abundant, rarely reddish.</text>
      <biological_entity id="o19180" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o19181" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity id="o19182" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="abundant" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o19180" id="r1239" name="part_of" negation="false" src="d0_s2" to="o19181" />
    </statement>
    <statement id="d0_s3">
      <text>Stems prostrate to ± decumbent, not flagelliform, not rooting at nodes, lateral to persistent basal rosettes, 0.2–1.5 (–2.7) dm, lengths 1/2–3 (–4) times basal leaves.</text>
      <biological_entity id="o19183" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="prostrate" name="growth_form_or_orientation" src="d0_s3" to="more or less decumbent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o19184" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="2.7" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="1.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o19186" is_modifier="false" name="length" src="d0_s3" value="1/2-3(-4) times basal leaves" value_original="1/2-3(-4) times basal leaves" />
      </biological_entity>
      <biological_entity id="o19184" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o19185" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o19186" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not in ranks;</text>
      <biological_entity id="o19187" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o19188" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o19189" name="rank" name_original="ranks" src="d0_s4" type="structure" />
      <relation from="o19188" id="r1240" name="in" negation="false" src="d0_s4" to="o19189" />
    </statement>
    <statement id="d0_s5">
      <text>cauline 0–2;</text>
      <biological_entity id="o19190" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o19191" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves palmate to subpalmate, sometimes subpinnate to pinnate (distal leaflets sometimes confluent), 1.5–12 (–15) cm;</text>
      <biological_entity id="o19192" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o19193" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="palmate" name="architecture" src="d0_s6" to="subpalmate" />
        <character char_type="range_value" from="subpinnate" modifier="sometimes" name="architecture" src="d0_s6" to="pinnate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs ± appressed to ascending, stiff to weak, glands absent or sparse or obscured, rarely abundant;</text>
      <biological_entity id="o19194" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o19195" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character char_type="range_value" from="less appressed" name="orientation" src="d0_s7" to="ascending" />
        <character char_type="range_value" from="stiff" name="fragility" src="d0_s7" to="weak" />
      </biological_entity>
      <biological_entity id="o19196" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscured" value_original="obscured" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s7" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets (3–) 5–9 (–11), at tip or to distal 3/4+ of leaf axis, separate to strongly overlapping, narrowly oblanceolate or cuneate to obovate, margins flat, less than distal 1/5 to whole length evenly, sometimes unevenly, incised 1/4–3/4+ to midvein, sometimes medially cleft as well, rarely entire, teeth (0–) 1–5 (–10) per side, surfaces similar to strongly dissimilar, abaxial green to white, cottony hairs absent or sparse to dense, adaxial green to grayish, not glaucous, long hairs usually stiff, rarely weak.</text>
      <biological_entity id="o19197" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o19198" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="11" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="9" />
        <character char_type="range_value" from="separate" name="arrangement" notes="" src="d0_s8" to="strongly overlapping" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s8" to="obovate" />
      </biological_entity>
      <biological_entity id="o19199" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o19200" name="axis" name_original="axis" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character char_type="range_value" constraint="at " constraintid="o19202" from="3/4" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19201" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o19202" name="axis" name_original="axis" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o19203" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="less; sometimes unevenly; unevenly" name="shape" notes="" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o19205" from="1/4" name="quantity" src="d0_s8" to="3/4" upper_restricted="false" />
        <character is_modifier="false" modifier="sometimes medially" name="architecture_or_shape" notes="" src="d0_s8" value="cleft" value_original="cleft" />
        <character is_modifier="false" modifier="well; rarely" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19204" name="1/5" name_original="1/5" src="d0_s8" type="structure" />
      <biological_entity id="o19205" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o19206" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s8" to="1" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="10" />
        <character char_type="range_value" constraint="per side" constraintid="o19207" from="1" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o19207" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o19208" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o19209" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" modifier="strongly; strongly" name="coloration" src="d0_s8" to="white" />
        <character is_modifier="false" modifier="strongly; strongly; strongly; strongly" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="strongly; strongly; strongly; strongly" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" value="sparse to dense" value_original="sparse to dense" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19210" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" modifier="strongly; strongly" name="coloration" src="d0_s8" to="white" />
        <character is_modifier="false" modifier="strongly; strongly; strongly; strongly" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="strongly; strongly; strongly; strongly" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19211" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="grayish" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o19212" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="rarely" name="fragility" src="d0_s8" value="weak" value_original="weak" />
      </biological_entity>
      <relation from="o19198" id="r1241" name="at" negation="false" src="d0_s8" to="o19199" />
      <relation from="o19198" id="r1242" name="at" negation="false" src="d0_s8" to="o19200" />
      <relation from="o19208" id="r1243" name="to" negation="false" src="d0_s8" to="o19209" />
      <relation from="o19208" id="r1244" name="to" negation="false" src="d0_s8" to="o19210" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (1–) 2–12 (–15) -flowered, usually ± cymose, open, sometimes racemiform when prostrate.</text>
      <biological_entity id="o19213" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(1-)2-12(-15)-flowered" value_original="(1-)2-12(-15)-flowered" />
        <character is_modifier="false" modifier="usually more or less" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="false" modifier="when prostrate" name="architecture" src="d0_s9" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels often recurved in fruit, 0.7–3 (–4.5) cm, proximal usually not much longer than distal.</text>
      <biological_entity id="o19214" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o19215" is_modifier="false" modifier="often" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19215" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o19216" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o19217" is_modifier="false" name="length_or_size" src="d0_s10" value="usually not much longer" value_original="usually not much longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19217" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o19218" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 2.5–6 mm diam.;</text>
      <biological_entity id="o19219" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals yellow, ± obcordate, (2.5–) 3.5–7 (–9) mm, longer than sepals, apex usually ± retuse;</text>
      <biological_entity id="o19220" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character constraint="than sepals" constraintid="o19221" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o19221" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o19222" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens ca. 20;</text>
      <biological_entity id="o19223" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, filiform to filiform-tapered, not papillate-swollen or in proximal 1/5, (1–) 1.5–3 mm.</text>
      <biological_entity id="o19224" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s15" to="filiform-tapered" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character is_modifier="false" name="shape" src="d0_s15" value="in proximal 1/5" value_original="in proximal 1/5" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s15" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19225" name="1/5" name_original="1/5" src="d0_s15" type="structure" />
      <relation from="o19224" id="r1245" name="in" negation="false" src="d0_s15" to="o19225" />
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth to ± rugose.</text>
      <biological_entity id="o19226" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s16" to="more or less rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 10 (8 in the flora).</discussion>
  <discussion>Section Concinnae, as here defined, accommodates a cluster of species that occur from the western Great Plains to California and to the mountains of northern Mexico. Stems are commonly prostrate or nearly so and are sometimes shorter than the basal leaves; pedicels tend to become recurved in fruit. Inflorescences often have abundant reddish papillae at the bases of hairs on hypanthia, epicalyx bractlets, and sepals. Leaves are most commonly palmate to subpalmate (except for Potentilla angelliae, P. macounii, and P. morefieldii), most often sparsely to densely cottony abaxially (except for P. multisecta, P. johnstonii, and P. sierrae-blancae), and strigose adaxially with stiff, verrucose hairs, which are sometimes yellowish. Petioles are usually sparsely to densely strigose with stiff, verrucose hairs.</discussion>
  <discussion>Both P. A. Rydberg (1908d) and B. C. Johnston (1980, 1985) restricted sect. Concinnae to species with palmate, abaxially cottony leaves. Given the unifying or transitional nature of other morphologic and biogeographic features, the circumscription presented here seems more justified.</discussion>
  <discussion>P. A. Rydberg (1908d) included New Mexico in the distribution of the otherwise Mexican species Potentilla oblanceolata Rydberg, repeated in subsequent floras (I. Tidestrom and T. Kittell 1941; W. C. Martin and C. R. Hutchins 1980). No specimens are currently known to confirm the presence of this species north of Mexico, and it is excluded here.</discussion>
  <discussion>Since Potentilla breweri (sect. Multijugae) is sometimes identified as a member of sect. Concinnae, it is included herein and keys out in the ninth couplet.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet abaxial surfaces: cottony hairs absent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet abaxial surfaces: cottony hairs sparse to dense</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Teeth 2–5 per side on distal 1/2+ of central leaflets, 3–10(–14) mm.</description>
      <determination>51 Potentilla multisecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Teeth 1(–3) per side on distal 1/4 or less of central leaflets, 1–2(–4) mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflets (3–)5–7(–9), straight hairs sparse to common; inflorescences 4–11-flowered; Nevada.</description>
      <determination>52 Potentilla johnstonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflets (3–)4–5, straight hairs mostly absent (except on margins); inflorescences 1–3-flowered; New Mexico.</description>
      <determination>53 Potentilla sierrae-blancae</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves palmate, leaflets on tip or at least less than distal 1/10 of leaf axis</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves subpalmate to pinnate, leaflets on distal (1/10–)1/5–3/4+ of leaf axis</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Central leaflets: teeth (1–)2–5(–10) per side on distal 1/4 to whole margins.</description>
      <determination>46 Potentilla concinna</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Central leaflets: teeth 0–1(–3) per side on less than distal 1/5(–1/3) of margins.</description>
      <determination>47 Potentilla bicrenata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaflets 5(–7) on distal (1/10–)1/5–1/4 of leaf axis, usually only 1 pair of leaflets separate from terminal leaflets</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaflets 5–9(–11) on distal 1/5–3/4+ of leaf axis, usually at least 2 pairs of leaflets separate from terminal leaflets</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Central leaflets: teeth 2–5(–6) per side on distal 1/3 to whole margins.</description>
      <determination>46 Potentilla concinna</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Central leaflets: teeth (0–)1(–2) per side on distal 1/4(–1/3) or less of margins.</description>
      <determination>48 Potentilla angelliae</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaflets on distal 1/5–1/2 of leaf axis; Alberta, Montana.</description>
      <determination>49 Potentilla macounii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaflets on distal (1/4–)1/2–3/4+ of leaf axis; California, Nevada, Oregon, Washington</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaflet surfaces ± similar, adaxial: cottony hairs sparse to dense, straight hairs soft; Sierra Nevada, Cascade Range, and n Great Basin.</description>
      <determination>37 Potentilla breweri (sect. Multijugae)</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaflet surfaces ± to strongly dissimilar, adaxial: cottony hairs absent, straight hairs stiff; White Mountains and adjacent Sierra Nevada, California.</description>
      <determination>50 Potentilla morefieldii</determination>
    </key_statement>
  </key>
</bio:treatment>