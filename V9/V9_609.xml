<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="(W. Wight) S. C. Mason" date="1913" rank="species">havardii</taxon_name>
    <place_of_publication>
      <publication_title>J. Agric. Res.</publication_title>
      <place_in_publication>1: 153, 176. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species havardii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100393</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amygdalus</taxon_name>
    <taxon_name authority="W. Wight" date="unknown" rank="species">havardii</taxon_name>
    <place_of_publication>
      <publication_title>in W. R. Dudley et al., Dudley Mem. Vol.,</publication_title>
      <place_in_publication>133. 1913 (as harvardii)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amygdalus;species havardii</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Havard’s almond</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, suckering unknown, much branched, 10–20 dm, thorny.</text>
      <biological_entity id="o30326" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with axillary end buds, puberulent.</text>
      <biological_entity id="o30327" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="end" id="o30328" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="axillary end" />
      <relation from="o30327" id="r2000" name="with" negation="false" src="d0_s1" to="o30328" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o30329" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–3 mm, glabrous or puberulent, eglandular;</text>
      <biological_entity id="o30330" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade rhombic, obovate, or fan-shaped, 0.5–1.6 (–2) × 0.2–0.8 (–1.4) cm, base broadly obtuse or rounded to nearly truncate, margins serrate or dentate in distal 1/2, teeth blunt to sharp, some callus-tipped, rarely glandular, apex rounded to obtuse, surfaces puberulent.</text>
      <biological_entity id="o30331" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="fan--shaped" value_original="fan--shaped" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30332" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o30333" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character constraint="in distal 1/2" constraintid="o30334" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o30334" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o30335" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s4" to="sharp" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="callus-tipped" value_original="callus-tipped" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o30336" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o30337" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers.</text>
      <biological_entity id="o30338" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o30339" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0 mm.</text>
      <biological_entity id="o30340" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, plants dioecious, blooming at leaf emergence;</text>
      <biological_entity id="o30341" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o30342" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioecious" value_original="dioecious" />
        <character constraint="at leaf" constraintid="o30343" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o30343" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium campanulate, 2.5–3 mm, glabrous externally;</text>
      <biological_entity id="o30344" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals spreading to reflexed, triangular, 0.7–1 mm, margins entire, sparsely ciliate, surfaces glabrate;</text>
      <biological_entity id="o30345" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s9" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30346" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o30347" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, obovate, 2 mm;</text>
      <biological_entity id="o30348" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries hairy.</text>
      <biological_entity id="o30349" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes reddish-brown, ovoid, 8–11 mm, puberulent;</text>
      <biological_entity id="o30350" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium tardily deciduous;</text>
      <biological_entity id="o30351" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mesocarps leathery to dry (splitting);</text>
      <biological_entity id="o30352" name="mesocarp" name_original="mesocarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="leathery" name="texture" src="d0_s14" to="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stones ovoid, slightly flattened.</text>
      <biological_entity id="o30353" name="stone" name_original="stones" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun; fruiting Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Draws, dry rocky slopes of canyons, limestone soil, igneous rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky slopes" modifier="draws" constraint="of canyons , limestone soil ," />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="limestone soil" />
        <character name="habitat" value="rock" modifier="igneous" />
        <character name="habitat" value="igneous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Prunus havardii is endemic to the Chihuahuan Desert of trans-Pecos Texas and across the Rio Grande in Mexico, with most collections from the Big Bend area.</discussion>
  
</bio:treatment>