<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Matsumura" date="1901" rank="species">yedoensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag. (Tokyo)</publication_title>
      <place_in_publication>15: 100. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species yedoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242341757</other_info_on_name>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Yoshino cherry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, not suckering, 40–80 (–160) dm, not thorny.</text>
      <biological_entity id="o10522" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="160" to_unit="dm" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with terminal end buds, sparsely hairy.</text>
      <biological_entity id="o10523" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="end" id="o10524" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="terminal end" />
      <relation from="o10523" id="r647" name="with" negation="false" src="d0_s1" to="o10524" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o10525" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–20 mm, hairy, sometimes glandular distally, glands 1–2;</text>
      <biological_entity id="o10526" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10527" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic-ovate to obovate, 5–12 × 2.5–7 cm, base rounded, margins doubly serrate, teeth aristate, glandular, apex acuminate, abaxial surface hairy along midribs and veins, adaxial glabrous.</text>
      <biological_entity id="o10528" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10529" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o10530" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o10531" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10532" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10533" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="along veins" constraintid="o10535" is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10534" name="midrib" name_original="midribs" src="d0_s4" type="structure" />
      <biological_entity id="o10535" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o10536" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–6-flowered, corymbs;</text>
      <biological_entity id="o10537" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-6-flowered" value_original="2-6-flowered" />
      </biological_entity>
      <biological_entity id="o10538" name="corymb" name_original="corymbs" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central axes 4–8 (–20) mm.</text>
      <biological_entity constraint="central" id="o10539" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 15–25 mm (subtended by leafy bracts), hairy.</text>
      <biological_entity id="o10540" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers blooming before leaf emergence;</text>
      <biological_entity id="o10541" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="before leaf" constraintid="o10542" is_modifier="false" name="life_cycle" src="d0_s8" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o10542" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>hypanthium tubular, 7–8 mm, hairy externally;</text>
      <biological_entity id="o10543" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals spreading, ovatelanceolate, 4–5 mm, margins glandular-toothed, abaxial surface hairy, adaxial sparsely hairy;</text>
      <biological_entity id="o10544" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10545" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10546" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10547" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white or pink, broadly elliptic to obovate, 13–15 mm;</text>
      <biological_entity id="o10548" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s11" to="obovate" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries glabrous.</text>
      <biological_entity id="o10549" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Drupes black, subglobose, 7–12 mm, glabrous;</text>
      <biological_entity id="o10550" name="drupe" name_original="drupes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mesocarps fleshy;</text>
      <biological_entity id="o10551" name="mesocarp" name_original="mesocarps" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stones ellipsoid, not flattened.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16 (Japan).</text>
      <biological_entity id="o10552" name="stone" name_original="stones" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10553" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abandoned plantings, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="abandoned plantings" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., D.C., Wash.; e Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>