<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">heuchera</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="species">micrantha</taxon_name>
    <taxon_name authority="(Rydberg) Rosendahl" date="1936" rank="variety">diversifolia</taxon_name>
    <place_of_publication>
      <publication_title>Minnesota Stud. Pl. Sci.</publication_title>
      <place_in_publication>2: 42. 1936,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus heuchera;species micrantha;variety diversifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065945</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">diversifolia</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 102. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Heuchera;species diversifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_name authority="Rosendahl, Butters &amp; Lakela" date="unknown" rank="variety">pacifica</taxon_name>
    <taxon_hierarchy>genus Heuchera;species micrantha;variety pacifica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: petiole short to long-stipitate-glandular;</text>
      <biological_entity id="o14548" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o14549" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade pentagonal or heptagonal, deeply lobed.</text>
      <biological_entity id="o14550" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o14551" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" name="shape" src="d0_s1" value="heptagonal" value_original="heptagonal" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences sparsely long-stipitate-glandular.</text>
      <biological_entity id="o14552" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: hypanthium turbinate to campanulate, 1.5–4.9 × 1.2–3.4 mm, sparsely long-stipitate-glandular;</text>
      <biological_entity id="o14553" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o14554" name="hypanthium" name_original="hypanthium" src="d0_s3" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s3" to="campanulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s3" to="4.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s3" to="3.4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals 0.6–1.2 mm, apex acute to mucronate;</text>
      <biological_entity id="o14555" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o14556" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14557" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 0.2–0.7 mm wide;</text>
      <biological_entity id="o14558" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o14559" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>styles 0.4–3.6 mm. 2n = 14, 28.</text>
      <biological_entity id="o14560" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o14561" name="style" name_original="styles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14562" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
        <character name="quantity" src="d0_s6" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed coniferous forest on talus, slate, serpentine, basalt, or granite base</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed coniferous" />
        <character name="habitat" value="forest" constraint="on talus , slate , serpentine , basalt , or granite base" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="slate" />
        <character name="habitat" value="basalt" modifier="serpentine" />
        <character name="habitat" value="granite base" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4e.</number>
  <discussion>Variety diversifolia occurs in the Coast Ranges, Cascade Range, and Klamath and Santa Lucia mountains.</discussion>
  
</bio:treatment>