<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">159</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1805" rank="genus">bryophyllum</taxon_name>
    <taxon_name authority="(Raymond-Hamet &amp; H. Perrier) Lauzac-Marchal" date="1974" rank="species">fedtschenkoi</taxon_name>
    <place_of_publication>
      <publication_title>Compt. Rend. Hebd. Séances Acad. Sci., Sér. D</publication_title>
      <place_in_publication>278: 2508. 1974  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus bryophyllum;species fedtschenkoi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092038</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalanchoë</taxon_name>
    <taxon_name authority="Raymond-Hamet &amp; H. Perrier" date="unknown" rank="species">fedtschenkoi</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Colon. Marseille, sér.</publication_title>
      <place_in_publication>3, 3: 75. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Kalanchoë;species fedtschenkoi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, polycarpic, gray-green to green or purple, glaucous or not.</text>
      <biological_entity id="o15757" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polycarpic" value_original="polycarpic" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s0" to="green or purple" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s0" value="not" value_original="not" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems suberect or later decumbent and rooting, branched, terete, rough with leaf-scars, to 8 dm × 1 cm.</text>
      <biological_entity id="o15758" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="later" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character constraint="with leaf-scars" constraintid="o15759" is_modifier="false" name="pubescence_or_relief" src="d0_s1" value="rough" value_original="rough" />
        <character char_type="range_value" from="0" from_unit="dm" name="length" notes="" src="d0_s1" to="8" to_unit="dm" />
        <character name="width" notes="" src="d0_s1" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15759" name="leaf-scar" name_original="leaf-scars" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite, evenly spaced, simple;</text>
      <biological_entity id="o15760" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s2" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole terete, (often turned obliquely), 1–6 mm;</text>
      <biological_entity id="o15761" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lavender-gray, obovate, 2–10 × 1.5–8 cm, margins crenate throughout or apically, apex obtuse, surfaces glaucous;</text>
      <biological_entity id="o15762" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="lavender-gray" value_original="lavender-gray" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15763" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout; throughout; apically" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o15764" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o15765" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bulbils borne mostly in notches of leaf margins on damaged or fallen leaves.</text>
      <biological_entity id="o15766" name="bulbil" name_original="bulbils" src="d0_s5" type="structure" />
      <biological_entity id="o15767" name="notch" name_original="notches" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o15768" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o15769" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="condition" src="d0_s5" value="damaged" value_original="damaged" />
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="fallen" value_original="fallen" />
      </biological_entity>
      <relation from="o15766" id="r1340" name="borne" negation="false" src="d0_s5" to="o15767" />
      <relation from="o15766" id="r1341" name="borne" negation="false" src="d0_s5" to="o15768" />
      <relation from="o15766" id="r1342" name="borne" negation="false" src="d0_s5" to="o15769" />
    </statement>
    <statement id="d0_s6">
      <text>Cymes open, corymbiform, to 1.5 dm diam.;</text>
      <biological_entity id="o15770" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
        <character char_type="range_value" from="0" from_unit="dm" name="diameter" src="d0_s6" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches to 5 cm.</text>
      <biological_entity id="o15771" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 5–15 mm.</text>
      <biological_entity id="o15772" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx violet-flecked, inflated, 10–18 mm, tube 6–11 mm, lobes deltate, 4–7 mm, shorter than tube, apex acute;</text>
      <biological_entity id="o15773" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15774" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="violet-flecked" value_original="violet-flecked" />
        <character is_modifier="false" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15775" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15776" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character constraint="than tube" constraintid="o15777" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15777" name="tube" name_original="tube" src="d0_s9" type="structure" />
      <biological_entity id="o15778" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla orange-red, 18–25 mm, contracted basally, lobes obovate, 5–8 mm, apex obtuse to rounded.</text>
      <biological_entity id="o15779" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15780" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange-red" value_original="orange-red" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o15781" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity id="o15782" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="rounded" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15783" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., Tex.; Indian Ocean Islands (Madagascar).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Lavender scallops</other_name>
  <discussion>Apparently, Bryophyllum fedtschenkoi is established only locally in the flora area, in waste places in southwestern Florida (Sanibel Island) and on clay mounds among native shrubs in coastal southern Texas.</discussion>
  
</bio:treatment>