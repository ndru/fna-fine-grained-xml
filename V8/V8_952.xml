<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="illustration_page">488</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">empetrum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">nigrum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1022. 1753,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus empetrum;species nigrum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200012671</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Empetrum</taxon_name>
    <taxon_name authority="Fernald &amp; Wiegand" date="unknown" rank="species">eamesii</taxon_name>
    <taxon_name authority="(Hagerup) D. Löve" date="unknown" rank="subspecies">hermaphroditum</taxon_name>
    <taxon_hierarchy>genus Empetrum;species eamesii;subspecies hermaphroditum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Empetrum</taxon_name>
    <taxon_name authority="Hagerup" date="unknown" rank="species">hermaphroditum</taxon_name>
    <taxon_hierarchy>genus Empetrum;species hermaphroditum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Empetrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nigrum</taxon_name>
    <taxon_name authority="(Hagerup) Böcher" date="unknown" rank="subspecies">hermaphroditum</taxon_name>
    <taxon_hierarchy>genus Empetrum;species nigrum;subspecies hermaphroditum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Empetrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nigrum</taxon_name>
    <taxon_name authority="(Hagerup) T. J. Sørensen" date="unknown" rank="variety">hermaphroditum</taxon_name>
    <taxon_hierarchy>genus Empetrum;species nigrum;variety hermaphroditum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Branches green, brown, or reddish, distally glabrous, eglandular or glandular, sparsely white-tomentose.</text>
      <biological_entity id="o14704" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves divergent, becoming reflexed;</text>
      <biological_entity id="o14705" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 2.5–7 mm.</text>
      <biological_entity id="o14706" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers unisexual or bisexual (plants synoecious, sometimes polygamous, or dioecious);</text>
      <biological_entity id="o14707" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s3" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals greenish pink becoming reddish purple.</text>
      <biological_entity id="o14708" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s4" value="greenish pink becoming reddish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Drupes black, opaque, 5–10 mm diam.</text>
      <biological_entity id="o14709" name="drupe" name_original="drupes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="opaque" value_original="opaque" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seeds light-brown, 1.5–3 mm. 2n = 26, 39, 52.</text>
      <biological_entity id="o14710" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14711" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="26" value_original="26" />
        <character name="quantity" src="d0_s6" value="39" value_original="39" />
        <character name="quantity" src="d0_s6" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed, coastal bluffs and sphagnum bogs, windswept southern arctic and alpine tundra and open subalpine and boreal forests and mountain summits</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" modifier="exposed" />
        <character name="habitat" value="sphagnum bogs" />
        <character name="habitat" value="windswept southern arctic" />
        <character name="habitat" value="alpine tundra" />
        <character name="habitat" value="open subalpine" />
        <character name="habitat" value="boreal forests" />
        <character name="habitat" value="mountain summits" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Maine, Mich., Minn., N.H., N.Y., Oreg., Vt., Wash.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>All black-fruited plants of the genus in North America are here considered to be Empetrum nigrum in the broad sense. There is great variation within the species, and various names have been proposed. Boreal and arctic diploids (2n = 26, E. Hultén 1968) with unisexual flowers, dioecious plants, have been treated as E. nigrum, and tetraploids (2n = 52) with bisexual flowers, synoecious plants, as E. hermaphroditum.</discussion>
  <discussion>Empetrum nigrum is typified on European, dioecious, diploid plants, and in this strict sense, there has been a question whether the name should be applied to North American plants (Á. Löve and D. Löve 1959) in areas with synoecious tetraploids, where diploids were thought to be absent.</discussion>
  <discussion>Dioecious, diploid, black-fruited crowberries are now well known from Maine, New Brunswick, Newfoundland, and Nova Scotia (J. Maunder, pers. comm.; P. Morisset, pers. comm.). There appears to be some ecological separation between the dioecious Empetrum nigrum and the synoecious E. nigrum; the dioecious plants in Newfoundland are reported to be replaced by the synoecious ones at higher elevations or in very exposed localities (J. Maunder, pers. comm.).</discussion>
  <discussion>Dioecious plants have been long known in the Pacific Northwest (J. A. Calder and R. L. Taylor 1968) and Alaska (E. Hultén 1968), and diploids have now been confirmed for the Cascade Range, Washington; Prince Rupert, British Columbia; and the Kenai Peninsula, Alaska (V. Mirré 2004). Some of these western diploids have chloroplast DNA more closely related to South American accessions (diploid Empetrum rubrum) than to any Northern Hemisphere Empetrum investigated with chloroplast trnS-trnfM and trnS-trnG sequences (M. Popp et al., unpubl.). The nuclear sequence data show a more complex pattern wherein the alleles found in the South American accessions represent a subset of the alleles found in the Northern Hemisphere accessions (nuclear low copy RPB2 and RPC2 sequences).</discussion>
  <discussion>Empetrum hermaphroditum, a name in prominent usage, is typified on a Greenlandic, synoecious tetraploid, and this name and combinations of it have been applied to virtually all black-fruited synoecious plants in North America and wherever else black-fruited, synoecious crowberries are found (cf. Á. Löve and D. Löve 1959). In Europe, in addition to the diploids, there are synoecious tetraploids of E. nigrum.</discussion>
  <discussion>V. Mirré (2004) concluded that tetraploid synoecious plants have arisen multiple times from different combinations of diploids resulting in a large pool of unstructured genetic variation. We conclude that synoecious tetraploids do not necessarily represent a distinct lineage and must, for the present, be subsumed by Empetrum nigrum as understood here.</discussion>
  <discussion>The population of Empetrum nigrum reported from Montauk Point, Long Island, New York, has been extirpated (S. E. Clemants, pers. comm.).</discussion>
  
</bio:treatment>