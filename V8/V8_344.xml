<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">DUDLEYA</taxon_name>
    <place_of_publication>
      <publication_title>New N. Amer. Crassul.,</publication_title>
      <place_in_publication>12. 1903  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus DUDLEYA</taxon_hierarchy>
    <other_info_on_name type="etymology">For William Russel Dudley, 1849–1911, American botanist</other_info_on_name>
    <other_info_on_name type="fna_id">111029</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, not viviparous, 7–10 dm, glabrous.</text>
      <biological_entity id="o21344" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="7" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems above ground (caudex) or underground (corm), usually erect, simple or 2-branched at apex, rarely with lateral vegetative branches, often fleshy;</text>
      <biological_entity id="o21345" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="at apex" constraintid="o21346" is_modifier="false" name="architecture" src="d0_s1" value="2-branched" value_original="2-branched" />
        <character is_modifier="false" modifier="often" name="texture" notes="" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21346" name="apex" name_original="apex" src="d0_s1" type="structure" />
      <biological_entity constraint="lateral" id="o21347" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o21345" id="r1761" modifier="rarely" name="with" negation="false" src="d0_s1" to="o21347" />
    </statement>
    <statement id="d0_s2">
      <text>floral stems annual, from rosette leaf-axil, simple or branched, overtopping rosette, with scattered smaller leaves.</text>
      <biological_entity constraint="floral" id="o21348" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="rosette" id="o21349" name="leaf-axil" name_original="leaf-axil" src="d0_s2" type="structure" />
      <biological_entity id="o21350" name="rosette" name_original="rosette" src="d0_s2" type="structure" />
      <biological_entity constraint="smaller" id="o21351" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o21348" id="r1762" name="from" negation="false" src="d0_s2" to="o21349" />
      <relation from="o21348" id="r1763" name="overtopping" negation="false" src="d0_s2" to="o21350" />
      <relation from="o21348" id="r1764" name="with" negation="false" src="d0_s2" to="o21351" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent or withering in early summer, drying persistent and often covering caudex for years, densely crowded in basal or terminal rosette, alternate, subclasping basally, petiolate or sessile;</text>
      <biological_entity id="o21352" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="in early summer" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
        <character is_modifier="false" name="condition" src="d0_s3" value="drying" value_original="drying" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="in rosette" constraintid="o21355" is_modifier="false" modifier="densely" name="arrangement" notes="" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21353" name="caudex" name_original="caudex" src="d0_s3" type="structure" />
      <biological_entity id="o21354" name="year" name_original="years" src="d0_s3" type="structure" />
      <biological_entity id="o21355" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o21352" id="r1765" modifier="often" name="covering" negation="false" src="d0_s3" to="o21353" />
      <relation from="o21352" id="r1766" name="for" negation="false" src="d0_s3" to="o21354" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to orbiculate, nearly laminar to terete or subglobose, 0.2–30 cm, fleshy, base not spurred, margins entire, not ciliate;</text>
      <biological_entity id="o21356" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="orbiculate" />
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s4" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="nearly laminar" name="shape" src="d0_s4" to="terete" />
        <character is_modifier="false" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21357" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o21358" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>veins entering margins.</text>
      <biological_entity id="o21359" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o21360" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <relation from="o21359" id="r1767" name="entering" negation="false" src="d0_s5" to="o21360" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences lateral cymes, 2+-cincinnate.</text>
      <biological_entity id="o21361" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="2+-cincinnate" value_original="2+-cincinnate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21362" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o21363" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (usually odorless), erect to pendent, 5-merous;</text>
      <biological_entity id="o21364" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals connate basally, all alike, shorter than petals;</text>
      <biological_entity id="o21365" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="alike" value_original="alike" />
        <character constraint="than petals" constraintid="o21366" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21366" name="petal" name_original="petals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals erect and forming cylindric to 5-gonal tube or spreading from near middle, connate basally or to middle, white, yellow, orange, or red;</text>
      <biological_entity id="o21367" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character constraint="from middle" constraintid="o21369" is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="basally" name="fusion" notes="" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="to middle" value_original="to middle" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o21368" name="tube" name_original="tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="cylindric" is_modifier="true" name="shape" src="d0_s10" to="5-gonal" />
      </biological_entity>
      <biological_entity id="o21369" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <biological_entity id="o21370" name="middle" name_original="middle" src="d0_s10" type="structure" />
      <relation from="o21367" id="r1768" name="forming" negation="false" src="d0_s10" to="o21368" />
      <relation from="o21367" id="r1769" name="to" negation="false" src="d0_s10" to="o21370" />
    </statement>
    <statement id="d0_s11">
      <text>nectary disc truncate, wider than tall;</text>
      <biological_entity constraint="nectary" id="o21371" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="width" src="d0_s11" value="wider than tall" value_original="wider than tall" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10;</text>
      <biological_entity id="o21372" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments adnate to corolla base;</text>
      <biological_entity id="o21373" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="to corolla base" constraintid="o21374" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o21374" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pistils erect or ascending to spreading, nearly distinct;</text>
      <biological_entity id="o21375" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s14" to="spreading" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary base rounded;</text>
      <biological_entity constraint="ovary" id="o21376" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles shorter than ovary.</text>
      <biological_entity id="o21377" name="style" name_original="styles" src="d0_s16" type="structure">
        <character constraint="than ovary" constraintid="o21378" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21378" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits erect to spreading.</text>
      <biological_entity id="o21379" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s17" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly ovoid, longitudinally ribbed, finely cross-ribbed.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 17.</text>
      <biological_entity id="o21380" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="longitudinally" name="architecture_or_shape" src="d0_s18" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s18" value="cross-ribbed" value_original="cross-ribbed" />
      </biological_entity>
      <biological_entity constraint="x" id="o21381" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Live-forever</other_name>
  <other_name type="common_name">siempreviva</other_name>
  <discussion>Species ca. 45 (26 species in the flora).</discussion>
  <discussion>Dudleya is distinguished by its erect sepals usually connivent to the corolla and its corolla convolute in bud, rarely imbricate. The epipetalous stamens are mostly shorter and adnate higher than the episepalous ones and are not reflexed.</discussion>
  <discussion>The center of distribution for all three subgenera of Dudleya is near the coast north and south of San Diego, California. Because this area is now largely urban, many of the endemics are more or less threatened: M. W. Skinner and B. M. Pavlik (1994) listed 27 species and subspecies of Dudleya as to some degree rare and endangered. However, most species, including narrow endemics, are abundant where they do grow.</discussion>
  <discussion>The herbage of some species, such as Dudleya edulis, is reported to have been eaten by Native Americans. Fanciers grow some species, and although the plants have a reputation for being hard to grow, some are easy enough (P. H. Thomson 1993). Some find a place in Californian rock gardens; none is in general cultivation.</discussion>
  <discussion>C. H. Uhl has studied nearly 400 collections of Dudleya, including all known species (C. H. Uhl and R. V. Moran 1953). All have a basic chromosome number of 17, and about 35% of populations are polyploid, some up to 16-ploid (Uhl 1994). D. Verity (pers. comm.) was able to cross Dudleya species in every combination he tried, regardless of morphology and level of ploidy, including such improbable hybrids as D. blochmaniae × pulverulenta, crossing two of the most unlike. Also, natural hybrids are known between most diploids that grow together; the hybrids are mostly rare (Moran 1951; Moran and Uhl 1952; P. H. Thomson 1993). All diploid hybrids that Uhl studied showed very good chromosome pairing, with no detectable abnormalities at meiosis.</discussion>
  <discussion>In subg. Dudleya and Stylophyllum an occasional rosette may reroot after breaking off or after the common stem dies and decays; in contrast to the many Crassulaceae that readily multiply by leaves and plantlets, these plants come almost solely from seeds. In subg. Hasseanthus, the rosette leaves do sometimes root and form new plants (M. Dodero 1996).</discussion>
  <discussion>The most primitive species would seem to be those large and much-branched plants of subg. Stylophyllum with broad, flat leaves, much-branched cymes, and open Sedum-like flowers, such as Dudleya virens subsp. insularis. At the other extreme is D. brevifolia, still with open flowers but with underground tuberous stems, tiny vernal rosettes of subglobose leaves on threadlike petioles, and relatively few flowers. The most-advanced inflorescence and flowers are in hummingbird-pollinated D. pulverulenta, of subg. Dudleya.</discussion>
  <discussion>The first-named species of Dudleya were placed in Cotyledon, Echeveria, or Sedum; A. Berger (1930), P. A. Munz (1935), and W. L. Jepson (1909–1943, vol. 2) treated Dudleya species under Echeveria. The two are quite distinct and evidently have a long, separate history. Dudleya is centered on the Pacific tectonic plate, with only a few species extending onto the western edge of the North American plate; Echeveria and relatives are on the North American plate and southward (C. H. Uhl 1994).</discussion>
  <discussion>Although all species of Dudleya readily hybridize, and also species of the Echeveria group, all attempts to cross Dudleya with Echeveria and relatives have failed (C. H. Uhl 1994). Furthermore, Uhl concluded that chromosome changes in Dudleya have been mainly through gene mutations at the molecular level, with few or no changes in gene sequences; in Echeveria and relatives, he found a wide range of diploid chromosome numbers, with evidence of rearrangements of gene sequences (Uhl 1992). These different mechanisms of chromosome change suggested that the two groups might not be so closely related as usually thought (Uhl 1993). From the different structures of their testas, U. Knapp (1994) agreed; H. ’t Hart (1995) thought that the two probably came from different mostly Old-World lineages: Dudleya from his paraphyletic Leucosedum-clade, with such genera as Pistorinia and Rosularia and with Sedum album, and allies; and Echeveria from his paraphyletic Acre-clade, with the other Mexican genera Graptopetalum, Pachyphytum, Sedum in part, and Villadia, and with Sedum acre, and allies, in the Old World.</discussion>
  <discussion>My descriptions are based largely on live plants. The five species of subg. Hasseanthus and the six of subg. Stylophyllum are fairly well defined and distinct; subg. Dudleya, despite some help from chromosome numerology, is still notoriously difficult. Although such diploids as Dudleya stolonifera are distinct endemics, D. abramsii and D. cymosa are wide-ranging and polymorphic diploids, whose diverse populations do not sort out easily into geographic subspecies. Similarly, such polyploids as D. nesiotica and D. traskiae are distinct endemics; others form difficult complexes, thus far defying classification. Hence the treatment of some species, and the keys to them, remain quite unsatisfactory.</discussion>
  <references>
    <reference>Bartel, J. A. 1993. Dudleya. In: J. C. Hickman, ed. 1993. The Jepson Manual. Higher Plants of California. Berkeley, Los Angeles, and London. Pp. 525–530.</reference>
    <reference>Moran, R. V. 1951. Natural hybrids between Dudleya and Hasseanthus. Bull. S. Calif. Acad. Sci. 50: 57–67.</reference>
    <reference>Moran, R. V. 1960. Dudleya. In: H. Jacobsen. 1960. A Handbook of Succulent Plants.... 3 vols. London. Vol. 1, pp. 344–359.</reference>
    <reference>Thomson, P. H. 1993. Dudleya and Hasseanthus Handbook. Bonsall, Calif.</reference>
    <reference>Uhl, C. H. and R. V. Moran. 1953. The cytotaxonomy of Dudleya and Hasseanthus. Amer. J. Bot. 40: 492–502.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems underground, mostly unbranched distally, tuberous corms; leaves mostly withering by anthesis, blade turgid except at very thin base; petioles present; corollas widely open, petals ascending to spreading from near middle; California.</description>
      <determination>10c Dudleya subg. Hasseanthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems above ground, often branching, sometimes simple, caudices; leaves usually persistent, rarely withering in early summer, blade turgid ± throughout; petioles absent; corollas widely or barely open, petals erect, ascending, outcurved, or spreading from near middle; sw United States</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas widely open, petals ascending, outcurved, or spreading from near middle; pistils well separated, usually not connivent, ascending to spreading in flower and in fruit; sw California.</description>
      <determination>10a Dudleya subg. Stylophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas usually tubular or 5-gonal, barely open, petals erect to rarely ascending, sometimes tips outcurved; pistils connivent and erect in flower, sometimes not connivent and suberect, nearly erect or slightly ascending in fruit; sw United States.</description>
      <determination>10b Dudleya subg. Dudleya</determination>
    </key_statement>
  </key>
</bio:treatment>