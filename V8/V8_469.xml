<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="treatment_page">224</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Rose" date="1904" rank="genus">LENOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Misc. Collect.</publication_title>
      <place_in_publication>47: 159, figs. 18, 19, plate 20. 1904  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus LENOPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lenos, trough, and phyllon, leaf</other_info_on_name>
    <other_info_on_name type="fna_id">117955</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, not viviparous, 1–7 dm, glabrous.</text>
      <biological_entity id="o1692" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branching near base, herbaceous.</text>
      <biological_entity id="o1693" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="near base" constraintid="o1694" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="growth_form_or_texture" notes="" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o1694" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves caducous, in basal rosettes and cauline, (± alike, gradually smaller distally), proximal opposite, distal alternate, (sometimes crowded at base), sessile, not connate basally;</text>
      <biological_entity id="o1695" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="position" notes="" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1696" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o1697" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1698" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o1695" id="r152" name="in" negation="false" src="d0_s2" to="o1696" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic or ovatelanceolate to oblanceolate, planoconvex, 1–2.5 cm, fleshy, base not spurred, margins entire;</text>
      <biological_entity id="o1699" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="planoconvex" value_original="planoconvex" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o1700" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o1701" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins not conspicuous.</text>
      <biological_entity id="o1702" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal spikes, racemes, or narrow thyrses, (usually 2+ per node, rarely solitary).</text>
      <biological_entity id="o1703" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o1704" name="spike" name_original="spikes" src="d0_s5" type="structure" />
      <biological_entity id="o1705" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o1706" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: flowers subsessile.</text>
      <biological_entity id="o1707" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o1708" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect, 5-merous;</text>
      <biological_entity id="o1709" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals distinct, subequal;</text>
      <biological_entity id="o1710" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals erect with outcurved tips, distinct, buff or dull yellow often marked with red;</text>
      <biological_entity id="o1711" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="with tips" constraintid="o1712" is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="buff" value_original="buff" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="yellow often marked with red" />
      </biological_entity>
      <biological_entity id="o1712" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="outcurved" value_original="outcurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o1713" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o1715" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o1714" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o1715" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o1715" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectaries subquadrate;</text>
      <biological_entity id="o1716" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10;</text>
      <biological_entity id="o1717" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments of epipetalous stamens adnate to corolla base;</text>
      <biological_entity id="o1718" name="filament" name_original="filaments" src="d0_s13" type="structure" constraint="stamen" constraint_original="stamen; stamen">
        <character constraint="to corolla base" constraintid="o1720" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="epipetalous" id="o1719" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity constraint="corolla" id="o1720" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o1718" id="r153" name="part_of" negation="false" src="d0_s13" to="o1719" />
    </statement>
    <statement id="d0_s14">
      <text>pistils erect, nearly distinct;</text>
      <biological_entity id="o1721" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary base truncate tapering to slender, outcurved styles;</text>
      <biological_entity constraint="ovary" id="o1722" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="size" src="d0_s15" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o1723" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="outcurved" value_original="outcurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2+ times shorter than ovaries.</text>
      <biological_entity id="o1724" name="style" name_original="styles" src="d0_s16" type="structure">
        <character constraint="ovary" constraintid="o1725" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2+ times shorter than ovaries" />
      </biological_entity>
      <biological_entity id="o1725" name="ovary" name_original="ovaries" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits erect.</text>
      <biological_entity id="o1726" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ellipsoid, finely grooved.</text>
      <biological_entity id="o1727" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="finely" name="architecture" src="d0_s18" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion>Species 7 (1 in the flora).</discussion>
  <discussion>C. H. Uhl (1993, 1996) crossed four species of Lenophyllum with plants of Echeveria, Graptopetalum, or Pachyphytum, thus placing it in the same large comparium; it seemed less closely related to the other genera than they to each other. Also, in contrast to other genera of the comparium, where polyploids generally are autoploid, here all species studied appeared to be alloploids, although of unknown parentage.</discussion>
  <references>
    <reference>Moran, R. V. 1994. The genus Lenophyllum (Crassulaceae). Haseltonia 2: 1–19.</reference>
    <reference>Rose, J. N. 1923. Lenophyllum texanum. Addisonia 8: 21.</reference>
    <reference>Uhl, C. H. 1996. Chromosomes and polyploidy in Lenophyllum (Crassulaceae). Amer. J. Bot. 83: 216–220.</reference>
    <reference>Uhl, C. H. 1993. Intergeneric hybrids of Mexican Crassulaceae. I. Lenophyllum. Cact. Succ. J. (Los Angeles) 65: 271–273.</reference>
  </references>
  
</bio:treatment>