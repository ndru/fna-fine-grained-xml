<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">saxifraga</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">rivularis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 404. 1753,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus saxifraga;species rivularis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092023</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (delicate), loosely tufted or matted, (green to purple), stoloniferous, weakly rhizomatous, (with bulbils in axils of basal leaves).</text>
      <biological_entity id="o3985" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o3986" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole ± flattened, 2–30 (–50) mm;</text>
      <biological_entity id="o3987" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade reniform, 3–5 (–7) -lobed (lobes rounded, sometimes ± obtuse, distalmost unlobed), (2.6–) 3.7–5.2 (–7.4) mm, slightly fleshy, margins entire, eciliate or sparsely glandular-ciliate, without lime-secreting hydathodes (with nonsecreting hydathodes at lobe apices), apex acute, surfaces glabrous.</text>
      <biological_entity id="o3988" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="7.4" to_unit="mm" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s3" to="5.2" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o3989" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
      <biological_entity id="o3990" name="hydathode" name_original="hydathodes" src="d0_s3" type="structure" />
      <biological_entity id="o3991" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3992" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o3989" id="r331" name="without" negation="false" src="d0_s3" to="o3990" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–3 (–5) -flowered cymes, sometimes solitary flowers, (flowers pedicellate), 1.7–7 cm, glabrous or sparsely to ± densely tangled, pink-tipped stipitate-glandular;</text>
      <biological_entity id="o3993" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o3994" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="2-3(-5)-flowered" value_original="2-3(-5)-flowered" />
      </biological_entity>
      <biological_entity id="o3995" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely to more or less densely" name="arrangement" src="d0_s4" value="tangled" value_original="tangled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pink-tipped" value_original="pink-tipped" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts (2–3), petiolate, (unlobed or 1–3-lobed, reduced).</text>
      <biological_entity id="o3996" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers (hypanthium U-shaped in longisection);</text>
      <biological_entity id="o3997" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, (sometimes rusty brown), elliptic to ovate, margins eciliate or sparsely glandular-ciliate, glabrous or sparsely to densely stipitate-glandular;</text>
      <biological_entity id="o3998" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
      <biological_entity id="o3999" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparsely glandular-ciliate glabrous or" name="pubescence" src="d0_s7" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="sparsely glandular-ciliate glabrous or" name="pubescence" src="d0_s7" to="sparsely densely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, sometimes pink tinged, not spotted, oblong to elliptic, 2–6 mm, to 2–3 times length of sepals;</text>
      <biological_entity id="o4000" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="pink tinged" value_original="pink tinged" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o4001" is_modifier="false" name="length" src="d0_s8" value="2-3 times length of sepals" />
      </biological_entity>
      <biological_entity id="o4001" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary 1/2 inferior.</text>
      <biological_entity id="o4002" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s9" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nunavut, Que.; Alaska, N.H.; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Some reports of Saxifraga sibirica Linnaeus from Canada are misidentifications of this species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants wholly green or purple only in inflorescences; hair crosswalls usually without color, rarely pale purple; inflorescences 2.7-7 cm, glabrous or sparsely stipitate-glandular; hypanthia sparsely short stipitate-glandular, hairs 0.1-0.3(-0.4) mm.</description>
      <determination>21a Saxifraga rivularis subsp. rivularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants mostly purple (at least inflorescences); hair crosswalls purple; inflorescences 1.7-3 cm, sparsely to densely stipitate-glandular; hypanthia sparsely to densely long stipitate-glandular, hairs (0.2-)0.3-0.6(-1.1) mm.</description>
      <determination>21b Saxifraga rivularis subsp. arctolitoralis</determination>
    </key_statement>
  </key>
</bio:treatment>