<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1817" rank="genus">phedimus</taxon_name>
    <taxon_name authority="(Linnaeus) ’t Hart in H. ’t Hart and U. Eggli" date="1995" rank="species">aizoon</taxon_name>
    <place_of_publication>
      <publication_title>in H. ’t Hart and U. Eggli, Evol. Syst. Crassulaceae,</publication_title>
      <place_in_publication>168. 1995,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus phedimus;species aizoon</taxon_hierarchy>
    <other_info_on_name type="fna_id">241000261</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">aizoon</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 430. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sedum;species aizoon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="Fischer &amp; C. A. Meyer" date="unknown" rank="species">kamtschaticum</taxon_name>
    <taxon_name authority="(Praeger) R. T. Clausen" date="unknown" rank="subspecies">ellacombeanum</taxon_name>
    <taxon_hierarchy>genus Sedum;species kamtschaticum;subspecies ellacombeanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowering-stems erect, 1.5–5 dm.</text>
      <biological_entity id="o16751" name="flowering-stem" name_original="flowering-stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate;</text>
      <biological_entity id="o16752" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic-lanceolate, 4–8 cm, margins not glandular, those of distal leaves irregularly serrate.</text>
      <biological_entity id="o16753" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16754" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16755" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o16754" id="r1428" name="part_of" negation="false" src="d0_s2" to="o16755" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals linear to lanceolate, 3.5–4.5 mm, apex blunt;</text>
      <biological_entity id="o16756" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o16757" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16758" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals erect basally, widely spreading distally, yellow, oblong to elliptic-lanceolate, 7–9 mm, apex mucronate.</text>
      <biological_entity id="o16759" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o16760" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="widely; distally" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic-lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16761" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Carpels connate basally, divergent in fruit.</text>
      <biological_entity id="o16762" name="carpel" name_original="carpels" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
        <character constraint="in fruit" constraintid="o16763" is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o16763" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky places, scrapes, old quarries</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky places" modifier="open" />
        <character name="habitat" value="scrapes" />
        <character name="habitat" value="old quarries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Man., N.B., N.W.T., Ont., Que., Sask.; Iowa, Mass., Minn., N.H., N.J., N.Y., Pa.; e, n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Orpin aizoon</other_name>
  
</bio:treatment>