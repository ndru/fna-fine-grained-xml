<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">arctostaphylos</taxon_name>
    <taxon_name authority="Parry" date="1887" rank="species">manzanita</taxon_name>
    <taxon_name authority="P. V. Wells" date="1988" rank="subspecies">glaucescens</taxon_name>
    <place_of_publication>
      <publication_title>Four Seasons</publication_title>
      <place_in_publication>8(1): 51. 1988,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus arctostaphylos;species manzanita;subspecies glaucescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092396</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants killed by fire;</text>
      <biological_entity id="o22663" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22664" name="fire" name_original="fire" src="d0_s0" type="structure" />
      <relation from="o22663" id="r1894" name="killed by" negation="false" src="d0_s0" to="o22664" />
    </statement>
    <statement id="d0_s1">
      <text>burl absent;</text>
      <biological_entity id="o22665" name="burl" name_original="burl" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs usually glabrous or sparsely short-hairy, sometimes sparsely glandular-hairy.</text>
      <biological_entity id="o22666" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s2" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades lightly to strongly glaucous-green.</text>
      <biological_entity id="o22667" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="lightly to strongly" name="coloration" src="d0_s3" value="glaucous-green" value_original="glaucous-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: axis 1 mm diam.</text>
      <biological_entity id="o22668" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o22669" name="axis" name_original="axis" src="d0_s4" type="structure">
        <character name="diameter" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ovaries glabrous, sometimes glandular-hairy.</text>
      <biological_entity id="o22670" name="ovary" name_original="ovaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits glabrous, sometimes glandular-hairy.</text>
      <biological_entity id="o22671" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Stones distinct.</text>
      <biological_entity id="o22672" name="stone" name_original="stones" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, foothill woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="foothill woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>58c.</number>
  <discussion>Subspecies glaucescens is widespread in the southern part of the outer North Coast Range. A shiny-green-leaved population with slender immature inflorescences in western Sonoma County (Annapolis), within the range of subsp. glaucescens, may be distinct.</discussion>
  
</bio:treatment>