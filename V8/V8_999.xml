<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Debra K. Trock</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">497</other_info_on_meta>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">GAULTHERIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 395. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 187. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus gaultheria;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Jean-François Gaulthier, 1708–1756, botanist and physician of Québec</other_info_on_name>
    <other_info_on_name type="fna_id">113331</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs, (sometimes rhizomatous or stoloniferous and rooting at nodes).</text>
      <biological_entity id="o16868" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or procumbent;</text>
      <biological_entity id="o16870" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs glabrous or hairy.</text>
      <biological_entity id="o16871" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, aromatic;</text>
      <biological_entity id="o16872" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, elliptic, or orbiculate to subcordate or reniform, coriaceous, margins serrate, crenate, or ciliate, plane or revolute, surfaces glabrous or hairy;</text>
      <biological_entity id="o16873" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="subcordate or reniform" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="subcordate or reniform" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o16874" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation reticulodromous or brochidodromous.</text>
      <biological_entity id="o16875" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="reticulodromous" value_original="reticulodromous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="brochidodromous" value_original="brochidodromous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, racemes, 2–12-flowered, sometimes flowers solitary;</text>
      <biological_entity id="o16876" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o16877" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-12-flowered" value_original="2-12-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>(bracteoles closely subtending flowers).</text>
      <biological_entity id="o16878" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals (4–) 5, connate basally to nearly their entire lengths, (sometimes exceeding petals), ovate, deltate, or cordate;</text>
      <biological_entity id="o16879" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16880" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally to nearly" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="nearly" name="lengths" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (4–) 5, connate ca. 1/2 to nearly their entire lengths, white or cream to pink, corolla urceolate to campanulate, lobes much shorter than tube;</text>
      <biological_entity id="o16881" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16882" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="nearly" name="lengths" src="d0_s9" value="entire" value_original="entire" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s9" to="pink" />
      </biological_entity>
      <biological_entity id="o16883" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="urceolate" name="shape" src="d0_s9" to="campanulate" />
      </biological_entity>
      <biological_entity id="o16884" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character constraint="than tube" constraintid="o16885" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o16885" name="tube" name_original="tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 8 or 10, included, (inserted at base of ovary);</text>
      <biological_entity id="o16886" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16887" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" unit="or" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments straight, flattened, usually widest proximally, glabrous or hairy, sometimes papillose, without spurs;</text>
      <biological_entity id="o16888" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16889" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="usually; proximally" name="width" src="d0_s11" value="widest" value_original="widest" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o16890" name="spur" name_original="spurs" src="d0_s11" type="structure" />
      <relation from="o16889" id="r1439" name="without" negation="false" src="d0_s11" to="o16890" />
    </statement>
    <statement id="d0_s12">
      <text>anthers with 2–4 awns or without awns, dehiscent by pores with ventral slits, (white disintegration tissue present dorsally along connective);</text>
      <biological_entity id="o16891" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16892" name="anther" name_original="anthers" src="d0_s12" type="structure" />
      <biological_entity id="o16893" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o16894" name="pore" name_original="pores" src="d0_s12" type="structure" />
      <biological_entity constraint="ventral" id="o16895" name="slit" name_original="slits" src="d0_s12" type="structure" />
      <relation from="o16892" id="r1440" name="with 2-4 awns or without" negation="false" src="d0_s12" to="o16893" />
      <relation from="o16893" id="r1441" name="by" negation="false" src="d0_s12" to="o16894" />
      <relation from="o16893" id="r1442" name="with" negation="false" src="d0_s12" to="o16895" />
    </statement>
    <statement id="d0_s13">
      <text>pistil 4–5-carpellate;</text>
      <biological_entity id="o16896" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16897" name="pistil" name_original="pistil" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="4-5-carpellate" value_original="4-5-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 5-locular;</text>
      <biological_entity id="o16898" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o16899" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="5-locular" value_original="5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma truncate or capitate.</text>
      <biological_entity id="o16900" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o16901" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular, 5-valved, globose, fleshy, (surrounded by persistent, fleshy calyx).</text>
      <biological_entity id="o16902" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-valved" value_original="5-valved" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 20–80+, ovoid;</text>
      <biological_entity id="o16903" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s17" to="80" upper_restricted="false" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>testa smooth.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 11, 12, 13.</text>
      <biological_entity id="o16904" name="testa" name_original="testa" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o16905" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="11" value_original="11" />
        <character name="quantity" src="d0_s19" value="12" value_original="12" />
        <character name="quantity" src="d0_s19" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies (Hispaniola, Windward Islands), Central America, South America, e Asia, Pacific Islands (New Zealand), Australia (including Tasmania); mostly temperate or montane in tropical latitudes.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="West Indies (Windward Islands)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia (including Tasmania)" establishment_means="native" />
        <character name="distribution" value="mostly temperate or montane in tropical latitudes" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <other_name type="common_name">Wintergreen</other_name>
  <discussion>Species ca. 115 (6 in the flora).</discussion>
  <discussion>Gaultheria is characterized by its fruit and by the stamens having flattened filaments and awned anthers. All of the species are woody to varying degrees; the growth form varies from erect or spreading shrubs to procumbent or creeping and mat-forming. Eastern Asia and the Andes mountains of South America are centers of diversity for this genus.</discussion>
  <discussion>In North America, the fruits and leaves of Gaultheria are a food source for wildlife, and native peoples have medicinal and food uses for some species. Oil of wintergreen (methyl salicylate) is found in the leaves and fruits of some species.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Erect or creeping shrubs; inflorescences racemes</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Creeping or spreading shrubs or subshrubs; inflorescences solitary flowers or with 2-3 flowers per node</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 1.5-3.5 cm; peduncles and pedicles sparsely hairy; sepals glabrous; fruits white.</description>
      <determination>5 Gaultheria pyroloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 4-8+ cm; peduncles and pedicels densely glandular-hairy; sepals glandular-hairy (hairs reddish); fruits black-purple.</description>
      <determination>6 Gaultheria shallon</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants creeping, mat-forming, roots adventitious or fibrous; stems repent, usually densely strigose or hirtellous, sometimes glabrous</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants creeping or spreading, not mat-forming, adventitious roots absent; stems decumbent, branches ascending to erect, lanate, often glabrescent</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems densely strigose; leaf blades 0.3-1 cm; flowers 4-merous; anthers with 2 bifurcating awns; fruits white.</description>
      <determination>1 Gaultheria hispidula</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems usually hirtellous, sometimes glabrous; leaf blades 1-2.5 cm; flowers 5-merous; anthers without awns; fruits red.</description>
      <determination>2 Gaultheria humifusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades ovate to subcordate; bracteoles 1-3 pairs; petals 3-4 mm; filaments glabrous, base with rounded, auriclelike projections; anthers without awns; w North America.</description>
      <determination>3 Gaultheria ovatifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades obovate to oval or orbiculate; bracteoles absent; petals 8-10 mm; filaments lanate-tomentose, base slightly widened; anthers with 2 awns; e North America.</description>
      <determination>4 Gaultheria procumbens</determination>
    </key_statement>
  </key>
</bio:treatment>