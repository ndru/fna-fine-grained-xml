<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mitella</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="species">pentandra</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>56: plate 2933. 1829  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus mitella;species pentandra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065976</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pectiantia</taxon_name>
    <taxon_name authority="(Hooker) Rydberg" date="unknown" rank="species">pentandra</taxon_name>
    <taxon_hierarchy>genus Pectiantia;species pentandra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes stoloniferous.</text>
      <biological_entity id="o4302" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems 8–48 (–60) cm.</text>
      <biological_entity id="o4303" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="48" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="48" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole (0.9–) 1.5–8.5 (–14) cm, subglabrous or sparsely short-stipitate-glandular and, sometimes, sparsely long-stipitate-glandular, longer hairs spreading to retrorse, white, tan, or brown;</text>
      <biological_entity id="o4304" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4305" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="8.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="longer" id="o4306" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="retrorse" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate-cordate, ± as long as wide, (1–) 2.3–8.5 × (1–) 2.1–8 cm, margins shallowly (3-), 5-lobed, 7-lobed, or 9-lobed, doubly crenate-dentate, glabrous, apex of terminal lobe acute to obtuse, surfaces subglabrous or sparsely long-stipitate-glandular;</text>
      <biological_entity id="o4307" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4308" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-cordate" value_original="ovate-cordate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s3" to="2.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s3" to="2.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.1" from_unit="cm" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4309" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character modifier="as-long-as wide" name="quantity" src="d0_s3" value="[3" value_original="[3" />
        <character is_modifier="false" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="9-lobed" value_original="9-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="9-lobed" value_original="9-lobed" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s3" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4310" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4311" name="lobe" name_original="lobe" src="d0_s3" type="structure" />
      <biological_entity id="o4312" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <relation from="o4310" id="r355" name="part_of" negation="false" src="d0_s3" to="o4311" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves usually absent, sometimes 1, proximal or distal, sessile or petiolate, blade 0.2–3 × 0.2–2.2 cm.</text>
      <biological_entity id="o4313" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o4314" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character modifier="sometimes" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o4315" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3 (–4), remotely 6–25-flowered, 1–2 flowers per node, not secund, 8–48 (–60) cm, glabrous, subglabrous, or sparsely short-stipitate-glandular proximally, short-stipitate-glandular distally.</text>
      <biological_entity id="o4316" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="remotely" name="architecture" src="d0_s5" value="6-25-flowered" value_original="6-25-flowered" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o4317" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="48" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="60" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="48" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4318" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o4317" id="r356" name="per" negation="false" src="d0_s5" to="o4318" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1.5–5 (–8) mm, short-stipitate-glandular.</text>
      <biological_entity id="o4319" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium broadly saucer-shaped, (0.6–) 0.8–2 × (1.7–) 2–2.8 mm;</text>
      <biological_entity id="o4320" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4321" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_length" src="d0_s7" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_width" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals spreading or recurved, greenish yellow or greenish, triangular to broadly triangular, 0.6–1.1 × 0.7–1.2 mm;</text>
      <biological_entity id="o4322" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4323" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="broadly triangular" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s8" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals greenish, 5–11-lobed, 1.5–3 mm, lobes linear, lateral lobes spreading;</text>
      <biological_entity id="o4324" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4325" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="5-11-lobed" value_original="5-11-lobed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4326" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4327" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 5, alternate sepals;</text>
      <biological_entity id="o4328" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4329" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4330" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments white, 0.1–0.2 mm;</text>
      <biological_entity id="o4331" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4332" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.2–0.3 × 0.5–0.7 mm;</text>
      <biological_entity id="o4333" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4334" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s12" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary ± completely inferior;</text>
      <biological_entity id="o4335" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4336" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less completely" name="position" src="d0_s13" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles divergent, flattened, 0.1–0.2 mm;</text>
      <biological_entity id="o4337" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o4338" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 2-lobed.</text>
      <biological_entity id="o4339" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o4340" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds reddish-brown, 0.6–0.9 mm, pitted.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o4341" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4342" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist woods, stream banks, avalanche tracks, wet mountain meadows, shaded banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="avalanche tracks" />
        <character name="habitat" value="wet mountain meadows" />
        <character name="habitat" value="shaded banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Calif., Colo., Idaho, Mont., Nev., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Five-stamen mitrewort</other_name>
  
</bio:treatment>