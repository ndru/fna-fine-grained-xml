<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">360</other_info_on_meta>
    <other_info_on_meta type="mention_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">533</other_info_on_meta>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sarraceniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sarracenia</taxon_name>
    <taxon_name authority="Wherry" date="1929" rank="species">jonesii</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>19: 385, fig. p. 387. 1929,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sarraceniaceae;genus sarracenia;species jonesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092283</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sarracenia</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">rubra</taxon_name>
    <taxon_name authority="(Wherry) Wherry" date="unknown" rank="subspecies">jonesii</taxon_name>
    <taxon_hierarchy>genus Sarracenia;species rubra;subspecies jonesii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming dense clumps;</text>
      <biological_entity id="o4733" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4734" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4733" id="r397" name="forming" negation="false" src="d0_s0" to="o4734" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 0.5–1.5 cm diam.</text>
      <biological_entity id="o4735" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="diameter" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pitchers persistent, appearing with flowers, producing similar pitchers through summer, erect, (long-petiolate, basal 1/4–1/3 of tube solid, without open cavity, tapering distally to form hollow tube and distinctly bulging abaxially mostly in distal 1/4 of tube), green, usually reddish or purple-veined adaxially, rarely with an overall deep maroon-purple suffusion, without white areolae, 21–73 cm, firm, waxy, external surface glabrous, wings 0.2–1 (–1.5) cm wide;</text>
      <biological_entity id="o4736" name="pitcher" name_original="pitchers" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s2" value="purple-veined" value_original="purple-veined" />
        <character char_type="range_value" from="21" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="73" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="firm" value_original="firm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="ceraceous" value_original="waxy" />
      </biological_entity>
      <biological_entity id="o4737" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o4738" name="pitcher" name_original="pitchers" src="d0_s2" type="structure" />
      <biological_entity id="o4739" name="areola" name_original="areolae" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="external" id="o4740" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4741" name="wing" name_original="wings" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o4736" id="r398" name="appearing with" negation="false" src="d0_s2" to="o4737" />
      <relation from="o4736" id="r399" name="producing similar" negation="false" src="d0_s2" to="o4738" />
      <relation from="o4736" id="r400" modifier="with an overall deep maroon-purple suffusion" name="without" negation="false" src="d0_s2" to="o4739" />
    </statement>
    <statement id="d0_s3">
      <text>orifice oval, 1–4 cm diam., rim maroon, tightly revolute, usually with slight to distinct indentation distal to wing often forming conspicuous spout extending over wing;</text>
      <biological_entity id="o4742" name="orifice" name_original="orifice" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oval" value_original="oval" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4743" name="rim" name_original="rim" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="maroon" value_original="maroon" />
        <character is_modifier="false" modifier="tightly" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o4744" name="indentation" name_original="indentation" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="slight" value_original="slight" />
        <character is_modifier="true" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character constraint="to wing" constraintid="o4745" is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o4745" name="wing" name_original="wing" src="d0_s3" type="structure" />
      <biological_entity id="o4746" name="spout" name_original="spout" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o4747" name="wing" name_original="wing" src="d0_s3" type="structure" />
      <relation from="o4743" id="r401" modifier="usually" name="with" negation="false" src="d0_s3" to="o4744" />
      <relation from="o4745" id="r402" name="forming" negation="false" src="d0_s3" to="o4746" />
      <relation from="o4743" id="r403" name="extending over" negation="false" src="d0_s3" to="o4747" />
    </statement>
    <statement id="d0_s4">
      <text>hood recurved adaxially, held well beyond and covering orifice, red-purple, veins on adaxial and abaxial surfaces distinctive, red-purple throughout, without white areolae, broadly ovate, undulate, 2.4–6.5 × 2.4–5.4 cm, longer than wide, base cordate, neck not constricted, 0.5–1 cm, apiculum 1–4 mm, adaxial surface glabrate or with hairs to 0.5 mm.</text>
      <biological_entity id="o4748" name="hood" name_original="hood" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="throughout" name="coloration_or_density" notes="" src="d0_s4" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s4" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="width" src="d0_s4" to="5.4" to_unit="cm" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
      <biological_entity id="o4749" name="orifice" name_original="orifice" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s4" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o4750" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s4" value="covering" value_original="covering" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s4" value="red-purple" value_original="red-purple" />
      </biological_entity>
      <biological_entity constraint="and adaxial abaxial" id="o4751" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="distinctive" value_original="distinctive" />
      </biological_entity>
      <biological_entity id="o4752" name="areola" name_original="areolae" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o4753" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o4754" name="neck" name_original="neck" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s4" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4755" name="apiculum" name_original="apiculum" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4756" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o4757" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o4748" id="r404" name="held" negation="false" src="d0_s4" to="o4749" />
      <relation from="o4748" id="r405" name="held" negation="false" src="d0_s4" to="o4750" />
      <relation from="o4748" id="r406" name="on" negation="false" src="d0_s4" to="o4751" />
      <relation from="o4748" id="r407" name="without" negation="false" src="d0_s4" to="o4752" />
      <relation from="o4756" id="r408" name="with" negation="false" src="d0_s4" to="o4757" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllodia absent.</text>
      <biological_entity id="o4758" name="phyllodium" name_original="phyllodia" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scapes 1–2, 32–70 cm, rarely exceeding tallest pitchers;</text>
      <biological_entity id="o4759" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
        <character char_type="range_value" from="32" from_unit="cm" name="some_measurement" src="d0_s6" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="tallest" id="o4760" name="pitcher" name_original="pitchers" src="d0_s6" type="structure" />
      <relation from="o4759" id="r409" modifier="rarely" name="exceeding" negation="false" src="d0_s6" to="o4760" />
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–2 cm.</text>
      <biological_entity id="o4761" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers moderately fragrant;</text>
      <biological_entity id="o4762" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately" name="odor" src="d0_s8" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals maroon, 2.5–3.5 × 1.5–2 cm;</text>
      <biological_entity id="o4763" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s9" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals maroon, distal portion obovate, 2.5–4 × 1.2–2.8 cm, margins erose;</text>
      <biological_entity id="o4764" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4765" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s10" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s10" to="2.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4766" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style disc green, 2.5–4 cm diam.</text>
      <biological_entity constraint="style" id="o4767" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s11" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 0.6–1.2 cm diam.</text>
      <biological_entity id="o4768" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="diameter" src="d0_s12" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.2–1.5 mm. 2n = 26.</text>
      <biological_entity id="o4769" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4770" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountain seepage bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountain" />
        <character name="habitat" value="seepage bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Mountain sweet pitcher plant</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sarracenia jonesii occurs in seepage bogs in the low mountains in southwestern North Carolina (Buncombe, Henderson, and Transylvania counties) and in seeps along gentle waterfalls on the adjacent South Carolina Blue Ridge escarpment (Greenville and Pickens counties). Within its native range, populations have been introduced and naturalized. It can form clumps to 0.5 m across in meadowlike seepage bogs. It is rare and vulnerable. Most of its prime locations have disappeared since 1950 due to development. It is federally listed as endangered and worthy of managed protection. Anthocyanin-free plants, with yellowish-green flowers and pitchers, are known in the wild. Plants from some locations can be vegetatively similar to other members of the S. rubra complex from the fall line of South Carolina or the Gulf Coast, or even some plants of S. alata hybrids west of Mobile Bay, Alabama, with bulges in the distal portions of their tubes. Always check the largest, well-grown pitchers for traits, and correlate with floral characters. Sarracenia jonesii is readily distinguished from S. rubra by its longer, solid petioles and scapes about as long as the pitchers.</discussion>
  <discussion>Sarracenia jonesii is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>