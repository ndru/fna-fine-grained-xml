<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch ex Borkhausen" date="unknown" rank="family">primulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">androsace</taxon_name>
    <taxon_name authority="Retzius" date="1781" rank="species">filiformis</taxon_name>
    <place_of_publication>
      <publication_title>Observ. Bot.</publication_title>
      <place_in_publication>2: 10. 1781  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family primulaceae;genus androsace;species filiformis</taxon_hierarchy>
    <other_info_on_name type="fna_id">200016907</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Androsace</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">capillaris</taxon_name>
    <taxon_hierarchy>genus Androsace;species capillaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, slender, not mat-forming.</text>
      <biological_entity id="o25831" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves in single rosette;</text>
      <biological_entity id="o25832" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o25833" name="rosette" name_original="rosette" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <relation from="o25832" id="r2162" name="in" negation="false" src="d0_s1" to="o25833" />
    </statement>
    <statement id="d0_s2">
      <text>petiole present;</text>
      <biological_entity id="o25834" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to deltate, 5–25 × 2–6 mm, base abruptly narrowing to winged petiole, margins without hairs, surfaces glabrous or slightly glandular, glands not stipitate.</text>
      <biological_entity id="o25835" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="deltate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25836" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abruptly" name="width" src="d0_s3" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o25837" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o25838" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o25839" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o25840" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o25841" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="stipitate" value_original="stipitate" />
      </biological_entity>
      <relation from="o25838" id="r2163" name="without" negation="false" src="d0_s3" to="o25839" />
    </statement>
    <statement id="d0_s4">
      <text>Scapes (often multiple), 5–25, 3–12 cm, glabrescent.</text>
      <biological_entity id="o25842" name="scape" name_original="scapes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="25" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 5–20-flowered;</text>
      <biological_entity id="o25843" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-20-flowered" value_original="5-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>involucral-bracts lanceolate, relatively narrow.</text>
      <biological_entity id="o25844" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="relatively" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels erect to arcuate, filiform, unequal, 1–6 cm.</text>
      <biological_entity id="o25845" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="course_or_shape" src="d0_s7" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx campanulate to hemispheric, without prominent ridges, ca. 2 mm, lobes erect, triangular, apex acute;</text>
      <biological_entity id="o25846" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25847" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s8" to="hemispheric" />
        <character name="some_measurement" notes="" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25848" name="ridge" name_original="ridges" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o25849" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o25850" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o25847" id="r2164" name="without" negation="false" src="d0_s8" to="o25848" />
    </statement>
    <statement id="d0_s9">
      <text>corolla-tube shorter than calyx, limb ca. 2 mm diam.</text>
      <biological_entity id="o25851" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25852" name="corolla-tube" name_original="corolla-tube" src="d0_s9" type="structure">
        <character constraint="than calyx" constraintid="o25853" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25853" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o25854" name="limb" name_original="limb" src="d0_s9" type="structure">
        <character name="diameter" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules slightly shorter than calyx, 2–4 mm. 2n = 20 (Asia).</text>
      <biological_entity id="o25855" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character constraint="than calyx" constraintid="o25856" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="slightly shorter" value_original="slightly shorter" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25856" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o25857" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early-mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Mont., Oreg., Utah, Wash., Wyo.; Europe; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Filiform rock jasmine</other_name>
  <discussion>Androsace filiformis grows in wetlands and is easily identified by the tiny flowers and delicate, filiform inflorescence that give the plants a graceful appearance. No other North American Androsace occurs in wetlands. Androsace filiformis occurs widely across Europe and Asia (including the Russian Far East) and in the western continental United States, with a notable gap in Alaska and Canada.</discussion>
  
</bio:treatment>