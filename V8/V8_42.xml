<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="A. Heller" date="1900" rank="species">binominatum</taxon_name>
    <place_of_publication>
      <publication_title>Cat. N. Amer. Pl., ed.</publication_title>
      <place_in_publication>2, 5. 1900  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species binominatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250065828</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">montanum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>210. 1898,</place_in_publication>
      <other_info_on_pub>not Philippi 1859</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Ribes;species montanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(A. Heller) Coville &amp; Britton" date="unknown" rank="species">binominata</taxon_name>
    <taxon_hierarchy>genus Grossularia;species binominata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–1 m.</text>
      <biological_entity id="o5888" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to prostrate, pubescent;</text>
      <biological_entity id="o5889" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="prostrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 3, 5–20 mm;</text>
      <biological_entity id="o5890" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5891" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o5890" id="r491" name="at" negation="false" src="d0_s2" to="o5891" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o5892" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o5893" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o5892" id="r492" name="on" negation="false" src="d0_s3" to="o5893" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 2–5 cm, puberulent to villous and stipitate-glandular;</text>
      <biological_entity id="o5894" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5895" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s4" to="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade suborbiculate, 3–5-lobed, deeply cleft, 2–5 cm, base cordate, surfaces pubescent, not stipitate-glandular, lobes broadly cuneate, sides mostly straight, margins dentate-crenate, apex rounded.</text>
      <biological_entity id="o5896" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o5897" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5898" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o5899" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o5900" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o5901" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5902" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate-crenate" value_original="dentate-crenate" />
      </biological_entity>
      <biological_entity id="o5903" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, solitary flowers or 2–3-flowered racemes, 1–4 cm (much shorter than leaves), axis hairy, flowers evenly spaced.</text>
      <biological_entity id="o5904" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o5905" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5906" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3-flowered" value_original="2-3-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5907" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5908" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 1–2 mm, bristly;</text>
      <biological_entity id="o5909" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate-ovate, 1.5–3 mm, puberulent.</text>
      <biological_entity id="o5910" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium green, broadly campanulate, 2–3 (–4) mm, pubescent;</text>
      <biological_entity id="o5911" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5912" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, reflexed, greenish white to green with red margins, lanceolate, 4–6 mm;</text>
      <biological_entity id="o5913" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5914" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" constraint="with margins" constraintid="o5915" from="greenish white" name="coloration" src="d0_s10" to="green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5915" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals nearly connivent, erect, white to pink, oblong, flat or shallowly concave abaxially, 2–3 mm;</text>
      <biological_entity id="o5916" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5917" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="shallowly; abaxially" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o5918" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o5919" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens longer than petals;</text>
      <biological_entity id="o5920" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o5921" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="than petals" constraintid="o5922" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o5922" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments slightly expanded at base, 2–4 mm, glabrous;</text>
      <biological_entity id="o5923" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o5924" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="at base" constraintid="o5925" is_modifier="false" modifier="slightly" name="size" src="d0_s14" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5925" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers reddish-brown, oval, 0.2–0.5 mm, apex blunt;</text>
      <biological_entity id="o5926" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o5927" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oval" value_original="oval" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5928" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary densely bristly and glandular;</text>
      <biological_entity id="o5929" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o5930" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s16" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate 1/2 their lengths, 2–4 mm, glabrous.</text>
      <biological_entity id="o5931" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o5932" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s17" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries palatability not known, yellow-green, ovoid, 8–10 mm, prickles yellow, nonglandular, stout, developing into spines, hairs glandular or eglandular.</text>
      <biological_entity id="o5933" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s18" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5934" name="prickle" name_original="prickles" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s18" value="stout" value_original="stout" />
        <character constraint="into spines" constraintid="o5935" is_modifier="false" name="development" src="d0_s18" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o5935" name="spine" name_original="spines" src="d0_s18" type="structure" />
      <biological_entity id="o5936" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane and subalpine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane and subalpine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Trailing gooseberry</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ribes binominatum occurs at relatively high elevations in the Coast Ranges of northern California and the Klamath Mountains of central and southern Oregon.</discussion>
  
</bio:treatment>