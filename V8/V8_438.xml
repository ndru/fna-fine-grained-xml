<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="S. Watson" date="1885" rank="species">stelliforme</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>20: 365. 1885,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species stelliforme</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092133</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="Raymond-Hamet" date="unknown" rank="species">topsentii</taxon_name>
    <taxon_hierarchy>genus Sedum;species topsentii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, somewhat tufted, glabrous.</text>
      <biological_entity id="o21828" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems rootstocks and erect shoots, branched proximally, sometimes bearing rosettes.</text>
      <biological_entity constraint="stems" id="o21829" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="stems" id="o21830" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21831" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o21829" id="r1828" modifier="sometimes" name="bearing" negation="false" src="d0_s1" to="o21831" />
      <relation from="o21830" id="r1829" modifier="sometimes" name="bearing" negation="false" src="d0_s1" to="o21831" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, spreading to ascending, sessile;</text>
      <biological_entity id="o21832" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade blue-green, not glaucous, linear-oblanceolate, terete to subterete, 4–9 (–15) × 1–2 mm, base broadly spurred, not scarious, apex obtuse.</text>
      <biological_entity id="o21833" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="blue-green" value_original="blue-green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s3" to="subterete" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21834" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o21835" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots erect or ascending, simple or branched, 1–4 (–9) cm, (with small glistening patches);</text>
      <biological_entity id="o21836" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades linear-oblanceolate, base truncate or spurred;</text>
      <biological_entity id="o21837" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblanceolate" value_original="linear-oblanceolate" />
      </biological_entity>
      <biological_entity id="o21838" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o21839" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences compact, 3-parted cymes, (5–) 10–25-flowered, monochasially branched;</text>
      <biological_entity id="o21840" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o21841" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="3-parted" value_original="3-parted" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="(5-)10-25-flowered" value_original="(5-)10-25-flowered" />
        <character is_modifier="false" modifier="monochasially" name="architecture" src="d0_s7" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches erect to spreading or recurved, rarely forked;</text>
      <biological_entity id="o21842" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading or recurved" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts suboblong, base truncate or spurred.</text>
      <biological_entity id="o21843" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="suboblong" value_original="suboblong" />
      </biological_entity>
      <biological_entity id="o21844" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent.</text>
      <biological_entity id="o21845" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5–6-merous;</text>
      <biological_entity id="o21846" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-6-merous" value_original="5-6-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals erect to spreading, distinct, blue-green, linear to narrowly oblong, unequal, ca. 2–4 (–6) × 0.7–1.5 mm, apex obtuse;</text>
      <biological_entity id="o21847" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="blue-green" value_original="blue-green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="narrowly oblong" />
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21848" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spreading, distinct, white tinged with purple, oblong, somewhat carinate, 4–7 mm, apex acute or broadly mucronate;</text>
      <biological_entity id="o21849" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white tinged with purple" value_original="white tinged with purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s13" value="carinate" value_original="carinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21850" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments white;</text>
      <biological_entity id="o21851" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers purplish;</text>
      <biological_entity id="o21852" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar scales dark-pink, spatulate.</text>
      <biological_entity constraint="nectar" id="o21853" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-pink" value_original="dark-pink" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spatulate" value_original="spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels stellately spreading in fruit, distinct, stramineous.</text>
      <biological_entity id="o21855" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 19, 22, 24, 44, 52.</text>
      <biological_entity id="o21854" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in fruit" constraintid="o21855" is_modifier="false" modifier="stellately" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21856" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="19" value_original="19" />
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
        <character name="quantity" src="d0_s18" value="24" value_original="24" />
        <character name="quantity" src="d0_s18" value="44" value_original="44" />
        <character name="quantity" src="d0_s18" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassland, moist areas, moist cliffs in conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist areas" modifier="grassland" />
        <character name="habitat" value="moist cliffs" constraint="in conifer forests" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex.; Mexico (Chihuahua, Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <discussion>Sedum stelliforme has glistening patches on the flowering branches and sepals. It is found in southern Colorado, Graham, Greenlee, and Apache counties in Arizona, the Zuni Mountains and Fort Wingate in New Mexico, and on the Mexican Plateau. More information is needed to determine whether S. topsentii should be separated from S. stelliforme.</discussion>
  
</bio:treatment>