<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="(Coville) Fedde" date="1910" rank="species">tularense</taxon_name>
    <place_of_publication>
      <publication_title>Just’s Bot. Jahresber.</publication_title>
      <place_in_publication>36(2): 519. 1910 (as tularensis)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species tularense</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065827</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="Coville" date="unknown" rank="species">tularensis</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 218. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grossularia;species tularensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–0.5 m.</text>
      <biological_entity id="o5997" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.5" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, villous-pubescent, stipitate-glandular;</text>
      <biological_entity id="o5998" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 3, 3–8 (–10) mm;</text>
      <biological_entity id="o5999" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6000" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
      <relation from="o5999" id="r494" name="at" negation="false" src="d0_s2" to="o6000" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes scattered.</text>
      <biological_entity id="o6001" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o6002" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o6001" id="r495" name="on" negation="false" src="d0_s3" to="o6002" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 2–2.5 cm, puberulent;</text>
      <biological_entity id="o6003" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6004" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade roundish, irregularly 3 (–5) -lobed, cleft 1/2+ to midrib, 2–5 cm, base broadly truncate, surfaces hairy, stipitate-glandular, lobes broadly cuneate, sides mostly straight or concave, margins very irregularly blunt-toothed, apex rounded.</text>
      <biological_entity id="o6005" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6006" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="roundish" value_original="roundish" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="3(-5)-lobed" value_original="3(-5)-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o6007" from="1/2" name="quantity" src="d0_s5" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6007" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o6008" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o6009" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6010" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6011" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity id="o6012" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very irregularly" name="shape" src="d0_s5" value="blunt-toothed" value_original="blunt-toothed" />
      </biological_entity>
      <biological_entity id="o6013" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, solitary flowers or 2–3-flowered racemes, 1–1.5 cm, axis villous, stipitate-glandular, flowers evenly spaced.</text>
      <biological_entity id="o6014" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o6015" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6016" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2-3-flowered" value_original="2-3-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6017" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6018" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 2–4 mm, villous, stipitate-glandular;</text>
      <biological_entity id="o6019" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate-ovate, 2–3 mm, villous, stipitate-glandular.</text>
      <biological_entity id="o6020" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium green, campanulate, 2–4 mm (as wide as long), sparsely pubescent to villous;</text>
      <biological_entity id="o6021" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6022" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="sparsely pubescent" name="pubescence" src="d0_s9" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, reflexed, green to white, narrowly oblong, abaxially shallowly concave, 6 mm;</text>
      <biological_entity id="o6023" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6024" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="abaxially shallowly" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals separated, erect, white, oblong-truncate, somewhat inrolled, 2–3 mm;</text>
      <biological_entity id="o6025" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6026" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="separated" value_original="separated" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="oblong-truncate" value_original="oblong-truncate" />
        <character is_modifier="false" modifier="somewhat" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o6027" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o6028" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens shorter than to as long as petals;</text>
      <biological_entity id="o6029" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6030" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="than to as-long-as petals" constraintid="o6031" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6031" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 2–2.5 mm, glabrous;</text>
      <biological_entity id="o6032" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o6033" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers white, narrowly oblong, 1–1.8 mm, apex blunt;</text>
      <biological_entity id="o6034" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o6035" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6036" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary densely villous with some gland-tipped bristles;</text>
      <biological_entity id="o6037" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o6038" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character constraint="with bristles" constraintid="o6039" is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6039" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate 1/2 their lengths, 4–6 mm, glabrous.</text>
      <biological_entity id="o6040" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o6041" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s17" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries palatability not known, light yellow, globose, 8–10 mm, hairs nonglandular, bristles glandular, developing into spines.</text>
      <biological_entity id="o6042" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s18" value="light yellow" value_original="light yellow" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6043" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o6044" name="bristle" name_original="bristles" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s18" value="glandular" value_original="glandular" />
        <character constraint="into spines" constraintid="o6045" is_modifier="false" name="development" src="d0_s18" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o6045" name="spine" name_original="spines" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Yellow pine and red fir forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="red fir forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Sequoia gooseberry</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ribes tularense is known from ten populations, all in Tulare County.</discussion>
  
</bio:treatment>