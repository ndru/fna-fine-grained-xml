<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="treatment_page">96</other_info_on_meta>
    <other_info_on_meta type="illustration_page">92</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">heuchera</taxon_name>
    <taxon_name authority="R. Brown in J. Franklin et al." date="1823" rank="species">richardsonii</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin et al., Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>766, plate 29. 1823,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus heuchera;species richardsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416653</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_name authority="Rosendahl, Butters &amp; Lakela" date="unknown" rank="variety">affinis</taxon_name>
    <taxon_hierarchy>genus Heuchera;species richardsonii;variety affinis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_name authority="Rosendahl, Butters &amp; Lakela" date="unknown" rank="variety">grayana</taxon_name>
    <taxon_hierarchy>genus Heuchera;species richardsonii;variety grayana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heuchera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">richardsonii</taxon_name>
    <taxon_name authority="Rosendahl, Butters &amp; Lakela" date="unknown" rank="variety">hispidior</taxon_name>
    <taxon_hierarchy>genus Heuchera;species richardsonii;variety hispidior;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs acaulescent;</text>
      <biological_entity id="o21735" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched.</text>
      <biological_entity id="o21736" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems (7–) 20–95 cm, densely long-stipitate-glandular.</text>
      <biological_entity id="o21737" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="95" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole densely or sparsely long or short-stipitate-glandular;</text>
      <biological_entity id="o21738" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21739" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate or cordate, deeply 5–7-lobed, 2.5–10 cm, base cordate or nearly truncate, lobes rounded, margins dentate, apex acute, surfaces long-stipitate-glandular abaxially, glabrous or long-stipitate-glandular adaxially.</text>
      <biological_entity id="o21740" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21741" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21742" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o21743" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o21744" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o21745" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21746" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences dense to diffuse.</text>
      <biological_entity id="o21747" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="dense" name="density" src="d0_s5" to="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium strongly bilaterally symmetric, free 2–7 mm, green, campanulate, abruptly inflated distal to adnation to ovary, 5–14 mm, short-stipitate-glandular;</text>
      <biological_entity id="o21748" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o21749" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character constraint="to adnation" constraintid="o21750" is_modifier="false" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o21750" name="adnation" name_original="adnation" src="d0_s6" type="structure" />
      <biological_entity id="o21751" name="ovary" name_original="ovary" src="d0_s6" type="structure" />
      <relation from="o21750" id="r1823" name="to" negation="false" src="d0_s6" to="o21751" />
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, green-tipped, equal, 1.3–4.2 mm, apex rounded (sinuses wider than petals);</text>
      <biological_entity id="o21752" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o21753" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green-tipped" value_original="green-tipped" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="4.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21754" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals erect, green or greenish white, rarely pink, narrowly spatulate, unlobed, 1.3–4 mm, margins finely dentate or coarsely fimbriate;</text>
      <biological_entity id="o21755" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o21756" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21757" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s8" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 1.5 mm included to 4 mm exserted;</text>
      <biological_entity id="o21758" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21759" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles from 0.6 mm included to 0.3 mm exserted, 4–6 mm, to 0.1 mm diam.</text>
      <biological_entity id="o21760" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21761" name="style" name_original="styles" src="d0_s10" type="structure">
        <character constraint="from 0.6 mm" is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s10" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, 7–14.5 mm, beaks divergent, not papillose.</text>
      <biological_entity id="o21762" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="14.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21763" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark or very dark-brown, ellipsoid, 0.6–0.9 mm. 2n = 14, 28.</text>
      <biological_entity id="o21764" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark" value_original="dark" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21765" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="14" value_original="14" />
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist or dry, low or upland prairies, basic rock outcroppings and bluffs, sandy, dry woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low" modifier="moist or dry" />
        <character name="habitat" value="upland prairies" />
        <character name="habitat" value="basic rock outcroppings" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="dry woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask.; Colo., Ill., Ind., Iowa, Kans., Mich., Minn., Mo., Mont., Nebr., N.Dak., Okla., S.Dak., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Richardson’s alum-root</other_name>
  <discussion>Heuchera richardsonii intergrades with H. americana where their ranges overlap in Arkansas, Illinois, Indiana, Michigan, Missouri, and Oklahoma; the intergrading form is recognized here as H. americana var. hirsuticaulis.</discussion>
  <discussion>Heuchera hispida (H. americana var. hispida here; see thereunder) was confused with H. richardsonii for almost a hundred years, until C. O. Rosendahl et al. (1933) pointed out that the plants from the Midwest then passing as H. hispida Pursh were distinct from Pursh’s species and were H. richardsonii.</discussion>
  <discussion>The Blackfoot, Cree, Lakota, and Woodlands Indians used decoctions and infusions of the roots of Heuchera richardsonii for diarrhea and as an eyewash, and the Lakota applied a poultice of powdered roots to sores (D. E. Moerman 1998).</discussion>
  
</bio:treatment>