<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">micranthes</taxon_name>
    <taxon_name authority="(Zhmylev) Brouillet &amp; Gornall" date="2007" rank="species">razshivinii</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 1021. 2007,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus micranthes;species razshivinii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065886</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Zhmylev" date="unknown" rank="species">razshivinii</taxon_name>
    <place_of_publication>
      <publication_title>Byull. Moskovk. Obshch. Isp. Prir., Otd. Biol., n. s.</publication_title>
      <place_in_publication>95(3): 87. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species razshivinii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or in clusters, rhizomatous.</text>
      <biological_entity id="o9728" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="in clusters" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="in clusters" value_original="in clusters" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o9729" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole ± distinct to indistinct, flattened, (0.3–) 0.5–3 cm;</text>
      <biological_entity id="o9730" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate to narrowly oblanceolate or narrowly spatulate, sometimes ± narrowly obovate, (0.5–) 0.9–3.5 cm, ± fleshy, base ± attenuate to cuneate, margins (3–) 5–7 (–9) -toothed in distal 1/2 (teeth 0.5 mm), sparsely ciliate, surfaces glabrous.</text>
      <biological_entity id="o9731" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly oblanceolate or narrowly spatulate" />
        <character is_modifier="false" modifier="sometimes more or less narrowly" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="0.9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s3" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o9732" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="less attenuate" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o9733" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in distal 1/2" constraintid="o9734" is_modifier="false" name="shape" src="d0_s3" value="(3-)5-7(-9)-toothed" value_original="(3-)5-7(-9)-toothed" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" notes="" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9734" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <biological_entity id="o9735" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences (2–) 4–16-flowered, open, racemiform, sometimes paniculiform thyrses, sometimes solitary flowers, 3–15.5 (–17 in fruit) cm, glabrous;</text>
      <biological_entity id="o9736" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="(2-)4-16-flowered" value_original="(2-)4-16-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <biological_entity id="o9737" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>(bracts reduced).</text>
      <biological_entity id="o9738" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="15.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals reflexed, oblong or lanceolate to ovate;</text>
      <biological_entity id="o9739" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o9740" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white to cream, often purplish, not spotted, linear to ± narrowly elliptic, not clawed, 2–4 mm, usually longer to sometimes shorter than sepals;</text>
      <biological_entity id="o9741" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o9742" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="cream" />
        <character constraint="than sepals" constraintid="o9743" is_modifier="false" name="size_or_length" src="d0_s7" value="longer to sometimes" value_original="longer to sometimes" />
        <character is_modifier="false" modifier="often" name="coloration" notes="" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="spotted" value_original="spotted" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="linear to more" value_original="linear to more" />
        <character is_modifier="false" modifier="more or less narrowly" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="than sepals" constraintid="o9744" is_modifier="false" name="size_or_length" src="d0_s7" value="longer to sometimes" value_original="longer to sometimes" />
      </biological_entity>
      <biological_entity id="o9743" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9744" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments linear, flattened;</text>
      <biological_entity id="o9745" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9746" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistils connate 1/2+ their lengths, (conic);</text>
      <biological_entity id="o9747" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9748" name="pistil" name_original="pistils" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s9" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary ± 1/2 inferior.</text>
      <biological_entity id="o9749" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9750" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s10" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules dark purple-black, valvate.</text>
      <biological_entity id="o9751" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple-black" value_original="dark purple-black" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra, stream banks, alpine rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="rocky slopes" modifier="alpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Micranthes razshivinii is found in eastern Alaska (eastern Brooks Range, from Atigun Pass eastward and from Mount McKinley National Park eastward) into Yukon and in the Mackenzie and Richardson mountains in the Northwest Territories. Its leaves are generally narrower than those of M. calycina and the teeth are less numerous. The glabrous inflorescence branches of M. razshivinii readily distinguish it from M. calycina, which has hairy branches. The ranges of the two species may overlap or at least abut in the Atigun Pass area of the Brooks Range, in the Mount McKinley National Park area, and possibly in the Saint Elias Mountains. Hybrids would be very difficult to distinguish from the parents but may occur. They have not been observed in the material examined. Plants of this species sometimes have been misidentified as M. davurica (Willdenow) Small, a species restricted to eastern Siberia.</discussion>
  
</bio:treatment>