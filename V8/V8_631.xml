<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">myrsinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lysimachia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">quadrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 147. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrsinaceae;genus lysimachia;species quadrifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092259</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_hierarchy>genus Lysimachia;species hirsuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">quadrifolia</taxon_name>
    <taxon_name authority="Peck" date="unknown" rank="variety">variegata</taxon_name>
    <taxon_hierarchy>genus Lysimachia;species quadrifolia;variety variegata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, usually simple, 3–10 dm, usually sparsely pubescent, at least at nodes (or glabrous);</text>
      <biological_entity id="o2632" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2633" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <relation from="o2632" id="r209" modifier="at-least" name="at" negation="false" src="d0_s0" to="o2633" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes slender to somewhat thickened;</text>
      <biological_entity id="o2634" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender to somewhat" value_original="slender to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o2635" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves whorled;</text>
      <biological_entity id="o2636" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent or 0.1–0.3 cm, eciliate;</text>
      <biological_entity id="o2637" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s4" value="0.1-0.3 cm" value_original="0.1-0.3 cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="eciliate" value_original="eciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic to lanceolate or ovate, 3–12 × 0.8–3.5 (–4.5) cm, base cuneate or rounded, slightly decurrent, margins entire, plane, glabrous or sometimes sparsely pubescent, apex acuminate or acute (rarely obtuse), surfaces densely to sparsely punctate, pubescent at least along abaxial veins and margins;</text>
      <biological_entity id="o2638" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="lanceolate or ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2639" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o2640" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2641" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2643" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o2644" name="margin" name_original="margins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate-arcuate.</text>
      <biological_entity id="o2642" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely to sparsely" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
        <character constraint="along margins" constraintid="o2644" is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="course_or_shape" src="d0_s6" value="pinnate-arcuate" value_original="pinnate-arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary in leaves (distalmost axils sometimes without flowers), solitary flowers.</text>
      <biological_entity id="o2645" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character constraint="in leaves" constraintid="o2646" is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2646" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o2647" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1.5–3 cm, sparsely pubescent.</text>
      <biological_entity id="o2648" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, calyx streaked with dark resin canals, 3–6 mm, slightly stipitate-glandular, lobes lanceolate, margins thin;</text>
      <biological_entity id="o2649" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2650" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o2651" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character constraint="with resin canals" constraintid="o2652" is_modifier="false" name="coloration" src="d0_s9" value="streaked" value_original="streaked" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="resin" id="o2652" name="canal" name_original="canals" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o2653" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o2654" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, corolla yellow with reddish base and, sometimes, margins, streaked with black or maroon resin canals, rotate, 5–8 mm, lobes with margins entire, apex acute to rounded, stipitate-glandular adaxially;</text>
      <biological_entity id="o2655" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2656" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o2657" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="with base" constraintid="o2658" is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o2658" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o2659" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character constraint="with resin canals" constraintid="o2660" is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="resin" id="o2660" name="canal" name_original="canals" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity id="o2661" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o2662" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2663" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o2661" id="r210" name="with" negation="false" src="d0_s10" to="o2662" />
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ca. 1.7 mm, shorter than corolla;</text>
      <biological_entity id="o2664" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2665" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.7" value_original="1.7" />
        <character constraint="than corolla" constraintid="o2666" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o2666" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>staminodes absent.</text>
      <biological_entity id="o2667" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2668" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 3–3.5 mm, sometimes dark-punctate, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 84.</text>
      <biological_entity id="o2669" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_relief" src="d0_s13" value="dark-punctate" value_original="dark-punctate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2670" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to mesic hardwood forests, lowlands, fens, moist clearings, roadsides, and fields, rocky thickets and slopes, seashores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic hardwood forests" />
        <character name="habitat" value="lowlands" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="moist clearings" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="rocky thickets" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="dry to mesic hardwood forests" />
        <character name="habitat" value="seashores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Ala., Conn., Del., D.C., Ga., Ill., Ind., Ky., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Whorled loosestrife</other_name>
  <other_name type="common_name">cross-wort</other_name>
  <other_name type="common_name">lysimaque à quatre feuilles</other_name>
  <discussion>A hybrid (known only from one population in Washington County, North Carolina) of Lysimachia quadrifolia with L. loomisii has been called L. ×radfordii H. E. Ahles.</discussion>
  
</bio:treatment>