<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="treatment_page">338</other_info_on_meta>
    <other_info_on_meta type="illustration_page">335</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">diapensiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">diapensia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">lapponica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 141. 1753  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family diapensiaceae;genus diapensia;species lapponica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220004003</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming rounded tussocks, 3–8 cm;</text>
      <biological_entity id="o7381" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7382" name="tussock" name_original="tussocks" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o7381" id="r583" name="forming" negation="false" src="d0_s0" to="o7382" />
    </statement>
    <statement id="d0_s1">
      <text>branches procumbent or decumbent to erect, not adventitiously rooted, proximal portions of stems densely covered by persistent leaf remnants.</text>
      <biological_entity id="o7383" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character name="growth_form" src="d0_s1" value="decumbent to erect" value_original="decumbent to erect" />
        <character is_modifier="false" modifier="not adventitiously" name="architecture" src="d0_s1" value="rooted" value_original="rooted" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7384" name="portion" name_original="portions" src="d0_s1" type="structure" />
      <biological_entity id="o7385" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="leaf" id="o7386" name="remnant" name_original="remnants" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o7384" id="r584" name="part_of" negation="false" src="d0_s1" to="o7385" />
      <relation from="o7384" id="r585" modifier="densely" name="covered by" negation="false" src="d0_s1" to="o7386" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves 7–15 mm;</text>
      <biological_entity id="o7387" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong-oblanceolate to narrowly spatulate, 1.3–2.3 mm wide, margins narrowly revolute, with narrow hyaline flange proximally.</text>
      <biological_entity id="o7388" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-oblanceolate" name="shape" src="d0_s3" to="narrowly spatulate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s3" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7389" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o7390" name="flange" name_original="flange" src="d0_s3" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o7389" id="r586" name="with" negation="false" src="d0_s3" to="o7390" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 5–20 mm, elongating to 40–50 mm.</text>
      <biological_entity id="o7391" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="length" src="d0_s4" value="elongating" value_original="elongating" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 6–7 mm;</text>
      <biological_entity id="o7392" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o7393" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla 7–10 mm, lobes usually white, sometimes cream, light pink, or rose.</text>
      <biological_entity id="o7394" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7395" name="corolla" name_original="corolla" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7396" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light pink" value_original="light pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light pink" value_original="light pink" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="rose" value_original="rose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules 3–4 (–6) mm diam. 2n = 12.</text>
      <biological_entity id="o7397" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7398" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun(-Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bare, rocky alpine summits, gravelly balds, cliff faces, rocky summits, ridges, slopes, fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky alpine summits" modifier="bare" />
        <character name="habitat" value="gravelly balds" />
        <character name="habitat" value="cliff faces" />
        <character name="habitat" value="rocky summits" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="fellfields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(10-)200-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Man., Nfld. and Labr., N.S., Nunavut, Que.; Maine, N.H., N.Y., Vt.; Europe (w Russian arctic and subarctic, Scandinavia, Scotland); Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Europe (w Russian arctic and subarctic)" establishment_means="native" />
        <character name="distribution" value="Europe (Scandinavia)" establishment_means="native" />
        <character name="distribution" value="Europe (Scotland)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Lapland pincushion plant</other_name>
  <other_name type="common_name">diapensie de Laponie</other_name>
  <discussion>In New England and Newfoundland, flowering phenology of Diapensia lapponica is bimodal (R. T. Day and P. J. Scott 1981). In some populations, one group of plants flowers in May through June, and another group flowers in a nonoverlapping period from late June through late August. The genetic basis for this has not been determined (M. M. Grandtner 1978).</discussion>
  <references>
    <reference>Day, R. T. and P. J. Scott. 1981. Autecological aspects of Diapensia lapponica L. in Newfoundland. Rhodora 83: 101–109.</reference>
    <reference>Day, R. T. and P. J. Scott. 1984. The biology of Diapensia lapponica in Newfoundland. Canad. Field-Naturalist 98: 425–439.</reference>
  </references>
  
</bio:treatment>