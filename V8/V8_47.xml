<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">leptanthum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 53. 1849  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species leptanthum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065833</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–2 m.</text>
      <biological_entity id="o27727" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or arching, crisped-puberulent, glabrescent;</text>
      <biological_entity id="o27728" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="crisped-puberulent" value_original="crisped-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 1–3, 2–19 mm;</text>
      <biological_entity id="o27729" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27730" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <relation from="o27729" id="r2327" name="at" negation="false" src="d0_s2" to="o27730" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent or sparse to dense.</text>
      <biological_entity id="o27731" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o27732" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s3" value="sparse to dense" value_original="sparse to dense" />
      </biological_entity>
      <relation from="o27731" id="r2328" name="on" negation="false" src="d0_s3" to="o27732" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole (0.1–) 0.7–2 (–4) cm, crisped-puberulent;</text>
      <biological_entity id="o27733" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27734" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="crisped-puberulent" value_original="crisped-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade orbiculate or reniform-orbiculate, 3–5 (–7) -lobed, cleft nearly to midrib, 0.5–1.6 (–2.7) cm, base subcordate or truncate, surfaces usually glabrous, sometimes puberulent, rarely glandular-pubescent, lobes oblong to cuneate, margins with acute teeth, sometimes revolute, apex rounded.</text>
      <biological_entity id="o27735" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o27736" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform-orbiculate" value_original="reniform-orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
        <character constraint="to midrib" constraintid="o27737" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="1.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27737" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o27738" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o27739" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o27740" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o27741" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" notes="" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o27742" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27743" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o27741" id="r2329" name="with" negation="false" src="d0_s5" to="o27742" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, solitary flowers or 2 (–4) -flowered racemes, 1.5–2.5 cm, axis puberulent, flowers evenly spaced.</text>
      <biological_entity id="o27744" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o27745" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27746" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2(-4)-flowered" value_original="2(-4)-flowered" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27747" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o27748" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 0.5–1 mm, puberulent;</text>
      <biological_entity id="o27749" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate, 0.5–4 mm, (with 2 smaller bractlets immediately proximal to each flower), puberulent.</text>
      <biological_entity id="o27750" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium greenish white to white, tubular, (2.3–) 4–6 mm, softly hispid abaxially, glabrous adaxially;</text>
      <biological_entity id="o27751" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27752" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s9" to="white" />
        <character is_modifier="false" name="shape" src="d0_s9" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="softly; abaxially" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, spreading, greenish white to white, lanceolate, (2.5–) 3.5–7 mm;</text>
      <biological_entity id="o27753" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o27754" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s10" to="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals nearly connivent, erect, cream with red margins, whitish or pinkish, oblanceolate to spatulate-obovate, not conspicuously revolute or inrolled, 2–4.4 mm;</text>
      <biological_entity id="o27755" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o27756" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character constraint="with margins" constraintid="o27757" is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s11" to="spatulate-obovate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27757" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o27758" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o27759" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens nearly as long as petals;</text>
      <biological_entity id="o27760" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o27761" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o27762" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <relation from="o27761" id="r2330" name="as long as" negation="false" src="d0_s13" to="o27762" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 1.3–3 mm, glabrous;</text>
      <biological_entity id="o27763" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o27764" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers cream to violet, oval, 0.5–1.6 mm, apex with cupshaped depression;</text>
      <biological_entity id="o27765" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o27766" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s15" to="violet" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oval" value_original="oval" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27767" name="apex" name_original="apex" src="d0_s15" type="structure" />
      <biological_entity id="o27768" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
      <relation from="o27767" id="r2331" name="with" negation="false" src="d0_s15" to="o27768" />
    </statement>
    <statement id="d0_s16">
      <text>ovary pubescence not bristly, hairs soft;</text>
      <biological_entity id="o27769" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o27770" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s16" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o27771" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s16" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate nearly to stigmas, 0.7–1 mm, glabrous.</text>
      <biological_entity id="o27772" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o27773" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="to stigmas" constraintid="o27774" is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27774" name="stigma" name_original="stigmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, dark red to black, globose, 5–10 mm, glabrous, sometimes sparsely puberulent or glandular-pubescent.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 16.</text>
      <biological_entity id="o27775" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s18" to="black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27776" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(-Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="common_name">Trumpet gooseberry</other_name>
  
</bio:treatment>