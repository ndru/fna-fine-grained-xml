<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose in N. L. Britton and J. N. Rose" date="1903" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and J. N. Rose,  New N. Amer. Crassul.,</publication_title>
      <place_in_publication>17. 1903  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species greenei;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092085</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices branched apically, (cespitose), 10–50 × 2–5 cm, axillary branches absent.</text>
      <biological_entity id="o25695" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s0" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o25696" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes 200–300, in clumps, 15–40 (–65) -leaved, 5–15 cm diam.;</text>
      <biological_entity id="o25697" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o25698" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character char_type="range_value" from="200" name="quantity" src="d0_s1" to="300" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="15-40(-65)-leaved" value_original="15-40(-65)-leaved" />
        <character char_type="range_value" from="5" from_unit="cm" name="diameter" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25699" name="clump" name_original="clumps" src="d0_s1" type="structure" />
      <relation from="o25698" id="r2158" name="in" negation="false" src="d0_s1" to="o25699" />
    </statement>
    <statement id="d0_s2">
      <text>blade green, oblong-oblanceolate to obovate, 3–11 × 1–3.5 cm, 4–8 mm thick, base 1–3 cm wide, apex acute to acuminate, subapiculate, surfaces sometimes farinose, not glaucous.</text>
      <biological_entity id="o25700" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25701" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="oblong-oblanceolate" name="shape" src="d0_s2" to="obovate" />
        <character name="thickness" src="d0_s2" unit="cm" value="3-11×1-3.5" value_original="3-11×1-3.5" />
        <character char_type="range_value" from="4" from_unit="mm" name="thickness" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25702" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25703" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="subapiculate" value_original="subapiculate" />
      </biological_entity>
      <biological_entity id="o25704" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="farinose" value_original="farinose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: cyme 3–6-branched, flat-topped or rounded;</text>
      <biological_entity id="o25705" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o25706" name="cyme" name_original="cyme" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-6-branched" value_original="3-6-branched" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches not twisted (flowers on topside), simple or 1–2 times bifurcate, (4–10 cm diam.);</text>
      <biological_entity id="o25707" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o25708" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s4" value="1-2 times bifurcate" value_original="1-2 times bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cincinni 3–5, 2–15-flowered, circinate, 1–9 cm;</text>
      <biological_entity id="o25709" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o25710" name="cincinnus" name_original="cincinni" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-15-flowered" value_original="2-15-flowered" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="circinate" value_original="circinate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral shoots 15–40 × 0.3–1.3 cm;</text>
      <biological_entity id="o25711" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o25712" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaves 15–25, spreading to ascending, triangular to ovate, 10–30 × 5–12 mm, apex acute, (erect).</text>
      <biological_entity id="o25713" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o25714" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="25" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25715" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect, not bent in fruit, 1–5 mm.</text>
      <biological_entity id="o25716" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o25717" is_modifier="false" modifier="not" name="shape" src="d0_s8" value="bent" value_original="bent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25717" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx 4–7 × 4–9 mm;</text>
      <biological_entity id="o25718" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25719" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals connate 1.5–2.5 mm, pale-yellow or whitish especially on margins, 8–12 × 3–5 mm, apex acute, tips sometimes outcurved;</text>
      <biological_entity id="o25720" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25721" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
        <character constraint="on margins" constraintid="o25722" is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25722" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o25723" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o25724" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="outcurved" value_original="outcurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistils connivent, erect.</text>
      <biological_entity id="o25725" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25726" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Unripe follicles erect.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 68, 102.</text>
      <biological_entity id="o25727" name="follicle" name_original="follicles" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="unripe" value_original="unripe" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25728" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="68" value_original="68" />
        <character name="quantity" src="d0_s13" value="102" value_original="102" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sea cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sea cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Greene’s dudleya</other_name>
  <discussion>Dudleya greenei, which occurs only on Santa Cruz, Santa Catalina, San Miguel, and Santa Rosa islands, is an insular segregate of the D. cespitosa complex and is not clearly separable. It is extremely variable, in part from island to island. Extreme variants of D. greenei differ from any plants seen of D. cespitosa, for example in having more numerous rosette leaves or outcurved petals. Also, certain variants kept in D. cespitosa have characteristics apparently not matched in D. greenei, such as red corollas and comparatively thin leaves that are channeled adaxially.</discussion>
  <discussion>Based on 12 collections studied by C. H. Uhl, Dudleya greenei is tetraploid and hexaploid. The three collections from San Miguel Island were hexaploid, the three from Santa Cruz Island and one from Catalina Island were tetraploid, and of the five from Santa Rosa Island, two were tetraploid and three hexaploid. Casual inspection suggests that D. greenei might be derived from something like D. candelabrum, now found on Santa Cruz and Santa Rosa islands, and D. farinosa, now found on the coast of California from Monterey County north.</discussion>
  
</bio:treatment>