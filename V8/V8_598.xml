<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch ex Borkhausen" date="unknown" rank="family">primulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">primula</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in A. Gray et al." date="1886" rank="species">cusickiana</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al.,  Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 2: 399. 1886,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family primulaceae;genus primula;species cusickiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092242</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Primula</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">cusickiana</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2: 393. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Primula;species angustifolia;variety cusickiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (3–) 5–10 cm, herbaceous;</text>
      <biological_entity id="o16279" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s0" value="herbaceous" value_original="herbaceous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes short, stout;</text>
      <biological_entity id="o16280" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rosettes not clumped;</text>
      <biological_entity id="o16281" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="clumped" value_original="clumped" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>vegetative parts only slightly farinose on calyx.</text>
      <biological_entity id="o16282" name="part" name_original="parts" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
        <character constraint="on calyx" constraintid="o16283" is_modifier="false" modifier="only slightly" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
      </biological_entity>
      <biological_entity id="o16283" name="calyx" name_original="calyx" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves not aromatic, indistinctly petiolate;</text>
      <biological_entity id="o16284" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s4" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole winged;</text>
      <biological_entity id="o16285" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade without deep reticulate veins abaxially, lanceolate to spatulate, 1–4 × 0.3–1.8 (–2.3) cm, thick, margins almost entire or somewhat dentate, apex obtuse or acute, surfaces glabrous.</text>
      <biological_entity id="o16286" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s6" to="spatulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="1.8" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o16287" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="depth" src="d0_s6" value="deep" value_original="deep" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o16288" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="almost" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="somewhat; somewhat" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16289" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16290" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o16286" id="r1381" name="without" negation="false" src="d0_s6" to="o16287" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 2–8-flowered;</text>
      <biological_entity id="o16291" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucral-bracts plane, unequal.</text>
      <biological_entity id="o16292" name="involucral-bract" name_original="involucral-bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect to arcuate, thin, 2–25 (–35) mm, length ca. 1–2 times bracts, flexuous.</text>
      <biological_entity id="o16293" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character constraint="bract" constraintid="o16294" is_modifier="false" name="length" src="d0_s9" value="1-2 times bracts" value_original="1-2 times bracts" />
        <character is_modifier="false" name="course" src="d0_s9" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o16294" name="bract" name_original="bracts" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers heterostylous;</text>
      <biological_entity id="o16295" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="heterostylous" value_original="heterostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx green, sometimes with farinose stripes on ridges, narrowly campanulate, 5–11 mm;</text>
      <biological_entity id="o16296" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16297" name="stripe" name_original="stripes" src="d0_s11" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s11" value="farinose" value_original="farinose" />
      </biological_entity>
      <biological_entity id="o16298" name="ridge" name_original="ridges" src="d0_s11" type="structure" />
      <relation from="o16296" id="r1382" modifier="sometimes" name="with" negation="false" src="d0_s11" to="o16297" />
      <relation from="o16297" id="r1383" name="on" negation="false" src="d0_s11" to="o16298" />
    </statement>
    <statement id="d0_s12">
      <text>corolla rose to magenta-violet, tube 7–14 mm, length 1–2 times calyx, glandular or eglandular, limb to 10–25 mm diam., lobes 5–10 mm, apex emarginate.</text>
      <biological_entity id="o16299" name="corolla" name_original="corolla" src="d0_s12" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s12" to="magenta-violet" />
      </biological_entity>
      <biological_entity id="o16300" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character constraint="calyx" constraintid="o16301" is_modifier="false" name="length" src="d0_s12" value="1-2 times calyx" value_original="1-2 times calyx" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o16301" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
      <biological_entity id="o16302" name="limb" name_original="limb" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s12" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16303" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16304" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid, length 1 times calyx.</text>
      <biological_entity id="o16305" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character constraint="calyx" constraintid="o16306" is_modifier="false" name="length" src="d0_s13" value="1 times calyx" value_original="1 times calyx" />
      </biological_entity>
      <biological_entity id="o16306" name="calyx" name_original="calyx" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds unknown.</text>
      <biological_entity id="o16307" name="seed" name_original="seeds" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>Throughout the Great Basin and Intermountain Region, Primula cusickiana exhibits local variation. With the exception of P. capillaris, the members of the P. cusickiana complex are treated as varieties distinguished somewhat arbitrarily by morphological differences; geographic distribution and ecology also separate them. Genetic studies using nuclear ITS and ETS sequences, cpDNA and AFLPs (S. Kelso et al. unpubl.; A. R. Mast et al. 2004) support the close relationships in this clade and to P. capillaris. This infraspecific taxonomic recognition does not diminish their biological importance as isolated endemics almost certainly relictual from the last Ice Age and now restricted to diminishing alpine habitat islands of the Intermountain Region. Given their limited distributions and habitat, which is often threatened by climatic or anthropogenic factors, all representatives of this complex should be considered of conservation concern.</discussion>
  <references>
    <reference>Holmgren, N. H. and S. Kelso. 2001. Primula cusickiana (Primulaceae) and its varieties. Brittonia 53: 154–156.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants glabrous except for white farina on involucral bracts and calyces</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants glandular-pubescent, at least in inflorescences, white-farinose on involucral bracts and calyces</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla tube length 1-1.5 times calyx.</description>
      <determination>17a Primula cusickiana var. cusickiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla tube length usually ca. 2 times calyx.</description>
      <determination>17c Primula cusickiana var. maguirei</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla tube length 1.5+ times calyx, 8-14 mm.</description>
      <determination>17b Primula cusickiana var. domensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corolla tube length to 1.5 times calyx, 7-10 mm.</description>
      <determination>17d Primula cusickiana var. nevadensis</determination>
    </key_statement>
  </key>
</bio:treatment>