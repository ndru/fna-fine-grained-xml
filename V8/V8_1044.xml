<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="treatment_page">535</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1819" rank="genus">gaylussacia</taxon_name>
    <taxon_name authority="(A. Gray) Pursh ex Small" date="1897" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>24: 443. 1897  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus gaylussacia;species tomentosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065728</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gaylussacia</taxon_name>
    <taxon_name authority="(Linnaeus) Torrey &amp; A. Gray" date="unknown" rank="species">frondosa</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>2: 19. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gaylussacia;species frondosa;variety tomentosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Decachaena</taxon_name>
    <taxon_name authority="(A. Gray) Small" date="unknown" rank="species">tomentosa</taxon_name>
    <taxon_hierarchy>genus Decachaena;species tomentosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 7.5–20 dm, forming small to extensive colonies (by rhizomes);</text>
      <biological_entity id="o22808" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7.5" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22809" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s0" to="extensive" />
      </biological_entity>
      <relation from="o22808" id="r1897" name="forming" negation="false" src="d0_s0" to="o22809" />
    </statement>
    <statement id="d0_s1">
      <text>branches spreading;</text>
      <biological_entity id="o22810" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs of current season pale green, densely hairy, sessile-glandular.</text>
      <biological_entity id="o22811" name="twig" name_original="twigs" src="d0_s2" type="structure" constraint="season; season">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity constraint="current" id="o22812" name="season" name_original="season" src="d0_s2" type="structure" />
      <relation from="o22811" id="r1898" name="part_of" negation="false" src="d0_s2" to="o22812" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 2–3 mm;</text>
      <biological_entity id="o22813" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22814" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade greenish white, glaucescent abaxially, dull green to yellowish green adaxially, ovate to oblong, 2.5–6 × 2–3 cm, subcoriaceous, base cuneate, margins entire, apex rounded to obtuse, surfaces densely short-hairy (longer hairs to ca. 0.2 mm), sessile-glandular abaxially.</text>
      <biological_entity id="o22815" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22816" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="abaxially" name="coating" src="d0_s4" value="glaucescent" value_original="glaucescent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dull" value_original="dull" />
        <character char_type="range_value" from="green" modifier="adaxially" name="coloration" src="d0_s4" to="yellowish green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o22817" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22818" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22819" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity id="o22820" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences drooping, 2–4-flowered, sometimes flowers solitary, bracteate, 1–2.5 cm, glabrous or pilose, sessile-glandular;</text>
      <biological_entity id="o22821" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
      <biological_entity id="o22822" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts early-deciduous, leaflike, 5–6 mm, shorter than pedicels, densely hairy.</text>
      <biological_entity id="o22823" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="early-deciduous" value_original="early-deciduous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character constraint="than pedicels" constraintid="o22824" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22824" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 8–15 (–20) mm, glabrous, sessile-glandular;</text>
      <biological_entity id="o22825" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 1–2, 1–2.5 mm.</text>
      <biological_entity id="o22826" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="2" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, 1–1.2 mm, glabrous or sparsely hairy, sessile-glandular;</text>
      <biological_entity id="o22827" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22828" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, corolla greenish white, campanulate-conic, 3–5 mm, lobes deltate, ca. 1 mm;</text>
      <biological_entity id="o22829" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22830" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o22831" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate-conic" value_original="campanulate-conic" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22832" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="deltate" value_original="deltate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 0.5–1 mm, ciliate;</text>
      <biological_entity id="o22833" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22834" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers included (tips barely exserted), 2.5 mm, thecae not divergent distally;</text>
      <biological_entity id="o22835" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o22836" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity id="o22837" name="theca" name_original="thecae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; distally" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary glabrous.</text>
      <biological_entity id="o22838" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o22839" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Drupes juicy, sweet, dark blue, sometimes black, rarely white, glaucous, 5–8 mm diam., glabrous.</text>
      <biological_entity id="o22840" name="drupe" name_original="drupes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="taste" src="d0_s14" value="sweet" value_original="sweet" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark blue" value_original="dark blue" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5 mm.</text>
      <biological_entity id="o22841" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet pine flatwoods and savannas, margins of cypress-gum depressions, margins of blackwater floodplains, streamhead ecotones, sometimes in dry soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet pine flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="margins" constraint="of cypress-gum depressions" />
        <character name="habitat" value="cypress-gum depressions" />
        <character name="habitat" value="moist to wet pine flatwoods" constraint="of blackwater floodplains , streamhead ecotones , sometimes in dry soils" />
        <character name="habitat" value="savannas" constraint="of blackwater floodplains , streamhead ecotones , sometimes in dry soils" />
        <character name="habitat" value="margins of cypress-gum depressions" constraint="of blackwater floodplains , streamhead ecotones , sometimes in dry soils" />
        <character name="habitat" value="margins" constraint="of blackwater floodplains , streamhead ecotones , sometimes in dry soils" />
        <character name="habitat" value="blackwater floodplains" />
        <character name="habitat" value="streamhead ecotones" />
        <character name="habitat" value="dry soils" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Hairy dangleberry</other_name>
  <discussion>Gaylussacia tomentosa is a coastal plain endemic that replaces G. frondosa from South Carolina south-ward. The leaves and twigs are covered with dense, tawny hairs, unlike the relatively sparse, white hairs of G. frondosa.</discussion>
  
</bio:treatment>