<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">530</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1819" rank="genus">gaylussacia</taxon_name>
    <taxon_name authority="(Michaux) A. Gray" date="1846" rank="species">brachycera</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>3: 54. 1846  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus gaylussacia;species brachycera;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065721</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vaccinium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">brachycerum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 234. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vaccinium;species brachycerum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Buxella</taxon_name>
    <taxon_name authority="(Michaux) Small" date="unknown" rank="species">brachycera</taxon_name>
    <taxon_hierarchy>genus Buxella;species brachycera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–4 dm, forming small to extensive colonies;</text>
      <biological_entity id="o24909" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24910" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s0" to="extensive" />
      </biological_entity>
      <relation from="o24909" id="r2082" name="forming" negation="false" src="d0_s0" to="o24910" />
    </statement>
    <statement id="d0_s1">
      <text>branches spreading or procumbent;</text>
      <biological_entity id="o24911" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs of current season pale green to grayish brown, (strongly angled), puberulent.</text>
      <biological_entity id="o24912" name="twig" name_original="twigs" src="d0_s2" type="structure" constraint="season; season">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s2" to="grayish brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="current" id="o24913" name="season" name_original="season" src="d0_s2" type="structure" />
      <relation from="o24912" id="r2083" name="part_of" negation="false" src="d0_s2" to="o24913" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent;</text>
      <biological_entity id="o24914" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–3 mm;</text>
      <biological_entity id="o24915" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade pale green abaxially, shiny dark green adaxially, ovate, 1–2.5 × 0.5–1.3 cm, coriaceous, base rounded, margins crenate or serrulate, (revolute), apex obtuse, without resinous dots, surfaces glabrous adaxially, glabrous or puberulent along midvein and near blade base.</text>
      <biological_entity id="o24916" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o24917" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24918" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o24919" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24920" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="without resinous dots; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="along midvein" constraintid="o24921" is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o24921" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity constraint="blade" id="o24922" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o24920" id="r2084" name="near" negation="false" src="d0_s5" to="o24922" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences spreading, 2–5-flowered, sometimes flowers solitary, ebracteate, 0.5–1 cm, puberulent.</text>
      <biological_entity id="o24923" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
      <biological_entity id="o24924" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="ebracteate" value_original="ebracteate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–3 mm, glabrous;</text>
      <biological_entity id="o24925" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles (early deciduous), 1–3, (ovate), 2–4 mm, (margins ciliate).</text>
      <biological_entity id="o24926" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 4–5, 0.3–0.5 mm, glabrous;</text>
      <biological_entity id="o24927" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24928" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4–5, corolla white to pink, campanulate-urceolate, 4 mm, lobes broadly deltate to rounded, 0.5–1.5 mm;</text>
      <biological_entity id="o24929" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24930" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o24931" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate-urceolate" value_original="campanulate-urceolate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o24932" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly deltate" name="shape" src="d0_s10" to="rounded" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–1.5 mm, glabrous;</text>
      <biological_entity id="o24933" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24934" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers included, 1–1.5 mm, thecae not divergent distally;</text>
      <biological_entity id="o24935" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o24936" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24937" name="theca" name_original="thecae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; distally" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary glabrous.</text>
      <biological_entity id="o24938" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o24939" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Drupes juicy, sweet, light blue, 6–8 mm diam., glabrous.</text>
      <biological_entity id="o24940" name="drupe" name_original="drupes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="juicy" value_original="juicy" />
        <character is_modifier="false" name="taste" src="d0_s14" value="sweet" value_original="sweet" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light blue" value_original="light blue" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–1.5 mm.</text>
      <biological_entity id="o24941" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Upland or montane woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="upland or montane woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(10-)200-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del., Ky., Md., N.C., Pa., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Box huckleberry</other_name>
  <discussion>Gaylussacia brachycera is a dwarf evergreen shrub that forms large, solid-mat, self-sterile colonies, each one appearing to consist of a single clone that may extend over more than one hectare. One colony in Perry County, Pennsylvania, is about 1.5 kilometers wide; it appears to be a single clone that is over 12,000 years old and has been labeled as the oldest living thing in the world.</discussion>
  <references>
    <reference>Pooler, M., R. Nicholson, and A. Vandergrift. 2008. Clonal fidelity in large colonies of Gaylussacia brachycera Gray (box huckleberry) assessed by DNA fingerprinting. N. E. Naturalist 15: 67–74.</reference>
    <reference>Wilbur, R. L. and S. Bloodworth. 2004. Notes on the box huckleberry, Gaylussacia brachycera (Ericaceae), and its unexpected presence in North Carolina. Rhodora 106: 371–377.</reference>
  </references>
  
</bio:treatment>