<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="treatment_page">40</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">oxyacanthoides</taxon_name>
    <taxon_name authority="(Greene) Morin" date="2007" rank="variety">cognatum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 1015. 2007,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species oxyacanthoides;variety cognatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065864</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">cognatum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 115. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ribes;species cognatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(Greene) Coville &amp; Britton" date="unknown" rank="species">cognata</taxon_name>
    <taxon_hierarchy>genus Grossularia;species cognata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">oxyacanthoides</taxon_name>
    <taxon_name authority="(Greene) Q. P. Sinnott" date="unknown" rank="subspecies">cognatum</taxon_name>
    <taxon_hierarchy>genus Ribes;species oxyacanthoides;subspecies cognatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: spines at nodes 1 (–3);</text>
      <biological_entity id="o25008" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o25009" name="spine" name_original="spines" src="d0_s0" type="structure" />
      <biological_entity id="o25010" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s0" to="3" />
        <character name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
      <relation from="o25009" id="r2089" name="at" negation="false" src="d0_s0" to="o25010" />
    </statement>
    <statement id="d0_s1">
      <text>internodal prickles absent to moderately dense.</text>
      <biological_entity id="o25011" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="internodal" id="o25012" name="prickle" name_original="prickles" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="moderately" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 2.5–3.5 (–4) cm, base truncate to cordate, surfaces sparsely to densely pubescent, ± glandular-puberulent, lobes oblong-rounded, margins with 5–9 crenate dentations.</text>
      <biological_entity id="o25013" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="distance" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25014" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cordate" />
      </biological_entity>
      <biological_entity id="o25015" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o25016" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-rounded" value_original="oblong-rounded" />
      </biological_entity>
      <biological_entity id="o25017" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <biological_entity id="o25018" name="dentation" name_original="dentations" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s2" to="9" />
        <character is_modifier="true" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <relation from="o25017" id="r2090" name="with" negation="false" src="d0_s2" to="o25018" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 3–7 mm.</text>
      <biological_entity id="o25019" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 1.5–5 mm, glabrous or stipitate-glandular and pilose.</text>
      <biological_entity id="o25020" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: hypanthium greenish white, narrowly tubular but constricted above ovary, 3.7–6 mm, finely pilose;</text>
      <biological_entity id="o25021" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25022" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
        <character constraint="above ovary" constraintid="o25023" is_modifier="false" name="size" src="d0_s5" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o25023" name="ovary" name_original="ovary" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sepals greenish white or ± pinkish, oblong, 4–6 mm, apex obtuse to rounded, finely pilose;</text>
      <biological_entity id="o25024" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25025" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s6" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25026" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="rounded" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals whitish, oblong-oblanceolate, 2.2–3.5 mm, base cuneate, apex rounded, erose;</text>
      <biological_entity id="o25027" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o25028" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25029" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o25030" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 1.7–2.5 mm;</text>
      <biological_entity id="o25031" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25032" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 7–8.5 mm. 2n = 16.</text>
      <biological_entity id="o25033" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25034" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25035" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, lower hillsides, moist woods, thickets, forest openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="lower hillsides" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="forest openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>49e.</number>
  <other_name type="common_name">Umatilla gooseberry</other_name>
  
</bio:treatment>