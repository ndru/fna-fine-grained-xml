<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tiarella</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">trifoliata</taxon_name>
    <taxon_name authority="(Hooker) Wheelock" date="1896" rank="variety">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 72. 1896,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus tiarella;species trifoliata;variety laciniata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065985</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tiarella</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">laciniata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 239, plate 77. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tiarella;species laciniata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves 3-foliolate;</text>
      <biological_entity constraint="basal" id="o10173" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade 1–5 × 1.5–6 cm;</text>
      <biological_entity id="o10174" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets petiolulate, deeply lobed.</text>
      <biological_entity id="o10175" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves 3-foliolate;</text>
      <biological_entity constraint="cauline" id="o10176" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 3 × 4 cm;</text>
      <biological_entity id="o10177" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="3" value_original="3" />
        <character name="width" src="d0_s4" unit="cm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflet lobes more than 1/2 length of leaflet, distal segments laciniate.</text>
      <biological_entity constraint="leaflet" id="o10178" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" value="1 length of leaflet" value_original="1 length of leaflet" />
        <character name="length" src="d0_s5" value="/2 length of leaflet" value_original="/2 length of leaflet" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 14.</text>
      <biological_entity constraint="distal" id="o10179" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10180" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded, moist woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded" />
        <character name="habitat" value="moist woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <discussion>The terminal leaflet of var. laciniata is rhombic.</discussion>
  
</bio:treatment>