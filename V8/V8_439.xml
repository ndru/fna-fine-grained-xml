<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="A. de Candolle" date="1847" rank="species">praealtum</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Soc. Phys. Genève</publication_title>
      <place_in_publication>11: 445. 1847,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species praealtum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092134</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, glabrous.</text>
      <biological_entity id="o218" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, pendulous, or prostrate, much-branched, not bearing basal rosettes.</text>
      <biological_entity id="o219" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
      </biological_entity>
      <biological_entity constraint="basal" id="o220" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o219" id="r14" name="bearing" negation="true" src="d0_s1" to="o220" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, spreading, sessile;</text>
      <biological_entity id="o221" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green tinged with red, not glaucous, elliptic-oblanceolate, laminar, 40–80 × 13–25 mm, base not spurred, not scarious, apex rounded.</text>
      <biological_entity id="o222" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green tinged with red" value_original="green tinged with red" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
        <character is_modifier="false" name="position" src="d0_s3" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o223" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o224" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots (axillary), ascending, usually simple, sometimes branched, 10–50 cm;</text>
      <biological_entity id="o225" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades elliptic-oblanceolate, base not spurred;</text>
      <biological_entity id="o226" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic-oblanceolate" value_original="elliptic-oblanceolate" />
      </biological_entity>
      <biological_entity id="o227" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s5" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o228" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences elongated paniculate cymes, 50–300+-flowered, 3–25-branched;</text>
      <biological_entity id="o229" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="50-300+-flowered" value_original="50-300+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-25-branched" value_original="3-25-branched" />
      </biological_entity>
      <biological_entity id="o230" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches not recurved, forked;</text>
      <biological_entity id="o231" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s8" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts similar to leaves, smaller.</text>
      <biological_entity id="o232" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o233" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o232" id="r15" name="to" negation="false" src="d0_s9" to="o233" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or to 1 mm.</text>
      <biological_entity id="o234" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0-1 mm" value_original="0-1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers (4–) 5 (–6) -merous;</text>
      <biological_entity id="o235" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="(4-)5(-6)-merous" value_original="(4-)5(-6)-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals erect, usually distinct, green, ovate, lanceolate, or elliptic-oblong, unequal, 1.5–9.6 × 1–3.2 mm, apex obtuse;</text>
      <biological_entity id="o236" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="9.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o237" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals widely spreading, distinct or slightly connate basally, yellow, lanceolate, carinate, ca. 7.5 mm, apex acute or obtuse, mucronate;</text>
      <biological_entity id="o238" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="slightly; basally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="carinate" value_original="carinate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="7.5" value_original="7.5" />
      </biological_entity>
      <biological_entity id="o239" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s13" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments yellow;</text>
      <biological_entity id="o240" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow;</text>
      <biological_entity id="o241" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar scales yellowish or translucent, subquadrate or reniform.</text>
      <biological_entity constraint="nectar" id="o242" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels widely divergent in fruit, distinct, brown.</text>
      <biological_entity id="o244" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 68.</text>
      <biological_entity id="o243" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in fruit" constraintid="o244" is_modifier="false" modifier="widely" name="arrangement" src="d0_s17" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o245" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late winter" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; c Mexico; Central America (Guatemala); introduced also in Europe (Italy), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="c Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="also in Europe (Italy)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <discussion>The first record of Sedum praealtum cultivated in the United States is from 1930. It is native in the trans-Mexican volcanic belt. It was reported from Ventura County in 1948 and has naturalized in the vicinity of Santa Cruz in Santa Cruz County.</discussion>
  
</bio:treatment>