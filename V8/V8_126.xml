<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Haworth" date="1812" rank="genus">micranthes</taxon_name>
    <taxon_name authority="(Waldstein &amp; Kitaibel ex Willdenow) Haworth" date="1821" rank="species">hieraciifolia</taxon_name>
    <place_of_publication>
      <publication_title>Saxifrag. Enum.,</publication_title>
      <place_in_publication>45. 1821 (as hieracifolia)  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus micranthes;species hieraciifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250065907</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="Waldstein &amp; Kitaibel ex Willdenow" date="unknown" rank="species">hieraciifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 641. 1799 (as hieracifolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Saxifraga;species hieraciifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hieraciifolia</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">angusticapsula</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species hieraciifolia;variety angusticapsula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Saxifraga</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hieraciifolia</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">rufopilosa</taxon_name>
    <taxon_hierarchy>genus Saxifraga;species hieraciifolia;variety rufopilosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly solitary, with short caudices, sometimes rhizomatous.</text>
      <biological_entity id="o25436" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o25437" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o25436" id="r2133" name="with" negation="false" src="d0_s0" to="o25437" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o25438" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole sometimes indistinct, flattened, 2–4 cm;</text>
      <biological_entity id="o25439" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s2" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to elliptic, 2–6 cm, ± fleshy, base cuneate to slightly attenuate, margins usually ± evenly serrulate to denticulate, ciliate, surfaces sparsely to densely hairy and sparsely stipitate-glandular.</text>
      <biological_entity id="o25440" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o25441" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="slightly attenuate" />
      </biological_entity>
      <biological_entity id="o25442" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="less evenly serrulate" name="shape" src="d0_s3" to="denticulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o25443" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 30+-flowered, constricted, spikelike thyrses, 7–30 cm, tangled-hairy proximally, densely purple-tipped stipitate-glandular distally.</text>
      <biological_entity id="o25444" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="30+-flowered" value_original="30+-flowered" />
        <character is_modifier="false" name="size" src="d0_s4" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o25445" name="thyrse" name_original="thyrses" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s4" value="tangled-hairy" value_original="tangled-hairy" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s4" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals spreading to reflexed, triangular to ovate;</text>
      <biological_entity id="o25446" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o25447" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="reflexed" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s5" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals dark reddish purple, not spotted, narrowly ovate to lanceolate, slightly clawed, 1.5–3 mm, equaling sepals;</text>
      <biological_entity id="o25448" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o25449" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s6" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25450" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="true" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments linear, flattened;</text>
      <biological_entity id="o25451" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o25452" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistils connate to 1/2 their lengths;</text>
      <biological_entity id="o25453" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25454" name="pistil" name_original="pistils" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s8" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 1/2+ inferior.</text>
      <biological_entity id="o25455" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25456" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s9" upper_restricted="false" />
        <character is_modifier="false" name="position" src="d0_s9" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules reddish to purple, valvate.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 112, 120.</text>
      <biological_entity id="o25457" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="purple" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s10" value="valvate" value_original="valvate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25458" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="112" value_original="112" />
        <character name="quantity" src="d0_s11" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, mossy tundra, streamsides, ledges, crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mossy tundra" modifier="wet" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T., Nunavut, Yukon; Alaska, Mont.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="common_name">Hawkweed-leaved saxifrage</other_name>
  <discussion>In British Columbia, Micranthes hieraciifolia is disjunct between the far north and the Okanagan-Thompson Plateau in the south (S. A. Harris 2003).</discussion>
  
</bio:treatment>