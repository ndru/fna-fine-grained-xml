<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">212</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="Fröderström" date="1936" rank="species">nanifolium</taxon_name>
    <place_of_publication>
      <publication_title>Acta Horti Gothob.</publication_title>
      <place_in_publication>10(app.): 96, figs. 736–746, plate 61. 1936,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species nanifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092124</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="Hemsley" date="unknown" rank="species">parvum</taxon_name>
    <taxon_name authority="(Fröderström) R. T. Clausen" date="unknown" rank="subspecies">nanifolium</taxon_name>
    <taxon_hierarchy>genus Sedum;species parvum;subspecies nanifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, mat-forming, glabrous.</text>
      <biological_entity id="o25519" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent, becoming erect, (reddish-shiny proximally), long-branched, not bearing rosettes.</text>
      <biological_entity id="o25520" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-branched" value_original="long-branched" />
      </biological_entity>
      <biological_entity id="o25521" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o25520" id="r2139" name="bearing" negation="true" src="d0_s1" to="o25521" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, (densely set), erect to slightly spreading, sessile;</text>
      <biological_entity id="o25522" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="slightly spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green with prominent red dots, not glaucous, sometimes waxy, orbiculate to broadly obovate, semiterete, 2.5–3.5 (–5) × 2–2.5 mm, base not spurred, not scarious, apex rounded to acute.</text>
      <biological_entity id="o25523" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="with base, apex" constraintid="o25524, o25525" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o25524" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="red dots" value_original="red dots" />
        <character is_modifier="true" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="true" modifier="sometimes" name="texture" src="d0_s3" value="ceraceous" value_original="waxy" />
        <character char_type="range_value" from="orbiculate" is_modifier="true" name="shape" src="d0_s3" to="broadly obovate" />
        <character is_modifier="true" name="shape" src="d0_s3" value="semiterete" value_original="semiterete" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o25525" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="red dots" value_original="red dots" />
        <character is_modifier="true" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="true" modifier="sometimes" name="texture" src="d0_s3" value="ceraceous" value_original="waxy" />
        <character char_type="range_value" from="orbiculate" is_modifier="true" name="shape" src="d0_s3" to="broadly obovate" />
        <character is_modifier="true" name="shape" src="d0_s3" value="semiterete" value_original="semiterete" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots erect, simple or branched, 2–4 cm;</text>
      <biological_entity id="o25526" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades orbiculate to broadly ovate, base not spurred;</text>
      <biological_entity id="o25527" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o25528" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s5" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o25529" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences subscorpioid cymes, 10–20-flowered, simple to 2-branched;</text>
      <biological_entity id="o25530" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="10-20-flowered" value_original="10-20-flowered" />
        <character char_type="range_value" from="simple" name="architecture" src="d0_s7" to="2-branched" />
      </biological_entity>
      <biological_entity id="o25531" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="subscorpioid" value_original="subscorpioid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches erect to spreading or recurved, sometimes forked;</text>
      <biological_entity id="o25532" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading or recurved" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts oblong, ca. 3 mm, base broadly spurred.</text>
      <biological_entity id="o25533" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o25534" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s9" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent.</text>
      <biological_entity id="o25535" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o25536" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals suberect, distinct basally, greenish, sometimes with reddish striations, subovate, unequal, 2–2.5 × 0.1–1.6 mm, apex obtuse, (papillose);</text>
      <biological_entity id="o25537" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="subovate" value_original="subovate" />
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25538" name="striation" name_original="striations" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o25539" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o25537" id="r2140" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o25538" />
    </statement>
    <statement id="d0_s13">
      <text>petals erect to spreading, distinct, yellow with prominent, short, longitudinal red stripes, lanceolate, carinate, 4.5–5 mm, apex subobtuse, narrowly mucronate;</text>
      <biological_entity id="o25540" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character constraint="with prominent , short , longitudinal red stripes , lanceolate , carinate" is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25541" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subobtuse" value_original="subobtuse" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments whitish or pale-yellow;</text>
      <biological_entity id="o25542" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow;</text>
      <biological_entity id="o25543" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar scales inconspicuous.</text>
      <biological_entity constraint="nectar" id="o25544" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels spreading in fruit, distinct, tan or brown with reddish striations.</text>
      <biological_entity id="o25546" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
      <biological_entity id="o25547" name="striation" name_original="striations" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 52, 53, 104.</text>
      <biological_entity id="o25545" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character constraint="in fruit" constraintid="o25546" is_modifier="false" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="tan" value_original="tan" />
        <character constraint="with striations" constraintid="o25547" is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25548" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="52" value_original="52" />
        <character name="quantity" src="d0_s18" value="53" value_original="53" />
        <character name="quantity" src="d0_s18" value="104" value_original="104" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early winter" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone gravel or outcrops in various vegetation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravel" modifier="limestone" constraint="in various vegetation" />
        <character name="habitat" value="outcrops" constraint="in various vegetation" />
        <character name="habitat" value="various vegetation" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <discussion>Sedum nanifolium is found in the Del Norte Mountains of Brewster County. The long-branched (to 20 cm), reddish-shiny stems are distinctive; see discussion under 19. S. robertsianum.</discussion>
  
</bio:treatment>