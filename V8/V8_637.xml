<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John J. Pipoly III,Jon M. Ricketson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">302</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">myrsinaceae</taxon_name>
    <taxon_name authority="Swartz" date="1788" rank="genus">ARDISIA</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>3, 48. 1788, name conserved  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrsinaceae;genus ARDISIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ardis, point of arrow or spear, alluding to anthers and/or corolla lobes</other_info_on_name>
    <other_info_on_name type="fna_id">102511</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, shrubs, or trees, not succulent, hairy, glabrescent.</text>
      <biological_entity id="o17712" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent, alternate (pseudoverticillate in A. japonica), monomorphic or dimorphic;</text>
      <biological_entity id="o17715" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present [absent];</text>
      <biological_entity id="o17716" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate, elliptic, or oblanceolate to obovate, base acute to obovate, margins entire, crenate, or undulate, flat, subrevolute, or revolute, apex obtuse or acute to acuminate, surfaces glabrous and/or with lepidote scales, sessile or stipitate-stellate hairs, or glandular-villous.</text>
      <biological_entity id="o17717" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
      <biological_entity id="o17718" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
      <biological_entity id="o17719" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subrevolute" value_original="subrevolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o17720" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o17721" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="with scales" constraintid="o17722" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17722" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="lepidote" value_original="lepidote" />
      </biological_entity>
      <biological_entity id="o17723" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s3" value="stipitate-stellate" value_original="stipitate-stellate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-villous" value_original="glandular-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, subterminal, axillary, or lateral panicles, racemes, cymes, subumbels, or umbels, sessile (pedunculate in A. japonica), longer than petioles.</text>
      <biological_entity id="o17724" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o17725" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
      <biological_entity id="o17726" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
      <biological_entity id="o17727" name="cyme" name_original="cymes" src="d0_s4" type="structure" />
      <biological_entity id="o17728" name="subumbel" name_original="subumbels" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="sessile" value_original="sessile" />
        <character constraint="than petioles" constraintid="o17730" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o17729" name="umbel" name_original="umbels" src="d0_s4" type="structure" />
      <biological_entity id="o17730" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels present [absent].</text>
      <biological_entity id="o17731" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 4–5 (–6), green, calyx lobes oblong-ovate to ovate, ± equaling tube;</text>
      <biological_entity id="o17732" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o17733" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="calyx" id="o17734" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
      <biological_entity id="o17735" name="tube" name_original="tube" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 4–5 (–6), corolla white to pink, cupuliform to campanulate, lobes ± equaling tube, apex acute to acuminate;</text>
      <biological_entity id="o17736" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17737" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o17738" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pink" />
        <character char_type="range_value" from="cupuliform" name="shape" src="d0_s7" to="campanulate" />
      </biological_entity>
      <biological_entity id="o17739" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o17740" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o17741" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens (4–) 5 (–6);</text>
      <biological_entity id="o17742" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17743" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments connate basally into inconspicuous tube, free from corolla-tube;</text>
      <biological_entity id="o17744" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17745" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="into tube" constraintid="o17746" is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character constraint="from corolla-tube" constraintid="o17747" is_modifier="false" name="fusion" notes="" src="d0_s9" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o17746" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o17747" name="corolla-tube" name_original="corolla-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stigma punctiform.</text>
      <biological_entity id="o17748" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17749" name="stigma" name_original="stigma" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits drupaceous, globose or subglobose (A. elliptica), sometimes longitudinally costate, 4–8 mm, fleshy (endocarp crusty or slightly bony), indehiscent.</text>
      <biological_entity id="o17750" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="sometimes longitudinally" name="architecture" src="d0_s11" value="costate" value_original="costate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1, white to black, globose, covered by membranous remnants of placenta.</text>
      <biological_entity id="o17752" name="remnant" name_original="remnants" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o17753" name="placenta" name_original="placenta" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o17751" id="r1484" name="covered by" negation="false" src="d0_s12" to="o17752" />
      <relation from="o17751" id="r1485" name="covered by" negation="false" src="d0_s12" to="o17753" />
    </statement>
    <statement id="d0_s13">
      <text>x = 6.</text>
      <biological_entity id="o17751" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="black" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity constraint="x" id="o17754" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mexico, West Indies, Central America, South America, Asia, Indian Ocean Islands (e, se Madagascar), Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (e)" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (se Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Marlberry</other_name>
  <discussion>Icacorea Aublet, name rejected</discussion>
  <discussion>Species ca. 400–500 (4 in the flora).</discussion>
  <references>
    <reference>Ricketson, J. M. and J. J. Pipoly. 2003. Revision of Ardisia subgenus Auriculardisia (Myrsinaceae). Ann. Missouri Bot. Gard. 90: 179–317.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants subshrubs, stoloniferous, 0.02-0.3(-0.4) m; leaf blade margins serrulate; inflores- cences 3-5-flowered.</description>
      <determination>4 Ardisia japonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants subshrubs, shrubs, or trees, not stoloniferous, 1-15.2 m; leaf blade margins crenulate, undulate, or entire; inflorescences 5-18+-flowered</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins crenulate or undulate (bearing vascularized nodules).</description>
      <determination>1 Ardisia crenata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade margins entire (without vascularized nodules)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branchlets glabrous; inflorescences subumbels or umbels.</description>
      <determination>2 Ardisia elliptica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branchlets sparsely to densely rufous-papillate, sometimes also with multicellular hairs; inflorescences panicles of racemes.</description>
      <determination>3 Ardisia escallonioides</determination>
    </key_statement>
  </key>
</bio:treatment>