<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="mention_page">309</other_info_on_meta>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">myrsinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lysimachia</taxon_name>
    <taxon_name authority="(Alph. Wood) Alph. Wood ex R. Knuth in H. G. A. Engler" date="1905" rank="species">tonsa</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler,  Pflanzenr.</publication_title>
      <place_in_publication>22[IV,237]: 277. 1905  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family myrsinaceae;genus lysimachia;species tonsa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092261</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ciliata</taxon_name>
    <taxon_name authority="Alph. Wood" date="unknown" rank="variety">tonsa</taxon_name>
    <place_of_publication>
      <publication_title>Class-book Bot. ed. s.n.(b),</publication_title>
      <place_in_publication>505. 1861</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lysimachia;species ciliata;variety tonsa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lysimachia</taxon_name>
    <taxon_name authority="Kearney" date="unknown" rank="species">tonsa</taxon_name>
    <taxon_name authority="(Kearney) R. Knuth" date="unknown" rank="variety">simplex</taxon_name>
    <taxon_hierarchy>genus Lysimachia;species tonsa;variety simplex;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Steironema</taxon_name>
    <taxon_name authority="(Alph. Wood) E. P. Bicknell" date="unknown" rank="species">intermedium</taxon_name>
    <taxon_hierarchy>genus Steironema;species intermedium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Steironema</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tonsum</taxon_name>
    <taxon_hierarchy>genus Steironema;species tonsum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, simple or branched distally, 3–7 dm, stipitate-glandular or pubescent;</text>
      <biological_entity id="o21288" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes slender to thickened (sometimes absent);</text>
      <biological_entity id="o21289" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o21290" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite;</text>
      <biological_entity id="o21291" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–4.5 cm, ciliate proximally near node, cilia 0.2–0.7 (–1.2) mm;</text>
      <biological_entity id="o21292" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4.5" to_unit="cm" />
        <character constraint="near node" constraintid="o21293" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21293" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o21294" name="cilium" name_original="cilia" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to ovate, 3–9 × (0.8–) 1–4.5 cm, base truncate, rounded, or cuneate, decurrent, margins entire or slightly sinuate, plane, sometimes obscurely ciliolate (rarely stipitate-glandular), apex acute to acuminate, surfaces not punctate, glabrous;</text>
      <biological_entity id="o21295" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21296" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o21297" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="sometimes obscurely" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o21298" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate-arcuate.</text>
      <biological_entity id="o21299" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s5" value="punctate" value_original="punctate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="course_or_shape" src="d0_s6" value="pinnate-arcuate" value_original="pinnate-arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary in distal leaves, solitary flowers or verticils.</text>
      <biological_entity id="o21300" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character constraint="in verticils" constraintid="o21303" is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21301" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21302" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o21303" name="verticil" name_original="verticils" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.8–4 cm, stipitate-glandular at least distally.</text>
      <biological_entity id="o21304" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="at-least distally; distally" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, calyx not streaked, 2.5–7.5 mm, glandular proximally or glabrous, lobes ovate to lanceolate, margins somewhat thickened;</text>
      <biological_entity id="o21305" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21306" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21307" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="streaked" value_original="streaked" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21308" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o21309" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s9" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, corolla yellow, often with reddish base, not streaked, rotate, 6–12 mm, lobes with margins usually entire or sometimes erose distally, apex apiculate, stipitate-glandular adaxially;</text>
      <biological_entity id="o21310" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21311" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21312" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="not" name="coloration" notes="" src="d0_s10" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rotate" value_original="rotate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21313" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o21314" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes; sometimes; distally" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o21315" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21316" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o21312" id="r1758" modifier="often" name="with" negation="false" src="d0_s10" to="o21313" />
      <relation from="o21314" id="r1759" name="with" negation="false" src="d0_s10" to="o21315" />
    </statement>
    <statement id="d0_s11">
      <text>filaments distinct or nearly so, shorter than corolla;</text>
      <biological_entity id="o21317" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21318" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character name="fusion" src="d0_s11" value="nearly" value_original="nearly" />
        <character constraint="than corolla" constraintid="o21319" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21319" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>staminodes 0.6–1 mm.</text>
      <biological_entity id="o21320" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21321" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 3.5–5.8 mm, not punctate, glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 34, 102.</text>
      <biological_entity id="o21322" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s13" value="punctate" value_original="punctate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21323" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
        <character name="quantity" src="d0_s14" value="102" value_original="102" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist hardwood forests, pine-oak woods, sandstone bluffs, rocky areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist hardwood forests" />
        <character name="habitat" value="pine-oak woods" />
        <character name="habitat" value="sandstone bluffs" />
        <character name="habitat" value="rocky areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ky., N.C., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Appalachian or southern loosestrife</other_name>
  
</bio:treatment>