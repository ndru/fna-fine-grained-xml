<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">19</other_info_on_meta>
    <other_info_on_meta type="illustration_page">16</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1815" rank="species">malvaceum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees,  Cycl.</publication_title>
      <place_in_publication>30: Ribes no. 13. 1815  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species malvaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065811</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">malvaceum</taxon_name>
    <taxon_name authority="Dunkle" date="unknown" rank="variety">clementinum</taxon_name>
    <taxon_hierarchy>genus Ribes;species malvaceum;variety clementinum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">malvaceum</taxon_name>
    <taxon_name authority="Abrams" date="unknown" rank="variety">viridifolium</taxon_name>
    <taxon_hierarchy>genus Ribes;species malvaceum;variety viridifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2 m.</text>
      <biological_entity id="o22120" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, tomentose with gland-tipped, bristly hairs;</text>
      <biological_entity id="o22121" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="with hairs" constraintid="o22122" is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o22122" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="true" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes absent;</text>
      <biological_entity id="o22123" name="spine" name_original="spines" src="d0_s2" type="structure" />
      <biological_entity id="o22124" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o22123" id="r1846" name="at" negation="false" src="d0_s2" to="o22124" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o22125" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o22126" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o22125" id="r1847" name="on" negation="false" src="d0_s3" to="o22126" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 1–5 cm, pubescent and stipitate-glandular;</text>
      <biological_entity id="o22127" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22128" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade roundish, 3–5-lobed, cleft nearly 1/4 to midrib, 2–6 cm, base deeply cordate, surfaces stipitate-glandular, glands colorless, and tomentose abaxially, rough-hairy adaxially, (dark green and rugose), lobes deltate, margins biserrate, apex obtuse.</text>
      <biological_entity id="o22129" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o22130" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="roundish" value_original="roundish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character constraint="to midrib" constraintid="o22131" name="quantity" src="d0_s5" value="1/4" value_original="1/4" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22131" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o22132" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o22133" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o22134" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="colorless" value_original="colorless" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="rough-hairy" value_original="rough-hairy" />
      </biological_entity>
      <biological_entity id="o22135" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o22136" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="biserrate" value_original="biserrate" />
      </biological_entity>
      <biological_entity id="o22137" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, 10–25-flowered racemes, 3–5 cm, axis stipitate-glandular, flowers evenly spaced.</text>
      <biological_entity id="o22138" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o22139" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="10-25-flowered" value_original="10-25-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22140" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o22141" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels jointed, 1–2 mm, pubescent, stipitate-glandular;</text>
      <biological_entity id="o22142" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts oblanceolate or wider, 6–9 mm, pubescent, stipitate-glandular.</text>
      <biological_entity id="o22143" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="width" src="d0_s8" value="wider" value_original="wider" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium pink, narrowly tubular-urceolate, 5–8 mm, stipitate-glandular abaxially, villous-pubescent adaxially;</text>
      <biological_entity id="o22144" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22145" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="tubular-urceolate" value_original="tubular-urceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="villous" value_original="villous-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals nearly overlapping at base, spreading, pink to purple, obovate, (1.5–) 4–6 mm;</text>
      <biological_entity id="o22146" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22147" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o22148" is_modifier="false" modifier="nearly" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22148" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>petals nearly connivent, erect, pink to white, oblongelliptic, not conspicuously revolute or inrolled, 2–3 mm;</text>
      <biological_entity id="o22149" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22150" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc not prominent;</text>
      <biological_entity id="o22151" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o22152" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens nearly as long as petals;</text>
      <biological_entity id="o22153" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o22154" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o22155" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <relation from="o22154" id="r1848" name="as long as" negation="false" src="d0_s13" to="o22155" />
    </statement>
    <statement id="d0_s14">
      <text>filaments broader toward base, 0.6 mm, glabrous;</text>
      <biological_entity id="o22156" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o22157" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="toward base" constraintid="o22158" is_modifier="false" name="width" src="d0_s14" value="broader" value_original="broader" />
        <character name="some_measurement" notes="" src="d0_s14" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22158" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers white, ovate, 1.2–1.3 mm, apex minutely apiculate;</text>
      <biological_entity id="o22159" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o22160" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22161" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary densely stipitate-glandular;</text>
      <biological_entity id="o22162" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o22163" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate nearly to stigmas, 6–7 mm, sparsely hairy.</text>
      <biological_entity id="o22164" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o22165" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="to stigmas" constraintid="o22166" is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22166" name="stigma" name_original="stigmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, purple, globose, 6–7 mm, hairs glandular.</text>
      <biological_entity id="o22167" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s18" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s18" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22168" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s18" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Chaparral currant</other_name>
  <discussion>Ribes malvaceum occurs in the southern North Coast, South Coast, Transverse, and Peninsular ranges, and the Channel Islands. It has also been reported from the Sierra Nevada in Tuolumne County. Its thick, rugose leaves, which are white-tomentose abaxially and dark green adaxially, and glaucous, white-haired berries are striking. Plants with dark green leaves occurring below 800 meters have been recognized as var. malvaceum, those with bright green leaves occurring up to 1500 meters as var. viridifolium.</discussion>
  
</bio:treatment>