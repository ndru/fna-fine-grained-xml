<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
    <other_info_on_meta type="illustration_page">92</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">heuchera</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="species">micrantha</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>15: plate 1302. 1830  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus heuchera;species micrantha</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065940</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs caulescent;</text>
      <biological_entity id="o22758" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched.</text>
      <biological_entity id="o22759" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems 6–57 cm, short to long-stipitate-glandular or glabrous, viscid.</text>
      <biological_entity id="o22760" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="57" to_unit="cm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s2" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole glabrous or sparsely to densely short to long-stipitate-glandular;</text>
      <biological_entity id="o22761" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22762" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely to densely" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade orbiculate to polygonal, shallowly to deeply 5–7 (–9) -lobed, 2.5–10 cm, base cordate, lobes rounded, margins dentate, apex rounded or obtuse, surfaces glabrous or short to long-stipitate-glandular, viscid.</text>
      <biological_entity id="o22763" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22764" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="polygonal" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s4" to="polygonal" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22765" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o22766" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o22767" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o22768" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o22769" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" name="coating" src="d0_s4" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences diffuse.</text>
      <biological_entity id="o22770" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium radially symmetric, free to 1.5 mm, greenish white, often tinged with red, obconic to hemispheric, broadly turbinate, or campanulate, 1–4.9 mm, long-stipitate-glandular, sometimes short-stipitate-glandular proximally;</text>
      <biological_entity id="o22771" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22772" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="tinged with red" value_original="tinged with red" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="hemispheric broadly turbinate or campanulate" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="hemispheric broadly turbinate or campanulate" />
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="hemispheric broadly turbinate or campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes; proximally" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals spreading to nearly erect, green or red-tipped, equal, 0.5–1.8 mm, apex rounded to acute or mucronate;</text>
      <biological_entity id="o22773" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22774" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="nearly erect" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red-tipped" value_original="red-tipped" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22775" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="acute or mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals often coiled, white or pale-pink, oblanceolate, (narrowly clawed), unlobed, 1.6–3.3 mm (2–3 times as long as sepals), margins entire;</text>
      <biological_entity id="o22776" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22777" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s8" value="coiled" value_original="coiled" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s8" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22778" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens exserted to 3 mm;</text>
      <biological_entity id="o22779" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22780" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character constraint="to 3 mm" is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles exserted to 2.5 mm, 0.2–4.2 mm, to 0.1 mm diam.</text>
      <biological_entity id="o22781" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22782" name="style" name_original="styles" src="d0_s10" type="structure">
        <character constraint="to 2.5 mm" is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s10" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, 3–8.5 mm, beaks divergent, not papillose.</text>
      <biological_entity id="o22783" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22784" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds black, broadly ellipsoid, (not curved), 0.5–0.8 mm.</text>
      <biological_entity id="o22785" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Crevice alum-root</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>The Skagit Indians rubbed pounded plants of Heuchera micrantha on hair to make it grow and applied it to cuts. The Thompson Indians used a mashed poultice of this root mixed with Douglas fir pitch for wounds. Chewed leaves and roots were spat on sores or wounds. Infusions of roots were taken for liver trouble and sore throat. Small, peeled, cleaned root pieces were chewed for mouth sores and gum boils (D. E. Moerman 1998).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades orbiculate to pentagonal, shallowly lobed.</description>
      <determination>4a Heuchera micrantha var. micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades pentagonal or heptagonal, deeply lobed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences short stipitate-glandular; petioles short stipitate-glandular or sparsely long stipitate-glandular</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences long stipitate-glandular; petioles short to long stipitate-glandular</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypanthia hemispheric, 1 times long as wide; petals 0.4-1.4 mm wide.</description>
      <determination>4b Heuchera micrantha var. macropetala</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypanthia obconic, 1.4 times longer than wide; petals 0.2-0.6 mm wide.</description>
      <determination>4c Heuchera micrantha var. erubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypanthia long stipitate-glandular, sepal apex rounded to acute.</description>
      <determination>4d Heuchera micrantha var. hartwegii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypanthia sparsely long stipitate-glandular, sepal apex acute to mucronate</description>
      <determination>4e Heuchera micrantha var. diversifolia</determination>
    </key_statement>
  </key>
</bio:treatment>