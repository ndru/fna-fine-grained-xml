<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crassula</taxon_name>
    <taxon_name authority="Thunberg" date="1778" rank="species">argentea</taxon_name>
    <place_of_publication>
      <publication_title>Nova Acta Phys.-Med. Acad. Caes. Leop.-Carol. Nat. Cur.</publication_title>
      <place_in_publication>6: 329, 337. 1778  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus crassula;species argentea</taxon_hierarchy>
    <other_info_on_name type="fna_id">242412765</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, perennial.</text>
      <biological_entity id="o6421" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, silvery in age, moderately branched, 30–50 cm.</text>
      <biological_entity id="o6422" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="in age" constraintid="o6423" is_modifier="false" name="coloration" src="d0_s1" value="silvery" value_original="silvery" />
        <character is_modifier="false" modifier="moderately" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6423" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades obovate, 20–70 mm, apex obtuse to retuse.</text>
      <biological_entity id="o6424" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="distance" src="d0_s2" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6425" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences compact, flowers 2 per node.</text>
      <biological_entity id="o6426" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o6427" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character constraint="per node" constraintid="o6428" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6428" name="node" name_original="node" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 8–12 mm.</text>
      <biological_entity id="o6429" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 4-merous;</text>
      <biological_entity id="o6430" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals (erect), triangular, 0.5 mm, apex acute;</text>
      <biological_entity id="o6431" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o6432" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals oblong-lanceolate, ca. 10 mm.</text>
      <biological_entity id="o6433" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Follicles erect, seed number not known, ovoid;</text>
      <biological_entity id="o6434" name="follicle" name_original="follicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6435" name="seed" name_original="seed" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>old follicles erect, boatshaped.</text>
      <biological_entity id="o6436" name="follicle" name_original="follicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="old" value_original="old" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="boat-shaped" value_original="boat-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds not seen.</text>
      <biological_entity id="o6437" name="seed" name_original="seeds" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges of lagoons, sand dunes, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of lagoons , sand dunes , disturbed areas" />
        <character name="habitat" value="lagoons" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Jade plant</other_name>
  <discussion>Crassula argentea is reported from Los Angeles and San Diego counties.</discussion>
  
</bio:treatment>