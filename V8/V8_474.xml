<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">227</other_info_on_meta>
    <other_info_on_meta type="illustration_page">225</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Rose" date="1911" rank="genus">graptopetalum</taxon_name>
    <taxon_name authority="Rose" date="1926" rank="species">bartramii</taxon_name>
    <place_of_publication>
      <publication_title>Addisonia</publication_title>
      <place_in_publication>11: 1, plate 353. 1926,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus graptopetalum;species bartramii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250092159</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echeveria</taxon_name>
    <taxon_name authority="(Rose) Kearney &amp; Peebles" date="unknown" rank="species">bartramii</taxon_name>
    <taxon_hierarchy>genus Echeveria;species bartramii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems without slender branches, 1–3 cm thick.</text>
      <biological_entity id="o11388" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="thickness" notes="" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o11389" name="branch" name_original="branches" src="d0_s0" type="structure" />
      <relation from="o11388" id="r946" name="without" negation="false" src="d0_s0" to="o11389" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: rosettes solitary or multiple, 15–70-leaved, 7–16 cm diam.;</text>
      <biological_entity id="o11390" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o11391" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="15-70-leaved" value_original="15-70-leaved" />
        <character char_type="range_value" from="7" from_unit="cm" name="diameter" src="d0_s1" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade blue-green, cuneate-oblanceolate or obovate, 3–10 × 1–4 cm, smooth, apex scarcely apiculate, surfaces somewhat glaucous.</text>
      <biological_entity id="o11392" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11393" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="blue-green" value_original="blue-green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate-oblanceolate" value_original="cuneate-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o11394" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="scarcely" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o11395" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences paniculate cymes;</text>
      <biological_entity id="o11396" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o11397" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>floral stems 1–3 dm;</text>
      <biological_entity constraint="floral" id="o11398" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s4" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal leaves 12–35 mm;</text>
      <biological_entity constraint="proximal" id="o11399" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches 7–18, not circinate, 1–6-flowered.</text>
      <biological_entity id="o11400" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="18" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s6" value="circinate" value_original="circinate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-6-flowered" value_original="1-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 4–18 mm.</text>
      <biological_entity id="o11401" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers mostly 5-merous;</text>
      <biological_entity id="o11402" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s8" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla 19–28 mm diam., tube 3–3.5 mm, lobes 8–15 × 2–3.5 mm;</text>
      <biological_entity id="o11403" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character char_type="range_value" from="19" from_unit="mm" name="diameter" src="d0_s9" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11404" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11405" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistils hollowed abaxially at base, abruptly narrowed to conic styles 0.5–1 mm. 2n = 62.</text>
      <biological_entity id="o11406" name="pistil" name_original="pistils" src="d0_s10" type="structure">
        <character char_type="range_value" from="abruptly narrowed" name="shape" src="d0_s10" to="conic" />
      </biological_entity>
      <biological_entity id="o11407" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o11408" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11409" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="62" value_original="62" />
      </biological_entity>
      <relation from="o11406" id="r947" name="hollowed" negation="false" src="d0_s10" to="o11407" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall–winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="winter" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices and gravelly slopes in mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Chihuahua, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>