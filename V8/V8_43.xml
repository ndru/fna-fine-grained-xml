<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="Greene" date="1885" rank="species">velutinum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 83. 1885  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species velutinum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065829</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grossularia</taxon_name>
    <taxon_name authority="(Greene) Coville &amp; Britton" date="unknown" rank="species">velutina</taxon_name>
    <taxon_hierarchy>genus Grossularia;species velutina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="M. Peck" date="unknown" rank="species">gooddingii</taxon_name>
    <taxon_hierarchy>genus Ribes;species gooddingii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">velutinum</taxon_name>
    <taxon_name authority="(A. Heller) Jepson" date="unknown" rank="variety">glanduliferum</taxon_name>
    <taxon_hierarchy>genus Ribes;species velutinum;variety glanduliferum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">velutinum</taxon_name>
    <taxon_name authority="(M. Peck) C. L. Hitchcock" date="unknown" rank="variety">gooddingii</taxon_name>
    <taxon_hierarchy>genus Ribes;species velutinum;variety gooddingii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–2 m.</text>
      <biological_entity id="o2671" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, (densely and intricately branched), glabrous or copiously pubescent when young;</text>
      <biological_entity id="o2672" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 1–3, 5–20 mm;</text>
      <biological_entity id="o2673" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2674" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <relation from="o2673" id="r211" name="at" negation="false" src="d0_s2" to="o2674" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes absent.</text>
      <biological_entity id="o2675" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o2676" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o2675" id="r212" name="on" negation="false" src="d0_s3" to="o2676" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 0.2–1.5 (–3.3) cm, pilose and glandular or stipitate-glandular;</text>
      <biological_entity id="o2677" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2678" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade nearly orbiculate to cordate or reniform, 3–5-lobed, cleft 1/3–1/2 (–3/4) to midrib, 0.5–2 cm, base broadly truncate to cordate, surfaces glabrous or finely pubescent and slightly glandular-puberulent, lobes cuneate, margins entire or 2–3-toothed, apex rounded.</text>
      <biological_entity id="o2679" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o2680" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="nearly orbiculate" name="shape" src="d0_s5" to="cordate or reniform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o2681" from="1/3" name="quantity" src="d0_s5" to="1/2-3/4" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2681" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o2682" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly truncate" name="shape" src="d0_s5" to="cordate" />
      </biological_entity>
      <biological_entity id="o2683" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s5" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o2684" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o2685" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-3-toothed" value_original="2-3-toothed" />
      </biological_entity>
      <biological_entity id="o2686" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences spreading, solitary flowers or 2 (–3) -flowered racemes, 0.5–1 cm (much shorter than leaves), axis pubescent, flowers evenly spaced.</text>
      <biological_entity id="o2687" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o2688" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2689" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="2(-3)-flowered" value_original="2(-3)-flowered" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2690" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2691" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels not jointed, 1–3 (–4) mm, glabrous, pubescent, or glandular-pubescent;</text>
      <biological_entity id="o2692" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate-ovate, 1–2 mm, pubescent.</text>
      <biological_entity id="o2693" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium whitish or yellowish, sometimes pink tinged, tubular to slightly campanulate, 1–2.5 (–2.8) mm, glabrous, pubescent, or stipitate-glandular and pubescent abaxially, glabrous adaxially, becoming indurate;</text>
      <biological_entity id="o2694" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2695" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="pink tinged" value_original="pink tinged" />
        <character char_type="range_value" from="tubular" name="shape" src="d0_s9" to="slightly campanulate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="indurate" value_original="indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals not overlapping, spreading to nearly erect, yellow to pinkish, oblong, 1–2 mm;</text>
      <biological_entity id="o2696" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2697" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="nearly erect" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals nearly connivent, erect, white or yellowish, elliptic-oblanceolate or oblong-obovate to spatulate, not conspicuously revolute or inrolled, 1.5–2.5 mm;</text>
      <biological_entity id="o2698" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2699" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s11" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s11" to="spatulate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc greenish or cream, raised, roundish, covering much of ovary;</text>
      <biological_entity id="o2700" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o2701" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
        <character is_modifier="false" name="prominence" src="d0_s12" value="raised" value_original="raised" />
        <character is_modifier="false" name="shape" src="d0_s12" value="roundish" value_original="roundish" />
        <character constraint="of ovary" constraintid="o2702" is_modifier="false" name="position_relational" src="d0_s12" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o2702" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens nearly as long as petals;</text>
      <biological_entity id="o2703" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2704" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o2705" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <relation from="o2704" id="r213" name="as long as" negation="false" src="d0_s13" to="o2705" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, 0.6–1.1 mm, glabrous;</text>
      <biological_entity id="o2706" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o2707" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers pale-yellow to light violet, oval, 0.5–1.2 mm, apex blunt or with punctate notch;</text>
      <biological_entity id="o2708" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o2709" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s15" to="light violet" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oval" value_original="oval" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2710" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s15" value="with punctate notch" value_original="with punctate notch" />
      </biological_entity>
      <biological_entity id="o2711" name="notch" name_original="notch" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration_or_relief" src="d0_s15" value="punctate" value_original="punctate" />
      </biological_entity>
      <relation from="o2710" id="r214" name="with" negation="false" src="d0_s15" to="o2711" />
    </statement>
    <statement id="d0_s16">
      <text>ovary usually densely crisped-puberulent and stipitate-glandular, rarely glabrous;</text>
      <biological_entity id="o2712" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o2713" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s16" value="crisped-puberulent" value_original="crisped-puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles completely connate, 3 mm, glabrous or finely pubescent.</text>
      <biological_entity id="o2714" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o2715" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="completely" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries palatable, yellow, becoming purple or dark reddish, globose, 4–9.5 mm, glabrous, sparsely to densely pubescent, or sparsely to densely stipitate-glandular pubescent.</text>
      <biological_entity id="o2716" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s18" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="shape" src="d0_s18" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s18" to="sparsely densely pubescent or" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s18" to="sparsely densely pubescent or" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s18" to="sparsely densely pubescent or" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush scrub, pinyon-juniper woodland, yellow pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="pinyon-juniper woodland" />
        <character name="habitat" value="yellow pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Idaho, Mont., Nev., Oreg., Utah, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Desert gooseberry</other_name>
  <discussion>The leaves of Ribes velutinum are thick and leathery.</discussion>
  
</bio:treatment>