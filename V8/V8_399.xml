<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="treatment_page">197</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">DIAMORPHA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 293. 1818, name conserved  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus DIAMORPHA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek diamorphe, contrary or different form, alluding to fruit compared with that of related genera</other_info_on_name>
    <other_info_on_name type="fna_id">109785</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, not viviparous, 0.2–1 dm, glabrous.</text>
      <biological_entity id="o20052" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s0" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branching, fleshy.</text>
      <biological_entity id="o20053" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, first crowded in winter rosette atop hypocotyl, remainder cauline, alternate, ± alike, not connate basally;</text>
      <biological_entity id="o20054" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character constraint="in rosette" constraintid="o20055" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s2" value="alike" value_original="alike" />
        <character is_modifier="false" modifier="not; basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o20055" name="rosette" name_original="rosette" src="d0_s2" type="structure">
        <character is_modifier="true" name="season" src="d0_s2" value="winter" value_original="winter" />
      </biological_entity>
      <biological_entity id="o20056" name="hypocotyl" name_original="hypocotyl" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o20057" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o20055" id="r1658" name="atop" negation="false" src="d0_s2" to="o20056" />
    </statement>
    <statement id="d0_s3">
      <text>petiole to 7 mm;</text>
      <biological_entity id="o20058" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong, subterete, 0.2–0.6 cm, succulent, base short-spurred, margins entire;</text>
      <biological_entity id="o20059" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s4" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o20060" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="short-spurred" value_original="short-spurred" />
      </biological_entity>
      <biological_entity id="o20061" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>veins not conspicuous.</text>
      <biological_entity id="o20062" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, lax, corymbose cymes (with uniparous branches).</text>
      <biological_entity id="o20063" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o20064" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o20065" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers erect, 4-merous;</text>
      <biological_entity id="o20066" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals connate basally, all alike;</text>
      <biological_entity id="o20067" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="alike" value_original="alike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, distinct, white or pinkish;</text>
      <biological_entity id="o20068" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o20069" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o20071" is_modifier="false" modifier="not" name="dehiscence" src="d0_s11" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o20070" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character constraint="in fruit" constraintid="o20071" is_modifier="false" modifier="not" name="dehiscence" src="d0_s11" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o20071" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>nectaries reniform;</text>
      <biological_entity id="o20072" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="reniform" value_original="reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2 times as many as sepals;</text>
      <biological_entity id="o20073" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character constraint="sepal" constraintid="o20074" is_modifier="false" name="size_or_quantity" src="d0_s13" value="2 times as many as sepals" />
      </biological_entity>
      <biological_entity id="o20074" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>filaments free;</text>
      <biological_entity id="o20075" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistils erect, connate 1/3–1/2 their lengths;</text>
      <biological_entity id="o20076" name="pistil" name_original="pistils" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/3" name="lengths" src="d0_s15" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary base rounded;</text>
      <biological_entity constraint="ovary" id="o20077" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 2+ times shorter than ovary.</text>
      <biological_entity id="o20078" name="style" name_original="styles" src="d0_s17" type="structure">
        <character constraint="ovary" constraintid="o20079" is_modifier="false" name="size_or_quantity" src="d0_s17" value="2+ times shorter than ovary" />
      </biological_entity>
      <biological_entity id="o20079" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Fruits cruciate capsules, erect, with spreading, centrally connate pistils, dehiscent by abaxial valves.</text>
      <biological_entity id="o20080" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s18" value="erect" value_original="erect" />
        <character constraint="by abaxial valves" constraintid="o20083" is_modifier="false" name="dehiscence" notes="" src="d0_s18" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o20081" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s18" value="cruciate" value_original="cruciate" />
      </biological_entity>
      <biological_entity id="o20082" name="pistil" name_original="pistils" src="d0_s18" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s18" value="spreading" value_original="spreading" />
        <character is_modifier="true" modifier="centrally" name="fusion" src="d0_s18" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20083" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <relation from="o20080" id="r1659" name="with" negation="false" src="d0_s18" to="o20082" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds subglobose-ovoid, finely granulate-striate.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 9.</text>
      <biological_entity id="o20084" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="subglobose-ovoid" value_original="subglobose-ovoid" />
        <character is_modifier="false" modifier="finely" name="coloration_or_pubescence_or_relief" src="d0_s19" value="granulate-striate" value_original="granulate-striate" />
      </biological_entity>
      <biological_entity constraint="x" id="o20085" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Elf orpine</other_name>
  <discussion>Species 1: se United States.</discussion>
  <discussion>Diamorpha is the only member of the family to have valved capsules instead of follicles that open along the ventral suture (rarely utricles), and it is notable also for its subglobose seeds. P. A. Sherwin and R. L. Wilbur (1971) worked out the floral anatomy of Diamorpha and of two species of Sedum sometimes thought related. They found Diamorpha remarkable for the solitary vascular strands of the sepals, the homocarpous adaxial carpellary traces, and the much-reduced abaxial carpellary bundle.</discussion>
  <discussion>Species 1</discussion>
  <references>
    <reference>McCormick, J. F. and R. B. Platt. 1964. Ecotypic differentiation in Diamorpha cymosa. Bot. Gaz. 125: 271–279.</reference>
    <reference>Sherwin, P. A. and R. L. Wilbur. 1971. The contributions of floral anatomy to the generic placement of Diamorpha smallii and Sedum pusillum. J. Elisha Mitchell Sci. Soc. 87: 103–114.</reference>
    <reference>Wiggs, D. N. and R. B. Platt. 1962. Ecology of Diamorpha cymosa. Ecology 43: 654–670.</reference>
    <reference>Wilbur, R. L. 1964. Notes on the genus Diamorpha (Crassulaceae). Rhodora 66: 87–92.</reference>
    <reference>Wilbur, R. L. 1977. Nomenclatural notes on the genus Diamorpha (Crassulaceae). Taxon 26: 137–139.</reference>
    <reference>Wyatt, R. 1981. Ant-pollination of the granite outcrop endemic Diamorpha smallii. Amer. J. Bot. 68: 1212–1217.</reference>
    <reference>Wyatt, R. and A. Stoneburner. 1981. Patterns of ant-mediated pollen dispersal in Diamorpha smallii (Crassulaceae). Syst. Bot. 6: 1–7.</reference>
  </references>
  
</bio:treatment>