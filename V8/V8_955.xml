<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wayne J. Elisens</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="treatment_page">489</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="D. Don" date="1826" rank="genus">COREMA</taxon_name>
    <place_of_publication>
      <publication_title>Edinburgh New Philos. J.</publication_title>
      <place_in_publication>2: 63. 1826  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus corema;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, korema, broom, alluding to growth form</other_info_on_name>
    <other_info_on_name type="fna_id">107996</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, (aromatic).</text>
      <biological_entity id="o24246" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (branches ascending);</text>
      <biological_entity id="o24248" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>old twigs rough with persistent leaf-bases, young twigs hairy.</text>
      <biological_entity id="o24249" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character constraint="with leaf-bases" constraintid="o24250" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o24250" name="leaf-base" name_original="leaf-bases" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o24251" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, opposite or whorled;</text>
      <biological_entity id="o24252" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o24253" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade coriaceous, margins entire.</text>
      <biological_entity id="o24254" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o24255" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal capitula (on current-year’s growth), 3–7 [–9] -flowered;</text>
      <biological_entity id="o24256" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="3-7[-9]-flowered" value_original="3-7[-9]-flowered" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o24257" name="capitulum" name_original="capitula" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>perulae absent.</text>
      <biological_entity id="o24258" name="perula" name_original="perulae" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual (dioecious), radially symmetric;</text>
      <biological_entity id="o24259" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4–5, distinct;</text>
      <biological_entity id="o24260" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals absent [3–4, distinct];</text>
      <biological_entity id="o24261" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens [2–] 3–4, exserted;</text>
      <biological_entity id="o24262" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers without awns, longitudinally dehiscent;</text>
      <biological_entity id="o24263" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" notes="" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o24264" name="awn" name_original="awns" src="d0_s12" type="structure" />
      <relation from="o24263" id="r2014" name="without" negation="false" src="d0_s12" to="o24264" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 3–5-locular;</text>
      <biological_entity id="o24265" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-5-locular" value_original="3-5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style exserted;</text>
      <biological_entity id="o24266" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma linear.</text>
      <biological_entity id="o24267" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits drupaceous, [white to pink] (gray), globose, dry [fleshy].</text>
      <biological_entity id="o24268" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s16" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds (pyrenes) 3–5, planoconvex, not winged, not tailed;</text>
      <biological_entity id="o24269" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="false" name="shape" src="d0_s17" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>testa smooth.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 13.</text>
      <biological_entity id="o24270" name="testa" name_original="testa" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o24271" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, sw Europe, Atlantic Islands (Azores).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="sw Europe" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Azores)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Broom crowberry</other_name>
  <other_name type="common_name">corème</other_name>
  <discussion>Tuckermania Klotzsch</discussion>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Corema was traditionally included in Empetraceae; phylogenetic studies based on morphological and molecular characters (A. A. Anderberg 1994c; K. A. Kron et al. 2002; Li J. H. et al. 2002) indicate that Empetraceae are nested within a monophyletic Ericaceae and that Ceratiola + Corema are in a lineage distinct from Empetrum. Previous authors recommended its inclusion within the Ericaceae based on comparative morphology and embryology (C. E. Wood Jr. and R. B. Channell 1959). Corema differs from Empetrum and Ceratiola by lacking petals and in having fewer seeds per fruit than Empetrum (six to nine), but more than Ceratiola (two). The capitate, terminal inflorescences (condensed racemes) of Corema differ from the axillary, solitary flowers of Empetrum and the axillary, three- to four-flowered, cymose clusters of Ceratiola. The genus has an amphi-Atlantic distribution; Corema album (Linnaeus) D. Don occurs in the Iberian peninsula and the Azores.</discussion>
  <references>
    <reference>Brown, S. 1913. Corema conradii. Bartonia 6: 1–7.</reference>
    <reference>Dunwoodie, P. W. 1990. Rare plants in coastal heathlands: Observations on Corema conradii (Empetraceae) and Helianthemum dumosum (Cistaceae). Rhodora 92: 22–26.</reference>
    <reference>Martine, C. T., D. Lubertazi, and A. DuBrul. 2005. The biology of Corema conradii: Natural history, reproduction, and observations of a post-fire seedling recruitment. N. E. Naturalist 12: 267–286.</reference>
  </references>
  
</bio:treatment>