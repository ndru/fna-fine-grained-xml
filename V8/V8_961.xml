<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="treatment_page">492</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ERICA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 352. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 167. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus erica;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ereiko, to break, alluding to brittle stems</other_info_on_name>
    <other_info_on_name type="fna_id">111987</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs [trees].</text>
      <biological_entity id="o6046" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, spreading, or creeping, (much-branched);</text>
      <biological_entity id="o6048" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs glabrous or hairy.</text>
      <biological_entity id="o6049" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, whorled;</text>
      <biological_entity id="o6050" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o6051" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade coriaceous, margins ciliate, prickled, or glabrous.</text>
      <biological_entity id="o6052" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o6053" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, umbels, racemes, or panicles, 10–30-flowered;</text>
      <biological_entity id="o6054" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o6055" name="umbel" name_original="umbels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-30-flowered" value_original="10-30-flowered" />
      </biological_entity>
      <biological_entity id="o6056" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-30-flowered" value_original="10-30-flowered" />
      </biological_entity>
      <biological_entity id="o6057" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-30-flowered" value_original="10-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perulae absent;</text>
    </statement>
    <statement id="d0_s8">
      <text>(bracteoles 2–3).</text>
      <biological_entity id="o6058" name="perula" name_original="perulae" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o6059" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4–5, distinct, (shorter than petals);</text>
      <biological_entity id="o6060" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4–5, connate nearly their entire lengths [connate ca. 1/2 their lengths], corolla persistent, campanulate;</text>
      <biological_entity id="o6061" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="lengths" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6062" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10, included or exserted;</text>
    </statement>
    <statement id="d0_s13">
      <text>(filaments glabrous);</text>
      <biological_entity id="o6063" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers with or without awns, dehiscent through narrowly oblong, subterminal pores;</text>
      <biological_entity id="o6064" name="anther" name_original="anthers" src="d0_s14" type="structure" />
      <biological_entity id="o6065" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="through narrowly" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s14" value="subterminal" value_original="subterminal" />
      </biological_entity>
      <biological_entity id="o6066" name="pore" name_original="pores" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="through narrowly" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s14" value="subterminal" value_original="subterminal" />
      </biological_entity>
      <relation from="o6064" id="r496" name="with or without" negation="false" src="d0_s14" to="o6065" />
      <relation from="o6064" id="r497" name="with or without" negation="false" src="d0_s14" to="o6066" />
    </statement>
    <statement id="d0_s15">
      <text>ovary pseudo-10-locular;</text>
      <biological_entity id="o6067" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="pseudo-10-locular" value_original="pseudo-10-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style (slender, straight), included or exserted, (glabrous);</text>
      <biological_entity id="o6068" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma filiform, obconic, or capitate.</text>
      <biological_entity id="o6069" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsular [drupaceous], ellipsoid, dehiscence loculicidal [indehiscent].</text>
      <biological_entity id="o6070" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="dehiscence" src="d0_s18" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ca. 10, ellipsoid to obovoid, not winged, not tailed;</text>
      <biological_entity id="o6071" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="10" value_original="10" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s19" to="obovoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s19" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s19" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>testa reticulate or foveolate.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 12.</text>
      <biological_entity id="o6072" name="testa" name_original="testa" src="d0_s20" type="structure">
        <character is_modifier="false" name="relief" src="d0_s20" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="relief" src="d0_s20" value="foveolate" value_original="foveolate" />
      </biological_entity>
      <biological_entity constraint="x" id="o6073" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="common_name">Heath</other_name>
  <discussion>Species ca. 860 (4 in the flora).</discussion>
  <discussion>All of the naturalized species of Erica, as well as some others and hybrids, are cultivated, especially in the northeastern and northwestern coastal areas (D. Metheny 1991). Most require acid soils, although E. carnea, E. vagans, and E. ×darleyensis (E. carnea × E. erigena) will accept neutral soils (A. Mikolajski 1997). Over 700 species are endemic to the Cape region of southern Africa; recent molecular phylogenetic studies suggest that the genus originated in Europe, and that the southern African species represent radiation from an ancestor of E. arborea (A. F. McGuire and K. A. Kron 2005).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels 5-8 mm; corollas 2.5-3.5 mm; anthers without awns; petioles 0.5-1 mm.</description>
      <determination>1 Erica vagans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels 1-3 mm; corollas 4-6 mm; anthers with awns; petioles 0.1-0.3 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigmas obconic, exserted; calyx lobes ovate</description>
      <determination>2 Erica lusitanica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stigmas capitate, not (or slightly) exserted; calyx lobes deltate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves in whorls of 3, surfaces glabrous, but with marginal prickles; ovaries glabrous; corolla lobes rounded apically.</description>
      <determination>3 Erica cinerea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves in whorls of 4, surfaces with scattered hairs and glands; ovaries hairy; corolla lobes acute apically.</description>
      <determination>4 Erica tetralix</determination>
    </key_statement>
  </key>
</bio:treatment>