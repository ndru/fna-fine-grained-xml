<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle" date="unknown" rank="family">grossulariaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ribes</taxon_name>
    <taxon_name authority="McClatchie" date="1897" rank="species">montigenum</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>5: 38. 1897  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grossulariaceae;genus ribes;species montigenum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065825</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ribes</taxon_name>
    <taxon_name authority="McClatchie" date="unknown" rank="species">nubigenum</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 80. 1894,</place_in_publication>
      <other_info_on_pub>not Philippi 1856</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Ribes;species nubigenum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.7–1.5 m.</text>
      <biological_entity id="o26285" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.7" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading or decumbent, copiously pubescent, puberulent, and stipitate-glandular;</text>
      <biological_entity id="o26286" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="copiously" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>spines at nodes 1–5, (1.5–) 4–6 (–10) mm;</text>
      <biological_entity id="o26287" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26288" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
      </biological_entity>
      <relation from="o26287" id="r2206" name="at" negation="false" src="d0_s2" to="o26288" />
    </statement>
    <statement id="d0_s3">
      <text>prickles on internodes sparse to dense.</text>
      <biological_entity id="o26289" name="prickle" name_original="prickles" src="d0_s3" type="structure" />
      <biological_entity id="o26290" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="dense" />
      </biological_entity>
      <relation from="o26289" id="r2207" name="on" negation="false" src="d0_s3" to="o26290" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 0.7–4 (–5) cm, pubescent, stipitate-glandular;</text>
      <biological_entity id="o26291" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26292" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade pentagonal, irregularly 5-lobed, cleft 2/3–3/4 to midrib, (0.5–) 1–3.5 (–4) cm, base cordate, surfaces densely pubescent or stipitate-glandular, lobes cuneate-rounded, margins irregularly serrate, toothed apex somewhat acute.</text>
      <biological_entity id="o26293" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o26294" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o26295" from="2/3" name="quantity" src="d0_s5" to="3/4" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26295" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <biological_entity id="o26296" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o26297" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26298" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate-rounded" value_original="cuneate-rounded" />
      </biological_entity>
      <biological_entity id="o26299" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o26300" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences pendent, 3–8 (–11) -flowered racemes, 2–3 cm, axis puberulent, stipitate-glandular, flowers evenly spaced.</text>
      <biological_entity id="o26301" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="pendent" value_original="pendent" />
      </biological_entity>
      <biological_entity id="o26302" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="3-8(-11)-flowered" value_original="3-8(-11)-flowered" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26303" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o26304" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels jointed, 1–4 (–5) mm, puberulent, stipitate-glandular;</text>
      <biological_entity id="o26305" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate-ovate, 1.3–3 mm, puberulent, stipitate-glandular.</text>
      <biological_entity id="o26306" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium pinkish to orangish, saucer-shaped, 0.5–1.5 mm, pubescent and stipitate-glandular abaxially, glabrous adaxially;</text>
      <biological_entity id="o26307" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26308" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s9" to="orangish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals separated, spreading, green to yellowish, pink, red, orange, or white, sometimes with pale-yellow, scarious margins, broadly ovate to obovate, 2.5–4 mm;</text>
      <biological_entity id="o26309" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26310" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="separated" value_original="separated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellowish pink red orange or white" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellowish pink red orange or white" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellowish pink red orange or white" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellowish pink red orange or white" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="yellowish pink red orange or white" />
        <character char_type="range_value" from="broadly ovate" name="shape" notes="" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26311" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="true" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o26310" id="r2208" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o26311" />
    </statement>
    <statement id="d0_s11">
      <text>petals widely separated, erect, red, pinkish, or purplish, cuneate-lunate, not conspicuously revolute or inrolled, 0.9–1.5 mm;</text>
      <biological_entity id="o26312" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26313" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s11" value="separated" value_original="separated" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cuneate-lunate" value_original="cuneate-lunate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s11" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc yellowish, pinkish, or red, flat, 5-angled, covering most of ovary;</text>
      <biological_entity id="o26314" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o26315" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s12" value="5-angled" value_original="5-angled" />
        <character constraint="of ovary" constraintid="o26316" is_modifier="false" name="position_relational" src="d0_s12" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o26316" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens as long as petals;</text>
      <biological_entity id="o26317" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o26318" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o26319" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <relation from="o26318" id="r2209" name="as long as" negation="false" src="d0_s13" to="o26319" />
    </statement>
    <statement id="d0_s14">
      <text>filaments linear, (0.5–) 0.9–1.6 mm, glabrous;</text>
      <biological_entity id="o26320" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o26321" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellow or cream, oblate to transversely elliptic, 0.5–0.8 mm, broader than long, apex notched;</text>
      <biological_entity id="o26322" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o26323" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="cream" value_original="cream" />
        <character char_type="range_value" from="oblate" name="shape" src="d0_s15" to="transversely elliptic" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s15" value="broader than long" value_original="broader than long" />
      </biological_entity>
      <biological_entity id="o26324" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary sparsely to thickly, usually purplish glandular-bristly;</text>
      <biological_entity id="o26325" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o26326" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="thickly; usually" name="coloration" src="d0_s16" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glandular-bristly" value_original="glandular-bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles connate ca. 4/5 their lengths, 1.1–1.8 mm, glabrous.</text>
      <biological_entity id="o26327" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o26328" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s17" value="4/5" value_original="4/5" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Berries somewhat palatable, bright red, obovoid-spheric, 5–10 mm, glandular-bristly.</text>
      <biological_entity id="o26329" name="berry" name_original="berries" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="bright red" value_original="bright red" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid-spheric" value_original="obovoid-spheric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glandular-bristly" value_original="glandular-bristly" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed ridges, open woods and slopes, talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="exposed ridges" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-4800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4800" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Alpine prickly currant</other_name>
  <other_name type="common_name">mountain gooseberry</other_name>
  <discussion>The lobed, yellowish, pinkish, or red nectary discs and purplish red filaments of Ribes montigenum are striking.</discussion>
  
</bio:treatment>