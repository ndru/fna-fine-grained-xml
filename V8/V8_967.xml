<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">495</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Kron &amp; Judd" date="2002" rank="subfamily">Harrimanelloideae</taxon_name>
    <taxon_name authority="Coville" date="1901" rank="genus">harrimanella</taxon_name>
    <taxon_name authority="(Pallas) Coville" date="1901" rank="species">stelleriana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Wash. Acad. Sci.</publication_title>
      <place_in_publication>3: 574. 1901  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily harrimanelloideae;genus harrimanella;species stelleriana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250065682</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andromeda</taxon_name>
    <taxon_name authority="Pallas" date="unknown" rank="species">stelleriana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Ross.</publication_title>
      <place_in_publication>1(2): 58. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Andromeda;species stelleriana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cassiope</taxon_name>
    <taxon_name authority="(Pallas) de Candolle" date="unknown" rank="species">stelleriana</taxon_name>
    <taxon_hierarchy>genus Cassiope;species stelleriana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems forming dense, soft mats;</text>
      <biological_entity id="o23232" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o23233" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
      </biological_entity>
      <relation from="o23232" id="r1933" name="forming" negation="false" src="d0_s0" to="o23233" />
    </statement>
    <statement id="d0_s1">
      <text>branches lax, tips ascending.</text>
      <biological_entity id="o23234" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o23235" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves divergent, not imbricate;</text>
      <biological_entity id="o23236" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear-oblong, 3–5 × 1–2 mm, apex obtuse to broadly acute.</text>
      <biological_entity id="o23237" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblong" value_original="linear-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23238" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="broadly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels to 1 cm, barely exceeding leaves at anthesis.</text>
      <biological_entity id="o23239" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23240" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <relation from="o23239" id="r1934" modifier="barely" name="exceeding" negation="false" src="d0_s4" to="o23240" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers erect to horizontal;</text>
      <biological_entity id="o23241" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals oblong-ovate, ca. 3 mm;</text>
      <biological_entity id="o23242" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong-ovate" value_original="oblong-ovate" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla white or tinged with pink, ca. 6 × 5 mm, lobes distinct for ca. 1/2+ their lengths, tips not recurved;</text>
      <biological_entity id="o23243" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="tinged with pink" value_original="tinged with pink" />
        <character name="length" src="d0_s7" unit="mm" value="6" value_original="6" />
        <character name="width" src="d0_s7" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o23244" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character constraint="for 1/2+ their lengths" is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o23245" name="tip" name_original="tips" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens to 3 mm.</text>
      <biological_entity id="o23246" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4–5 mm diam.</text>
      <biological_entity id="o23247" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, rocky slopes, subalpine or alpine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska, Wash.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Alaska moss heather</other_name>
  
</bio:treatment>