<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>George M. Diggs Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">397</other_info_on_meta>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Niedenzu" date="1889" rank="subfamily">Arbutoideae</taxon_name>
    <taxon_name authority="Small in N. L. Britton et al." date="1914" rank="genus">ORNITHOSTAPHYLOS</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al.,  N. Amer. Fl.</publication_title>
      <place_in_publication>29: 101. 1914  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily arbutoideae;genus ornithostaphylos;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ornithos, bird, and staphyle, cluster of grapes, allusion obscure</other_info_on_name>
    <other_info_on_name type="fna_id">123206</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, burled;</text>
      <biological_entity id="o16602" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark sometimes reddish purple when young, thin, sometimes peeling.</text>
      <biological_entity id="o16603" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, rigidly branched, minutely tomentose.</text>
      <biological_entity id="o16604" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rigidly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite or whorled, bifacial;</text>
      <biological_entity id="o16605" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bifacial" value_original="bifacial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to linear-oblong or narrowly lanceolate, coriaceous, margins entire, revolute, abaxial surface minutely tomentose, adaxial glabrous.</text>
      <biological_entity id="o16606" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong or narrowly lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o16607" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16608" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16609" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences panicles, 10–20-flowered.</text>
      <biological_entity constraint="inflorescences" id="o16610" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="10-20-flowered" value_original="10-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual;</text>
      <biological_entity id="o16611" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals persistent, (4–) 5, connate to 1/2 their lengths, triangular;</text>
      <biological_entity id="o16612" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s7" to="1/2" />
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (4–) 5, connate 2/3–3/4 their lengths, white, corolla nearly globose to urceolate (sometimes widest proximally);</text>
      <biological_entity id="o16613" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="2/3" name="lengths" src="d0_s8" to="3/4" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o16614" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" from="nearly globose" name="shape" src="d0_s8" to="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 8–10, included, (equal);</text>
      <biological_entity id="o16615" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="10" />
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments dilated near base;</text>
      <biological_entity id="o16616" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="near base" constraintid="o16617" is_modifier="false" name="shape" src="d0_s10" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o16617" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers with awns, dehiscent by 2 slits;</text>
      <biological_entity id="o16618" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character constraint="by slits" constraintid="o16620" is_modifier="false" name="dehiscence" notes="" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o16619" name="awn" name_original="awns" src="d0_s11" type="structure" />
      <biological_entity id="o16620" name="slit" name_original="slits" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o16618" id="r1414" name="with" negation="false" src="d0_s11" to="o16619" />
    </statement>
    <statement id="d0_s12">
      <text>ovary 5-locular;</text>
      <biological_entity id="o16621" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="5-locular" value_original="5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma slightly capitate.</text>
      <biological_entity id="o16622" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Drupes brown or reddish-brown, globose or nearly so, dry, smooth;</text>
      <biological_entity id="o16623" name="drupe" name_original="drupes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character name="shape" src="d0_s14" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s14" value="dry" value_original="dry" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 5, connate into solid, globose endocarp.</text>
      <biological_entity id="o16624" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character constraint="into endocarp" constraintid="o16625" is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o16625" name="endocarp" name_original="endocarp" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="solid" value_original="solid" />
        <character is_modifier="true" name="shape" src="d0_s15" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 10, connate.</text>
      <biological_entity id="o16626" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="s Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Baja California or Baja birdbush</other_name>
  <discussion>Species 1: s California, nw Mexico.</discussion>
  <discussion>Species 1</discussion>
  
</bio:treatment>