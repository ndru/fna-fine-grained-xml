<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle &amp; Sprengel" date="unknown" rank="family">styracaceae</taxon_name>
    <taxon_name authority="J. Ellis ex Linnaeus" date="1759" rank="genus">HALESIA</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1041, 1044, 1369. 1759, name conserved  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family styracaceae;genus HALESIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Stephen Hales, 1677–1761, English botanist</other_info_on_name>
    <other_info_on_name type="fna_id">114485</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees: pith chambered [continuous];</text>
      <biological_entity id="o26881" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o26883" name="pith" name_original="pith" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="chambered" value_original="chambered" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>winter buds with scales;</text>
      <biological_entity id="o26884" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o26886" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="true" name="season" src="d0_s1" value="winter" value_original="winter" />
      </biological_entity>
      <biological_entity id="o26887" name="scale" name_original="scales" src="d0_s1" type="structure" />
      <relation from="o26886" id="r2247" name="with" negation="false" src="d0_s1" to="o26887" />
    </statement>
    <statement id="d0_s2">
      <text>fertile shoots of current growing season without fully developed leaves (rarely fully developed in H. diptera).</text>
      <biological_entity id="o26888" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o26890" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o26891" name="season" name_original="season" src="d0_s2" type="structure" constraint="current" />
      <biological_entity id="o26892" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="fully" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o26890" id="r2248" name="part_of" negation="false" src="d0_s2" to="o26891" />
      <relation from="o26890" id="r2249" name="without" negation="false" src="d0_s2" to="o26892" />
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades: margins serrate.</text>
      <biological_entity id="o26893" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure" />
      <biological_entity id="o26894" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences borne on shoots of previous growing season, contracted axillary racemes, usually (2–) 3–6-flowered, sometimes solitary flowers, articulation between pedicel and flower present.</text>
      <biological_entity id="o26895" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26896" name="shoot" name_original="shoots" src="d0_s4" type="structure" />
      <biological_entity constraint="axillary" id="o26897" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s4" value="previous" value_original="previous" />
        <character is_modifier="true" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o26898" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s4" value="previous" value_original="previous" />
        <character is_modifier="true" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="(2-)3-6-flowered" value_original="(2-)3-6-flowered" />
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o26899" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s4" value="previous" value_original="previous" />
        <character is_modifier="true" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="(2-)3-6-flowered" value_original="(2-)3-6-flowered" />
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o26900" name="pedicel" name_original="pedicel" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s4" value="previous" value_original="previous" />
        <character is_modifier="true" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="(2-)3-6-flowered" value_original="(2-)3-6-flowered" />
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o26901" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_order" src="d0_s4" value="previous" value_original="previous" />
        <character is_modifier="true" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s4" value="(2-)3-6-flowered" value_original="(2-)3-6-flowered" />
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o26895" id="r2250" name="borne on" negation="false" src="d0_s4" to="o26896" />
      <relation from="o26895" id="r2251" name="borne on" negation="false" src="d0_s4" to="o26897" />
      <relation from="o26895" id="r2252" name="borne on" negation="false" src="d0_s4" to="o26898" />
      <relation from="o26895" id="r2253" name="borne on" negation="false" src="d0_s4" to="o26899" />
      <relation from="o26895" id="r2254" name="borne on" negation="false" src="d0_s4" to="o26900" />
      <relation from="o26895" id="r2255" name="borne on" negation="false" src="d0_s4" to="o26901" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: hypanthium completely adnate to ovary wall;</text>
      <biological_entity id="o26902" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26903" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character constraint="to ovary wall" constraintid="o26904" is_modifier="false" modifier="completely" name="fusion" src="d0_s5" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o26904" name="wall" name_original="wall" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sepals 4, distinct;</text>
      <biological_entity id="o26905" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o26906" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla lobes 4, imbricate in bud, portion free from androecium distinct or connate proximally;</text>
      <biological_entity id="o26907" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="corolla" id="o26908" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character constraint="in bud" constraintid="o26909" is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o26909" name="bud" name_original="bud" src="d0_s7" type="structure" />
      <biological_entity id="o26910" name="portion" name_original="portion" src="d0_s7" type="structure">
        <character constraint="from androecium" constraintid="o26911" is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o26911" name="androecium" name_original="androecium" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 7–16;</text>
      <biological_entity id="o26912" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26913" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments with free portion connate proximally;</text>
      <biological_entity id="o26914" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26915" name="filament" name_original="filaments" src="d0_s9" type="structure" />
      <biological_entity id="o26916" name="portion" name_original="portion" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o26915" id="r2256" name="with" negation="false" src="d0_s9" to="o26916" />
    </statement>
    <statement id="d0_s10">
      <text>pistil 2–4-carpellate;</text>
      <biological_entity id="o26917" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26918" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-4-carpellate" value_original="2-4-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary inferior, proximally 2–4-septate;</text>
      <biological_entity id="o26919" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26920" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="inferior" value_original="inferior" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s11" value="2-4-septate" value_original="2-4-septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 4 per carpel, 2 proximal pendulous, 2 distal erect, unitegmic.</text>
      <biological_entity id="o26921" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26922" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character constraint="per carpel" constraintid="o26923" name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s12" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="pendulous" value_original="pendulous" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="position_or_shape" src="d0_s12" value="distal" value_original="distal" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="unitegmic" value_original="unitegmic" />
      </biological_entity>
      <biological_entity id="o26923" name="carpel" name_original="carpel" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits nutlike, with 2 or 4 longitudinal, corky wings, oblong, ellipsoid, or clavate;</text>
      <biological_entity id="o26924" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity id="o26925" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s13" value="corky" value_original="corky" />
      </biological_entity>
      <relation from="o26924" id="r2257" name="with" negation="false" src="d0_s13" to="o26925" />
    </statement>
    <statement id="d0_s14">
      <text>beak distinct.</text>
      <biological_entity id="o26926" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–4, not filling fruit cavity, not grooved, fusiform;</text>
      <biological_entity id="o26927" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="4" />
        <character is_modifier="false" modifier="not; not" name="architecture" src="d0_s15" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o26928" name="cavity" name_original="cavity" src="d0_s15" type="structure" />
      <relation from="o26927" id="r2258" name="filling" negation="true" src="d0_s15" to="o26928" />
    </statement>
    <statement id="d0_s16">
      <text>hilum inconspicuous;</text>
      <biological_entity id="o26929" name="hilum" name_original="hilum" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>seed-coat thin.</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 12.</text>
      <biological_entity id="o26930" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="x" id="o26931" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e United States, Asia (China); warm-temperate areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e United States" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="warm-temperate areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Silverbell</other_name>
  <other_name type="common_name">snowdrop tree</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Combined morphological and molecular data support a clade of Halesia, Melliodendron, Parastyrax, Pterostyrax, and Rehderodendron (P. W. Fritsch et al. 2001). All of these genera share the same type of ovary architecture, with two apotropous distal ovules and two epitropous proximal ovules per carpel, as well as similarities in fruit and seed morphology. The genus is unusual for an eastern Asian-eastern North American disjunct in that more species occur in North America than in Asia. A morphological phylogenetic estimate of Halesia places the two North American taxa as the sister group of the Asian species H. macgregorii Chun. In contrast, DNA sequence data place H. macgregorii as sister to Rehderodendron and the two United States species as unresolved as to their closest relative (Fritsch et al.).</discussion>
  <references>
    <reference>Chester, E. W. 1966. A Biosystematic Study of the Genus Halesia Ellis (Styracaceae). Ph.D. dissertation. University of Tennessee.</reference>
    <reference>Godfrey, R. K. 1958b. Some identities in Halesia (Styracaceae). Rhodora 60: 86–88.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas glabrous, tube 7-26 mm; stamens 12-16; filaments adnate to corolla 2-3 mm; styles glabrous; fruits 4-winged, wings roughly equal.</description>
      <determination>1 Halesia carolina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas hairy, tube to 1 mm; stamens 7-10; filaments adnate to corolla to 1 mm; styles hairy; fruits 2- or 4-winged, if 4-winged then 2 broad wings alternate with 2 much narrower wings.</description>
      <determination>2 Halesia diptera</determination>
    </key_statement>
  </key>
</bio:treatment>