<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="mention_page">184</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="Rose &amp; Davidson" date="1923" rank="species">parva</taxon_name>
    <place_of_publication>
      <publication_title>Bull. S. Calif. Acad. Sci.</publication_title>
      <place_in_publication>22: 5. 1923  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species parva;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092078</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">abramsii</taxon_name>
    <taxon_name authority="(Rose &amp; Davidson) Bartel" date="unknown" rank="subspecies">parva</taxon_name>
    <taxon_hierarchy>genus Dudleya;species abramsii;subspecies parva;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices often branching apically, 3–5 × 0.1–0.7 cm, (from root ca. 3 mm thick but strongly constricted irregularly), axillary branches absent.</text>
      <biological_entity id="o11280" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often; apically" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s0" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o11281" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves withering in early summer;</text>
      <biological_entity id="o11282" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="in early summer" is_modifier="false" name="life_cycle" src="d0_s1" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rosettes 1–3, (in clumps), 5–10-leaved, 1–3 cm diam.;</text>
      <biological_entity id="o11283" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-10-leaved" value_original="5-10-leaved" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade green, becoming purplish, linear to oblanceolate, 1.5–5 × 0.3–0.7 cm, 1.5–2 mm thick, base 0.5–1.1 cm wide, apex acute, subapiculate, surfaces not farinose, slightly glaucous.</text>
      <biological_entity id="o11284" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="oblanceolate" />
        <character name="thickness" src="d0_s3" unit="cm" value="1.5-5×0.3-0.7" value_original="1.5-5×0.3-0.7" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11285" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11286" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="subapiculate" value_original="subapiculate" />
      </biological_entity>
      <biological_entity id="o11287" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="farinose" value_original="farinose" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: cymes 2–3-branched, obpyramidal;</text>
      <biological_entity id="o11288" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o11289" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-3-branched" value_original="2-3-branched" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obpyramidal" value_original="obpyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches not twisted (flowers on topside), simple or 1 times bifurcate, (3–5 cm wide);</text>
      <biological_entity id="o11290" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o11291" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s5" value="1 times bifurcate" value_original="1 times bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cincinni mostly 1–2, (simple), 5–12-flowered, not circinate, 3–8 cm;</text>
      <biological_entity id="o11292" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o11293" name="cincinnus" name_original="cincinni" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-12-flowered" value_original="5-12-flowered" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s6" value="circinate" value_original="circinate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral shoots 5–18 × 0.1–0.2 cm;</text>
      <biological_entity id="o11294" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o11295" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="18" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s7" to="0.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaves 10–20, ascending, triangular-ovate, 5–15 × 3–6 mm, apex acute.</text>
      <biological_entity id="o11296" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o11297" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="20" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11298" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, not bent in fruit, (stout), 1–3 mm.</text>
      <biological_entity id="o11299" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o11300" is_modifier="false" modifier="not" name="shape" src="d0_s9" value="bent" value_original="bent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11300" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx 3–5 × 3–5 mm;</text>
      <biological_entity id="o11301" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11302" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals connate 1–2 mm, pale-yellow, sometimes red-lineolate, 8–12 × 2–3.5 mm, apex broadly acute, tips slightly outcurved;</text>
      <biological_entity id="o11303" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11304" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_pubescence_or_relief" src="d0_s11" value="red-lineolate" value_original="red-lineolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11305" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11306" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="outcurved" value_original="outcurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistils connivent, erect.</text>
      <biological_entity id="o11307" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11308" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Unripe follicles erect.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o11309" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="unripe" value_original="unripe" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11310" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly clay soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly clay soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Conejo or Diablo range dudleya</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Dudleya parva is known only from Ventura County; it is considered fairly threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi). Plants reported as D. parva from San Luis Obispo County (R. F. Hoover 1965, 1970) appear to be a small form of D. abramsii subsp. bettinae.</discussion>
  <discussion>Dudleya parva is distinguished by its small size and its fugacious rosette leaves. Mature plants look like seedlings flowering precociously. They are leafless in summer and fall, like D. cymosa subsp. marcescens and like members of subg. Hasseanthus. Flower structure, and particularly the insertion of the stamens, indicates that it is related to D. abramsii. The epipetalous filaments of D. parva are 4–9.5 mm and adnate 1–1.5 mm, the antisepalous ones are 5–10 mm, adnate 1.5–2 mm. Dudleya abramsii is a larger plant, with persistent rosettes of more numerous and usually larger leaves, with longer and more slender pedicels, and usually with a longer corolla tube.</discussion>
  <discussion>Dudleya parva is in the Center for Plant Conservation’s National Collection of Endangered Plants as D. abramsii subsp. parva.</discussion>
  
</bio:treatment>