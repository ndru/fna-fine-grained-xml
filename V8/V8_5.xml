<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Nancy R. Morin</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="treatment_page">6</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Agardh" date="unknown" rank="family">ITEACEAE</taxon_name>
    <taxon_hierarchy>family ITEACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">20157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [subshrubs, trees, vines], evergreen to semievergreen [deciduous].</text>
      <biological_entity id="o16373" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="evergreen" name="duration" src="d0_s0" to="semievergreen" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, simple;</text>
      <biological_entity id="o16374" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules present;</text>
      <biological_entity id="o16375" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o16376" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins unlobed, glandular-serrate [entire].</text>
      <biological_entity constraint="blade" id="o16377" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="glandular-serrate" value_original="glandular-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal or axillary racemes [panicles].</text>
      <biological_entity id="o16378" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o16379" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual;</text>
      <biological_entity id="o16380" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth and androecium nearly hypogenous [3/4 epigynous];</text>
      <biological_entity id="o16381" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o16382" name="androecium" name_original="androecium" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium adnate to ovary at base, free distally;</text>
      <biological_entity id="o16383" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character constraint="to ovary" constraintid="o16384" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="distally" name="fusion" notes="" src="d0_s8" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o16384" name="ovary" name_original="ovary" src="d0_s8" type="structure" />
      <biological_entity id="o16385" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o16384" id="r1394" name="at" negation="false" src="d0_s8" to="o16385" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 5, connate proximally;</text>
      <biological_entity id="o16386" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, distinct;</text>
      <biological_entity id="o16387" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>nectary disc present;</text>
      <biological_entity constraint="nectary" id="o16388" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 5, antisepalous, inserted at nectary margin, free, distinct;</text>
      <biological_entity id="o16389" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s12" value="antisepalous" value_original="antisepalous" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="nectary" id="o16390" name="margin" name_original="margin" src="d0_s12" type="structure" />
      <relation from="o16389" id="r1395" name="inserted at" negation="false" src="d0_s12" to="o16390" />
    </statement>
    <statement id="d0_s13">
      <text>anthers introrsely dehiscent by longitudinal slits;</text>
      <biological_entity id="o16391" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="by slits" constraintid="o16392" is_modifier="false" modifier="introrsely" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o16392" name="slit" name_original="slits" src="d0_s13" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s13" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistils 1, 2-carpellate;</text>
      <biological_entity id="o16393" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary nearly superior [to 3/4 inferior], 2-locular;</text>
    </statement>
    <statement id="d0_s16">
      <text>placentation axile;</text>
      <biological_entity id="o16394" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s15" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s16" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules anatropous, bitegmic, crassinucellate;</text>
      <biological_entity id="o16395" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="crassinucellate" value_original="crassinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 2, distinct or connate apically;</text>
      <biological_entity id="o16396" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="apically" name="fusion" src="d0_s18" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 2, terminal, capitate.</text>
      <biological_entity id="o16397" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s19" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits capsular, dehiscence septicidal.</text>
      <biological_entity id="o16398" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s20" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds 20–30, dark-brown or yellowish-brown, narrowly fusiform or ovoid;</text>
      <biological_entity id="o16399" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s21" to="30" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s21" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endosperm sparse, fleshy.</text>
      <biological_entity id="o16400" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s22" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="texture" src="d0_s22" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e United States, e Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e United States" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Sweetspire Family</other_name>
  <discussion>Genus 1, species 29 (1 in the flora).</discussion>
  <discussion>Iteaceae, which has been included in Grossulariaceae (A. Cronquist 1981) or Escalloniaceae (R. F. Thorne 1992b), has a disjunct distribution, with Itea virginica in southeastern North America and other species in southeastern Africa and southeastern Asia. Molecular data place it sister to the Mexican genus Pterostemon S. Schauer within the core Saxifragales (M. Fishbein et al. 2001).</discussion>
  <references>
    <reference>Kubitzki, K. 2007. Iteaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 9+ vols. Berlin etc. Vol. 9, pp. 202–204.</reference>
  </references>
  
</bio:treatment>