<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="treatment_page">358</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">sarraceniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sarracenia</taxon_name>
    <taxon_name authority="Wherry" date="1933" rank="species">oreophila</taxon_name>
    <place_of_publication>
      <publication_title>Bartonia</publication_title>
      <place_in_publication>15: 8, plate 1, figs. 2, 3. 1933  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sarraceniaceae;genus sarracenia;species oreophila</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092286</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming moderate clumps;</text>
      <biological_entity id="o22344" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22345" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="moderate" value_original="moderate" />
      </biological_entity>
      <relation from="o22344" id="r1856" name="forming" negation="false" src="d0_s0" to="o22345" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1–1.5 cm diam.</text>
      <biological_entity id="o22346" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s1" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pitchers marcescent, withering by mid summer, appearing with or just prior to flowers in 1 flush, erect, green to yellowish green, sometimes purple-veined or suffused with purple, without white areolae, 18–75 cm, firm, surfaces glabrous, wings 0.5–1 cm wide;</text>
      <biological_entity id="o22347" name="pitcher" name_original="pitchers" src="d0_s2" type="structure">
        <character is_modifier="false" name="condition" src="d0_s2" value="marcescent" value_original="marcescent" />
        <character constraint="by mid summer" is_modifier="false" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character is_modifier="false" modifier="in 1 flush" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="yellowish green" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="purple-veined" value_original="purple-veined" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="suffused with purple" value_original="suffused with purple" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="75" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o22348" name="prior-to-flower" name_original="prior-to-flowers" src="d0_s2" type="structure" />
      <biological_entity id="o22349" name="areola" name_original="areolae" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o22350" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22351" name="wing" name_original="wings" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o22348" id="r1857" name="appearing with" negation="false" src="d0_s2" to="o22348" />
      <relation from="o22347" id="r1858" name="without" negation="false" src="d0_s2" to="o22349" />
    </statement>
    <statement id="d0_s3">
      <text>orifice oval, 2–4.5 cm diam., rim green to red, flared and loosely revolute, often with slight indentation immediately distal to wing;</text>
      <biological_entity id="o22352" name="orifice" name_original="orifice" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oval" value_original="oval" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s3" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22353" name="rim" name_original="rim" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="red" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flared" value_original="flared" />
        <character is_modifier="false" modifier="loosely" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o22354" name="indentation" name_original="indentation" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="slight" value_original="slight" />
        <character constraint="to wing" constraintid="o22355" is_modifier="false" modifier="immediately" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o22355" name="wing" name_original="wing" src="d0_s3" type="structure" />
      <relation from="o22353" id="r1859" modifier="often" name="with" negation="false" src="d0_s3" to="o22354" />
    </statement>
    <statement id="d0_s4">
      <text>hood recurved adaxially, held well beyond and covering orifice, yellow-green, infrequently suffused with purple, or purple reticulate-veined or purple-spotted at neck, without white areolae, broadly ovate-reniform, somewhat undulate, 2–8 × 2.5–8 cm, ± as long as wide, proximal margins weakly cordate such that opposite lobes reflex abaxially, not touching, neck (rarely red-blotched), constricted, 1–2 cm, margins revolute, apiculum 1–2 mm, adaxial surface with hairs to 0.5 mm.</text>
      <biological_entity id="o22356" name="hood" name_original="hood" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="adaxially" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s4" value="suffused with purple" value_original="suffused with purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple reticulate-veined" value_original="purple reticulate-veined" />
        <character constraint="at neck" constraintid="o22358" is_modifier="false" name="coloration" src="d0_s4" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s4" value="ovate-reniform" value_original="ovate-reniform" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22357" name="orifice" name_original="orifice" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s4" value="covering" value_original="covering" />
      </biological_entity>
      <biological_entity id="o22358" name="neck" name_original="neck" src="d0_s4" type="structure" />
      <biological_entity id="o22359" name="areola" name_original="areolae" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22360" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="weakly; such" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o22361" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o22362" name="neck" name_original="neck" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially; not" name="size" src="d0_s4" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22363" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o22364" name="apiculum" name_original="apiculum" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22365" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o22366" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o22356" id="r1860" name="held" negation="false" src="d0_s4" to="o22357" />
      <relation from="o22356" id="r1861" name="without" negation="false" src="d0_s4" to="o22359" />
      <relation from="o22356" id="r1862" modifier="more or less" name="as long as" negation="false" src="d0_s4" to="o22360" />
      <relation from="o22365" id="r1863" name="with" negation="false" src="d0_s4" to="o22366" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllodia 3–5, usually more numerous than pitchers, decumbent to ascending, weakly to strongly falcate, 5–18 × 0.5–3.5 cm.</text>
      <biological_entity id="o22367" name="phyllodium" name_original="phyllodia" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character constraint="than pitchers" constraintid="o22368" is_modifier="false" name="quantity" src="d0_s5" value="more numerous" value_original="more numerous" />
        <character char_type="range_value" from="decumbent" modifier="usually" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" modifier="weakly to strongly" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22368" name="pitcher" name_original="pitchers" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Scapes 45–70 cm, shorter than pitchers;</text>
      <biological_entity id="o22369" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s6" to="70" to_unit="cm" />
        <character constraint="than pitchers" constraintid="o22370" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22370" name="pitcher" name_original="pitchers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts (yellowish), 0.6–1.2 cm, (blunt apically).</text>
      <biological_entity id="o22371" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers slightly ill-scented;</text>
      <biological_entity id="o22372" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="odor" src="d0_s8" value="ill-scented" value_original="ill-scented" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals yellow, 3–5 × 2–3 cm;</text>
      <biological_entity id="o22373" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s9" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, distal portion oblongelliptic to slightly obovate, 4–5.5 × 1.4–1.7 cm, margins entire;</text>
      <biological_entity id="o22374" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22375" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s10" to="slightly obovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="width" src="d0_s10" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22376" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style disc yellow-green, 5–8.5 cm diam.</text>
      <biological_entity constraint="style" id="o22377" name="disc" name_original="disc" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="5" from_unit="cm" name="diameter" src="d0_s11" to="8.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 1.5–1.8 cm diam.</text>
      <biological_entity id="o22378" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s12" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.8–2 mm. 2n = 26.</text>
      <biological_entity id="o22379" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22380" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seepage bogs, wet thickets, boggy stream banks, wet sands on riverbanks, seeps in rich oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepage bogs" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="boggy stream banks" />
        <character name="habitat" value="wet sands" constraint="on riverbanks , seeps in rich oak woodlands" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="seeps" constraint="in rich oak woodlands" />
        <character name="habitat" value="rich oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Green pitcher plant</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Sarracenia oreophila is rare and local, the first pitcher plant to be listed as federally endangered. Populations are threatened by fire suppression and land drainage. It occurs in isolated colonies in open wetlands and in shaded woods when fire or pasturing do not remove vegetation cover. In open, wet sites it makes impressive stands. It is the only Sarracenia to occur in rocky sandbar deposits along a major river in northeastern Alabama. It is known from central and northeastern Alabama, adjacent Georgia, and the mountains of North Carolina (Clay County). Early reports from Tennessee are not supported by specimen evidence. Sarracenia oreophila is able to survive drought better than other species of the genus. But, unlike the others, the pitchers do not last well into the summer and usually die down completely by mid July.</discussion>
  <discussion>Sarracenia oreophila is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>