<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter W. Fritsch</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="treatment_page">339</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle &amp; Sprengel" date="unknown" rank="family">STYRACACEAE</taxon_name>
    <taxon_hierarchy>family STYRACACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10861</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, deciduous [evergreen], hairs stellate [scales].</text>
      <biological_entity id="o26663" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o26665" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, simple;</text>
      <biological_entity id="o26666" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules absent;</text>
      <biological_entity id="o26667" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o26668" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins serrate or entire, rarely also lobed.</text>
      <biological_entity constraint="blade" id="o26669" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences false-terminal and/or axillary racemes or panicles, sometimes solitary flowers.</text>
      <biological_entity id="o26670" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o26671" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <biological_entity id="o26672" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o26673" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual [plants gynodioecious];</text>
      <biological_entity id="o26674" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth and androecium perigynous to epigynous;</text>
      <biological_entity id="o26675" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="perigynous" name="position" src="d0_s7" to="epigynous" />
      </biological_entity>
      <biological_entity id="o26676" name="androecium" name_original="androecium" src="d0_s7" type="structure">
        <character char_type="range_value" from="perigynous" name="position" src="d0_s7" to="epigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium adnate to ovary wall at various levels;</text>
      <biological_entity id="o26677" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character constraint="to ovary wall" constraintid="o26678" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="ovary" id="o26678" name="wall" name_original="wall" src="d0_s8" type="structure" />
      <biological_entity id="o26679" name="level" name_original="levels" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="various" value_original="various" />
      </biological_entity>
      <relation from="o26678" id="r2229" name="at" negation="false" src="d0_s8" to="o26679" />
    </statement>
    <statement id="d0_s9">
      <text>sepals (2–) 4–5 (–9) [absent], distinct or connate, sometimes forming tube with or without minute apical teeth;</text>
      <biological_entity id="o26680" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s9" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="9" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o26681" name="tube" name_original="tube" src="d0_s9" type="structure" />
      <biological_entity constraint="apical" id="o26682" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o26680" id="r2230" modifier="sometimes" name="forming" negation="false" src="d0_s9" to="o26681" />
      <relation from="o26680" id="r2231" modifier="sometimes" name="with or without" negation="false" src="d0_s9" to="o26682" />
    </statement>
    <statement id="d0_s10">
      <text>petals 4–5 (–8), connate proximally;</text>
      <biological_entity id="o26683" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="8" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens usually 2 (–4) times [equaling] number of corolla lobes, adnate to corolla at base only or to 1/2 length of corolla, uniseriate in appearance;</text>
      <biological_entity id="o26684" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character constraint="lobe" constraintid="o26685" is_modifier="false" name="size_or_quantity" src="d0_s11" value="2(-4) times number of corolla lobes" />
        <character constraint="to corolla" constraintid="o26686" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
        <character constraint="in appearance" constraintid="o26688" is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s11" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o26685" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o26686" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
      <biological_entity id="o26687" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="0 length of corolla" name="length" src="d0_s11" to="1/2 length of corolla" />
      </biological_entity>
      <biological_entity id="o26688" name="appearance" name_original="appearance" src="d0_s11" type="structure" />
      <relation from="o26686" id="r2232" name="at" negation="false" src="d0_s11" to="o26687" />
    </statement>
    <statement id="d0_s12">
      <text>anthers dehiscent by longitudinal slits;</text>
      <biological_entity id="o26689" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character constraint="by slits" constraintid="o26690" is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o26690" name="slit" name_original="slits" src="d0_s12" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistils 1, 2–4 [–5] -carpellate;</text>
      <biological_entity id="o26691" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-4[-5]-carpellate" value_original="2-4[-5]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary partly or completely inferior, proximally 2–4 [–5] -septate, 1-locular by distal attenuation of septa;</text>
      <biological_entity constraint="septum" id="o26693" name="attenuation" name_original="attenuation" src="d0_s14" type="structure" constraint_original="septum distal; septum" />
      <biological_entity id="o26694" name="septum" name_original="septa" src="d0_s14" type="structure" />
      <relation from="o26693" id="r2233" name="part_of" negation="false" src="d0_s14" to="o26694" />
    </statement>
    <statement id="d0_s15">
      <text>placentation axile [near basal];</text>
      <biological_entity id="o26692" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="completely" name="position" src="d0_s14" value="inferior" value_original="inferior" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s14" value="2-4[-5]-septate" value_original="2-4[-5]-septate" />
        <character constraint="by distal attenuation" constraintid="o26693" is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules anatropous, uni or bitegmic, tenuinucellate;</text>
      <biological_entity id="o26695" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="tenuinucellate" value_original="tenuinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 1, simple [3-parted];</text>
      <biological_entity id="o26696" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 1, terminal, truncate or minutely 2–4 [–5] -lobed.</text>
      <biological_entity id="o26697" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s18" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s18" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s18" value="2-4[-5]-lobed" value_original="2-4[-5]-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsular, dehiscence loculicidal, or nutlike (dry and indehiscent), sometimes winged, [drupaceous].</text>
      <biological_entity id="o26698" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="nutlike" value_original="nutlike" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1–4 [–ca. 50], brown, ellipsoid to globose or fusiform [flat, winged];</text>
      <biological_entity id="o26699" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s20" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="brown" value_original="brown" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s20" to="globose or fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryo straight or slightly curved;</text>
      <biological_entity id="o26700" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="course" src="d0_s21" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s21" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endosperm copious, cellular, oily.</text>
      <biological_entity id="o26701" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s22" value="copious" value_original="copious" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="cellular" value_original="cellular" />
        <character is_modifier="false" name="coating" src="d0_s22" value="oily" value_original="oily" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, s Europe, e, se Asia; warm-temperate and tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="warm-temperate and tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Snowbell or Storax Family</other_name>
  <discussion>Genera 11, species ca. 160 (2 genera, 7 species in the flora).</discussion>
  <discussion>Species of Halesia and Styrax are sometimes cultivated for ornament, and species of Alniphyllum, Melliodendron, Pterostyrax, Rehderodendron, and Sinojackia have been successfully grown in the United States.</discussion>
  <discussion>About 80% of the species of Styracaceae belong to Styrax; the next largest genera in the family are Rehderodendron and Sinojackia, with five and six species, respectively. All genera are endemic to eastern and/or southeastern Asia with the exception of the two in the flora.</discussion>
  <discussion>The Styracaceae have been shown to be monophyletic on the basis of morphological and DNA sequence evidence (P. W. Fritsch et al. 2001). Studies with DNA sequence data place Styracaceae within order Ericales (in the sense of Angiosperm Phylogeny Group 2003; C. M. Morton et al. 1996; A. A. Anderberg et al. 2002) as sister to Diapensiaceae, and Styracaceae + Diapensiaceae are sister to Symplocaceae (J. Schönenberger et al. 2005). The South American genus Pamphilia is now included within Styrax (B. Wallnöfer 1997; Fritsch 1999, 2001).</discussion>
  <discussion>The fossil record of the Styracaceae extends back to the early Eocene (B. H. Tiffney 1985).</discussion>
  <discussion>In the present treatment, calyx dimensions include the hypanthium, whereas those of the corolla and stamens do not. Leaf dimensions are those of the larger examples on each specimen except where noted.</discussion>
  <references>
    <reference>Dickison, W. C. 1993. Floral anatomy of the Styracaceae, including observations on intra-ovarian trichomes. Bot. J. Linn. Soc. 112: 223–255.</reference>
    <reference>Dickison, W. C. and K. D. Phend. 1985. Wood anatomy of the Styracaceae: Evolutionary and ecological considerations. I.A.W.A. Bull., N.S. 6: 3–22.</reference>
    <reference>Fritsch, P. W. 2004. Styracaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 7+ vols. Berlin etc. Vol. 6, pp. 434–442.</reference>
    <reference>Fritsch, P. W., C. M. Morton, Chen T., and C. Meldrum. 2001. Phylogeny and biogeography of the Styracaceae. Int. J. Pl. Sci. 162: S95–S116.</reference>
    <reference>Morton, C. M. and W. C. Dickison. 1992. Comparative pollen morphology of the Styracaceae. Grana 31: 1–15.</reference>
    <reference>Perkins, J. R. 1928. Übersicht über die Gattungen der Styracaceae sowie Zusammenstellung der Abbildungen und der Literatur über die Arten dieser Familie bis zum Jahre 1928.... Leipzig.</reference>
    <reference>Spongberg, S. A. 1976b. Styracaceae hardy in temperate North America. J. Arnold Arbor. 57: 55–73.</reference>
    <reference>Wood, C. E. Jr. and R. B. Channell. 1960. The genera of the Ebenales in the southeastern United States. J. Arnold Arbor. 41: 1–35.</reference>
    <reference>Miers, J. 1859. On the natural order Styraceae, as distinguished from the Symplocaceae. Ann. Mag. Nat. Hist., ser. 3, 3: 394–404.</reference>
    <reference>Schadel, W. E. and W. C. Dickison. 1979. Leaf anatomy and venation patterns of the Styracaceae. J. Arnold Arbor. 60: 8–37.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Winter buds without scales; pith continuous; inflorescences borne on shoots of current growing season; fertile shoots of current growing season with fully developed leaves; articulation between pedicels and flowers absent; corolla lobes 5-6(-8); ovaries partly inferior; fruits not winged.</description>
      <determination>1 Styrax</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Winter buds with scales; pith chambered; inflorescences borne on shoots of previous growing season; fertile shoots of current growing season without fully developed leaves (rarely fully developed); articulation between pedicels and flowers present; corolla lobes 4; ovaries completely inferior; fruits winged.</description>
      <determination>2 Halesia</determination>
    </key_statement>
  </key>
</bio:treatment>