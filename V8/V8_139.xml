<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman,Nicholas D. Levsen</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="treatment_page">70</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CHRYSOSPLENIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 398. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 189. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus CHRYSOSPLENIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chrysos, gold, and splenos, spleen, alluding to color of flowers and to alleged medical properties</other_info_on_name>
    <other_info_on_name type="fna_id">107013</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, rhizomatous, stoloniferous, (rhizomes and stolons with functional leaves or stolon leaves reduced, nonfunctional, scalelike);</text>
      <biological_entity id="o14613" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex absent.</text>
      <biological_entity id="o14614" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems repent, decumbent, or ascending to erect, leafy or leafless, (1.2–) 2–30 cm (often as short as 2 cm in C. wrightii), glabrous or sparsely to densely villous.</text>
      <biological_entity id="o14615" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="repent" value_original="repent" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafless" value_original="leafless" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s2" to="sparsely densely villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline or arising from stolons or rhizomes, opposite or alternate;</text>
      <biological_entity id="o14616" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character constraint="from rhizomes" constraintid="o14618" is_modifier="false" name="orientation" src="d0_s3" value="arising" value_original="arising" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o14617" name="stolon" name_original="stolons" src="d0_s3" type="structure" />
      <biological_entity id="o14618" name="rhizome" name_original="rhizomes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o14619" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present, glabrous or villous;</text>
      <biological_entity id="o14620" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate, depressed-ovate, depressed-elliptic, reniform, flabellate, or, sometimes, nearly orbiculate, unlobed, base attenuate, cuneate, truncate, or cordate, ultimate margins subentire, crenate, crenulate, or crenate-dentate, crenae sometimes prominent and margins appearing ± lobed, apex obtuse, rounded, or truncate, surfaces glabrous or sparsely villous to villous;</text>
      <biological_entity id="o14621" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="depressed-ovate" value_original="depressed-ovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="depressed-elliptic" value_original="depressed-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" modifier="sometimes; nearly" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o14622" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o14623" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate-dentate" value_original="crenate-dentate" />
      </biological_entity>
      <biological_entity id="o14624" name="crena" name_original="crenae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o14626" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o14627" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="true" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o14625" id="r1254" name="appearing" negation="false" src="d0_s6" to="o14626" />
      <relation from="o14625" id="r1255" name="appearing" negation="false" src="d0_s6" to="o14627" />
    </statement>
    <statement id="d0_s7">
      <text>venation palmate.</text>
      <biological_entity id="o14625" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s6" to="villous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences simple or compound cymes, from terminal bud in rosette, 2–30-flowered, sometimes flowers solitary, bracteate.</text>
      <biological_entity id="o14628" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="2-30-flowered" value_original="2-30-flowered" />
      </biological_entity>
      <biological_entity id="o14629" name="cyme" name_original="cymes" src="d0_s8" type="structure" />
      <biological_entity constraint="terminal" id="o14630" name="bud" name_original="bud" src="d0_s8" type="structure" />
      <biological_entity id="o14631" name="rosette" name_original="rosette" src="d0_s8" type="structure" />
      <biological_entity id="o14632" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <relation from="o14628" id="r1256" name="from" negation="false" src="d0_s8" to="o14630" />
      <relation from="o14630" id="r1257" name="in" negation="false" src="d0_s8" to="o14631" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium 1/2–3/4 adnate medially or distally to ovary, 0.5–1.5 mm free from ovary, greenish or yellow-green;</text>
      <biological_entity id="o14633" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14634" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s9" to="3/4" />
        <character constraint="to ovary" constraintid="o14635" is_modifier="false" modifier="medially" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.5" to_unit="mm" />
        <character constraint="from ovary" constraintid="o14636" is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o14635" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o14636" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals 4, yellow, greenish yellow, green, greenish red, reddish orange, or purple, sometimes purple-spotted;</text>
      <biological_entity id="o14637" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14638" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish red" value_original="greenish red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="purple-spotted" value_original="purple-spotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals absent;</text>
      <biological_entity id="o14639" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14640" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary disc conspicuous or apparently absent;</text>
      <biological_entity id="o14641" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectary" id="o14642" name="disc" name_original="disc" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="apparently" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2–8, usually 4 or 8;</text>
      <biological_entity id="o14643" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14644" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="8" />
        <character modifier="usually" name="quantity" src="d0_s13" unit="or" value="4" value_original="4" />
        <character modifier="usually" name="quantity" src="d0_s13" unit="or" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments lanceolate to narrowly oblong;</text>
      <biological_entity id="o14645" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o14646" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s14" to="narrowly oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1/2–3/4-inferior, 1-locular, carpels connate 3/4–4/5 their lengths;</text>
      <biological_entity id="o14647" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o14648" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
      </biological_entity>
      <biological_entity id="o14649" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character char_type="range_value" from="3/4" name="lengths" src="d0_s15" to="4/5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>placentation parietal;</text>
      <biological_entity id="o14650" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s16" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 2;</text>
      <biological_entity id="o14651" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o14652" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 2.</text>
      <biological_entity id="o14653" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o14654" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Capsules (cuplike after dehiscence), 2-beaked (beaks divergent).</text>
      <biological_entity id="o14655" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="2-beaked" value_original="2-beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds dark-brown or reddish-brown, ellipsoid, ovoid, or spheroid, smooth.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 11.</text>
      <biological_entity id="o14656" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="spheroid" value_original="spheroid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s20" value="spheroid" value_original="spheroid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o14657" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, s South America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Golden-saxifrage</other_name>
  <other_name type="common_name">dorine</other_name>
  <discussion>Species ca. 60 (6 in the flora).</discussion>
  <discussion>A wide range of chromosome numbers has been reported in Chrysosplenium. M. Nakazawa et al. (1997) and D. E. Soltis et al. (2001b) hypothesized that these numbers arose from an ancestral base of x = 11, with multiple lines of dysploidy (reported as aneuploidy). The historical division of the genus into two major clades—opposite-leaved species and alternate-leaved species—is supported by flavonoid (B. A. Bohm and F. W. Collins 1979) and molecular data (Nakazawa et al.; Soltis et al.). Phylogenetic analyses using the rbcL and matK genes (Nakazawa et al.; Soltis et al.) have provided limited resolution for species-level relationships.</discussion>
  <discussion>The relationship of the alternate-leaved, North American species to those in Europe and Asia has not been examined in detail. Morphological similarities between the North American species and Eurasian Chrysosplenium alternifolium Linnaeus have caused some authors to treat C. iowense, C. rosendahlii, C. tetrandrum, and C. wrightii as infraspecific taxa of C. alternifolium. Each of them is maintained here as a distinct species pending further studies.</discussion>
  <discussion>Stamen number can vary among flowers within an inflorescence in some alternate-leaved species (C. O. Rosendahl 1947; J. G. Packer 1963), with lateral flowers sometimes smaller and having fewer stamens than the central flowers. In species usually producing eight stamens, Packer observed that variable stamen numbers resulted from the systematic failure of stamens to develop. Stamens alternating with the styles are suppressed first. In flowers with fewer than four stamens, the stamens alternating with the styles are suppressed. This corresponds with developmental observations made by L.-P. Ronse Decraene et al. (1998) in Chrysosplenium alternifolium Linnaeus.</discussion>
  <discussion>D. B. O. Savile (1953) demonstrated splash-cup dispersal of seeds in Chrysosplenium americanum, reporting a maximum dispersal distance of 40 cm in the laboratory. R. M. Weber (1979) reported a maximum dispersal distance of 30 cm for C. iowense in the field and 45 cm in the laboratory. Among three Japanese species examined in the field, H. Nakanishi (2002) reported a maximum dispersal distance of 116 cm.</discussion>
  <references>
    <reference>Bohm, B. A. and F. W. Collins. 1979. Flavonoids of some species of Chrysosplenium. Biochem. Syst. &amp; Ecol. 7: 195–201.</reference>
    <reference>Hara, H. 1957. Synopsis of the genus Chrysosplenium L. (Saxifragaceae). J. Fac. Sci. Univ. Tokyo, Sect. 3, Bot. 7: 1–90.</reference>
    <reference>Packer, J. G. 1963. The taxonomy of some North American species of Chrysosplenium L., section Alternifolia Franchet. Canad. J. Bot. 41: 85–103.</reference>
    <reference>Rosendahl, C. O. 1947. Studies in Chrysosplenium with special reference to the taxonomic status and distribution of C. iowense. Rhodora 40: 25–36.</reference>
    <reference>Soltis, D. E. et al. 2001b. Phylogenetic relationships and evolution in Chrysosplenium (Saxifragaceae) based on matK sequence data. Amer. J. Bot. 88: 883–893.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves opposite, sometimes alternate distally; inflorescences solitary flowers or open cymes</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves alternate; inflorescences compact cymes</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Anthers purple, red, or orange; leaf blade margins subentire or 5-7(-9)-crenate or -crenulate; seeds puberulent; c, e North America.</description>
      <determination>1 Chrysosplenium americanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Anthers yellow; leaf blade margins 9-17(-21)-crenate or -crenate-dentate; seeds glabrous; w North America.</description>
      <determination>2 Chrysosplenium glechomifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Nectary discs prominent, yellow or purple, 8-lobed; stolons white, tan, or yellowish, 1-2.5(-3) mm diam., usually densely villous, hairs reddish brown; stamens 8; leaves fleshy; petioles of stolon leaves sparsely to densely villous, hairs reddish brown.</description>
      <determination>6 Chrysosplenium wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Nectary discs apparently absent or yellow, unlobed; stolons white, 0.3-1.2 mm diam., sparsely villous, hairs white, reddish brown, or purplish; stamens 2-8; leaves membranous or fleshy; petioles of stolon leaves sometimes glabrous, usually sparsely villous, hairs white or reddish brown</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens (3-)4; sepals usually erect, sometimes spreading.</description>
      <determination>5 Chrysosplenium tetrandrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 2-8; sepals spreading</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals yellow or greenish yellow, not purple-spotted; temperate or boreal.</description>
      <determination>3 Chrysosplenium iowense</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals green or greenish yellow, usually purple-spotted distally, rarely not purple-spotted; arctic.</description>
      <determination>4 Chrysosplenium rosendahlii</determination>
    </key_statement>
  </key>
</bio:treatment>