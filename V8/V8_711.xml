<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">cyrillaceae</taxon_name>
    <taxon_name authority="Banks ex C. F. Gaertner" date="1807" rank="genus">cliftonia</taxon_name>
    <taxon_name authority="(Lamarck) Britton ex Sargent" date="1891" rank="species">monophylla</taxon_name>
    <place_of_publication>
      <publication_title>Silva</publication_title>
      <place_in_publication>2: 7. 1891  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyrillaceae;genus cliftonia;species monophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092296</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ptelea</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">monophylla</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck and J. Poiret, Tabl. Encycl.</publication_title>
      <place_in_publication>1: 336. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ptelea;species monophylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 15 m, often forming dense stands.</text>
      <biological_entity id="o17009" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17010" name="stand" name_original="stands" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o17009" id="r1447" modifier="often" name="forming" negation="false" src="d0_s0" to="o17010" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole absent or relatively short;</text>
      <biological_entity id="o17011" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17012" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade bluish white abaxially, green adaxially, elliptic to oblanceolate, 2.5–10 × 1.2–1.8 cm, coriaceous, base cuneate, narrowed to petiole, apex acute to obtuse or shallowly emarginate.</text>
      <biological_entity id="o17013" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o17014" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s2" value="bluish white" value_original="bluish white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s2" to="1.8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o17015" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character constraint="to petiole" constraintid="o17016" is_modifier="false" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o17016" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o17017" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse or shallowly emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers fragrant;</text>
      <biological_entity id="o17018" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="odor" src="d0_s3" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals white or pink, 6–8 mm.</text>
      <biological_entity id="o17019" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 5–7 mm.</text>
      <biological_entity id="o17020" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic soils in seepage bogs ("hillside bogs," "pitcher-plant bogs"), along blackwater streams, depressions in pine savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic soils" constraint="in seepage bogs" />
        <character name="habitat" value="seepage bogs" />
        <character name="habitat" value="hillside bogs" />
        <character name="habitat" value="pitcher-plant bogs" />
        <character name="habitat" value="blackwater streams" />
        <character name="habitat" value="depressions" constraint="in pine savannas" />
        <character name="habitat" value="pine savannas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Buckwheat tree</other_name>
  <other_name type="common_name">black titi</other_name>
  <discussion>The common name “buckwheat tree” alludes to the resemblance of the fruits with those of Fagopyrum (Polygonaceae). Cliftonia monophylla is a coastal plain endemic that forms dense patches of trees, except in sites that are burned frequently, where it is shrubby.</discussion>
  
</bio:treatment>