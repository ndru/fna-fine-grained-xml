<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="(Rose) Moran" date="1953" rank="subgenus">Hasseanthus</taxon_name>
    <taxon_name authority="(Moran) Moran" date="1953" rank="species">nesiotica</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>7: 110. 1953,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus hasseanthus;species nesiotica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250092096</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hasseanthus</taxon_name>
    <taxon_name authority="Moran" date="unknown" rank="species">nesioticus</taxon_name>
    <place_of_publication>
      <publication_title>Desert Pl. Life</publication_title>
      <place_in_publication>22: 99, figs. 5–7. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hasseanthus;species nesioticus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Corms subglobose to irregular, 1–3 cm × 7–20 mm.</text>
      <biological_entity id="o8060" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s0" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="3" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s0" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 8–16;</text>
      <biological_entity id="o8061" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s1" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–4 (–7) mm wide, to 1/3 as wide as blade;</text>
      <biological_entity id="o8062" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="as-wide-as blade" constraintid="o8063" from="0" name="quantity" src="d0_s2" to="1/3" />
      </biological_entity>
      <biological_entity id="o8063" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade green, linear-oblanceolate to spatulate, 2.5–3.5 (–5) cm × 3–15 (–25) mm, 2–5 mm thick, base 4–12 mm wide, apex acute to obtuse, surfaces sometimes glaucous.</text>
      <biological_entity id="o8064" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="thickness" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="thickness" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="×3-15" from_inclusive="false" from_unit="mm" name="thickness" src="d0_s3" to="25" to_unit="mm" />
        <character name="thickness" src="d0_s3" unit="mm" value="×3-15" value_original="×3-15" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8065" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8066" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o8067" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: cincinni 3–8-flowered, 2–4 cm;</text>
      <biological_entity id="o8068" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o8069" name="cincinnus" name_original="cincinni" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-8-flowered" value_original="3-8-flowered" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>floral shoots 3–10 cm × 1–3 mm (slightly thicker distally);</text>
      <biological_entity id="o8070" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="floral" id="o8071" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaves 15–25, spreading, blade triangular-lanceolate to ovate, 1–2.5 cm × 5–10 mm, 3–6 mm thick, apex subacute to obtuse.</text>
      <biological_entity id="o8072" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o8073" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s6" to="25" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o8074" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="thickness" src="d0_s6" to="2.5" to_unit="cm" />
        <character name="thickness" src="d0_s6" unit="mm" value="×5-10" value_original="×5-10" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8075" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers with musky, sweet odor;</text>
      <biological_entity id="o8076" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="odor" notes="" src="d0_s7" value="sweet" value_original="sweet" />
      </biological_entity>
      <biological_entity id="o8077" name="musky" name_original="musky" src="d0_s7" type="structure" />
      <relation from="o8076" id="r651" name="with" negation="false" src="d0_s7" to="o8077" />
    </statement>
    <statement id="d0_s8">
      <text>petals connate 1–2 mm, erect or spreading, white, yellowish green on keel and toward base, not lined, elliptic, 7–14 × 3.5–5.5 mm, apex acute, corolla 4–5 mm diam. at base and 5–18 mm diam. at apex;</text>
      <biological_entity id="o8078" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character constraint="on keel and toward base" constraintid="o8079" is_modifier="false" name="coloration" src="d0_s8" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s8" value="lined" value_original="lined" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s8" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8079" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o8080" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8081" name="corolla" name_original="corolla" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o8082" from="4" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="at apex" constraintid="o8083" from="5" from_unit="mm" name="diameter" notes="" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8082" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o8083" name="apex" name_original="apex" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pistils connivent or separate, erect at anthesis;</text>
      <biological_entity id="o8084" name="pistil" name_original="pistils" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="separate" value_original="separate" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 4–7 mm;</text>
      <biological_entity id="o8085" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 1–1.5 mm.</text>
      <biological_entity id="o8086" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Follicles ascending, with adaxial margins ca. 45–75º above horizontal.</text>
      <biological_entity constraint="adaxial" id="o8088" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <relation from="o8087" id="r652" modifier="above horizontal" name="with" negation="false" src="d0_s12" to="o8088" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 68.</text>
      <biological_entity id="o8087" name="follicle" name_original="follicles" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8089" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy soil on flats near sea</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="heavy soil" constraint="on flats near sea" />
        <character name="habitat" value="flats" constraint="near sea" />
        <character name="habitat" value="sea" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Dudleya nesiotica is known from the west end of Santa Cruz Island, where it is abundant, and it is considered seriously threatened (California Native Plant Society, http://cnps.web.aplus.net/cgi-bin/inv/inventory.cgi). It resembles D. blochmaniae in its white petals and especially in its floral odor. It differs from other species of subg. Hasseanthus and approaches subg. Dudleya in its larger and more-erect petals and its more-erect pistils. The first collection had rosette leaves broader than in other species, not only in the blade but also in the petiolar region and at the base. These facts together with the chromosome number suggested that D. nesiotica might be an allotetraploid of D. blochmaniae, or a relative, and some member of subg. Dudleya (R. V. Moran 1951). However, some later-found plants of D. nesiotica have the leaves much narrower, in fact linear-oblanceolate, as shown by S. Junak et al. (1995). M. Dodero (1996) considered subg. Hasseanthus monophyletic and thought D. nesiotica most closely related to its near neighbor D. blochmaniae subsp. insularis on Santa Rosa Island 10 kilometers to the west, with which it shares four allozyme characters. D. Wilken (pers. comm.) has found mostly linear leaves in D. nesiotica of open sites but great variation in leaf shape in plants from denser vegetation.</discussion>
  <discussion>Dudleya nesiotica is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>