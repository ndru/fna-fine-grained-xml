<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">211</other_info_on_meta>
    <other_info_on_meta type="treatment_page">212</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sedum</taxon_name>
    <taxon_name authority="Alexander" date="1936" rank="species">robertsianum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>63: 201, fig. 1. 1936,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus sedum;species robertsianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250092125</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sedum</taxon_name>
    <taxon_name authority="Hemsley" date="unknown" rank="species">parvum</taxon_name>
    <taxon_name authority="(Alexander) R. T. Clausen" date="unknown" rank="subspecies">robertsianum</taxon_name>
    <taxon_hierarchy>genus Sedum;species parvum;subspecies robertsianum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, tufted, glabrous.</text>
      <biological_entity id="o9093" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, branched basally, (fleshy), with numerous decumbent branchlets, not forming rosettes.</text>
      <biological_entity id="o9094" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o9095" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o9096" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <relation from="o9094" id="r734" name="with" negation="false" src="d0_s1" to="o9095" />
      <relation from="o9094" id="r735" name="forming" negation="true" src="d0_s1" to="o9096" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves (persistent), alternate, spreading, sessile;</text>
      <biological_entity id="o9097" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade yellow-green, not glaucous, ovate, subterete, somewhat flattened, 5–8 × 3–4 mm, (thick, turgid), base not spurred, not scarious, apex apiculate, (surfaces minutely papillose, caused by reflections of inner facets of windowed cells).</text>
      <biological_entity id="o9098" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9099" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o9100" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowering shoots (axillary), erect, simple or branched, 5–10 cm;</text>
      <biological_entity id="o9101" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaf-blades ovate, base not spurred;</text>
      <biological_entity id="o9102" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o9103" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s5" value="spurred" value_original="spurred" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>offsets not formed.</text>
      <biological_entity id="o9104" name="offset" name_original="offsets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymes, 6–12-flowered, simple or 2-branched, sometimes with short branch at base with solitary flower;</text>
      <biological_entity constraint="inflorescences" id="o9105" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="6-12-flowered" value_original="6-12-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-branched" value_original="2-branched" />
      </biological_entity>
      <biological_entity id="o9106" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o9107" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o9108" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o9105" id="r736" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o9106" />
      <relation from="o9106" id="r737" name="at" negation="false" src="d0_s7" to="o9107" />
      <relation from="o9107" id="r738" name="with" negation="false" src="d0_s7" to="o9108" />
    </statement>
    <statement id="d0_s8">
      <text>branches not recurved, sometimes forked;</text>
      <biological_entity id="o9109" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts similar to leaves, smaller.</text>
      <biological_entity id="o9110" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o9111" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o9110" id="r739" name="to" negation="false" src="d0_s9" to="o9111" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels absent or to 0.5 mm.</text>
      <biological_entity id="o9112" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0-0.5 mm" value_original="0-0.5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers (4–) 5-merous;</text>
      <biological_entity id="o9113" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="(4-)5-merous" value_original="(4-)5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals spreading to reflexed, distinct, yellow-green, lanceolate, unequal, ca. 2 × ca. 0.8 mm, apex obtuse;</text>
      <biological_entity id="o9114" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="reflexed" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character name="length" src="d0_s12" unit="mm" value="2" value_original="2" />
        <character name="width" src="d0_s12" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
      <biological_entity id="o9115" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spreading, nearly distinct, bright-yellow, lanceolate, canaliculate, ca. 4 mm, apex acute;</text>
      <biological_entity id="o9116" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="nearly" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="canaliculate" value_original="canaliculate" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o9117" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments color unknown;</text>
      <biological_entity id="o9118" name="filament" name_original="filaments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers color unknown;</text>
      <biological_entity id="o9119" name="anther" name_original="anthers" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>nectar scales pale-yellow, oblong.</text>
      <biological_entity constraint="nectar" id="o9120" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Carpels spreading, distinct, tan to reddish.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 28.</text>
      <biological_entity id="o9121" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s17" to="reddish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9122" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow, calcareous soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow" />
        <character name="habitat" value="calcareous soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion>Sedum robertsianum occurs in the Del Norte and Glass mountains of Brewster County.</discussion>
  <discussion>Sedum robertsianum is a somewhat confusing taxonomic entity. In a treatment contributed in the 1970s for the Flora of the Chihuahuan Desert Region (M. C. Johnston and J. S. Henrickson, in prep.), R. T. Clausen placed S. robertsianum in synonymy with Mexican S. parvum but did not assign it to subspecies status. However, only subsp. nanifolium occurred in both Texas and Mexico. Later, Clausen (1981) made S. robertsianum a subspecies of S. parvum. In a study of the systematics of the S. parvum complex, G. L. Nesom and B. L. Turner (1995) treated S. robertsianum as a species of uncertain status. They cited specimens from the Del Norte Mountains (the type locality of S. robertsianum, see Clausen 1981) as S. nanifolium, which they elevated from S. parvum subsp. nanifolium. It is possible that there are two species of yellow-flowered sedums within one mountain range in western Texas. It is also possible that there is only one species, and either S. robertsianum is synonymous with S. nanifolium, or it is a distinct species and the only Sedum in the Del Norte Mountains of western Texas.</discussion>
  
</bio:treatment>