<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Reid V. Moran</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CRASSULA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 282. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 136. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus CRASSULA</taxon_hierarchy>
    <other_info_on_name type="etymology">ula, diminutive, alluding to leaves</other_info_on_name>
    <other_info_on_name type="fna_id">108270</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [shrubs], annual or perennial, aquatic or terrestrial, not viviparous, 0.1–5 dm, glabrous [pubescent].</text>
      <biological_entity id="o15129" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="viviparous" value_original="viviparous" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, decumbent, or spreading, simple or branching, succulent.</text>
      <biological_entity id="o15130" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="texture" src="d0_s1" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent or deciduous, cauline, opposite, sessile, connate basally;</text>
      <biological_entity id="o15131" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o15132" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s2" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate, oblong, triangular to lanceolate or oblanceolate, or linear, laminar, 0.1–7 cm, fleshy, base not spurred, margins entire, with glands (hydathodes) in submarginal rows [scattered];</text>
      <biological_entity id="o15133" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s3" to="lanceolate or oblanceolate or linear" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s3" to="lanceolate or oblanceolate or linear" />
        <character is_modifier="false" name="position" src="d0_s3" value="laminar" value_original="laminar" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o15134" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s3" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o15135" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15136" name="gland" name_original="glands" src="d0_s3" type="structure" />
      <biological_entity constraint="submarginal" id="o15137" name="row" name_original="rows" src="d0_s3" type="structure" />
      <relation from="o15135" id="r1294" name="with" negation="false" src="d0_s3" to="o15136" />
      <relation from="o15136" id="r1295" name="in" negation="false" src="d0_s3" to="o15137" />
    </statement>
    <statement id="d0_s4">
      <text>veins not conspicuous.</text>
      <biological_entity id="o15138" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences thyrses or panicles [solitary flowers] in axils of leaves (flowers clustered when distal leaves smaller and crowded).</text>
      <biological_entity id="o15139" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o15140" name="thyrse" name_original="thyrses" src="d0_s5" type="structure" />
      <biological_entity id="o15141" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o15142" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity id="o15143" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o15139" id="r1296" name="in" negation="false" src="d0_s5" to="o15142" />
      <relation from="o15140" id="r1297" name="in" negation="false" src="d0_s5" to="o15142" />
      <relation from="o15141" id="r1298" name="in" negation="false" src="d0_s5" to="o15142" />
      <relation from="o15142" id="r1299" name="part_of" negation="false" src="d0_s5" to="o15143" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels present.</text>
      <biological_entity id="o15144" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers erect, 3–4 (–5) -merous;</text>
      <biological_entity id="o15145" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-4(-5)-merous" value_original="3-4(-5)-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals connate basally, all alike;</text>
      <biological_entity id="o15146" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s8" value="alike" value_original="alike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals spreading or recurved, distinct [connate], whitish;</text>
      <biological_entity id="o15147" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calyx and corolla not circumscissile in fruit;</text>
      <biological_entity id="o15148" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o15150" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o15149" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o15150" is_modifier="false" modifier="not" name="dehiscence" src="d0_s10" value="circumscissile" value_original="circumscissile" />
      </biological_entity>
      <biological_entity id="o15150" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectaries linear [various];</text>
      <biological_entity id="o15151" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens as many as sepals;</text>
      <biological_entity id="o15152" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o15153" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <relation from="o15152" id="r1300" name="as many as" negation="false" src="d0_s12" to="o15153" />
    </statement>
    <statement id="d0_s13">
      <text>filaments free;</text>
      <biological_entity id="o15154" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistils spreading to erect, distinct;</text>
      <biological_entity id="o15155" name="pistil" name_original="pistils" src="d0_s14" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s14" to="erect" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary base rounded;</text>
      <biological_entity constraint="ovary" id="o15156" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2+ times shorter than ovary.</text>
      <biological_entity id="o15157" name="style" name_original="styles" src="d0_s16" type="structure">
        <character constraint="ovary" constraintid="o15158" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2+ times shorter than ovary" />
      </biological_entity>
      <biological_entity id="o15158" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits slightly recurved or ascending to erect.</text>
      <biological_entity id="o15159" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s17" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds oblong or ellipsoid to reniform, ridged, sometimes also papillate.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 8 (secondarily 7).</text>
      <biological_entity id="o15160" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s18" to="reniform" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o15161" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Bermuda, Central America, South America, Eurasia, s Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Pygmyweed</other_name>
  <other_name type="common_name">Latin crassus</other_name>
  <other_name type="common_name">thick</other_name>
  <other_name type="common_name">and</other_name>
  <discussion>Bulliarda de Candolle; Tillaea Linnaeus; Tillaeastrum Britton</discussion>
  <discussion>Species ca. 250 (11 in the flora).</discussion>
  <discussion>Before A. Berger (1930) included Tillaea in Crassula, most American authors kept it a separate genus. Although I have argued for keeping it in Crassula (R. V. Moran 1992b), the matter is not closed. Based on studies still in progress, H. ’t Hart (1995) again separated Tillaea, but with data from only one species. E. J. Van Jaarsveld (2004) treated Tillaea as a synonym of Crassula.</discussion>
  <discussion>The annual species of Crassula fall into two sections. In sect. Glomeratae, flowers are solitary in leaf axils, with two flowers per leaf pair. In much-branched plants with leaves often smaller and crowded skyward, the greater part of the plant may fairly be called a thyrse. In sect. Helophytum, flowers seem again to be in leaf axils but for only one leaf of each pair; in fact, they are terminal, each flower ending one section of the sympodial axis (R. V. Moran 1992b, fig. 4). H. Merxmüller et al. (1971, fig. 2) suggested that Tillaea (in the broad sense of H. ’t Hart 1995) may be biphyletic; if these annuals are separated from Crassula, then, it may be best to recognize two genera, Tillaea for sect. Glomeratae and Tillaeastrum for sect. Helophytum.</discussion>
  <discussion>In some species, the aquatic phase may look quite different from the stranded phase, and these flimsy plants make unrevealing dried specimens. In treating the American species, M. Bywater and G. E. Wickens (1984) made good use of seed-coat structure, as shown in scanning electron microscopy (SEM) photographs filed with specimens they studied (as of UC). They showed that the cells of the testa are in a jigsaw pattern, with more or less interlocking lobes. In Crassula connata and C. tillaea, the cell lobes are rounded, the surface smooth or minutely rugulose; C. aquatica has rounded, interlocking lobes with a minutely rugulose surface; C. longipes cell lobes are triangular, the surface rugulose; C. solieri cell lobes are triangular, the surface wax-covered; C. viridis has cell lobes rounded, surface rugose; C. drummondii has cell lobes triangular, surface smooth. My treatment here is based largely on theirs, with some changes from R. V. Moran (1992b). Without SEM, many specimens are hard to identify, and there is still much to learn.</discussion>
  <discussion>Many perennial African species of Crassula are grown in American collections, and some are planted outdoors in warm climates. The large, shrubby C. obovata Haworth and the subshrubby C. tetragona persist about trash heaps and old gardens in California and seem on the borderline of escaping captivity; all perennial species in the flora are introduced.</discussion>
  <references>
    <reference>Bywater, M. and G. E. Wickens. 1984. New World species of the genus Crassula. Kew Bull. 39: 699–728.</reference>
    <reference>Merxmüller, H., H. C. Friedrich, and J. Grau. 1971. Cytotaxonomische Untersuchungen zur Gattungsstruktur von Crassula. Ann. Naturhist. Mus. Wien 75: 111–119.</reference>
    <reference>Moran, R. V. 1992b. Pygmyweed (Crassula connata) etc. in western North America. Cact. Succ. J. (Los Angeles) 64: 223–231.</reference>
    <reference>Toelken, H. R. 1977. A revision of the genus Crassula in southern Africa. Contr. Bolus Herb. 8. Van</reference>
    <reference>Jaarsveld, E. J. 2003. Crassula. In: U. Eggli, ed. 2003. Illustrated Handbook of Succulent Plants. Crassulaceae. New York. Pp. 32–84.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants perennial</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants annual</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades linear to triangular; flowers 5-merous.</description>
      <determination>9 Crassula tetragona</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades obovate to broadly elliptic; flowers 4-merous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pedicels 8-12 mm; petals oblong-lanceolate, ca. 10 mm.</description>
      <determination>10 Crassula argentea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pedicels 3-8 mm; petals narrowly triangular, 3-4 mm.</description>
      <determination>11 Crassula multicava</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers (1-)2 per node; sepals slightly shorter than, equaling, or exceeding petals, apex acute to attenuate or acuminate; follicles 1-2-seeded, old follicles boat-shaped, ascending; plants terrestrial</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers 1 per node; sepals ca. 1/2 as long as petals, apex rounded to obtuse; follicles 6-17-seeded, old follicles flat, spreading; plants often aquatic, sometimes becoming stranded</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals usually 3, 1-1.5 mm, apex attenuate, apiculate; petals 0.5-1 mm.</description>
      <determination>2 Crassula tillaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals usually 4 or 5, 0.5-2 mm, apex acute to acuminate or mucronate; petals 0.6-1.5 mm</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals (3-)4, apex acute to acuminate</description>
      <determination>1 Crassula connata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals 5, apex mucronate.</description>
      <determination>8 Crassula colligata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Seeds papillate</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Seeds not papillate</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades linear, 3-12 mm, apex narrowly acute or attenuate; seeds 0.4-0.6 mm; mountains, Arizona, Idaho.</description>
      <determination>6 Crassula viridis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades narrow-lanceolate, 1.5-3 mm, apex obtuse; seeds 0.2-0.5 mm; often near sea level, Colorado, Kansas, South Carolina, Texas.</description>
      <determination>7 Crassula drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Follicles erect, oblong; sepals ovate to oblong, 0.5-1.5 mm</description>
      <determination>3 Crassula aquatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Follicles ascending, obliquely lanceolate; sepals triangular-ovate to lanceolate, 0.4-1 mm</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Seeds 0.2-0.4 mm, dull, rugulose.</description>
      <determination>4 Crassula longipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Seeds 0.3-0.6 mm, shiny, smooth.</description>
      <determination>5 Crassula solieri</determination>
    </key_statement>
  </key>
</bio:treatment>