<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Saint-Hilaire" date="unknown" rank="family">crassulaceae</taxon_name>
    <taxon_name authority="Britton &amp; Rose" date="1903" rank="genus">dudleya</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">dudleya</taxon_name>
    <taxon_name authority="(M. E. Jones) Britton &amp; Rose" date="1903" rank="species">saxosa</taxon_name>
    <taxon_name authority="(Rose) Moran" date="1957" rank="subspecies">collomiae</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>14: 108. 1957,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crassulaceae;genus dudleya;subgenus dudleya;species saxosa;subspecies collomiae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250092082</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dudleya</taxon_name>
    <taxon_name authority="Rose" date="unknown" rank="species">collomiae</taxon_name>
    <place_of_publication>
      <publication_title>Desert Pl. Life</publication_title>
      <place_in_publication>6: 68. 1934 (as collomae)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dudleya;species collomiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echeveria</taxon_name>
    <taxon_name authority="(Rose) Kearney &amp; Peebles" date="unknown" rank="species">collomiae</taxon_name>
    <taxon_hierarchy>genus Echeveria;species collomiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudices 1.5–3 cm diam.</text>
      <biological_entity id="o143" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s0" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 5–15 × 1–2.5 cm, 2–6 mm thick.</text>
      <biological_entity id="o144" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character name="thickness" src="d0_s1" unit="cm" value="5-15×1-2.5" value_original="5-15×1-2.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: floral shoots red, 5–12-leaved, 15–40 × 0.3–0.6 cm;</text>
      <biological_entity id="o145" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="floral" id="o146" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="5-12-leaved" value_original="5-12-leaved" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="0.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cincinni 4–15-flowered, 3–12 cm.</text>
      <biological_entity id="o147" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o148" name="cincinnus" name_original="cincinni" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="4-15-flowered" value_original="4-15-flowered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 5–20 mm.</text>
      <biological_entity id="o149" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 4–7 mm;</text>
      <biological_entity id="o150" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o151" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals bright-yellow, red-tinged, 12–16 (–20) mm.</text>
      <biological_entity id="o152" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 136.</text>
      <biological_entity id="o153" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-tinged" value_original="red-tinged" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o154" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="136" value_original="136" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices and rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12c.</number>
  <discussion>The well-isolated polyploid subsp. collomiae is similar to the diploid subsp. aloides, differing mainly in its larger flowers.</discussion>
  
</bio:treatment>