<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Linda M. Prince</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">323</other_info_on_meta>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">THEACEAE</taxon_name>
    <taxon_hierarchy>family THEACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10882</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, glabrous or hairy.</text>
      <biological_entity id="o14351" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent or deciduous, alternate [spiral or distichous], simple;</text>
      <biological_entity id="o14353" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s1" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules absent;</text>
      <biological_entity id="o14354" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present;</text>
      <biological_entity id="o14355" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade coriaceous or chartaceous, margins toothed [entire], teeth deciduously gland-tipped.</text>
      <biological_entity id="o14356" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o14357" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o14358" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deciduously" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary [2–3], bracteate.</text>
      <biological_entity id="o14359" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o14360" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers bisexual;</text>
      <biological_entity id="o14361" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o14362" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o14363" name="androecium" name_original="androecium" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals [4–] 5 [–14], distinct or connate proximally, concave, relatively thick;</text>
      <biological_entity id="o14364" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="14" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s8" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5 (–8) [–14], distinct or connate proximally;</text>
      <biological_entity id="o14365" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectary disc absent;</text>
      <biological_entity constraint="nectary" id="o14366" name="disc" name_original="disc" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens (50–) 75–125 (–150), in 1–5 bundles or not bundled, connate proximally or distinct, usually adnate to petals or corolla;</text>
      <biological_entity id="o14367" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s11" to="75" to_inclusive="false" />
        <character char_type="range_value" from="125" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="150" />
        <character char_type="range_value" from="75" name="quantity" src="d0_s11" to="125" />
      </biological_entity>
      <biological_entity id="o14368" name="bundle" name_original="bundles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o14369" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="usually" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o14370" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="usually" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o14371" name="bundle" name_original="bundles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o14372" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="usually" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o14373" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="true" modifier="usually" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <relation from="o14367" id="r1227" name="in" negation="false" src="d0_s11" to="o14368" />
      <relation from="o14367" id="r1228" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14369" />
      <relation from="o14367" id="r1229" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14370" />
      <relation from="o14369" id="r1230" name="in" negation="false" src="d0_s11" to="o14371" />
      <relation from="o14370" id="r1231" name="in" negation="false" src="d0_s11" to="o14371" />
      <relation from="o14369" id="r1232" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14372" />
      <relation from="o14369" id="r1233" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14373" />
      <relation from="o14370" id="r1234" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14372" />
      <relation from="o14370" id="r1235" modifier="in 1-5 bundles or not bundled , connate or distinct , usually adnate to petals or corolla" name="in" negation="false" src="d0_s11" to="o14373" />
    </statement>
    <statement id="d0_s12">
      <text>anthers versatile [basifixed], 4-locular, dehiscent by longitudinal slits;</text>
      <biological_entity id="o14374" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s12" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="4-locular" value_original="4-locular" />
        <character constraint="by slits" constraintid="o14375" is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o14375" name="slit" name_original="slits" src="d0_s12" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistils 1, [3–] (4–) 5 (–6) [–10] -carpellate;</text>
      <biological_entity id="o14376" name="pistil" name_original="pistils" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="[3-](4-)5(-6)[-10]-carpellate" value_original="[3-](4-)5(-6)[-10]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, [3–] 5 [–10] -locular;</text>
    </statement>
    <statement id="d0_s15">
      <text>placentation primarily (falsely; H. Keng 1952) axile;</text>
      <biological_entity id="o14377" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="[3-]5[-10]-locular" value_original="[3-]5[-10]-locular" />
        <character is_modifier="false" modifier="primarily" name="placentation" src="d0_s15" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules anatropous (campylotropous), bitegmic, tenuinucellate;</text>
      <biological_entity id="o14378" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="tenuinucellate" value_original="tenuinucellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 1 or 5 [–6], simple [branched];</text>
      <biological_entity id="o14379" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="6" />
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas 1–5 [–7], usually lobed.</text>
      <biological_entity id="o14380" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="7" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s18" to="5" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s18" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsular, woody, dehiscence loculicidal and septicidal, [rarely irregular or fruits drupaceous].</text>
      <biological_entity id="o14381" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="texture" src="d0_s19" value="woody" value_original="woody" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 2–20, reddish-brown to dark-brown, compressed or lenticular [angular], sometimes winged, (testa vascularized, ± lignified);</text>
      <biological_entity id="o14382" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s20" to="20" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s20" to="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s20" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s20" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryo straight, rarely curved;</text>
      <biological_entity id="o14383" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="course" src="d0_s21" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s21" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endosperm nuclear, usually slight.</text>
      <biological_entity id="o14384" name="endosperm" name_original="endosperm" src="d0_s22" type="structure">
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s22" value="nuclear" value_original="nuclear" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s22" value="slight" value_original="slight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Tea Family</other_name>
  <discussion>Genera 9, species ca. 450 (3 genera, 4 species in the flora).</discussion>
  <discussion>Theaceae are found throughout warm-temperate, subtropical, and tropical forests; they are most diverse in the Old World in numbers of genera and species. Each genus is restricted to a single major geographic area (e.g., Asia versus New World tropics) except for Stewartia, with two species found in eastern North America and all other species restricted to Asia.</discussion>
  <discussion>Molecular phylogenetic analyses indicate that Theaceae should be defined to include only subfamily Theoideae of A. Cronquist (1981). These analyses place Theaceae in Ericales of the Asterid I clade (Angiosperm Phylogeny Group 1998, 2003); identification of the closest relative remains elusive despite the accumulation of many data sets (A. A. Anderberg et al. 2002). The other closely related members of Cronquist’s Theaceae, Ternstroemioideae (Ternstroemiaceae of Angiosperm Phylogeny Group 1998 and Anderberg et al.; now Pentaphylacaceae of Angiosperm Phylogeny Group 2003 and J. Schönenberger et al. 2005), do not occur in the flora area. Morphological (H. K. Airy Shaw 1936), developmental (Tsou C. H. 1998), and molecular data (L. M. Prince and C. R. Parks 2001) identify three tribes within Theaceae in the narrow sense. Relationships among the tribes remain unclear, and significant disagreement continues over the circumscription of genera and species (see Prince 2007). Taxonomy here follows Prince and Parks.</discussion>
  <discussion>There is a rich fossil record for Theaceae, beginning from late Cretaceous through Tertiary. Much of the fossil record was reviewed by P. J. Grote and D. L. Dilcher (1989). Some fossils attributed to Theaceae cannot be positively placed in the family due to poor preservation; care should be taken when considering the literature.</discussion>
  <discussion>Enough fossil specimens are available to state confidently that Theaceae in the narrow sense were distributed throughout the temperate Northern Hemisphere. Preserved organs include fruits, seeds, leaves (compressions), pollen, wood, and (perhaps) some flowers (amber). The number and distribution of known fossils indicate Theaceae were a conspicuous component of the forests of North America, Europe, and Asia in the past.</discussion>
  <discussion>Theaceae are economically important, including beverage plants (tea: primarily Camellia sinensis (Linnaeus) Kuntze), cooking oil (C. oleifera), ornamentals (Camellia spp., Franklinia alatamaha, Gordonia lasianthus, Polyspora spp., Schima spp., Stewartia spp.), timber (Polyspora spp., Schima spp.), and potential medicinals (Camellia spp.).</discussion>
  <references>
    <reference>Airy Shaw, H. K. 1936. Notes on the genus Schima and on the classification of the Theaceae–Camellioideae. Bull. Misc. Inform. Kew 1936: 496–500.</reference>
    <reference>Keng, H. 1952. Comparative morphological studies in Theaceae. Univ. Calif. Publ. Bot. 33: 269–384.</reference>
    <reference>Kobuski, C. E. 1951. Studies in the Theaceae, XXI. The species of Theaceae indigenous to the United States. J. Arnold Arbor. 32: 123–138.</reference>
    <reference>Prince, L. M. 2007. A brief nomenclatural review of genera and tribes in Theaceae. Aliso 24: 105–121.</reference>
    <reference>Prince, L. M. and C. R. Parks. 2001. Phylogenetic relationships of Theaceae inferred from chloroplast DNA data. Amer. J. Bot. 88: 2309–2320.</reference>
    <reference>Stevens, P. F. et al. 2004b. Theaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 9+ vols. Berlin etc. Vol. 6, pp. 463–471.</reference>
    <reference>Tsou, C. H. 1997. Embryology of the Theaceae—Anther and ovule development of Camellia, Franklinia, and Schima. Amer. J. Bot. 84: 369–381.</reference>
    <reference>Tsou, C. H. 1998. Early floral development of Camellioideae (Theaceae). Amer. J. Bot. 85: 1531–1547.</reference>
    <reference>Wood, C. E. Jr. 1959b. The genera of Theaceae of the southeastern United States. J. Arnold Arbor. 40: 413–419.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts usually persistent; capsules conic, columella absent.</description>
      <determination>3 Stewartia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence bracts deciduous; capsules ovoid or subglobose, columella present</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels (3-)5-7 cm; sepals persistent; capsule dehiscence loculicidal from apex only; seeds ovoid, apical wing prominent.</description>
      <determination>1 Gordonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels 0.2-1.5(-4) cm; sepals deciduous; capsule dehiscence loculicidal from apex and septicidal from base; seeds reniform, apical wing relatively narrow or absent.</description>
      <determination>2 Franklinia</determination>
    </key_statement>
  </key>
</bio:treatment>