<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">mitella</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">caulescens</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray,  Fl. N. Amer.</publication_title>
      <place_in_publication>1: 586. 1840  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus mitella;species caulescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065977</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mitellastra</taxon_name>
    <taxon_name authority="(Nuttall) Howell" date="unknown" rank="species">caulescens</taxon_name>
    <taxon_hierarchy>genus Mitellastra;species caulescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually stoloniferous.</text>
      <biological_entity id="o6255" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowering-stems 13–45 cm.</text>
      <biological_entity id="o6256" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1.5–14 cm, short-stipitate-glandular and, usually, long-stipitate-glandular, longer hairs spreading to retrorse, white;</text>
      <biological_entity id="o6257" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6258" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="longer" id="o6259" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="retrorse" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly cordate to broadly ovate, ± as long as wide, 1.2–6.5 (–8.5) × 1.4–7 (–8.2) cm, margins 3-lobed, 5-lobed, or 7-lobed, crenate-dentate, irregularly ciliate, apex of terminal lobe acute to obtuse, surfaces short-stipitate-glandular and sparsely long-stipitate-glandular;</text>
      <biological_entity id="o6260" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6261" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly cordate" name="shape" src="d0_s3" to="broadly ovate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6262" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="8.2" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6263" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="8.2" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6264" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="wide" value_original="wide" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" is_modifier="true" name="length" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s3" to="8.2" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" is_modifier="true" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="7-lobed" value_original="7-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6265" name="lobe" name_original="lobe" src="d0_s3" type="structure" />
      <relation from="o6261" id="r502" modifier="more or less" name="as long as" negation="false" src="d0_s3" to="o6262" />
      <relation from="o6261" id="r503" modifier="more or less" name="as long as" negation="false" src="d0_s3" to="o6263" />
      <relation from="o6261" id="r504" modifier="more or less" name="as long as" negation="false" src="d0_s3" to="o6264" />
      <relation from="o6262" id="r505" name="part_of" negation="false" src="d0_s3" to="o6265" />
      <relation from="o6263" id="r506" name="part_of" negation="false" src="d0_s3" to="o6265" />
      <relation from="o6264" id="r507" name="part_of" negation="false" src="d0_s3" to="o6265" />
    </statement>
    <statement id="d0_s4">
      <text>cauline leaves 1–3, proximal or mid cauline, alternate, sessile or petiolate, blade 0.3–4.3 (–6) × 0.4–4.4 (–7) cm.</text>
      <biological_entity id="o6266" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o6267" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s4" value="mid" value_original="mid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o6268" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="4.3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s4" to="4.3" to_unit="cm" />
        <character char_type="range_value" from="4.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s4" to="4.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–2 (–4), remotely 5–25-flowered, 1 (–2) flowers per node, (anthesis basipetalous), not secund, 13–45 cm, short-stipitate-glandular and sparsely long-stipitate-glandular proximally, short-stipitate-glandular distally.</text>
      <biological_entity id="o6269" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" modifier="remotely" name="architecture" src="d0_s5" value="5-25-flowered" value_original="5-25-flowered" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6270" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s5" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s5" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s5" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6271" name="node" name_original="node" src="d0_s5" type="structure" />
      <relation from="o6270" id="r508" name="per" negation="false" src="d0_s5" to="o6271" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 2–8 mm, short-stipitate-glandular.</text>
      <biological_entity id="o6272" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium broadly campanulate, 1.5–2 × 3–5.5 mm;</text>
      <biological_entity id="o6273" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6274" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals spreading or recurved, greenish, deltate-ovate, 0.9–1.5 × 1.4–2 mm;</text>
      <biological_entity id="o6275" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6276" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate-ovate" value_original="deltate-ovate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s8" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals whitish green or greenish, sometimes purple-tinged proximally, 5–9-lobed, 3–4 mm, lobes linear, lateral lobes spreading;</text>
      <biological_entity id="o6277" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6278" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish green" value_original="whitish green" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="sometimes; proximally" name="coloration" src="d0_s9" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="shape" src="d0_s9" value="5-9-lobed" value_original="5-9-lobed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6279" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6280" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 5, opposite sepals;</text>
      <biological_entity id="o6281" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6282" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o6283" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments usually purple proximally and white distally, sometimes entirely white, 0.6–0.9 mm;</text>
      <biological_entity id="o6284" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6285" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s11" value="purple proximally and white" value_original="purple proximally and white" />
        <character is_modifier="false" modifier="distally; sometimes entirely" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 0.2–0.4 × 0.2–0.4 mm;</text>
      <biological_entity id="o6286" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6287" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s12" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 1/2 inferior;</text>
      <biological_entity id="o6288" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6289" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s13" value="inferior" value_original="inferior" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles erect or spreading, cylindric, 0.8–1.1 mm;</text>
      <biological_entity id="o6290" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o6291" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas unlobed.</text>
      <biological_entity id="o6292" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o6293" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds reddish purple or dark reddish-brown, 0.7–1 mm, pitted.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o6294" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish purple" value_original="reddish purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6295" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep woods, meadows, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep woods" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Creeping mitrewort</other_name>
  
</bio:treatment>