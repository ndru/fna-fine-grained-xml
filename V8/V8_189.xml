<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">saxifragaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">heuchera</taxon_name>
    <taxon_name authority="(Torrey) Seringe in A. P. de Candolle and A. L. P. P. de Candolle" date="1830" rank="species">bracteata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle,  Prodr.</publication_title>
      <place_in_publication>4: 52. 1830  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family saxifragaceae;genus heuchera;species bracteata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065956</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tiarella</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">bracteata</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>2: 204. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tiarella;species bracteata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs usually subcaulescent;</text>
      <biological_entity id="o3280" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched or unbranched.</text>
      <biological_entity id="o3281" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowering-stems leafy, 6–28 (–38) cm, short-stipitate-glandular.</text>
      <biological_entity id="o3282" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="38" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="28" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole short to medium stipitate-glandular;</text>
      <biological_entity id="o3283" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3284" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="short to medium" value_original="short to medium" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade reniform or broadly ovate, shallowly 5–7-lobed, 1.5–4 cm, base subcordate or truncate, lobes rounded, margins sharply dentate, apex often mucronate, surfaces short-stipitate-glandular or sparsely long-stipitate-glandular abaxially, short-stipitate-glandular adaxially.</text>
      <biological_entity id="o3285" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3286" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3287" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o3288" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o3289" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o3290" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o3291" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences dense, (secund).</text>
      <biological_entity id="o3292" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium weakly bilaterally symmetric, free 0.5–1.5 mm, greenish yellow, narrowly campanulate, 3–5 mm, short-stipitate-glandular;</text>
      <biological_entity id="o3293" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3294" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="weakly bilaterally" name="architecture_or_shape" src="d0_s6" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, green-tipped, equal, 1–1.5 mm, apex rounded;</text>
      <biological_entity id="o3295" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3296" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green-tipped" value_original="green-tipped" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3297" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals erect, green, oblanceolate, unlobed, 2 mm, margins entire;</text>
      <biological_entity id="o3298" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3299" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3300" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens exserted 0.5–1 mm;</text>
      <biological_entity id="o3301" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3302" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles exserted 1 mm, 1.5–2 mm, 0.1+ mm diam.</text>
      <biological_entity id="o3303" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3304" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, 4–6 mm, beaks divergent, not papillose.</text>
      <biological_entity id="o3305" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3306" name="beak" name_original="beaks" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds brownish black, nearly straight along 1 side, convex on other side, 0.7–0.8 mm.</text>
      <biological_entity id="o3307" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brownish black" value_original="brownish black" />
        <character constraint="along side" constraintid="o3308" is_modifier="false" modifier="nearly" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character constraint="on side" constraintid="o3309" is_modifier="false" name="shape" notes="" src="d0_s12" value="convex" value_original="convex" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3308" name="side" name_original="side" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o3309" name="side" name_original="side" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded rocky ledges and outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded rocky ledges" />
        <character name="habitat" value="outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Rocky Mountain alumroot</other_name>
  <discussion>Heuchera bracteata occurs in the Rocky Mountains and foothills of northern Colorado and southern Wyoming.</discussion>
  
</bio:treatment>