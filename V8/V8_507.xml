<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard P. Wunderlin,R. David Whetstone</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="treatment_page">245</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">sapotaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CHRYSOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 192. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl.</publication_title>
      <place_in_publication>5, 88. 1754  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sapotaceae;genus CHRYSOPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chrysos, gold, and phyllon, leaf</other_info_on_name>
    <other_info_on_name type="fna_id">107005</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees.</text>
      <biological_entity id="o26606" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unarmed, densely hairy to glabrate.</text>
      <biological_entity id="o26607" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character char_type="range_value" from="densely hairy" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, alternate;</text>
      <biological_entity id="o26608" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent;</text>
      <biological_entity id="o26609" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o26610" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade: base cuneate to rounded, apex acute to acuminate [rounded or emarginate], surfaces densely hairy [glabrous] abaxially, usually glabrous adaxially.</text>
      <biological_entity id="o26611" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o26612" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o26613" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o26614" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences fascicles or solitary flowers.</text>
      <biological_entity id="o26615" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o26616" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 4–5 [–6] in 1 whorl, imbricate, abaxially densely hairy;</text>
      <biological_entity id="o26617" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26618" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character char_type="range_value" constraint="in whorl" constraintid="o26619" from="4" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o26619" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (4–) 5 (–6) [–8], greenish white or greenish yellow, sericeous adaxially, lobes undivided, equaling or shorter than [exceeding] corolla-tube;</text>
      <biological_entity id="o26620" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26621" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="6" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s8" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o26622" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than corolla-tube" constraintid="o26623" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o26623" name="corolla-tube" name_original="corolla-tube" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4–5 [–8], distinct;</text>
      <biological_entity id="o26624" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26625" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s9" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes absent [present];</text>
      <biological_entity id="o26626" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26627" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistil 3–5 (–6) [–12] -carpellate;</text>
      <biological_entity id="o26628" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26629" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5(-6)[-12]-carpellate" value_original="3-5(-6)[-12]-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary 3–6 [–12] -locular, densely hairy;</text>
      <biological_entity id="o26630" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26631" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-6[-12]-locular" value_original="3-6[-12]-locular" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>placentation axile.</text>
      <biological_entity id="o26632" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="placentation" src="d0_s13" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries purple to black [yellow, orange, red, brown, green], ellipsoid to ovoid, glabrous [hairy].</text>
      <biological_entity id="o26633" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s14" to="black" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="ovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1 [–5], brown, laterally compressed;</text>
      <biological_entity id="o26634" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="5" />
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hilum narrowly ovate to obovate;</text>
      <biological_entity id="o26635" name="hilum" name_original="hilum" src="d0_s16" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s16" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>embryo vertical;</text>
      <biological_entity id="o26636" name="embryo" name_original="embryo" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="vertical" value_original="vertical" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endosperm present.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = [12, 13, 14,] 26.</text>
      <biological_entity id="o26637" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o26638" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="[12" value_original="[12" />
        <character name="quantity" src="d0_s19" value="13" value_original="13" />
        <character name="quantity" src="d0_s19" value="14" value_original="14" />
        <character name="quantity" src="d0_s19" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, Asia, Africa, Indian Ocean Islands (Madagascar), Australia; nearly worldwide in tropics and subtropics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="nearly worldwide in tropics and subtropics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Cainito</other_name>
  <discussion>Species ca. 70 (1 in the flora).</discussion>
  <discussion>Most species of Chrysophyllum are located in the Neotropics. Species with edible fruits are numerous; C. africanum A. de Candolle, the “odara pear” or “African star apple,” is sold commercially. Chrysophyllum cainito Linnaeus, the star-apple, is cultivated for its foliage and fruit in south Florida. The star-apple is distinguished by having larger (3 cm in diameter), several-seeded fruits. Other species have valuable woods that are used in various ways.</discussion>
  
</bio:treatment>