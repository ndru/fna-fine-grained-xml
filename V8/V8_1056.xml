<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">526</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Arnott" date="1832" rank="subfamily">Vaccinioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">vaccinium</taxon_name>
    <taxon_name authority="A. Gray" date="1846" rank="section">Cyanococcus</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>3: 53. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily vaccinioideae;genus vaccinium;section cyanococcus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">317367</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, usually erect, 0.1-50 dm, rhizomatous, (twigs of previous season verrucose, perennating buds dimorphic).</text>
      <biological_entity id="o1039" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually deciduous, rarely persistent, (abaxial surface hairy or glabrous, adaxial surface usually glabrous).</text>
      <biological_entity id="o1040" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s1" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences corymbs, terminal on axillary shoots from buds of previous season.</text>
      <biological_entity constraint="inflorescences" id="o1041" name="corymb" name_original="corymbs" src="d0_s2" type="structure">
        <character constraint="on axillary shoots" constraintid="o1042" is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1042" name="shoot" name_original="shoots" src="d0_s2" type="structure" />
      <biological_entity id="o1043" name="bud" name_original="buds" src="d0_s2" type="structure" />
      <biological_entity constraint="previous" id="o1044" name="season" name_original="season" src="d0_s2" type="structure" />
      <relation from="o1042" id="r105" name="from" negation="false" src="d0_s2" to="o1043" />
      <relation from="o1043" id="r106" name="part_of" negation="false" src="d0_s2" to="o1044" />
    </statement>
    <statement id="d0_s3">
      <text>Pedicels articulated with calyx-tube.</text>
      <biological_entity id="o1045" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character constraint="with calyx-tube" constraintid="o1046" is_modifier="false" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
      </biological_entity>
      <biological_entity id="o1046" name="calyx-tube" name_original="calyx-tube" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 5;</text>
      <biological_entity id="o1047" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o1048" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 5, connate for nearly their entire lengths, corolla urceolate to cylindric;</text>
      <biological_entity id="o1049" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1050" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character constraint="for nearly their entire lengths" is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o1051" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character char_type="range_value" from="urceolate" name="shape" src="d0_s5" to="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens 10, included;</text>
      <biological_entity id="o1052" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1053" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="10" value_original="10" />
        <character is_modifier="false" name="position" src="d0_s6" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers without awns, tubules 2-4 mm, with terminal pores.</text>
      <biological_entity id="o1054" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1055" name="anther" name_original="anthers" src="d0_s7" type="structure" />
      <biological_entity id="o1056" name="awn" name_original="awns" src="d0_s7" type="structure" />
      <biological_entity id="o1057" name="tubule" name_original="tubules" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1058" name="pore" name_original="pores" src="d0_s7" type="structure" />
      <relation from="o1055" id="r107" name="without" negation="false" src="d0_s7" to="o1056" />
      <relation from="o1057" id="r108" name="with" negation="false" src="d0_s7" to="o1058" />
    </statement>
    <statement id="d0_s8">
      <text>Berries pseudo 10-locular.</text>
      <biological_entity id="o1059" name="berry" name_original="berries" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s8" value="10-locular" value_original="10-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds (4-) 10-25 (-40).</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24, 48, 72.</text>
      <biological_entity id="o1060" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="10" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="40" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1061" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
        <character name="quantity" src="d0_s10" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <number>45i.</number>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 9 (9 in the flora).</discussion>
  <discussion>Section Cyanococcus contains populations that are all homoploids, regardless of species, and are interfertile; naturally occurring hybrids have been described (S. P. Vander Kloet 1988 and references therein). The blueberries are endemic to North America; large-scale plantings elsewhere have resulted in the establishment of adventive populations.</discussion>
  <references>
    <reference>Bruederle, L. P. and N. Vorsa. 1994. Genetic differentiation of diploid blueberry, Vaccinium sect. Cyanococcus (Ericaceae). Syst. Bot. 19: 337–349.</reference>
    <reference>Qu, L., J. F. Hancock, and J. H. Whallon. 1998. Evolution in an autopolyploid group displaying predominantly bivalent pairing at meiosis: Genomic similarity of diploid Vaccinium darrowii and autotetraploid V. corymbosum (Ericaceae). Amer. J. Bot. 85: 698–703.</reference>
    <reference>Vander Kloet, S. P. 1976. Nomenclature, taxonomy, and biosystematics of Vaccinium section Cyanococcus (the blueberries) in North America: 1. Natural barriers to gene exchange between Vaccinium angustifolium Ait. and Vaccinium corymbosum L. Rhodora 78: 503–515.</reference>
  </references>
  
</bio:treatment>