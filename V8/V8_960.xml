<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="treatment_page">491</other_info_on_meta>
    <other_info_on_meta type="illustration_page">492</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Salisbury" date="1802" rank="genus">calluna</taxon_name>
    <taxon_name authority="(Linnaeus) Hull" date="1808" rank="species">vulgaris</taxon_name>
    <place_of_publication>
      <publication_title>Brit. Fl. ed.</publication_title>
      <place_in_publication>2, 114. 1808,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus calluna;species vulgaris;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220002166</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">vulgaris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 352. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erica;species vulgaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 15–60 (–100) cm.</text>
      <biological_entity id="o7565" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves widely spaced on leading shoots, closely spaced and imbricate on later shoots;</text>
      <biological_entity id="o7566" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="on leading shoots" constraintid="o7567" is_modifier="false" modifier="widely" name="arrangement" src="d0_s1" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="closely" name="arrangement" notes="" src="d0_s1" value="spaced" value_original="spaced" />
        <character constraint="on shoots" constraintid="o7568" is_modifier="false" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity constraint="leading" id="o7567" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity id="o7568" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="later" value_original="later" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade oblong-lanceolate to ovatelanceolate, 2.5–3.5 × 0.5–0.7 mm, base auriculate-clasping, surfaces glabrous, keeled abaxially, concave adaxially.</text>
      <biological_entity id="o7569" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7570" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o7571" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels: bracteoles 6–8 (4 distalmost simulating sepals).</text>
      <biological_entity id="o7572" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <biological_entity id="o7573" name="bracteole" name_original="bracteoles" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals exceeding corolla, pinkish purple to white, petaloid, 3–4 mm;</text>
      <biological_entity id="o7574" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o7575" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="pinkish purple" name="coloration" src="d0_s4" to="white" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7576" name="corolla" name_original="corolla" src="d0_s4" type="structure" />
      <relation from="o7575" id="r599" name="exceeding" negation="false" src="d0_s4" to="o7576" />
    </statement>
    <statement id="d0_s5">
      <text>corolla pinkish purple to white, lobes 2 (–3) mm;</text>
      <biological_entity id="o7577" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o7578" name="corolla" name_original="corolla" src="d0_s5" type="structure">
        <character char_type="range_value" from="pinkish purple" name="coloration" src="d0_s5" to="white" />
      </biological_entity>
      <biological_entity id="o7579" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments glabrous;</text>
      <biological_entity id="o7580" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7581" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 1 mm.</text>
      <biological_entity id="o7582" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7583" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 1–2 mm, hairy.</text>
      <biological_entity id="o7584" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 0.5–0.7 × 0.2–0.3 mm. 2n = 16.</text>
      <biological_entity id="o7585" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s9" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7586" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet acidic sites in bogs and fens, upland sites in old pastures and roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet acidic sites" constraint="in bogs and fens , upland sites in old pastures and roadsides" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="upland sites" constraint="in old pastures and roadsides" />
        <character name="habitat" value="old pastures" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S.; Conn., Mass., Mich., N.J., N.C., R.I., Vt., W.Va.; Europe; w Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Calluna vulgaris is well known as a constituent of moorlands in northern and western Europe, especially northern England, Ireland, and Scotland. The places where it is naturalized in North America are mostly coastal; inland it often is associated with railroads.</discussion>
  
</bio:treatment>