<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/13 14:59:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">481</other_info_on_meta>
    <other_info_on_meta type="treatment_page">483</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">ericaceae</taxon_name>
    <taxon_name authority="Link" date="1829" rank="subfamily">Ericoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">kalmia</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>138. 1788  ,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ericaceae;subfamily ericoideae;genus kalmia;species hirsuta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250065669</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaedaphne</taxon_name>
    <taxon_name authority="(Walter) Kuntze" date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_hierarchy>genus Chamaedaphne;species hirsuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalmia</taxon_name>
    <taxon_name authority="W. Bartram" date="unknown" rank="species">ciliata</taxon_name>
    <taxon_hierarchy>genus Kalmia;species ciliata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kalmiella</taxon_name>
    <taxon_name authority="(Walter) Small" date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_hierarchy>genus Kalmiella;species hirsuta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs spreading to erect, 0.3–0.6 (–1.2) m.</text>
      <biological_entity id="o14910" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="1.2" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs terete, viscid, puberulent, densely hispid.</text>
      <biological_entity id="o14911" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o14912" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.1–1 mm, hirsute-puberulent;</text>
      <biological_entity id="o14913" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute-puberulent" value_original="hirsute-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to ovate, 0.5–1.4 × 0.1–0.8 cm, margins slightly revolute, apex acute, rounded-apiculate, surfaces usually puberulent, hispid, and stipitate-glandular, rarely glabrous.</text>
      <biological_entity id="o14914" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14915" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o14916" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="rounded-apiculate" value_original="rounded-apiculate" />
      </biological_entity>
      <biological_entity id="o14917" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, scattered along stem, usually solitary flowers, sometimes fascicles or compact racemes, 2–5-flowered.</text>
      <biological_entity id="o14918" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character constraint="along stem" constraintid="o14919" is_modifier="false" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o14919" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <biological_entity id="o14920" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o14921" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sometimes" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-5-flowered" value_original="2-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 10–15 mm.</text>
      <biological_entity id="o14922" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals tardily deciduous, green, lanceolate, 3–8 mm, apex acute, surfaces puberulent, hispid, stipitate-glandular;</text>
      <biological_entity id="o14923" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14924" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14925" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14926" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals connate their entire lengths, pink (rarely white), red near anther pockets, with ring of red spots proximal to pockets, 8–10 × 10–15 mm, usually sparsely hirsute and stipitate-glandular on keels abaxially, puberulent at base adaxially;</text>
      <biological_entity id="o14927" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o14928" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="lengths" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character constraint="near anther pockets" constraintid="o14929" is_modifier="false" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" notes="" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character constraint="on keels" constraintid="o14933" is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="at base" constraintid="o14934" is_modifier="false" name="pubescence" notes="" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="anther" id="o14929" name="pocket" name_original="pockets" src="d0_s8" type="structure" />
      <biological_entity id="o14930" name="ring" name_original="ring" src="d0_s8" type="structure" />
      <biological_entity id="o14931" name="proximal" name_original="proximal" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="red spots" value_original="red spots" />
      </biological_entity>
      <biological_entity id="o14932" name="pocket" name_original="pockets" src="d0_s8" type="structure" />
      <biological_entity id="o14933" name="keel" name_original="keels" src="d0_s8" type="structure" />
      <biological_entity id="o14934" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o14928" id="r1275" name="with" negation="false" src="d0_s8" to="o14930" />
      <relation from="o14930" id="r1276" name="part_of" negation="false" src="d0_s8" to="o14931" />
      <relation from="o14930" id="r1277" name="to" negation="false" src="d0_s8" to="o14932" />
    </statement>
    <statement id="d0_s9">
      <text>filaments 3–4 mm;</text>
      <biological_entity id="o14935" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14936" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 5–7.5 mm.</text>
      <biological_entity id="o14937" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14938" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 5-locular, 2–3.5 × 2–4 mm, sparsely stipitate-glandular.</text>
      <biological_entity id="o14939" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="5-locular" value_original="5-locular" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds not winged, ovoid, 0.2–0.5 mm. 2n = 24.</text>
      <biological_entity id="o14940" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14941" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sandy savannas, sand hills, and pine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sandy savannas" />
        <character name="habitat" value="sand hills" />
        <character name="habitat" value="pine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Sandhill laurel</other_name>
  
</bio:treatment>