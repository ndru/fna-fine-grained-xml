<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="(Abrams) R. Knuth" date="1919" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Notizbl. Bot. Gart. Berlin-Dahlem</publication_title>
      <place_in_publication>7: 300. 1919</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species californica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101495</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthoxalis</taxon_name>
    <taxon_name authority="Abrams" date="1907" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>34: 264. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus xanthoxalis;species californica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxalis</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">albicans</taxon_name>
    <taxon_name authority="(Abrams) G. Eiten" date="unknown" rank="subspecies">californica</taxon_name>
    <taxon_hierarchy>genus oxalis;species albicans;subspecies californica</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">California wood-sorrel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, caulescent, rhizomes and stolons absent, bulbs absent.</text>
      <biological_entity id="o21829" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o21830" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21831" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21832" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial stems usually 2–8 from base, erect to ascending, 10–40 cm, becoming woody proximally, glabrous or very sparsely short-puberulent, hairs curved-ascending, nonseptate.</text>
      <biological_entity id="o21833" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" constraint="from base" constraintid="o21834" from="2" name="quantity" src="d0_s1" to="8" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="becoming; proximally" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s1" value="short-puberulent" value_original="short-puberulent" />
      </biological_entity>
      <biological_entity id="o21834" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o21835" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="curved-ascending" value_original="curved-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o21836" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules oblong, margins narrowly flanged, apical auricles absent;</text>
      <biological_entity id="o21837" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o21838" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="flanged" value_original="flanged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21839" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 3–8 cm, hairs nonseptate;</text>
      <biological_entity id="o21840" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21841" name="hair" name_original="hairs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3, gray-green, obcordate, 8–15 mm, lobed 1/5–1/3 length, surfaces sparsely strigose, oxalate deposits absent.</text>
      <biological_entity id="o21842" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="1/5" name="length" src="d0_s5" to="1/3" />
      </biological_entity>
      <biological_entity id="o21843" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o21844" name="deposit" name_original="deposits" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences umbelliform cymes, 1 (–3) -flowered;</text>
      <biological_entity id="o21845" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="1(-3)-flowered" value_original="1(-3)-flowered" />
      </biological_entity>
      <biological_entity id="o21846" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="umbelliform" value_original="umbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles 2–9 cm.</text>
      <biological_entity id="o21847" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers distylous;</text>
      <biological_entity id="o21848" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="distylous" value_original="distylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepal apices without tubercles, surfaces glabrous;</text>
      <biological_entity constraint="sepal" id="o21849" name="apex" name_original="apices" src="d0_s9" type="structure" />
      <biological_entity id="o21850" name="tubercle" name_original="tubercles" src="d0_s9" type="structure" />
      <biological_entity id="o21851" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21849" id="r1788" name="without" negation="false" src="d0_s9" to="o21850" />
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, with red lines proximally, 7–11 (–13) mm.</text>
      <biological_entity id="o21852" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21853" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
      <relation from="o21852" id="r1789" name="with" negation="false" src="d0_s10" to="o21853" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules angular-cylindric, abruptly tapering to apex, 10–15 mm, strigillose, hairs very short, nonseptate.</text>
      <biological_entity id="o21854" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="angular-cylindric" value_original="angular-cylindric" />
        <character constraint="to apex" constraintid="o21855" is_modifier="false" modifier="abruptly" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o21855" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity id="o21856" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oxalis californica is recognized by its caulescent habit, stems with reduced vestiture (glabrous or very sparsely short-puberulent), one (to three) flowers on long peduncles and pedicels, the yellow corollas often drying with a blue or purplish tinge, and relatively wide, glabrous, and usually purplish or pinkish tinged sepals.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Dec–)Feb–Apr(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes and flats, brushy ridges, roadside banks, canyon bottoms, rock outcrops, grasslands, oak chaparral, coastal sage scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="brushy ridges" />
        <character name="habitat" value="banks" modifier="roadside" />
        <character name="habitat" value="canyon bottoms" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="oak chaparral" />
        <character name="habitat" value="coastal sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(0–)30–800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="30" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>