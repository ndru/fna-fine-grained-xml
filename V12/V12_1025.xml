<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="genus">MORTONIA</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">sempervirens</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 35, plate 4. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus mortonia;species sempervirens</taxon_hierarchy>
    <other_info_on_name type="fna_id">220008836</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Texas mortonia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 0.6–1.5 m.</text>
      <biological_entity id="o5854" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade oblongelliptic to narrowly obovate, 3–5 × 1.5–2 mm, length 2–2.5 times width, rigid, base rounded, margins not thickened, revolute, apex narrowly obtuse to acute, mucronate, surfaces usually scabridulous, rarely glabrous.</text>
      <biological_entity id="o5855" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5856" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s1" to="narrowly obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s1" value="2-2.5" value_original="2-2.5" />
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
      </biological_entity>
      <biological_entity id="o5857" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5858" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o5859" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="narrowly obtuse" name="shape" src="d0_s1" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o5860" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences cymose racemes.</text>
      <biological_entity id="o5861" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o5862" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals deltate, 1 mm;</text>
      <biological_entity id="o5863" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o5864" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate" value_original="deltate" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals oblong, 2 mm.</text>
      <biological_entity id="o5865" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5866" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 5–6 mm.</text>
      <biological_entity id="o5867" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, Mortonia sempervirens occurs in trans-Pecos Texas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring; fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky limestone hills, ledges, stony prairies.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky limestone hills" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="stony prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>