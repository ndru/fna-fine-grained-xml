<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PARNASSIA</taxon_name>
    <taxon_name authority="Rafinesque" date="1840" rank="species">glauca</taxon_name>
    <place_of_publication>
      <publication_title>Autik. Bot.,</publication_title>
      <place_in_publication>42. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus parnassia;species glauca</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101472</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with caudices.</text>
      <biological_entity id="o5417" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o5418" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o5417" id="r479" name="with" negation="false" src="d0_s0" to="o5418" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 10–60 cm.</text>
      <biological_entity id="o5419" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal in rosettes;</text>
      <biological_entity id="o5420" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o5421" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5422" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <relation from="o5421" id="r480" name="in" negation="false" src="d0_s2" to="o5422" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 1.5–17 cm;</text>
      <biological_entity id="o5423" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5424" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (of larger leaves) oblong to orbiculate-ovate, 20–70 × 10–50 mm, longer than to ca. as long as wide, base cuneate to subcordate, apex obtuse to subacute;</text>
      <biological_entity id="o5425" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5426" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="orbiculate-ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
        <character constraint="as-long-as base, apex" constraintid="o5427, o5428" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o5427" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="subcordate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subacute" />
      </biological_entity>
      <biological_entity id="o5428" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="subcordate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline on proximal 1/2 of stem or absent.</text>
      <biological_entity id="o5429" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o5430" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5431" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o5432" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o5430" id="r481" name="on" negation="false" src="d0_s5" to="o5431" />
      <relation from="o5431" id="r482" name="part_of" negation="false" src="d0_s5" to="o5432" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals reflexed in fruit, oblong, elliptic, or ovate, 2.5–5 mm, margins hyaline, 0.2–0.5 mm wide, entire, apex rounded;</text>
      <biological_entity id="o5433" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5434" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o5435" is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5435" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o5436" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5437" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 5–12-veined, oblong to ovate, 9–18 × 6–10 mm, length 2–4 times sepals, base cuneate to rounded, margins entire or undulate;</text>
      <biological_entity id="o5438" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5439" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-12-veined" value_original="5-12-veined" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s7" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o5440" is_modifier="false" name="length" src="d0_s7" value="2-4 times sepals" value_original="2-4 times sepals" />
      </biological_entity>
      <biological_entity id="o5440" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o5441" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
      <biological_entity id="o5442" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 7–10 mm;</text>
      <biological_entity id="o5443" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5444" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1.2–2.8 mm;</text>
      <biological_entity id="o5445" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5446" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 3-fid almost to base, gland-tipped, 4–7 mm, shorter than to ca. equaling stamens, apical glands elliptic-oblong to subreniform, 0.3–0.7 mm;</text>
      <biological_entity id="o5447" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5448" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character constraint="to base" constraintid="o5449" is_modifier="false" name="shape" src="d0_s10" value="3-fid" value_original="3-fid" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character constraint="than to ca equaling stamens" constraintid="o5450" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5449" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o5450" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5451" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic-oblong" name="shape" src="d0_s10" to="subreniform" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary green.</text>
      <biological_entity id="o5452" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5453" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 12–14 mm. 2n = 32.</text>
      <biological_entity id="o5454" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5455" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The name Parnassia caroliniana has been misapplied to this species (for example, N. L. Britton and A. Brown 1913).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet meadows, shores, fens, ditches, seeps, wet, calcareous soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" modifier="seasonally" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="soils" modifier="wet calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr. (Nfld.), Ont., Que., Sask.; Conn., Ill., Ind., Iowa, Maine, Mass., Mich., Minn., N.H., N.J., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>