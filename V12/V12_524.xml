<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">478</other_info_on_meta>
    <other_info_on_meta type="mention_page">474</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHILADELPHUS</taxon_name>
    <taxon_name authority="W. H. Evans ex Rydberg in N. L. Britton et al." date="1905" rank="species">mearnsii</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>22: 174. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus philadelphus;species mearnsii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250043613</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Philadelphus</taxon_name>
    <taxon_name authority="S. Y. Hu" date="unknown" rank="species">hitchcockianus</taxon_name>
    <taxon_hierarchy>genus philadelphus;species hitchcockianus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mearnsii</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="subspecies">bifidus</taxon_name>
    <taxon_hierarchy>genus p.;species mearnsii;subspecies bifidus</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Mearns’s mock orange</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–15 (–40) dm.</text>
      <biological_entity id="o18292" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems tan-brown, weathering gray, striate, stiffly divaricately branched, strigose-sericeous;</text>
      <biological_entity id="o18293" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan-brown" value_original="tan-brown" />
        <character is_modifier="false" name="condition" src="d0_s1" value="weathering" value_original="weathering" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s1" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="stiffly divaricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="strigose-sericeous" value_original="strigose-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–3.5 cm;</text>
      <biological_entity id="o18294" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>short-shoots often present;</text>
      <biological_entity id="o18295" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary buds exposed.</text>
      <biological_entity constraint="axillary" id="o18296" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: petiole 0.5–4 mm;</text>
      <biological_entity id="o18297" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o18298" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade green or gray-green abaxially and adaxially, oblong-lanceolate, linear-lanceolate, or elliptic to ovate, 0.5–1.7 (–3) × 0.1–0.6 (–1.1) cm, coriaceous, margins entire, ± revolute, surfaces ± equally moderately strigose, hairs appressed, coarse, 0.2–0.6 (–0.9) mm, without understory of coiled-crisped hairs;</text>
      <biological_entity id="o18299" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o18300" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s6" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o18301" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o18302" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less equally moderately" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o18303" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="relief" src="d0_s6" value="coarse" value_original="coarse" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18304" name="understory" name_original="understory" src="d0_s6" type="structure" />
      <biological_entity id="o18305" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="coiled-crisped" value_original="coiled-crisped" />
      </biological_entity>
      <relation from="o18303" id="r1500" name="without" negation="false" src="d0_s6" to="o18304" />
      <relation from="o18304" id="r1501" name="part_of" negation="false" src="d0_s6" to="o18305" />
    </statement>
    <statement id="d0_s7">
      <text>veins inconspicuous.</text>
      <biological_entity id="o18306" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o18307" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences: flowers solitary, produced from previous years long-shoots, often appearing axillary.</text>
      <biological_entity id="o18308" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o18309" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s8" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="previous" id="o18310" name="year" name_original="years" src="d0_s8" type="structure" />
      <biological_entity constraint="previous" id="o18311" name="long-shoot" name_original="long-shoots" src="d0_s8" type="structure" />
      <relation from="o18309" id="r1502" name="produced from" negation="false" src="d0_s8" to="o18310" />
      <relation from="o18309" id="r1503" name="produced from" negation="false" src="d0_s8" to="o18311" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 0.5–2 mm.</text>
      <biological_entity id="o18312" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: hypanthium sparsely to moderately strigose-sericeous or glabrous;</text>
      <biological_entity id="o18313" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18314" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s10" value="strigose-sericeous" value_original="strigose-sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals ovatelanceolate, 2–3.5 (–4.2) × 1.3–2.3 mm, apex acute to acuminate-caudate, abaxial surface sparsely to moderately strigose-sericeous or glabrous, adaxial surface glabrous except villous along distal margins;</text>
      <biological_entity id="o18315" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18316" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s11" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18317" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate-caudate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18318" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="strigose-sericeous" value_original="strigose-sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18319" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="along distal margins" constraintid="o18320" is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18320" name="margin" name_original="margins" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals white, oblong-lanceolate to broadly oblong-ovate, (5–) 7–8.5 (–10) × 2.5–4.5 (–5.8) mm;</text>
      <biological_entity id="o18321" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18322" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s12" to="broadly oblong-ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s12" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="5.8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 13–18 (–24);</text>
      <biological_entity id="o18323" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18324" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="24" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s13" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct, 1.3–4 mm;</text>
      <biological_entity id="o18325" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o18326" name="all" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1, clavate, 2–3.2 mm, narrow base 0.5–1.5 mm;</text>
      <biological_entity id="o18327" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o18328" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18329" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmatic portion 1.3–2 mm, distally lobed to 0.5 mm.</text>
      <biological_entity id="o18330" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="stigmatic" id="o18331" name="portion" name_original="portion" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character constraint="to 0.5 mm" is_modifier="false" modifier="distally" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules ovoid-turbinate, 3–5.5 × 3.2–5.5 mm, sepals ± persistent at distal 1/3 or more distally, capsule distal surface impressed in 4 (–8) radial lines.</text>
      <biological_entity id="o18332" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid-turbinate" value_original="ovoid-turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s17" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18333" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character constraint="at " constraintid="o18335" is_modifier="false" modifier="more or less" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18334" name="1/3" name_original="1/3" src="d0_s17" type="structure" />
      <biological_entity id="o18335" name="capsule" name_original="capsule" src="d0_s17" type="structure" />
      <biological_entity constraint="distal" id="o18336" name="1/3" name_original="1/3" src="d0_s17" type="structure" />
      <biological_entity id="o18337" name="capsule" name_original="capsule" src="d0_s17" type="structure" />
      <biological_entity constraint="distal" id="o18338" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character constraint="in lines" constraintid="o18339" is_modifier="false" name="prominence" src="d0_s17" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o18339" name="line" name_original="lines" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="8" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="4" value_original="4" />
        <character is_modifier="true" name="arrangement" src="d0_s17" value="radial" value_original="radial" />
      </biological_entity>
      <relation from="o18335" id="r1504" name="at" negation="false" src="d0_s17" to="o18336" />
      <relation from="o18335" id="r1505" name="at" negation="false" src="d0_s17" to="o18337" />
    </statement>
    <statement id="d0_s18">
      <text>Seeds not caudate, 1–1.2 mm.</text>
      <biological_entity id="o18340" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s18" value="caudate" value_original="caudate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Philadelphus hitchcockianus, with its hypanthium and abaxial sepal surfaces glabrous, is here synonymized with P. mearnsii, which has its hypanthium and abaxial sepal surfaces moderately sericeous-strigose; substantial variation in this character is found in several populations. Hu S. Y. (1954–1956) alleged that P. hitchcockianus and P. mearnsii also differ in fruit size; however, that character is variable and overlapping.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun; fruiting May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone mountains, oak-pinyon zones.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone mountains" />
        <character name="habitat" value="oak-pinyon zones" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1800(–2200) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>