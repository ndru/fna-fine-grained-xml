<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Richard ex Brongniart" date="1826" rank="genus">COLUBRINA</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Fam. Rhamnées,</publication_title>
      <place_in_publication>61, plate 4, fig. 3. 1826</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus colubrina</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin coluber, racer snake, perhaps alluding to twisting of deep furrows on stems of some species</other_info_on_name>
    <other_info_on_name type="fna_id">107722</other_info_on_name>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Nakedwood</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, unarmed [armed with thorns];</text>
      <biological_entity id="o30024" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bud-scales present at least on short-shoots.</text>
      <biological_entity id="o30026" name="bud-scale" name_original="bud-scales" src="d0_s1" type="structure">
        <character constraint="on short-shoots" constraintid="o30027" is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o30027" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent or deciduous, alternate [opposite], sometimes fascicled on short-shoots;</text>
      <biological_entity id="o30028" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character constraint="on short-shoots" constraintid="o30029" is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o30029" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade not gland-dotted;</text>
    </statement>
    <statement id="d0_s4">
      <text>pinnately veined or 3-veined from base (acrodromous).</text>
      <biological_entity id="o30030" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character constraint="from base" constraintid="o30031" is_modifier="false" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o30031" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, usually cymes, thyrses, or fascicles, rarely flowers solitary;</text>
      <biological_entity id="o30032" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o30033" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
      <biological_entity id="o30034" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o30035" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels not fleshy in fruit.</text>
      <biological_entity id="o30036" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o30038" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30037" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o30038" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o30038" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o30039" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o30040" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium hemispheric, 2–3 mm wide;</text>
      <biological_entity id="o30041" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, spreading, brown to greenish, ovate-triangular to triangular, keeled adaxially;</text>
      <biological_entity id="o30042" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s10" to="greenish" />
        <character char_type="range_value" from="ovate-triangular" name="shape" src="d0_s10" to="triangular" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, greenish or yellowish to creamy white, ± hooded, spatulate or obovate, clawed;</text>
      <biological_entity id="o30043" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s11" to="creamy white" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary fleshy, adnate to and sometimes ± filling hypanthium;</text>
      <biological_entity id="o30044" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o30045" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure" />
      <relation from="o30044" id="r2463" modifier="sometimes more or less; more or less" name="filling" negation="false" src="d0_s12" to="o30045" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5;</text>
      <biological_entity id="o30046" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 1/2-inferior to inferior, 3 (–4) -locular;</text>
      <biological_entity id="o30047" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3(-4)-locular" value_original="3(-4)-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, connate proximally.</text>
      <biological_entity id="o30048" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, sometimes tardily dehiscent, 3-locular.</text>
      <biological_entity constraint="fruits" id="o30049" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sometimes tardily" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 30 (8 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, se Asia, Indian Ocean Islands (Madagascar), Pacific Islands (Hawaii); introduced widely.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="widely" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Branches of some Colubrina species are rigid but rarely produce thorns. Those of C. californica have been described as spinescent or subspinescent, alluding to short branches that sometimes are attenuate to relatively sharp points, but these are not the same as the clearly defined thorns that occur in other genera of the family.</discussion>
  <references>
    <reference>Johnston, M. C. 1971. Revision of Colubrina (Rhamnaceae). Brittonia 23: 2–53.</reference>
    <reference>Nesom, G. L. 2013. Taxonomic notes on Colubrina (Rhamnaceae). Phytoneuron 2013-4: 1–21.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade margins entire, mucronulate to obscurely serrulate, or crenulate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 1–2.5(–3.5) cm; Arizona, California, Nevada.</description>
      <determination>3. Colubrina californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 4–15 cm; Florida.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces tawny-tomentose, adaxial surfaces villous-strigose, glabrescent, secondary veins relatively straight, 6–12 pairs; inflorescences 20–50(–70)-flowered; peduncles 8–15 mm.</description>
      <determination>6. Colubrina cubensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces sparsely strigose or red-brown tomentose, glabrescent or persistently tomentose on veins, adaxial surfaces glabrous or tomentose and glabrescent, secondary veins arcuate, (4–)5–9 pairs; inflorescences 8–30-flowered; peduncles 1–10 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves deciduous, blades herbaceous, 4–9 cm, abaxial surfaces sparsely strigose, glabrescent, adaxial surfaces glabrous; peduncles 1–7 mm.</description>
      <determination>7. Colubrina elliptica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves persistent, blades subcoriaceous, 5–15 cm, both surfaces tomentose, abaxial glabrescent except persistently tomentose on veins, adaxial glabrescent; peduncles (3–)5–10 mm.</description>
      <determination>8. Colubrina arborescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade margins serrate, serrulate, or crenate-serrate.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Shrubs, erect to sprawling or clambering; leaf blade surfaces glabrous or glabrate; Florida.</description>
      <determination>5. Colubrina asiatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Shrubs or trees, erect, (rarely clambering in C. greggii); leaf blade surfaces (one or both) hairy; sc, sw United States.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades 1–3(–4) cm, margins with 2–20 teeth per side.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades (1–)3–4 cm, margins with 10–20 teeth per side; inflorescences fascicles, 2–4(–7)-flowered, or flowers solitary, peduncles absent.</description>
      <determination>1. Colubrina texensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades 1–2.5(–3.5) cm, margins with 2–7 teeth per side; inflorescences usually cymes or thyrses, 2–12-flowered, sometimes flowers solitary, peduncles 1–2 mm.</description>
      <determination>3. Colubrina californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades 3–12 cm, margins with 40–100 teeth per side.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades 3–7.5 cm, margins with 40–70 teeth per side; inflorescences 6–15-flowered, peduncles 2–8 mm.</description>
      <determination>2. Colubrina stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades (3–)4–12 cm, margins with 50–100 teeth per side; inflorescences (10–)20–40-flowered; peduncles 5–20 mm.</description>
      <determination>4. Colubrina greggii</determination>
    </key_statement>
  </key>
</bio:treatment>