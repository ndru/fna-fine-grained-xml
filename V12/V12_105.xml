<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="S. Watson" date="1886" rank="species">gracillima</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>21: 438. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species gracillima</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101578</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(S. Watson) Millspaugh" date="unknown" rank="species">gracillima</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species gracillima</taxon_hierarchy>
  </taxon_identification>
  <number>44.</number>
  <other_name type="common_name">Mexican sandmat</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with slender taproot.</text>
      <biological_entity id="o1737" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity constraint="slender" id="o1738" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o1737" id="r148" name="with" negation="false" src="d0_s0" to="o1738" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, 5–25 cm, glabrous.</text>
      <biological_entity id="o1739" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o1740" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules distinct, subulate-filiform, entire, 0.3–0.5 mm, glabrous;</text>
      <biological_entity id="o1741" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate-filiform" value_original="subulate-filiform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.4–0.9 mm, glabrous;</text>
      <biological_entity id="o1742" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly oblong to linear, often slightly falcate, 2–15 × 0.3–0.8 mm, base symmetric to subsymmetric, rounded to attenuate, margins entire, thickened and often revolute, apex acute to obtuse, surfaces glabrous;</text>
      <biological_entity id="o1743" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s5" to="linear" />
        <character is_modifier="false" modifier="often slightly" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1744" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="symmetric" name="architecture" src="d0_s5" to="subsymmetric" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
      <biological_entity id="o1745" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o1746" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o1747" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>only midvein conspicuous.</text>
      <biological_entity id="o1748" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal nodes;</text>
      <biological_entity id="o1749" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o1750" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1750" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.1–0.9 mm.</text>
      <biological_entity id="o1751" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre turbinate, 0.4–0.5 × 0.4–0.6 mm, glabrous;</text>
      <biological_entity id="o1752" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s9" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellow to pink, oblong to slightly reniform, (0–) 0.1–1 × 0.1–0.2 mm;</text>
      <biological_entity id="o1753" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="pink" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="slightly reniform" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s10" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s10" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white to pink, ovate to oblong, 0.2–0.4 × 0.1–0.3 mm, distal margin usually entire, rarely emarginate.</text>
      <biological_entity id="o1754" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s11" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1755" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s11" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 5–12.</text>
      <biological_entity id="o1756" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o1757" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1758" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.1–0.2 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o1759" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1760" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules broadly ovoid, 1.1–1.4 mm diam., glabrous;</text>
      <biological_entity id="o1761" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="diameter" src="d0_s15" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 0.8–1.1 mm.</text>
      <biological_entity id="o1762" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds orange to tan or reddish-brown, narrowly ovoid, 4-angled in cross-section, 0.9–1.1 × 0.5–0.6 mm, smooth.</text>
      <biological_entity id="o1763" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s17" to="tan or reddish-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character constraint="in cross-section" constraintid="o1764" is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" notes="" src="d0_s17" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" notes="" src="d0_s17" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1764" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Euphorbia gracillima occurs from south-central Arizona (Pima and Pinal counties) south through northwestern Mexico.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and dry washes in desert scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="dry washes" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Chihuahua, Jalisco, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>