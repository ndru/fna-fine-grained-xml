<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">OXALIDACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">OXALIS</taxon_name>
    <taxon_name authority="Aiten ex G. Don" date="1831" rank="species">bowiei</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Hist.</publication_title>
      <place_in_publication>1: 761. 1831</place_in_publication>
      <other_info_on_pub>(as bowii)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species bowiei</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101504</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxalis</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">purpurata</taxon_name>
    <taxon_name authority="(Herbert) Sonder" date="unknown" rank="variety">bowiei</taxon_name>
    <taxon_hierarchy>genus oxalis;species purpurata;variety bowiei</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Red-flower wood-sorrel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, acaulous, rhizomes present vertical, slender or thickened, sparsely scaly, stolons absent, bulbs solitary, ovate, 2–4 cm;</text>
      <biological_entity id="o25426" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulous" value_original="acaulous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o25427" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s0" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o25428" name="stolon" name_original="stolons" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o25429" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulb scales 5-nerved.</text>
      <biological_entity constraint="bulb" id="o25430" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="5-nerved" value_original="5-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, rarely absent at flowering;</text>
      <biological_entity id="o25431" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character constraint="at flowering" is_modifier="false" modifier="rarely" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole (4–) 6–16 cm, densely glandular-puberulent;</text>
      <biological_entity id="o25432" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3, green to purplish abaxially, green adaxially, obcordate, (12–) 30–60 mm, lobed 1/6–1/3 length, lobes apically convex, often fleshy, surfaces densely glandular-puberulent, oxalate deposits absent.</text>
      <biological_entity id="o25433" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="purplish abaxially" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="60" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="1/6" name="length" src="d0_s4" to="1/3" />
      </biological_entity>
      <biological_entity id="o25434" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o25435" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity constraint="oxalate" id="o25436" name="deposit" name_original="deposits" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences umbelliform cymes, 4–12-flowered;</text>
      <biological_entity id="o25437" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="4-12-flowered" value_original="4-12-flowered" />
      </biological_entity>
      <biological_entity id="o25438" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="umbelliform" value_original="umbelliform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scapes 15–20 cm, densely glandular-puberulent.</text>
      <biological_entity id="o25439" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers heterostylous;</text>
      <biological_entity id="o25440" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="heterostylous" value_original="heterostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepal apices without tubercles;</text>
      <biological_entity constraint="sepal" id="o25441" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o25442" name="tubercle" name_original="tubercles" src="d0_s8" type="structure" />
      <relation from="o25441" id="r2068" name="without" negation="false" src="d0_s8" to="o25442" />
    </statement>
    <statement id="d0_s9">
      <text>petals greenish yellow basally, pink to deep rose-pink or red distally, 15–20 mm.</text>
      <biological_entity id="o25443" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s9" value="greenish yellow" value_original="greenish yellow" />
        <character char_type="range_value" from="pink" modifier="distally" name="coloration" src="d0_s9" to="deep rose-pink or red" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules not seen.</text>
      <biological_entity id="o25444" name="capsule" name_original="capsules" src="d0_s10" type="structure" />
    </statement>
  </description>
  <discussion>Oxalis bowiei is a naturalized garden escape in Oroville (Butte County; V. H. Oswald and L. Ahart 1994). Oxalis bowiei Aiton ex G. Don, from the Cape of Good Hope, was described as hoary-pubescent with peduncles about equal in length to the leaves and with red flowers. It perhaps is not the same species as O. bowiei Herbert (1833), provenance unspecified, but the color illustration clearly shows the commonly cultivated plant of contemporary commerce. Apparently neither name has been typified.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Dec, Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Oct" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Africa (South Africa); introduced also in Europe, Asia (China), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Africa (South Africa)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>