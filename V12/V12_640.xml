<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="(Reichenbach) Engelmann" date="1852" rank="section">Linopsis</taxon_name>
    <taxon_name authority="(Small) H. J. P. Winkler in H. G. A. Engler et al." date="1931" rank="species">arenicola</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 19a: 116. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linopsis;species arenicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101699</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cathartolinum</taxon_name>
    <taxon_name authority="Small in N. L. Britton et al." date="1907" rank="species">arenicola</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>25: 75. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cathartolinum;species arenicola</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Sand flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial (flowering 1st year), 25–70 cm, glabrous.</text>
      <biological_entity id="o26828" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually multiple from base, sometimes 1, unbranched or few-branched proximal to inflorescence, slender, wiry, prominently ribbed in inflorescence.</text>
      <biological_entity id="o26829" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from base" constraintid="o26830" is_modifier="false" modifier="usually" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="size" notes="" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="wiry" value_original="wiry" />
        <character constraint="in inflorescence" constraintid="o26832" is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s1" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o26830" name="base" name_original="base" src="d0_s1" type="structure">
        <character modifier="sometimes" name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o26831" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <biological_entity id="o26832" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves early deciduous, alternate or basalmost opposite, appressed-ascending;</text>
      <biological_entity id="o26833" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed-ascending" value_original="appressed-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands present, reddish, becoming dark;</text>
      <biological_entity constraint="stipular" id="o26834" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s3" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 5–15 × 0.5–1.2 mm, margins entire or with scattered minute marginal glands, not ciliate, apex acute;</text>
      <biological_entity id="o26835" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26836" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with scattered minute marginal glands" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" notes="" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o26837" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o26836" id="r2198" name="with" negation="false" src="d0_s4" to="o26837" />
    </statement>
    <statement id="d0_s5">
      <text>1-nerved.</text>
      <biological_entity id="o26838" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymes.</text>
      <biological_entity constraint="inflorescences" id="o26839" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0–2 mm.</text>
      <biological_entity id="o26840" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals persistent, lanceolate to ovate or inner ones sometimes obovate, outer sepals 2.5–3.6 mm, margins hyaline, not scarious, all glandular-toothed, apex acuminate;</text>
      <biological_entity id="o26841" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26842" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o26843" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o26844" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26845" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s8" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o26846" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals yellow, obovate, 4–6.5 mm;</text>
      <biological_entity id="o26847" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26848" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3 mm;</text>
      <biological_entity id="o26849" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26850" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.3–0.7 mm;</text>
      <biological_entity id="o26851" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26852" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodia present or absent;</text>
      <biological_entity id="o26853" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26854" name="staminodium" name_original="staminodia" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles distinct, 2–3 mm;</text>
      <biological_entity id="o26855" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o26856" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas capitate.</text>
      <biological_entity id="o26857" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o26858" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules pyriform, 2–2.5 mm diam., apex pointed, dehiscing readily into 10, 1-seeded segments, segments falling freely, false septa incomplete, margins of septa ciliate.</text>
      <biological_entity id="o26859" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26860" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="pointed" value_original="pointed" />
        <character constraint="into segments" constraintid="o26861" is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o26861" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="10" value_original="10" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="1-seeded" value_original="1-seeded" />
      </biological_entity>
      <biological_entity id="o26862" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="freely" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="false" id="o26863" name="septum" name_original="septa" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="incomplete" value_original="incomplete" />
      </biological_entity>
      <biological_entity id="o26864" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26865" name="septum" name_original="septa" src="d0_s15" type="structure" />
      <relation from="o26864" id="r2199" name="part_of" negation="false" src="d0_s15" to="o26865" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–1.5 × 0.6–1 mm. 2n = 36.</text>
      <biological_entity id="o26866" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s16" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26867" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>All parts of Linum arenicola flowers are yellow; the stamens are held close to the styles, with anthers at the same level as stigmas. The staminodia are low, deltoid, and less than 0.5 mm. Linum arenicola is only known from about nine sites in Miami-Dade County and the Florida Keys in Monroe County. Its habitat of pine rocklands has been almost completely destroyed by urban development and altered fire regimes. J. R. McDill (2009) reported that L. arenicola grouped with L. rupestre (southwestern United States), L. flagellare (Small) H. J. P. Winkler (southcentral Mexico), and L. bahamense Northrop (Bahamas), all perennials with many branches arising from a woody taproot or caudex. Linum arenicola and L. bahamense both occur on calcareous soils, and C. M. Rogers (1984) considered them to be closely related.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Jun(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow soils of ephemeral pools, calcareous soils, slash pine woods over oölite, pine-palmetto rocklands, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow soils" constraint="of ephemeral pools , calcareous soils ," />
        <character name="habitat" value="ephemeral pools" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="pine woods" modifier="slash" constraint="over oölite , pine-palmetto rocklands , disturbed areas" />
        <character name="habitat" value="oölite" />
        <character name="habitat" value="pine-palmetto rocklands" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>