<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">54</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">FRANGULA</taxon_name>
    <taxon_name authority="(Eschscholtz) A. Gray" date="1849" rank="species">californica</taxon_name>
    <taxon_name authority="(Jepson) Kartesz &amp; Gandhi" date="1994" rank="subspecies">crassifolia</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>76: 448. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus frangula;species californica;subspecies crassifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101475</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhamnus</taxon_name>
    <taxon_name authority="Eschscholtz" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="Jepson" date="1925" rank="variety">crassifolia</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>615. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus rhamnus;species californica;variety crassifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Jepson) C. B. Wolf" date="unknown" rank="subspecies">crassifolia</taxon_name>
    <taxon_hierarchy>genus r.;species californica;subspecies crassifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">tomentella</taxon_name>
    <taxon_name authority="(Jepson) J. O. Sawyer" date="unknown" rank="subspecies">crassifolia</taxon_name>
    <taxon_hierarchy>genus r.;species tomentella;subspecies crassifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1d.</number>
  <other_name type="common_name">Thickleaf coffeeberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades green to gray-green adaxially, broadly elliptic to oblongelliptic, 8–10 cm, margins usually entire, sometimes serrulate, flat, apex obtuse to rounded, abaxial surface densely and closely white stellate-hairy, without intermixed simple, erect hairs, adaxial surface moderately to densely hirsutulous and sparsely stellate-hairy;</text>
      <biological_entity id="o20343" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="green" modifier="adaxially" name="coloration" src="d0_s0" to="gray-green" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s0" to="oblongelliptic" />
        <character char_type="range_value" from="8" from_unit="cm" name="distance" src="d0_s0" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20344" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s0" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s0" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o20345" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s0" to="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20346" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="closely" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o20347" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20348" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o20346" id="r1668" name="without" negation="false" src="d0_s0" to="o20347" />
    </statement>
    <statement id="d0_s1">
      <text>veins prominent abaxially.</text>
      <biological_entity id="o20349" name="vein" name_original="veins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Drupe stones 2.</text>
      <biological_entity constraint="drupe" id="o20350" name="stone" name_original="stones" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>