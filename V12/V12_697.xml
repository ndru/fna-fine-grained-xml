<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="(A. Gray) Small in N. L. Britton et al." date="1907" rank="genus">HESPEROLINON</taxon_name>
    <taxon_name authority="(Greene) Small in N. L. Britton et al." date="1907" rank="species">clevelandii</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>25: 85. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus hesperolinon;species clevelandii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101735</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linum</taxon_name>
    <taxon_name authority="Greene" date="1882" rank="species">clevelandii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>9: 121. 1882</place_in_publication>
      <other_info_on_pub>(as clevelandi)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus linum;species clevelandii</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Allen Springs dwarf flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 5–20 (–30) cm, glabrous or glabrate;</text>
    </statement>
    <statement id="d0_s1">
      <text>unbranched proximally or proximal branches whorled, branches from distal nodes dichotomous, widely spreading.</text>
      <biological_entity id="o31266" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31267" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
      </biological_entity>
      <biological_entity id="o31268" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" notes="" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="distal" id="o31269" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="dichotomous" value_original="dichotomous" />
      </biological_entity>
      <relation from="o31268" id="r2556" name="from" negation="false" src="d0_s1" to="o31269" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o31270" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands very inconspicuous, present at proximal nodes, absent distally;</text>
      <biological_entity constraint="stipular" id="o31271" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="very" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="at proximal nodes" constraintid="o31272" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31272" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="presence" notes="" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear or narrowly oblong, 10–13 (–20) × 2–2.5 mm, base flat, not clasping, margins without stalked glands.</text>
      <biological_entity id="o31273" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31274" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o31275" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o31276" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o31275" id="r2557" name="without" negation="false" src="d0_s4" to="o31276" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: cymes monochasial (scorpioid or helicoid), open, branches unequal (main axis obvious), internodes long, flowers widely scattered;</text>
      <biological_entity id="o31277" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o31278" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="monochasial" value_original="monochasial" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o31279" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o31280" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o31281" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bract margins without prominent glands.</text>
      <biological_entity id="o31282" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="bract" id="o31283" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o31284" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o31283" id="r2558" name="without" negation="false" src="d0_s6" to="o31284" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 5–25 mm, scarcely longer in fruit, spreading at angles 70–80 (–90) °, scarcely bent at apex.</text>
      <biological_entity id="o31285" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
        <character constraint="in fruit" constraintid="o31286" is_modifier="false" modifier="scarcely" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="at angles" constraintid="o31287" is_modifier="false" name="orientation" notes="" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="at apex" constraintid="o31288" is_modifier="false" modifier="scarcely" name="shape" notes="" src="d0_s7" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o31286" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o31287" name="angle" name_original="angles" src="d0_s7" type="structure">
        <character name="degree" src="d0_s7" value="70-80(-90)°" value_original="70-80(-90)°" />
      </biological_entity>
      <biological_entity id="o31288" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals erect or reflexed at tip, lanceolate, 1.5–2.5 mm, usually equal, sometimes one larger, marginal glands absent or minute, surfaces glabrous;</text>
      <biological_entity id="o31289" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o31290" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="at tip" constraintid="o31291" is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o31291" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="marginal" id="o31292" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s8" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o31293" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals not or slightly spreading at anthesis, yellow, often with reddish or orange streak on midvein, oblanceolate, sometimes obovate, 0.5–2.5 (–4) mm, apex notched or erose;</text>
      <biological_entity id="o31294" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o31295" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31296" name="midvein" name_original="midvein" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="orange streak" value_original="orange streak" />
      </biological_entity>
      <biological_entity id="o31297" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="notched" value_original="notched" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o31295" id="r2559" modifier="often" name="with" negation="false" src="d0_s9" to="o31296" />
    </statement>
    <statement id="d0_s10">
      <text>cup yellow, rim with petal attachment protruding prominently in sinus or strongly indented;</text>
      <biological_entity id="o31298" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o31299" name="cup" name_original="cup" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o31300" name="rim" name_original="rim" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="indented" value_original="indented" />
      </biological_entity>
      <biological_entity constraint="petal" id="o31301" name="attachment" name_original="attachment" src="d0_s10" type="structure">
        <character constraint="in sinus" constraintid="o31302" is_modifier="false" name="prominence" src="d0_s10" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity id="o31302" name="sinus" name_original="sinus" src="d0_s10" type="structure" />
      <relation from="o31300" id="r2560" name="with" negation="false" src="d0_s10" to="o31301" />
    </statement>
    <statement id="d0_s11">
      <text>stamens included;</text>
      <biological_entity id="o31303" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o31304" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 1–2 mm;</text>
      <biological_entity id="o31305" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31306" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers yellow, dehisced anthers 0.5–0.8 (–1.2) mm;</text>
      <biological_entity id="o31307" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o31308" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31309" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <relation from="o31308" id="r2561" name="dehisced" negation="false" src="d0_s13" to="o31309" />
    </statement>
    <statement id="d0_s14">
      <text>ovary chambers 6;</text>
      <biological_entity id="o31310" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="ovary" id="o31311" name="chamber" name_original="chambers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, yellow, 0.5–1 (–1.8) mm, included.</text>
      <biological_entity id="o31312" name="flower" name_original="flowers" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 36.</text>
      <biological_entity id="o31313" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31314" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hesperolinon clevelandii occurs in the inner North Coast Ranges from Mendocino to Napa counties and on the Mount Hamilton Range in Santa Clara and Stanislaus counties. It can be distinguished from H. micranthum by its yellow stamens and petals. The flowers in Mount Hamilton populations may be twice as large as those of other populations and might warrant recognition as a subspecies (H. K. Sharsmith 1961).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral margins, oak woodlands, ponderosa pine woodlands, serpentine or volcanic soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral margins" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="pine woodlands" modifier="ponderosa" />
        <character name="habitat" value="serpentine" />
        <character name="habitat" value="volcanic soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>