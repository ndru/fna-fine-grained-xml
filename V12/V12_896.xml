<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="illustration_page">236</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Zuccarini" date="1832" rank="species">antisyphilitica</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>15(2, Beibl.): 58. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species antisyphilitica;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101531</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Candelilla</other_name>
  <other_name type="common_name">wax plant</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, with much-branched, fleshy rootstock.</text>
      <biological_entity id="o11424" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o11425" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="much-branched" value_original="much-branched" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o11424" id="r943" name="with" negation="false" src="d0_s0" to="o11425" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, few branched, 25–50 (–100) cm, glabrous or puberulent, pencil-like, in age covered with flaky, exfoliating layer of wax.</text>
      <biological_entity id="o11426" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="shape" src="d0_s1" value="pencil-like" value_original="pencil-like" />
      </biological_entity>
      <biological_entity id="o11427" name="age" name_original="age" src="d0_s1" type="structure" />
      <biological_entity id="o11428" name="layer" name_original="layer" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
        <character is_modifier="true" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o11429" name="wax" name_original="wax" src="d0_s1" type="substance">
        <character is_modifier="true" name="fragility" src="d0_s1" value="flaky" value_original="flaky" />
        <character is_modifier="true" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <relation from="o11426" id="r944" name="in" negation="false" src="d0_s1" to="o11427" />
      <relation from="o11427" id="r945" name="covered with" negation="false" src="d0_s1" to="o11428" />
      <relation from="o11427" id="r946" name="covered with" negation="false" src="d0_s1" to="o11429" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate usually caducous, sometimes persisting;</text>
      <biological_entity id="o11430" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules 0.4–0.5 mm;</text>
      <biological_entity id="o11431" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole absent;</text>
      <biological_entity id="o11432" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to deltate-subulate, 2.5–4 × 1 mm, thick, fleshy, base usually rounded and swollen, rarely cuneate, margins entire, apex acute, surfaces puberulent, adaxial sometimes canescent;</text>
      <biological_entity id="o11433" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="deltate-subulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character name="width" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o11434" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o11435" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11436" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11437" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation inconspicuous.</text>
      <biological_entity constraint="adaxial" id="o11438" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in axillary congested cymes, near branch tips or solitary at distal nodes;</text>
      <biological_entity id="o11439" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="axillary" id="o11440" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity constraint="branch" id="o11441" name="tip" name_original="tips" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o11442" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o11439" id="r947" name="in" negation="false" src="d0_s7" to="o11440" />
      <relation from="o11439" id="r948" name="near" negation="false" src="d0_s7" to="o11441" />
      <relation from="o11439" id="r949" name="near" negation="false" src="d0_s7" to="o11442" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0–1 mm, lanulose.</text>
      <biological_entity id="o11443" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="lanulose" value_original="lanulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1.6–2.2 × 1.6–1.9 mm, puberulent to canescent;</text>
      <biological_entity id="o11444" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s9" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s9" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s9" to="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 5, pinkish, narrowly oblong to reniform, 0.3–0.4 × 0.8–1 mm;</text>
      <biological_entity id="o11445" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s10" to="reniform" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s10" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white to pink, ovate, oblong, or transversely oblong, 1.3–2.5 × 1.4–2.5 mm, usually erose, rarely entire.</text>
      <biological_entity id="o11446" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 50–70.</text>
      <biological_entity id="o11447" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="70" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o11448" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11449" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.9–1.1 mm, 2-fid nearly entire length.</text>
      <biological_entity id="o11450" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11451" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong to ovoid, 3.9–4.2 × 3.6–3.9 mm, glabrous;</text>
      <biological_entity id="o11452" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="ovoid" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="length" src="d0_s15" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="3.6" from_unit="mm" name="width" src="d0_s15" to="3.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 3.1–3.3 mm.</text>
      <biological_entity id="o11453" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s16" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds whitish gray, narrowly ovoid, 2.4–3.1 × 1.4–1.6 mm, irregularly rugose-tuberculate;</text>
      <biological_entity id="o11454" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish gray" value_original="whitish gray" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s17" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s17" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly" name="relief" src="d0_s17" value="rugose-tuberculate" value_original="rugose-tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle crescent-shaped, 0.3–0.6 × 0.6–0.8 mm.</text>
      <biological_entity id="o11455" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="crescent--shaped" value_original="crescent--shaped" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s18" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s18" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia antisyphilitica is the only pencil-stemmed species of Euphorbia occurring in the flora area. The species is characteristic of the Chihuahuan Desert scrub of Mexico from Chihuahua and Coahuila south to Hidalgo and Querétaro, and barely enters into the United States in southern New Mexico (Doña Ana and Lincoln counties) and southwest (Brewster, Hudspeth, Presidio, and Terrell counties) and south (Starr and Webb counties) Texas. The stems are covered in a conspicuous coat of exfoliating wax, and the plants historically have been harvested for this product, although the practice is much less prevalent now. The specific epithet refers to its traditional medicinal use in treating sexually transmitted infections.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round in response to sufficient rainfall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in response to sufficient rainfall" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="in response to sufficient rainfall" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub, frequently on limestone substrates.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="limestone" modifier="frequently" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>