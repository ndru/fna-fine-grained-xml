<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Richard ex Brongniart" date="1826" rank="genus">COLUBRINA</taxon_name>
    <taxon_name authority="I. M. Johnston" date="1924" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 12: 1085. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus colubrina;species californica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101384</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Colubrina</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="species">texensis</taxon_name>
    <taxon_name authority="(I. M. Johnston) L. D. Benson" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus colubrina;species texensis;variety californica</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Las Animas nakedwood</other_name>
  <other_name type="common_name">California snakebush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect, 1–2 (–3, rarely to 8) m.</text>
      <biological_entity id="o16702" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± straight, intricately branched, white-tomentose, glabrescent.</text>
      <biological_entity id="o16703" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="intricately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, sometimes fascicled;</text>
      <biological_entity id="o16704" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–4 mm;</text>
      <biological_entity id="o16705" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic to oblongelliptic or elliptic-obovate, 1–2.5 (–3.5) cm, coriaceous, base rounded to cuneate, margins entire or mucronulate to obscurely serrulate, teeth (0–) 2–7 per side, apex rounded to truncate, both surfaces usually minutely hirtellous to sparsely pilose, abaxial sometimes strigose;</text>
      <biological_entity id="o16706" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="oblongelliptic or elliptic-obovate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o16707" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
      <biological_entity id="o16708" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="mucronulate" name="shape" src="d0_s4" to="obscurely serrulate" />
      </biological_entity>
      <biological_entity id="o16709" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s4" to="2" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o16710" from="2" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
      <biological_entity id="o16710" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o16711" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate" />
      </biological_entity>
      <biological_entity id="o16712" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually minutely hirtellous" name="pubescence" src="d0_s4" to="sparsely pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pinnately veined, secondary-veins 3–4 (–5) pairs, ± straight.</text>
      <biological_entity constraint="abaxial" id="o16713" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s5" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o16714" name="secondary-vein" name_original="secondary-veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymes or thyrses, 2–12-flowered, or flowers solitary;</text>
      <biological_entity id="o16715" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-12-flowered" value_original="2-12-flowered" />
      </biological_entity>
      <biological_entity id="o16716" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-12-flowered" value_original="2-12-flowered" />
      </biological_entity>
      <biological_entity id="o16717" name="thyrse" name_original="thyrses" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-12-flowered" value_original="2-12-flowered" />
      </biological_entity>
      <biological_entity id="o16718" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncles 1–2 mm;</text>
    </statement>
    <statement id="d0_s8">
      <text>fruiting pedicels 2–4 mm.</text>
      <biological_entity id="o16719" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16720" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o16719" id="r1390" name="fruiting" negation="false" src="d0_s8" to="o16720" />
    </statement>
    <statement id="d0_s9">
      <text>Capsules 7–9 mm.</text>
      <biological_entity id="o16721" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In Arizona, Colubrina californica occurs in La Paz, Maricopa, Pima, Pinal, and Yuma counties; in California, all known records are from Imperial, Riverside, and San Diego counties; in Nevada it is known from Clark County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Dec–)Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy washes, arroyos, alluvial slopes and fans, granite slopes, creosote bush and desert scrubs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy washes" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="alluvial slopes" />
        <character name="habitat" value="fans" />
        <character name="habitat" value="granite slopes" />
        <character name="habitat" value="bush" modifier="creosote" />
        <character name="habitat" value="desert scrubs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(100–)300–1000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>