<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">FRANGULA</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray ex J. G. Cooper" date="1857" rank="species">purshiana</taxon_name>
    <taxon_name authority="J. O. Sawyer &amp; S. W. Edwards" date="2007" rank="subspecies">ultramafica</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>54: 172, fig. 1. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus frangula;species purshiana;subspecies ultramafica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101312</other_info_on_name>
  </taxon_identification>
  <number>3c.</number>
  <other_name type="common_name">Caribou coffeeberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2 m, shrublike.</text>
      <biological_entity id="o6909" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="shrublike" value_original="shrublike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs green to gray or dull brown, densely hairy.</text>
      <biological_entity id="o6910" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="gray or dull brown" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous or semideciduous;</text>
      <biological_entity id="o6911" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s2" value="semideciduous" value_original="semideciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly oblong or broadly elliptic to ovate or obovate, (3.5–) 5–10 cm, distinctly coriaceous, base mostly obtuse or tapered, margins serrulate or entire, often wavy, apex obtuse, often notched, surfaces papillate, sparsely to densely hairy or adaxial velvety, glaucous adaxially when fresh.</text>
      <biological_entity id="o6912" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s3" to="ovate or obovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="distinctly" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o6913" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o6914" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o6915" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o6916" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6917" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="velvety" value_original="velvety" />
        <character is_modifier="false" modifier="when fresh" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Of the three subspecies of Frangula purshiana, subsp. ultramafica is the most distinctive and might warrant species status. It appears to be restricted to the Feather River complex of serpentinized peridotite and associated mafic and ultramafic substrates near Bucks Lake in Plumas County. Its firm, bluish or greenish gray leaves are suggestive of evergreen F. californica subsp. tomentella, but they are broader and larger and bear only simple, erect hairs. The leaves are deciduous as in F. rubra, but the large, broad leaves and fruits with three stones are like those of F. purshiana.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seeps, montane chaparral, open forests over mafic and ultramafic substrates.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seeps" />
        <character name="habitat" value="montane chaparral" />
        <character name="habitat" value="open forests" constraint="over mafic and ultramafic substrates" />
        <character name="habitat" value="mafic" />
        <character name="habitat" value="ultramafic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>