<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">VITACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VITIS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Vitis</taxon_name>
    <taxon_name authority="House" date="1921" rank="species">shuttleworthii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>7: 129. 1921</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family vitaceae;genus vitis;subgenus vitis;species shuttleworthii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101305</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vitis</taxon_name>
    <taxon_name authority="Shuttleworth ex Planchon in A. L. P. de Candolle and C. de Candolle" date="1887" rank="species">coriacea</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Phan.</publication_title>
      <place_in_publication>5: 345. 1887</place_in_publication>
      <other_info_on_pub>not Miquel 1863</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus vitis;species coriacea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="Engelmann ex Durand" date="unknown" rank="species">candicans</taxon_name>
    <taxon_name authority="L. H. Bailey" date="unknown" rank="variety">coriacea</taxon_name>
    <taxon_hierarchy>genus v.;species candicans;variety coriacea</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Calloosa or leatherleaf or Florida grape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderately high climbing, sparsely branched.</text>
      <biological_entity id="o6109" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="moderately" name="height" src="d0_s0" value="high" value_original="high" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branches: bark exfoliating in shreds;</text>
      <biological_entity id="o6110" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o6111" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character constraint="in shreds" constraintid="o6112" is_modifier="false" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o6112" name="shred" name_original="shreds" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodal diaphragms 2.5–6 mm thick, sometimes continuing halfway into internode;</text>
      <biological_entity id="o6113" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity constraint="nodal" id="o6114" name="diaphragm" name_original="diaphragms" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="thickness" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6115" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character is_modifier="true" name="position" src="d0_s2" value="halfway" value_original="halfway" />
      </biological_entity>
      <relation from="o6114" id="r539" modifier="sometimes" name="continuing" negation="false" src="d0_s2" to="o6115" />
    </statement>
    <statement id="d0_s3">
      <text>branchlets subterete to terete, densely to sparsely tomentose, growing tips not enveloped by unfolding leaves;</text>
      <biological_entity id="o6116" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o6117" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="subterete" name="shape" src="d0_s3" to="terete" />
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character constraint="by leaves" constraintid="o6119" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="enveloped" value_original="enveloped" />
      </biological_entity>
      <biological_entity id="o6118" name="tip" name_original="tips" src="d0_s3" type="structure" />
      <biological_entity id="o6119" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <relation from="o6117" id="r540" name="growing" negation="false" src="d0_s3" to="o6118" />
    </statement>
    <statement id="d0_s4">
      <text>tendrils along length of branchlets, persistent, branched, tendrils (or inflorescences) at only 2 consecutive nodes;</text>
      <biological_entity id="o6120" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <biological_entity id="o6121" name="tendril" name_original="tendrils" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o6122" name="branchlet" name_original="branchlets" src="d0_s4" type="structure" />
      <biological_entity id="o6123" name="tendril" name_original="tendrils" src="d0_s4" type="structure" />
      <biological_entity id="o6124" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="consecutive" value_original="consecutive" />
      </biological_entity>
      <relation from="o6121" id="r541" name="along" negation="false" src="d0_s4" to="o6122" />
      <relation from="o6123" id="r542" name="at" negation="false" src="d0_s4" to="o6124" />
    </statement>
    <statement id="d0_s5">
      <text>nodes not red-banded.</text>
      <biological_entity id="o6125" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o6126" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s5" value="red-banded" value_original="red-banded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves: stipules less than 1 mm;</text>
      <biological_entity id="o6127" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o6128" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole 1/2–3/4 blade;</text>
      <biological_entity id="o6129" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o6130" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s7" to="3/4" />
      </biological_entity>
      <biological_entity id="o6131" name="blade" name_original="blade" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>blade broadly cordate to nearly reniform, 4–12 cm, usually unlobed, sometimes 3-shouldered, infrequently deeply 3–5 lobed, apex acute to obtuse, abaxial surface not glaucous, densely white to rusty tomentose, concealed (except sometimes veins) by hairs, adaxial surface floccose to glabrate.</text>
      <biological_entity id="o6132" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o6133" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly cordate" name="shape" src="d0_s8" to="nearly reniform" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="3-shouldered" value_original="3-shouldered" />
        <character char_type="range_value" from="3" modifier="infrequently deeply; deeply" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o6134" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6135" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="densely white" name="coloration" src="d0_s8" to="rusty" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character constraint="by hairs" constraintid="o6136" is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o6136" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o6137" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s8" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 4–10 cm.</text>
      <biological_entity id="o6138" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers functionally unisexual.</text>
      <biological_entity id="o6139" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries dark red to purple-black, slightly or not glaucous, globose, 12+ mm diam., skin separating from pulp;</text>
      <biological_entity id="o6140" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s11" to="purple-black" />
        <character is_modifier="false" modifier="slightly; not" name="pubescence" src="d0_s11" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s11" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6141" name="skin" name_original="skin" src="d0_s11" type="structure">
        <character constraint="from pulp" constraintid="o6142" is_modifier="false" name="arrangement" src="d0_s11" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o6142" name="pulp" name_original="pulp" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lenticels absent.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 38.</text>
      <biological_entity id="o6143" name="lenticel" name_original="lenticels" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6144" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vitis shuttleworthii is endemic to peninsular Florida and apparently is the closest relative of V. mustangensis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–early May; fruiting Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Well-drained pinelands, thickets.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="well-drained pinelands" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>