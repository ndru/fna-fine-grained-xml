<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">ELATINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ELATINE</taxon_name>
    <taxon_name authority="Schkuhr" date="1789–1791" rank="species">triandra</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Handb.</publication_title>
      <place_in_publication>1: 345, plate 109b, fig. 2. 1789–1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family elatinaceae;genus elatine;species triandra</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200014279</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elatine</taxon_name>
    <taxon_name authority="(Nylander) Kauffmann" date="unknown" rank="species">callitrichoides</taxon_name>
    <taxon_hierarchy>genus elatine;species callitrichoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triandra</taxon_name>
    <taxon_name authority="Nylander" date="unknown" rank="variety">callitrichoides</taxon_name>
    <taxon_hierarchy>genus e.;species triandra;variety callitrichoides</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Three-stamened waterwort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, submersed, 2–10 cm.</text>
      <biological_entity id="o12894" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, highly branched.</text>
      <biological_entity id="o12895" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves light green to green;</text>
      <biological_entity id="o12896" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s2" to="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules triangular or ovatelanceolate, 0.7–1 mm, margins dentate, apex acute or obtuse;</text>
      <biological_entity id="o12897" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12898" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o12899" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–3 mm;</text>
      <biological_entity id="o12900" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear, lanceolate, or narrowly oblong, 3–10 × 1.5–3.5 mm, base attenuate, apex acute or obtuse.</text>
      <biological_entity id="o12901" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12902" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o12903" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.3–0.4 mm, erect.</text>
      <biological_entity id="o12904" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals (2–) 3 (–4), usually 1 reduced, ovate, 0.5 (–0.7) × 0.2–0.4 mm;</text>
      <biological_entity id="o12905" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12906" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="4" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character modifier="usually" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="0.7" to_unit="mm" />
        <character name="length" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 3, white or reddish, widely ovate or elliptic, 0.6–1.2 × 0.5 mm;</text>
      <biological_entity id="o12907" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12908" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="widely" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s8" to="1.2" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3;</text>
      <biological_entity id="o12909" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12910" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 3.</text>
      <biological_entity id="o12911" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12912" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules compressed-globose, 3-locular, 1.2–1.7 [–2] mm diam.</text>
      <biological_entity id="o12913" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="compressed-globose" value_original="compressed-globose" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="3-locular" value_original="3-locular" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="diameter" src="d0_s11" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 10–25 per locule, oblong, straight or slightly curved, 0.4–0.5 × 0.1–0.2 mm;</text>
      <biological_entity id="o12914" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o12915" from="10" name="quantity" src="d0_s12" to="25" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s12" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12915" name="locule" name_original="locule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>pits angular-hexagonal, length 2–3 times width, in 6–8 rows, 16–25 per row.</text>
      <biological_entity id="o12917" name="row" name_original="rows" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s13" to="8" />
        <character char_type="range_value" constraint="per row" constraintid="o12918" from="16" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
      <biological_entity id="o12918" name="row" name_original="row" src="d0_s13" type="structure" />
      <relation from="o12916" id="r1082" name="in" negation="false" src="d0_s13" to="o12917" />
    </statement>
    <statement id="d0_s14">
      <text>2n = ca. 40.</text>
      <biological_entity id="o12916" name="pit" name_original="pits" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="angular-hexagonal" value_original="angular-hexagonal" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s13" value="2-3" value_original="2-3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12919" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Elatine triandra is a popular aquarium plant. Reports of E. triandra from Yukon were based on misidentification of Callitriche hermaphroditica (B. Bennett et al. 2010). Elatine triandra is a common weed in rice fields of south and southeast Asia (K. Moody 1989), thus rice farming or aquarium trade may have been responsible for its introduction.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shores, pools.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shores" />
        <character name="habitat" value="pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1100(–2500) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., N.W.T., Ont., Sask.; Ala., Ariz., Calif., Colo., Conn., Ga., La., Maine, Mass., Minn., Nebr., Nev., N.J., N.Y., N.C., N.Dak., Okla., Pa., S.C., Tex., Utah, Va., Wis.; e, se Asia; also introduced in South America (Brazil), Europe, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="also  in South America (Brazil)" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>