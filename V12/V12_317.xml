<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">PHYLLANTHACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PHYLLANTHUS</taxon_name>
    <taxon_name authority="Brandegee" date="1905" rank="species">evanescens</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>5: 207. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phyllanthaceae;genus phyllanthus;species evanescens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101671</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phyllanthus</taxon_name>
    <taxon_name authority="L. C. Wheeler" date="unknown" rank="species">pudens</taxon_name>
    <taxon_hierarchy>genus phyllanthus;species pudens</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Birdseed leafflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, monoecious, 1–5 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>branching not phyllanthoid.</text>
      <biological_entity id="o6918" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems proximally terete, distally compressed, winged, scabridulous.</text>
      <biological_entity id="o6919" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves distichous, all well developed;</text>
      <biological_entity id="o6920" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distichous" value_original="distichous" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules auriculate, pale green with pale-brown margins;</text>
      <biological_entity id="o6921" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character constraint="with margins" constraintid="o6922" is_modifier="false" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o6922" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic or oblong, 8–20 × 2.5–10 mm, base obtuse to rounded, apex acute to obtuse, both surfaces glabrous.</text>
      <biological_entity id="o6923" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6924" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o6925" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o6926" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymules, bisexual, with 1–2 (–3) pistillate flowers and 1–3 staminate flowers.</text>
      <biological_entity constraint="inflorescences" id="o6927" name="cymule" name_original="cymules" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6929" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o6927" id="r611" name="with" negation="false" src="d0_s6" to="o6929" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: staminate 0.5–0.8 mm, pistillate sharply reflexed in fruit, (1–) 1.4–1.8 (–2.2) mm.</text>
      <biological_entity id="o6930" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character constraint="in fruit" constraintid="o6931" is_modifier="false" modifier="sharply" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s7" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6931" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 5–6, pale brownish green with narrow white margins, flat, 0.5–0.7 mm;</text>
      <biological_entity id="o6932" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6933" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale brownish" value_original="pale brownish" />
        <character constraint="with margins" constraintid="o6934" is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s8" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6934" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary extrastaminal, 5–6 glands;</text>
      <biological_entity id="o6935" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6936" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="extrastaminal" value_original="extrastaminal" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity id="o6937" name="gland" name_original="glands" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 3, filaments connate basally to most of length.</text>
      <biological_entity id="o6938" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6939" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o6940" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="length" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals (5–) 6, green (sometimes tinged pink) with narrow white margins, flat, 0.7–1.2 mm, 1-veined;</text>
      <biological_entity id="o6941" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6942" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
        <character constraint="with margins" constraintid="o6943" is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s11" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o6943" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary annular, unlobed.</text>
      <biological_entity id="o6944" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6945" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="annular" value_original="annular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 2.8–3.2 mm diam., smooth.</text>
      <biological_entity id="o6946" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="diameter" src="d0_s13" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds uniformly brown, 1.3–1.5 mm, irregularly verrucose.</text>
      <biological_entity id="o6947" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly" name="relief" src="d0_s14" value="verrucose" value_original="verrucose" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants from the United States and northeastern Mexico generally have been called Phyllanthus pudens and from the rest of Mexico and Central America P. evanescens. Characters used to distinguish these species (fruiting pedicel length and seed size) overlap broadly and recent authors treat them as synonyms (G. L. Webster 2001; V. W. Steinmann 2007). A report of P. evanescens (as P. pudens) from Arkansas (E. Sundell et al. 1999) is based on introduced plants in a nursery; it does not appear to have become established there. The species is introduced in Alabama, first collected there in 2012.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal prairies, mesquite brushlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal prairies" />
        <character name="habitat" value="mesquite brushlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., La., Tex.; Mexico; Central America (Nicaragua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Nicaragua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>