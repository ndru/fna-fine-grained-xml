<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann in W. H. Emory" date="1859" rank="species">lata</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 188. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species lata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101585</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1857" rank="species">dilatata</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>2(4): 175. 1857</place_in_publication>
      <other_info_on_pub>not Hochstetter ex A. Richard  1850</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus euphorbia;species dilatata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Engelmann) Small" date="unknown" rank="species">lata</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species lata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">rinconis</taxon_name>
    <taxon_hierarchy>genus e.;species rinconis</taxon_hierarchy>
  </taxon_identification>
  <number>55.</number>
  <other_name type="common_name">Broadleaf spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with moderately thickened to robust rootstock.</text>
      <biological_entity id="o26313" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o26314" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="moderately" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="fragility" src="d0_s0" value="robust" value_original="robust" />
      </biological_entity>
      <relation from="o26313" id="r2131" name="with" negation="false" src="d0_s0" to="o26314" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, or prostrate, 10–25 cm, strigose to short-sericeous or ± villous.</text>
      <biological_entity id="o26315" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect or prostrate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect or prostrate" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s1" to="short-sericeous or more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o26316" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules distinct, filiform, 0.8–1.3 mm, strigose to short-sericeous or ± villous;</text>
      <biological_entity id="o26317" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s3" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s3" to="short-sericeous or more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–2 mm, densely strigose to short-sericeous or ± villous;</text>
      <biological_entity id="o26318" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="densely strigose" name="pubescence" src="d0_s4" to="short-sericeous or more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly to broadly ovate-deltate, older ones often falcate, 4–12 × 3–7 mm, base asymmetric, obliquely rounded to obtuse, noticeably wider on one side, margins entire, often ± revolute, apex broadly acute, surfaces strigose to short-sericeous or ± villous;</text>
      <biological_entity id="o26319" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s5" value="ovate-deltate" value_original="ovate-deltate" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="older" value_original="older" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26320" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="obliquely rounded" name="shape" src="d0_s5" to="obtuse" />
        <character constraint="on side" constraintid="o26321" is_modifier="false" modifier="noticeably" name="width" src="d0_s5" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o26321" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o26322" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="often more or less" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o26323" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>obscurely 3–5-veined from base, midvein prominent abaxially.</text>
      <biological_entity id="o26324" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s5" to="short-sericeous or more or less villous" />
        <character constraint="from base" constraintid="o26325" is_modifier="false" modifier="obscurely" name="architecture" src="d0_s6" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o26325" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o26326" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal nodes;</text>
      <biological_entity id="o26327" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o26328" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26328" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 1–3 mm.</text>
      <biological_entity id="o26329" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre broadly campanulate, 2–2.5 × 2.2–2.6, strigose;</text>
      <biological_entity id="o26330" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="2.5" />
        <character char_type="range_value" from="2.2" name="quantity" src="d0_s9" to="2.6" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, greenish, oblong to semilunate, 0.2–0.7 × 0.6–1 mm;</text>
      <biological_entity id="o26331" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="semilunate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s10" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages rudimentary or white, forming narrow band, (0–) 0.1–0.2 × (0–) 0.6–1 mm, distal margin entire or crenate.</text>
      <biological_entity id="o26332" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s11" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s11" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_width" src="d0_s11" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26333" name="band" name_original="band" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="distal" id="o26334" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="crenate" value_original="crenate" />
      </biological_entity>
      <relation from="o26332" id="r2132" name="forming" negation="false" src="d0_s11" to="o26333" />
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 25–35.</text>
      <biological_entity id="o26335" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary densely strigose to short-sericeous or ± villous;</text>
      <biological_entity id="o26336" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26337" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="densely strigose" name="pubescence" src="d0_s13" to="short-sericeous or more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles dark purplish, 0.8–1.2 mm, 2-fid 1/2 to nearly entire length.</text>
      <biological_entity id="o26338" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26339" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark purplish" value_original="dark purplish" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid, 1.9–2.3 × 2–2.4 mm, strigose to short-sericeous or ± villous;</text>
      <biological_entity id="o26340" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s15" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s15" to="short-sericeous or more or less villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.7–2.2 mm.</text>
      <biological_entity id="o26341" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s16" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds whitish, oblong, 4-angled in cross-section, faces concave, 1.5–1.8 (–2) × 0.6–0.9 mm, smooth.</text>
      <biological_entity id="o26342" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character constraint="in cross-section" constraintid="o26343" is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
      </biological_entity>
      <biological_entity id="o26343" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 28, 56.</text>
      <biological_entity id="o26344" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s17" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s17" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s17" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26345" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
        <character name="quantity" src="d0_s18" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountain slopes, canyons, basins, rocky prairies, roadsides, disturbed sites, usually in calcareous soils, sometimes in igneous-derived, sandy or rocky soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountain slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="basins" />
        <character name="habitat" value="rocky prairies" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="igneous-derived" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., N.Mex., Okla., Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>