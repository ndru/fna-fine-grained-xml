<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jinshuang Ma</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="P. Browne" date="1756" rank="genus">CROSSOPETALUM</taxon_name>
    <place_of_publication>
      <publication_title>Civ. Nat. Hist. Jamaica,</publication_title>
      <place_in_publication>145, plate 16, fig. 1. 1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus crossopetalum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek krossos, fringe, and petalon, petal, alluding to fimbriate petals of the type species</other_info_on_name>
    <other_info_on_name type="fna_id">108429</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees.</text>
      <biological_entity id="o31934" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets terete.</text>
      <biological_entity id="o31936" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite or whorled;</text>
      <biological_entity id="o31937" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules present;</text>
      <biological_entity id="o31938" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o31939" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade margins spiny-toothed, crenate, or entire;</text>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate.</text>
      <biological_entity constraint="blade" id="o31940" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="spiny-toothed" value_original="spiny-toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences axillary, cymes.</text>
      <biological_entity id="o31941" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o31942" name="cyme" name_original="cymes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, radially symmetric;</text>
      <biological_entity id="o31943" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o31944" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o31945" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium absent;</text>
      <biological_entity id="o31946" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4, distinct;</text>
      <biological_entity id="o31947" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 4, white, pale green, reddish, or purplish;</text>
      <biological_entity id="o31948" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary intrastaminal, annular, fleshy;</text>
      <biological_entity id="o31949" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="intrastaminal" value_original="intrastaminal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="annular" value_original="annular" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 4, free from and inserted under nectary;</text>
      <biological_entity id="o31950" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character constraint="under nectary" constraintid="o31951" is_modifier="false" name="position" src="d0_s14" value="free-inserted" value_original="free-inserted" />
      </biological_entity>
      <biological_entity id="o31951" name="nectary" name_original="nectary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes 0;</text>
      <biological_entity id="o31952" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistil 4-carpellate;</text>
      <biological_entity id="o31953" name="pistil" name_original="pistil" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="4-carpellate" value_original="4-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary superior, immersed in and adnate to nectary, 4-locular, placentation axile;</text>
      <biological_entity id="o31954" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="superior" value_original="superior" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="immersed" value_original="immersed" />
        <character constraint="to nectary" constraintid="o31955" is_modifier="false" name="fusion" src="d0_s17" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" notes="" src="d0_s17" value="4-locular" value_original="4-locular" />
        <character is_modifier="false" name="placentation" src="d0_s17" value="axile" value_original="axile" />
      </biological_entity>
      <biological_entity id="o31955" name="nectary" name_original="nectary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1;</text>
      <biological_entity id="o31956" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 4;</text>
      <biological_entity id="o31957" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovule 1 per locule.</text>
      <biological_entity id="o31958" name="ovule" name_original="ovule" src="d0_s20" type="structure">
        <character constraint="per locule" constraintid="o31959" name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31959" name="locule" name_original="locule" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>Fruits drupes, red, 1 [–2] -locular by abortion, obovoid or subglobose, apex not beaked.</text>
      <biological_entity constraint="fruits" id="o31960" name="drupe" name_original="drupes" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="red" value_original="red" />
        <character constraint="by abortion" constraintid="o31961" is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s21" value="1[-2]-locular" value_original="1[-2]-locular" />
        <character is_modifier="false" name="shape" src="d0_s21" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s21" value="subglobose" value_original="subglobose" />
      </biological_entity>
      <biological_entity id="o31961" name="abortion" name_original="abortion" src="d0_s21" type="structure" />
      <biological_entity id="o31962" name="apex" name_original="apex" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s21" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 1 [–2] per fruit, obovoid, not winged;</text>
      <biological_entity id="o31963" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s22" to="2" />
        <character constraint="per fruit" constraintid="o31964" name="quantity" src="d0_s22" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s22" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o31964" name="fruit" name_original="fruit" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>aril absent.</text>
      <biological_entity id="o31965" name="aril" name_original="aril" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 35 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, to 0.5 m; stems spreading or prostrate; leaf margins spiny-toothed; pedicels 3–7 mm.</description>
      <determination>1. Crossopetalum ilicifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, 3–4 m; stems erect; leaf margins crenate or entire; pedicels 7–12 mm.</description>
      <determination>2. Crossopetalum rhacoma</determination>
    </key_statement>
  </key>
</bio:treatment>