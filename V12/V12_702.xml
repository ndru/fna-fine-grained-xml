<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="illustration_page">315</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Graham) Baillon" date="1858" rank="section">Poinsettia</taxon_name>
    <taxon_name authority="Engelmann in W. H. Emory" date="1859" rank="species">bifurcata</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 190. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section poinsettia;species bifurcata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101656</other_info_on_name>
  </taxon_identification>
  <number>130.</number>
  <other_name type="common_name">Forked spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with slender, fibrous taproot.</text>
      <biological_entity id="o24963" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24964" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <relation from="o24963" id="r2029" name="with" negation="false" src="d0_s0" to="o24964" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 20–70 cm, glabrous or with few scattered spreading hairs;</text>
      <biological_entity id="o24965" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="with few scattered spreading hairs" />
      </biological_entity>
      <biological_entity id="o24966" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o24965" id="r2030" name="with" negation="false" src="d0_s1" to="o24966" />
    </statement>
    <statement id="d0_s2">
      <text>branches arcuate, branching appearing dichotomous.</text>
      <biological_entity id="o24967" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="course_or_shape" src="d0_s2" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="dichotomous" value_original="dichotomous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually alternate, occasionally opposite at proximalmost node;</text>
      <biological_entity id="o24968" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o24969" name="node" name_original="node" src="d0_s3" type="structure" />
      <relation from="o24968" id="r2031" name="occasionally opposite" negation="false" src="d0_s3" to="o24969" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 15–49 mm, glabrous;</text>
      <biological_entity id="o24970" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="49" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually ovate, rarely oblong or elliptic, 13–54 × 7–38 mm, base usually rounded to broadly cuneate, rarely truncate, margins finely serrulate, apex obtuse, surfaces glabrous or with few scattered hairs;</text>
      <biological_entity id="o24971" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s5" to="54" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="38" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24972" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually rounded" name="shape" src="d0_s5" to="broadly cuneate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o24973" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o24974" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24976" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o24975" id="r2032" name="with" negation="false" src="d0_s5" to="o24976" />
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate, midvein prominent.</text>
      <biological_entity id="o24975" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with few scattered hairs" value_original="with few scattered hairs" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o24977" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathial arrangement: terminal dichasial branches 2, few-branched (weakly defined);</text>
      <biological_entity id="o24978" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o24979" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="few-branched" value_original="few-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pleiochasial bracts 2–3, opposite or whorled, wholly green, similar in shape and size to distal leaves;</text>
      <biological_entity id="o24980" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o24981" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="wholly" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24982" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o24981" id="r2033" name="to" negation="false" src="d0_s8" to="o24982" />
    </statement>
    <statement id="d0_s9">
      <text>dichasial bracts smaller than distal leaves, often white at base.</text>
      <biological_entity id="o24983" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o24984" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="dichasial" value_original="dichasial" />
        <character constraint="than distal leaves" constraintid="o24985" is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character constraint="at base" constraintid="o24986" is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="distal" id="o24985" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o24986" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Cyathia: peduncle 0.9–3.5 (–6.2) mm.</text>
      <biological_entity id="o24987" name="cyathium" name_original="cyathia" src="d0_s10" type="structure" />
      <biological_entity id="o24988" name="peduncle" name_original="peduncle" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucre tubular or obconic, 1–1.7 × 0.7–1.4 mm, glabrous except for few hairs on lobes;</text>
      <biological_entity id="o24989" name="involucre" name_original="involucre" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.4" to_unit="mm" />
        <character constraint="except-for hairs" constraintid="o24990" is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24990" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o24991" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o24990" id="r2034" name="on" negation="false" src="d0_s11" to="o24991" />
    </statement>
    <statement id="d0_s12">
      <text>involucral lobes divided into several linear, smooth lobes;</text>
      <biological_entity id="o24992" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s12" value="involucral" value_original="involucral" />
        <character constraint="into lobes" constraintid="o24993" is_modifier="false" name="shape" src="d0_s12" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o24993" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="several" value_original="several" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>glands 1 (–3), greenish, sessile and broadly attached, 0.3–0.4 × 0.4–0.8 mm, opening oblong to subcircular, glabrous;</text>
      <biological_entity id="o24994" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="3" />
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="broadly" name="fixation" src="d0_s13" value="attached" value_original="attached" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s13" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="subcircular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>appendages petaloid, white, elliptic, oblong, transversely oblong, or forming thin, lunate rim on gland margin, not incurved and covering glands, 0.3–0.9 × 0.6–1.3 mm, entire, undulate or slightly lobed, glabrous.</text>
      <biological_entity id="o24995" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="not" name="orientation" notes="" src="d0_s14" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s14" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s14" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s14" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24996" name="rim" name_original="rim" src="d0_s14" type="structure">
        <character is_modifier="true" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character is_modifier="true" name="shape" src="d0_s14" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity constraint="gland" id="o24997" name="margin" name_original="margin" src="d0_s14" type="structure" />
      <biological_entity id="o24998" name="gland" name_original="glands" src="d0_s14" type="structure" />
      <relation from="o24995" id="r2035" name="forming" negation="false" src="d0_s14" to="o24996" />
      <relation from="o24995" id="r2036" name="on" negation="false" src="d0_s14" to="o24997" />
      <relation from="o24995" id="r2037" name="covering" negation="false" src="d0_s14" to="o24998" />
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers 20–30.</text>
      <biological_entity id="o24999" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s15" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o25000" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o25001" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 0.6–1 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o25002" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o25003" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules oblate, 2.8–3.1 × 3.6–4.5 mm, glabrous;</text>
      <biological_entity id="o25004" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s18" to="3.1" to_unit="mm" />
        <character char_type="range_value" from="3.6" from_unit="mm" name="width" src="d0_s18" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>columella 1.9–2.4 mm.</text>
      <biological_entity id="o25005" name="columella" name_original="columella" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s19" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds brown to blackish, ovoid, rounded in cross-section, 1.9–2.4 × 1.5–1.8 mm, irregularly and coarsely tuberculate;</text>
      <biological_entity id="o25006" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s20" to="blackish" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character constraint="in cross-section" constraintid="o25007" is_modifier="false" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" notes="" src="d0_s20" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s20" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly; coarsely" name="relief" src="d0_s20" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o25007" name="cross-section" name_original="cross-section" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>caruncle absent or rudimentary.</text>
      <biological_entity id="o25008" name="caruncle" name_original="caruncle" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s21" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia bifurcata is found in the mountains of southern New Mexico (Doña Ana, Grant, Lincoln, Otero, and Sierra counties) and trans-Pecos Texas (Brewster, Jeff Davis, and Presidio counties).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riparian areas with cottonwoods and willows, pinyon pine woodlands, pine-oak forests, Douglas fir forests with pines.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian areas" constraint="with cottonwoods and willows , pinyon pine woodlands , pine-oak forests ," />
        <character name="habitat" value="cottonwoods" />
        <character name="habitat" value="willows" />
        <character name="habitat" value="pinyon pine woodlands" />
        <character name="habitat" value="pine-oak forests" />
        <character name="habitat" value="fir forests" constraint="with pines" />
        <character name="habitat" value="pines" modifier="with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900–2300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico; Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>