<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1819" rank="genus">ARCEUTHOBIUM</taxon_name>
    <taxon_name authority="Engelmann" date="1850" rank="species">campylopodum</taxon_name>
    <taxon_name authority="(Engelmann) Nickrent" date="2012" rank="subspecies">abietinum</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-51: 9. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species campylopodum;subspecies abietinum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416902</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arceuthobium</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_name authority="Engelmann in W. H. Brewer et al." date="1880" rank="variety">abietinum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. California</publication_title>
      <place_in_publication>2: 106. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus arceuthobium;species douglasii;variety abietinum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Razoumofskya</taxon_name>
    <taxon_name authority="(Engelmann) Abrams" date="unknown" rank="species">abietina</taxon_name>
    <taxon_hierarchy>genus razoumofskya;species abietina</taxon_hierarchy>
  </taxon_identification>
  <number>7b.</number>
  <other_name type="common_name">Fir dwarf mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming witches brooms.</text>
      <biological_entity id="o18930" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18931" name="witch" name_original="witches" src="d0_s0" type="structure" />
      <relation from="o18930" id="r1549" name="forming" negation="false" src="d0_s0" to="o18931" />
    </statement>
    <statement id="d0_s1">
      <text>Stems yellow, green, olive green, orange, brown, or red, 8 (–22) cm;</text>
    </statement>
    <statement id="d0_s2">
      <text>third internode 4–14 (–23) × 1.5–2 (–4) mm, dominant shoot 1.5–6 mm diam. at base.</text>
      <biological_entity id="o18932" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="olive green" value_original="olive green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="22" to_unit="cm" />
        <character name="some_measurement" src="d0_s1" unit="cm" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o18933" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="23" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18934" name="shoot" name_original="shoot" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="dominant" value_original="dominant" />
        <character char_type="range_value" constraint="at base" constraintid="o18935" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18935" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers 2.5 mm diam.;</text>
      <biological_entity id="o18936" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character name="diameter" src="d0_s3" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 3 (–4).</text>
      <biological_entity id="o18937" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 4 × 2 mm.</text>
      <biological_entity id="o18938" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="mm" value="4" value_original="4" />
        <character name="width" src="d0_s5" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meiosis occurs in July, with fruits maturing 13 to 14 months after pollination.</discussion>
  <discussion>Subspecies abietinum includes forma speciales concoloris Hawksworth &amp; Wiens, which parasitizes Abies concolor (white fir), and forma speciales magnificae Hawksworth &amp; Wiens, which parasitizes A. magnifica (red fir). These forms were based upon inoculation studies showing that seeds of one form apparently will not infect the other's host species and vice versa (J. R. Parmeter and R. F. Scharpf 1963). Morphologically the two forms are extremely similar, with the former having a greater mean shoot height (10 versus 6 cm). The white fir dwarf mistletoe occurs throughout the above geographical range whereas red fir dwarf mistletoe is restricted to California and southwestern Oregon. In addition to the above two species of fir, Abies durangensis and A. grandis are principal hosts; secondary to rare hosts include A. lasiocarpa, Picea breweriana, Pinus ayacahuite, P. contorta, P. lambertiana, and P. monticola.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug(–Sep); fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jul" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests generally with fir.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="coniferous" />
        <character name="habitat" value="fir" modifier="generally with" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Oreg., Utah, Wash.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>