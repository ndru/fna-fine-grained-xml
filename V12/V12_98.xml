<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="illustration_page">85</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="(Hooker) Nuttall in J. Torrey and A. Gray" date="1838" rank="species">cuneatus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">cuneatus</taxon_name>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species cuneatus;variety cuneatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101434</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceanothus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cuneatus</taxon_name>
    <taxon_name authority="(Rose) McMinn" date="unknown" rank="variety">submontanus</taxon_name>
    <taxon_hierarchy>genus ceanothus;species cuneatus;variety submontanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="species">oblanceolatus</taxon_name>
    <taxon_hierarchy>genus c.;species oblanceolatus</taxon_hierarchy>
  </taxon_identification>
  <number>32a.</number>
  <other_name type="common_name">Buck brush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1.5–3.5 m.</text>
      <biological_entity id="o4907" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="3.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o4908" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets grayish brown to light gray, glaucous.</text>
      <biological_entity id="o4909" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="grayish brown" name="coloration" src="d0_s2" to="light gray" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades of fascicled and non-fascicled leaves flat, elliptic to widely oblanceolate, 6–22 (–30) × 3–12 (–22) mm, length usually 2+ times width, margins entire, apex usually obtuse to rounded, rarely truncate.</text>
      <biological_entity id="o4910" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="widely oblanceolate" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="l_w_ratio" src="d0_s3" value="2+" value_original="2+" />
      </biological_entity>
      <biological_entity id="o4911" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="non-fascicled" value_original="non-fascicled" />
      </biological_entity>
      <biological_entity id="o4912" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4913" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s3" to="rounded" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
      <relation from="o4910" id="r438" name="part_of" negation="false" src="d0_s3" to="o4911" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals, petals, and nectary usually white, sometimes pale blue or pale lavender.</text>
      <biological_entity id="o4914" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4915" name="sepal" name_original="sepals" src="d0_s4" type="structure" />
      <biological_entity id="o4916" name="petal" name_original="petals" src="d0_s4" type="structure" />
      <biological_entity id="o4917" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="pale blue" value_original="pale blue" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale lavender" value_original="pale lavender" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsules 4–6 mm wide.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 24.</text>
      <biological_entity id="o4918" name="capsule" name_original="capsules" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4919" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety cuneatus in Oregon and in the Klamath Mountains of northern California is characterized by relatively small, elliptic to oblanceolate leaf blades 6–12 millimeters. The type specimen, collected by David Douglas in the upper Willamette Valley of Oregon, falls within this range. Low-growing, moundlike plants in the Klamath Mountains, less than eight tenths of a meter, with spreading stems, leaves similar in size and shape, and white to pale blue sepals and petals, are treated here as Ceanothus arcuatus. Elsewhere, var. cuneatus is characterized by leaf blades 9–30 millimeters.</discussion>
  <discussion>Shrubs to 3.5 meters with large leaf blades 15–30 × 9–18(–22) millimeters have been named Ceanothus cuneatus var. dubius J. T. Howell, and are restricted to sandy soils and open sites in chaparral and mixed evergreen forests of the Santa Cruz Mountains. Plants in the Transverse and Peninsular ranges of southern California, with narrowly oblanceolate leaf blades with sparsely canescent abaxial surfaces, have been named C. oblanceolatus Davidson. Putative hybrids with C. pauciflorus have been reported from several localities in the southern Sierra Nevada (H. McMinn 1944). Formally named hybrids involving var. cuneatus include C. ×connivens Greene (either with C. prostratus or C. fresnensis), C. ×flexilis McMinn (with C. prostratus), and C. ×humboldtensis Roof (with C. pumilus).</discussion>
  <discussion>Wood of var. cuneatus was used by Native Americans to make tools and arrow foreshafts (D. E. Moerman 1998).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, ridges, sometimes on serpentine, chaparral, oak and oak-pine woodlands, conifer forests, gravelly floodplains.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="serpentine" modifier="sometimes" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="oak-pine woodlands" />
        <character name="habitat" value="conifer forests" />
        <character name="habitat" value="floodplains" modifier="gravelly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>