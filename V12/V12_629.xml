<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Urban" date="1902" rank="genus">KRUGIODENDRON</taxon_name>
    <place_of_publication>
      <publication_title>Symb. Antill.</publication_title>
      <place_in_publication>3: 313. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus krugiodendron</taxon_hierarchy>
    <other_info_on_name type="etymology">For Carl Wilhelm Krug, 1833–1898, major collaborator with Urban on the West Indian flora, and Greek dendron, tree</other_info_on_name>
    <other_info_on_name type="fna_id">117297</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Leadwood</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, unarmed;</text>
      <biological_entity id="o29849" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bud-scales present.</text>
      <biological_entity id="o29851" name="bud-scale" name_original="bud-scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, usually opposite or subopposite, rarely alternate, not borne on short-shoots;</text>
      <biological_entity id="o29852" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o29853" name="short-shoot" name_original="short-shoots" src="d0_s2" type="structure" />
      <relation from="o29852" id="r2453" name="borne on" negation="true" src="d0_s2" to="o29853" />
    </statement>
    <statement id="d0_s3">
      <text>blade not gland-dotted;</text>
    </statement>
    <statement id="d0_s4">
      <text>pinnately veined, secondary-veins arching near margins, higher order veins not forming adaxially raised reticulum.</text>
      <biological_entity id="o29854" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o29855" name="secondary-vein" name_original="secondary-veins" src="d0_s4" type="structure">
        <character constraint="near margins" constraintid="o29856" is_modifier="false" name="orientation" src="d0_s4" value="arching" value_original="arching" />
        <character is_modifier="false" name="position" notes="" src="d0_s4" value="higher" value_original="higher" />
      </biological_entity>
      <biological_entity id="o29856" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o29857" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity id="o29858" name="reticulum" name_original="reticulum" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="adaxially" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <relation from="o29857" id="r2454" name="forming" negation="false" src="d0_s4" to="o29858" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, within foliage, umbels;</text>
      <biological_entity id="o29859" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o29860" name="foliage" name_original="foliage" src="d0_s5" type="structure" />
      <biological_entity id="o29861" name="umbel" name_original="umbels" src="d0_s5" type="structure" />
      <relation from="o29859" id="r2455" name="within" negation="false" src="d0_s5" to="o29860" />
    </statement>
    <statement id="d0_s6">
      <text>peduncles and pedicels not fleshy in fruit.</text>
      <biological_entity id="o29862" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o29864" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o29863" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o29864" is_modifier="false" modifier="not" name="texture" src="d0_s6" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o29864" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o29865" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o29866" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium very shallowly cupulate, 3–4 mm wide;</text>
      <biological_entity id="o29867" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="very shallowly" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, spreading, greenish yellow, triangular to triangular-ovate, crested adaxially;</text>
      <biological_entity id="o29868" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish yellow" value_original="greenish yellow" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s10" to="triangular-ovate" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s10" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0;</text>
      <biological_entity id="o29869" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary fleshy, filling hypanthium, margin pentagonal, decagonal, or weakly subcrenulate;</text>
      <biological_entity id="o29870" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o29871" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure" />
      <biological_entity id="o29872" name="margin" name_original="margin" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s12" value="subcrenulate" value_original="subcrenulate" />
      </biological_entity>
      <relation from="o29870" id="r2456" name="filling" negation="false" src="d0_s12" to="o29871" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5;</text>
      <biological_entity id="o29873" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary superior, 2-locular;</text>
      <biological_entity id="o29874" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-locular" value_original="2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 1.</text>
      <biological_entity id="o29875" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits drupes, 5–8 [–12] mm;</text>
      <biological_entity constraint="fruits" id="o29876" name="drupe" name_original="drupes" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stone 1, indehiscent.</text>
      <biological_entity id="o29877" name="stone" name_original="stone" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., s Mexico, West Indies, Central America (s to Costa Rica).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America (s to Costa Rica)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>