<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="illustration_page">490</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Zuccarini" date="1844" rank="genus">EUCNIDE</taxon_name>
    <taxon_name authority="Parry" date="1875" rank="species">urens</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Naturalist</publication_title>
      <place_in_publication>9: 144. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus eucnide;species urens</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101800</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mentzelia</taxon_name>
    <taxon_name authority="Parry ex A. Gray" date="1874" rank="species">urens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 71. 1874</place_in_publication>
      <other_info_on_pub>not Vellozo 1831</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus mentzelia;species urens</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Desert stingbush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, moundlike (wider than tall).</text>
      <biological_entity id="o19571" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade usually unlobed, sometimes inconspicuously lobed.</text>
      <biological_entity id="o19572" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19573" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="sometimes inconspicuously" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels (fruiting) less than 3 cm, usually curved to nodding.</text>
      <biological_entity id="o19574" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers radially symmetric;</text>
      <biological_entity id="o19575" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s3" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>corolla funnelform, petals connate 5 mm, 1-colored, white to cream, spatulate, to 45 mm;</text>
      <biological_entity id="o19576" name="corolla" name_original="corolla" src="d0_s4" type="structure" />
      <biological_entity id="o19577" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
        <character name="some_measurement" src="d0_s4" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="1-colored" value_original="1-colored" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="cream" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 50+, inserted at base of corolla, included, most aggregated around style, only longer, outermost stamens spreading away from style, filaments 10–20 mm, longer than anthers.</text>
      <biological_entity id="o19578" name="stamen" name_original="stamens" src="d0_s5" type="structure" constraint="corolla" constraint_original="corolla; corolla">
        <character char_type="range_value" from="50" name="quantity" src="d0_s5" upper_restricted="false" />
        <character is_modifier="false" name="position" src="d0_s5" value="included" value_original="included" />
        <character constraint="around style" constraintid="o19581" is_modifier="false" name="arrangement" src="d0_s5" value="aggregated" value_original="aggregated" />
        <character is_modifier="false" modifier="only" name="length_or_size" notes="" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o19579" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o19580" name="corolla" name_original="corolla" src="d0_s5" type="structure" />
      <biological_entity id="o19581" name="style" name_original="style" src="d0_s5" type="structure" />
      <biological_entity constraint="outermost" id="o19582" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character constraint="away-from style" constraintid="o19583" is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o19583" name="style" name_original="style" src="d0_s5" type="structure" />
      <biological_entity id="o19585" name="anther" name_original="anthers" src="d0_s5" type="structure" />
      <relation from="o19578" id="r1622" name="inserted at" negation="false" src="d0_s5" to="o19579" />
      <relation from="o19578" id="r1623" name="part_of" negation="false" src="d0_s5" to="o19580" />
    </statement>
    <statement id="d0_s6">
      <text>2n = 42.</text>
      <biological_entity id="o19584" name="all" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character constraint="than anthers" constraintid="o19585" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19586" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eucnide urens is found primarily in the Mojave Desert but extends into surrounding areas. Asydates inyoensis, soft-wing flower beetles of the family Melyridae, have been found in flowers of E. urens; they collect pollen on dorsal setae and likely serve as pollinators.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clefts in cliffs, rocky slopes, wash margins, limestone, desert scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clefts" constraint="in cliffs , rocky slopes ," />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="margins" modifier="wash" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-50–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="50" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>