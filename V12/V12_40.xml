<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ACALYPHA</taxon_name>
    <taxon_name authority="Sprengel" date="1826" rank="species">poiretii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>3: 879. 1826</place_in_publication>
      <other_info_on_pub>(as poireti)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus acalypha;species poiretii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242427508</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acalypha</taxon_name>
    <taxon_name authority="Poiret in J. Lamarck et al." date="1804" rank="species">macrostachyos</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>6: 208. 1804</place_in_publication>
      <other_info_on_pub>not A. macrostachya Jacquin 1797</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus acalypha;species macrostachyos</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Poiret’s copperleaf</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 1–4 dm, monoecious.</text>
      <biological_entity id="o16317" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, pubescent and sparsely hirsute.</text>
      <biological_entity id="o16318" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 1–4.5 cm;</text>
      <biological_entity id="o16319" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16320" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to elliptic, 2–5 × 1–3.5 cm, base obtuse to rounded, margins serrate-crenate, apex acute.</text>
      <biological_entity id="o16321" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16322" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16323" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o16324" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="serrate-crenate" value_original="serrate-crenate" />
      </biological_entity>
      <biological_entity id="o16325" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences bisexual, axillary;</text>
      <biological_entity id="o16326" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>peduncle 0.1–0.5 cm, pistillate portion 2–4 × 0.8–1.2 cm (shorter on proximal inflorescences), staminate portion 0.3–1 cm;</text>
      <biological_entity id="o16327" name="peduncle" name_original="peduncle" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16328" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s5" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16329" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>allomorphic pistillate flowers common, terminal on staminate portion of inflorescences.</text>
      <biological_entity id="o16330" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="common" value_original="common" />
        <character constraint="on portion" constraintid="o16331" is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16331" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16332" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <relation from="o16331" id="r1361" name="part_of" negation="false" src="d0_s6" to="o16332" />
    </statement>
    <statement id="d0_s7">
      <text>Pistillate bracts (normal flowers) crowded (inflorescence axis not visible between bracts), 4–5 × 6–8 mm, abaxial surface pubescent and sparsely stipitate-glandular;</text>
      <biological_entity id="o16333" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16334" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lobes 7–9, triangular, 1/5 bract length;</text>
      <biological_entity id="o16335" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="9" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character name="quantity" src="d0_s8" value="1/5" value_original="1/5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>of allomorphic flowers absent.</text>
      <biological_entity id="o16336" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels of allomorphic flowers rudimentary.</text>
      <biological_entity id="o16337" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o16338" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="allomorphic" value_original="allomorphic" />
      </biological_entity>
      <relation from="o16337" id="r1362" name="part_of" negation="false" src="d0_s10" to="o16338" />
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: pistil 3-carpellate (normal flowers), 1-carpellate (allomorphic flowers);</text>
      <biological_entity id="o16339" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16340" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-carpellate" value_original="3-carpellate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-carpellate" value_original="1-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles unbranched.</text>
      <biological_entity id="o16341" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16342" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules smooth, pubescent;</text>
      <biological_entity id="o16343" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>allomorphic fruits obovoid, 1.2–1.5 × 1–1.2 mm, muricate, pubescent.</text>
      <biological_entity id="o16344" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="allomorphic" value_original="allomorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s14" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s14" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.2–1.5 mm, minutely pitted.</text>
      <biological_entity id="o16345" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Acalypha poiretii is known in the flora area from the lower Rio Grande valley (Cameron, Hidalgo, and Starr counties). It was collected in the late nineteenth century on ballast dumps in Alabama, Florida, and New Jersey, but has not been reported again from any of these states.</discussion>
  <discussion>Some authors (for example, R. Govaerts et al. 2000) have treated Acalypha poiretii and A. alnifolia Poiret as synonyms, in which case the latter would be the correct name for this species; however, the types of the two names clearly belong to different species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; c, e Mexico; Central America (Guatemala); introduced West Indies, South America, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>