<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">427</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1819" rank="genus">ARCEUTHOBIUM</taxon_name>
    <taxon_name authority="Hawksworth &amp; Wiens" date="1964" rank="species">gillii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>16: 55, figs. 1A, 2A. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species gillii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101752</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Chihuahua pine dwarf mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming nonsystemic witches brooms;</text>
      <biological_entity id="o27647" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27648" name="witch" name_original="witches" src="d0_s0" type="structure" />
      <relation from="o27647" id="r2273" name="forming nonsystemic" negation="false" src="d0_s0" to="o27648" />
    </statement>
    <statement id="d0_s1">
      <text>staminate and pistillate plants dimorphic: staminate taller with open divaricate branching, pistillate shorter with dense branching.</text>
      <biological_entity id="o27649" name="whole-organism" name_original="" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27650" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character constraint="with open divaricate branching" is_modifier="false" name="height" src="d0_s1" value="taller" value_original="taller" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="pistillate" value_original="pistillate" />
        <character constraint="with dense branching" is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems olive green, greenish brown, greenish yellow, or orange;</text>
    </statement>
    <statement id="d0_s3">
      <text>secondary branching fanlike, branches 8–15 (–25) cm, third internode 5–18 × 2–4.5 mm, dominant shoot 2.5–8 mm diam. at base.</text>
      <biological_entity id="o27651" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="olive green" value_original="olive green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="orange" value_original="orange" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="secondary" value_original="secondary" />
      </biological_entity>
      <biological_entity id="o27652" name="whole-organism" name_original="" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s3" value="fanlike" value_original="fanlike" />
      </biological_entity>
      <biological_entity id="o27653" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27654" name="internode" name_original="internode" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27655" name="shoot" name_original="shoot" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="dominant" value_original="dominant" />
        <character char_type="range_value" constraint="at base" constraintid="o27656" from="2.5" from_unit="mm" name="diameter" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27656" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Staminate pedicels absent.</text>
      <biological_entity id="o27657" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers slightly asymmetric, (proximal petal deflexed at anthesis), lenticular in bud, 2.5–4 mm diam.;</text>
      <biological_entity id="o27658" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character constraint="in bud" constraintid="o27659" is_modifier="false" name="shape" src="d0_s5" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" notes="" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27659" name="bud" name_original="bud" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petals 3, same color as stem abaxially, tawny reddish-brown adaxially.</text>
      <biological_entity id="o27660" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="false" modifier="adaxially" name="coloration" notes="" src="d0_s6" value="tawny reddish-brown" value_original="tawny reddish-brown" />
      </biological_entity>
      <biological_entity id="o27661" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o27660" id="r2274" name="as" negation="false" src="d0_s6" to="o27661" />
    </statement>
    <statement id="d0_s7">
      <text>Berries proximally olive green and conspicuously glaucous with bluish hue, distally greenish or yellowish-brown, 4–5 × 2–3 mm.</text>
      <biological_entity id="o27662" name="berry" name_original="berries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="olive green" value_original="olive green" />
        <character constraint="with bluish hue" is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds pyriform to ellipsoid, 3–4 × 2–3 mm, endosperm dark olive green.</text>
      <biological_entity id="o27663" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="pyriform" name="shape" src="d0_s8" to="ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 28.</text>
      <biological_entity id="o27664" name="endosperm" name_original="endosperm" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark olive" value_original="dark olive" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27665" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meiosis occurs in September, with fruits maturing 19 to 20 months after pollination; seeds germinate in April.</discussion>
  <discussion>In the flora area, Arceuthobium gillii is found in the Chiricahua, Huachuca, Santa Catalina, and Santa Rita mountains of Arizona, and the Animas Mountains of New Mexico. Subspecies nigrum Hawksworth &amp; Wiens, which occurs in the Sierra Madre Occidental and Oriental of Mexico, was subsequently elevated to species rank as A. nigrum (Hawksworth &amp; Wiens) Hawksworth &amp; Wiens. The two species are apparently allopatric but occur in the same mountain range in northern Durango, Mexico. Both form nonsystemic witches’ brooms on members of Pinus subsect. Leiophylla Loudon and are closely related as shown by molecular analyses (D. L. Nickrent et al. 2004).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr; fruiting Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous and mixed forests with Chihuahua pine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="mixed forests" constraint="with chihuahua" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Durango, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>