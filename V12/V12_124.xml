<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">502</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="(M. E. Jones) Tidestrom" date="1925" rank="species">integra</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>25: 363. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species integra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101850</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mentzelia</taxon_name>
    <taxon_name authority="(Nuttall) A. Gray" date="unknown" rank="species">multiflora</taxon_name>
    <taxon_name authority="M. E. Jones" date="1895" rank="variety">integra</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 689. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mentzelia;species multiflora;variety integra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nuttallia</taxon_name>
    <taxon_name authority="Rydberg 1913" date="unknown" rank="species">lobata</taxon_name>
    <other_info_on_name>not M. lobata (Hooker) Hemsley 1880</other_info_on_name>
    <taxon_hierarchy>genus nuttallia;species lobata</taxon_hierarchy>
  </taxon_identification>
  <number>45.</number>
  <other_name type="common_name">Virgin blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual or biennial, candelabra-form.</text>
      <biological_entity id="o9610" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, erect, straight;</text>
      <biological_entity id="o9611" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distal or along entire stem, distal or proximal longest, antrorse, straight, proximal not decumbent;</text>
      <biological_entity id="o9612" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="along entire stem" value_original="along entire stem" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o9613" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o9612" id="r801" name="along" negation="false" src="d0_s2" to="o9613" />
    </statement>
    <statement id="d0_s3">
      <text>hairy or glabrescent.</text>
      <biological_entity constraint="proximal" id="o9614" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade (23–) 29–78.4 × 3–14.6 mm, widest intersinus distance 1.6–8.5 mm;</text>
      <biological_entity id="o9615" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9616" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="23" from_unit="mm" name="atypical_length" src="d0_s4" to="29" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="29" from_unit="mm" name="length" src="d0_s4" to="78.4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="14.6" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s4" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate or elliptic, margins dentate to serrate, teeth 4–12, proximal sides slightly antrorse, distal sides perpendicular to leaf axis, 0.8–5.3 mm;</text>
      <biological_entity id="o9617" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o9618" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9619" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="dentate" name="architecture_or_shape" src="d0_s5" to="serrate" />
      </biological_entity>
      <biological_entity id="o9620" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="12" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9621" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9622" name="side" name_original="sides" src="d0_s5" type="structure">
        <character constraint="to leaf axis" constraintid="o9623" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="5.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o9623" name="axis" name_original="axis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal oblanceolate, elliptic, lanceolate, or linear, base not clasping, tapered, margins usually dentate to serrate, rarely entire, teeth (0–) 2–10, proximal sides antrorse, distal sides perpendicular to leaf axis, 0.4–5.3 mm;</text>
      <biological_entity id="o9624" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o9625" name="side" name_original="sides" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o9626" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o9627" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually dentate" name="architecture_or_shape" src="d0_s6" to="serrate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9628" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9629" name="side" name_original="sides" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="antrorse" value_original="antrorse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9630" name="side" name_original="sides" src="d0_s6" type="structure">
        <character constraint="to leaf axis" constraintid="o9631" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="5.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o9631" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with simple grappling-hook, complex grappling-hook, and needlelike trichomes, largest trichomes with pearly white bases, adaxial surface with needlelike trichomes.</text>
      <biological_entity id="o9632" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o9633" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o9635" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o9636" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity constraint="largest" id="o9637" name="trichome" name_original="trichomes" src="d0_s7" type="structure" />
      <biological_entity id="o9638" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9639" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o9640" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o9633" id="r802" name="with" negation="false" src="d0_s7" to="o9635" />
      <relation from="o9633" id="r803" name="with" negation="false" src="d0_s7" to="o9636" />
      <relation from="o9637" id="r804" name="with" negation="false" src="d0_s7" to="o9638" />
      <relation from="o9639" id="r805" name="with" negation="false" src="d0_s7" to="o9640" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o9641" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o9642" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 8.6–13.9 (–17.4) × 2.9–4.7 (–6.1) mm, apex rounded, glabrous abaxially;</text>
      <biological_entity id="o9643" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9644" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="13.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="17.4" to_unit="mm" />
        <character char_type="range_value" from="8.6" from_unit="mm" name="length" src="d0_s9" to="13.9" to_unit="mm" />
        <character char_type="range_value" from="4.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="6.1" to_unit="mm" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="width" src="d0_s9" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9645" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 6.7–13.2 (–15.4) × 1.7–4.3 mm, usually without, rarely with, anthers, second whorl with anthers;</text>
      <biological_entity id="o9646" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9647" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o9648" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o9649" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="13.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="15.4" to_unit="mm" />
        <character char_type="range_value" from="6.7" from_unit="mm" name="length" src="d0_s10" to="13.2" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s10" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9650" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o9651" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o9652" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o9651" id="r806" modifier="usually; rarely" name="with" negation="false" src="d0_s10" to="o9652" />
    </statement>
    <statement id="d0_s11">
      <text>anthers straight after dehiscence, epidermis smooth;</text>
      <biological_entity id="o9653" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o9654" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o9655" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 4.8–11.3 mm.</text>
      <biological_entity id="o9656" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9657" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.8" from_unit="mm" name="some_measurement" src="d0_s12" to="11.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped, 6.2–11.4 (–13) × 6–8.4 mm, base rounded, not longitudinally ridged.</text>
      <biological_entity id="o9658" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="11.4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="13" to_unit="mm" />
        <character char_type="range_value" from="6.2" from_unit="mm" name="length" src="d0_s13" to="11.4" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="8.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9659" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls wavy, papillae 5–13 per cell.</text>
      <biological_entity id="o9660" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o9661" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o9662" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o9664" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 20.</text>
      <biological_entity id="o9663" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o9664" from="5" name="quantity" src="d0_s14" to="13" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9665" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia integra is found in northwestern Arizona, southeastern Nevada, and southwestern Utah.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, outcrops, hillsides, washes, dunes, sandy, gravelly, or volcanic soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="gravelly" modifier="sandy" />
        <character name="habitat" value="volcanic soils" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1800 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>