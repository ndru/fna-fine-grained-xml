<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Persoon" date="1806" rank="subgenus">Esula</taxon_name>
    <taxon_name authority="Mayfield" date="2013" rank="species">austrotexana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">austrotexana</taxon_name>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;subgenus esula;species austrotexana;variety austrotexana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101626</other_info_on_name>
  </taxon_identification>
  <number>94a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–22 cm.</text>
      <biological_entity id="o4269" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade linear-oblanceolate, apex rounded.</text>
      <biological_entity id="o4270" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4271" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-oblanceolate" value_original="linear-oblanceolate" />
      </biological_entity>
      <biological_entity id="o4272" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Dichasial bracts reniform-ovate to subdeltate-ovate, base truncate.</text>
      <biological_entity id="o4273" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="dichasial" value_original="dichasial" />
        <character char_type="range_value" from="reniform-ovate" name="shape" src="d0_s2" to="subdeltate-ovate" />
      </biological_entity>
      <biological_entity id="o4274" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seeds 1.6–1.7 × 1.1–1.3 mm, surface with regular concave depressions.</text>
      <biological_entity id="o4275" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s3" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s3" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4277" name="depression" name_original="depressions" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="regular" value_original="regular" />
        <character is_modifier="true" name="shape" src="d0_s3" value="concave" value_original="concave" />
      </biological_entity>
      <relation from="o4276" id="r386" name="with" negation="false" src="d0_s3" to="o4277" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 26.</text>
      <biological_entity id="o4276" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <biological_entity constraint="2n" id="o4278" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety austrotexana is restricted to Atascosa, Bexar, and Wilson counties. According to M. H. Mayfield (2013), only five documented localities are known.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–300 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>