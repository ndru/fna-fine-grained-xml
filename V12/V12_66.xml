<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="Barneby" date="1966" rank="species">nephradenia</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>10: 314. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species nephradenia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101543</other_info_on_name>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Paria or Utah spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with slender little-branched taproot.</text>
      <biological_entity id="o31095" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity constraint="slender" id="o31096" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="little-branched" value_original="little-branched" />
      </biological_entity>
      <relation from="o31095" id="r2537" name="with" negation="false" src="d0_s0" to="o31096" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, branched, dichotomous distally and slightly angled, 4–25 cm, glabrous or sparsely strigillose.</text>
      <biological_entity id="o31097" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o31098" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules 0.1–0.2 mm;</text>
      <biological_entity id="o31099" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s3" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–6 mm, glabrous or sparsely strigillose;</text>
      <biological_entity id="o31100" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually linear to narrowly-elliptic, occasionally ovate to obovate, 14–42 × 3–10 mm, progressively narrower distally, base attenuate, margins entire, apex usually acute, rarely obtuse, surfaces glabrous or sparsely strigillose;</text>
      <biological_entity id="o31101" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear to narrowly-elliptic" value_original="linear to narrowly-elliptic" />
        <character char_type="range_value" from="occasionally ovate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s5" to="42" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o31102" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o31103" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31104" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation inconspicuous.</text>
      <biological_entity id="o31105" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal bifurcations of stems;</text>
      <biological_entity id="o31106" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal bifurcations" constraintid="o31107" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o31107" name="bifurcation" name_original="bifurcations" src="d0_s7" type="structure" />
      <biological_entity id="o31108" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <relation from="o31107" id="r2538" name="part_of" negation="false" src="d0_s7" to="o31108" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.6–2.4 mm, glabrous or strigillose.</text>
      <biological_entity id="o31109" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre campanulate, 1–1.1 × 1.2–1.4 mm, strigillose at least toward apex;</text>
      <biological_entity id="o31110" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character constraint="toward apex" constraintid="o31111" is_modifier="false" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o31111" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>glands 5, green-yellow, oblong, 0.4–0.6 × 0.7–1 mm;</text>
      <biological_entity id="o31112" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green-yellow" value_original="green-yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s10" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages whitish to yellow-green, lunate to broadly ovate, 0.2–0.5 × 0.7–1.1 mm, entire or slightly crenulate.</text>
      <biological_entity id="o31113" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s11" to="yellow-green" />
        <character char_type="range_value" from="lunate" name="shape" src="d0_s11" to="broadly ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s11" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 25–30.</text>
      <biological_entity id="o31114" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o31115" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31116" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.7–1 mm, 2-fid at apex.</text>
      <biological_entity id="o31117" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31118" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character constraint="at apex" constraintid="o31119" is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o31119" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblate to subglobose, 2.9–3.2 × 3.2–3.4 mm, glabrous;</text>
      <biological_entity id="o31120" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s15" to="subglobose" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s15" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s15" to="3.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 2.8–3.1 mm.</text>
      <biological_entity id="o31121" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s16" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds light gray to whitish, oblong-ovoid, rounded in cross-section, 2.3–2.6 × 1.3–1.5 mm, dimpled and rugulose;</text>
      <biological_entity id="o31122" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="light gray" name="coloration" src="d0_s17" to="whitish" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character constraint="in cross-section" constraintid="o31123" is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" notes="" src="d0_s17" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" notes="" src="d0_s17" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s17" value="dimpled" value_original="dimpled" />
        <character is_modifier="false" name="relief" src="d0_s17" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity id="o31123" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o31124" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia nephradenia is the only species of the genus endemic to the Colorado Plateau of Utah and adjacent Colorado.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Saltbush, blackbrush, Ephedra-dominated scrub and desert communities.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="blackbrush" />
        <character name="habitat" value="scrub" />
        <character name="habitat" value="desert communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>