<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <taxon_name authority="Nuttall" date="1835" rank="species">betonicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>5: 173. 1835</place_in_publication>
      <other_info_on_pub>(as betonicaefolia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species betonicifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417367</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tragia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">urticifolia</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="variety">texana</taxon_name>
    <taxon_hierarchy>genus tragia;species urticifolia;variety texana</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Betony-leaf noseburn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, 2–5 dm.</text>
      <biological_entity id="o4301" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="herb" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to trailing, green to whitish green, apex never flexuous.</text>
      <biological_entity id="o4303" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="trailing" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="whitish green" />
      </biological_entity>
      <biological_entity id="o4304" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="never" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 10–40 mm;</text>
      <biological_entity id="o4305" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4306" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade triangular-lanceolate to triangular-ovate, 1.5–6 × 1–3.5 cm, base cordate to truncate, margins serrate, apex acute.</text>
      <biological_entity id="o4307" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4308" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s3" to="triangular-ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4309" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate to truncate" value_original="cordate to truncate" />
      </biological_entity>
      <biological_entity id="o4310" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o4311" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal (often appearing leaf-opposed), glands absent, staminate flowers 15–80 per raceme, distally clustered;</text>
      <biological_entity id="o4312" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4313" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4314" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per raceme" constraintid="o4315" from="15" name="quantity" src="d0_s4" to="80" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_growth_form" notes="" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o4315" name="raceme" name_original="raceme" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>staminate bracts 1–2 mm.</text>
      <biological_entity id="o4316" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate 0.7–1 mm, persistent base 0.3–0.6 mm, shorter than subtending bract;</text>
      <biological_entity id="o4317" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4318" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character constraint="than subtending bract" constraintid="o4319" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o4319" name="bract" name_original="bract" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate 3–4 mm in fruit.</text>
      <biological_entity id="o4320" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o4321" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4321" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3–4, green, sometimes red-tinged, 1.2–2.3 mm;</text>
      <biological_entity id="o4322" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4323" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="red-tinged" value_original="red-tinged" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3 (–4), filaments 0.4–1 mm.</text>
      <biological_entity id="o4324" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4325" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o4326" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals lanceolate, 1.8–5 mm;</text>
      <biological_entity id="o4327" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4328" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles connate 1/3 length;</text>
      <biological_entity id="o4329" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4330" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/3" value_original="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas papillate.</text>
      <biological_entity id="o4331" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4332" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 7–9 mm wide.</text>
      <biological_entity id="o4333" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds dark-brown with light-brown streaks, 3–4 mm.</text>
      <biological_entity id="o4334" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character constraint="with streaks" constraintid="o4335" is_modifier="false" name="coloration" src="d0_s14" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4335" name="streak" name_original="streaks" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Tragia betonicifolia resemble those of T. urticifolia but differ in the greater number of branches from the root crowns, the shorter length of the persistent staminate flower pedicel bases, the longer, narrower pistillate sepals, and the distally clustered arrangement of the staminate flowers.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer; fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy soils, disturbed fields, prairies, open woods.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="disturbed fields" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="woods" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Kans., La., Miss., Mo., Okla., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>