<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator/>
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="(S. Watson) Weberbauer in H. G. A. Engler and K. Prantl" date="1896" rank="subgenus">Cerastes</taxon_name>
    <taxon_name authority="J. T. Howell" date="1937" rank="species">gloriosus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 43. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes;species gloriosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101443</other_info_on_name>
  </taxon_identification>
  <number>38.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.1–3 m, matlike to moundlike.</text>
      <biological_entity id="o12142" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m"/>
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matlike" value_original="matlike"/>
        <character is_modifier="false" name="shape" src="d0_s0" value="moundlike" value_original="moundlike"/>
        <character name="growth_form" value="shrub"/>
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, spreading, ascending, or erect, sometimes rooting at proximal nodes;</text>
      <biological_entity id="o12143" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate"/>
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading"/>
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending"/>
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect"/>
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending"/>
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect"/>
        <character constraint="at proximal nodes" constraintid="o12144" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rooting" value_original="rooting"/>
      </biological_entity>
      <biological_entity constraint="proximal" id="o12144" name="node" name_original="nodes" src="d0_s1" type="structure"/>
    </statement>
    <statement id="d0_s2">
      <text>branchlets green to brown or reddish-brown, flexible to rigid, strigillose or tomentulose.</text>
      <biological_entity id="o12145" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="brown or reddish-brown"/>
        <character is_modifier="false" name="fragility" src="d0_s2" value="pliable" value_original="flexible"/>
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid"/>
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigillose" value_original="strigillose"/>
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose"/>
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not fascicled, not crowded;</text>
      <biological_entity id="o12146" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s3" value="fascicled" value_original="fascicled"/>
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="crowded" value_original="crowded"/>
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–4 mm;</text>
      <biological_entity id="o12147" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat to ± cupped or folded lengthwise adaxially, widely elliptic, obovate, or suborbiculate, 10–40 (–45) × 5–24 mm, base cuneate to ± rounded, margins not revolute, sometimes slightly thickened, dentate to denticulate most of length, teeth 9–35, apex rounded, truncate, or retuse, abaxial surface pale green, sparsely strigillose or glabrate, adaxial surface dark green, ± shiny, glabrous.</text>
      <biological_entity id="o12148" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat"/>
        <character char_type="range_value" from="cuneate" modifier="more or less" name="shape" src="d0_s5" to="more or less rounded"/>
      </biological_entity>
      <biological_entity id="o12149" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded"/>
        <character is_modifier="true" modifier="lengthwise adaxially; widely" name="shape" src="d0_s5" value="elliptic" value_original="elliptic"/>
        <character is_modifier="true" modifier="more or less; more or less" name="shape" src="d0_s5" value="obovate" value_original="obovate"/>
        <character is_modifier="true" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate"/>
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s5" to="45" to_unit="mm"/>
        <character char_type="range_value" from="10" from_unit="mm" is_modifier="true" name="length" src="d0_s5" to="40" to_unit="mm"/>
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="width" src="d0_s5" to="24" to_unit="mm"/>
      </biological_entity>
      <biological_entity id="o12150" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute"/>
        <character is_modifier="false" modifier="sometimes slightly" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened"/>
        <character char_type="range_value" from="dentate" name="length" src="d0_s5" to="denticulate"/>
      </biological_entity>
      <biological_entity id="o12151" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s5" to="35"/>
      </biological_entity>
      <biological_entity id="o12152" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded"/>
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate"/>
        <character is_modifier="false" name="shape" src="d0_s5" value="retuse" value_original="retuse"/>
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate"/>
        <character is_modifier="false" name="shape" src="d0_s5" value="retuse" value_original="retuse"/>
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12153" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green"/>
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="strigillose" value_original="strigillose"/>
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate"/>
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12154" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green"/>
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s5" value="shiny" value_original="shiny"/>
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous"/>
      </biological_entity>
      <relation from="o12148" id="r1014" modifier="more or less" name="cupped" negation="false" src="d0_s5" to="o12149"/>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary, 0.9–2.5 cm.</text>
      <biological_entity id="o12155" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary"/>
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm"/>
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals, petals, and nectary deep blue to bluish purple.</text>
      <biological_entity id="o12156" name="flower" name_original="flowers" src="d0_s7" type="structure"/>
      <biological_entity id="o12157" name="sepal" name_original="sepals" src="d0_s7" type="structure"/>
      <biological_entity id="o12158" name="petal" name_original="petals" src="d0_s7" type="structure"/>
      <biological_entity id="o12159" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="deep" value_original="deep"/>
        <character char_type="range_value" from="blue" name="coloration" src="d0_s7" to="bluish purple"/>
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 4–6 mm wide, lobed;</text>
      <biological_entity id="o12160" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm"/>
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed"/>
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves usually smooth, sometimes rugulose or crested distal to middle, horns subapical, minute, not rugose, intermediate ridges absent.</text>
      <biological_entity id="o12161" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth"/>
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s9" value="rugulose" value_original="rugulose"/>
        <character is_modifier="false" name="position" src="d0_s9" value="crested" value_original="crested"/>
        <character char_type="range_value" from="distal" name="position" src="d0_s9" to="middle"/>
      </biological_entity>
      <biological_entity id="o12162" name="horn" name_original="horns" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subapical" value_original="subapical"/>
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute"/>
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="rugose" value_original="rugose"/>
      </biological_entity>
      <biological_entity id="o12163" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="intermediate" value_original="intermediate"/>
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent"/>
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native"/>
      </biological_entity>
    </statement>
  </description>
  <discussion>Ceanothus gloriosus is composed of three varieties occurring along the northern California coast from Humboldt County to Marin County. Variety gloriosus and var. porrectus generally differ primarily by leaf shape, length and width, and the number of marginal teeth. Variety exaltatus differs from the other two varieties primarily in stature. Complex hybrids with C. cuneatus var. ramulosus, C. divergens, and C. sonomensis were studied by J. T. Howell (1940).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs 0.8–3 m; stems erect to ascending.</description>
      <determination>38c. Ceanothus gloriosus var. exaltatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs 0.1–0.5 m; stems prostrate to spreading.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades widely obovate to suborbiculate, 23–31(–45) × 17–24 mm, marginal teeth 13–31.</description>
      <determination>38a. Ceanothus gloriosus var. gloriosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades elliptic, obovate, or narrowly obovate, 10–21 × 5–15 mm, marginal teeth 9–19.</description>
      <determination>38b. Ceanothus gloriosus var. porrectus</determination>
    </key_statement>
  </key>
</bio:treatment>