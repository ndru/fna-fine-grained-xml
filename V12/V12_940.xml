<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">polygonifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 455. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species polygonifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101604</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="unknown" rank="species">polygonifolia</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species polygonifolia</taxon_hierarchy>
  </taxon_identification>
  <number>73.</number>
  <other_name type="common_name">Dune or seaside spurge</other_name>
  <other_name type="common_name">euphorbe à feuilles de renouée</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, with taproot.</text>
      <biological_entity id="o13768" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o13769" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o13768" id="r1143" name="with" negation="false" src="d0_s0" to="o13769" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually prostrate, occasionally ascending, 5–30 cm, glabrous.</text>
      <biological_entity id="o13770" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o13771" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules usually distinct, occasionally connate basally (distal portion of stem), triangular-subulate, entire or divided, 0.8–1.5 mm, glabrous;</text>
      <biological_entity id="o13772" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="occasionally; basally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular-subulate" value_original="triangular-subulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–3 mm, glabrous;</text>
      <biological_entity id="o13773" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong, linear-oblong, or linear-lanceolate, 5–16 × 2–4 mm, base slightly asymmetric, obtuse or subcordate, margins entire, apex obtuse, often mucronulate, surfaces uniformly green or reddish tinged, glabrous;</text>
      <biological_entity id="o13774" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13775" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o13776" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13777" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation obscure.</text>
      <biological_entity id="o13778" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish tinged" value_original="reddish tinged" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary or in small, cymose clusters at distal nodes;</text>
      <biological_entity id="o13779" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o13780" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="in small , cymose clusters" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13780" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.5–5 mm.</text>
      <biological_entity id="o13781" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic-campanulate, 1.2–1.7 × 1–1.4 mm, glabrous;</text>
      <biological_entity id="o13782" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic-campanulate" value_original="obconic-campanulate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s9" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, occasionally rudimentary, green-yellow to tan or orange-tinged, occasionally shortly stipitate, usually broadly oval to subcircular, sometimes figure eight-shaped, shallowly cupped, 0.1–0.3 × 0.2–0.4 mm;</text>
      <biological_entity id="o13783" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" modifier="occasionally" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" from="green-yellow" name="coloration" src="d0_s10" to="tan or orange-tinged" />
        <character is_modifier="false" modifier="occasionally shortly" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character char_type="range_value" from="usually broadly oval" name="shape" src="d0_s10" to="subcircular" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="8--shaped" value_original="8--shaped" />
        <character char_type="range_value" from="0.1" from_unit="mm" modifier="shallowly" name="length" src="d0_s10" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" modifier="shallowly" name="width" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages absent or rudimentary.</text>
      <biological_entity id="o13784" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 5–14.</text>
      <biological_entity id="o13785" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o13786" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13787" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.7–1 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o13788" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13789" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules broadly ovoid, 3–3.5 (–4) × (2–) 2.4–3 mm, glabrous;</text>
      <biological_entity id="o13790" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s15" to="2.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 2–3 mm.</text>
      <biological_entity id="o13791" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ashy white, wedge-shaped to slightly ovoid, weakly dorsiventrally compressed and elliptic-terete to bluntly subangled in cross-section, back strongly rounded, face slightly rounded, (2–) 2.2–2.8 × 1.6–1.9 mm, smooth or minutely pitted, with smooth brown line from top to bottom on adaxial side.</text>
      <biological_entity id="o13792" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="ashy white" value_original="ashy white" />
        <character char_type="range_value" from="wedge-shaped" name="shape" src="d0_s17" to="slightly ovoid" />
        <character is_modifier="false" modifier="weakly dorsiventrally" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character char_type="range_value" constraint="in cross-section" constraintid="o13793" from="elliptic-terete" name="shape" src="d0_s17" to="bluntly subangled" />
        <character is_modifier="false" modifier="back strongly" name="shape" notes="" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o13793" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
      <biological_entity id="o13794" name="face" name_original="face" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s17" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s17" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s17" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s17" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o13795" name="line" name_original="line" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character char_type="range_value" constraint="on adaxial side" constraintid="o13796" from="top" name="position" src="d0_s17" to="bottom" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13796" name="side" name_original="side" src="d0_s17" type="structure" />
      <relation from="o13794" id="r1144" name="with" negation="false" src="d0_s17" to="o13795" />
    </statement>
  </description>
  <discussion>Euphorbia polygonifolia is native to coastal beaches and dunes along the Atlantic Ocean from the maritime provinces of Canada south to northern Florida. The species also occurs disjunctly along the shores of the North American Great Lakes. Euphorbia polygonifolia was native to Quebec but is now considered extirpated from the province. It has apparently been introduced in Europe, but it is unclear if it has persisted there (L. C. Wheeler 1941). Immature individuals of this species can be somewhat difficult to distinguish from E. bombensis where their ranges overlap (Virginia to northern Florida). Where they occur together, E. polygonifolia tends to be a pioneer species on the upper beach and foredune front, whereas E. bombensis tends to inhabit areas behind the foredune (R. D. Porcher and D. A. Rayner 2002). Euphorbia polygonifolia can be distinguished also by its larger capsules and larger, wedge-shaped to slightly ovoid seeds.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting early summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="early summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy maritime and freshwater beaches and foredunes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy maritime" />
        <character name="habitat" value="freshwater" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="foredunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Conn., Del., Fla., Ga., Ill., Ind., Maine, Md., Mass., Mich., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>