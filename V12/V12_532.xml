<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Michael J. Huft</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HIPPOMANE</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1191. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 499. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus hippomane</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hippos, horse, and mania, fury, alluding to effect of the caustic latex on horses</other_info_on_name>
    <other_info_on_name type="fna_id">115512</other_info_on_name>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Manchineel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, monoecious;</text>
      <biological_entity id="o2793" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs absent;</text>
      <biological_entity id="o2794" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex white.</text>
      <biological_entity id="o2795" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, alternate, simple;</text>
      <biological_entity id="o2796" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, caducous;</text>
      <biological_entity id="o2797" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present, glands at apex;</text>
      <biological_entity id="o2798" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2799" name="gland" name_original="glands" src="d0_s5" type="structure" />
      <biological_entity id="o2800" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o2799" id="r255" name="at" negation="false" src="d0_s5" to="o2800" />
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed, margins remotely serrate or crenate, laminar glands absent;</text>
      <biological_entity id="o2801" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o2802" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="remotely" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation pinnate.</text>
      <biological_entity constraint="laminar" id="o2803" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences bisexual (pistillate flowers proximal, staminate distal), terminal, spikelike thyrses;</text>
      <biological_entity id="o2804" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2805" name="thyrse" name_original="thyrses" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glands subtending each bract 2.</text>
      <biological_entity id="o2806" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o2807" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels: staminate present, pistillate rudimentary.</text>
      <biological_entity id="o2808" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals 2, imbricate, connate proximally;</text>
      <biological_entity id="o2809" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2810" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o2811" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2812" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary absent;</text>
      <biological_entity id="o2813" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2814" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 2, connate basally;</text>
      <biological_entity id="o2815" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2816" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistillode absent.</text>
      <biological_entity id="o2817" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2818" name="pistillode" name_original="pistillode" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: sepals 3 [–4], connate proximally;</text>
      <biological_entity id="o2819" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2820" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="4" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 0;</text>
      <biological_entity id="o2821" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2822" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>nectary absent;</text>
      <biological_entity id="o2823" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2824" name="nectary" name_original="nectary" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pistil 6–9-carpellate;</text>
      <biological_entity id="o2825" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2826" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="6-9-carpellate" value_original="6-9-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 6–9, connate basally, unbranched.</text>
      <biological_entity id="o2827" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2828" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s20" to="9" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits drupes.</text>
      <biological_entity constraint="fruits" id="o2829" name="drupe" name_original="drupes" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds elliptic-compressed;</text>
      <biological_entity id="o2830" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="elliptic-compressed" value_original="elliptic-compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>caruncle absent.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 11.</text>
      <biological_entity id="o2831" name="caruncle" name_original="caruncle" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o2832" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 or 3 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, n South America, Pacific Islands (Galápagos Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Galápagos Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hippomane horrida Urban &amp; Ekman and H. spinosa Linnaeus are endemic to Hispaniola.</discussion>
  
</bio:treatment>