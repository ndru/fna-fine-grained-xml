<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">509</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="J. Darlington" date="1934" rank="species">oreophila</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 175. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species oreophila</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101820</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Argus blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, bushlike, with subterranean caudices.</text>
      <biological_entity id="o13561" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="bushlike" value_original="bushlike" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13562" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <relation from="o13561" id="r1127" name="with" negation="false" src="d0_s0" to="o13562" />
    </statement>
    <statement id="d0_s1">
      <text>Stems multiple, erect, straight;</text>
      <biological_entity id="o13563" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distal or along entire stem, distal longest or all ± equal, antrorse, upcurved;</text>
      <biological_entity id="o13564" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="along entire stem" value_original="along entire stem" />
      </biological_entity>
      <biological_entity id="o13565" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o13564" id="r1128" name="along" negation="false" src="d0_s2" to="o13565" />
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="distal" id="o13566" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character name="length" src="d0_s2" value="all" value_original="all" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="upcurved" value_original="upcurved" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 17–103 × 7.6–41.2 mm, widest intersinus distance 5.1–35.3 mm;</text>
      <biological_entity id="o13567" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13568" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s4" to="103" to_unit="mm" />
        <character char_type="range_value" from="7.6" from_unit="mm" name="width" src="d0_s4" to="41.2" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="5.1" from_unit="mm" name="some_measurement" src="d0_s4" to="35.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate to elliptic, margins serrate, teeth 6–22, slightly antrorse, 0.4–5.3 mm;</text>
      <biological_entity id="o13569" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o13570" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity id="o13571" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o13572" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="22" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal elliptic, lanceolate, or deltate, base clasping, margins serrate, teeth 6–16, slightly antrorse, 0.5–4.2 mm;</text>
      <biological_entity id="o13573" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o13574" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o13575" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o13576" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o13577" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="16" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="antrorse" value_original="antrorse" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with complex grappling-hook trichomes, adaxial surface with complex grappling-hook and needlelike trichomes, both surfaces green, moderately hairy.</text>
      <biological_entity id="o13578" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o13579" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity constraint="grappling-hook" id="o13580" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13581" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o13582" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o13583" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity id="o13584" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o13579" id="r1129" name="with" negation="false" src="d0_s7" to="o13580" />
      <relation from="o13581" id="r1130" name="with" negation="false" src="d0_s7" to="o13582" />
      <relation from="o13581" id="r1131" name="with" negation="false" src="d0_s7" to="o13583" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins entire.</text>
      <biological_entity id="o13585" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o13586" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 7–14.5 (–16.2) × 1.7–5.2 mm, apex rounded, glabrous abaxially;</text>
      <biological_entity id="o13587" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13588" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="14.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="16.2" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="14.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s9" to="5.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13589" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 5.3–11.1 (–15.4) × 0.6–4.4 mm, with anthers, second whorl with anthers;</text>
      <biological_entity id="o13590" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13591" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o13592" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o13593" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="11.1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="15.4" to_unit="mm" />
        <character char_type="range_value" from="5.3" from_unit="mm" name="length" src="d0_s10" to="11.1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13594" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o13595" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o13596" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o13593" id="r1132" name="with" negation="false" src="d0_s10" to="o13594" />
      <relation from="o13595" id="r1133" name="with" negation="false" src="d0_s10" to="o13596" />
    </statement>
    <statement id="d0_s11">
      <text>anthers twisted after dehiscence, epidermis smooth;</text>
      <biological_entity id="o13597" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13598" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o13599" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3.8–8.1 mm.</text>
      <biological_entity id="o13600" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13601" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s12" to="8.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cupshaped, 5.8–9 × 5.2–8.8 mm, base rounded, not longitudinally ridged.</text>
      <biological_entity id="o13602" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="5.8" from_unit="mm" name="length" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="5.2" from_unit="mm" name="width" src="d0_s13" to="8.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13603" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls straight, papillae 6–17 per cell.</text>
      <biological_entity id="o13604" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o13605" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o13606" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o13608" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 22.</text>
      <biological_entity id="o13607" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o13608" from="6" name="quantity" src="d0_s14" to="17" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13609" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia oreophila is found in Inyo, Riverside, and San Bernardino counties, California, and Clark, Esmeralda, Lincoln, and Nye counties, Nevada. The California populations have a smaller stature than those in Nevada, and phylogenetic analysis indicated that the species is potentially polyphyletic.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sparsely vegetated slopes, roadcuts, loose, rocky and sandy limestone soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="sparsely vegetated" />
        <character name="habitat" value="roadcuts" />
        <character name="habitat" value="loose" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy limestone soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>