<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="(Reichenbach) Engelmann" date="1852" rank="section">Linopsis</taxon_name>
    <taxon_name authority="C. M. Rogers" date="1979" rank="species">lundellii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>8: 184, fig. 4c. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linopsis;species lundellii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101721</other_info_on_name>
  </taxon_identification>
  <number>31.</number>
  <other_name type="common_name">Sullivan City flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 10–40 cm, glabrate.</text>
      <biological_entity id="o2354" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrate" value_original="glabrate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, few-branched.</text>
      <biological_entity id="o2355" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="few-branched" value_original="few-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, spreading;</text>
      <biological_entity id="o2356" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands moderately developed at proximal nodes, absent on distal nodes;</text>
      <biological_entity constraint="stipular" id="o2357" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character constraint="at proximal nodes" constraintid="o2358" is_modifier="false" modifier="moderately" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character constraint="on distal nodes" constraintid="o2359" is_modifier="false" name="presence" notes="" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2358" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o2359" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 5–30 × 0.5–1.5 mm, margins entire, not ciliate, apex acute.</text>
      <biological_entity id="o2360" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2361" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2362" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences panicles.</text>
      <biological_entity constraint="inflorescences" id="o2363" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 5–13 mm.</text>
      <biological_entity id="o2364" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals deciduous, linear-lanceolate to lanceolate, 4–12 mm, margins narrowly scarious, glandular-toothed, apex acute to acuminate;</text>
      <biological_entity id="o2365" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2366" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2367" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s7" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o2368" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow to orange salmon, faintly maroon banded near base, obcordate, 7–12 mm;</text>
      <biological_entity id="o2369" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2370" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange salmon" />
        <character constraint="near base" constraintid="o2371" is_modifier="false" modifier="faintly" name="coloration" src="d0_s8" value="maroon banded" value_original="maroon banded" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2371" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 4–5 mm;</text>
      <biological_entity id="o2372" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2373" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 1–1.5 mm;</text>
      <biological_entity id="o2374" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2375" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodia absent;</text>
      <biological_entity id="o2376" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2377" name="staminodium" name_original="staminodia" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles connate nearly to apex, 3–4 mm;</text>
      <biological_entity id="o2378" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2379" name="style" name_original="styles" src="d0_s12" type="structure">
        <character constraint="to apex" constraintid="o2380" is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2380" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas dark, capitate.</text>
      <biological_entity id="o2381" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2382" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark" value_original="dark" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid, 3.3–4 × 2.6–3.1 mm, apex obtuse, dehiscing into 5, 2-seeded segments, segments persistent on plant, false septa complete, proximal part membranaceous, not terminating in loose fringe, distal part cartilaginous, margins not ciliate.</text>
      <biological_entity id="o2383" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s14" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2384" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character constraint="into segments" constraintid="o2385" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o2385" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="2-seeded" value_original="2-seeded" />
      </biological_entity>
      <biological_entity id="o2386" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character constraint="on plant" constraintid="o2387" is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o2387" name="plant" name_original="plant" src="d0_s14" type="structure" />
      <biological_entity constraint="false" id="o2388" name="septum" name_original="septa" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="complete" value_original="complete" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2389" name="part" name_original="part" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
      <biological_entity id="o2390" name="fringe" name_original="fringe" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s14" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2391" name="part" name_original="part" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence_or_texture" src="d0_s14" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity id="o2392" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o2389" id="r220" name="terminating in" negation="false" src="d0_s14" to="o2390" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2.5–2.7 × 1.1 mm. 2n = 30.</text>
      <biological_entity id="o2393" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="2.7" to_unit="mm" />
        <character name="width" src="d0_s15" unit="mm" value="1.1" value_original="1.1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2394" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Linum lundellii occurs in southern Texas and adjacent Tamaulipas (the collection from Nuevo León, Mueller 470, TEX, made at 2400 m, may be misidentified); it can be distinguished from other species by its relatively very short styles. C. M. Rogers (1968) identified a variable population of yellow-flowered plants that he included in L. berlandieri var. filifloium (then treated as L. rigidum var. filifolium). As a result of subsequent study of these plants, he concluded that L. lundellii and L. elongatum should be recognized as separate species. Rogers (1979) compared garden-grown plants of these three taxa and observed that the anthers of L. lundellii are at the same level as the stigmas at anthesis and that pollen had already been deposited on stigmas when the flowers opened, whereas styles of L. berlandieri var. filifolium and L. elongatum are much longer than the stamens and pollen is not shed before anthesis.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy loam in arroyos, gravelly hillsides, mesquite scrub woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy loam" constraint="in arroyos , gravelly hillsides" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="gravelly hillsides" />
        <character name="habitat" value="mesquite scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>