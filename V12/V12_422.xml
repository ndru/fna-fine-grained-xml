<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">ZIZIPHUS</taxon_name>
    <taxon_name authority="Lamarck in J. Lamarck et al." date="1789" rank="species">mauritiana</taxon_name>
    <place_of_publication>
      <publication_title>Encycl.</publication_title>
      <place_in_publication>3: 319. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ziziphus;species mauritiana</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200013471</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhamnus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">jujuba</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 194. 1753</place_in_publication>
      <other_info_on_pub>not Ziziphus jujuba Miller 1768</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus rhamnus;species jujuba</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Indian jujube</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, 3–10 (–15) m;</text>
      <biological_entity id="o6246" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="m" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>secondary branches white-silvery to grayish, becoming brown, tomentose, glabrescent, not thorn-tipped, axillary thorns absent;</text>
      <biological_entity constraint="secondary" id="o6248" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="white-silvery" name="coloration" src="d0_s1" to="grayish" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6249" name="thorn" name_original="thorns" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipular spines usually present, straight or recurving, 2–3 mm, solitary or paired, sometimes absent.</text>
      <biological_entity constraint="stipular" id="o6250" name="spine" name_original="spines" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurving" value_original="recurving" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, alternate;</text>
      <biological_entity id="o6251" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade whitish to tawny abaxially, dark green adaxially, oblong to elliptic or elliptic-ovate, 2.5–8 cm, base obtuse to rounded, usually oblique, margins serrulate, apex rounded, abaxial surface tomentose, adaxial surface glabrous;</text>
      <biological_entity id="o6252" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s4" to="tawny abaxially" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic or elliptic-ovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6253" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="usually" name="orientation_or_shape" src="d0_s4" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o6254" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o6255" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6256" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>3-veined from base.</text>
      <biological_entity constraint="adaxial" id="o6257" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="from base" constraintid="o6258" is_modifier="false" name="architecture" src="d0_s5" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o6258" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymes, 2–8-flowered.</text>
      <biological_entity constraint="inflorescences" id="o6259" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium and sepals greenish to greenish white;</text>
      <biological_entity id="o6260" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6261" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s7" to="greenish white" />
      </biological_entity>
      <biological_entity id="o6262" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s7" to="greenish white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white.</text>
      <biological_entity id="o6263" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6264" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Drupes ripening from yellow or orange to red or reddish-brown, globose to ovoid or oblong, 20–30 cm. 2n = 24.</text>
      <biological_entity id="o6265" name="drupe" name_original="drupes" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="ripening" value_original="ripening" />
        <character char_type="range_value" from="orange" name="coloration" src="d0_s9" to="red or reddish-brown" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="ovoid or oblong" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s9" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6266" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Florida records for Ziziphus mauritiana are from Broward and Miami-Dade counties. Reports of the species from California (that is, USDA Plants Database) presumably refer to Z. jujuba.</discussion>
  <discussion>Ziziphus mauritiana is a major commercial fruit-producing species in India with many cultivars varying in fruiting season and in fruit form, size, color, flavor, and keeping quality. The fruit is rich in vitamin C and is eaten raw, pickled, or used in beverages.</discussion>
  <discussion>The illegitimate name Ziziphus jujuba (Linnaeus) Gaertner is sometimes used for this species.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, thicket edges, rockland hammocks.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="thicket edges" />
        <character name="habitat" value="hammocks" modifier="rockland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; c, se Asia; introduced also in West Indies (Jamaica, Lesser Antilles, Puerto Rico), Europe, elsewhere in Asia, Africa, Indian Ocean Islands (Seychelles Islands), Pacific Islands (Fiji, Hawaii, New Caledonia, Papua New Guinea, Philippines), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value=" also in West Indies (Jamaica)" establishment_means="introduced" />
        <character name="distribution" value=" also in West Indies (Lesser Antilles)" establishment_means="introduced" />
        <character name="distribution" value=" also in West Indies (Puerto Rico)" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="elsewhere in Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Seychelles Islands)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Fiji)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Caledonia)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Papua New Guinea)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Philippines)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>