<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">humilis</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10, 2: 1276. 1759</place_in_publication>
      <other_info_on_pub>(as humile)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species humilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101974</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Croton</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">berlandieri</taxon_name>
    <taxon_hierarchy>genus croton;species berlandieri</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Low croton</other_name>
  <other_name type="common_name">pepperbush</other_name>
  <other_name type="common_name">salvia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 3–8 dm, monoecious.</text>
      <biological_entity id="o23950" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems much branched, stellate-hairy, viscid.</text>
      <biological_entity id="o23951" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not clustered;</text>
      <biological_entity id="o23952" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules 2–5 stipitate-glands, to 0.5 mm;</text>
      <biological_entity id="o23953" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23954" name="stipitate-gland" name_original="stipitate-glands" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.7–3 (–5) cm, 3/8–5/8 leaf-blade length, glands absent at apex;</text>
      <biological_entity id="o23955" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="3/8" name="quantity" src="d0_s4" to="5/8" />
      </biological_entity>
      <biological_entity id="o23956" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure" />
      <biological_entity id="o23957" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o23958" is_modifier="false" name="length" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23958" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to oblong, 1.5–8 × 1–2 (–5) cm, base rounded to subcordate, margins entire to minutely glandular-denticulate, apex abruptly acute to acuminate, both surfaces pale green, abaxial densely stellate-hairy, adaxial tomentose, glabrescent.</text>
      <biological_entity id="o23959" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23960" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="subcordate" />
      </biological_entity>
      <biological_entity id="o23961" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="minutely glandular-denticulate" />
      </biological_entity>
      <biological_entity id="o23962" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="abruptly acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o23963" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23964" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23965" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences bisexual or unisexual, racemes, 3–7 cm, staminate flowers 20–35, pistillate flowers 2–6.</text>
      <biological_entity id="o23966" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o23967" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23968" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="35" />
      </biological_entity>
      <biological_entity id="o23969" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: staminate 3–4 mm, pistillate 1–2 (–3) mm.</text>
      <biological_entity id="o23970" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 5, 3–4 mm, abaxial surface stellate-hairy;</text>
      <biological_entity id="o23971" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23972" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23973" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, spatulate, 3–4 mm, abaxial surface glabrous except margins ciliate basally;</text>
      <biological_entity id="o23974" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23975" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23976" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character constraint="except margins" constraintid="o23977" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23977" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 15–35.</text>
      <biological_entity id="o23978" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23979" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 5, equal, 4 mm, margins entire, sessile or shortly stipitate-glandular, apex incurved, abaxial surface stellate-hairy;</text>
      <biological_entity id="o23980" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23981" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o23982" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23983" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23984" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0 or 5, white, subulate, 1 mm;</text>
      <biological_entity id="o23985" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23986" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s12" unit="or" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular;</text>
      <biological_entity id="o23987" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23988" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, 3–5 mm, 4-fid, terminal segments 12.</text>
      <biological_entity id="o23989" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23990" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-fid" value_original="4-fid" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o23991" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 4–5 × 4 mm, smooth;</text>
      <biological_entity id="o23992" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s15" to="5" to_unit="mm" />
        <character name="width" src="d0_s15" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella apex with 3 rounded, inflated lobes.</text>
      <biological_entity constraint="columella" id="o23993" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o23994" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s16" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o23993" id="r1975" name="with" negation="false" src="d0_s16" to="o23994" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 3–4 × 2.5–3 mm, shiny.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 20.</text>
      <biological_entity id="o23995" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s17" to="3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23996" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Croton humilis is a mainly West Indian species extending from southernmost Florida (Collier and Monroe counties) to the Bahamas, Cuba, Hispaniola, Jamaica, and Puerto Rico, and up the Caribbean coast of Mexico to southernmost Texas (Cameron, Hidalgo, Starr, Willacy, and Zapata counties). Texas plants have more stamens (30–35) than Florida plants (15–20).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, thickets, disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Tex.; e, se Mexico; West Indies.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="se Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>