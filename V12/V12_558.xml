<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="1831" rank="species">soliman</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>6: 361. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species soliman</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101968</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Soliman’s croton</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–8 dm, monoecious.</text>
      <biological_entity id="o18538" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely branched distally, sparsely stellate-hairy.</text>
      <biological_entity id="o18539" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not clustered;</text>
      <biological_entity id="o18540" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linear, (0–) 1–2 mm, sometimes short-stipitate-glandular;</text>
      <biological_entity id="o18541" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–2 cm, 2/5–1/2 leaf-blade length, glands absent at apex;</text>
      <biological_entity id="o18542" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="2/5" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o18543" name="leaf-blade" name_original="leaf-blade" src="d0_s4" type="structure" />
      <biological_entity id="o18544" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o18545" is_modifier="false" name="length" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18545" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate, 2–5 × 1.5–2.5 cm, base rounded-cuneate, margins entire, with scattered glandular-capitate processes 1 mm, usually denser at base, apex acute to acuminate, abaxial surface green, glabrous or sparsely stellate-hairy, usually along margin, adaxial surface slightly darker green, glabrous.</text>
      <biological_entity id="o18546" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18547" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-cuneate" value_original="rounded-cuneate" />
      </biological_entity>
      <biological_entity id="o18548" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="at base" constraintid="o18550" is_modifier="false" modifier="usually" name="density" notes="" src="d0_s5" value="denser" value_original="denser" />
      </biological_entity>
      <biological_entity id="o18549" name="process" name_original="processes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="glandular-capitate" value_original="glandular-capitate" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o18550" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18551" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18552" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o18553" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o18554" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="darker green" value_original="darker green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o18548" id="r1518" name="with" negation="false" src="d0_s5" to="o18549" />
      <relation from="o18552" id="r1519" modifier="usually" name="along" negation="false" src="d0_s5" to="o18553" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences bisexual, racemes, 2–4 cm, staminate flowers 10–30, pistillate flowers 3–6.</text>
      <biological_entity id="o18555" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o18556" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18557" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
      <biological_entity id="o18558" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: staminate 1–5 mm, pistillate 2–5 mm.</text>
      <biological_entity id="o18559" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 5, 0.8–1 mm, abaxial surface sparsely stellate-hairy;</text>
      <biological_entity id="o18560" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18561" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18562" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, spatulate, 1–1.3 mm, abaxial surface glabrous except margins ciliate basally;</text>
      <biological_entity id="o18563" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18564" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18565" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character constraint="except margins" constraintid="o18566" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18566" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 15–20.</text>
      <biological_entity id="o18567" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18568" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 5, unequal, 5–6 mm, margins entire, apex straight to slightly incurved, abaxial surface glabrous or with a few stellate hairs;</text>
      <biological_entity id="o18569" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18570" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18571" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18572" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18573" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="with a few stellate hairs" />
      </biological_entity>
      <biological_entity id="o18574" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s11" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o18573" id="r1520" name="with" negation="false" src="d0_s11" to="o18574" />
    </statement>
    <statement id="d0_s12">
      <text>petals 0 or 5, white, linear, 2.5–3.5 mm;</text>
      <biological_entity id="o18575" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18576" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s12" unit="or" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary 3-locular;</text>
      <biological_entity id="o18577" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18578" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, 3–4 mm, 4-fid, terminal segments 12.</text>
      <biological_entity id="o18579" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18580" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-fid" value_original="4-fid" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o18581" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 6 × 5–6 mm, smooth;</text>
      <biological_entity id="o18582" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character name="length" src="d0_s15" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella apex with 3 rounded, inflated lobes.</text>
      <biological_entity constraint="columella" id="o18583" name="apex" name_original="apex" src="d0_s16" type="structure" />
      <biological_entity id="o18584" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s16" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o18583" id="r1521" name="with" negation="false" src="d0_s16" to="o18584" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 4–5 × 3–4 mm, shiny.</text>
      <biological_entity id="o18585" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Croton soliman is most similar to C. humilis. In the flora area, C. soliman is found only in Cameron County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, low ridges.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="low ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; e, s Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>