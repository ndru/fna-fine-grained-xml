<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">VITACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VITIS</taxon_name>
    <taxon_name authority="(Planchon) Rehder" date="1927" rank="subgenus">Muscadinia</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">rotundifolia</taxon_name>
    <taxon_name authority="(J. H. Simpson ex Planchon) M. O. Moore" date="1991" rank="variety">munsoniana</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>14: 345. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family vitaceae;genus vitis;subgenus muscadinia;species rotundifolia;variety munsoniana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vitis</taxon_name>
    <taxon_name authority="J. H. Simpson ex Planchon in A. L. P. P. de Candolle and C. de Candolle" date="1887" rank="species">munsoniana</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Phan.</publication_title>
      <place_in_publication>5: 615. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus vitis;species munsoniana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muscadinia</taxon_name>
    <taxon_name authority="(J. H. Simpson ex Planchon) Small" date="unknown" rank="species">munsoniana</taxon_name>
    <taxon_hierarchy>genus muscadinia;species munsoniana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rotundifolia</taxon_name>
    <taxon_name authority="McFarlin ex D. B. Ward" date="unknown" rank="variety">pygmaea</taxon_name>
    <taxon_hierarchy>genus v.;species rotundifolia;variety pygmaea</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <other_name type="common_name">Everbearing or bullace or pygmy grape</other_name>
  <other_name type="common_name">little muscadine</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants high climbing to sometimes shrubby or trailing.</text>
      <biological_entity id="o6311" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height" src="d0_s0" value="high" value_original="high" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing to sometimes" value_original="climbing to sometimes" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="shrubby" value_original="shrubby" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blade 3–8 cm diam.</text>
      <biological_entity id="o6312" name="leaf-blade" name_original="leaf-blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Berries 12–30 per infructescence, 8–12 mm diam.</text>
      <biological_entity id="o6313" name="berry" name_original="berries" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per infructescence" constraintid="o6314" from="12" name="quantity" src="d0_s2" to="30" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" notes="" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6314" name="infructescence" name_original="infructescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Seeds to 6 mm.</text>
      <biological_entity id="o6315" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety munsoniana is distinguished from the more widespread var. rotundifolia by its smaller and more numerous berries, and these traits remain consistent under greenhouse conditions. It may well represent a species distinct from Vitis rotundifolia.</discussion>
  <discussion>Variety pygmaea was described based on collections from Polk and Highlands counties, Florida, with shrubby or trailing habit, small leaves, and narrow and ridged seed raphe. Field studies by J. Wen suggest that these characteristics intergrade with those of var. munsoniana and that it is an extreme xeric ecotype of var. munsoniana, which occurs in the same areas.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year-round in peninsular Florida, late Apr–May farther north; fruiting late Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in peninsular Florid" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" modifier="farther north" to="May" from="late Apr" />
        <character name="fruiting time" char_type="range_value" modifier="in peninsular Florid" to="" from="" constraint=" year round" />
        <character name="fruiting time" char_type="range_value" modifier="farther north" to="May" from="late Apr" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="late Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Floodplain forests, riverbanks, hammocks, pinelands, sand pine scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="floodplain forests" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="scrub" modifier="pine" />
        <character name="habitat" value="sand" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>