<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">439</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VISCUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1023. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 448. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus viscum</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin name for mistletoe, alluding to viscid fruits</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">134632</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Christmas mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [herbs], dioecious [monoecious];</text>
    </statement>
    <statement id="d0_s1">
      <text>parasitic on branches of woody angiosperms and gymnosperms, infections localized.</text>
      <biological_entity id="o17463" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character constraint="on branches" constraintid="o17464" is_modifier="false" name="nutrition" src="d0_s1" value="parasitic" value_original="parasitic" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o17464" name="branch" name_original="branches" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems single or multiple;</text>
    </statement>
    <statement id="d0_s3">
      <text>branching pseudodichotomous [percurrent].</text>
      <biological_entity id="o17465" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves well developed [scalelike].</text>
      <biological_entity id="o17466" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s4" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary or terminal, dichasial cymes.</text>
      <biological_entity id="o17467" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17468" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="dichasial" value_original="dichasial" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: petals (3–) 4 (–6), triangular, distinct;</text>
      <biological_entity id="o17469" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17470" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s6" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="6" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens (3–) 4 (–6);</text>
      <biological_entity id="o17471" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17472" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers multilocular, dehiscing by numerous pores;</text>
      <biological_entity id="o17473" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17474" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="multilocular" value_original="multilocular" />
        <character constraint="by pores" constraintid="o17475" is_modifier="false" name="dehiscence" src="d0_s8" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o17475" name="pore" name_original="pores" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectary absent.</text>
      <biological_entity id="o17476" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17477" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: petals (3–) 4 (–6), triangular, distinct;</text>
      <biological_entity id="o17478" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17479" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary 0-locular;</text>
      <biological_entity id="o17480" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17481" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="0-locular" value_original="0-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style absent [short-conic];</text>
      <biological_entity id="o17482" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17483" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma poorly differentiated [capitate].</text>
      <biological_entity id="o17484" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17485" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="poorly" name="variability" src="d0_s13" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Berries sessile in bracteal cup [pedicel present, not recurved], not explosively dehiscent, 1-colored, smooth [warty], scars of petal remnants at apex.</text>
      <biological_entity id="o17486" name="berry" name_original="berries" src="d0_s14" type="structure">
        <character constraint="in bracteal, cup, scars" constraintid="o17487, o17488, o17489" is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17487" name="bracteal" name_original="bracteal" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not explosively" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="1-colored" value_original="1-colored" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o17488" name="cup" name_original="cup" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not explosively" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="1-colored" value_original="1-colored" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o17489" name="scar" name_original="scars" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not explosively" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="1-colored" value_original="1-colored" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="petal" id="o17490" name="remnant" name_original="remnants" src="d0_s14" type="structure" />
      <biological_entity id="o17491" name="apex" name_original="apex" src="d0_s14" type="structure" />
      <relation from="o17487" id="r1439" name="part_of" negation="false" src="d0_s14" to="o17490" />
      <relation from="o17488" id="r1440" name="part_of" negation="false" src="d0_s14" to="o17490" />
      <relation from="o17489" id="r1441" name="part_of" negation="false" src="d0_s14" to="o17490" />
      <relation from="o17487" id="r1442" name="at" negation="false" src="d0_s14" to="o17491" />
      <relation from="o17488" id="r1443" name="at" negation="false" src="d0_s14" to="o17491" />
      <relation from="o17489" id="r1444" name="at" negation="false" src="d0_s14" to="o17491" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds mucilaginous when removed from fruit, endosperm slightly flattened, ovate to elliptic in broadest outline;</text>
      <biological_entity id="o17492" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="when removed from fruit" name="coating" src="d0_s15" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity id="o17493" name="fruit" name_original="fruit" src="d0_s15" type="structure" />
      <biological_entity id="o17494" name="endosperm" name_original="endosperm" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="slightly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="true" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="true" name="shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" name="width" src="d0_s15" value="broadest" value_original="broadest" />
      </biological_entity>
      <relation from="o17492" id="r1445" name="removed from" negation="false" src="d0_s15" to="o17493" />
      <relation from="o17492" id="r1446" name="removed from" negation="false" src="d0_s15" to="o17494" />
    </statement>
    <statement id="d0_s16">
      <text>embryo oriented transversely.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 14.</text>
      <biological_entity id="o17495" name="embryo" name_original="embryo" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="transversely" name="orientation" src="d0_s16" value="oriented" value_original="oriented" />
      </biological_entity>
      <biological_entity constraint="x" id="o17496" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 130 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; Eurasia, Africa, Indian Ocean Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Viscum is widely distributed in the Old World and is present in North America via purposeful introduction. The genus is most diverse in tropical and southern Africa, where various species form a decreasing aneuploid series (from × = 14) (D. Wiens 1975). Higher gametic chromosome numbers are the result of polyploidy, relatively uncommon in Viscaceae. Several species of dioecious Viscum show translocation heterozygosity that determines plant sexuality and sex ratios in populations (A. Aparicio 1993; Wiens and B. A. Barlow 1979).</discussion>
  <references>
    <reference>Bussing, A., ed. 2000. Mistletoe: The genus Viscum. Amsterdam.</reference>
    <reference>Gill, L. S. 1935. Arceuthobium in the United States. Trans. Connecticut Acad. Arts 32: 111–245</reference>
    <reference>Wiens, D. and B. A. Barlow. 1979. Translocation heterozygosity and the origin of dioecy in Viscum. Heredity 42: 201–222.</reference>
  </references>
  
</bio:treatment>