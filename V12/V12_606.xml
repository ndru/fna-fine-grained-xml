<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="C. M. Rogers" date="1966" rank="genus">SCLEROLINON</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>18: 182, figs. 2–6. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus sclerolinon</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek skleros, hard, and linon, flax, alluding to fruit</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">129791</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Hard flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, glabrous, sometimes glaucous.</text>
      <biological_entity id="o5771" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually unbranched, sometimes branched proximally and corymbosely branched distally.</text>
      <biological_entity id="o5772" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="corymbosely; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persistent, opposite, or distal sometimes alternate;</text>
      <biological_entity id="o5773" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5774" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipular glands absent;</text>
      <biological_entity constraint="stipular" id="o5775" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong to elliptic, margins entire, or distal leaves sometimes serrate.</text>
      <biological_entity id="o5776" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic" />
      </biological_entity>
      <biological_entity id="o5777" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o5778" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymes.</text>
      <biological_entity constraint="inflorescences" id="o5779" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels articulated.</text>
      <biological_entity id="o5780" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="articulated" value_original="articulated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals persistent, 5, connate at base, unequal in size, margins glandular-toothed, glabrous;</text>
      <biological_entity id="o5781" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5782" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character constraint="at base" constraintid="o5783" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o5783" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o5784" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="glandular-toothed" value_original="glandular-toothed" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 5, distinct, attached proximal to rim of cup between filament bases, yellow, appendages absent;</text>
      <biological_entity id="o5785" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5786" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fixation" src="d0_s8" value="attached" value_original="attached" />
        <character constraint="to rim" constraintid="o5787" is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="between bases" constraintid="o5789" id="o5787" name="rim" name_original="rim" src="d0_s8" type="structure" constraint_original="between  bases, " />
      <biological_entity id="o5788" name="cup" name_original="cup" src="d0_s8" type="structure" />
      <biological_entity constraint="filament" id="o5789" name="base" name_original="bases" src="d0_s8" type="structure" />
      <biological_entity id="o5790" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o5787" id="r511" name="part_of" negation="false" src="d0_s8" to="o5788" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 5;</text>
      <biological_entity id="o5791" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5792" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 0;</text>
      <biological_entity id="o5793" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5794" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistil 2-carpellate, ovary 4-locular, false septa complete and similar to true septa;</text>
      <biological_entity id="o5795" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5796" name="pistil" name_original="pistil" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-carpellate" value_original="2-carpellate" />
      </biological_entity>
      <biological_entity id="o5797" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="4-locular" value_original="4-locular" />
      </biological_entity>
      <biological_entity constraint="false" id="o5798" name="septum" name_original="septa" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="complete" value_original="complete" />
      </biological_entity>
      <biological_entity id="o5799" name="septum" name_original="septa" src="d0_s11" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s11" value="true" value_original="true" />
      </biological_entity>
      <relation from="o5798" id="r512" name="to" negation="false" src="d0_s11" to="o5799" />
    </statement>
    <statement id="d0_s12">
      <text>styles 2, distinct, or connate at base;</text>
      <biological_entity id="o5800" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5801" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o5802" is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o5802" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas capitate, wider than styles.</text>
      <biological_entity id="o5803" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o5804" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
        <character constraint="than styles" constraintid="o5805" is_modifier="false" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o5805" name="style" name_original="styles" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits schizocarps, breaking into 4 nutlets, or indehiscent.</text>
      <biological_entity constraint="fruits" id="o5806" name="schizocarp" name_original="schizocarps" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o5807" name="nutlet" name_original="nutlets" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <relation from="o5806" id="r513" name="breaking into" negation="false" src="d0_s14" to="o5807" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 4, narrowly ovate.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 6.</text>
      <biological_entity id="o5808" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="x" id="o5809" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>