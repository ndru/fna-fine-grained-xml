<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">GARRYACEAE</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1834" rank="genus">GARRYA</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>20: plate 1686. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family garryaceae;genus garrya</taxon_hierarchy>
    <other_info_on_name type="etymology">For Nicholas Garry, 1782–1856, deputy-governor of the Hudson’s Bay Company from 1822–1835, diarist of his 1821 travels in the Northwest Territories, friend and benefactor of David Douglas</other_info_on_name>
    <other_info_on_name type="fna_id">113275</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees.</text>
      <biological_entity id="o17786" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade flat to concave-convex, coriaceous, margins entire, flat, revolute, or strongly undulate.</text>
      <biological_entity id="o17788" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o17789" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s1" to="concave-convex" />
        <character is_modifier="false" name="texture" src="d0_s1" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o17790" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s1" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences axillary, aments;</text>
      <biological_entity id="o17791" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o17792" name="ament" name_original="aments" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>bracts opposite, distinct or connate basally;</text>
      <biological_entity id="o17793" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracteoles 0.</text>
      <biological_entity id="o17794" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels: staminate present, pistillate absent.</text>
      <biological_entity id="o17795" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: sepals 4, valvate in bud, apically connivent by intertwined hairs, linear to lanceolate-oblong;</text>
      <biological_entity id="o17796" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17797" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character constraint="in bud" constraintid="o17798" is_modifier="false" name="arrangement_or_dehiscence" src="d0_s6" value="valvate" value_original="valvate" />
        <character constraint="by hairs" constraintid="o17799" is_modifier="false" modifier="apically" name="arrangement" notes="" src="d0_s6" value="connivent" value_original="connivent" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s6" to="lanceolate-oblong" />
      </biological_entity>
      <biological_entity id="o17798" name="bud" name_original="bud" src="d0_s6" type="structure" />
      <biological_entity id="o17799" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="intertwined" value_original="intertwined" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 0;</text>
      <biological_entity id="o17800" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17801" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>nectary absent or vestigial;</text>
      <biological_entity id="o17802" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17803" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens alternate with sepals, anthers basifixed.</text>
      <biological_entity id="o17804" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o17805" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character constraint="with sepals" constraintid="o17806" is_modifier="false" name="arrangement" src="d0_s9" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o17806" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o17807" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="basifixed" value_original="basifixed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals 2, sometimes rudimentary;</text>
      <biological_entity id="o17808" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17809" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 0;</text>
      <biological_entity id="o17810" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17811" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary absent;</text>
      <biological_entity id="o17812" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17813" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 2;</text>
      <biological_entity id="o17814" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17815" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2 (–3), erect or recurved, linear-lanceolate;</text>
      <biological_entity id="o17816" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17817" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="3" />
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas decurrent on adaxial surfaces of styles.</text>
      <biological_entity id="o17818" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17819" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character constraint="on adaxial surfaces" constraintid="o17820" is_modifier="false" name="shape" src="d0_s15" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17820" name="surface" name_original="surfaces" src="d0_s15" type="structure" />
      <biological_entity id="o17821" name="style" name_original="styles" src="d0_s15" type="structure" />
      <relation from="o17820" id="r1469" name="part_of" negation="false" src="d0_s15" to="o17821" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits berries, dark blue to black, drying whitish-gray, subglobose to ovoid, fleshy, becoming brittle.</text>
      <biological_entity constraint="fruits" id="o17822" name="berry" name_original="berries" src="d0_s16" type="structure">
        <character char_type="range_value" from="dark blue" name="coloration" src="d0_s16" to="black" />
        <character is_modifier="false" name="condition" src="d0_s16" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish-gray" value_original="whitish-gray" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s16" to="ovoid" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" modifier="becoming" name="fragility" src="d0_s16" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds (1–) 2 per fruit.</text>
      <biological_entity id="o17824" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>x = 11.</text>
      <biological_entity id="o17823" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="2" to_inclusive="false" />
        <character constraint="per fruit" constraintid="o17824" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="x" id="o17825" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 17 (8 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, sc United States, Mexico, West Indies (Cuba, Hispaniola, Jamaica), Central America; some cultivated.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="some cultivated" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Two sections of Garrya have been recognized (see key below). Species of sect. Garrya range from Washington to Baja California and New Mexico, with one outlier in Guatemala, G. corvorum Standley &amp; Steyermark. Section Fadyenia de Candolle ranges from trans-Pecos Texas to Arizona and from Mexico (including Baja California) to Central America plus the West Indies (Cuba, Hispaniola, and Jamaica). In addition to the features noted in the key, in sect. Garrya pistillate flowers bear a pair of small appendages at the ovary apex near the style base, whereas in sect. Fadyenia pistillate flowers occasionally produce two foliaceous bracts partially adnate to the ovary. The phylogenetic distinction between the two sections has been confirmed by D. O. Burge (2011).</discussion>
  <discussion>The staminate flowers in pendulous aments of Garrya are similar to those in other genera specialized for wind-pollination. The staminate aments are more flexible than the pistillate and more responsive to wind currents. P. A. Munz (1959) noted that in California many of the species intergrade extensively where their distributions overlap, and hybridization and intraspecific variation underlie continuing uncertainty about delimitations of taxa. Natural hybrids, however, have not been reported elsewhere. The artificial hybrids G. ×issaquahensis Talbot de Malahide ex E. C. Nelson (G. elliptica × G. fremontii) and G. ×thuretii Carrière (G. elliptica × G. fadyenii Hooker) have been bred for garden planting.</discussion>
  <discussion>The common name silktassel alludes to the sericeous vestiture of the long, showy aments in sect. Garrya. Plants are cultivated mainly for the foliage and showy staminate aments, and cultivars are mostly staminate plants propagated from cuttings.</discussion>
  <discussion>Stem extracts of Garrya laurifolia Bentham are toxic but are used as an antidiarrhetic throughout rural Mexico, and bark extracts were reportedly used by Native Americans to treat fever (G. V. Dahling 1978).</discussion>
  <references>
    <reference>Burge, D. O. 2011. Molecular phylogenetics of Garrya (Garryaceae). Madroño 58: 249–255.</reference>
    <reference>Dahling, G. V. 1978. Systematics and evolution of Garrya. Contr. Gray Herb. 209: 1–104.</reference>
    <reference>Eyde, R. H. 1964. Inferior ovary and generic affinities of Garrya. Amer. J. Bot. 51: 1083–1092.</reference>
    <reference>Liston, A. 2003. A new interpretation of floral morphology in Garrya (Garryaceae). Taxon 52: 271–276.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Staminate aments 1–3 cm; pistillate aments loose, internodes 4+ mm, erect, sometimes branched; pistillate bracts distinct or connate basally, each usually subtending 1 flower, at least proximal similar in size and shape to distal leaves; Arizona, New Mexico, Texas [Garrya sect. Fadyenia].</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces glabrous or sparsely strigose.</description>
      <determination>1. Garrya wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces persistently sparsely to densely puberulent-tomentulose to tomentulose with coiling to recurved hairs.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 1.6–4(–5.5) × 0.7–2.5 cm, margins undulate, ± muricate-roughened, especially distal to middle, adaxial surfaces usually persistently tomentose, sometimes ± glabrescent.</description>
      <determination>2. Garrya goldmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 4.5–8 × 2.5–5 cm, margins flat, smooth, adaxial surfaces glabrate or glabrous.</description>
      <determination>3. Garrya lindheimeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Staminate aments 3–20 cm; pistillate aments compact, internodes to 1 mm, pendulous, unbranched; pistillate bracts connate proximally into deep cup, at least at proximal nodes each subtending 3 flowers, differing in size and shape from leaves; Arizona, California, Nevada, New Mexico, Oregon, Utah, Washington [Garrya sect. Garrya].</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces usually densely, sometimes becoming sparsely, closely tomentose, hairs curled or crisped, interwoven.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade apices rounded to obtuse; pistillate and staminate aments 8–15 cm.</description>
      <determination>4. Garrya elliptica</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade apices acuminate; pistillate and staminate aments 2.5–7 cm.</description>
      <determination>5. Garrya veatchii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces glabrous, glabrate, or sparsely to densely strigose-sericeous or strigose, hairs antrorsely appressed.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades abaxially whitish, adaxially yellow-green to gray-green, dull; berries densely strigose-sericeous, sometimes glabrate toward base.</description>
      <determination>6. Garrya flavescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades abaxially green, adaxially bright to olive green, glossy; berries glabrous, glabrate, or sparsely strigose near apex.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Staminate aments 7–20 cm; leaf blade abaxial surfaces glabrous or sparsely strigose.</description>
      <determination>7. Garrya fremontii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Staminate aments 5–7 cm; leaf blade abaxial surfaces densely strigose-sericeous.</description>
      <determination>8. Garrya buxifolia</determination>
    </key_statement>
  </key>
</bio:treatment>