<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CELASTRACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">PARNASSIA</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>1: 320. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family celastraceae;genus parnassia;species parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101465</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parnassia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">palustris</taxon_name>
    <taxon_name authority="(de Candolle) B. Boivin" date="unknown" rank="variety">parviflora</taxon_name>
    <taxon_hierarchy>genus parnassia;species palustris;variety parviflora</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with caudices.</text>
      <biological_entity id="o14990" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14991" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o14990" id="r1259" name="with" negation="false" src="d0_s0" to="o14991" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–35 cm.</text>
      <biological_entity id="o14992" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal in rosettes;</text>
      <biological_entity id="o14993" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o14994" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14995" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <relation from="o14994" id="r1260" name="in" negation="false" src="d0_s2" to="o14995" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.4–2 cm;</text>
      <biological_entity id="o14996" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14997" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (of larger leaves) ovate to oblong, 6–35 × 5–25 mm, base cuneate to subcordate, apex rounded to subacute;</text>
      <biological_entity id="o14998" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14999" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15000" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o15001" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline on proximal 1/2 to middle of stem.</text>
      <biological_entity id="o15002" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o15003" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o15004" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity constraint="middle" id="o15005" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15006" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o15003" id="r1261" name="on" negation="false" src="d0_s5" to="o15004" />
      <relation from="o15004" id="r1262" name="to" negation="false" src="d0_s5" to="o15005" />
      <relation from="o15005" id="r1263" name="part_of" negation="false" src="d0_s5" to="o15006" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals spreading in fruit, linear-lanceolate to oblong or elliptic-oblong, 3–6 mm, margins not hyaline, entire, apex obtuse;</text>
      <biological_entity id="o15007" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15008" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o15009" is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" notes="" src="d0_s6" to="oblong or elliptic-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15009" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o15010" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15011" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 5–13-veined, oblong to elliptic, 3.5–10 × 4–6 mm, length 1.1–1.5 times sepals, base rounded to cuneate, margins entire;</text>
      <biological_entity id="o15012" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15013" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-13-veined" value_original="5-13-veined" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o15014" is_modifier="false" name="length" src="d0_s7" value="1.1-1.5 times sepals" value_original="1.1-1.5 times sepals" />
      </biological_entity>
      <biological_entity id="o15014" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o15015" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="cuneate" />
      </biological_entity>
      <biological_entity id="o15016" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens (2–) 4–7 mm;</text>
      <biological_entity id="o15017" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15018" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1–1.6 mm;</text>
      <biological_entity id="o15019" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15020" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes obovate, divided distally into 5–7 (–9) gland-tipped filaments, (2–) 3.5–5 mm, shorter than stamens, apical glands suborbicular, 0.2–0.3 mm;</text>
      <biological_entity id="o15021" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15022" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character constraint="into filaments" constraintid="o15023" is_modifier="false" name="shape" src="d0_s10" value="divided" value_original="divided" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s10" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="5" to_unit="mm" />
        <character constraint="than stamens" constraintid="o15024" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15023" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="9" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="7" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o15024" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity constraint="apical" id="o15025" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary green.</text>
      <biological_entity id="o15026" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15027" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 7–10 mm. 2n = 36.</text>
      <biological_entity id="o15028" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15029" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Parnassia parviflora has been included in P. palustris by some authors. Small-flowered plants of P. palustris usually have the staminodes divided into about nine filaments distally and the anthers exceed 1.5 mm, but rarely some plants cannot be clearly assigned to one or other of these species. In Nunavut, P. parviflora is known only from Akimiski Island in James Bay.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, calcareous shores, meadows, fens, seepy scree slopes.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="calcareous shores" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="scree slopes" modifier="seepy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–2900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Nfld. and Labr., N.S., Nunavut, Ont., P.E.I., Que., Sask.; Alaska, Ariz., Calif., Colo., Idaho, Mich., Mont., Nev., N.Dak., S.Dak., Utah, Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>