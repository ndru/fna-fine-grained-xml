<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Linum</taxon_name>
    <taxon_name authority="Desfontaines" date="1798" rank="species">grandiflorum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Atlant.</publication_title>
      <place_in_publication>1: 277, plate 78. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linum;species grandiflorum;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250063162</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Flowering or red or scarlet or crimson flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 10–60 cm, glabrous, glaucous.</text>
      <biological_entity id="o14885" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or sometimes decumbent at base, usually freely branched.</text>
      <biological_entity id="o14886" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o14887" is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="usually freely" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o14887" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear to lanceolate or narrowly elliptic, 10–30 × 2–3 (–7) mm.</text>
      <biological_entity id="o14888" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14889" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate or narrowly elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences cymes, few-flowered.</text>
      <biological_entity constraint="inflorescences" id="o14890" name="cyme" name_original="cymes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 10–25 mm.</text>
      <biological_entity id="o14891" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers heterostylous;</text>
      <biological_entity id="o14892" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="heterostylous" value_original="heterostylous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals lanceolate, 7–11 mm, margins glabrous, apex acuminate;</text>
      <biological_entity id="o14893" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14894" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14895" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals bright red to maroon, fading to purple, broadly obovate, 15–30 mm;</text>
      <biological_entity id="o14896" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s7" to="maroon fading" />
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s7" to="maroon fading" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 8–10 mm;</text>
      <biological_entity id="o14897" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 5 mm;</text>
      <biological_entity id="o14898" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodia not seen;</text>
      <biological_entity id="o14899" name="staminodium" name_original="staminodia" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>styles connate proximal 1/2, 4.5 mm (short-styled) or 8–10 mm (long-styled);</text>
      <biological_entity id="o14900" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character name="some_measurement_or_shape" src="d0_s11" unit="mm" value="1/2" value_original="1/2" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="4.5" value_original="4.5" />
        <character name="some_measurement" src="d0_s11" value="8-10 mm" value_original="8-10 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas clavate.</text>
      <biological_entity id="o14901" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid-globose, 6–7 mm diam., apex apiculate, segments persistent on plant, margins not seen.</text>
      <biological_entity id="o14902" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid-globose" value_original="ovoid-globose" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14903" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o14904" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character constraint="on plant" constraintid="o14905" is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o14905" name="plant" name_original="plant" src="d0_s13" type="structure" />
      <biological_entity id="o14906" name="margin" name_original="margins" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–3 × 0.5–1 mm. 2n = 16.</text>
      <biological_entity id="o14907" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14908" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Linum grandiflorum occasionally escapes from gardens and persists along roadsides and trails. This showy garden plant has blue anthers.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="disturbed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2700 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Colo., Fla., Ky., Nebr., N.Y., Ohio, Pa., Tex., Utah; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>