<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HYDRANGEA</taxon_name>
    <taxon_name authority="Walter" date="1788" rank="species">radiata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>251. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus hydrangea;species radiata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101965</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hydrangea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arborescens</taxon_name>
    <taxon_name authority="(Walter) E. M. McClintock" date="unknown" rank="subspecies">radiata</taxon_name>
    <taxon_hierarchy>genus hydrangea;species arborescens;subspecies radiata</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Silver-leaf or snowy hydrangea</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–30 dm.</text>
      <biological_entity id="o15134" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs glabrous or sparsely to densely tomentose, trichomes usually white, sometimes brown or orangish brown.</text>
      <biological_entity id="o15135" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s1" to="sparsely densely tomentose" />
      </biological_entity>
      <biological_entity id="o15136" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="orangish brown" value_original="orangish brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o15137" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole (0.7–) 1.1–8 cm, glabrous or glabrous abaxially and sparsely to densely tomentose adaxially;</text>
      <biological_entity id="o15138" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely to densely; adaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly to broadly ovate or elliptic, 4–15.2 × 1.3–11.2 cm, unlobed, base truncate to cuneate, margins dentate to serrate, apex acute to acuminate, abaxial surface white or grayish, densely tomentose, trichomes at 40× either smooth, 1–3 mm, or sparsely tuberculate, 0.3–1 mm, adaxial surface green, glabrous or sparsely hirsute along major veins.</text>
      <biological_entity id="o15139" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="15.2" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s4" to="11.2" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o15140" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
      <biological_entity id="o15141" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="dentate to serrate" value_original="dentate to serrate" />
      </biological_entity>
      <biological_entity id="o15142" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15143" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o15144" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" modifier="at 40×either smooth" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="relief" src="d0_s4" value="tuberculate" value_original="tuberculate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15145" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="along veins" constraintid="o15146" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o15146" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences compact, (100–) 200–500-flowered, dome-shaped to hemispheric, 4–13 × 5–14 cm;</text>
      <biological_entity id="o15147" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(100-)200-500-flowered" value_original="(100-)200-500-flowered" />
        <character char_type="range_value" from="dome-shaped" name="shape" src="d0_s5" to="hemispheric" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle 1.6–8.3 cm, sparsely to densely tomentose.</text>
      <biological_entity id="o15148" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" src="d0_s6" to="8.3" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.8–3.2 mm, glabrous or sparsely hirsute.</text>
      <biological_entity id="o15149" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sterile flowers usually present, rarely absent, white or greenish white, tube 5–14 mm, lobes 3–4 (–5), obovate to broadly ovate or round, 5–18 × 3–15 mm.</text>
      <biological_entity id="o15150" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish white" value_original="greenish white" />
      </biological_entity>
      <biological_entity id="o15151" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15152" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="broadly ovate or round" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Bisexual flowers: hypanthium adnate to ovary to near its apex, 0.9–1.2 × 1–1.3 mm, strongly 8–10-ribbed in fruit, glabrous;</text>
      <biological_entity id="o15153" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15154" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character constraint="to ovary" constraintid="o15155" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15155" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o15156" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s9" value="8-10-ribbed" value_original="8-10-ribbed" />
      </biological_entity>
      <biological_entity id="o15157" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <relation from="o15155" id="r1276" name="to" negation="false" src="d0_s9" to="o15156" />
      <relation from="o15156" id="r1277" name="in" negation="false" src="d0_s9" to="o15157" />
    </statement>
    <statement id="d0_s10">
      <text>sepals deltate to triangular, 0.3–0.8 × 0.3–0.5 mm, margins entire, apex acute to acuminate, abaxial surface glabrous;</text>
      <biological_entity id="o15158" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15159" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s10" to="triangular" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s10" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15160" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15161" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15162" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals caducous, white to yellowish white, elliptic to narrowly ovate, 1.2–1.6 × 0.7–1.1 mm;</text>
      <biological_entity id="o15163" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15164" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="caducous" value_original="caducous" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="yellowish white" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="narrowly ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 1.5–4.5 × 0.1–0.2 mm;</text>
      <biological_entity id="o15165" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15166" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.3–0.5 mm;</text>
      <biological_entity id="o15167" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15168" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistil 2 (–3) -carpellate, ovary completely inferior or nearly so;</text>
      <biological_entity id="o15169" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15170" name="pistil" name_original="pistil" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="2(-3)-carpellate" value_original="2(-3)-carpellate" />
      </biological_entity>
      <biological_entity id="o15171" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="completely" name="position" src="d0_s14" value="inferior" value_original="inferior" />
        <character name="position" src="d0_s14" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2 (–3), distinct, (0.6–) 0.9–1.5 mm.</text>
      <biological_entity id="o15172" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o15173" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="3" />
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules hemispheric, (1.3–) 1.7–2.8 × 1.8–3 mm.</text>
      <biological_entity id="o15174" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_length" src="d0_s16" to="1.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s16" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 0.3–0.6 (–0.8) mm.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 36.</text>
      <biological_entity id="o15175" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15176" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist deciduous forests, ravines, banks, rocky slopes, cliffs.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="forests" modifier="deciduous" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>