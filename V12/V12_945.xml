<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">470</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_name authority="Torrey in War Department [U.S.]" date="1857" rank="genus">WHIPPLEA</taxon_name>
    <taxon_name authority="Torrey in War Department [U.S.]" date="1857" rank="species">modesta</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 90, plate 7. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus whipplea;species modesta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220014292</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 10 dm;</text>
      <biological_entity id="o16389" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>adventitious-roots at proximal nodes.</text>
      <biological_entity id="o16390" name="adventitiou-root" name_original="adventitious-roots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o16391" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o16390" id="r1366" name="at" negation="false" src="d0_s1" to="o16391" />
    </statement>
    <statement id="d0_s2">
      <text>Flowering branches weak, (0.4–) 0.6–1.5 dm, appressed-pubescent.</text>
      <biological_entity id="o16392" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.6" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 0–3 mm, pilose;</text>
      <biological_entity id="o16393" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16394" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 10–40 × 10–30 mm, base rounded or tapered, apex obtuse, abaxial surface strigose, adaxial with white, pustule-based trichomes.</text>
      <biological_entity id="o16395" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16396" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16397" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o16398" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16399" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16400" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o16401" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="pustule-based" value_original="pustule-based" />
      </biological_entity>
      <relation from="o16400" id="r1367" name="with" negation="false" src="d0_s4" to="o16401" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences congested to open;</text>
      <biological_entity id="o16402" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="congested" name="architecture" src="d0_s5" to="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle 20–50 mm, appressed-pubescent.</text>
      <biological_entity id="o16403" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="50" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.5–2.5 mm, appressed-pubescent.</text>
      <biological_entity id="o16404" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers odorless;</text>
      <biological_entity id="o16405" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="odor" src="d0_s8" value="odorless" value_original="odorless" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 1.4–2 × 2.3–2.6 mm;</text>
      <biological_entity id="o16406" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 1.5–2 × 0.2–0.5 mm;</text>
      <biological_entity id="o16407" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 2.5–4 × 1–1.5 mm;</text>
      <biological_entity id="o16408" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 1.2 × 0.5 mm;</text>
      <biological_entity id="o16409" name="all" name_original="filaments" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.4–0.6 mm;</text>
      <biological_entity id="o16410" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 1–1.2 mm.</text>
      <biological_entity id="o16411" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 1.5–2 × 2–2.5 mm.</text>
      <biological_entity id="o16412" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–1.5 mm.</text>
      <biological_entity id="o16413" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Whipplea modesta grows on the west side of the Cascade and Coast ranges. In Washington, it is known only from Clallam County.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky sites, open to sparsely forested areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="open to forested areas" modifier="sparsely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(30–)400–1300(–1700) m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="400" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1700" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>