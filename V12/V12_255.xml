<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="illustration_page">421</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">VISCACEAE</taxon_name>
    <taxon_name authority="M. Bieberstein" date="1819" rank="genus">ARCEUTHOBIUM</taxon_name>
    <taxon_name authority="Nuttall ex Engelmann" date="1850" rank="species">americanum</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6. 214. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species americanum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250063317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Razoumofskya</taxon_name>
    <taxon_name authority="(Nuttall ex Engelmann) Kuntze" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus razoumofskya;species americana</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Lodgepole pine dwarf mistletoe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually forming systemic witches brooms, sometimes nonsystemic witches brooms in secondary hosts.</text>
      <biological_entity id="o8405" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8406" name="witch" name_original="witches" src="d0_s0" type="structure" />
      <biological_entity constraint="secondary" id="o8407" name="host" name_original="hosts" src="d0_s0" type="structure" />
      <relation from="o8405" id="r691" name="forming systemic" negation="false" src="d0_s0" to="o8406" />
      <relation from="o8405" id="r692" modifier="sometimes" name="in" negation="false" src="d0_s0" to="o8407" />
    </statement>
    <statement id="d0_s1">
      <text>Stems yellowish to olive green;</text>
    </statement>
    <statement id="d0_s2">
      <text>secondary branching whorled, branches 5–9 (–30) cm, third internode 6–23 × 1–2 mm, dominant shoot 1–3 mm diam. at base.</text>
      <biological_entity id="o8408" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s1" to="olive green" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s2" value="secondary" value_original="secondary" />
      </biological_entity>
      <biological_entity id="o8409" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
      </biological_entity>
      <biological_entity id="o8410" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8411" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="23" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8412" name="shoot" name_original="shoot" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="dominant" value_original="dominant" />
        <character char_type="range_value" constraint="at base" constraintid="o8413" from="1" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8413" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate pedicels present.</text>
      <biological_entity id="o8414" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers radially symmetric, subglobose in bud, 2.2 mm diam.;</text>
      <biological_entity id="o8415" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character constraint="in bud" constraintid="o8416" is_modifier="false" name="shape" src="d0_s4" value="subglobose" value_original="subglobose" />
        <character name="diameter" notes="" src="d0_s4" unit="mm" value="2.2" value_original="2.2" />
      </biological_entity>
      <biological_entity id="o8416" name="bud" name_original="bud" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 3 (–4), same color as stems.</text>
      <biological_entity id="o8417" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8418" name="stem" name_original="stems" src="d0_s5" type="structure" />
      <relation from="o8417" id="r693" name="as" negation="false" src="d0_s5" to="o8418" />
    </statement>
    <statement id="d0_s6">
      <text>Berries proximally olive green, distally yellowish to reddish-brown, 3.5–4.5 × 1.5–2.5 mm.</text>
      <biological_entity id="o8419" name="berry" name_original="berries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s6" value="olive green" value_original="olive green" />
        <character char_type="range_value" from="distally yellowish" name="coloration" src="d0_s6" to="reddish-brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds ellipsoid, 2.4 × 1.1 mm, endosperm green.</text>
      <biological_entity id="o8420" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s7" unit="mm" value="2.4" value_original="2.4" />
        <character name="width" src="d0_s7" unit="mm" value="1.1" value_original="1.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 28.</text>
      <biological_entity id="o8421" name="endosperm" name_original="endosperm" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8422" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Meiosis occurs in August, with fruits maturing 16 months after pollination; seeds germinate in May.</discussion>
  <discussion>The principal hosts of Arceuthobium americanum are Pinus contorta var. latifolia in western North America, P. contorta var. murrayana in the Sierra Nevada and Cascade ranges of the western United States, and P. banksiana in western Canada. A study utilizing AFLPs (C. A. Jerome and B. A. Ford 2002) documented that the parasite exists as three genetic races that correspond to these host species. Arceuthobium americanum has the most extensive geographic range of any species of the genus, and can utilize other species as secondary hosts, including P. albicaulis, P. flexilis, P. jeffreyi, and P. ponderosa, as well as a number of rare hosts. Although young infections may be localized, A. americanum eventually forms massive systemic witches’ brooms. Interestingly, when parasitizing some secondary hosts, the brooms may become nonsystemic, possibly indicating partial breakdown of coordinated developmental pathways.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jun; fruiting Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous forests, especially with jack or lodgepole pine.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous forests" />
        <character name="habitat" value="jack" modifier="especially" />
        <character name="habitat" value="lodgepole" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–3400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Sask.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>