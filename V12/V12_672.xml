<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CEANOTHUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Ceanothus</taxon_name>
    <taxon_name authority="Trelease" date="1888" rank="species">impressus</taxon_name>
    <taxon_name authority="McMinn in M. van Rensselaer and H. McMinn" date="1942" rank="variety">nipomensis</taxon_name>
    <place_of_publication>
      <publication_title>Ceanothus,</publication_title>
      <place_in_publication>219, figs. 12, 13. 1942</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus ceanothus;species impressus;variety nipomensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101416</other_info_on_name>
  </taxon_identification>
  <number>22b.</number>
  <other_name type="common_name">Nipomo ceanothus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, open, not intricately branched, 1.5–3 m.</text>
      <biological_entity id="o22848" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character is_modifier="false" modifier="not intricately" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ± flat to cupped, widely elliptic to suborbiculate, 11–20 (–25) × 7–17 (–20) mm, margins thick to weakly revolute, teeth evident adaxially;</text>
      <biological_entity id="o22849" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character constraint="to margins" constraintid="o22850" is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o22850" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="widely elliptic" is_modifier="true" name="shape" src="d0_s1" to="suborbiculate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s1" to="25" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" is_modifier="true" name="length" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s1" to="20" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s1" to="17" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o22851" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s1" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>veins moderately furrowed.</text>
      <biological_entity id="o22852" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s2" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety nipomensis occurs primarily on Nipomo Mesa and the eastern San Luis Range of southern San Luis Obispo County. R. F. Hoover (1970) regarded it as derived from hybridization between var. impressus and Ceanothus oliganthus. However, there is little evidence to indicate that populations of these two taxa overlap in geographic and ecological range. We treat var. nipomensis as part of C. impressus, based on its furrowed veins and fruit morphology. Urbanization has reduced the number of known populations to a relatively few, scattered localities.</discussion>
  <discussion>Variety nipomensis is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly, open sites, chaparral, oak woodlands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" modifier="sandy or gravelly" />
        <character name="habitat" value="oak" modifier="chaparral" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>