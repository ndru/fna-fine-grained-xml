<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Roberto J. Urtecho</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">190</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 980. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 421. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia</taxon_hierarchy>
    <other_info_on_name type="etymology">For Hieronymus Bock, 1498–1553, German botanist; from Greek tragos, goat, bock being the German equivalent</other_info_on_name>
    <other_info_on_name type="fna_id">133273</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Noseburns</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, subshrubs, or vines, perennial, monoecious [dioecious];</text>
      <biological_entity id="o24029" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairy, hairs unbranched, always some stinging (sometimes inconspicuous except on ovaries and capsules), sometimes glandular;</text>
      <biological_entity id="o24030" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" notes="" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o24032" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="always" name="toxicity" src="d0_s1" value="stinging" value_original="stinging" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex absent.</text>
      <biological_entity id="o24033" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, alternate, simple (usually 3-foliolate in T. laciniata);</text>
      <biological_entity id="o24034" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, persistent;</text>
      <biological_entity id="o24035" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present, glands absent;</text>
      <biological_entity id="o24036" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24037" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade usually unlobed, sometimes lobed basally (sometimes deeply 3-lobed in T. laciniata) [palmately lobed], margins serrate, crenate, dentate, or entire, laminar glands absent;</text>
      <biological_entity id="o24038" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="sometimes; basally" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o24039" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>venation pinnate or palmate at base, pinnate distally [palmate].</text>
      <biological_entity constraint="laminar" id="o24040" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pinnate" value_original="pinnate" />
        <character constraint="at base" constraintid="o24041" is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity id="o24041" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences bisexual (pistillate flowers proximal, staminate distal) [unisexual], axillary, terminal, or leaf-opposed, racemes [rarely with single pistillate branch];</text>
      <biological_entity id="o24042" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s8" value="leaf-opposed" value_original="leaf-opposed" />
        <character is_modifier="false" name="position" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s8" value="leaf-opposed" value_original="leaf-opposed" />
      </biological_entity>
      <biological_entity id="o24043" name="raceme" name_original="racemes" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>glands subtending each bract 0.</text>
      <biological_entity id="o24044" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o24045" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels present, staminate with persistent base, pistillate elongated in fruit.</text>
      <biological_entity id="o24046" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character constraint="with base" constraintid="o24047" is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character constraint="in fruit" constraintid="o24048" is_modifier="false" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o24047" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o24048" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals 3–5, usually green, sometimes reddish green, not petaloid, valvate, distinct;</text>
      <biological_entity id="o24049" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24050" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 0;</text>
      <biological_entity id="o24051" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24052" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary absent [present];</text>
      <biological_entity id="o24053" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24054" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 2–6 (–10) [–25], distinct or connate basally (connate 1/2 length in T. nigricans);</text>
      <biological_entity id="o24055" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24056" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistillode present [absent].</text>
      <biological_entity id="o24057" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24058" name="pistillode" name_original="pistillode" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: sepals 6, usually green, sometimes reddish green, not petaloid, connate basally;</text>
      <biological_entity id="o24059" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24060" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 0;</text>
      <biological_entity id="o24061" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24062" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>nectary absent;</text>
      <biological_entity id="o24063" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24064" name="nectary" name_original="nectary" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pistil 3-carpellate;</text>
      <biological_entity id="o24065" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24066" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 3, connate basally to 1/2 [most of] length, unbranched.</text>
      <biological_entity id="o24067" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24068" name="style" name_original="styles" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="length" src="d0_s20" to="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, usually 3 carpels maturing, except often 1 maturing in T. brevispica.</text>
      <biological_entity constraint="fruits" id="o24069" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character modifier="usually" name="quantity" src="d0_s21" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o24070" name="carpel" name_original="carpels" src="d0_s21" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s21" value="maturing" value_original="maturing" />
        <character modifier="often" name="quantity" src="d0_s21" value="1" value_original="1" />
        <character constraint="in t" is_modifier="false" name="life_cycle" src="d0_s21" value="maturing" value_original="maturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds globose to ovoid;</text>
      <biological_entity id="o24071" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s22" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>caruncle absent.</text>
      <biological_entity id="o24072" name="caruncle" name_original="caruncle" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 175 (15 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, West Indies, Central America, South America, Asia, Africa, Australia; primarily tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="primarily tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tragia is a taxonomically difficult genus that is characterized by stinging hairs. Although many species of Tragia are twining vines, most species in the flora area are subshrubs or herbs. Some species are used medicinally for their anti-inflammatory, analgesic, vermifugic, and antihyperglycemic properties. Two sections are represented in the flora area: Tragia and Leptobotrys (Baillon) Müller Arg. Molecular phylogenetic analysis (W. M. Cardinal-McTeague and L. J. Gillespie, unpubl.) suggests that Tragia is polyphyletic and that sect. Leptobotrys (T. smallii, T. urens) should be segregated as a distinct genus; these results are supported by pollen morphology (L. J. Gillespie 1994). Tragia volubilis Linnaeus was collected from Florida once (1842–1848, F. Rugel, US), but has not been collected there since and is presumed extirpated in the flora area. This species is widespread in the Caribbean and Latin America.</discussion>
  <references>
    <reference>Miller, K. I. 1964. A Taxonomic Study of the Species of Tragia in the United States. Ph.D. thesis. Purdue University.</reference>
    <reference>Miller, K. I. and G. L. Webster. 1967. A preliminary revision of Tragia (Euphorbiaceae) in the United States. Rhodora 69: 241–305.</reference>
    <reference>Urtecho, R. J. 1996. A Taxonomic Study of the Mexican Species of Tragia (Euphorbiaceae). Ph.D. dissertation. University of California, Davis.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence glands stipitate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting pedicels 3–7 mm; leaf blades narrowly ovate to lanceolate, bases shallowly cordate to truncate; persistent base of staminate pedicels 0.3–0.7 mm; stigmas smooth to undulate; Texas.</description>
      <determination>5. Tragia glanduligera</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting pedicels 7–11 mm; leaf blades ovate to triangular-ovate, bases deeply cordate; persistent base of staminate pedicels 1.8–2 mm; stigmas undulate to subpapillate; Arizona.</description>
      <determination>6. Tragia jonesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence glands absent or sessile (T. nepetifolia and T. ramosa).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 11–13 mm wide; leaf blades 4.5–10(–13) cm, base cordate; petioles 15–85 mm; stamens 3.</description>
      <determination>4. Tragia cordata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 4–11 mm wide (9–13 mm in T. smallii); leaf blades 1–8(–10) cm, base acute, obtuse, subcuneate, cuneate, truncate, subcordate, cordate, subhastate, or hastate; petioles 0–38(–41) mm; stamens 2–6(–10).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 2.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades orbiculate to elliptic, margins serrate to crenate.</description>
      <determination>13. Tragia smallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades usually oblanceolate to linear, sometimes elliptic, margins entire or irregularly sinuate.</description>
      <determination>14. Tragia urens</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 3–6(–10).</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves usually 3-foliolate, sometimes 3-lobed nearly to base.</description>
      <determination>7. Tragia laciniata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves simple, usually unlobed, sometimes lobed basally.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems purple-green to reddish black or brownish red to maroon-green; staminate flowers 2–5 per raceme.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades acicular to narrowly oblong, 1–6 cm, margins usually entire, sometimes serrulate, petioles 0.5–2 mm; stamens connate basally; capsules 4–5 mm wide.</description>
      <determination>8. Tragia leptophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades oblong to oblanceolate, 3–7 cm, margins coarsely serrate, petioles 1–5 mm; stamens connate 1/2 length; capsules 6–7 mm wide.</description>
      <determination>10. Tragia nigricans</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems green, whitish green, reddish green, dark green, or gray-green; staminate flowers 2–80 per raceme.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stigmas papillate.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blade margins coarsely dentate to coarsely serrate; staminate sepals reddish green; Arizona, Colorado, New Mexico.</description>
      <determination>9. Tragia nepetifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blade margins serrate; staminate sepals green, sometimes red-tinged; c, e United States.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Staminate flowers 15–80 per raceme, distally clustered; persistent base of staminate pedicel 0.3–0.6 mm, shorter than subtending bract; pistillate sepals 1.8–5 mm.</description>
      <determination>2. Tragia betonicifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Staminate flowers 11–40 per raceme, evenly distributed; persistent base of staminate pedicel 1–1.8 mm, longer than subtending bract; pistillate sepals 1.3–2.3 mm.</description>
      <determination>15. Tragia urticifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stigmas smooth, undulate, or subpapillate.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades suborbiculate to ovate; Florida.</description>
      <determination>12. Tragia saxicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades linear-lanceolate, lanceolate, ovate, triangular, subhastate or cordate; sc, sw United States.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Staminate flowers 2–8 per racemes; capsules with often 1 carpel maturing; stems decumbent, twining, or erect, apices usually flexuous.</description>
      <determination>3. Tragia brevispica</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Staminate flowers 2–20 per raceme; capsules with usually 3 carpels maturing; stems erect to trailing, apices flexuous or not.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades usually triangular to subhastate, sometimes ovate, base cordate, hastate, or truncate; stems gray-green, apices often flexuous; stigmas undulate to subpapillate; styles connate to 1/3 length, short-exserted; stamens 3–4.</description>
      <determination>1. Tragia amblyodonta</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades linear-lanceolate to narrowly ovate, base truncate to weakly cordate; stems dark green to light green, apices rarely flexuous; stigmas smooth to undulate; styles connate 1/3–1/2 length, long-exserted; stamens 3–6(–10).</description>
      <determination>11. Tragia ramosa</determination>
    </key_statement>
  </key>
</bio:treatment>