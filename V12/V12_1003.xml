<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Small" date="unknown" rank="family">PICRODENDRACEAE</taxon_name>
    <taxon_name authority="Engelmann ex Parry" date="1885" rank="genus">TETRACOCCUS</taxon_name>
    <taxon_name authority="Coville &amp; Gilman" date="1936" rank="species">ilicifolius</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>26: 531. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family picrodendraceae;genus tetracoccus;species ilicifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101662</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Holly-leaf four-pod spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs to 1.5 m.</text>
      <biological_entity id="o4549" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves opposite, not clustered on short-shoots;</text>
      <biological_entity id="o4550" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character constraint="on short-shoots" constraintid="o4551" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o4551" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade ovate to broadly elliptic, 15–30 × 7–20 mm, base obtuse, margins prominently serrate-dentate, apex acute to obtuse.</text>
      <biological_entity id="o4552" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="broadly elliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4553" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4554" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_shape" src="d0_s2" value="serrate-dentate" value_original="serrate-dentate" />
      </biological_entity>
      <biological_entity id="o4555" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: staminate congested paniclelike thyrses, 15–35 mm;</text>
      <biological_entity id="o4556" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4557" name="thyrse" name_original="thyrses" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="congested" value_original="congested" />
        <character is_modifier="true" name="shape" src="d0_s3" value="paniclelike" value_original="paniclelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate flowers solitary.</text>
      <biological_entity id="o4558" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o4559" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels: staminate essentially absent;</text>
      <biological_entity id="o4560" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pistillate 8–15 mm, tomentose.</text>
      <biological_entity id="o4561" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: sepals 7–9;</text>
      <biological_entity id="o4562" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4563" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 7–9, filaments villous basally.</text>
      <biological_entity id="o4564" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4565" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity id="o4566" name="all" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers: sepals 8, in 2 series, greenish, 2.5–4 mm, sparsely tomentose abaxially, densely tomentose adaxially;</text>
      <biological_entity id="o4567" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4568" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="8" value_original="8" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o4569" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4568" id="r413" name="in" negation="false" src="d0_s9" to="o4569" />
    </statement>
    <statement id="d0_s10">
      <text>pistil usually 4-carpellate, 3 mm, tomentose.</text>
      <biological_entity id="o4570" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4571" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="4-carpellate" value_original="4-carpellate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds (1–) 2 per locule, glossy brownish red, elliptic-oblong, 4–5 mm, usually with one flattened radial surface, smooth;</text>
      <biological_entity id="o4572" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s11" to="2" to_inclusive="false" />
        <character constraint="per locule" constraintid="o4573" name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s11" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-oblong" value_original="elliptic-oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o4573" name="locule" name_original="locule" src="d0_s11" type="structure" />
      <biological_entity id="o4574" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="radial" value_original="radial" />
      </biological_entity>
      <relation from="o4572" id="r414" modifier="usually" name="with" negation="false" src="d0_s11" to="o4574" />
    </statement>
    <statement id="d0_s12">
      <text>caruncle present.</text>
      <biological_entity id="o4575" name="caruncle" name_original="caruncle" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tetracoccus ilicifolius is restricted to the mountains flanking Death Valley.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub on limestone outcrops.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" constraint="on limestone" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>