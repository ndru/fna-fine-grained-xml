<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">VITACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">VITIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 202. 1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 95. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family vitaceae;genus vitis</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, vine</other_info_on_name>
    <other_info_on_name type="fna_id">134649</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(Planchon) Small" date="unknown" rank="genus">Muscadinia</taxon_name>
    <taxon_hierarchy>genus muscadinia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Grape</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Lianas, climbing by tendrils, sprawling, or occasionally shrubby, functionally dioecious (synoecious in V. vinifera).</text>
      <biological_entity id="o23340" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="by tendrils" constraintid="o23341" is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="occasionally" name="growth_form" src="d0_s0" value="shrubby" value_original="shrubby" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" modifier="occasionally" name="growth_form" src="d0_s0" value="shrubby" value_original="shrubby" />
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="liana" />
      </biological_entity>
      <biological_entity id="o23341" name="tendril" name_original="tendrils" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Branches: bark exfoliating (adherent in V. rotundifolia);</text>
      <biological_entity id="o23342" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o23343" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="relief" src="d0_s1" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pith brown, interrupted by nodal diaphragms (continuous through nodes in V. rotundifolia);</text>
      <biological_entity id="o23344" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o23345" name="pith" name_original="pith" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character constraint="by nodal diaphragms" constraintid="o23346" is_modifier="false" name="architecture" src="d0_s2" value="interrupted" value_original="interrupted" />
      </biological_entity>
      <biological_entity constraint="nodal" id="o23346" name="diaphragm" name_original="diaphragms" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>tendrils 2–3-branched (unbranched in V. rotundifolia), rarely absent, without adhesive discs.</text>
      <biological_entity id="o23347" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o23348" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="2-3-branched" value_original="2-3-branched" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adhesive" id="o23349" name="disc" name_original="discs" src="d0_s3" type="structure" />
      <relation from="o23348" id="r1921" name="without" negation="false" src="d0_s3" to="o23349" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves simple.</text>
      <biological_entity id="o23350" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences functionally unisexual (bisexual in V. vinifera), leaf-opposed, thyrses.</text>
      <biological_entity id="o23351" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="position" src="d0_s5" value="leaf-opposed" value_original="leaf-opposed" />
      </biological_entity>
      <biological_entity id="o23352" name="thyrse" name_original="thyrses" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers functionally unisexual (bisexual in V. vinifera);</text>
      <biological_entity id="o23353" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx a minute rim, entire or 5-toothed;</text>
      <biological_entity id="o23354" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="5-toothed" value_original="5-toothed" />
      </biological_entity>
      <biological_entity id="o23355" name="rim" name_original="rim" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (3–) 5 (–9), connate distally, forming calyptra;</text>
      <biological_entity id="o23356" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s8" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="9" />
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" modifier="distally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o23357" name="calyptra" name_original="calyptra" src="d0_s8" type="structure" />
      <relation from="o23356" id="r1922" name="forming" negation="false" src="d0_s8" to="o23357" />
    </statement>
    <statement id="d0_s9">
      <text>nectary free, (3–) 5 (–9) glands alternating with stamens;</text>
      <biological_entity id="o23358" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="9" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o23359" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character constraint="with stamens" constraintid="o23360" is_modifier="false" name="arrangement" src="d0_s9" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o23360" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens usually (3–) 5 (–9), sometimes 0 in pistillate flowers;</text>
      <biological_entity id="o23361" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="9" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character constraint="in flowers" constraintid="o23362" modifier="sometimes" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23362" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style conic, short.</text>
      <biological_entity id="o23363" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Berries purple or black.</text>
      <biological_entity id="o23364" name="berry" name_original="berries" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1–4 per fruit.</text>
      <biological_entity id="o23366" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>x = 10.</text>
      <biological_entity id="o23365" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per fruit" constraintid="o23366" from="1" name="quantity" src="d0_s13" to="4" />
      </biological_entity>
      <biological_entity constraint="x" id="o23367" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 70 (19, including 3 hybrids, in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, n South America, Eurasia; introduced nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vitis is nearly restricted to temperate regions of the northern hemisphere, with one extremely variable species (V. tiliifolia Humboldt &amp; Bonpland ex Schultes) extending into northern South America.</discussion>
  <discussion>The North American species of Vitis are of considerable economic importance and have played a significant part in commercial viticulture over the last century. The introduction of native North American species into France in the mid 1800s led to the introduction of grape phylloxera, Daktulosphaira vitifoliae, an insect to which the grape of commerce (V. vinifera) is susceptible. Many European vineyards were virtually destroyed by the 1860s. The reconstruction of the European vineyards was made possible by using native North American species, many of which are resistant to grape phylloxera, as rootstocks for and in hybridizations with V. vinifera. Native North American species of Vitis have also played a major role in establishing viticulture as an industry in North America, particularly in areas other than California and Oregon, such as Florida, New York, North Carolina, Ontario, Texas, and Virginia.</discussion>
  <discussion>Two subgenera of Vitis commonly have been recognized. Subgenus Vitis, which includes the majority of species, is widely distributed in the Northern Hemisphere; subg. Muscadinia, with two species, is restricted to the southeastern United States, the West Indies, and Mexico (G. K. Brizicky 1965; Wen J. 2007). J. K. Small (1903) elevated subg. Muscadinia to generic rank, a treatment followed by some authors, for example A. S. Weakley et al. (2012). However, a recent phylogenetic study of the Vitis-Ampelocissus clade by Liu X. Q. et al. (2015) showed that the Central American Ampelocissus erdvendbergianus Planchon is sister to Vitis and that subg. Muscadinia and subg. Vitis are sister groups. To maintain the monophyly of Vitis and Ampelocissus, A. erdvendbergianus needs to be transferred to Vitis. Recognizing Muscadinia as a distinct genus would require description of a monospecific new genus for A. erdvendbergianus; it makes better sense to maintain a broadly circumscribed Vitis.</discussion>
  <discussion>In the key and descriptions that follow, nodal diaphragm thickness is in the current year's growth, leaf blade pubescence is on fully mature leaves unless otherwise noted, and berry diameter is of 3–4-seeded berries.</discussion>
  <references>
    <reference>Bailey, L. H. 1934. The species of grapes peculiar to North America. Gentes Herbarum 3: 154–244.</reference>
    <reference>Comeaux, B. L., W. B. Nesbitt, and P. R. Fantz. 1987. Taxonomy of the native grapes of North Carolina. Castanea 52: 197–215.</reference>
    <reference>Moore, M. O. 1987. A study of selected Vitis (Vitaceae) taxa in the southeastern United States. Rhodora 89: 75–91.</reference>
    <reference>Moore, M. O. 1991. Classification and systematics of eastern North American Vitis L. (Vitaceae) north of Mexico. Sida 14: 339–367.</reference>
    <reference>Munson, T. V. 1909. Foundations of American Grape Culture. New York.</reference>
    <reference>Tröndle, D. et al. 2010. Molecular phylogeny of the genus Vitis (Vitaceae) based on plastid markers. Amer. J. Bot. 97: 1168–1178.</reference>
    <reference>Zecca G. et al. 2012. The timing and the mode of evolution of wild grapes. Molec. Phylogen. Evol. 62: 736–747.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tendrils unbranched; bark adherent; lenticels prominent; pith continuous through nodes [1a. subg. Muscadinia].</description>
      <determination>1. Vitis rotundifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tendrils branched or absent; bark exfoliating; lenticels inconspicuous or absent; pith interrupted by nodal diaphragms [1b. subg. Vitis].</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers bisexual; berries oblong to ellipsoid, skin adhering to pulp.</description>
      <determination>2. Vitis vinifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers functionally unisexual; berries globose, skin separating from pulp.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade abaxial surface glaucous (sometimes obscured by hairs).</description>
      <determination>3. Vitis aestivalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade abaxial surface not glaucous (concealed by hairs in V. labrusca).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Tendrils or inflorescences present at 3+ consecutive nodes or almost all nodes.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade abaxial surface densely and persistently arachnoid, concealed (except sometimes veins) by hairs; nodal diaphragms 0.5–2.5 mm thick; tendrils at almost all nodes.</description>
      <determination>4. Vitis labrusca</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade abaxial surface ± densely arachnoid when young, sparsely arachnoid when mature, visible through hairs; nodal diaphragms 0.3–1.1 mm thick; tendrils usually not at all nodes.</description>
      <determination>5. Vitis ×novae-angliae</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Tendrils or inflorescences present at only 2 consecutive nodes.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade abaxial surface densely tomentose, concealed (except sometimes veins) by hairs; berries 12+ mm diam.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stipules 1.5–4 mm; nodal diaphragms 1.5–3 mm thick; Alabama, Arkansas, Louisiana, Oklahoma, Texas.</description>
      <determination>6. Vitis mustangensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stipules to 1 mm; nodal diaphragms 2.5–6 mm thick; Florida.</description>
      <determination>7. Vitis shuttleworthii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade abaxial surface usually glabrous, moderately arachnoid, or hirtellous, sometimes tomentose (California and s Oregon), visible through hairs; berries 4–12 mm diam. (except 12+ mm in V. ×doaniana and V. ×champinii).</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades reniform, abaxial surface usually glabrous, sometimes sparsely hirtellous on veins and in vein axils; tendrils absent or only at distalmost nodes.</description>
      <determination>8. Vitis rupestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades usually cordate to cordate ovate, sometimes orbiculate or nearly reniform, abaxial surface glabrous or hairy; tendrils along length of branchlets.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Nodal diaphragms to 0.5(–1) mm thick; branchlet growing tips enveloped by unfolding leaves.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants low to moderately high climbing, much branched; tendrils soon deciduous if not attached to support; branchlets arachnoid or glabrate, growing tips sparsely to densely hairy; inflorescences 3–7(–9) cm.</description>
      <determination>9. Vitis acerifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Plants usually moderate to high climbing, sometimes sprawling, sparsely branched; tendrils persistent; branchlets glabrous or sparsely hirtellous, growing tips glabrous or sparsely hairy; inflorescences (4–)9–12 cm.</description>
      <determination>10. Vitis riparia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Nodal diaphragms 1–4 mm thick; branchlet growing tips not enveloped by unfolding leaves.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Berries 12+ mm diam.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade abaxial surface moderately to densely arachnoid, hirtellous on veins; berries glaucous.</description>
      <determination>11. Vitis ×doaniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade abaxial surface sparsely arachnoid to glabrate, not hirtellous; berries usually not, sometimes very slightly, glaucous.</description>
      <determination>12. Vitis ×champinii</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Berries 4–12 mm diam.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blade abaxial surface sparsely to densely tomentose; California, s Oregon.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Berries moderately to heavily glaucous, 8–10 mm diam.; branchlet tomentum thinning in age; nodal diaphragms 3–4 mm thick.</description>
      <determination>13. Vitis californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Berries slightly or not glaucous, 4–6 mm diam.; branchlet tomentum usually persistent; nodal diaphragms 1.5–3 mm thick.</description>
      <determination>14. Vitis girdiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blade abaxial surface glabrous or sparsely to densely arachnoid or hirtellous; much of United States, but not California or Oregon.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants sprawling to low climbing, shrubby, much branched; tendrils soon deciduous if not attached to means of support; Arizona, Nevada, New Mexico, trans-Pecos Texas, Utah.</description>
      <determination>15. Vitis arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants usually moderate to high climbing, sometimes ± shrubby and sprawling when without support, sparsely branched; tendrils persistent; e United States, including Texas, not in trans-Pecos region.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Branchlets ± angled, densely hirtellous and/or sparsely to densely arachnoid, to glabrate; berries 4–8 mm diam.; nodes sometimes red-banded.</description>
      <determination>16. Vitis cinerea</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Branchlets terete or subterete, glabrous or sparsely arachnoid; berries 8–12 mm diam.; nodes not red-banded.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Nodal diaphragms 2.5–4 mm thick; leaf blades usually deeply lobed, apices long acuminate; branchlets uniformly red, purplish red, or chestnut.</description>
      <determination>17. Vitis palmata</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Nodal diaphragms 1–2.5 mm thick; leaf blades unlobed or shallowly lobed, sometimes deeply lobed on ground shoots, apices acute to short acuminate; branchlets gray to green or brown, if purplish only on one side.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Berries usually with lenticels; inflorescences 3–7 cm; branchlet growing tips sparsely to densely hairy; leaf blades 5–8(–10) cm; branchlets sparsely arachnoid or glabrous.</description>
      <determination>18. Vitis monticola</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Berries without lenticels; inflorescences 9–19 cm; branchlet growing tips glabrous to sparsely hairy; leaf blades (5–)9–18 cm; branchlets glabrous.</description>
      <determination>19. Vitis vulpina</determination>
    </key_statement>
  </key>
</bio:treatment>