<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Roeper in J. É. Duby" date="1828" rank="section">Anisophyllum</taxon_name>
    <taxon_name authority="Engelmann ex Boissier in A. P. de Candolle and A. L. P. P. de Candolle" date="1862" rank="species">astyla</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>15(2): 40. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section anisophyllum;species astyla</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101553</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaesyce</taxon_name>
    <taxon_name authority="(Engelmann ex Boissier) Millspaugh" date="unknown" rank="species">astyla</taxon_name>
    <taxon_hierarchy>genus chamaesyce;species astyla</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Alkali or Pecos spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with woody or fibrous-fleshy taproot, 5–12 mm thick.</text>
      <biological_entity id="o21776" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="5" from_unit="mm" name="thickness" notes="" src="d0_s0" to="12" to_unit="mm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o21777" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous-fleshy" value_original="fibrous-fleshy" />
      </biological_entity>
      <relation from="o21776" id="r1784" name="with" negation="false" src="d0_s0" to="o21777" />
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent, ascending, or erect, few-to-many emerging from woody crown, 5–25 (–50) cm, glabrous.</text>
      <biological_entity id="o21778" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="few" name="quantity" src="d0_s1" to="many" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21779" name="crown" name_original="crown" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o21778" id="r1785" name="emerging from" negation="false" src="d0_s1" to="o21779" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o21780" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules connate into deltate scale, 0.2–0.5 mm, minutely lacerate at apex, glabrous;</text>
      <biological_entity id="o21781" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character constraint="into scale" constraintid="o21782" is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="0.5" to_unit="mm" />
        <character constraint="at apex" constraintid="o21783" is_modifier="false" modifier="minutely" name="shape" src="d0_s3" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21782" name="scale" name_original="scale" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o21783" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–0.2 (–0.3) mm, glabrous;</text>
      <biological_entity id="o21784" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade orbiculate-reniform to acute-cordate, 2–5 (–8) × 2–5 (–6) mm, base ± asymmetric, cordate to auriculate, sometimes clasping stem, margins entire, apex narrowly acute, surfaces glabrous;</text>
      <biological_entity id="o21785" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="orbiculate-reniform" name="shape" src="d0_s5" to="acute-cordate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21786" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="auriculate" />
      </biological_entity>
      <biological_entity id="o21787" name="stem" name_original="stem" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o21788" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21789" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2-veined or 3-veined from base, but usually only midvein conspicuous.</text>
      <biological_entity id="o21790" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-veined" value_original="2-veined" />
        <character constraint="from base" constraintid="o21791" is_modifier="false" name="architecture" src="d0_s6" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o21791" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o21792" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia solitary at distal nodes;</text>
      <biological_entity id="o21793" name="cyathium" name_original="cyathia" src="d0_s7" type="structure">
        <character constraint="at distal nodes" constraintid="o21794" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21794" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 0.3–1 (–1.5) mm.</text>
      <biological_entity id="o21795" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre broadly campanulate, 0.8–1.4 × 0.9–1.4 mm, glabrous;</text>
      <biological_entity id="o21796" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s9" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4, yellow-green to brownish, oblong, 0.2–0.3 × 0.5–0.7 mm;</text>
      <biological_entity id="o21797" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s10" to="brownish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s10" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages white, flabellate to oblong, 0.1–0.2 (–0.5) × 0.4–0.8 mm, distal margin entire or dentate-crenate.</text>
      <biological_entity id="o21798" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="flabellate" name="shape" src="d0_s11" to="oblong" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s11" to="0.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21799" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="dentate-crenate" value_original="dentate-crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 22–26.</text>
      <biological_entity id="o21800" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="22" name="quantity" src="d0_s12" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o21801" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21802" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.3–0.4 mm, unbranched, thickened-clavate.</text>
      <biological_entity id="o21803" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21804" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s14" value="thickened-clavate" value_original="thickened-clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid and broadly triangular, 1.5–1.9 (–2.5) × 1.4–1.6 (–2.2) mm, glabrous;</text>
      <biological_entity id="o21805" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s15" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.2–1.8 mm.</text>
      <biological_entity id="o21806" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds white, oblong, 4-angled in cross-section, adaxial faces slightly concave, with long raphe between, 1.5–1.8 × 0.7–1 mm, markedly foveolate, with irregular to ± parallel or anastomosing ridges.</text>
      <biological_entity id="o21807" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character constraint="in cross-section" constraintid="o21808" is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
      </biological_entity>
      <biological_entity id="o21808" name="cross-section" name_original="cross-section" src="d0_s17" type="structure" />
      <biological_entity id="o21810" name="raphe" name_original="raphe" src="d0_s17" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s17" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o21811" name="ridge" name_original="ridges" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
        <character is_modifier="true" modifier="more or less" name="arrangement" src="d0_s17" value="parallel" value_original="parallel" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <relation from="o21809" id="r1786" name="with" negation="false" src="d0_s17" to="o21810" />
      <relation from="o21809" id="r1787" name="with" negation="false" src="d0_s17" to="o21811" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 28.</text>
      <biological_entity constraint="adaxial" id="o21809" name="face" name_original="faces" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" notes="" src="d0_s17" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" notes="" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="markedly" name="relief" src="d0_s17" value="foveolate" value_original="foveolate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21812" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia astyla is a specialist on halophytic, alkaline soils and is known in the flora area only in part of Pecos County. The species is closely related to E. jejuna but differs in its sessile or sub-sessile leaves with a cordate-auriculate base and involucral gland appendages that are not deeply lobed or cleft.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late spring–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="early fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert, grasslands, limestone substrates, usually on very saline or alkaline soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="substrates" modifier="limestone" />
        <character name="habitat" value="saline" modifier="usually on very" />
        <character name="habitat" value="alkaline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1100 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>