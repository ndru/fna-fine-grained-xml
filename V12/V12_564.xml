<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="(Kuntze) B. W. van Ee &amp; P. E. Berry" date="2010" rank="species">heptalon</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>35: 159. 2010</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species heptalon</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101969</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxydectes</taxon_name>
    <taxon_name authority="Kuntze" date="1891" rank="species">heptalon</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>2: 610. 1891</place_in_publication>
      <other_info_on_pub>based on Croton berlandieri Müller Arg., Linnaea 34: 141. 1865, not Torrey 1859</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus oxydectes;species heptalon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="(A. M. Ferguson) Croizat" date="unknown" rank="species">albinoides</taxon_name>
    <taxon_hierarchy>genus c.;species albinoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">capitatus</taxon_name>
    <taxon_name authority="(A. M. Ferguson) Shinners" date="unknown" rank="variety">albinoides</taxon_name>
    <taxon_hierarchy>genus c.;species capitatus;variety albinoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="A. M. Ferguson" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="A. M. Ferguson" date="unknown" rank="variety">albinoides</taxon_name>
    <taxon_hierarchy>genus c.;species engelmannii;variety albinoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="J. M. Coulter" date="unknown" rank="species">muelleri</taxon_name>
    <taxon_name authority="(A. M. Ferguson) Croizat" date="unknown" rank="variety">albinoides</taxon_name>
    <taxon_hierarchy>genus c.;species muelleri;variety albinoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heptallon</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">aromaticum</taxon_name>
    <taxon_hierarchy>genus heptallon;species aromaticum</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Woolly croton</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, 5–15 dm, monoecious;</text>
      <biological_entity id="o13087" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems, leaves, and buds whitish-hairy when young, becoming glabrate.</text>
      <biological_entity id="o13088" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o13089" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13090" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="whitish-hairy" value_original="whitish-hairy" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems well branched distally, stellate-hairy.</text>
      <biological_entity id="o13091" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not clustered;</text>
      <biological_entity id="o13092" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules linear, 2–7 mm;</text>
      <biological_entity id="o13093" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–5 cm, glands absent at apex;</text>
      <biological_entity id="o13094" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13095" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character constraint="at apex" constraintid="o13096" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13096" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade ovatelanceolate, 3–10 × 1–5 cm, base cordate to rounded, margins entire, apex acute, abaxial surface pale green, not appearing brown-dotted, no stellate hairs with brown centers, adaxial surface darker green, both stellate-hairy.</text>
      <biological_entity id="o13097" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13098" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s6" to="rounded" />
      </biological_entity>
      <biological_entity id="o13099" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13100" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13101" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o13102" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="brown-dotted" value_original="brown-dotted" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="no" value_original="no" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s6" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o13103" name="center" name_original="centers" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13104" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="darker green" value_original="darker green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o13101" id="r1098" name="appearing" negation="true" src="d0_s6" to="o13102" />
      <relation from="o13101" id="r1099" name="with" negation="true" src="d0_s6" to="o13103" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences bisexual, racemes, 2–4 cm, staminate flowers 3–10, pistillate flowers 4–8.</text>
      <biological_entity id="o13105" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o13106" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13107" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <biological_entity id="o13108" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels: staminate 2–4 mm, pistillate 1–2 mm.</text>
      <biological_entity id="o13109" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate flowers: sepals (4–) 5, 1–2 mm, abaxial surface stellate-hairy;</text>
      <biological_entity id="o13110" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o13111" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13112" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, linear-oblong-lanceolate, 1–1.5 mm, abaxial surface stellate-hairy;</text>
      <biological_entity id="o13113" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o13114" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong-lanceolate" value_original="linear-oblong-lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13115" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 9–12.</text>
      <biological_entity id="o13116" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o13117" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: sepals 7–8, subequal, 3–6 mm, margins entire, apex straight to slightly incurved, abaxial surface whitish appressed-tomentose;</text>
      <biological_entity id="o13118" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13119" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s12" to="8" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13120" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13121" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s12" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13122" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="appressed-tomentose" value_original="appressed-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals 0;</text>
      <biological_entity id="o13123" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13124" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 3-locular;</text>
      <biological_entity id="o13125" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13126" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, 3–4 mm, 4-fid, terminal segments 12.</text>
      <biological_entity id="o13127" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13128" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="4-fid" value_original="4-fid" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13129" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules 6–8 × 6–7 mm, smooth;</text>
      <biological_entity id="o13130" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s16" to="8" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>columella tipped with 3-pronged grappling hooklike appendage.</text>
      <biological_entity id="o13131" name="columella" name_original="columella" src="d0_s17" type="structure">
        <character constraint="with appendage" constraintid="o13132" is_modifier="false" name="architecture" src="d0_s17" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o13132" name="appendage" name_original="appendage" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="hooklike" value_original="hooklike" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 4–5 × 2–2.5 mm, shiny.</text>
      <biological_entity id="o13133" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shiny" value_original="shiny" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Morphological differences among Croton heptalon and its multifid-styled relatives in sect. Heptallon, especially C. capitatus and C. lindheimeri, can be quite subtle. In general, C. heptalon can be distinguished from C. capitatus by its more elongate pistillate part of the inflorescence, non-recurving sepal tips in the pistillate flowers, and more cordate leaf bases on larger basal leaves. Whitish pubescence on its young growth and styles that branch once into four terminal segments distinguish C. heptalon from C. lindheimeri.</discussion>
  <discussion>Croton muelleri J. M. Coulter, which is an illegitimate name, pertains here.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Beaches, coastal dunes, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beaches" />
        <character name="habitat" value="coastal dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; e Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="e Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>