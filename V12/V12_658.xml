<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">366</other_info_on_meta>
    <other_info_on_meta type="illustration_page">364</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">CHRYSOBALANACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CHRYSOBALANUS</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">icaco</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">icaco</taxon_name>
    <taxon_hierarchy>family chrysobalanaceae;genus chrysobalanus;species icaco;subspecies icaco</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101571</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysobalanus</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">interior</taxon_name>
    <taxon_hierarchy>genus chrysobalanus;species interior</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="G. Meyer" date="unknown" rank="species">pellocarpus</taxon_name>
    <taxon_hierarchy>genus c.;species pellocarpus</taxon_hierarchy>
  </taxon_identification>
  <number>1a.</number>
  <other_name type="common_name">Coco-plum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees 1–5 m;</text>
      <biological_entity id="o20842" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems usually arising singly.</text>
      <biological_entity id="o20844" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="arising" value_original="arising" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs reddish, glabrate, lenticels elliptic, pith tan;</text>
      <biological_entity id="o20845" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o20846" name="lenticel" name_original="lenticels" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o20847" name="pith" name_original="pith" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bark striate.</text>
      <biological_entity id="o20848" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s3" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules ovate, 0.8–2.5 mm;</text>
      <biological_entity id="o20849" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20850" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 2–3 mm;</text>
      <biological_entity id="o20851" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20852" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade broadly elliptic, broadly ovate or broadly obovate, 3.5–6 × 3–5 cm, length 1.2–1.5 times width, base cuneate, obtuse, or rounded, margins revolute, apex emarginate, rounded or obtuse, surfaces glabrescent except for scattered hairs along midvein.</text>
      <biological_entity id="o20853" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o20854" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.2-1.5" value_original="1.2-1.5" />
      </biological_entity>
      <biological_entity id="o20855" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20856" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o20857" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o20858" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character constraint="except-for hairs" constraintid="o20859" is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o20859" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o20860" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <relation from="o20859" id="r1711" name="along" negation="false" src="d0_s6" to="o20860" />
    </statement>
    <statement id="d0_s7">
      <text>Thyrses: rachis densely hairy;</text>
      <biological_entity id="o20861" name="thyrse" name_original="thyrses" src="d0_s7" type="structure" />
      <biological_entity id="o20862" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles caducous, sessile, ovate, 1.5 mm.</text>
      <biological_entity id="o20863" name="thyrse" name_original="thyrses" src="d0_s8" type="structure" />
      <biological_entity id="o20864" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium 2.5–3 mm;</text>
      <biological_entity id="o20865" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20866" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals ovate to triangular, 1 mm, both surfaces densely strigose;</text>
      <biological_entity id="o20867" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20868" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="triangular" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o20869" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, spatulate to narrowly spatulate, 3.5–4 mm, glabrous, margins erose, apex obtuse.</text>
      <biological_entity id="o20870" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20871" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s11" to="narrowly spatulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20872" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o20873" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes white, yellow-green, pink, red, dark purple, or black, (1.2–) 1.5–2 (–2.5) cm;</text>
      <biological_entity id="o20874" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>endocarp 6-ribbed with secondary ribs.</text>
      <biological_entity constraint="secondary" id="o20876" name="rib" name_original="ribs" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 22.</text>
      <biological_entity id="o20875" name="endocarp" name_original="endocarp" src="d0_s13" type="structure">
        <character constraint="with secondary ribs" constraintid="o20876" is_modifier="false" name="architecture_or_shape" src="d0_s13" value="6-ribbed" value_original="6-ribbed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20877" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chrysobalanus icaco is known in the flora area only from southern Florida. Leaf morphology varies widely and has prompted a proliferation of names for this taxon. The fruits are eaten, and their flavor varies from a taste of marshmallow to apple; they have folk medicinal value in tropical areas. Although preserved as a foodstuff and used as an ornamental, the species has little current economic value in Florida; it is being investigated for cultivation there as a tropical fruit.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, beaches, frequently calcareous (shelly) sands.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="( shelly ) sands" modifier="frequently calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; e South America; introduced in Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="e South America" establishment_means="native" />
        <character name="distribution" value="in Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>