<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">BARTONIA</taxon_name>
    <taxon_name authority="(K. H. Thorne) J. J. Schenk &amp; L. Hufford" date="2010" rank="species">lagarosa</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>57: 247. 2010</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section bartonia;species lagarosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101837</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mentzelia</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="unknown" rank="species">pumila</taxon_name>
    <taxon_name authority="K. H. Thorne" date="1986" rank="variety">lagarosa</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>46: 558, fig. 1. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mentzelia;species pumila;variety lagarosa</taxon_hierarchy>
  </taxon_identification>
  <number>34.</number>
  <other_name type="common_name">Slender-lobed blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants biennial, candelabra-form.</text>
      <biological_entity id="o1826" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, erect, straight;</text>
      <biological_entity id="o1827" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches distal, distal longest, antrorse, straight;</text>
      <biological_entity id="o1828" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hairy.</text>
      <biological_entity constraint="distal" id="o1829" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="longest" value_original="longest" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="antrorse" value_original="antrorse" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blade 11.3–103 × 4.8–20.1 mm, widest intersinus distance 1.2–5.7 mm;</text>
      <biological_entity id="o1830" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1831" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="11.3" from_unit="mm" name="length" src="d0_s4" to="103" to_unit="mm" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="width" src="d0_s4" to="20.1" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s4" to="5.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal oblanceolate to elliptic, margins pinnate to pinnatisect, lobes 8–20, slightly antrorse or perpendicular to leaf axis, 1.4–8.2 mm;</text>
      <biological_entity id="o1832" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o1833" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity id="o1834" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="pinnate" name="architecture_or_shape" src="d0_s5" to="pinnatisect" />
      </biological_entity>
      <biological_entity id="o1835" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="20" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s5" value="antrorse" value_original="antrorse" />
        <character constraint="to leaf axis" constraintid="o1836" is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="8.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o1836" name="axis" name_original="axis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>distal elliptic to lanceolate, base not clasping, margins pinnatisect, lobes 6–16, slightly antrorse or perpendicular to leaf axis, 1.6–7.5 mm;</text>
      <biological_entity id="o1837" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o1838" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o1839" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o1840" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity id="o1841" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="16" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="antrorse" value_original="antrorse" />
        <character constraint="to leaf axis" constraintid="o1842" is_modifier="false" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o1842" name="axis" name_original="axis" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface with simple grappling-hook, complex grappling-hook, and occasionally needlelike trichomes, adaxial surface with simple grappling-hook and needlelike trichomes.</text>
      <biological_entity id="o1843" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o1844" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o1845" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o1846" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="complex" value_original="complex" />
      </biological_entity>
      <biological_entity id="o1847" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="occasionally" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1848" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o1849" name="grappling-hook" name_original="grappling-hook" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o1850" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="needlelike" value_original="needlelike" />
      </biological_entity>
      <relation from="o1844" id="r156" name="with" negation="false" src="d0_s7" to="o1845" />
      <relation from="o1848" id="r157" name="with" negation="false" src="d0_s7" to="o1849" />
    </statement>
    <statement id="d0_s8">
      <text>Bracts: margins usually entire, sometimes toothed or pinnate.</text>
      <biological_entity id="o1851" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity id="o1852" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: petals golden yellow, 8.3–13 × 2.2–5.4 mm, apex acute or rounded, glabrous abaxially;</text>
      <biological_entity id="o1853" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1854" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="8.3" from_unit="mm" name="length" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s9" to="5.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1855" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens golden yellow, 5 outermost petaloid, filaments narrowly spatulate, slightly clawed, 6.5–10.7 × 1.7–4.3 mm, without anthers, second whorl with anthers;</text>
      <biological_entity id="o1856" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1857" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden yellow" value_original="golden yellow" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o1858" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o1859" name="all" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s10" to="10.7" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s10" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1860" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o1861" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity id="o1862" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <relation from="o1859" id="r158" name="without" negation="false" src="d0_s10" to="o1860" />
      <relation from="o1861" id="r159" name="with" negation="false" src="d0_s10" to="o1862" />
    </statement>
    <statement id="d0_s11">
      <text>anthers straight after dehiscence, epidermis smooth;</text>
      <biological_entity id="o1863" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1864" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o1865" name="epidermis" name_original="epidermis" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 6.1–10.2 mm.</text>
      <biological_entity id="o1866" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1867" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="6.1" from_unit="mm" name="some_measurement" src="d0_s12" to="10.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules cylindric, 12.1–21.2 × 4.9–7.6 mm, base tapering or rounded, not longitudinally ridged.</text>
      <biological_entity id="o1868" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="12.1" from_unit="mm" name="length" src="d0_s13" to="21.2" to_unit="mm" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="width" src="d0_s13" to="7.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1869" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not longitudinally" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds: coat anticlinal cell-walls wavy, papillae 29–31 per cell.</text>
      <biological_entity id="o1870" name="seed" name_original="seeds" src="d0_s14" type="structure" />
      <biological_entity id="o1871" name="coat" name_original="coat" src="d0_s14" type="structure" />
      <biological_entity id="o1872" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="anticlinal" value_original="anticlinal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o1874" name="cell" name_original="cell" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 22.</text>
      <biological_entity id="o1873" name="papilla" name_original="papillae" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o1874" from="29" name="quantity" src="d0_s14" to="31" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1875" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia lagarosa is allopatric with two of the three species most similar to it, namely M. holmgreniorum and M. filifolia, and nearly allopatric with the third, M. laciniata. Where the ranges of M. lagarosa and M. laciniata overlap in western Colorado, they can be distinguished by petal length [8.3–13 mm in M. lagarosa versus 14–23.8(–26) mm in M. laciniata], outermost stamen length (6.5–10.7 mm in M. lagarosa versus 12–20 mm in M. laciniata), and number of seed coat cell papillae (29–31 per cell in M. lagarosa versus 5–14 per cell in M. laciniata); in addition, M. lagarosa bears both simple grappling-hook and needlelike trichomes on its adaxial leaf blade surfaces, whereas leaf blades of M. laciniata bear only needlelike trichomes adaxially. In the Intermountain Flora, N. H. Holmgren et al. (2005) treated M. lagarosa as a synonym of M. multiflora, but J. J. Schenk and L. Hufford (2011) showed not only that M. lagarosa is distinct from M. multiflora, but also that the latter does not occur in the intermountain region.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sparsely vegetated hills, slopes, knolls, white ash and limestone soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hills" modifier="sparsely vegetated" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="white ash" />
        <character name="habitat" value="limestone soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>