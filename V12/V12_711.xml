<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">LOASACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MENTZELIA</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Bentham &amp; Hooker f." date="1867" rank="section">TRACHYPHYTUM</taxon_name>
    <taxon_name authority="Greene" date="1891" rank="species">nitens</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.,</publication_title>
      <place_in_publication>234. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family loasaceae;genus mentzelia;section trachyphytum;species nitens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101884</other_info_on_name>
  </taxon_identification>
  <number>79.</number>
  <other_name type="common_name">Shining blazingstar</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants candelabra-form, 5–20 (–35) cm.</text>
      <biological_entity id="o32119" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves persisting;</text>
      <biological_entity constraint="basal" id="o32120" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole present or absent;</text>
      <biological_entity id="o32121" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to linear, margins usually deeply to shallowly lobed, rarely entire.</text>
      <biological_entity id="o32122" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
      </biological_entity>
      <biological_entity id="o32123" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply to shallowly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: petiole absent;</text>
      <biological_entity constraint="cauline" id="o32124" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o32125" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovatelanceolate to linear, to 15 cm, margins deeply to shallowly lobed or entire.</text>
      <biological_entity constraint="cauline" id="o32126" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o32127" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32128" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Bracts green, lanceolate, 4.5–8.3 × 1.2–3.6 mm, width 1/5–1/2 length, not concealing capsule, margins entire.</text>
      <biological_entity id="o32129" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s6" to="8.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s6" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1/(1/5-1/2)" value_original="1/(1/5-1/2)" />
      </biological_entity>
      <biological_entity id="o32130" name="capsule" name_original="capsule" src="d0_s6" type="structure" />
      <biological_entity id="o32131" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o32129" id="r2628" name="concealing" negation="true" src="d0_s6" to="o32130" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 3–8 mm;</text>
      <biological_entity id="o32132" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o32133" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow to orange proximally, yellow distally, (7–) 8–18 mm, apex rounded or acute apex;</text>
      <biological_entity id="o32134" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o32135" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange proximally" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32136" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o32137" name="apex" name_original="apex" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stamens 20+, 3–8 mm, filaments monomorphic, filiform, unlobed;</text>
      <biological_entity id="o32138" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32139" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32140" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 4–8 mm.</text>
      <biological_entity id="o32141" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32142" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules clavate, 13–26 × 2–3.5 mm, axillary curved to 180° at maturity, usually inconspicuously longitudinally ribbed.</text>
      <biological_entity id="o32143" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s11" to="26" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character constraint="at maturity" is_modifier="false" modifier="0-180°" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually inconspicuously longitudinally" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 15–40, in 2+ rows distal to mid fruit, tan, usually dark-mottled, usually irregularly polygonal, occasionally triangular prisms proximal to mid fruit, surface colliculate under 10x magnification;</text>
      <biological_entity id="o32144" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s12" to="40" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="dark-mottled" value_original="dark-mottled" />
        <character is_modifier="false" modifier="usually irregularly" name="shape" src="d0_s12" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o32145" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o32146" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o32147" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o32148" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character constraint="under magnification" constraintid="o32149" is_modifier="false" name="relief" src="d0_s12" value="colliculate" value_original="colliculate" />
      </biological_entity>
      <biological_entity id="o32149" name="magnification" name_original="magnification" src="d0_s12" type="structure" />
      <relation from="o32144" id="r2629" name="in" negation="false" src="d0_s12" to="o32145" />
    </statement>
    <statement id="d0_s13">
      <text>recurved flap over hilum usually present;</text>
      <biological_entity id="o32150" name="flap" name_original="flap" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o32151" name="hilum" name_original="hilum" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o32150" id="r2630" name="over" negation="false" src="d0_s13" to="o32151" />
    </statement>
    <statement id="d0_s14">
      <text>seed-coat cell outer periclinal wall domed, domes on seed edges less than 1/2 as tall as wide at maturity.</text>
      <biological_entity constraint="seed-coat" id="o32152" name="cell" name_original="cell" src="d0_s14" type="structure" />
      <biological_entity constraint="outer" id="o32153" name="wall" name_original="wall" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="periclinal" value_original="periclinal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="domed" value_original="domed" />
      </biological_entity>
      <biological_entity constraint="seed" id="o32155" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s14" to="1/2" />
      </biological_entity>
      <relation from="o32154" id="r2631" name="on" negation="false" src="d0_s14" to="o32155" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o32154" name="dome" name_original="domes" src="d0_s14" type="structure">
        <character constraint="at maturity" is_modifier="false" name="width" src="d0_s14" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32156" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Mentzelia nitens is similar to both M. eremophila and M. jonesii but exhibits little distributional overlap with either species. See 71. M. eremophila and 73. M. jonesii for discussion of similarities. Reports of M. nitens from Arizona are based on specimens treated here as M. jonesii.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy washes, rocky slopes, desert scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy washes" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2000 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>