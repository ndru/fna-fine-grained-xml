<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="de Candolle ex Perleb" date="unknown" rank="family">LINACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">LINUM</taxon_name>
    <taxon_name authority="(Reichenbach) Engelmann" date="1852" rank="section">Linopsis</taxon_name>
    <taxon_name authority="(Trelease) H. J. P. Winkler in H. G. A. Engler et al." date="1931" rank="species">subteres</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 19a: 116. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linopsis;species subteres</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linum</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">aristatum</taxon_name>
    <taxon_name authority="Trelease in A. Gray et al." date="1897" rank="variety">subteres</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,2): 347. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus linum;species aristatum;variety subteres</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">L.</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">leptopoda</taxon_name>
    <taxon_hierarchy>genus l.;species leptopoda</taxon_hierarchy>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Sprucemont flax</other_name>
  <other_name type="common_name">Utah yellow flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, 15–50 cm, glabrous and glaucous.</text>
      <biological_entity id="o20502" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems stiffly spreading-ascending, branched at base and distal to middle.</text>
      <biological_entity id="o20503" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s1" value="spreading-ascending" value_original="spreading-ascending" />
        <character constraint="at base" constraintid="o20504" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="distal" name="position" notes="" src="d0_s1" to="middle" />
      </biological_entity>
      <biological_entity id="o20504" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate or proximalmost opposite, crowded at base, appressed-ascending;</text>
      <biological_entity constraint="proximalmost" id="o20506" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character constraint="at base" constraintid="o20507" is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="appressed-ascending" value_original="appressed-ascending" />
      </biological_entity>
      <biological_entity id="o20507" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipular glands absent;</text>
      <biological_entity constraint="stipular" id="o20508" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to lanceolate or linear-lanceolate, 8–17 × 1.2–2.3 mm, margins entire, not ciliate, apex apiculate.</text>
      <biological_entity id="o20509" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate or linear-lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="17" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s4" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20510" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20511" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences few-flowered racemes.</text>
      <biological_entity id="o20512" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o20513" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="few-flowered" value_original="few-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels (5–) 20–30 (–60) mm.</text>
      <biological_entity id="o20514" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals persistent, lanceolate to lanceovate, 4.5–7 mm, margins narrowly scarious, inner sepals conspicuously toothed, outer ones very coarsely glandular-toothed, sometimes sparsely so, apex acuminate or narrowly acute;</text>
      <biological_entity id="o20515" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o20516" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="lanceovate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20517" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20518" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="outer" id="o20519" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="very coarsely" name="shape" src="d0_s7" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o20520" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely; sparsely" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals lemon yellow, obovate, 9–15 mm;</text>
      <biological_entity id="o20521" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o20522" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="lemon yellow" value_original="lemon yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 5–7 mm;</text>
      <biological_entity id="o20523" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20524" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 1–2 mm;</text>
      <biological_entity id="o20525" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20526" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodia absent;</text>
      <biological_entity id="o20527" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20528" name="staminodium" name_original="staminodia" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles connate to within 0.8–3 mm of apex, 5.7–9 mm;</text>
      <biological_entity id="o20529" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o20530" name="style" name_original="styles" src="d0_s12" type="structure">
        <character constraint="of apex" constraintid="o20531" is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20531" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas capitate.</text>
      <biological_entity id="o20532" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20533" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid (distinctly longer than broad), 3.5–4.6 × 2.5–3.1 mm, apex sharp-pointed, dehiscing completely into 5, 2-seeded segments (very easily crushed), segments persistent on plant, false septa incomplete, proximal margins terminating in loose fringe, cartilaginous plates at base of segments poorly developed.</text>
      <biological_entity id="o20534" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s14" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s14" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20535" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="sharp-pointed" value_original="sharp-pointed" />
        <character constraint="into segments" constraintid="o20536" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o20536" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="2-seeded" value_original="2-seeded" />
      </biological_entity>
      <biological_entity id="o20537" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character constraint="on plant" constraintid="o20538" is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o20538" name="plant" name_original="plant" src="d0_s14" type="structure" />
      <biological_entity constraint="false" id="o20539" name="septum" name_original="septa" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="incomplete" value_original="incomplete" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20540" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o20541" name="fringe" name_original="fringe" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s14" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o20542" name="plate" name_original="plates" src="d0_s14" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s14" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s14" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o20543" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s14" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o20544" name="segment" name_original="segments" src="d0_s14" type="structure" />
      <relation from="o20540" id="r1672" name="terminating in" negation="false" src="d0_s14" to="o20541" />
      <relation from="o20540" id="r1673" name="terminating in" negation="false" src="d0_s14" to="o20542" />
      <relation from="o20540" id="r1674" name="at" negation="false" src="d0_s14" to="o20543" />
      <relation from="o20543" id="r1675" name="part_of" negation="false" src="d0_s14" to="o20544" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2.5–3 × 0.9–1.2 mm. 2n = 30.</text>
      <biological_entity id="o20545" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20546" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Linum subteres is most closely related to L. vernale; it has lemon yellow petals, rather than orange to salmon-colored with a maroon base, and relatively thick, crowded, broad basal leaves (C. M. Rogers 1984). Leaves on the proximal half of each stem are closely spaced and imbricate; distal branches and inflorescence are widely spaced and subtended by closely appressed, relatively long, narrow leaves or bracts, giving the upper part of the plant a leafless look.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, clay, sagebrush and pinyon-juniper zones.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="pinyon-juniper zones" modifier="sagebrush and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>