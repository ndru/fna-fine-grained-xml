<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="illustration_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Persoon" date="1806" rank="subgenus">Esula</taxon_name>
    <taxon_name authority="Waldstein &amp; Kitaibel" date="1803" rank="species">virgata</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Icon. Pl. Hung.</publication_title>
      <place_in_publication>2: 176, plate 162. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;subgenus esula;species virgata</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242321554</other_info_on_name>
  </taxon_identification>
  <number>124.</number>
  <other_name type="common_name">Leafy spurge</other_name>
  <other_name type="common_name">wolf’s milk</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with slender, spreading rootstock.</text>
      <biological_entity id="o10427" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10428" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o10427" id="r868" name="with" negation="false" src="d0_s0" to="o10428" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, unbranched or branched, 20–90 cm, glabrous.</text>
      <biological_entity id="o10429" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0–1 mm;</text>
      <biological_entity id="o10430" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10431" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear to linear-oblanceolate or linear-oblong (margins parallel or almost parallel at midleaf), 40–90 × 3–12 mm, base truncate or abruptly attenuate, margins entire, apex acute or rounded, sometimes mucronulate, surfaces glabrous;</text>
      <biological_entity id="o10432" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10433" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-oblanceolate or linear-oblong" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s3" to="90" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10434" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10435" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10436" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o10437" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation inconspicuous, only midvein prominent.</text>
      <biological_entity id="o10438" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o10439" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cyathial arrangement: terminal pleiochasial branches 5–17, each 1–2 times 2-branched;</text>
      <biological_entity id="o10440" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o10441" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="17" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2 times 2-branched" value_original="1-2 times 2-branched " />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pleiochasial bracts similar in shape to but shorter and wider than distal leaves;</text>
      <biological_entity id="o10442" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o10443" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pleiochasial" value_original="pleiochasial" />
        <character is_modifier="false" modifier="in shape to" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character constraint="than distal leaves" constraintid="o10444" is_modifier="false" name="width" src="d0_s6" value="shorter and wider" value_original="shorter and wider" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10444" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>dichasial bracts distinct, broadly ovate, rhombic, or reniform, base cordate or cuneate, margins entire, apex obtuse to rounded, mucronulate;</text>
      <biological_entity id="o10445" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o10446" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
      </biological_entity>
      <biological_entity id="o10447" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10448" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10449" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary cymose branches 0–18.</text>
      <biological_entity id="o10450" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o10451" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cyathia: peduncle 0–1 mm.</text>
      <biological_entity id="o10452" name="cyathium" name_original="cyathia" src="d0_s9" type="structure" />
      <biological_entity id="o10453" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucre campanulate, 1.5–3.5 × 1.7–3 mm, glabrous;</text>
      <biological_entity id="o10454" name="involucre" name_original="involucre" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glands 4, crescent-shaped, 0.6–1.5 × 1.3–2.5 mm;</text>
      <biological_entity id="o10455" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s11" value="crescent--shaped" value_original="crescent--shaped" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>horns divergent to convergent, 0.2–0.8 mm.</text>
      <biological_entity id="o10456" name="horn" name_original="horns" src="d0_s12" type="structure">
        <character char_type="range_value" from="divergent" name="arrangement" src="d0_s12" to="convergent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers 10–25.</text>
      <biological_entity id="o10457" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: ovary glabrous;</text>
      <biological_entity id="o10458" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10459" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 1.7–2.5 mm, 2-fid.</text>
      <biological_entity id="o10460" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10461" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules subglobose, 2.5–3.5 × 3–4.5 mm, slightly lobed;</text>
      <biological_entity id="o10462" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s16" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>cocci rounded, smooth except finely granulate toward abaxial line, glabrous;</text>
      <biological_entity id="o10463" name="cocci" name_original="cocci" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
        <character constraint="except abaxial line" constraintid="o10464" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10464" name="line" name_original="line" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="finely" name="texture" src="d0_s17" value="granulate" value_original="granulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella 2–3.3 mm.</text>
      <biological_entity id="o10465" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds yellowbrown to gray or mottled, oblong-ellipsoid to oblong-ovoid, 2.2–2.6 × 1.3–1.6 mm, smooth;</text>
      <biological_entity id="o10466" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s19" to="gray or mottled" />
        <character char_type="range_value" from="oblong-ellipsoid" name="shape" src="d0_s19" to="oblong-ovoid" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s19" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s19" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>caruncle subconic, 0.6–1 × 0.7–0.9 mm. 2n = 60.</text>
      <biological_entity id="o10467" name="caruncle" name_original="caruncle" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="subconic" value_original="subconic" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s20" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s20" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10468" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia virgata has caused significant economic and ecological impact over large portions of the United States and Canada. It is part of a taxonomically complex group of species native to Europe and Asia, and there has been much confusion over the naming of the species that has become widely established in the New World (A. Radcliffe-Smith 1985; P. M. Catling and G. Mitrow 2012). There has been speculation that hybridization and polyploidy have played a role in the weediness of leafy spurge, and it is possible that the widespread occurrence of leafy spurge in North America is at least partly due to multiple introductions in grain imported from Eurasia (Ma J. S. 2010). Nonetheless, a re-evaluation of the leafy spurge complex by Berry et al. (unpubl.) revealed that E. esula Linnaeus and E. virgata are two distinct, albeit related species. The true E. esula is restricted in range to certain parts of Europe and shows little tendency toward weediness where it occurs. In contrast, E. virgata is much more widespread across Europe and temperate Asia, where it shows the same weedy characteristics as leafy spurge in the New World. More importantly, it is morphologically consistent with the North American material of leafy spurge.</discussion>
  <discussion>According to D. V. Geltman (1998), the best way to distinguish morphologically between Euphorbia virgata and E. esula is by differences in their leaf shape. In E. virgata, the leaf blades are linear to linear-oblanceolate or linear-oblong, 6–15 times longer than wide, with margins that are parallel or almost parallel at the middle of the blade; the apex is usually acute, and the base is truncate or abruptly attenuate. In E. esula, the leaf blades are oblanceolate to obovate-elliptic (distinctly wider toward apex), 3–8(–10) times longer than wide, with margins not parallel at the middle of the leaf; the apex is rounded to subacute, and the base is gradually attenuate to cuneate.</discussion>
  <discussion>There are some herbarium specimens of Euphorbia esula from North America that indicate it probably occurred sporadically in certain states in the late 1800s and early 1900s, but the authors have no evidence that it has persisted in any of those places. Therefore in this treatment, E. esula is considered to be a waif in the North American flora and, by excluding it here, the authors hope to avoid confusion between it and the widespread E. virgata.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pastures, fields, waste places, shorelines, railroads, open disturbed areas.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pastures" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="shorelines" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="disturbed areas" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2600 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., S.Dak., Utah, Vt., Wash., W.Va., Wis., Wyo.; Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>