<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="Persoon" date="1806" rank="subgenus">Esula</taxon_name>
    <taxon_name authority="Tidestrom" date="1935" rank="species">yaquiana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>48: 41. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;subgenus esula;species yaquiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101649</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">schizoloba</taxon_name>
    <taxon_name authority="Norton" date="1899" rank="variety">mollis</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Euphorbia,</publication_title>
      <place_in_publication>43, plate 43. 1899</place_in_publication>
      <other_info_on_pub>not E. mollis C. C. Gmelin 1806</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus euphorbia;species schizoloba;variety mollis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">incisa</taxon_name>
    <taxon_name authority="(Norton) L. C. Wheeler" date="unknown" rank="variety">mollis</taxon_name>
    <taxon_hierarchy>genus e.;species incisa;variety mollis</taxon_hierarchy>
  </taxon_identification>
  <number>125.</number>
  <other_name type="common_name">Hairy Mojave spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with thick rootstock.</text>
      <biological_entity id="o8840" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o8841" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o8840" id="r723" name="with" negation="false" src="d0_s0" to="o8841" />
    </statement>
    <statement id="d0_s1">
      <text>Stems slender, erect or ascending, sometimes sinuous, densely branched near base, 10–50 cm, moderately to densely puberulent to lanulose.</text>
      <biological_entity id="o8842" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s1" value="sinuous" value_original="sinuous" />
        <character constraint="near base" constraintid="o8843" is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
        <character char_type="range_value" from="puberulent" modifier="moderately to densely; densely" name="pubescence" src="d0_s1" to="lanulose" />
      </biological_entity>
      <biological_entity id="o8843" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0–1 mm;</text>
      <biological_entity id="o8844" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8845" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually lanceolate or elliptic-lanceolate, sometimes slightly oblanceolate, 8–30 × 6–14 mm, base usually acute, occasionally short-attenuate, rarely obtuse, margins entire, apex usually acute, occasionally obtuse, acuminate, or cuspidate, surfaces sparsely to moderately puberulent to lanulose;</text>
      <biological_entity id="o8846" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8847" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8848" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="short-attenuate" value_original="short-attenuate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o8849" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8850" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o8851" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="puberulent" modifier="moderately" name="pubescence" src="d0_s3" to="lanulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>venation pinnate, sometimes obscure, midvein prominent.</text>
      <biological_entity id="o8852" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o8853" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cyathial arrangement: terminal pleiochasial branches 3–5, each 1–2 times 2-branched;</text>
      <biological_entity id="o8854" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8855" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2 times 2-branched" value_original="1-2 times 2-branched " />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pleiochasial bracts broadly ovate to subcordate, usually similar in size to, occasionally wider than, distal leaves;</text>
      <biological_entity id="o8856" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o8857" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pleiochasial" value_original="pleiochasial" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="subcordate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8858" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>dichasial bracts distinct, broadly ovate to almost reniform, base obtuse, margins entire, apex obtuse, acuminate to cuspidate;</text>
      <biological_entity id="o8859" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity id="o8860" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate to almost" value_original="ovate to almost" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s7" value="reniform" value_original="reniform" />
      </biological_entity>
      <biological_entity id="o8861" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o8862" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8863" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s7" to="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary cymose branches 0–5.</text>
      <biological_entity id="o8864" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="cyathial" value_original="cyathial" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o8865" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cyathia: peduncle 0.3–0.8 mm.</text>
      <biological_entity id="o8866" name="cyathium" name_original="cyathia" src="d0_s9" type="structure" />
      <biological_entity id="o8867" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucre campanulate to broadly turbinate, 2.2–3 × 2–2.5 mm, puberulent to lanulose;</text>
      <biological_entity id="o8868" name="involucre" name_original="involucre" src="d0_s10" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s10" to="broadly turbinate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s10" to="lanulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glands 4, semicircular, trapezoidal, or elliptic-truncate, 0.8–1.5 × 1–2.2 mm, margins strongly crenate or dentate;</text>
      <biological_entity id="o8869" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s11" value="semicircular" value_original="semicircular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="trapezoidal" value_original="trapezoidal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-truncate" value_original="elliptic-truncate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="trapezoidal" value_original="trapezoidal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-truncate" value_original="elliptic-truncate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8870" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>horns usually absent, if present then straight, 0.1–0.2 mm, generally equaling teeth on gland margin.</text>
      <biological_entity id="o8871" name="horn" name_original="horns" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8872" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="generally" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="gland" id="o8873" name="margin" name_original="margin" src="d0_s12" type="structure" />
      <relation from="o8872" id="r724" name="on" negation="false" src="d0_s12" to="o8873" />
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers 12–20.</text>
      <biological_entity id="o8874" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: ovary usually puberulent, occasionally lanulose;</text>
      <biological_entity id="o8875" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8876" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s14" value="lanulose" value_original="lanulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 1–1.2 mm, 2-fid.</text>
      <biological_entity id="o8877" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8878" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules oblong-ovoid, 3.5–4 × 3–4 mm, 3-lobed;</text>
      <biological_entity id="o8879" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>cocci rounded, smooth, usually puberulent, occasionally lanulose;</text>
      <biological_entity id="o8880" name="cocci" name_original="cocci" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s17" value="lanulose" value_original="lanulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>columella 2.5–3 mm.</text>
      <biological_entity id="o8881" name="columella" name_original="columella" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds gray to whitish, oblong cylindric, 2–3 × 1.5–1.8 mm, irregularly shallowly pitted to almost smooth;</text>
      <biological_entity id="o8882" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s19" to="whitish" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s19" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s19" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly shallowly" name="relief" src="d0_s19" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s19" value="pitted to almost" value_original="pitted to almost" />
        <character is_modifier="false" modifier="almost" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>caruncle conic, 0.6 × 0.6 mm.</text>
      <biological_entity id="o8883" name="caruncle" name_original="caruncle" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="conic" value_original="conic" />
        <character name="length" src="d0_s20" unit="mm" value="0.6" value_original="0.6" />
        <character name="width" src="d0_s20" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia yaquiana is endemic to Pima and Graham counties in southern Arizona and is known only from the Santa Catalina and Pinaleño mountains. Records of E. yaquiana from southwestern Colorado (as E. incisa var. mollis) likely represent misidentifications of E. brachycera; therefore, those disjunct occurrences have been excluded here from the distribution of E. yaquiana. Euphorbia yaquiana has often been treated as a synonym of E. schizoloba var. mollis, but molecular phylogenetic data show that it is more closely related to E. brachycera and E. chamaesula (J. A. Peirson et al. 2014).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ponderosa pine forests, oak-pine mixed forests, dry stream banks and beds, open scrub areas, roadsides.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponderosa pine forests" />
        <character name="habitat" value="oak-pine mixed forests" />
        <character name="habitat" value="dry stream banks" />
        <character name="habitat" value="beds" />
        <character name="habitat" value="open scrub areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2200 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>