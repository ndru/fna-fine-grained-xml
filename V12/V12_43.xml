<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TRAGIA</taxon_name>
    <taxon_name authority="(Müller Arg.) Pax &amp; K. Hoffmann in H. G. A. Engler" date="1919" rank="species">amblyodonta</taxon_name>
    <place_of_publication>
      <publication_title>Pflanzenr.</publication_title>
      <place_in_publication>68[IV,147]: 51. 1919</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species amblyodonta</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101945</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tragia</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">nepetifolia</taxon_name>
    <taxon_name authority="Müller Arg. in A. P. de Candolle and A. L. P. P. de Candolle" date="1866" rank="variety">amblyodonta</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>15(2): 934. 1866</place_in_publication>
      <other_info_on_pub>(as nepetaefolia)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus tragia;species nepetifolia;variety amblyodonta</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Dog-tooth or blunt-toothed noseburn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 1.2–5 dm.</text>
      <biological_entity id="o10469" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to trailing, gray-green, apex often flexuous.</text>
      <biological_entity id="o10470" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="trailing" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o10471" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 4–20 (–30) mm;</text>
      <biological_entity id="o10472" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10473" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually triangular to subhastate, sometimes ovate, 1–4.5 × 0.8–3 cm, base cordate, hastate, or truncate, margins crenate to serrate, apex acute to obtuse.</text>
      <biological_entity id="o10474" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10475" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually triangular" name="shape" src="d0_s3" to="subhastate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10476" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o10477" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="serrate" />
      </biological_entity>
      <biological_entity id="o10478" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal or axillary, glands absent, staminate flowers 5–16 per raceme;</text>
      <biological_entity id="o10479" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o10480" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10481" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per raceme" constraintid="o10482" from="5" name="quantity" src="d0_s4" to="16" />
      </biological_entity>
      <biological_entity id="o10482" name="raceme" name_original="raceme" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>staminate bracts 0.9–2 mm.</text>
      <biological_entity id="o10483" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: staminate 0.7–1.2 mm, persistent base 0.2–0.8 mm;</text>
      <biological_entity id="o10484" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10485" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 1.5–4 mm in fruit.</text>
      <biological_entity id="o10486" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10487" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10487" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3–4, green, 0.9–1.2 mm;</text>
      <biological_entity id="o10488" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10489" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 3–4, filaments 0.2–0.7 mm.</text>
      <biological_entity id="o10490" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10491" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity id="o10492" name="all" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: sepals lanceolate, 1–2.5 mm;</text>
      <biological_entity id="o10493" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10494" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles connate to 1/3 length, short-exserted;</text>
      <biological_entity id="o10495" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10496" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="length" src="d0_s11" to="1/3" />
        <character is_modifier="false" name="position" src="d0_s11" value="short-exserted" value_original="short-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas undulate to subpapillate.</text>
      <biological_entity id="o10497" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10498" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="relief" src="d0_s12" value="subpapillate" value_original="subpapillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 7–8 mm wide.</text>
      <biological_entity id="o10499" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds brown with tan mottling, 2.5–3.5 mm. 2n = 110.</text>
      <biological_entity id="o10500" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown with tan mottling" value_original="brown with tan mottling" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10501" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="110" value_original="110" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tragia amblyodonta is easily distinguished from other members of Tragia by the combination of usually triangular to subhastate leaf blades, gray-green coloration, and painfully stinging hairs. Both stomata diameter and pollen grain size of T. amblyodonta are larger than in any other North American species of Tragia (K. I. Miller and G. L. Webster 1967).</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall; fruiting summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="fruiting time" char_type="range_value" to="late fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky, exposed slopes in xerophytic scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" modifier="dry rocky exposed" constraint="in xerophytic scrub" />
        <character name="habitat" value="xerophytic scrub" modifier="in" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1400 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>