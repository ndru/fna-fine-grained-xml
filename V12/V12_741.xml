<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">EUPHORBIA</taxon_name>
    <taxon_name authority="(Schlechtendal) Baillon" date="1858" rank="section">Alectoroctonum</taxon_name>
    <taxon_name authority="(Klotzsch &amp; Garcke) Boissier in A. P. de Candolle and A. L. P. P. de Candolle" date="1862" rank="species">macropus</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>15(2): 52. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum;species macropus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101540</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anisophyllum</taxon_name>
    <taxon_name authority="Klotzsch &amp; Garcke" date="1859" rank="species">macropus</taxon_name>
    <place_of_publication>
      <publication_title>Abh. Königl. Akad. Wiss. Berlin</publication_title>
      <place_in_publication>1859: 33. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus anisophyllum;species macropus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euphorbia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">biformis</taxon_name>
    <taxon_hierarchy>genus euphorbia;species biformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">E.</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">plummerae</taxon_name>
    <taxon_hierarchy>genus e.;species plummerae</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Huachuca mountain spurge</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with thick, globose to elongated tubers, 2–8 cm.</text>
      <biological_entity id="o24345" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="8" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o24346" name="tuber" name_original="tubers" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="true" name="shape" src="d0_s0" value="globose" value_original="globose" />
        <character is_modifier="true" name="length" src="d0_s0" value="elongated" value_original="elongated" />
      </biological_entity>
      <relation from="o24345" id="r1991" name="with" negation="false" src="d0_s0" to="o24346" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, branched, 10–45 (–60) cm, glabrous, puberulent, or densely hirsute to setose, often with 2-layered indumentum of long hairs intermixed with short hairs.</text>
      <biological_entity id="o24347" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="densely hirsute" name="pubescence" src="d0_s1" to="setose" />
        <character char_type="range_value" from="densely hirsute" name="pubescence" src="d0_s1" to="setose" />
      </biological_entity>
      <biological_entity id="o24348" name="indumentum" name_original="indumentum" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="2-layered" value_original="2-layered" />
      </biological_entity>
      <biological_entity id="o24349" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o24350" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o24347" id="r1992" modifier="often" name="with" negation="false" src="d0_s1" to="o24348" />
      <relation from="o24348" id="r1993" name="part_of" negation="false" src="d0_s1" to="o24349" />
      <relation from="o24348" id="r1994" name="intermixed with" negation="false" src="d0_s1" to="o24350" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually opposite, occasionally whorled distally, or rarely with 1–2 alternate leaves;</text>
      <biological_entity id="o24351" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="occasionally; distally" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
      </biological_entity>
      <biological_entity id="o24352" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="2" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
      <relation from="o24351" id="r1995" modifier="rarely" name="with" negation="false" src="d0_s2" to="o24352" />
    </statement>
    <statement id="d0_s3">
      <text>stipules 0.1–0.2 mm;</text>
      <biological_entity id="o24353" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s3" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–18 mm, hirsute, sericeous, or strigose;</text>
      <biological_entity id="o24354" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to ovate or almost orbiculate, 6–54 × 2–19 mm, base rounded to attenuate, margins entire, occasionally ciliate with stiff recurved hairs, apex acute to obtuse, surfaces usually hirsute, sericeous, or strigose, occasionally glabrous adaxially;</text>
      <biological_entity id="o24355" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="ovate" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="54" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24356" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
      <biological_entity id="o24357" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="with hairs" constraintid="o24358" is_modifier="false" modifier="occasionally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o24358" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o24359" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>venation conspicuous.</text>
      <biological_entity id="o24360" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="occasionally; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cyathia in weakly-defined terminal dichasia;</text>
      <biological_entity id="o24361" name="cyathium" name_original="cyathia" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o24362" name="dichasium" name_original="dichasia" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="weakly-defined" value_original="weakly-defined" />
      </biological_entity>
      <relation from="o24361" id="r1996" name="in" negation="false" src="d0_s7" to="o24362" />
    </statement>
    <statement id="d0_s8">
      <text>peduncle 1.4–5.8 mm, glabrous.</text>
      <biological_entity id="o24363" name="peduncle" name_original="peduncle" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="5.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucre obconic to campanulate, 1.1–1.4 × 0.5–1.5 mm, glabrous or strigillose;</text>
      <biological_entity id="o24364" name="involucre" name_original="involucre" src="d0_s9" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s9" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>glands 4–5, greenish, oblong, 0.2 × 0.4–0.5 mm;</text>
      <biological_entity id="o24365" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character name="length" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>appendages usually yellowish or green, rarely dark purple, ovate, flabellate, semiorbiculate, or oblong, 0.3–0.9 × 0.4–1.1 mm, usually entire.</text>
      <biological_entity id="o24366" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="semiorbiculate" value_original="semiorbiculate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="semiorbiculate" value_original="semiorbiculate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s11" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers 10–15.</text>
      <biological_entity id="o24367" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: ovary glabrous, sericeous, or strigillose;</text>
      <biological_entity id="o24368" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24369" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 0.4–0.6 mm, 2-fid 1/2 length.</text>
      <biological_entity id="o24370" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24371" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-fid" value_original="2-fid" />
        <character name="length" src="d0_s14" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblate, 2.3–3 × 3.1–4.2 mm, glabrous, sericeous, or strigillose;</text>
      <biological_entity id="o24372" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblate" value_original="oblate" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="3.1" from_unit="mm" name="width" src="d0_s15" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>columella 1.6–2.1 mm.</text>
      <biological_entity id="o24373" name="columella" name_original="columella" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds black to light-brown, broadly ovoid to subglobose, rounded in cross-section, 1.5–2.3 × 1.4–1.8 mm, smooth or with low rounded tubercles;</text>
      <biological_entity id="o24374" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s17" to="light-brown" />
        <character char_type="range_value" from="broadly ovoid" name="shape" src="d0_s17" to="subglobose" />
        <character constraint="in cross-section , 1.5-2.3×1.4-1.8 mm , smooth or with tubercles" constraintid="o24375" is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24375" name="tubercle" name_original="tubercles" src="d0_s17" type="structure">
        <character is_modifier="true" name="position" src="d0_s17" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>caruncle absent.</text>
      <biological_entity id="o24376" name="caruncle" name_original="caruncle" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Euphorbia macropus is a widespread and common Mexican species just barely entering the flora area in southeastern Arizona, where most of the collections are from the Huachuca Mountains.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks and rocky slopes in pine-oak woodlands, sometimes with juniper, Douglas fir-pine forests.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="rocky slopes" constraint="in pine-oak woodlands" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="juniper" modifier="sometimes with" />
        <character name="habitat" value="fir-pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2200m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico; Central America (Guatemala, Honduras).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>