<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CROTON</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">glandulosus</taxon_name>
    <taxon_name authority="(A. M. Ferguson) R. W. Long" date="1970" rank="variety">floridanus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>72: 22. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus croton;species glandulosus;variety floridanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250102010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Croton</taxon_name>
    <taxon_name authority="A. M. Ferguson" date="1901" rank="species">floridanus</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>12: 50, plate 15. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus croton;species floridanus</taxon_hierarchy>
  </taxon_identification>
  <number>12b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–2 dm.</text>
      <biological_entity id="o22921" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely stellate-hairy, hairs appressed, radii equal, central radius lacking.</text>
      <biological_entity id="o22922" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o22923" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="variability" src="d0_s1" value="equal" value_original="equal" />
        <character is_modifier="false" name="position" src="d0_s1" value="central" value_original="central" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole apical glands sessile, circular when dry, 0.2–0.3 mm diam.;</text>
      <biological_entity id="o22924" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22925" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity constraint="apical" id="o22926" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="circular" value_original="circular" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 0.6–1.5 (–3) × 0.5–1.2 cm, length mostly 2 times width or less, membranous, marginal teeth rounded, both surfaces glabrate;</text>
      <biological_entity id="o22927" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22928" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.2" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s3" value="2" value_original="2" />
        <character name="l_w_ratio" src="d0_s3" value="less" value_original="less" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o22929" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o22930" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base markedly 3-veined.</text>
      <biological_entity id="o22931" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22932" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="markedly" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety floridanus grows in Broward, Collier, Lee, Manatee, Martin, and Pinellas counties.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Beaches, sand dunes, old fields, disturbed sites, waste places.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beaches" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>