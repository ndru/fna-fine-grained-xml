<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">63</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">RHAMNACEAE</taxon_name>
    <taxon_name authority="Cavanilles" date="1799" rank="genus">CONDALIA</taxon_name>
    <taxon_name authority="I. M. Johnston" date="1924" rank="species">globosa</taxon_name>
    <taxon_name authority="I. M. Johnston" date="1924" rank="variety">pubescens</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>4, 12: 1087. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus condalia;species globosa;variety pubescens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101368</other_info_on_name>
  </taxon_identification>
  <number>3a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or small trees, 1–4 (–6) m;</text>
      <biological_entity id="o18081" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="true" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>primary branches usually not thorn-tipped, secondary branches suppressed, hispidulous, thorn-tipped, with short-shoots;</text>
      <biological_entity constraint="primary" id="o18083" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o18084" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="suppressed" value_original="suppressed" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
      </biological_entity>
      <biological_entity id="o18085" name="short-shoot" name_original="short-shoots" src="d0_s1" type="structure" />
      <relation from="o18084" id="r1488" name="with" negation="false" src="d0_s1" to="o18085" />
    </statement>
    <statement id="d0_s2">
      <text>internodes 2–7 mm.</text>
      <biological_entity id="o18086" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 1–2 mm;</text>
      <biological_entity id="o18087" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18088" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dull green, occasionally burnt orange abaxially, spatulate to spatulate-elliptic, 3–13 (–22) × 1.5–5 mm, subcoriaceous, margins entire, not revolute, apex obtuse to mucronate, surfaces sparsely to densely hispidulous or puberulent, abaxial intervein surfaces microvesiculate, not appearing waxy;</text>
      <biological_entity id="o18089" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18090" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="occasionally; abaxially" name="coloration" src="d0_s4" value="burnt orange" value_original="burnt orange" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="spatulate-elliptic" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o18091" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o18092" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="mucronate" />
      </biological_entity>
      <biological_entity id="o18093" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="intervein" id="o18094" name="surface" name_original="surfaces" src="d0_s4" type="structure" constraint_original="abaxial intervein">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="ceraceous" value_original="waxy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation thick and raised, conspicuous abaxially.</text>
      <biological_entity id="o18095" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="raised" value_original="raised" />
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences on short-shoots, 1–7-flowered.</text>
      <biological_entity id="o18096" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
      <biological_entity id="o18097" name="short-shoot" name_original="short-shoots" src="d0_s6" type="structure" />
      <relation from="o18096" id="r1489" name="on" negation="false" src="d0_s6" to="o18097" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 2.5–4.5 (–6.5) mm.</text>
      <biological_entity id="o18098" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals deciduous in fruit;</text>
      <biological_entity id="o18099" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18100" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o18101" is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o18101" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals 0.</text>
      <biological_entity id="o18102" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18103" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Drupes usually globose, 3.4–5.1 mm;</text>
      <biological_entity id="o18104" name="drupe" name_original="drupes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stones 1-seeded.</text>
      <biological_entity id="o18105" name="stone" name_original="stones" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-seeded" value_original="1-seeded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mainly Jan–Apr, sporadically year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mainly" to="Apr" from="Jan" />
        <character name="flowering time" char_type="range_value" modifier="sporadically" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry desert washes, drainages, canyons, open slopes, creosote bush scrub.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry desert washes" />
        <character name="habitat" value="drainages" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="open slopes" />
        <character name="habitat" value="bush scrub" modifier="creosote" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1500 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Baja California Sur, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>