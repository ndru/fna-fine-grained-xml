<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">HYDRANGEACEAE</taxon_name>
    <taxon_hierarchy>family hydrangeaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">242413881</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Hydrangea Family</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, shrubs, trees, or vines [herbs], evergreen or deciduous.</text>
      <biological_entity id="o1513" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character name="growth_form" value="vine" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually opposite, sometimes whorled [alternate], simple;</text>
      <biological_entity id="o1517" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules absent;</text>
      <biological_entity id="o1518" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present or absent;</text>
      <biological_entity id="o1519" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade sometimes palmately lobed, margins entire, serrate, serrulate, dentate, denticulate, or crenate;</text>
      <biological_entity id="o1520" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes palmately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate or acrodromous (Fendlera, Fendlerella, Philadelphus, Whipplea).</text>
      <biological_entity id="o1521" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="acrodromous" value_original="acrodromous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, cymes, panicles, racemes, or corymbs, or flowers solitary.</text>
      <biological_entity id="o1522" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o1523" name="cyme" name_original="cymes" src="d0_s6" type="structure" />
      <biological_entity id="o1524" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o1525" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o1526" name="corymb" name_original="corymbs" src="d0_s6" type="structure" />
      <biological_entity id="o1527" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual [unisexual], or sometimes marginal ones sterile, radially symmetric (bisexual ones) or bilaterally symmetric with enlarged petaloid sepals (sterile ones);</text>
      <biological_entity id="o1528" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s7" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
        <character constraint="with sepals" constraintid="o1529" is_modifier="false" modifier="bilaterally; bilaterally" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
      <biological_entity id="o1529" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="petaloid" value_original="petaloid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth and androecium nearly hypogynous, perigynous, or epigynous;</text>
      <biological_entity id="o1530" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="epigynous" value_original="epigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="epigynous" value_original="epigynous" />
      </biological_entity>
      <biological_entity id="o1531" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="position" src="d0_s8" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="epigynous" value_original="epigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
        <character is_modifier="false" name="position" src="d0_s8" value="epigynous" value_original="epigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium completely adnate to ovary or adnate to ovary proximally, free distally;</text>
      <biological_entity id="o1532" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character constraint="to " constraintid="o1534" is_modifier="false" modifier="completely" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="distally" name="fusion" notes="" src="d0_s9" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o1533" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
      <biological_entity id="o1534" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 4–12, distinct or connate basally;</text>
      <biological_entity id="o1535" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="12" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 4–12, connate basally [entirely, then calyptrate];</text>
      <biological_entity id="o1536" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s11" to="12" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectary usually present, rarely absent;</text>
      <biological_entity id="o1537" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 8–200, usually distinct, sometimes connate proximally, free;</text>
      <biological_entity id="o1538" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s13" to="200" />
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes; proximally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers dehiscing by longitudinal slits;</text>
      <biological_entity id="o1539" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="by slits" constraintid="o1540" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o1540" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pistil 1, 2–12-carpellate, ovary less than 1/2 inferior, 1/2 inferior, or completely inferior, 1–12-locular, placentation usually axile proximally, parietal distally, rarely strictly axile or parietal;</text>
      <biological_entity id="o1541" name="pistil" name_original="pistil" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-12-carpellate" value_original="2-12-carpellate" />
      </biological_entity>
      <biological_entity id="o1542" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s15" to="1/2" />
        <character is_modifier="false" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" modifier="completely" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" modifier="completely" name="position" src="d0_s15" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-12-locular" value_original="1-12-locular" />
        <character is_modifier="false" modifier="usually; proximally" name="placentation" src="d0_s15" value="axile" value_original="axile" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s15" value="parietal" value_original="parietal" />
        <character is_modifier="false" modifier="rarely strictly" name="arrangement" src="d0_s15" value="axile" value_original="axile" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 1–50 per locule, anatropous;</text>
      <biological_entity id="o1543" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o1544" from="1" name="quantity" src="d0_s16" to="50" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s16" value="anatropous" value_original="anatropous" />
      </biological_entity>
      <biological_entity id="o1544" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 1–12, distinct or connate proximally to most of length;</text>
      <biological_entity id="o1545" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="12" />
        <character is_modifier="false" name="length" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="length" src="d0_s17" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas (1–) 2–12.</text>
      <biological_entity id="o1546" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s18" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s18" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsules [berries], dehiscence septicidal, loculicidal, interstylar, or intercostal.</text>
      <biological_entity constraint="fruits" id="o1547" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" name="dehiscence" src="d0_s19" value="loculicidal" value_original="loculicidal" />
        <character is_modifier="false" name="position" src="d0_s19" value="interstylar" value_original="interstylar" />
        <character is_modifier="false" name="position" src="d0_s19" value="intercostal" value_original="intercostal" />
        <character is_modifier="false" name="position" src="d0_s19" value="interstylar" value_original="interstylar" />
        <character is_modifier="false" name="position" src="d0_s19" value="intercostal" value_original="intercostal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1–50 per locule, funicular appendage present (Fendlerella, Whipplea) or absent.</text>
      <biological_entity id="o1548" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o1549" from="1" name="quantity" src="d0_s20" to="50" />
      </biological_entity>
      <biological_entity id="o1549" name="locule" name_original="locule" src="d0_s20" type="structure" />
      <biological_entity id="o1550" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s20" value="funicular" value_original="funicular" />
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 17, species ca. 240 (9 genera, 25 species in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Eurasia, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>A. Cronquist (1981) placed Hydrangeaceae among a group of woody families traditionally allied with Saxifragaceae. Phylogenetic studies consistently place Hydrangeaceae in the Cornales and sister to Loasaceae (A. L. Hempel et al. 1995; D. E. Soltis et al. 1995; L. Hufford et al. 2001; Hufford 2004). Within Hydrangeaceae, the western North American genera Fendlera and Jamesia form a clade (subfam. Jamesioideae L. Hufford) that is sister to the rest of the family (subfam. Hydrangeoideae Burnett) (Hufford et al.; Hufford). Subfamily Hydrangeoideae comprises two tribes: Philadelpheae de Candolle ex Duby and Hydrangeeae de Candolle. North American genera in the former are Carpenteria, Deutzia, Fendlerella, Philadelphus, and Whipplea. A molecular phylogenetic study by Y. De Smet et al. (2015) clarified relationships within Hydrangeeae, found Hydrangea to be polyphyletic, and promoted adoption of a broader concept of Hydrangea that includes the eight other genera in the tribe. The two North American genera in the tribe, Decumaria and Hydrangea, are circumscribed here in their traditional senses.</discussion>
  <discussion>The Hydrangeaceae are well represented in the paleobotanical record dating back to the Upper Cretaceous but best represented in the Tertiary (L. Hufford 2004). Some genera are sources of popular introduced or native ornamentals, including Carpenteria, Deutzia, Hydrangea, and Philadelphus. Some ornamentals have become established outside of cultivation in the flora area. A few North American Hydrangeaceae have reputed medicinal (D. E. Moerman 1998) or toxicologic (G. E. Burrows and R. J. Tyrl 2001) properties.</discussion>
  <discussion>Trichomes in most Hydrangeaceae consist of a long, unicellular portion, often borne on a multicellular base. The unicellular portion often bears tubercles on its surface. Sometimes instead of tubercles, it bears long extensions, making the trichome appear branched or dendritic. Such trichomes are here referred to as branched.</discussion>
  <references>
    <reference>De Smet, Y. et al. 2015. Molecular phylogenetics and new (infra)generic classification to alleviate polyphyly in tribe Hydrangeeae (Cornales: Hydrangeaceae). Taxon 64: 741–752.</reference>
    <reference>Hufford, L. 1995. Seed morphology of Hydrangeaceae and its phylogenetic implications. Int. J. Plant Sci. 156: 555–580.</reference>
    <reference>Hufford, L. 1997. A phylogenetic analysis of Hydrangeaceae based on morphological data. Int. J. Plant Sci. 158: 652–672.</reference>
    <reference>Hufford, L. 1998. Early development of androecia in polystemonous Hydrangeaceae. Amer. J. Bot. 85: 1057–1067.</reference>
    <reference>Hufford, L. 2004. Hydrangeaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 10+ vols. Berlin etc. Vol. 6, pp. 202–215.</reference>
    <reference>Hufford, L., M. L. Moody, and D. E. Soltis. 2001. A phylogenetic analysis of Hydrangeaceae based on sequences of the plastid gene matK and their combination with rbcL and morphological data. Int. J. Plant Sci. 162: 835–846.</reference>
    <reference>Small, J. K. and P. A. Rydberg. 1905. Hydrangeaceae. In: N. L. Britton et al., eds. North American Flora.... 1905+. New York. 47+ vols. Vol. 22, pp. 159–178.</reference>
    <reference>Soltis, D. E., Xiang Q. Y., and L. Hufford. 1995. Relationships and evolution of Hydrangeaceae based on rbcL sequence data. Amer. J. Bot. 82: 504–514.</reference>
    <reference>Spongberg, S. A. 1972. The genera of Saxifragaceae in the southeastern United States. J. Arnold Arbor. 53: 409–498.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Woody vines.</description>
      <determination>9 Decumaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Subshrubs, shrubs, or trees.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Twigs with stellate and simple trichomes.</description>
      <determination>5 Deutzia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Twigs glabrous or with simple or, sometimes, branched trichomes, never with stellate trichomes.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers both sterile and bisexual.</description>
      <determination>8 Hydrangea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers all bisexual.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens (11–)13–90 or 150–200.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals 4; petals 4 (or 8+ in some horticultural forms); ovaries inferior to 1/2 inferior, 4-locular; styles 1 or 4; leaves deciduous; stamens (11–)13–90.</description>
      <determination>6 Philadelphus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals 5–7; petals 5–7(–8); ovaries nearly superior, 5–7-locular; styles 1; leaves persistent; stamens 150–200.</description>
      <determination>7 Carpenteria</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 8–12.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems prostrate to decumbent.</description>
      <determination>4 Whipplea</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems erect, ascending, or spreading.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Filament apices 2-lobed, lobes prolonged beyond anthers; seeds (1–)2–4(–6) per locule.</description>
      <determination>2 Fendlera</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Filament apices not 2-lobed; seeds 1 or 10–50 per locule.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences 100–1000-flowered; capsule dehiscence interstylar, creating pore at base of styles.</description>
      <determination>8 Hydrangea</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences 1–35-flowered; capsule dehiscence septicidal.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blade margins usually crenate to dentate, rarely entire; blades ovate or broadly ovate to obovate, rhombic, or suborbiculate, venation pinnate; seeds 25–50 per locule.</description>
      <determination>1 Jamesia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blade margins entire; blades elliptic to lanceolate, oblanceolate, obovate, or linear-oblong, venation acrodromous; seeds 1 per locule.</description>
      <determination>3 Fendlerella</determination>
    </key_statement>
  </key>
</bio:treatment>