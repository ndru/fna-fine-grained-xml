<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>Lynn J. Gillespie</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">159</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">EUPHORBIACEAE</taxon_name>
    <taxon_name authority="Loureiro" date="1790" rank="genus">VERNICIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Cochinch.</publication_title>
      <place_in_publication>2: 541, 586. 1790</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus vernicia</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin vernix, varnish, alluding to use of seed oil in finishes</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">134493</other_info_on_name>
  </taxon_identification>
  <number>15.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, monoecious [dioecious];</text>
      <biological_entity id="o21728" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hairs unbranched or 2-fid;</text>
      <biological_entity id="o21729" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>latex whitish or reddish (often not apparent).</text>
      <biological_entity id="o21730" name="latex" name_original="latex" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous [persistent], alternate, simple;</text>
      <biological_entity id="o21731" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present, caducous;</text>
      <biological_entity id="o21732" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present, glands present at apex;</text>
      <biological_entity id="o21733" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21734" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character constraint="at apex" constraintid="o21735" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21735" name="apex" name_original="apex" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed or palmately lobed, margins entire, laminar glands absent or present in sinuses of lobes;</text>
      <biological_entity id="o21736" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o21737" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21739" name="sinuse" name_original="sinuses" src="d0_s6" type="structure" />
      <biological_entity id="o21740" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <relation from="o21739" id="r1782" name="part_of" negation="false" src="d0_s6" to="o21740" />
    </statement>
    <statement id="d0_s7">
      <text>venation palmate.</text>
      <biological_entity constraint="laminar" id="o21738" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character constraint="in sinuses" constraintid="o21739" is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences bisexual (cymules staminate or bisexual, pistillate flower central, staminate lateral) [unisexual], terminal, paniclelike thyrses;</text>
      <biological_entity id="o21741" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21742" name="thyrse" name_original="thyrses" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="paniclelike" value_original="paniclelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glands subtending each bract 0.</text>
      <biological_entity id="o21743" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o21744" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels present.</text>
      <biological_entity id="o21745" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: sepals 2 (–3), valvate, connate basally;</text>
      <biological_entity id="o21746" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21747" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="3" />
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s11" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5 (–6), distinct, white or pink;</text>
      <biological_entity id="o21748" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21749" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectary extrastaminal, 5 (–6) glands;</text>
      <biological_entity id="o21750" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21751" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="extrastaminal" value_original="extrastaminal" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="6" />
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21752" name="gland" name_original="glands" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens (7–) 8–12 (–14), in 2 whorls, connate into androphore, outer whorl connate basally, inner whorl longer and connate proximally to much of length;</text>
      <biological_entity id="o21753" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21754" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s14" to="8" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="14" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="12" />
        <character constraint="into androphore" constraintid="o21756" is_modifier="false" name="fusion" notes="" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o21755" name="whorl" name_original="whorls" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21756" name="androphore" name_original="androphore" src="d0_s14" type="structure" />
      <biological_entity constraint="outer" id="o21757" name="whorl" name_original="whorl" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21758" name="whorl" name_original="whorl" src="d0_s14" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="proximally to much; much" name="length" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o21754" id="r1783" name="in" negation="false" src="d0_s14" to="o21755" />
    </statement>
    <statement id="d0_s15">
      <text>pistillode absent.</text>
      <biological_entity id="o21759" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21760" name="pistillode" name_original="pistillode" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: sepals 2 (–3), connate basally;</text>
      <biological_entity id="o21761" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21762" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="3" />
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 5 (–6), distinct, white or pink;</text>
      <biological_entity id="o21763" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21764" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="6" />
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>nectary 5 (–6) glands (often inconspicuous);</text>
      <biological_entity id="o21765" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21766" name="nectary" name_original="nectary" src="d0_s18" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="6" />
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21767" name="gland" name_original="glands" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>pistil [3–] 4 (–5) -carpellate;</text>
      <biological_entity id="o21768" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21769" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="[3-]4(-5)-carpellate" value_original="[3-]4(-5)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles [3–] 4 (–5), distinct or connate basally, 2-fid.</text>
      <biological_entity id="o21770" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o21771" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s20" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="5" />
        <character name="quantity" src="d0_s20" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s20" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, tardily dehiscent.</text>
      <biological_entity constraint="fruits" id="o21772" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s21" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds obovoid [subglobose];</text>
      <biological_entity id="o21773" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="obovoid" value_original="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>caruncle absent.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 11.</text>
      <biological_entity id="o21774" name="caruncle" name_original="caruncle" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o21775" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (1 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>introduced; e Asia; introduced also in Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e Asia" establishment_means="introduced" />
        <character name="distribution" value="also in Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The seeds of all three species of Vernicia are pressed for oil, which is used in the production of varnish and high quality paints; of these the tung-oil tree (V. fordii) is the most important commercially. Vernicia montana Loureiro is sometimes cultivated in the southeastern United States, but is not known to be naturalized there. It may be distinguished from V. fordii by its persistent leaves with stalked, cup-shaped glands at the petiole apex and blades mostly 3-lobed; fruits with distinct grooves and ridges; and inflorescences mostly unisexual.</discussion>
  <discussion>Species of Vernicia previously have been included within Aleurites J. R. Forster &amp; G. Forster, but the two genera are now considered distinct and closely related (H. K. Airy Shaw 1967; W. Stuppy et al. 1999). Aleurites may be distinguished by its stellate hairs, 2(–3)-locular ovary, fleshy indehiscent fruit, and 17–32 stamens (versus simple or 2-fid hairs, 3–5-locular ovary, dehiscent fruits, and 7–14 stamens in Vernicia). Aleurites moluccanus (Linnaeus) Willdenow (candlenut or Indian walnut) is occasionally cultivated in Florida and rarely escapes locally, but does not appear to be naturalized there.</discussion>
  <references>
    <reference>Airy Shaw, H. K. 1967. Notes on Malaysian and other Asiatic Euphorbiaceae LXXII. Generic segregation in the affinity of Aleurites J. R. et G. Forster. Kew Bull. 20: 393–395.</reference>
    <reference>Stuppy, W. et al. 1999. A revision of the genera Aleurites J. R. Forst. &amp; G. Forst., Reutealis Airy Shaw and Vernicia Lour. (Euphorbiaceae). Blumea 44: 73–-98.</reference>
  </references>
  
</bio:treatment>