<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/16 13:52:59</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">ZYGOPHYLLACEAE</taxon_name>
    <taxon_name authority="Scopoli" date="1777" rank="genus">KALLSTROEMIA</taxon_name>
    <taxon_name authority="B. L. Turner" date="1950" rank="species">perennans</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>18: 155. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zygophyllaceae;genus kallstroemia;species perennans</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101333</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Kallstroemia</taxon_name>
    <taxon_name authority="L. O. Williams" date="1935" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>22: 49, plate 4. 1935</place_in_publication>
      <other_info_on_pub>not (Bentham) Engler 1890</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus kallstroemia;species hirsuta</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o14758" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to ascending, to 0.2 m, densely hispid with white or yellow bulbous-based hairs and strigose with white antrorse hairs.</text>
      <biological_entity id="o14759" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="0.2" to_unit="m" />
        <character constraint="with hairs" constraintid="o14760" is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character constraint="with hairs" constraintid="o14761" is_modifier="false" name="pubescence" notes="" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o14760" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
      <biological_entity id="o14761" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="antrorse" value_original="antrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves elliptic in outline, 2.5–5 × 2–3 cm;</text>
      <biological_entity id="o14762" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="in outline" constraintid="o14763" is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" notes="" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14763" name="outline" name_original="outline" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipules 3–5 × 1–1.5 mm;</text>
      <biological_entity id="o14764" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 8–10, oblong to ovate, 13–18 × 6–10 mm, middle pairs largest, surfaces densely appressed-hirsute, veins and margins sericeous.</text>
      <biological_entity id="o14765" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s4" to="18" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s4" value="middle" value_original="middle" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o14766" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="appressed-hirsute" value_original="appressed-hirsute" />
      </biological_entity>
      <biological_entity id="o14767" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o14768" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 15–20 mm in flower, 25–36 mm in fruit, shorter than subtending leaves, little thickened distally, bent sharply at base and straight distally.</text>
      <biological_entity id="o14769" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in flower" constraintid="o14770" from="15" from_unit="mm" name="some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o14771" from="25" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="36" to_unit="mm" />
        <character constraint="than subtending leaves" constraintid="o14772" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="distally" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character constraint="at base" constraintid="o14773" is_modifier="false" name="shape" src="d0_s5" value="bent" value_original="bent" />
        <character is_modifier="false" modifier="distally" name="course" notes="" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o14770" name="flower" name_original="flower" src="d0_s5" type="structure" />
      <biological_entity id="o14771" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
      <biological_entity constraint="subtending" id="o14772" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o14773" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 35–45 mm diam.;</text>
      <biological_entity id="o14774" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="diameter" src="d0_s6" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals persistent, lanceolate, 13–15 × 1.5–2.5 mm, 1/2 as long as petals, in flower longer than style, in fruit spreading from base of mature mericarps and longer than fruit body but shorter than beak, margins becoming sharply involute, densely hispid and strigose;</text>
      <biological_entity id="o14775" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
        <character name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
        <character constraint="than style" constraintid="o14778" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="than beak" constraintid="o14783" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14776" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o14777" name="flower" name_original="flower" src="d0_s7" type="structure">
        <character constraint="than style" constraintid="o14778" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14778" name="style" name_original="style" src="d0_s7" type="structure" />
      <biological_entity id="o14779" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character constraint="from base" constraintid="o14780" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o14780" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="than fruit body" constraintid="o14782" is_modifier="false" name="length_or_size" notes="" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14781" name="mericarp" name_original="mericarps" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o14782" name="body" name_original="body" src="d0_s7" type="structure">
        <character constraint="than beak" constraintid="o14783" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14783" name="beak" name_original="beak" src="d0_s7" type="structure" />
      <biological_entity id="o14784" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="becoming sharply" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
      <relation from="o14775" id="r1247" name="in" negation="false" src="d0_s7" to="o14777" />
      <relation from="o14775" id="r1248" name="in" negation="false" src="d0_s7" to="o14779" />
      <relation from="o14780" id="r1249" name="part_of" negation="false" src="d0_s7" to="o14781" />
    </statement>
    <statement id="d0_s8">
      <text>petals not marcescent, 1-colored, usually orange, sometimes pale orange to almost salmon-colored, obovate, 19–26 × 10 mm;</text>
      <biological_entity id="o14785" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s8" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="1-colored" value_original="1-colored" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale orange to almost" value_original="pale orange to almost" />
        <character is_modifier="false" modifier="sometimes; almost" name="coloration" src="d0_s8" value="salmon-colored" value_original="salmon-colored" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s8" to="26" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 2/3 as long as style;</text>
      <biological_entity id="o14786" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character constraint="as-long-as style" constraintid="o14787" name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o14787" name="style" name_original="style" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers orange, ovoid, 1.5 mm;</text>
      <biological_entity id="o14788" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary ovoid, 3 mm diam., hairy;</text>
      <biological_entity id="o14789" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character name="diameter" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style cylindric, 6 mm, 2 times as long as ovary, hispid;</text>
      <biological_entity id="o14790" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="6" value_original="6" />
        <character constraint="ovary" constraintid="o14791" is_modifier="false" name="length" src="d0_s12" value="2 times as long as ovary" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o14791" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigma extending along distal 1/3 of style.</text>
      <biological_entity id="o14792" name="stigma" name_original="stigma" src="d0_s13" type="structure" />
      <biological_entity id="o14793" name="distal 1/3" name_original="distal 1/3" src="d0_s13" type="structure" />
      <biological_entity id="o14794" name="style" name_original="style" src="d0_s13" type="structure" />
      <relation from="o14792" id="r1250" name="extending along" negation="false" src="d0_s13" to="o14793" />
      <relation from="o14792" id="r1251" name="extending along" negation="false" src="d0_s13" to="o14794" />
    </statement>
    <statement id="d0_s14">
      <text>Schizocarps broadly ovoid, 5–6 × 8–10 mm, hispid and strigose;</text>
      <biological_entity id="o14795" name="schizocarp" name_original="schizocarps" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>beak cylindric, 6–10 mm, longer than fruit body, base slightly conic, hirsute;</text>
      <biological_entity id="o14796" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="10" to_unit="mm" />
        <character constraint="than fruit body" constraintid="o14797" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="fruit" id="o14797" name="body" name_original="body" src="d0_s15" type="structure" />
      <biological_entity id="o14798" name="base" name_original="base" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>mericarps 4 × 2.5 mm, abaxially crossridged, ± keeled, sides pitted, adaxial edge straight.</text>
      <biological_entity id="o14799" name="mericarp" name_original="mericarps" src="d0_s16" type="structure">
        <character name="length" src="d0_s16" unit="mm" value="4" value_original="4" />
        <character name="width" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s16" value="crossridged" value_original="crossridged" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o14800" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14801" name="edge" name_original="edge" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Kallstroemia perennans is a rare western Texas endemic, found only in Val Verde County on the Edwards Plateau and in Brewster and Presidio counties in the trans-Pecos. The species is in the Center for Plant Conservation's National Collection of Endangered Plants.</discussion>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug, following spring and summer rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="range_value" modifier="rains" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Somewhat barren limestone or gypseous soils.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soils" modifier="or gypseous" />
      </biological_entity>
    </statement>
  </description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–900 m.</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>