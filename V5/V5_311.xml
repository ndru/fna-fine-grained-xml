<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler,Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/17 21:02:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">152</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <place_of_publication>
      <publication_title>in M. Napier, Encycl. Brit. ed.</publication_title>
      <place_in_publication>7, 5: 99. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily Caryophylloideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20604</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="family">Caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl" date="unknown" rank="subfamily">Silenoideae</taxon_name>
    <taxon_hierarchy>family Caryophyllaceae;subfamily Silenoideae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, biennial, or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or rhizomatous, sometimes stoloniferous.</text>
      <biological_entity id="o180" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, seldom sprawling, decumbent, or prostrate, simple or branched.</text>
      <biological_entity id="o181" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="seldom" name="growth_form_or_orientation" src="d0_s2" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, rarely whorled, connate proximally, petiolate (basal leaves) or often sessile, not stipulate;</text>
      <biological_entity id="o182" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear or subulate to ovate, not succulent or rarely so (Silene).</text>
      <biological_entity id="o183" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
        <character name="texture" src="d0_s4" value="rarely" value_original="rarely" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal cymes, thyrses, fascicles, or capitula, or flowers solitary, axillary;</text>
      <biological_entity id="o184" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o185" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
      <biological_entity id="o186" name="thyrse" name_original="thyrses" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity id="o187" name="capitulum" name_original="capitula" src="d0_s5" type="structure" />
      <biological_entity id="o188" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous, scarious, or absent;</text>
      <biological_entity id="o189" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>involucel bracteoles present or often absent.</text>
      <biological_entity constraint="involucel" id="o190" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present or rarely flowers sessile or subsessile.</text>
      <biological_entity id="o191" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o192" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or seldom unisexual (the species then often dioecious), often conspicuous;</text>
      <biological_entity id="o193" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="seldom" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o194" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o195" name="androecium" name_original="androecium" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, connate (1/4–) 1/2+ their lengths into cup or tube, (1–) 5–40 (–62) mm, apex not hooded or awned;</text>
      <biological_entity id="o196" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" constraint="into tube" constraintid="o198" from="1/4" name="lengths" src="d0_s11" to="1/2" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s11" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s11" to="62" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o197" name="cup" name_original="cup" src="d0_s11" type="structure" />
      <biological_entity id="o198" name="tube" name_original="tube" src="d0_s11" type="structure" />
      <biological_entity id="o199" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals absent or 5, often showy, white to pink or red, usually clawed, auricles absent or sometimes present, coronal appendages sometimes present, blade apex entire or emarginate to 2-fid, sometimes dentate to lacinate;</text>
      <biological_entity id="o200" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s12" value="showy" value_original="showy" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="pink or red" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o201" name="auricle" name_original="auricles" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="coronal" id="o202" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="blade" id="o203" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s12" value="emarginate to 2-fid" value_original="emarginate to 2-fid" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s12" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens (5 or) 10 (absent in pistillate flowers), in 1 or 2 whorls, arising from base of ovary;</text>
      <biological_entity id="o204" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o205" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o206" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
      <relation from="o204" id="r25" name="in" negation="false" src="d0_s13" to="o205" />
      <relation from="o205" id="r26" name="part_of" negation="false" src="d0_s13" to="o206" />
    </statement>
    <statement id="d0_s14">
      <text>staminodes absent or rarely 1–10;</text>
      <biological_entity id="o207" name="staminode" name_original="staminodes" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s14" value="rarely" value_original="rarely" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 1-locular, sometimes 2-locular proximally (Vaccaria), or 3–5-locular (some Silene);</text>
      <biological_entity id="o208" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="sometimes; proximally" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="3-5-locular" value_original="3-5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2–3 (–5) (absent in staminate flowers), distinct;</text>
      <biological_entity id="o209" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s16" to="3" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 2–3 (–5) (absent in staminate flowers).</text>
      <biological_entity id="o210" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits capsules, opening by 4–6 (–10) valves or teeth;</text>
      <biological_entity constraint="fruits" id="o211" name="capsule" name_original="capsules" src="d0_s18" type="structure" />
      <biological_entity id="o212" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o213" name="tooth" name_original="teeth" src="d0_s18" type="structure" />
      <relation from="o211" id="r27" name="opening by" negation="false" src="d0_s18" to="o212" />
      <relation from="o211" id="r28" name="opening by" negation="false" src="d0_s18" to="o213" />
    </statement>
    <statement id="d0_s19">
      <text>carpophore usually present.</text>
      <biological_entity id="o214" name="carpophore" name_original="carpophore" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 4–150 (–500+), reddish to gray or often brown or black, usually reniform and laterally compressed to globose, sometimes oblong or shield-shaped and dorsiventrally compressed;</text>
      <biological_entity id="o215" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="500" upper_restricted="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s20" to="150" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s20" to="gray" />
        <character name="coloration" src="d0_s20" value="often" value_original="often" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="black" value_original="black" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s20" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="laterally compressed" name="shape" src="d0_s20" to="globose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s20" value="shield--shaped" value_original="shield--shaped" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>embryo peripheral and curved, or central and straight.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 7, 10, 12, [13?,] 14, 15, 17, [18].</text>
      <biological_entity id="o216" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="position" src="d0_s21" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" name="course" src="d0_s21" value="curved" value_original="curved" />
        <character is_modifier="false" name="position" src="d0_s21" value="central" value_original="central" />
        <character is_modifier="false" name="course" src="d0_s21" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="x" id="o217" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="7" value_original="7" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="10" value_original="10" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="12" value_original="12" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="[13" value_original="[13" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="14" value_original="14" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="15" value_original="15" />
        <character name="quantity" src="d0_s22" unit=",,,?,] ,,1," value="17" value_original="17" />
        <character name="atypical_quantity" src="d0_s22" unit=",,,?,] ,,1," value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North-temperate regions, Europe (esp. Mediterranean region), Asia (esp. Mediterranean region e to c Asia), Africa (Mediterranean region, Republic of South Africa).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North-temperate regions" establishment_means="native" />
        <character name="distribution" value="Europe (esp. Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Asia (esp. Mediterranean region e to c Asia)" establishment_means="native" />
        <character name="distribution" value="Africa (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Africa (Republic of South Africa)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43d.</number>
  <other_name type="past_name">Caryophylleae</other_name>
  <discussion>Genera 20 or 26, species ca. 1500 (8 genera, 89 species in the flora).</discussion>
  <discussion>Caryophylloideae can be characterized by the presence of sepals connate into a cup or (usually) long tube, clawed petals (often with appendages and auricles), and a lack of stipules. The largest genera in the family [Silene (incl. Lychnis), about 700 species; Dianthus, about 320 species] are in the Caryophylloideae; together with Gypsophila (about 150 species), these three genera include about three-quarters of the species found in the family. Three tribes are often differentiated on calyx venation and number of styles, with two, Caryophylleae and Sileneae, incorporating nearly all of the genera.</discussion>
  <discussion>Caryophylloideae share the caryophyllad type of embryogeny with Alsinoideae and, as postulated by V. Bittrich (1993), the two may form a monophyletic group. Results from preliminary molecular studies by M. Nepokroeff et al. (2002) and R. D. Smissen et al. (2002) reinforce that hypothesis, but the relationships among members of the two subfamilies remain unclear.</discussion>
  <discussion>Most of the molecular work within the subfamily has focused on Sileneae and more specifically on trying to determine whether or not Silene is monophyletic.</discussion>
  <references>
    <reference>Maguire, B. 1950. Studies in the Caryophyllaceae. IV. A synopsis of the North American species of the subfamily Silenoideae. Rhodora 52: 233–245.</reference>
    <reference>Oxelman, B., M. Lidén, R. K. Rabeler, and M. Popp. 2000. A revised generic classification of the tribe Sileneae (Caryophyllaceae). Nordic J. Bot. 21: 743–748.</reference>
  </references>
  
</bio:treatment>