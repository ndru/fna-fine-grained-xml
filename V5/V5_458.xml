<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="(M. E. Jones) Reveal in P. A. Munz" date="1968" rank="variety">ambiguum</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.,</publication_title>
      <place_in_publication>61. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species microthecum;variety ambiguum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060388</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">aureum</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="variety">ambiguum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 719. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species aureum;variety ambiguum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 0.5–5 × 1–8 dm.</text>
      <biological_entity id="o6222" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o6224" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o6225" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems 0.2–1 dm, mostly floccose.</text>
      <biological_entity id="o6226" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o6227" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="1" to_unit="dm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade 0.8–2.5 × (0.2–) 0.3–0.6 (–0.8) cm, densely white or reddish-brown-tomentose abaxially, floccose adaxially, margins rarely revolute.</text>
      <biological_entity id="o6228" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6229" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s3" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="reddish-brown-tomentose" value_original="reddish-brown-tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o6230" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–5 (–12) cm;</text>
      <biological_entity id="o6231" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches tomentose to floccose.</text>
      <biological_entity id="o6232" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 2–2.5 mm, tomentose to floccose.</text>
      <biological_entity id="o6233" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s6" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (1.5–) 2–2.5 (–3) mm;</text>
      <biological_entity id="o6234" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth yellow.</text>
      <biological_entity id="o6235" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 1.5–2 mm.</text>
      <biological_entity id="o6236" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to rocky soil, sagebrush and saltbush communities, pinyon-juniper and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to rocky soil" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="saltbush communities" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1100-)1900-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="1900" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3300" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Yellow-flowered wild buckwheat</other_name>
  <discussion>Variety ambiguum occurs in eastern California, northwestern Nevada, and southeastern Oregon. In portions of its range, var. ambiguum appears to be little more than a yellow-flowered phase of var. laxiflorum. However, where the two occur together, intermediates are rarely found, and limited garden studies indicate that flower color is genetically fixed. Nonflowering specimens from southern Owyhee County, Idaho, have been tentatively assigned to this taxon.</discussion>
  
</bio:treatment>