<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="mention_page">32</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">paronychia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1838" rank="species">setacea</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 170. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus paronychia;species setacea;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paronychia</taxon_name>
    <taxon_name authority="B. L. Turner" date="unknown" rank="species">lundellorum</taxon_name>
    <taxon_hierarchy>genus Paronychia;species lundellorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paronychia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">setacea</taxon_name>
    <taxon_name authority="Chaudhri" date="unknown" rank="variety">longibracteata</taxon_name>
    <taxon_hierarchy>genus Paronychia;species setacea;variety longibracteata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, biennial, or perennial;</text>
      <biological_entity id="o22230" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot filiform to slender.</text>
      <biological_entity id="o22231" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, much-branched distally, 5–20 cm, finely pubescent to puberulent.</text>
      <biological_entity id="o22232" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="finely pubescent" name="pubescence" src="d0_s2" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules lanceolate, 3–8 mm, apex acuminate, entire;</text>
      <biological_entity id="o22233" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22234" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22235" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 5–20 × 0.3–1 mm, leathery, apex minutely cuspidate, glabrous.</text>
      <biological_entity id="o22236" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22237" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o22238" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s4" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cymes terminal and lateral, 5–20+-flowered, dichasial and rather diffuse, forming clusters 3–15 mm wide.</text>
      <biological_entity id="o22239" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-20+-flowered" value_original="5-20+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" modifier="rather" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5-merous, ± short-campanulate, with enlarged hypanthium and calyx widening distally, 2.4–3.2 mm, moderately pubescent proximately with antrorsely appressed to spreading hairs;</text>
      <biological_entity id="o22240" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="short-campanulate" value_original="short-campanulate" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3.2" to_unit="mm" />
        <character constraint="with hairs" constraintid="o22243" is_modifier="false" modifier="moderately" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22241" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o22242" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="width" src="d0_s6" value="widening" value_original="widening" />
      </biological_entity>
      <biological_entity id="o22243" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="antrorsely appressed" is_modifier="true" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
      <relation from="o22240" id="r2481" name="with" negation="false" src="d0_s6" to="o22241" />
      <relation from="o22240" id="r2482" name="with" negation="false" src="d0_s6" to="o22242" />
    </statement>
    <statement id="d0_s7">
      <text>sepals green to tan, midrib and lateral pair of veins absent to evident, oblong, 1.2–1.3 mm, leathery to rigid, margins whitish to translucent, 0.05–0.1 mm wide, scarious, apex terminated by awn, hood ± obscure, narrowly rounded, awn widely divergent, 0.6–1.3 mm, broadly conic in proximal 1/3 with white to yellowish, scabrous spine;</text>
      <biological_entity id="o22244" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="tan" />
      </biological_entity>
      <biological_entity id="o22245" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character constraint="of veins" constraintid="o22246" is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="leathery" name="texture" src="d0_s7" to="rigid" />
      </biological_entity>
      <biological_entity id="o22246" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22247" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s7" to="translucent" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s7" to="0.1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o22248" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o22249" name="awn" name_original="awn" src="d0_s7" type="structure" />
      <biological_entity id="o22250" name="hood" name_original="hood" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o22251" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
        <character constraint="in proximal 1/3" constraintid="o22252" is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22252" name="1/3" name_original="1/3" src="d0_s7" type="structure" />
      <biological_entity id="o22253" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s7" to="yellowish" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o22248" id="r2483" name="terminated by" negation="false" src="d0_s7" to="o22249" />
      <relation from="o22252" id="r2484" name="with" negation="false" src="d0_s7" to="o22253" />
    </statement>
    <statement id="d0_s8">
      <text>staminodes subulate, 0.6–0.7 mm;</text>
      <biological_entity id="o22254" name="staminode" name_original="staminodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 1, cleft in distal 1/2, 0.4–0.6 mm.</text>
      <biological_entity id="o22255" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character constraint="in distal 1/2" constraintid="o22256" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22256" name="1/2" name_original="1/2" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Utricles ovoid-oblong, 1 mm, smooth, glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 64.</text>
      <biological_entity id="o22257" name="utricle" name_original="utricles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid-oblong" value_original="ovoid-oblong" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22258" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone barrens, gravelly or sandy slopes and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone barrens" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Bristle nailwort</other_name>
  <discussion>We could not distinguish Paronychia lundellorum from P. setacea based on features that Turner used for differentiation, nor did we discover any other discriminating characters. The former was described as perennial (versus annual for the latter) and as having “decided pedicellate” (versus sessile) and longer flowers. Recent data (W. Carr, pers. comm.) indicate that it does bloom in the first year. Furthermore, we have observed that the pedicel character is inconsistent and the flower lengths overlap extensively. Further study of the allopatric populations named P. lundellorum is warranted.</discussion>
  
</bio:treatment>