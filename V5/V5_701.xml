<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">348</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Jepson" date="unknown" rank="variety">bahiiforme</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Calif.</publication_title>
      <place_in_publication>1: 425. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety bahiiforme;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060540</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">polyanthum</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">bahiiforme</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 159. 1870 (as bahiaeforme)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species polyanthum;variety bahiiforme;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading mats, 0.8–2 (–2.5) × 3–6 dm.</text>
      <biological_entity id="o26484" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o26485" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, usually 0.5–1.5 dm, tomentose, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o26486" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" modifier="usually" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o26487" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o26486" id="r2938" name="without" negation="false" src="d0_s1" to="o26487" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in rather compact rosettes;</text>
      <biological_entity id="o26488" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26489" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
      </biological_entity>
      <relation from="o26488" id="r2939" name="in" negation="false" src="d0_s2" to="o26489" />
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to oval, 0.5–1.5 (–1.7) × 0.3–0.7 cm, densely white to gray-lanate on both surfaces, margins plane.</text>
      <biological_entity id="o26490" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oval" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.7" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character constraint="on surfaces" constraintid="o26491" is_modifier="false" name="pubescence" src="d0_s3" value="gray-lanate" value_original="gray-lanate" />
      </biological_entity>
      <biological_entity id="o26491" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o26492" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compound-umbellate, branched 2–5 times;</text>
      <biological_entity id="o26493" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-umbellate" value_original="compound-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="" src="d0_s4" value="2-5 times" value_original="2-5 times " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches tomentose, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o26494" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o26495" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o26496" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o26494" id="r2940" name="without" negation="false" src="d0_s5" to="o26495" />
      <relation from="o26495" id="r2941" name="part_of" negation="false" src="d0_s5" to="o26496" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–3 mm, lobes 2–3.5 (–4) mm.</text>
      <biological_entity id="o26497" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26498" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 5–8 mm;</text>
      <biological_entity id="o26499" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o26500" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, mostly serpentine flats and slopes, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to" modifier="gravelly" />
        <character name="habitat" value="sandy to serpentine flats" modifier="mostly" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107bb.</number>
  <other_name type="past_name">bahiaeforme</other_name>
  <other_name type="common_name">Santa Clara sulphur flower</other_name>
  <discussion>Variety bahiiforme occurs in widely scattered locations in the Central Coast Ranges (Colusa, Contra Costa, Monterey, Napa, San Benito, Santa Clara, and Sonoma counties) with a disjunct population in the San Gabriel Mountains of Los Angeles County. The more depauperate relative, var. minus, occurs in the mountains to the east.</discussion>
  
</bio:treatment>