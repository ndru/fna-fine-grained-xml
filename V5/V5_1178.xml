<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="(Meisner) H. Gross" date="1913" rank="section">Echinocaulon</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Int. Géogr. Bot.</publication_title>
      <place_in_publication>4: 27. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section echinocaulon;</taxon_hierarchy>
    <other_info_on_name type="fna_id">316731</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="section">Echinocaulon</taxon_name>
    <place_of_publication>
      <publication_title>in N. Wallich, Pl. Asiat. Rar.</publication_title>
      <place_in_publication>3: 58. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;section Echinocaulon;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually scandent or, rarely, ascending or erect, simple or branched, with recurved prickles.</text>
      <biological_entity id="o16721" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="scandent" value_original="scandent" />
        <character name="growth_form" src="d0_s0" value="," value_original="," />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o16722" name="prickle" name_original="prickles" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o16721" id="r1861" name="with" negation="false" src="d0_s0" to="o16722" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: ocrea chartaceous or foliaceous, margins eciliate or ciliate;</text>
      <biological_entity id="o16723" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o16724" name="ocreum" name_original="ocrea" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o16725" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="eciliate" value_original="eciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole, if present, rarely winged, not auriculate;</text>
      <biological_entity id="o16726" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o16727" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade without dark triangular or lunate blotch adaxially, broadly lanceolate or elliptic to triangular, base truncate to sagittate to cordate, margins entire or hastately lobed.</text>
      <biological_entity id="o16728" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16729" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s3" to="triangular" />
      </biological_entity>
      <biological_entity id="o16730" name="blotch" name_original="blotch" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s3" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o16731" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="sagittate" />
      </biological_entity>
      <biological_entity id="o16732" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="hastately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <relation from="o16729" id="r1862" name="without" negation="false" src="d0_s3" to="o16730" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and axillary, spikelike, paniclelike, or capitate, uninterrupted or, rarely, interrupted proximally.</text>
      <biological_entity id="o16733" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="shape" src="d0_s4" value="paniclelike" value_original="paniclelike" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="uninterrupted" value_original="uninterrupted" />
        <character name="architecture" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers homostylous, articulation swollen or not;</text>
      <biological_entity id="o16734" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o16735" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
        <character name="shape" src="d0_s5" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth campanulate;</text>
      <biological_entity id="o16736" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals 4–5, connate 1/4–1/2 their length;</text>
      <biological_entity id="o16737" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s7" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 5–8;</text>
      <biological_entity id="o16738" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles deciduous, 2–3, included or, rarely, exserted, erect or spreading.</text>
      <biological_entity id="o16739" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character name="position" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, South America, Asia, se Africa, e Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="se Africa" establishment_means="native" />
        <character name="distribution" value="e Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32b.</number>
  <discussion>Species 21 (5 in the flora).</discussion>
  <references>
    <reference>Park, C. W. 1986. Nomenclatural typifications in Polygonum section Echinocaulon (Polygonaceae). Brittonia 38: 394–406.</reference>
    <reference>Park, C. W. 1987. Flavonoid chemistry of Polygonum sect. Echinocaulon: A systematic survey. Syst. Bot. 12: 167–179.</reference>
    <reference>Park, C. W. 1988. Taxonomy of Polygonum section Echinocaulon (Polygonaceae). Mem. New York Bot. Gard. 47: 1–82.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ocreae, at least some, foliaceous, green; perianths fleshy and blue in fruit</description>
      <determination>2 Persicaria perfoliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ocreae chartaceous, brownish; perianths not fleshy and blue in fruit</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ocreae margins truncate, ciliate with bristles 2-4 mm</description>
      <determination>6 Persicaria bungeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Ocreae margins oblique, eciliate or ciliate with bristles to 2.5 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades broadly hastate to hastate-cordate or triangular; tepals 4</description>
      <determination>3 Persicaria arifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades linear-lanceolate to oblong; tepals 5</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles usually glabrous, sometimes with retrorse prickles proximally; leaves petiolate; bases of leaf blades sagittate to cordate; stamens 8</description>
      <determination>4 Persicaria sagittata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Peduncles usually stipitate-glandular; leaves sessile or subsessile; bases of leaf blades cordate to truncate or cuneate; stamens 5</description>
      <determination>5 Persicaria meisneriana</determination>
    </key_statement>
  </key>
</bio:treatment>