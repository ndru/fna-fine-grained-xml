<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="1835" rank="species">compositum</taxon_name>
    <taxon_name authority="Hooker" date="1853" rank="variety">leianthum</taxon_name>
    <place_of_publication>
      <publication_title>Hooker’s J. Bot. Kew Gard. Misc.</publication_title>
      <place_in_publication>5: 264. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species compositum;variety leianthum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060227</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">compositum</taxon_name>
    <taxon_name authority="(S. Watson ex Piper) H. St. John &amp; F. A. Warren" date="unknown" rank="variety">simplex</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species compositum;variety simplex;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–4 (–5) dm.</text>
      <biological_entity id="o5393" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems usually slender, infrequently fistulose, 1–3 dm, usually glabrous, rarely floccose.</text>
      <biological_entity id="o5394" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="infrequently" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate to deltoid, 3–10 (–15) × 2–4 (–4.5) cm.</text>
      <biological_entity id="o5395" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="deltoid" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres glabrous or more commonly weakly glandular-puberulent.</text>
      <biological_entity id="o5396" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="commonly weakly" name="pubescence" src="d0_s3" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 5–6 mm;</text>
      <biological_entity id="o5397" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>perianth usually bright-yellow.</text>
      <biological_entity id="o5398" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly slopes, mixed grassland and sagebrush communities, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="montane conifer woodlands" modifier="oak and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-1600(-2000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="30" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>109b.</number>
  <other_name type="common_name">Smooth arrow-leaf wild buckwheat</other_name>
  <discussion>Variety leianthum is mostly occasional to locally common in eastern Washington, northwestern and west-central Idaho, and northeastern Oregon.</discussion>
  
</bio:treatment>