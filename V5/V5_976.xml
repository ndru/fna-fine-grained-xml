<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="(Torrey) Torrey &amp; A. Gray" date="1870" rank="section">Acanthogonum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 197. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section acanthogonum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">316735</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Torrey" date="unknown" rank="section">Acanthogonum</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 132. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Acanthogonum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="Parry" date="unknown" rank="section">Chorizanthella</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;section Chorizanthella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate to spreading or erect.</text>
      <biological_entity id="o26237" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="spreading or erect" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblanceolate to elliptic or round.</text>
      <biological_entity id="o26238" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="elliptic or round" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not disarticulating at each node.</text>
      <biological_entity id="o26239" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="at node" constraintid="o26240" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o26240" name="node" name_original="node" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: bracts awned or awnless.</text>
      <biological_entity id="o26241" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o26242" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="awned" value_original="awned" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="awnless" value_original="awnless" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres cylindric, campanulate, or urceolate, not ventricose basally, 3-angled, 3-ribbed, 3-toothed, 5-toothed, or occasionally 6-toothed, without membranous or scarious margins;</text>
      <biological_entity id="o26243" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="ventricose" value_original="ventricose" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="5-toothed" value_original="5-toothed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="6-toothed" value_original="6-toothed" />
      </biological_entity>
      <biological_entity id="o26244" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o26243" id="r2911" name="without" negation="false" src="d0_s4" to="o26244" />
    </statement>
    <statement id="d0_s5">
      <text>awns equal, sometimes anterior one longest.</text>
      <biological_entity id="o26245" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="anterior" value_original="anterior" />
        <character is_modifier="false" name="length" src="d0_s5" value="longest" value_original="longest" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1 (–2);</text>
      <biological_entity id="o26246" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth white to rose or yellow, thinly to densely pubescent;</text>
      <biological_entity id="o26247" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose or yellow" />
        <character is_modifier="false" modifier="thinly to densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 3, 6, or 9;</text>
      <biological_entity id="o26248" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character name="quantity" src="d0_s8" value="6" value_original="6" />
        <character name="quantity" src="d0_s8" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments adnate faucially at or near top of floral-tube.</text>
      <biological_entity id="o26249" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="top of floral-tube" constraintid="o26250" is_modifier="false" modifier="faucially" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o26250" name="floral-tube" name_original="floral-tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes brown to black, lenticular or 3-gonous.</text>
      <biological_entity id="o26251" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s10" to="black" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-gonous" value_original="3-gonous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12c.2.</number>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>G. J. Goodman (1934) assigned Chorizanthe polygonoides to Acanthogonum, which previously included C. rigida and C. corrugata (see below). J. L. Reveal and C. B. Hardham (1989b) considered that taxon to represent a section within Chorizanthe, expanded it further to include both C. orcuttiana and C. watsonii, and divided it into five subsections. Section Acanthogonum differs from sect. Ptelosepala by having involucral tubes that are not both 6-ribbed and 6-toothed at the same time.</discussion>
  
</bio:treatment>