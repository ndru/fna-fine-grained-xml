<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">458</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="Goodman" date="1934" rank="species">obovata</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 70. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species obovata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060079</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect to prostrate, (0.5–) 1–3 (–4) × 1–4 (–5) dm, pubescent.</text>
      <biological_entity id="o17042" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="prostrate" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_length" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o17043" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–2 (–3) cm;</text>
      <biological_entity id="o17044" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate, 0.5–2.5 × 0.3–1 cm, thinly pubescent adaxially, soft-hirsute abaxially.</text>
      <biological_entity id="o17045" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="thinly; adaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="soft-hirsute" value_original="soft-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with involucres in open clusters 2–4 (–6) cm diam., greenish or reddish;</text>
      <biological_entity id="o17046" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="in open clusters 2-4(-6) cm" is_modifier="false" name="diam" src="d0_s4" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="diam" src="d0_s4" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o17047" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <relation from="o17046" id="r1896" name="with" negation="false" src="d0_s4" to="o17047" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2–3 at proximal node, usually leaflike, without whorl of sessile bracts about midstem, elliptic, 0.5–1.5 cm × 2–6 (–8) mm, abruptly reduced above proximal node, becoming scalelike, linear, aciculate, acerose, 0.2–1 cm × 1–3 mm, awns straight, 1–2 mm.</text>
      <biological_entity id="o17048" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="at proximal node" constraintid="o17049" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" notes="" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character constraint="above proximal node" constraintid="o17053" is_modifier="false" modifier="abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="shape" notes="" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aciculate" value_original="aciculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17049" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o17050" name="whorl" name_original="whorl" src="d0_s5" type="structure" />
      <biological_entity id="o17051" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17052" name="midstem" name_original="midstem" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o17053" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o17054" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o17048" id="r1897" name="without" negation="false" src="d0_s5" to="o17050" />
      <relation from="o17050" id="r1898" name="part_of" negation="false" src="d0_s5" to="o17051" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–10+, grayish, urceolate, slightly ventricose basally, 3–4 mm, slightly corrugate, without scarious or membranous margins, thinly to densely pubescent;</text>
      <biological_entity id="o17055" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="10" upper_restricted="false" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="thinly to densely" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17056" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o17055" id="r1899" name="without" negation="false" src="d0_s6" to="o17056" />
    </statement>
    <statement id="d0_s7">
      <text>teeth erect to spreading, unequal, 1–2 mm;</text>
      <biological_entity id="o17057" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns straight or uncinate with longer anterior one straight, mostly 1 mm, others uncinate, 0.5–1 mm.</text>
      <biological_entity id="o17058" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character constraint="with longer anterior one" constraintid="o17059" is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character modifier="mostly" name="some_measurement" notes="" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="longer anterior" id="o17059" name="one" name_original="one" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o17060" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers exserted;</text>
      <biological_entity id="o17061" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth bicolored with floral-tube greenish white to white and tepals white to pink, cylindric, 4–4.5 (–5) mm, sparsely pubescent;</text>
      <biological_entity id="o17062" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character constraint="with floral-tube" constraintid="o17063" is_modifier="false" name="coloration" src="d0_s10" value="bicolored" value_original="bicolored" />
      </biological_entity>
      <biological_entity id="o17063" name="floral-tube" name_original="floral-tube" src="d0_s10" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s10" to="white" />
      </biological_entity>
      <biological_entity id="o17064" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate 1/2 their length, dimorphic, obovate, those of outer whorl spreading, 2 times longer than those of inner whorl, rounded or slightly obcordate apically, those of inner whorl erect, narrower, fimbriate apically;</text>
      <biological_entity id="o17065" name="tepal" name_original="tepals" src="d0_s11" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character constraint="of inner whorl" constraintid="o17067" is_modifier="false" name="length_or_size" src="d0_s11" value="2 times longer than" value_original="2 times longer than" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly; apically" name="shape" src="d0_s11" value="obcordate" value_original="obcordate" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="width" src="d0_s11" value="narrower" value_original="narrower" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17066" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o17067" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o17068" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <relation from="o17065" id="r1900" name="part_of" negation="false" src="d0_s11" to="o17066" />
      <relation from="o17065" id="r1901" name="part_of" negation="false" src="d0_s11" to="o17068" />
    </statement>
    <statement id="d0_s12">
      <text>stamens (6–) 9, mostly included;</text>
      <biological_entity id="o17069" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s12" to="9" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="9" value_original="9" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 4–4.5 mm, glabrous;</text>
      <biological_entity id="o17070" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow to golden, oblong, 0.9–1.1 mm.</text>
      <biological_entity id="o17071" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="golden" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s14" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, globose-lenticular, 3–3.5 mm. 2n = 38, 40, 42.</text>
      <biological_entity id="o17072" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose-lenticular" value_original="globose-lenticular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17073" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or calcareous soils, mixed grassland, coastal scrub, or chaparral communities, pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Spoon-sepal spineflower</other_name>
  <discussion>Chorizanthe obovata is found in the Coast Ranges. The whitish flowers quickly distinguish it from C. palmeri and the other reddish-flowered members of this complex. Immature plants can be confused with C. staticoides; the floral features readily separate the two species.</discussion>
  
</bio:treatment>