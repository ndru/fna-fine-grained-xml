<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="mention_page">455</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">cuspidata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">cuspidata</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species cuspidata;variety cuspidata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060069</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants decumbent to prostrate, 0.5–1.5 × 0.5–10 dm.</text>
      <biological_entity id="o16286" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="decumbent" name="growth_form_or_orientation" src="d0_s0" to="prostrate" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 0.5–2 (–2.5) × 0.3–0.7 cm.</text>
      <biological_entity id="o16287" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s1" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 1–2 mm, with or without pinkish, thin, scarious margins, awns uncinate apically.</text>
      <biological_entity id="o16288" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity id="o16289" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="pinkish" value_original="pinkish" />
        <character is_modifier="true" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o16290" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="pinkish" value_original="pinkish" />
        <character is_modifier="true" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="true" name="texture" src="d0_s2" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o16288" id="r1809" modifier="apically" name="with or without" negation="false" src="d0_s2" to="o16289" />
      <relation from="o16288" id="r1810" modifier="apically" name="with or without" negation="false" src="d0_s2" to="o16290" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers 2–2.5 mm.</text>
      <biological_entity id="o16291" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Achenes 2–2.5 mm.</text>
      <biological_entity id="o16292" name="achene" name_original="achenes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy places, coastal scrub communities, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy places" />
        <character name="habitat" value="coastal scrub communities" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8a.</number>
  <other_name type="common_name">San Francisco spineflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety cuspidata is localized in the greater San Francisco area, where it is rapidly becoming rare due to increased development. No collections of San Francisco spineflower have been made in Marin or Alameda counties since 1881. Populations may still be seen in San Mateo County, but their numbers have decreased greatly since the 1950s.</discussion>
  
</bio:treatment>