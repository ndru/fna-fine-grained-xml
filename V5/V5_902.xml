<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Reveal &amp; Ertter" date="1977" rank="genus">GOODMANIA</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>28: 427, fig. 1. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus goodmania;</taxon_hierarchy>
    <other_info_on_name type="etymology">For George Jones Goodman, 1904–1999, authority on Chorizanthe</other_info_on_name>
    <other_info_on_name type="fna_id">113927</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o4290" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o4291" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems arising directly from the root, spreading to decumbent or prostrate, solid, not fistulose or disarticulating into ringlike segments, pubescent or glabrous.</text>
      <biological_entity id="o4292" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from root" constraintid="o4293" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="into segments" constraintid="o4294" is_modifier="false" name="architecture" src="d0_s2" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4293" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o4294" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ringlike" value_original="ringlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually persistent through anthesis, basal and cauline, opposite;</text>
      <biological_entity id="o4295" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="through anthesis , basal and cauline , opposite" is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o4296" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade broadly elliptic or round to reniform, becoming linear, margins entire, awn-tipped at proximal nodes.</text>
      <biological_entity id="o4297" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s5" to="reniform" />
        <character is_modifier="false" modifier="becoming" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o4298" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="at proximal nodes" constraintid="o4299" is_modifier="false" name="architecture" src="d0_s5" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4299" name="node" name_original="nodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, cymose;</text>
      <biological_entity id="o4300" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches dichotomous, not brittle or disarticulating into segments, round, usually glabrous;</text>
      <biological_entity id="o4301" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s7" value="brittle" value_original="brittle" />
        <character constraint="into segments" constraintid="o4302" is_modifier="false" name="architecture" src="d0_s7" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4302" name="segment" name_original="segments" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts 2, distinct, somewhat leaflike and seemingly succulent, mucronate, pubescent to glabrate.</text>
      <biological_entity id="o4303" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="seemingly" name="texture" src="d0_s8" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s8" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent.</text>
      <biological_entity id="o4304" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucral-bracts in 1 whorl of 5, distinct, narrowly lanceolate, awn-tipped.</text>
      <biological_entity id="o4305" name="involucral-bract" name_original="involucral-bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" notes="" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o4306" name="whorl" name_original="whorl" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
      <relation from="o4305" id="r479" name="in" negation="false" src="d0_s10" to="o4306" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers (6–) 9–15 per involucral cluster at any single time during anthesis;</text>
      <biological_entity id="o4307" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s11" to="9" to_inclusive="false" />
        <character char_type="range_value" constraint="at time" constraintid="o4308" from="9" name="quantity" src="d0_s11" to="15" />
      </biological_entity>
      <biological_entity id="o4308" name="time" name_original="time" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth yellow, broadly campanulate when open, narrowly urceolate when closed, woolly-tomentose abaxially;</text>
      <biological_entity id="o4309" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="when open" name="shape" src="d0_s12" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="when closed" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="woolly-tomentose" value_original="woolly-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals 6, connate proximally, monomorphic, entire apically;</text>
      <biological_entity id="o4310" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 9;</text>
      <biological_entity id="o4311" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments basally adnate, glabrous;</text>
      <biological_entity id="o4312" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers yellow, ovate.</text>
      <biological_entity id="o4313" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes included, light-brown, not winged, 3-gonous, glabrous.</text>
      <biological_entity id="o4314" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="included" value_original="included" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o4315" name="seed" name_original="seeds" src="d0_s18" type="structure" />
      <biological_entity id="o4316" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Yellow spinecape</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Goodmania is allied to Eriogonum subg. Ganysma but its point of origin is obscure. The most logical point to suggest within subg. Ganysma is somewhere around the E. inflatum complex. The bright green color of the flowering stems and inflorescence branches, the pubescent yellow flowers, and the near-glabrous condition of the plant body are somewhat akin to those found in Stenogonum. Each of these segregate genera is confined to arid regions in the American West, all appear to have rather recent origins, and each seems to be exhibiting a type of variation different from the norm seen among the annual wild buckwheats.</discussion>
  
</bio:treatment>