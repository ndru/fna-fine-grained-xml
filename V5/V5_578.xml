<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Porter ex S. Watson" date="1877" rank="species">kennedyi</taxon_name>
    <taxon_name authority="(Brandegee) Reveal in P. A. Munz" date="1968" rank="variety">purpusii</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.,</publication_title>
      <place_in_publication>67. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species kennedyi;variety purpusii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060353</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Brandegee" date="unknown" rank="species">purpusii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 457. 1899 (as purpusi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species purpusii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">kennedyi</taxon_name>
    <taxon_name authority="(Brandegee) Munz" date="unknown" rank="subspecies">purpusii</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species kennedyi;subspecies purpusii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, densely matted, 0.4–1.2 × 1–3 dm.</text>
      <biological_entity id="o5700" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="length" src="d0_s0" to="1.2" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades elliptic, (0.25–) 0.3–0.6 × 0.15–0.35 cm, white-tomentose on both surfaces, margins not revolute.</text>
      <biological_entity id="o5701" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.25" from_unit="cm" name="atypical_length" src="d0_s1" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s1" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.15" from_unit="cm" name="width" src="d0_s1" to="0.35" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o5702" is_modifier="false" name="pubescence" src="d0_s1" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o5702" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
      <biological_entity id="o5703" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s1" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Scapes 0.4–1 cm, glabrous or rarely floccose.</text>
      <biological_entity id="o5704" name="scape" name_original="scapes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s2" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres 1.5–2 mm, glabrous or sparsely tomentose.</text>
      <biological_entity id="o5705" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 2–2.5 mm.</text>
      <biological_entity id="o5706" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Achenes 2.5–3 mm.</text>
      <biological_entity id="o5707" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats and slopes, mixed sagebrush and grassland communities, pinyon, juniper, and Jeffrey pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="mixed sagebrush" />
        <character name="habitat" value="grassland communities" />
        <character name="habitat" value="pinyon" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="jeffrey pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>73e.</number>
  <other_name type="common_name">Purpus wild buckwheat</other_name>
  <discussion>Variety purpusii differs from other varieties of the species in its distribution disjunct to the north and east, and its more arid, desert-like habitat. It is common in Inyo and southern Mono counties of California, with scattered populations known near Inyokern in Kern County. In Nevada, it is known only from the Orchard Springs area of Esmeralda County. The bright white tomentum of the leaves is distinctive, and the low mats are sometimes used for dried decorations. Purpus wild buckwheat is an excellent rock-garden plant.</discussion>
  
</bio:treatment>