<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal" date="1981" rank="species">soredium</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>41: 229, fig. 1. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species soredium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060496</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, scapose, 0.01–0.1 × 1–3 dm, densely white-tomentose, whitish.</text>
      <biological_entity id="o4390" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.01" from_unit="dm" name="length" src="d0_s0" to="0.1" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" value_original="whitish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems matted, with persistent leaf-bases, up to 1/5 height of plant;</text>
      <biological_entity id="o4391" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o4392" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/5 height of plant" />
      </biological_entity>
      <relation from="o4391" id="r489" name="with" negation="false" src="d0_s1" to="o4392" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o4393" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems scapelike, erect, slender, solid, not fistulose, 0.02–0.15 dm, tomentose.</text>
      <biological_entity id="o4394" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.02" from_unit="dm" name="some_measurement" src="d0_s3" to="0.15" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, fasciculate and sheathing nearly entire length of stem;</text>
      <biological_entity id="o4395" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4396" name="stem" name_original="stem" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.05–0.2 (–0.3) cm, tomentose;</text>
      <biological_entity id="o4397" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="some_measurement" src="d0_s5" to="0.2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly elliptic to narrowly oblong, 0.2–0.7 × (0.07–) 0.1–0.25 cm, densely white-tomentose, margins plane.</text>
      <biological_entity id="o4398" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s6" to="narrowly oblong" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s6" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.07" from_unit="cm" name="atypical_width" src="d0_s6" to="0.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.25" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity id="o4399" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, 0.5–1 cm;</text>
      <biological_entity id="o4400" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches absent;</text>
      <biological_entity id="o4401" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 6–8, scalelike, lanceolate to narrowly triangular, 1.3–1.6 mm.</text>
      <biological_entity id="o4402" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s9" to="8" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="narrowly triangular" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o4403" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 4–6 per cluster, turbinate, 2–2.5 × 1.3–1.5 mm, rigid;</text>
      <biological_entity id="o4404" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="4" name="quantity" src="d0_s11" to="6" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s11" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth (4–) 5, erect, 0.5–0.6 mm.</text>
      <biological_entity id="o4405" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers (1.5–) 2–2.5 mm;</text>
      <biological_entity id="o4406" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white, glabrous;</text>
      <biological_entity id="o4407" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, slightly dimorphic, oblanceolate to ovate or obovate, those of outer whorl 1.5–2 mm wide, those of inner whorl 1–1.5 mm wide;</text>
      <biological_entity id="o4408" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="ovate or obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o4409" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o4410" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o4408" id="r490" name="part_of" negation="false" src="d0_s15" to="o4409" />
      <relation from="o4408" id="r491" name="part_of" negation="false" src="d0_s15" to="o4410" />
    </statement>
    <statement id="d0_s16">
      <text>stamens included, 2–2.5 mm;</text>
      <biological_entity id="o4411" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments sparsely pubescent proximally.</text>
      <biological_entity id="o4412" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 2–2.5 mm, glabrous.</text>
      <biological_entity id="o4413" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky limestone slopes, mixed saltbush and sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky limestone slopes" />
        <character name="habitat" value="mixed saltbush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Frisco wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum soredium is a rare and localized species known only from the San Francisco Mountains of Beaver County. It is considered a “sensitive” species in Utah, and occasionally is seen in cultivation.</discussion>
  
</bio:treatment>