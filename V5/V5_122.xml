<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Nuttall) Ikonnikov" date="1973" rank="species">congesta</taxon_name>
    <taxon_name authority="(Rydberg) R. L. Hartman &amp; Rabeler" date="2004" rank="variety">cephaloidea</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 239. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species congesta;variety cephaloidea;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">cephaloidea</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>39: 316. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species cephaloidea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">congesta</taxon_name>
    <taxon_name authority="(Rydberg) Maguire" date="unknown" rank="variety">cephaloidea</taxon_name>
    <taxon_hierarchy>genus Arenaria;species congesta;variety cephaloidea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 20–40 (–50) cm, glabrous.</text>
      <biological_entity id="o15856" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal blades erect to somewhat spreading, filiform, 3–8 (–14) cm × 0.4–0.7 mm, herbaceous.</text>
      <biological_entity id="o15857" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o15858" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect to somewhat" value_original="erect to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="14" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s1" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences capitate, often pyramidal, dense cymes;</text>
      <biological_entity id="o15859" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
      <biological_entity id="o15860" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts closely enveloping sepals.</text>
      <biological_entity id="o15861" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o15862" name="sepal" name_original="sepals" src="d0_s3" type="structure" />
      <relation from="o15861" id="r1766" modifier="closely" name="enveloping" negation="false" src="d0_s3" to="o15862" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels ± absent, mostly shorter than 0.1 mm, glabrous.</text>
      <biological_entity id="o15863" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character modifier="mostly shorter than" name="some_measurement" src="d0_s4" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sepals weakly to conspicuously 1–3-veined, 4–5 mm, apex acute to acuminate.</text>
      <biological_entity id="o15864" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly to conspicuously" name="architecture" src="d0_s5" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15865" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, gravelly woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly woods" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4b.</number>
  <other_name type="common_name">Sharptip sandwort</other_name>
  <discussion>The pyramidal inflorescence of var. cephaloidea bears a fanciful resemblance to the grass Dactylis glomerata Linnaeus and some members of Carex Linnaeus sect. Ovales Kunth.</discussion>
  
</bio:treatment>