<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rumex Linnaeus Sect.. Rumex" date="unknown" rank="section">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1935" rank="species">pycnanthus</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>38: 372. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section rumex;species pycnanthus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060797</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">subalpinus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 720. 1895 (as subalpina),</place_in_publication>
      <other_info_on_pub>not (Schur) Simonkai 1886</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Rumex;species subalpinus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Osterhout" date="unknown" rank="species">densiflorus</taxon_name>
    <taxon_name authority="(Rechinger f.) Á. Löve" date="unknown" rank="subspecies">pycnanthus</taxon_name>
    <taxon_hierarchy>genus Rumex;species densiflorus;subspecies pycnanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous or indistinctly papillose-pubescent, with fusiform or creeping horizontal rhizome.</text>
      <biological_entity id="o11944" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="indistinctly" name="pubescence" src="d0_s0" value="papillose-pubescent" value_original="papillose-pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11945" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="fusiform" value_original="fusiform" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <relation from="o11944" id="r1327" name="with" negation="false" src="d0_s0" to="o11945" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched above middle (only in inflorescence), 60–100 cm.</text>
      <biological_entity id="o11946" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="above middle" constraintid="o11947" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11947" name="middle" name_original="middle" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocreae deciduous or partially persistent at maturity;</text>
      <biological_entity id="o11948" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11949" name="ocrea" name_original="ocreae" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character constraint="at maturity" is_modifier="false" modifier="partially" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade normally oblong-lanceolate, 20–45 × 8–10 cm, normally more than 3 times as long as wide, base broadly cuneate, truncate, or weakly cordate, margins entire, flat or indistinctly crisped, apex obtuse or broadly acute.</text>
      <biological_entity id="o11950" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11951" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="normally" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s3" to="45" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3+" value_original="3+" />
      </biological_entity>
      <biological_entity id="o11952" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o11953" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o11954" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, occupying distal 1/2 of stem, often dense, narrowly paniculate.</text>
      <biological_entity id="o11955" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="often" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="narrowly" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o11956" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o11955" id="r1328" name="occupying" negation="false" src="d0_s4" to="o11956" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels articulated in proximal 1/2, occasionally almost near base, filiform, 3–11 mm, articulation indistinct or weakly evident.</text>
      <biological_entity id="o11957" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="in proximal 1/2" constraintid="o11958" is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11958" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o11959" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o11960" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" modifier="weakly" name="prominence" src="d0_s5" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o11957" id="r1329" modifier="occasionally almost; almost" name="near" negation="false" src="d0_s5" to="o11959" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 10–20 in whorls;</text>
      <biological_entity id="o11961" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o11962" from="10" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o11962" name="whorl" name_original="whorls" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inner tepals ovate-deltoid or ovate-triangular, 4–7 × 3–5 mm, widest at or near base, base truncate or weakly emarginate, margins erose to minutely dentate at least near base, apex narrowly acute;</text>
      <biological_entity constraint="inner" id="o11963" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-triangular" value_original="ovate-triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character constraint="at base" constraintid="o11964" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o11964" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o11965" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s7" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o11966" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="erose to minutely" value_original="erose to minutely" />
        <character constraint="near base" constraintid="o11967" is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o11967" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o11968" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tubercles absent.</text>
      <biological_entity id="o11969" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes deep brown to reddish-brown, 3–4 × 1.5–2.2 mm. 2n = 120.</text>
      <biological_entity id="o11970" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="deep" value_original="deep" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s9" to="reddish-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11971" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams and rivers in montane, subalpine, and alpine zones</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" constraint="in montane , subalpine , and alpine zones" />
        <character name="habitat" value="rivers" constraint="in montane , subalpine , and alpine zones" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine zones" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <discussion>Rumex pycnanthus was reported (as R. subalpinus M. E. Jones) from White Pine County, Nevada (Mont E. Lewis 1973) and may occur in that state; according to J. T. Kartesz (1987, vol. 1), that record was based on misidentification of R. californicus.</discussion>
  
</bio:treatment>