<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="M. E. Jones" date="1893" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>4: 281. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species bicolor;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060193</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microthecum</taxon_name>
    <taxon_name authority="(M. E. Jones) S. Stokes" date="unknown" rank="subspecies">bicolor</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species microthecum;subspecies bicolor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, compact, not scapose, 0.2–0.8 × 0.5–2 (–3) dm, reddish or tannish-tomentose, reddish.</text>
      <biological_entity id="o14689" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="length" src="d0_s0" to="0.8" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tannish-tomentose" value_original="tannish-tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to slightly spreading, often with persistent leaf-bases, up to 1/4 height of plant;</text>
      <biological_entity id="o14690" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="slightly spreading" />
      </biological_entity>
      <biological_entity id="o14691" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/4 height of plant" />
      </biological_entity>
      <relation from="o14690" id="r1604" modifier="often" name="with" negation="false" src="d0_s1" to="o14691" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems compact;</text>
      <biological_entity constraint="caudex" id="o14692" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems spreading to somewhat erect, slender, solid, not fistulose, 0.05–0.2 dm, tomentose.</text>
      <biological_entity id="o14693" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading to somewhat" value_original="spreading to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="some_measurement" src="d0_s3" to="0.2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, 1 per node or fasciculate;</text>
      <biological_entity id="o14694" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character constraint="per node" constraintid="o14695" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s4" value="fasciculate" value_original="fasciculate" />
      </biological_entity>
      <biological_entity id="o14695" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1–0.2 cm, tomentose to floccose;</text>
      <biological_entity id="o14696" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.2" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s5" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear-oblanceolate to narrowly elliptic, 0.5–1.7 (–2) × 0.1–0.2 (–0.3) cm, densely white-tomentose abaxially, slightly less so and often reddish or tannish adaxially, margins revolute.</text>
      <biological_entity id="o14697" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s6" to="narrowly elliptic" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s6" to="0.2" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="slightly less; less; often" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="tannish" value_original="tannish" />
      </biological_entity>
      <biological_entity id="o14698" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences umbellate to cymose, compact, rarely reduced to single involucre, 0.5–1 × 0.5–1.5 cm;</text>
      <biological_entity id="o14699" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="umbellate" name="architecture" src="d0_s7" to="cymose" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s7" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" notes="" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14700" name="involucre" name_original="involucre" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentose;</text>
      <biological_entity id="o14701" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, linear, 0.7–1.3 mm.</text>
      <biological_entity id="o14702" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles erect, 0.1–0.3 (–0.4) cm, tomentose to floccose.</text>
      <biological_entity id="o14703" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s10" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s10" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate-campanulate, 2–4 × 1.5–3 mm, tomentose to floccose or glabrous;</text>
      <biological_entity id="o14704" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o14705" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s11" to="floccose or glabrous" />
      </biological_entity>
      <biological_entity id="o14705" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.4–0.7 mm.</text>
      <biological_entity id="o14706" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2.5–4 (–4.5) mm;</text>
      <biological_entity id="o14707" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white, glabrous;</text>
      <biological_entity id="o14708" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, dimorphic, those of outer whorl broadly obovate to nearly orbiculate, (2–) 2.5–3 mm wide, those of inner whorl oblanceolate to narrowly elliptic, 1–1.5 mm wide;</text>
      <biological_entity id="o14709" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s15" to="nearly orbiculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="narrowly elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o14710" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o14711" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o14709" id="r1605" name="part_of" negation="false" src="d0_s15" to="o14710" />
      <relation from="o14709" id="r1606" name="part_of" negation="false" src="d0_s15" to="o14711" />
    </statement>
    <statement id="d0_s16">
      <text>stamens long-exserted, 3–5 mm;</text>
      <biological_entity id="o14712" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="long-exserted" value_original="long-exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments sparsely pilose proximally or glabrous.</text>
      <biological_entity id="o14713" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 3–3.5 mm, glabrous.</text>
      <biological_entity id="o14714" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Silty, sandy, or heavy clay washes, flats, and slopes, saltbush and blackbrush communities, juniper or pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="silty" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="heavy clay washes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="blackbrush communities" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2000(-2300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Pretty wild buckwheat</other_name>
  <discussion>Eriogonum bicolor is common from Castle Valley and the San Rafael Swells of Carbon and Emery counties, Utah, eastward to Grand Valley of Grand County, Utah, and Mesa County, Colorado. It occurs to the south in Garfield, San Juan, Sevier, and Wayne counties of Utah, especially in Canyonlands National Park and the Grand Staircase-Escalante National Monument. The species is worthy of cultivation, although being a slow-growing perennial of rather specialized soils it is a challenge. Seed germination in garden plots is reasonably successful; transplanting is difficult.</discussion>
  
</bio:treatment>