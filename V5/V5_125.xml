<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Nuttall) Ikonnikov" date="1973" rank="species">congesta</taxon_name>
    <taxon_name authority="(Rydberg) Dorn" date="2001" rank="variety">lithophila</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Wyoming ed.</publication_title>
      <place_in_publication>3, 376. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species congesta;variety lithophila;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060135</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">subcongesta</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="variety">lithophila</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 148. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species subcongesta;variety lithophila;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="(Rydberg) Rydberg" date="unknown" rank="species">lithophila</taxon_name>
    <taxon_hierarchy>genus Arenaria;species lithophila;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 10–30 cm, glabrous.</text>
      <biological_entity id="o17100" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal blades erect to somewhat spreading, filiform, 3–6 cm × 0.3–0.6 mm, herbaceous.</text>
      <biological_entity id="o17101" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o17102" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect to somewhat" value_original="erect to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s1" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences proliferating, ± loose cymes, secondary axes to 2 cm;</text>
      <biological_entity id="o17103" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="proliferating" value_original="proliferating" />
      </biological_entity>
      <biological_entity id="o17104" name="cyme" name_original="cymes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o17105" name="axis" name_original="axes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts often not closely enveloping sepals.</text>
      <biological_entity id="o17106" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o17107" name="sepal" name_original="sepals" src="d0_s3" type="structure" />
      <relation from="o17106" id="r1903" modifier="often not closely" name="enveloping" negation="false" src="d0_s3" to="o17107" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 1–5 mm, glabrous.</text>
      <biological_entity id="o17108" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sepals weakly to conspicuously 1–3-veined, 3.5–5 mm, apex obtuse or rounded.</text>
      <biological_entity id="o17109" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly to conspicuously" name="architecture" src="d0_s5" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17110" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky, sagebrush slopes and hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" modifier="open" />
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.; Colo., Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4e.</number>
  <other_name type="common_name">Loosehead sandwort</other_name>
  <discussion>M. F. Baad (1969) suggested the possible relationship of var. lithophila to Eremogone capillaris, but he followed B. Maguire (1947) in retaining it in E. congesta, noting the existence of intermediates between var. congesta and var. lithophila.</discussion>
  
</bio:treatment>