<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">149</other_info_on_meta>
    <other_info_on_meta type="illustration_page">145</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Mackenzie" date="1914" rank="genus">geocarpon</taxon_name>
    <taxon_name authority="Mackenzie" date="1914" rank="species">minimum</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>14: 67. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus geocarpon;species minimum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220005528</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems greenish brown or strongly suffused with red, 1–4 cm × 0.5 mm or less;</text>
      <biological_entity id="o1083" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s0" value="suffused with red" value_original="suffused with red" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s0" to="4" to_unit="cm" />
        <character name="width" src="d0_s0" unit="mm" value="0.5" value_original="0.5" />
        <character name="width" src="d0_s0" value="less" value_original="less" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal nodes 1–3 mm apart, internodes zigzag, distal nodes 5–10 mm apart.</text>
      <biological_entity constraint="proximal" id="o1084" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o1085" name="internode" name_original="internodes" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o1086" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 3–4 mm, margins somewhat involute.</text>
      <biological_entity id="o1087" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1088" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape_or_vernation" src="d0_s2" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescence bracts connate, strongly red or purple-tinged, keeled, triangular, 2.5–3.5 mm, apex acute.</text>
      <biological_entity constraint="inflorescence" id="o1089" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1090" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers funnelform-campanulate, 3–4 mm;</text>
      <biological_entity id="o1091" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="funnelform-campanulate" value_original="funnelform-campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepal lobes ± erect to slightly spreading, subequal;</text>
      <biological_entity constraint="sepal" id="o1092" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="less erect" name="orientation" src="d0_s5" to="slightly spreading" />
        <character is_modifier="false" name="size" src="d0_s5" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary greenish, ca. 3 mm, apex narrow at anthesis, minutely glandular-toothed or retuse.</text>
      <biological_entity id="o1093" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1094" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s6" value="glandular-toothed" value_original="glandular-toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules opening along 3 wirelike valve margins.</text>
      <biological_entity id="o1095" name="capsule" name_original="capsules" src="d0_s7" type="structure" />
      <biological_entity constraint="valve" id="o1096" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s7" value="wirelike" value_original="wirelike" />
      </biological_entity>
      <relation from="o1095" id="r108" name="opening along" negation="false" src="d0_s7" to="o1096" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds 0.3–0.5 mm.</text>
      <biological_entity id="o1097" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone glades and alkali barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone glades" />
        <character name="habitat" value="alkali barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., La., Mo., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Tiny tim</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Geocarpon minimum is known from fewer than 35 sites, about one-third of which have relatively large, vigorous populations. The species is listed as federally threatened. In Missouri, it is found only in shallow depressions in slightly tilted sandstone strata within sandstone glade plant communities. In Arkansas and Louisiana, it is found in saline-alkaline soils on the edge of highly localized, surficial concentrations of sodium and magnesium, locally known as “slick spots”; a similar “saline barren” hosts the recently discovered (2004) Texas population. These austere and nearly barren patches of mineral soil are scattered across savannalike formations classified as saline soil prairies.</discussion>
  <discussion>Geocarpon minimum is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>