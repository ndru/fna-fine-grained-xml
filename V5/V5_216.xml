<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) Fenzl in C. F. von Ledebour" date="1842" rank="species">dicranoides</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. von Ledebour, Fl. Ross.</publication_title>
      <place_in_publication>1: 395. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species dicranoides;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060930</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cherleria</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">dicranoides</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>1: 63. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cherleria;species dicranoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Maguire" date="unknown" rank="species">chamissonis</taxon_name>
    <taxon_hierarchy>genus Arenaria;species chamissonis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) Hultén" date="unknown" rank="species">dicranoides</taxon_name>
    <taxon_hierarchy>genus Arenaria;species dicranoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, dioecious, forming dense cushions to 10 cm or more diam., with branching caudex, arising from taproot.</text>
      <biological_entity id="o14197" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character char_type="range_value" from="forming dense cushions" name="character" src="d0_s0" to="diam" />
        <character constraint="from taproot" constraintid="o14200" is_modifier="false" name="orientation" notes="" src="d0_s0" value="arising" value_original="arising" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14198" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o14199" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o14200" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o14197" id="r1541" name="forming" negation="false" src="d0_s0" to="o14198" />
      <relation from="o14197" id="r1542" name="with" negation="false" src="d0_s0" to="o14199" />
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, 4-angled, 1–4 cm, glabrous;</text>
      <biological_entity id="o14201" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches erect or ascending, thickly clothed with marcescent leaves.</text>
      <biological_entity id="o14202" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o14203" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="marcescent" value_original="marcescent" />
      </biological_entity>
      <relation from="o14202" id="r1543" modifier="thickly" name="clothed with" negation="false" src="d0_s2" to="o14203" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile;</text>
      <biological_entity id="o14204" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades oblanceolate to obovate or elliptic, 3–5 × 1–1.5 mm, succulent, base cuneate, margins entire, apex acute or abruptly acuminate to obtuse, glabrous;</text>
      <biological_entity constraint="basal" id="o14205" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o14206" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o14207" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14208" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="abruptly acuminate" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline shorter.</text>
      <biological_entity constraint="cauline" id="o14209" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary-flowered in axils of foliage leaves;</text>
      <biological_entity id="o14210" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character constraint="in axils" constraintid="o14211" is_modifier="false" name="architecture" src="d0_s6" value="solitary-flowered" value_original="solitary-flowered" />
      </biological_entity>
      <biological_entity id="o14211" name="axil" name_original="axils" src="d0_s6" type="structure" />
      <biological_entity constraint="foliage" id="o14212" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o14211" id="r1544" name="part_of" negation="false" src="d0_s6" to="o14212" />
    </statement>
    <statement id="d0_s7">
      <text>bract 1, foliaceous, ca. 1 mm.</text>
      <biological_entity id="o14213" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–5 mm, glabrous.</text>
      <biological_entity id="o14214" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers unisexual, 3–4 mm diam.;</text>
      <biological_entity id="o14215" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, 3-veined, keeled, 2.5–3 mm, margins narrow, apex acute, glabrous or with sparse, short, glandular pubescence;</text>
      <biological_entity id="o14216" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14217" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o14218" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="with sparse , short , glandular pubescence" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals absent;</text>
      <biological_entity id="o14219" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10, shorter than sepals;</text>
      <biological_entity id="o14220" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
        <character constraint="than sepals" constraintid="o14221" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14221" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>styles 3, erect, becoming outwardly curved, ca. 1 mm;</text>
      <biological_entity id="o14222" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="becoming outwardly" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>staminate flowers with brownish, peglike, conspicuous nectaries alternating with and attached to base of stamens;</text>
      <biological_entity id="o14223" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character constraint="to base" constraintid="o14225" is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o14224" name="nectary" name_original="nectaries" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s14" value="peglike" value_original="peglike" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o14225" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o14226" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <relation from="o14223" id="r1545" name="with" negation="false" src="d0_s14" to="o14224" />
      <relation from="o14225" id="r1546" name="part_of" negation="false" src="d0_s14" to="o14226" />
    </statement>
    <statement id="d0_s15">
      <text>pistillate flowers with well-developed but nonfunctional stamens and nectaries.</text>
      <biological_entity id="o14227" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="nonfunctional" id="o14228" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="true" name="development" src="d0_s15" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity id="o14229" name="nectary" name_original="nectaries" src="d0_s15" type="structure" />
      <relation from="o14227" id="r1547" name="with" negation="false" src="d0_s15" to="o14228" />
      <relation from="o14227" id="r1548" name="with" negation="false" src="d0_s15" to="o14229" />
    </statement>
    <statement id="d0_s16">
      <text>Capsules straw colored, broadly ovoid, ca. 3 × 2 mm, ca. equaling sepals, apex obtuse, opening by 3 valves, each of which splits into 2;</text>
      <biological_entity id="o14230" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="straw colored" value_original="straw colored" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character name="length" src="d0_s16" unit="mm" value="3" value_original="3" />
        <character name="width" src="d0_s16" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14231" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o14232" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o14233" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o14234" name="split" name_original="splits" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <relation from="o14232" id="r1549" name="opening by" negation="false" src="d0_s16" to="o14233" />
    </statement>
    <statement id="d0_s17">
      <text>carpophore absent.</text>
      <biological_entity id="o14235" name="carpophore" name_original="carpophore" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 1, brown, broadly reniform with thickened rim, ca. 1.1 mm diam., finely verrucate.</text>
      <biological_entity id="o14237" name="rim" name_original="rim" src="d0_s18" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s18" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>2n = 26.</text>
      <biological_entity id="o14236" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character constraint="with rim" constraintid="o14237" is_modifier="false" modifier="broadly" name="shape" src="d0_s18" value="reniform" value_original="reniform" />
        <character name="diameter" notes="" src="d0_s18" unit="mm" value="1.1" value_original="1.1" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s18" value="verrucate" value_original="verrucate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14238" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arctic screes, fellfields, gravelly tundra, rocky knolls on wide variety of rock types</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arctic screes" />
        <character name="habitat" value="fellfields" />
        <character name="habitat" value="gravelly tundra" />
        <character name="habitat" value="rocky knolls" constraint="on wide variety of rock types" />
        <character name="habitat" value="wide variety" constraint="of rock types" />
        <character name="habitat" value="rock types" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska; Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Chamisso’s starwort</other_name>
  <discussion>Stellaria dicranoides is of uncertain generic position. Many workers have placed it in the genus Arenaria. The absence of petals deprives us of a key character separating Stellaria from Arenaria. The ovate capsule with its three valves, each tardily dehiscent into two, suggests Arenaria or Minuartia. However, the chromosome number of 2n = 26 is more often associated with Stellaria. The single large seed, which fills the capsule, is unusual. In its floral structure, including its large nectaries and unisexual flowers, S. dicranoides closely resembles the European M. (Cherleria) sedoides (Linnaeus) Hiern. In fact, Chamisso, who first described this species, placed it in the genus Cherleria.</discussion>
  
</bio:treatment>