<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Poiret) Fenzl" date="1833" rank="species">capillaris</taxon_name>
    <taxon_name authority="(Maguire) R. L. Hartman &amp; Rabeler" date="2004" rank="variety">americana</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 239. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species capillaris;variety americana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060128</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Poiret" date="unknown" rank="species">capillaris</taxon_name>
    <taxon_name authority="Maguire" date="unknown" rank="subspecies">americana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>74: 41. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species capillaris;subspecies americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="(Maguire) Ikonnikov" date="unknown" rank="species">capillaris</taxon_name>
    <taxon_name authority="(Maguire) R. J. Davis" date="unknown" rank="variety">americana</taxon_name>
    <taxon_hierarchy>genus Arenaria;species capillaris;variety americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eremogone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>genus Eremogone;species americana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (3–) 5–15 (–25) cm, glabrous and glaucous proximally, stipitate-glandular distally.</text>
      <biological_entity id="o27037" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pedicels stipitate-glandular.</text>
      <biological_entity id="o27038" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals stipitate-glandular;</text>
      <biological_entity id="o27039" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o27040" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ovary often with uniseriate, 3–8-celled hairs in proximal 1/2 or throughout.</text>
      <biological_entity id="o27041" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o27042" name="ovary" name_original="ovary" src="d0_s3" type="structure" />
      <biological_entity id="o27043" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s3" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="3-8-celled" value_original="3-8-celled" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27044" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o27042" id="r3001" name="with" negation="false" src="d0_s3" to="o27043" />
      <relation from="o27043" id="r3002" name="in" negation="false" src="d0_s3" to="o27044" />
    </statement>
    <statement id="d0_s4">
      <text>Capsules often with uniseriate, 3–8-celled hairs in distal 1/2 or throughout.</text>
      <biological_entity id="o27046" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s4" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="3-8-celled" value_original="3-8-celled" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27047" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <relation from="o27045" id="r3003" name="with" negation="false" src="d0_s4" to="o27046" />
      <relation from="o27046" id="r3004" name="in" negation="false" src="d0_s4" to="o27047" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 22.</text>
      <biological_entity id="o27045" name="capsule" name_original="capsules" src="d0_s4" type="structure" />
      <biological_entity constraint="2n" id="o27048" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, talus slopes, aspen woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="aspen woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Idaho, Mont., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <other_name type="common_name">Fescue sandwort</other_name>
  <discussion>M. F. Baad (1969) reported populations from Idaho, Nevada, Oregon, and Wyoming that he considered to be intermediate between var. americana and Eremogone kingii var. glabrescens, and one from central Washington that he considered to be intermediate between var. americana and E. congesta var. prolifera.</discussion>
  
</bio:treatment>