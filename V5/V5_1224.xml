<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John G. Packer,Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">600</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="genus">KOENIGIA</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>1: 3, 35. 1767</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>12, 2: 71, 104. 1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus koenigia;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Johann Gerhard König, 1827–1785, pupil of Linnaeus</other_info_on_name>
    <other_info_on_name type="fna_id">117203</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o28274" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent, ascending, or erect, glabrous.</text>
      <biological_entity id="o28275" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline, alternate or subopposite, petiolate;</text>
      <biological_entity id="o28276" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subopposite" value_original="subopposite" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ocrea persistent, chartaceous;</text>
      <biological_entity id="o28277" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate-ovate to orbiculate, margins entire.</text>
      <biological_entity id="o28278" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate-ovate" name="shape" src="d0_s5" to="orbiculate" />
      </biological_entity>
      <biological_entity id="o28279" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, paniclelike or cymelike, not pedunculate.</text>
      <biological_entity id="o28280" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="paniclelike" value_original="paniclelike" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cymelike" value_original="cymelike" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels absent or present.</text>
      <biological_entity id="o28281" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, 3–10 per ocreate fascicle, bases not stipelike;</text>
      <biological_entity id="o28282" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o28283" from="3" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o28283" name="fascicle" name_original="fascicle" src="d0_s8" type="structure" />
      <biological_entity id="o28284" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="stipelike" value_original="stipelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth nonaccrescent, greenish, often tinged white or pink distally, narrowly campanulate, glabrous or occasionally with scattered glands;</text>
      <biological_entity id="o28285" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="nonaccrescent" value_original="nonaccrescent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="tinged white" value_original="tinged white" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s9" value="occasionally" value_original="occasionally" />
      </biological_entity>
      <biological_entity id="o28286" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o28285" id="r3161" name="with" negation="false" src="d0_s9" to="o28286" />
    </statement>
    <statement id="d0_s10">
      <text>tepals 3 [4], distinct, sepaloid, monomorphic;</text>
      <biological_entity id="o28287" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s10" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sepaloid" value_original="sepaloid" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens (1–) 3 [–5];</text>
      <biological_entity id="o28288" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s11" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="5" />
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct, free, glabrous;</text>
      <biological_entity id="o28289" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers white or yellowish, ovate to elliptic;</text>
      <biological_entity id="o28290" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2 (–3), erect, distinct;</text>
      <biological_entity id="o28291" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="3" />
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas capitate.</text>
      <biological_entity id="o28292" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes included or barely exserted, light-brown or brown to black, unwinged, unevenly 2-gonous, rarely 3-gonous, glabrous.</text>
      <biological_entity id="o28293" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character is_modifier="false" modifier="barely" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s16" to="black" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="unwinged" value_original="unwinged" />
        <character is_modifier="false" modifier="unevenly" name="shape" src="d0_s16" value="2-gonous" value_original="2-gonous" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o28294" name="seed" name_original="seeds" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>x = 7.</text>
      <biological_entity id="o28295" name="embryo" name_original="embryo" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o28296" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alpine, arctic, and circumpolar, n North America, s South America, n Europe, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Alpine" establishment_means="native" />
        <character name="distribution" value="arctic" establishment_means="native" />
        <character name="distribution" value="and circumpolar" establishment_means="native" />
        <character name="distribution" value="n North America" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>The five other species of Koenigia are endemic to high mountains of southeastern Asia, primarily the Himalayas (O. Hedberg 1997). K. Haraldson (1978) and L.-P. Ronse Decraene and J. R. Akeroyd (1988) placed Koenigia close to Aconogonon based on morphological data. Preliminary molecular data seem to support that relationship (A. S. Lamb Frye and K. A. Kron 2003).</discussion>
  <references>
    <reference>Hedberg, O. 1997. The genus Koenigia L. emend. Hedberg (Polygonaceae). Bot. J. Linn. Soc. 124: 295–330.</reference>
    <reference>Löve, Á. and P. Sarkar. 1957. Chromosomes and relationships of Koenigia islandica. Canad. J. Bot. 35: 507–514.</reference>
  </references>
  
</bio:treatment>