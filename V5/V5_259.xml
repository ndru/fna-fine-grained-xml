<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="1754" rank="genus">minuartia</taxon_name>
    <taxon_name authority="(Rydberg) House" date="1921" rank="species">macrantha</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>7: 132. 1921</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus minuartia;species macrantha;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060639</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsinopsis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">macrantha</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 407. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Alsinopsis;species macrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsinanthe</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber" date="unknown" rank="species">macrantha</taxon_name>
    <taxon_hierarchy>genus Alsinanthe;species macrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Maguire" date="unknown" rank="species">filiorum</taxon_name>
    <taxon_hierarchy>genus Arenaria;species filiorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson" date="unknown" rank="species">macrantha</taxon_name>
    <taxon_hierarchy>genus Arenaria;species macrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="(Wahlenberg) Smith" date="unknown" rank="species">rubella</taxon_name>
    <taxon_name authority="(Maguire) S. L. Welsh" date="unknown" rank="variety">filiorum</taxon_name>
    <taxon_hierarchy>genus Arenaria;species rubella;variety filiorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Minuartia</taxon_name>
    <taxon_name authority="(Maguire) McNeill" date="unknown" rank="species">filiorum</taxon_name>
    <taxon_hierarchy>genus Minuartia;species filiorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose or mat-forming.</text>
      <biological_entity id="o12008" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots occasionally filiform or often woody, somewhat thickened to moderately stout.</text>
      <biological_entity id="o12009" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="often; often" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character char_type="range_value" from="thickened" modifier="somewhat" name="size" src="d0_s1" to="moderately stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to procumbent, green, 2–15 cm, glabrous, internodes of all stems 0.3–1 (–2) times as long as leaves.</text>
      <biological_entity id="o12010" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12011" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o12013" is_modifier="false" name="length" src="d0_s2" value="0.3-1(-2) times as long as leaves" />
      </biological_entity>
      <biological_entity id="o12012" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o12013" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o12011" id="r1333" name="part_of" negation="false" src="d0_s2" to="o12012" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves moderately to tightly overlapping (proximal cauline), variably spaced, progressively more so distally (distal cauline), connate proximally, with loose, scarious sheath 0.3–0.8 mm;</text>
      <biological_entity id="o12014" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately to tightly" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="variably" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="progressively; distally; proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o12015" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character is_modifier="true" name="texture" src="d0_s3" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o12014" id="r1334" name="with" negation="false" src="d0_s3" to="o12015" />
    </statement>
    <statement id="d0_s4">
      <text>blade straight to slightly outcurved, green, flat, to 3-angled distally, 1–3-veined, midvein more prominent than 2 lateral-veins, subulate to linear, 5–10 × 0.5–1.2 mm, flexuous, margins not thickened, scarious, smooth, apex green, rounded, thickened and navicular, shiny, glabrous;</text>
      <biological_entity id="o12016" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="outcurved" value_original="outcurved" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s4" to="3-angled" />
        <character char_type="range_value" from="flat" modifier="distally" name="shape" src="d0_s4" to="3-angled" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o12017" name="midvein" name_original="midvein" src="d0_s4" type="structure">
        <character constraint="than 2 lateral-veins" constraintid="o12018" is_modifier="false" name="prominence" src="d0_s4" value="more prominent" value_original="more prominent" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subulate to linear" value_original="subulate to linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o12018" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12019" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12020" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s4" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary leaves present among proximal cauline leaves.</text>
      <biological_entity constraint="axillary" id="o12021" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal cauline" id="o12022" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o12021" id="r1335" name="present among" negation="false" src="d0_s5" to="o12022" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences solitary flowers, terminal, or 2–5 (–8) -flowered, open cymes;</text>
      <biological_entity id="o12023" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-5(-8)-flowered" value_original="2-5(-8)-flowered" />
      </biological_entity>
      <biological_entity id="o12024" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o12025" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts broadly subulate, herbaceous or scarious-margined proximally.</text>
      <biological_entity id="o12026" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s7" value="scarious-margined" value_original="scarious-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicles 0.2–1.5 cm, glabrous.</text>
      <biological_entity id="o12027" name="pedicle" name_original="pedicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium disc-shaped;</text>
      <biological_entity id="o12028" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12029" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="disc--shaped" value_original="disc--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals strongly 3-veined, ovate to lanceolate (herbaceous portion lanceolate), 3.5–5 mm, to 5.5 mm in fruit, apex green or purple in part, sharply acute to acuminate, not hooded, glabrous;</text>
      <biological_entity id="o12030" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12031" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s10" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o12032" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12032" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o12033" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character constraint="in part" constraintid="o12034" is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character char_type="range_value" from="sharply acute" name="shape" notes="" src="d0_s10" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12034" name="part" name_original="part" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>petals oblong to obovate, 0.7–1.8 times as long as sepals, apex rounded to blunt, entire.</text>
      <biological_entity id="o12035" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12036" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="obovate" />
        <character constraint="sepal" constraintid="o12037" is_modifier="false" name="length" src="d0_s11" value="0.7-1.8 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o12037" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o12038" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s11" to="blunt" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules on stipe ca. 0.2 mm, broadly ovoid, 3–3.8 mm, shorter than sepals.</text>
      <biological_entity id="o12039" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.8" to_unit="mm" />
        <character constraint="than sepals" constraintid="o12041" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12040" name="stipe" name_original="stipe" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity id="o12041" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <relation from="o12039" id="r1336" name="on" negation="false" src="d0_s12" to="o12040" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds black, suborbiculate with radicle prolonged to rounded beak, somewhat compressed, 0.7–1 mm, tuberculate;</text>
      <biological_entity id="o12042" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character constraint="with radicle" constraintid="o12043" is_modifier="false" name="shape" src="d0_s13" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="somewhat" name="shape" notes="" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o12043" name="radicle" name_original="radicle" src="d0_s13" type="structure">
        <character is_modifier="false" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o12044" name="beak" name_original="beak" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tubercles low, rounded.</text>
      <biological_entity id="o12045" name="tubercle" name_original="tubercles" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, often limestone, areas, spruce-fir forests, alpine lake shores, tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="limestone" modifier="often" />
        <character name="habitat" value="areas" />
        <character name="habitat" value="spruce-fir forests" />
        <character name="habitat" value="lake shores" modifier="alpine" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">House’s stitchwort</other_name>
  <other_name type="common_name">large-flower sandwort</other_name>
  <discussion>B. Maguire (1958) segregated Minuartia filiorum (as Arenaria filiorum) from M. macrantha on the basis of habit (annual or at most a weak perennial), 3–7 flowers per inflorescence, and petals shorter than the sepals. Some populations may be distinguished using those features; the number of flowers per inflorescence is more variable than Maguire noted, and the seeds of the plants are identical with those of typical M. macrantha. We concur with W. A. Weber’s herbarium annotations that M. filiorum and M. macrantha are conspecific.</discussion>
  
</bio:treatment>