<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="S. Watson" date="1877" rank="species">wheeleri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 272. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species wheeleri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060104</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="R. Hoffmann" date="unknown" rank="species">insularis</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;species insularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect to spreading, 0.5–2 (–2.5) × 1–2 dm, thinly pubescent.</text>
      <biological_entity id="o21102" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o21103" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–3 cm;</text>
      <biological_entity id="o21104" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to oblong, 0.5–2 × 0.2–0.6 cm, thinly pubescent adaxially, tomentose abaxially.</text>
      <biological_entity id="o21105" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="thinly; adaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences mostly flat-topped, openly branched, greenish to reddish;</text>
      <biological_entity id="o21106" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s4" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts persistent, 2, usually leaflike at proximal nodes and similar to leaf-blades, short-petiolate, oblong, 0.5–1.2 cm × 2–4 mm, sessile, reduced and scalelike at distal nodes, linear, acicular, often acerose, 0.1–0.5 cm × 0.5–1 mm, awns straight, 0.5–1 mm.</text>
      <biological_entity id="o21107" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character constraint="at proximal nodes" constraintid="o21108" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="at distal nodes" constraintid="o21110" is_modifier="false" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="length" src="d0_s5" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21108" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o21109" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o21110" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o21111" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o21107" id="r2359" name="to" negation="false" src="d0_s5" to="o21109" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres in dense terminal clusters with 1 at node of dichotomies, reddish, cylindric, not ventricose, 2–2.5 mm, corrugate, without scarious or membranous margins, thinly pubescent with stoutish, recurved hairs;</text>
      <biological_entity id="o21112" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character constraint="with hairs" constraintid="o21117" is_modifier="false" modifier="thinly" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o21113" name="cluster" name_original="clusters" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character constraint="at node" constraintid="o21114" modifier="with" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o21114" name="node" name_original="node" src="d0_s6" type="structure" />
      <biological_entity id="o21115" name="dichotomy" name_original="dichotomies" src="d0_s6" type="structure" />
      <biological_entity id="o21116" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o21117" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s6" value="stoutish" value_original="stoutish" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o21112" id="r2360" name="in" negation="false" src="d0_s6" to="o21113" />
      <relation from="o21114" id="r2361" name="part_of" negation="false" src="d0_s6" to="o21115" />
      <relation from="o21112" id="r2362" name="without" negation="false" src="d0_s6" to="o21116" />
    </statement>
    <statement id="d0_s7">
      <text>teeth spreading, unequal, 0.3–0.8 (–1) mm, with 3 longer ones more erect than 3 shorter and less-prominent ones;</text>
      <biological_entity id="o21118" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="more erect than" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="more erect than" name="prominence" src="d0_s7" value="less-prominent" value_original="less-prominent" />
      </biological_entity>
      <biological_entity constraint="longer" id="o21119" name="one" name_original="ones" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character modifier="more erect than" name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <relation from="o21118" id="r2363" name="with" negation="false" src="d0_s7" to="o21119" />
    </statement>
    <statement id="d0_s8">
      <text>awns uncinate, 0.3–0.5 mm.</text>
      <biological_entity id="o21120" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers exserted;</text>
      <biological_entity id="o21121" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth white or rose or red with white lobes, cylindric, 2.5–3 mm, glabrous except for few scattered hairs ca. midlength along midrib abaxially;</text>
      <biological_entity id="o21122" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="rose" value_original="rose" />
        <character constraint="with lobes" constraintid="o21123" is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character constraint="except-for hairs" constraintid="o21124" is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21123" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21124" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="scattered" value_original="scattered" />
        <character constraint="along midrib" constraintid="o21125" is_modifier="false" name="position" src="d0_s10" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o21125" name="midrib" name_original="midrib" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>tepals connate 1/2 their length, monomorphic to slightly dimorphic, oblong, rounded apically, those of outer whorl usually slightly broader and longer than those of inner whorl;</text>
      <biological_entity id="o21126" name="tepal" name_original="tepals" src="d0_s11" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21127" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <relation from="o21126" id="r2364" name="part_of" negation="false" src="d0_s11" to="o21127" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 6, included;</text>
      <biological_entity id="o21128" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 1.5–2 mm, glabrous;</text>
      <biological_entity id="o21129" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers pink to red, oblong, 0.3–0.4 mm.</text>
      <biological_entity id="o21130" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="red" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, lenticular, 2.5–3 mm.</text>
      <biological_entity id="o21131" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky slopes, coastal scrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky slopes" />
        <character name="habitat" value="coastal scrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400(-600) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Wheeler’s spineflower</other_name>
  <discussion>Chorizanthe wheeleri is a rare insular endemic known only from Santa Cruz and Santa Rosa islands.</discussion>
  
</bio:treatment>