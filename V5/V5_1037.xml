<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">507</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Small" date="1895" rank="species">fascicularis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>22: 367, plate 246. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species fascicularis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060779</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">verticillatus</taxon_name>
    <taxon_name authority="(Small) Á. Löve" date="unknown" rank="subspecies">fascicularis</taxon_name>
    <taxon_hierarchy>genus Rumex;species verticillatus;subspecies fascicularis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock [and, according to Rechinger f. (1937), with fusiform-incrassate root fibers].</text>
      <biological_entity id="o15727" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15728" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o15727" id="r1742" name="with" negation="false" src="d0_s0" to="o15728" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or decumbent, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 50–60 (–70) cm.</text>
      <biological_entity id="o15729" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o15730" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o15731" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <relation from="o15729" id="r1743" modifier="usually" name="producing" negation="false" src="d0_s1" to="o15730" />
      <relation from="o15729" id="r1744" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o15731" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades with lateral-veins forming angle of ca. 80° with midvein especially near base, ovate or ovate-elliptic, 10–25 × 4–12 cm, usually ca. 2 times as long as wide, fleshy, coriaceous, base rounded or truncate-cuneate, occasionally indistinctly cordate, margins entire, flat, apex acute.</text>
      <biological_entity id="o15732" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s2" to="12" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o15733" name="lateral-vein" name_original="lateral-veins" src="d0_s2" type="structure" />
      <biological_entity id="o15734" name="angle" name_original="angle" src="d0_s2" type="structure" />
      <biological_entity id="o15735" name="midvein" name_original="midvein" src="d0_s2" type="structure" />
      <biological_entity id="o15736" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o15737" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate-cuneate" value_original="truncate-cuneate" />
        <character is_modifier="false" modifier="occasionally indistinctly" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o15738" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o15739" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o15732" id="r1745" name="with" negation="false" src="d0_s2" to="o15733" />
      <relation from="o15733" id="r1746" name="forming" negation="false" src="d0_s2" to="o15734" />
      <relation from="o15732" id="r1747" modifier="80°" name="with" negation="false" src="d0_s2" to="o15735" />
      <relation from="o15732" id="r1748" modifier="80°" name="with" negation="false" src="d0_s2" to="o15736" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/3–1/2 of stem, usually lax, interrupted in proximal part, broadly paniculate.</text>
      <biological_entity id="o15740" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character constraint="in proximal part" constraintid="o15742" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o15741" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15742" name="part" name_original="part" src="d0_s3" type="structure" />
      <relation from="o15740" id="r1749" name="occupying" negation="false" src="d0_s3" to="o15741" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal part, distinctly thickened distally 8–13 mm, (2.5–) 3–4 times as long as inner tepals, articulation slightly swollen.</text>
      <biological_entity id="o15743" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in proximal part" constraintid="o15744" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" modifier="distinctly; distally" name="size_or_width" notes="" src="d0_s4" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
        <character constraint="tepal" constraintid="o15745" is_modifier="false" name="length" src="d0_s4" value="(2.5-)3-4 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15744" name="part" name_original="part" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o15745" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o15746" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–20 in whorls;</text>
      <biological_entity id="o15747" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o15748" from="10" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o15748" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals orbiculate or rounded-triangular, 4–5 × 4–5 mm, base truncate or subcordate, margins entire, or rarely indistinctly erose, apex acute or acuminate (with broadly triangular tip);</text>
      <biological_entity constraint="inner" id="o15749" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-triangular" value_original="rounded-triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15750" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o15751" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely indistinctly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely indistinctly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o15752" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles 3, equal or subequal, minutely punctate and/or rugose in proximal part.</text>
      <biological_entity id="o15753" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s7" value="punctate" value_original="punctate" />
        <character constraint="in proximal part" constraintid="o15754" is_modifier="false" name="relief" src="d0_s7" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15754" name="part" name_original="part" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark-brown, 2–2.5 (–3) × 1.8–2.5 mm. 2n = 60.</text>
      <biological_entity id="o15755" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15756" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, marshes, wet meadows, shores of lakes and rivers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="shores" constraint="of lakes and rivers" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rivers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Rumex fascicularis was mentioned for North Carolina (Á. Löve 1986). It is closely related to and sometimes treated as a subspecies of R. verticillatus.</discussion>
  
</bio:treatment>