<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="species">ovalifolium</taxon_name>
    <taxon_name authority="Reveal" date="1989" rank="variety">pansum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>66: 259. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species ovalifolium;variety pansum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060445</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–5 dm wide.</text>
      <biological_entity id="o17026" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades usually elliptic, (0.6–) 1–2 (–2.5) cm, densely tomentose, margins not brownish.</text>
      <biological_entity id="o17027" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s1" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_distance" src="d0_s1" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s1" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o17028" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Scapes erect, (5–) 7–20 cm, thinly tomentose.</text>
      <biological_entity id="o17029" name="scape" name_original="scapes" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences umbellate, 1–5 (–7) × (1.5–) 2–5 cm, thinly tomentose;</text>
      <biological_entity id="o17030" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="umbellate" value_original="umbellate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches absent.</text>
      <biological_entity id="o17031" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 1 per node, 4–5 mm.</text>
      <biological_entity id="o17032" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character constraint="per node" constraintid="o17033" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17033" name="node" name_original="node" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2.5–5 (–7) mm;</text>
      <biological_entity id="o17034" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth white.</text>
      <biological_entity id="o17035" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, sagebrush communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="conifer" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>98f.</number>
  <other_name type="common_name">Branched cushion wild buckwheat</other_name>
  <discussion>Variety pansum occurs in two disjunct series of scattered populations. One is in central Idaho (Blaine, Boise, Custer, Elmore, Lemhi, and Valley counties); the second is in western Montana (Lewis and Clark, Lincoln, Missoula, Powell, Sanders, and Silver Bow counties). This variety bridges the morphologic gap between E. ovalifolium and E. strictum var. proliferum. The inflorescence of var. pansum is umbellate with branches up to 3 cm long, but never compoundly branched as in E. strictum. The umbellate condition becomes obvious only in late anthesis or during early fruit-set, so immature plants of var. pansum might be mistaken for var. purpureum, large specimens of var. depressum, or short-scaped plants of var. ochroleucum. Still, careful observation of such specimens will show an early branching condition, although this can be obscured by bracts subtending the inflorescence or an abundance of early flowers in the numerous involucres. The branched cushion wild buckwheat is certainly worth consideration as an ornamental.</discussion>
  
</bio:treatment>