<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1857" rank="species">leptocladon</taxon_name>
    <taxon_name authority="(Eastwood) Reveal" date="1966" rank="variety">ramosissimum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Utah Acad. Sci.</publication_title>
      <place_in_publication>42: 289. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species leptocladon;variety ramosissimum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060366</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">ramosissimum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 6: 322. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species ramosissimum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">effusum</taxon_name>
    <taxon_name authority="(Small) S. Stokes" date="unknown" rank="subspecies">pallidum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species effusum;subspecies pallidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–8 × 5–15 dm.</text>
      <biological_entity id="o10370" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems white-tomentose to floccose.</text>
      <biological_entity id="o10371" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="white-tomentose" name="pubescence" src="d0_s1" to="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear-lanceolate to narrowly oblong, 1.5–3.5 × (0.2–) 0.3–1 cm, margins plane or infrequently revolute.</text>
      <biological_entity id="o10372" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10373" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="narrowly oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s2" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10374" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="infrequently" name="shape" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescence branches tomentose, rarely floccose.</text>
      <biological_entity constraint="inflorescence" id="o10375" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Perianths white.</text>
      <biological_entity id="o10376" name="perianth" name_original="perianths" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>White or infrequently red blow sand on flats, washes and slopes, mixed grassland, saltbush, ephedra, and sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="white" />
        <character name="habitat" value="infrequently red blow" />
        <character name="habitat" value="sand" constraint="on flats , washes and slopes , mixed grassland , saltbush , ephedra , and sagebrush communities , pinyon-juniper woodlands" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="ephedra" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <other_name type="common_name">San Juan wild buckwheat</other_name>
  <discussion>Variety ramosissimum is the more common of the expressions in the species. It occurs in Emery, Garfield, San Juan, and Wayne counties in Utah, and in Montezuma County, Colorado. It is common in Apache, western Coconino, and Navajo counties, Arizona, and in San Juan County, New Mexico.</discussion>
  
</bio:treatment>