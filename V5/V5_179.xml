<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">75</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Thuillier" date="1799" rank="species">glomeratum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Env. Paris ed.</publication_title>
      <place_in_publication>2, 226. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species glomeratum;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242000277</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Suksdorf" date="unknown" rank="species">acutatum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species acutatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">fulvum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species fulvum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, with slender taproots.</text>
      <biological_entity id="o17351" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="slender" id="o17352" name="taproot" name_original="taproots" src="d0_s0" type="structure" />
      <relation from="o17351" id="r1919" name="with" negation="false" src="d0_s0" to="o17352" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, branched, 5–45 cm, hairy, glandular at least distally, rarely eglandular;</text>
      <biological_entity id="o17353" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>small axillary tufts of leaves absent.</text>
      <biological_entity constraint="axillary" id="o17354" name="tuft" name_original="tufts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o17355" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o17354" id="r1920" name="consist_of" negation="false" src="d0_s2" to="o17355" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not marcescent, ± sessile;</text>
      <biological_entity id="o17356" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="condition" src="d0_s3" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 5–20 (–30) × 2–8 (–15) mm, apex apiculate, covered with spreading, white, long hairs;</text>
      <biological_entity id="o17357" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17358" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o17359" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
      </biological_entity>
      <relation from="o17358" id="r1921" name="covered with" negation="false" src="d0_s4" to="o17359" />
    </statement>
    <statement id="d0_s5">
      <text>basal with blade oblanceolate or obovate, narrowed proximally, sometimes spatulate;</text>
      <biological_entity constraint="basal" id="o17360" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" notes="" src="d0_s5" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o17361" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <relation from="o17360" id="r1922" name="with" negation="false" src="d0_s5" to="o17361" />
    </statement>
    <statement id="d0_s6">
      <text>cauline with blade broadly ovate or elliptic-ovate.</text>
      <biological_entity constraint="cauline" id="o17362" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o17363" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic-ovate" value_original="elliptic-ovate" />
      </biological_entity>
      <relation from="o17362" id="r1923" name="with" negation="false" src="d0_s6" to="o17363" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–50-flowered, aggregated into dense, cymose clusters or in more-open dichasia;</text>
      <biological_entity id="o17364" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-50-flowered" value_original="3-50-flowered" />
        <character constraint="into dense , cymose clusters or in dichasia" constraintid="o17365" is_modifier="false" name="arrangement" src="d0_s7" value="aggregated" value_original="aggregated" />
      </biological_entity>
      <biological_entity id="o17365" name="dichasium" name_original="dichasia" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="more-open" value_original="more-open" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts: proximal herbaceous, distal lanceolate, apex acute, with long, mainly eglandular hairs.</text>
      <biological_entity id="o17366" name="bract" name_original="bracts" src="d0_s8" type="structure" />
      <biological_entity constraint="proximal" id="o17367" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17368" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o17369" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o17370" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="true" modifier="mainly" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o17369" id="r1924" name="with" negation="false" src="d0_s8" to="o17370" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect to spreading, often arcuate distally, 0.1–5 mm, shorter than capsule, glandular-pubescent.</text>
      <biological_entity id="o17371" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="spreading" />
        <character is_modifier="false" modifier="often; distally" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character constraint="than capsule" constraintid="o17372" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o17372" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals green, rarely dark-red tipped, lanceolate, 4–5 mm, margins narrow, apex very acute, usually with glandular-hairs as well as long white hairs usually extending beyond apex;</text>
      <biological_entity id="o17373" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17374" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="dark-red tipped" value_original="dark-red tipped" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17375" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o17376" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="very" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o17377" name="glandular-hair" name_original="glandular-hairs" src="d0_s10" type="structure" />
      <biological_entity id="o17378" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o17379" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <relation from="o17376" id="r1925" modifier="usually" name="with" negation="false" src="d0_s10" to="o17377" />
      <relation from="o17377" id="r1926" name="as well as" negation="false" src="d0_s10" to="o17378" />
      <relation from="o17378" id="r1927" name="extending beyond" negation="false" src="d0_s10" to="o17379" />
    </statement>
    <statement id="d0_s11">
      <text>petals oblanceolate, 3–5 mm, rarely absent, usually shorter than sepals, apex deeply 2-fid;</text>
      <biological_entity id="o17380" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17381" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character constraint="than sepals" constraintid="o17382" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o17382" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o17383" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10;</text>
      <biological_entity id="o17384" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17385" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 5.</text>
      <biological_entity id="o17386" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o17387" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules narrowly cylindric, curved, 7–10 mm;</text>
      <biological_entity id="o17388" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>teeth 10, erect, margins convolute.</text>
      <biological_entity id="o17389" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o17390" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds pale-brown, 0.5–0.6 mm, finely tuberculate;</text>
      <biological_entity id="o17391" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="pale-brown" value_original="pale-brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>testa inflated or not.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 72.</text>
      <biological_entity id="o17392" name="testa" name_original="testa" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
        <character name="shape" src="d0_s17" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17393" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering throughout growing season.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arable land, waste places, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arable land" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Nfld. and Labr. (Nfld.), N.S., Ont., Que., Yukon; Ala., Alaska, Ariz., Ark., Calif., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Kans., Ky., La., Md., Mass., Mich., Miss., Mo., Mont., Nev., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., Tenn., Tex., Va., Wash., W.Va.; Europe; introduced and common in Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="and common in Mexico" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Sticky mouse-ear chickweed</other_name>
  <other_name type="common_name">céraiste aggloméré</other_name>
  <discussion>Cerastium glomeratum often has been reported as C. viscosum Linneaus, an ambiguous name; see discussion under the genus.</discussion>
  
</bio:treatment>