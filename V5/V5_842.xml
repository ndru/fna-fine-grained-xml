<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Gandoger" date="1906" rank="species">salicornioides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Roy. Bot. Belgique</publication_title>
      <place_in_publication>42: 187. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species salicornioides;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060488</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">demissum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species demissum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">demissum</taxon_name>
    <taxon_name authority="(Gandoger) S. Stokes" date="unknown" rank="variety">romanum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species demissum;variety romanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">salicornioides</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;variety salicornioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, annual, (0.5–) 1–3 dm, glabrous, greenish to reddish.</text>
      <biological_entity id="o9312" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="reddish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o9313" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o9314" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 0.2–0.6 dm, glabrous.</text>
      <biological_entity id="o9315" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o9316" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="0.6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o9317" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–3 cm, floccose;</text>
      <biological_entity id="o9318" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to orbiculate, 0.4–1.5 × 0.4–1.2 cm, white-tomentose abaxially, floccose or glabrous and greenish adaxially, margins plane.</text>
      <biological_entity id="o9319" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="orbiculate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="1.2" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o9320" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open, 5–25 × 5–25 cm;</text>
      <biological_entity id="o9321" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches glabrous;</text>
      <biological_entity id="o9322" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–2 × 1–2 mm.</text>
      <biological_entity id="o9323" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent or erect, straight, slender, 0.1–0.5 cm, glabrous, absent distally.</text>
      <biological_entity id="o9324" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s9" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate, 1.5–2 × 1–1.7 mm, glabrous;</text>
      <biological_entity id="o9325" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o9326" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1.2–1.7 mm;</text>
      <biological_entity id="o9327" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pale-yellow, glabrous, pustulose proximally;</text>
      <biological_entity id="o9328" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pale-yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="relief" src="d0_s13" value="pustulose" value_original="pustulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, ovate;</text>
      <biological_entity id="o9329" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens included, 1.2–1.8 mm;</text>
      <biological_entity id="o9330" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o9331" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes brown, lenticular, 1.6–2 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 36.</text>
      <biological_entity id="o9332" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9333" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey flats and slopes, saltbush, greasewood, and sagebrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="greasewood" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1300(-1500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="700" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>188.</number>
  <other_name type="common_name">Saltwort wild buckwheat</other_name>
  <discussion>Eriogonum salicornioides is known from Harney and Malheur counties in southeastern Oregon and Elmore and Owyhee counties of southwestern Idaho. Its attribution to Nevada is based on a Percy Train collection (Jun 1934, NY, PAC) from a dry desert valley in northern Humboldt County. Efforts to relocate the species in that state have been unsuccessful.</discussion>
  
</bio:treatment>