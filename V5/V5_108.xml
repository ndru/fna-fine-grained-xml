<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">54</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arenaria</taxon_name>
    <taxon_name authority="Shinners" date="1962" rank="species">ludens</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>1: 51. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus arenaria;species ludens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060021</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots filiform.</text>
      <biological_entity id="o1" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–18+, erect to ascending, green or reddish, 15–30 (–45) cm;</text>
      <biological_entity id="o2" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="18" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes ± terete, 1–10 times as long as leaves, dull, retrorsely pubescent in 2 lines.</text>
      <biological_entity id="o3" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character constraint="leaf" constraintid="o4" is_modifier="false" name="length" src="d0_s3" value="1-10 times as long as leaves" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
        <character constraint="in lines" constraintid="o5" is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5" name="line" name_original="lines" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves usually connate basally, with narrow, scarious sheath 0.2–0.5 mm, petiolate (proximal leaves) or sessile;</text>
      <biological_entity id="o6" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually; basally" name="fusion" src="d0_s4" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o7" name="sheath" name_original="sheath" src="d0_s4" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o6" id="r0" name="with" negation="false" src="d0_s4" to="o7" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 2–4 mm;</text>
      <biological_entity id="o8" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade 1-veined, vein prominent abaxially, usually narrowly lanceolate, elliptic, or oblanceolate, 10–17 × 2–4 mm, herbaceous, margins ± flat, herbaceous, dull, ciliate in proximal 1/2, apex acute to acuminate, not pustulate, glabrous;</text>
      <biological_entity id="o9" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o10" name="vein" name_original="vein" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="17" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o11" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character constraint="in proximal 1/2" constraintid="o12" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12" name="1/2" name_original="1/2" src="d0_s6" type="structure" />
      <biological_entity id="o13" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s6" value="pustulate" value_original="pustulate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>axillary leaf clusters absent.</text>
      <biological_entity constraint="axillary" id="o14" name="leaf" name_original="leaf" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, open, minutely bracteate, 3–45+-flowered cymes.</text>
      <biological_entity id="o15" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o16" name="cyme" name_original="cymes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="3-45+-flowered" value_original="3-45+-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect or ascending in fruit, 3–20 mm, retrorsely pubescent in 2 lines.</text>
      <biological_entity id="o17" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o18" is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="20" to_unit="mm" />
        <character constraint="in lines" constraintid="o19" is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o19" name="line" name_original="lines" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals green or often purple, 1-veined, strongly keeled proximally, ovatelanceolate to narrowly lanceolate (herbaceous portion pale, narrowly lanceolate to linear), 3–4 mm, not enlarging in fruit, apex acuminate, not pustulate, glabrous;</text>
      <biological_entity id="o20" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="strongly; proximally" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s10" to="narrowly lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character constraint="in fruit" constraintid="o22" is_modifier="false" modifier="not" name="size" src="d0_s10" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o22" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o23" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s10" value="pustulate" value_original="pustulate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals oblong to broadly elliptic, 2.8–4 mm, 1–11/3 times as long as sepals, apex rounded.</text>
      <biological_entity id="o24" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="broadly elliptic" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character constraint="sepal" constraintid="o26" is_modifier="false" name="length" src="d0_s11" value="1-11/3 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o26" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o27" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules loosely enclosed by calyx, ovoid, 3–3.5 mm, 4/5–1 times as long as sepals.</text>
      <biological_entity id="o28" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character constraint="sepal" constraintid="o30" is_modifier="false" name="length" src="d0_s12" value="4/5-1 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o29" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
      <biological_entity id="o30" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <relation from="o28" id="r1" modifier="loosely" name="enclosed by" negation="false" src="d0_s12" to="o29" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 7–15, dark-brown to black, suborbicular, slightly compressed, 0.6–0.7 mm, shiny, obscurely tuberculate (20×).</text>
      <biological_entity id="o31" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s13" to="15" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s13" to="black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
        <character name="quantity" src="d0_s13" value="[20" value_original="[20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early autumn.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early autumn" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Igneous soil on cliffs and ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="igneous" constraint="on cliffs and ledges" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="igneous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; n Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Trans-Pecos sandwort</other_name>
  <discussion>Arenaria ludens may be more closely related to A. lanuginosa than to A. benthamii, the taxon with which it is often confused, if seed morphology is any indication.</discussion>
  
</bio:treatment>