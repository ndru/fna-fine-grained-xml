<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Reveal" date="1969" rank="species">concinnum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>96: 476, fig. 1. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species concinnum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060228</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, annual, (0.5–) 1–6 (–10) dm, glabrous, green.</text>
      <biological_entity id="o5435" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o5436" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o5437" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, usually hollow and fistulose, 0.5–2 dm, glabrous.</text>
      <biological_entity id="o5438" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o5439" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal or sheathing up stems 1–6 cm;</text>
      <biological_entity id="o5440" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="up stems" constraintid="o5441" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o5441" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–10 cm, pilose-hispid;</text>
      <biological_entity id="o5442" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-hispid" value_original="pilose-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade subcordate, (0.5–) 1–4 (–6) × (0.5–) 1–5 (–7) cm, pilose-hispid and bright green on both surfaces, margins plane.</text>
      <biological_entity id="o5443" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose-hispid" value_original="pilose-hispid" />
        <character constraint="on surfaces" constraintid="o5444" is_modifier="false" name="coloration" src="d0_s5" value="bright green" value_original="bright green" />
      </biological_entity>
      <biological_entity id="o5444" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o5445" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences narrowly cymose, open, 5–80 × 5–35 (–40) cm;</text>
      <biological_entity id="o5446" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="80" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches often fistulose, glabrous;</text>
      <biological_entity id="o5447" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–4 × 0.5–2 mm.</text>
      <biological_entity id="o5448" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles deflexed, straight, slender, (0.05–) 0.1–0.25 cm, glabrous.</text>
      <biological_entity id="o5449" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="0.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s9" to="0.25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate, 1.2–1.6 × 1–1.4 mm, glabrous;</text>
      <biological_entity id="o5450" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s10" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.4–0.6 mm.</text>
      <biological_entity id="o5451" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–2.5 mm;</text>
      <biological_entity id="o5452" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth greenish white to reddish, glabrous;</text>
      <biological_entity id="o5453" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s13" to="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, oblong;</text>
      <biological_entity id="o5454" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens included to exserted, 1–1.5 mm;</text>
      <biological_entity id="o5455" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="included" name="position" src="d0_s15" to="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o5456" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes dark greenish brown to black, 3-gonous, 1.4–1.7 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o5457" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark greenish" value_original="dark greenish" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s17" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5458" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats, washes, and slopes, saltbush and sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="washes" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>163.</number>
  <other_name type="common_name">Darin’s wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum concinnum is locally common on the mesas of the U.S. Department of Energy’s Nevada Test Site east and north of Beatty, Nye County. Although the area is subject to considerable habitat destruction, the species seems to be surviving and is neither threatened nor endangered, although it is properly considered a “sensitive” species given its limited range and the potential for more profound habitat destruction in the area.</discussion>
  
</bio:treatment>