<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal" date="2004" rank="species">artificis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>86: 121. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species artificis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060186</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, not scapose, (2–) 3–4.5 × 1–2 (–3) dm, glabrous.</text>
      <biological_entity id="o6498" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_length" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s0" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, without persistent leaf-bases, up to 1/3 height of plant;</text>
      <biological_entity id="o6499" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o6500" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/3 height of plant" />
      </biological_entity>
      <relation from="o6499" id="r705" name="without" negation="false" src="d0_s1" to="o6500" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o6501" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect, slender, solid, not fistulose, 1–2 dm, pubescent among leaves.</text>
      <biological_entity id="o6502" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
        <character constraint="among leaves" constraintid="o6503" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6503" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal or sheathing up stem 1–3 cm, 1 per node;</text>
      <biological_entity id="o6504" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="up stem" constraintid="o6505" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o6505" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character constraint="per node" constraintid="o6506" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6506" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 2.5–4 cm, tomentose;</text>
      <biological_entity id="o6507" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly elliptic, 2–5 × (0.8–) 1–1.5 (–1.7) cm, grayish-tomentose on both surfaces, margins plane.</text>
      <biological_entity id="o6508" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_width" src="d0_s6" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o6509" is_modifier="false" name="pubescence" src="d0_s6" value="grayish-tomentose" value_original="grayish-tomentose" />
      </biological_entity>
      <biological_entity id="o6509" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o6510" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, 10–25 × 5–15 cm;</text>
      <biological_entity id="o6511" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s7" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, glabrous;</text>
      <biological_entity id="o6512" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular, 2–3 mm.</text>
      <biological_entity id="o6513" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles erect, slender, 0.2–1 cm, glabrous.</text>
      <biological_entity id="o6514" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s10" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node, turbinate, 3.5–4 × 2–2.5 mm;</text>
      <biological_entity id="o6515" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o6516" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6516" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5 (–6), erect, 0.5–0.8 mm.</text>
      <biological_entity id="o6517" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3.5–4 mm;</text>
      <biological_entity id="o6518" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth ochroleucous to pale-yellow or, rarely, yellow, glabrous;</text>
      <biological_entity id="o6519" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="ochroleucous" name="coloration" src="d0_s14" to="pale-yellow" />
        <character name="coloration" src="d0_s14" value="," value_original="," />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/5, monomorphic, oblong;</text>
      <biological_entity id="o6520" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/5" value_original="1/5" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 5–6 mm;</text>
      <biological_entity id="o6521" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o6522" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, 3.5–4 mm, glabrous.</text>
      <biological_entity id="o6523" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to somewhat gravelly, volcanic slopes, mixed grassland and sagebrush communities, juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="somewhat" />
        <character name="habitat" value="volcanic slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="sandy to gravelly" modifier="somewhat" />
        <character name="habitat" value="juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Kaye’s wild buckwheat</other_name>
  <discussion>Eriogonum artificis is known from a single location near Frisco in Beaver County. Care must be taken not to confuse Kaye’s wild buckwheat with glabrous forms of E. spathulatum that occur nearby. The flowers of E. artificis are usually pale yellow, but occasionally plants are found with cream-colored and distinctly yellow flowers.</discussion>
  
</bio:treatment>