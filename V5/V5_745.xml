<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="(J. T. Howell) Reveal in P. A. Munz" date="1968" rank="species">twisselmannii</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.</publication_title>
      <place_in_publication>40. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species twisselmannii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060535</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_name authority="J. T. Howell" date="unknown" rank="variety">twisselmannii</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>10: 13. 1963</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species douglasii;variety twisselmannii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, matted, occasionally polygamodioecious, (0.2–) 0.5–1.5 × 3–4 dm, tomentose.</text>
      <biological_entity id="o13503" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_length" src="d0_s0" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex spreading;</text>
      <biological_entity id="o13504" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o13505" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect or nearly so, slender, solid, not fistulose, arising at nodes of caudex branches and at distal nodes of short, nonflowering aerial branches, (0.2–) 0.5–1.2 dm, tomentose, with a whorl of 4–8 leaflike bracts ca. midlength, similar to leaf-blades, 0.4–0.8 × 0.1–0.3 cm.</text>
      <biological_entity id="o13506" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o13507" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s2" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="at nodes" constraintid="o13508" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s2" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="1.2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" notes="" src="d0_s2" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" notes="" src="d0_s2" to="0.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13508" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity constraint="caudex" id="o13509" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o13510" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity constraint="nonflowering" id="o13511" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
      <biological_entity id="o13512" name="whorl" name_original="whorl" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o13513" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s2" to="8" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o13514" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure" />
      <relation from="o13508" id="r1468" name="part_of" negation="false" src="d0_s2" to="o13509" />
      <relation from="o13507" id="r1469" name="at" negation="false" src="d0_s2" to="o13510" />
      <relation from="o13510" id="r1470" name="part_of" negation="false" src="d0_s2" to="o13511" />
      <relation from="o13507" id="r1471" name="with" negation="false" src="d0_s2" to="o13512" />
      <relation from="o13512" id="r1472" name="part_of" negation="false" src="d0_s2" to="o13513" />
      <relation from="o13507" id="r1473" name="to" negation="false" src="d0_s2" to="o13514" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves in loose to congested basal rosettes;</text>
      <biological_entity id="o13515" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o13516" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="loose to congested" value_original="loose to congested" />
      </biological_entity>
      <relation from="o13515" id="r1474" name="in" negation="false" src="d0_s3" to="o13516" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.2–0.4 cm, thinly tomentose;</text>
      <biological_entity id="o13517" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s4" to="0.4" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to elliptic, 0.5–1 × (0.2–) 0.4–0.6 cm, white or grayish-tomentose abaxially, thinly tomentose to nearly glabrous and dull green adaxially.</text>
      <biological_entity id="o13518" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s5" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="0.6" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="grayish-tomentose" value_original="grayish-tomentose" />
        <character char_type="range_value" from="thinly tomentose" name="pubescence" src="d0_s5" to="nearly glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences capitate, 0.7–1.5 cm wide;</text>
      <biological_entity id="o13519" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches absent;</text>
      <biological_entity id="o13520" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts absent immediately below involucre.</text>
      <biological_entity id="o13521" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character constraint="immediately below involucre" constraintid="o13522" is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13522" name="involucre" name_original="involucre" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres 1 per node, campanulate, (2.5–) 4–5 × 4–6 mm;</text>
      <biological_entity id="o13523" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="per node" constraintid="o13524" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13524" name="node" name_original="node" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 6–9, lobelike, strongly reflexed, (2–) 3–5 mm.</text>
      <biological_entity id="o13525" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="9" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="lobelike" value_original="lobelike" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers (4–) 5–6 mm, including 1–1.3 mm stipelike base;</text>
      <biological_entity id="o13526" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13527" name="base" name_original="base" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s11" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o13526" id="r1475" name="including" negation="false" src="d0_s11" to="o13527" />
    </statement>
    <statement id="d0_s12">
      <text>perianth pale-yellow, villous abaxially;</text>
      <biological_entity id="o13528" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals slightly dimorphic, those of outer whorl obtuse, 3–5 × 3–3.5 mm, those of inner whorl broadly oblanceolate, 5–6 × 2.5–3 mm;</text>
      <biological_entity id="o13529" name="tepal" name_original="tepals" src="d0_s13" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13530" name="whorl" name_original="whorl" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o13531" name="whorl" name_original="whorl" src="d0_s13" type="structure" />
      <relation from="o13529" id="r1476" name="part_of" negation="false" src="d0_s13" to="o13530" />
      <relation from="o13529" id="r1477" name="part_of" negation="false" src="d0_s13" to="o13531" />
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted, 4–5 mm;</text>
      <biological_entity id="o13532" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o13533" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown, 5–5.5 mm, glabrous except for sparsely pubescent beak.</text>
      <biological_entity id="o13534" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13535" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky outcrops, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2300-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>122.</number>
  <other_name type="common_name">Twisselmann’s wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum twisselmannii is a rare species of the southern Sierra Nevada and is known only from Slate Mountain and The Needles in Tulare County. It is worthy of being on a “watch” list of rare plants in California, but because of its remote location, it is not threatened.</discussion>
  
</bio:treatment>