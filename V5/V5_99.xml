<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler,Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/17 21:02:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <place_of_publication>
      <publication_title>in S. L. Endlicher, Gen. Pl.</publication_title>
      <place_in_publication>13: 963. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily Alsinoideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20603</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, winter-annual, annual, biennial, or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted and/or rhizomatous, rarely with tuberous thickenings (Pseudostellaria).</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o1" name="thickening" name_original="thickenings" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending or erect, simple or branched.</text>
      <biological_entity id="o2" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, connate proximally or not, often petiolate (basal leaves), not stipulate;</text>
      <biological_entity id="o3" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" modifier="proximally; proximally; not" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subulate or linear to spatulate, lanceolate, or broadly ovate, seldom succulent.</text>
      <biological_entity id="o4" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="spatulate lanceolate or broadly ovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="spatulate lanceolate or broadly ovate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="spatulate lanceolate or broadly ovate" />
        <character is_modifier="false" modifier="seldom" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal or axillary cymes, or flowers solitary;</text>
      <biological_entity id="o5" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o6" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
      <biological_entity id="o7" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous or reduced, herbaceous to scarious (or rarely absent);</text>
      <biological_entity id="o8" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s6" to="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>involucel bracteoles absent.</text>
      <biological_entity constraint="involucel" id="o9" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels present or rarely flowers sessile.</text>
      <biological_entity id="o10" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or seldom unisexual, sometimes inconspicuous;</text>
      <biological_entity id="o12" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="seldom" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth and androecium hypogynous or perigynous, often slightly;</text>
      <biological_entity id="o13" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s10" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o14" name="androecium" name_original="androecium" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="hypogynous" value_original="hypogynous" />
        <character is_modifier="false" name="position" src="d0_s10" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium cup, dish, or disc-shaped;</text>
      <biological_entity constraint="hypanthium" id="o15" name="cup" name_original="cup" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="dish" value_original="dish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="disc--shaped" value_original="disc--shaped" />
        <character is_modifier="false" name="shape" src="d0_s11" value="dish" value_original="dish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="disc--shaped" value_original="disc--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals (4–) 5, distinct or seldom connate basally, sometimes hooded, not awned;</text>
      <biological_entity id="o16" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="seldom; basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s12" value="hooded" value_original="hooded" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals absent or (1–) 4–5, usually white, sometimes translucent, yellowish white, pink, or brownish, seldom clawed, auricles absent, coronal appendages absent, blade apex entire or 2-fid, sometimes jagged or emarginate, rarely laciniate;</text>
      <biological_entity id="o17" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s13" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish white" value_original="yellowish white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="seldom" name="shape" src="d0_s13" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o18" name="auricle" name_original="auricles" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="coronal" id="o19" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="blade" id="o20" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s13" value="jagged" value_original="jagged" />
        <character is_modifier="false" name="shape" src="d0_s13" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens absent or (1–) 5 (–10), in 1 or 2 whorls, arising from base of ovary, a nectariferous disc, or sometimes the hypanthium or hypanthium rim;</text>
      <biological_entity id="o21" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s14" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="10" />
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o22" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o23" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="nectariferous" id="o24" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o25" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure" />
      <biological_entity constraint="hypanthium" id="o26" name="rim" name_original="rim" src="d0_s14" type="structure" />
      <relation from="o21" id="r0" name="in" negation="false" src="d0_s14" to="o22" />
      <relation from="o21" id="r1" name="in" negation="false" src="d0_s14" to="o23" />
      <relation from="o21" id="r2" name="in" negation="false" src="d0_s14" to="o25" />
      <relation from="o21" id="r3" name="in" negation="false" src="d0_s14" to="o26" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes absent or 1–5 (–8);</text>
      <biological_entity id="o27" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="8" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary 1-locular or rarely 3-locular (Wilhelmsia);</text>
      <biological_entity id="o28" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles (2–) 3–5 (–6), distinct;</text>
      <biological_entity id="o29" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s17" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas (2–) 3–5 (–6).</text>
      <biological_entity id="o30" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s18" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="6" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s18" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits capsules, or rarely utricles (Scleranthus), opening by (2–) 3–6, occasionally 8 or 10 valves or (3 or) 6–10 teeth;</text>
      <biological_entity constraint="fruits" id="o31" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s19" to="10" />
      </biological_entity>
      <biological_entity id="o32" name="utricle" name_original="utricles" src="d0_s19" type="structure">
        <character modifier="occasionally" name="quantity" src="d0_s19" unit="or valves" value="8" value_original="8" />
        <character modifier="occasionally" name="quantity" src="d0_s19" unit="or valves" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o33" name="tooth" name_original="teeth" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>carpophore present or often absent.</text>
      <biological_entity id="o34" name="carpophore" name_original="carpophore" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds 1–60+, yellowish or tan to dark red or often brown or black, usually reniform or triangular to circular and laterally compressed or ovoid to globose, rarely oblong and dorsiventrally compressed (Holosteum);</text>
      <biological_entity id="o35" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s21" to="60" upper_restricted="false" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s21" to="dark red" />
        <character name="coloration" src="d0_s21" value="often" value_original="often" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="black" value_original="black" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s21" to="circular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s21" to="globose" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s21" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>embryo usually peripheral and curved, rarely central and straight (Holosteum).</text>
    </statement>
    <statement id="d0_s23">
      <text>x = 6–15, 17–19, 23.</text>
      <biological_entity id="o36" name="embryo" name_original="embryo" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s22" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" name="course" src="d0_s22" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s22" value="central" value_original="central" />
        <character is_modifier="false" name="course" src="d0_s22" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="x" id="o37" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s23" to="15" />
        <character char_type="range_value" from="17" name="quantity" src="d0_s23" to="19" />
        <character name="quantity" src="d0_s23" value="23" value_original="23" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North-temperate regions, South America (Andean region), Europe (Mediterranean region), w, c Asia (Himalayas, Mediterranean region), Africa (Mediterranean region).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North-temperate regions" establishment_means="native" />
        <character name="distribution" value="South America (Andean region)" establishment_means="native" />
        <character name="distribution" value="Europe (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="c Asia (Himalayas)" establishment_means="native" />
        <character name="distribution" value="c Asia (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Africa (Mediterranean region)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43c.</number>
  <other_name type="past_name">Alsineae</other_name>
  <discussion>Genera 30, species ca. 1040 (16 genera, 137 species in the flora).</discussion>
  <discussion>Alsinoideae, often considered basal in the family and the least specialized, is in some ways the most heterogeneous of the subfamilies. Members of its largest tribe (Alsineae) share the following characteristics: stipules absent, sepals free or at most basally connate, and capsular fruits. Indehiscent fruits, relatively short hypanthia, and other floral reductions occur in varying combinations in the approximately 30 species placed in four other tribes. A broad molecular survey of Alsinoideae has revealed two major lineages and lack of support for the existing tribal circumscriptions (M. Nepokroeff et al. 2002). About three-fourths of the species are members of Arenaria, Cerastium, Minuartia, and Stellaria.</discussion>
  <discussion>Attempts have been made to move Scleranthus (fruit a utricle surrounded by an enlarged hypanthium) from Alsinoideae to either Paronychioideae (J. Hutchinson 1973, as Illecebraceae) or Scleranthaceae (A. Takhtajan 1997). Recent molecular and morphological studies by R. D. Smissen et. al. (2002, 2003) supported its retention in the Alsinoideae.</discussion>
  <references>
    <reference>Fernald, M. L. 1919. The unity of the genus Arenaria. Rhodora 21: 1–7.</reference>
    <reference>Maguire, B. 1951. Studies in the Caryophyllaceae. V. Arenaria in America north of Mexico. Amer. Midl. Naturalist 46: 493–511.</reference>
    <reference>McNeill, J. 1962. Taxonomic studies in the Alsinoideae: I. Generic and infra-generic groups. Notes Roy. Bot. Gard. Edinburgh 24: 79–155.</reference>
    <reference>McNeill, J. 1980b. The delimitation of Arenaria (Caryophyllaceae) and related genera in North America, with 11 new combinations in Minuartia. Rhodora 82: 495–502.</reference>
    <reference>Wofford, B. E. 1981. External seed morphology of Arenaria (Caryophyllaceae) of the southeastern United States. Syst. Bot. 6: 126–135.</reference>
  </references>
  
</bio:treatment>