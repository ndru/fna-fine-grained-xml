<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">409</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">spergulinum</taxon_name>
    <taxon_name authority="(S. Stokes) J. T. Howell" date="1950" rank="variety">pratense</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>6: 80. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species spergulinum;variety pratense;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060500</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="species">pratense</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>3: 201. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species pratense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate to ascending, 0.5–1 dm.</text>
      <biological_entity id="o17021" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="ascending" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="1" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescence branches: internodes not glandular.</text>
      <biological_entity constraint="inflorescence" id="o17022" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o17023" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 1.8–2 mm;</text>
      <biological_entity id="o17024" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth hirsutulous.</text>
      <biological_entity id="o17025" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy granitic slopes, high-elevation meadow and sagebrush communities, subalpine to alpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy granitic slopes" />
        <character name="habitat" value="high-elevation meadow" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="subalpine to alpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(2500-)2800-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3500" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>191c.</number>
  <other_name type="common_name">Mountain meadow wild buckwheat</other_name>
  <discussion>Variety pratense is restricted to the southern Sierra Nevada in Inyo and Tulare counties.</discussion>
  
</bio:treatment>