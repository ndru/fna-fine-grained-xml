<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">204</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="illustration_page">203</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Hooker" date="1830" rank="species">scouleri</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 88. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species scouleri;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060885</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o24726" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o24727" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched, woody, crowns 1–several.</text>
      <biological_entity id="o24728" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o24729" name="crown" name_original="crowns" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, simple proximal to inflorescence, slender or stout, 10–80 cm, puberulent.</text>
      <biological_entity id="o24730" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="to inflorescence" constraintid="o24731" is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o24731" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves 2 per node;</text>
      <biological_entity id="o24732" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o24733" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24733" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal petiolate, blade oblanceolate, 6–25 cm × 4–30 mm, retrorsely puberulent on both surfaces;</text>
      <biological_entity constraint="basal" id="o24734" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o24735" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="30" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o24736" is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o24736" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline in 1–12 pairs, usually sessile, blade well developed, lanceolate to ovatelanceolate, oblanceolate, or rarely linear or linear-lanceolate.</text>
      <biological_entity constraint="cauline" id="o24737" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o24738" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="12" />
      </biological_entity>
      <biological_entity id="o24739" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s6" value="developed" value_original="developed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovatelanceolate oblanceolate or rarely linear or linear-lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovatelanceolate oblanceolate or rarely linear or linear-lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovatelanceolate oblanceolate or rarely linear or linear-lanceolate" />
      </biological_entity>
      <relation from="o24737" id="r2742" name="in" negation="false" src="d0_s6" to="o24738" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, pseudoracemose, or rarely paniculate, erect or nodding, with 1–12 flowering nodes, 2–20-flowered, open or dense, flowers paired or in many-flowered whorls, bracteate, cymes often sessile;</text>
      <biological_entity id="o24740" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pseudoracemose" value_original="pseudoracemose" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="2-20-flowered" value_original="2-20-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o24741" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="12" />
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o24742" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="in many-flowered whorls" value_original="in many-flowered whorls" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o24743" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
      <biological_entity id="o24744" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o24740" id="r2743" name="with" negation="false" src="d0_s7" to="o24741" />
      <relation from="o24742" id="r2744" name="in" negation="false" src="d0_s7" to="o24743" />
    </statement>
    <statement id="d0_s8">
      <text>bracts 3–60 mm.</text>
      <biological_entity id="o24745" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels becoming deflexed at base of calyx, 1/4–2 times calyx, glandular-pubescent.</text>
      <biological_entity id="o24746" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character constraint="at base" constraintid="o24747" is_modifier="false" modifier="becoming" name="orientation" src="d0_s9" value="deflexed" value_original="deflexed" />
        <character constraint="calyx" constraintid="o24749" is_modifier="false" name="size_or_quantity" notes="" src="d0_s9" value="1/4-2 times calyx" value_original="1/4-2 times calyx" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o24747" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o24748" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o24749" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <relation from="o24747" id="r2745" name="part_of" negation="false" src="d0_s9" to="o24748" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers shortly pedicellate or sessile;</text>
      <biological_entity id="o24750" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx prominently 10-veined, campanulate or tubular in flower, clavate, turbinate, or fusiform in fruit, constricted or not at base around carpophore in fruit, 8–20 × 3–8 mm, veins parallel, purplish or green, with pale commissures;</text>
      <biological_entity id="o24751" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s11" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character constraint="in flower" constraintid="o24752" is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character constraint="in fruit" constraintid="o24753" is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character name="size" src="d0_s11" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o24752" name="flower" name_original="flower" src="d0_s11" type="structure" />
      <biological_entity id="o24753" name="fruit" name_original="fruit" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o24754" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o24755" name="carpophore" name_original="carpophore" src="d0_s11" type="structure" />
      <biological_entity id="o24756" name="fruit" name_original="fruit" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24757" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o24758" name="commissure" name_original="commissures" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="pale" value_original="pale" />
      </biological_entity>
      <relation from="o24751" id="r2746" name="at" negation="false" src="d0_s11" to="o24754" />
      <relation from="o24754" id="r2747" name="around" negation="false" src="d0_s11" to="o24755" />
      <relation from="o24755" id="r2748" name="in" negation="false" src="d0_s11" to="o24756" />
      <relation from="o24757" id="r2749" name="with" negation="false" src="d0_s11" to="o24758" />
    </statement>
    <statement id="d0_s12">
      <text>lobes lanceolate, 2–5 mm, apex obtuse with broad-membranous margin and tip;</text>
      <biological_entity id="o24759" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24760" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character constraint="with tip" constraintid="o24762" is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24761" name="margin" name_original="margin" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="broad-membranous" value_original="broad-membranous" />
      </biological_entity>
      <biological_entity id="o24762" name="tip" name_original="tip" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>corolla white, greenish white, or pink, sometimes tinged pink or purple, clawed, claw longer than calyx, limb deeply 2–4-lobed, often with smaller lateral teeth, 2.5–8 mm, appendages 1–3 mm;</text>
      <biological_entity id="o24763" name="corolla" name_original="corolla" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="tinged pink" value_original="tinged pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o24764" name="claw" name_original="claw" src="d0_s13" type="structure">
        <character constraint="than calyx" constraintid="o24765" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o24765" name="calyx" name_original="calyx" src="d0_s13" type="structure" />
      <biological_entity id="o24766" name="limb" name_original="limb" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s13" value="2-4-lobed" value_original="2-4-lobed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="smaller lateral" id="o24767" name="tooth" name_original="teeth" src="d0_s13" type="structure" />
      <biological_entity id="o24768" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o24766" id="r2750" modifier="often" name="with" negation="false" src="d0_s13" to="o24767" />
    </statement>
    <statement id="d0_s14">
      <text>stamens ± equaling corolla claw;</text>
      <biological_entity id="o24769" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <biological_entity constraint="corolla" id="o24770" name="claw" name_original="claw" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3–4, ± equaling corolla claw.</text>
      <biological_entity id="o24771" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="4" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o24772" name="claw" name_original="claw" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s15" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ovoid to ellipsoid, equaling or slightly longer than calyx, opening by 6 or 8 teeth;</text>
      <biological_entity id="o24773" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="ellipsoid" />
        <character is_modifier="false" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
        <character constraint="than calyx" constraintid="o24774" is_modifier="false" name="length_or_size" src="d0_s16" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o24774" name="calyx" name_original="calyx" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>carpophore 1.5–6 mm.</text>
      <biological_entity id="o24775" name="carpophore" name_original="carpophore" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds brown or grayish brown, reniform, 1–1.5 mm, margins papillate, rugose on sides.</text>
      <biological_entity id="o24776" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="grayish brown" value_original="grayish brown" />
        <character is_modifier="false" name="shape" src="d0_s18" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24777" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
        <character constraint="on sides" constraintid="o24778" is_modifier="false" name="relief" src="d0_s18" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o24778" name="side" name_original="sides" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Oreg., Utah, Wash., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>Silene scouleri is a very complex species that appears to be in the process of diverging into at least three different entities. Subspecies scouleri is a plant of the Pacific coast and lowlands. It has tall, stiffly erect stems, lanceolate to broadly lanceolate leaves, and a viscid inflorescence with many-flowered whorls of almost sessile flowers ranging in color from greenish white to rich pink. At the other extreme is subsp. pringlei, a plant of the mountains in Mexico extending northwards into Arizona and New Mexico. It has slender, somewhat nodding flowering stems with very narrow leaves. The flowers are usually paired at each node and secund on slender pedicels about equaling the calyx in length. The petals are off-white, sometimes tinged with dusky purple. Between the two extremes is subsp. hallii, a short, stocky plant of the Rocky Mountains and foothills with a few-flowered inflorescence. It has a larger, campanulate calyx, and some of the flowers usually become deflexed. Differentiation among these three forms is incomplete and plants indeterminate to subspecies are frequently encountered in areas away from the main distribution centers of the three subspecies. In northern Oregon and Idaho there appear to be populations connecting S. scouleri with S. oregana. They have some of the characteristics of S. oregana but not its laciniate petals. They may represent a more luxuriant form growing in taller vegetation, but their status needs further study.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces campanulate, not or only slightly clavate in fruit, 13-18(-20) × (5-)6-8 mm; inflorescences with (1-)3-6(-8) flowering nodes; plants 10-40cm; pedicels stout</description>
      <determination>53b Silene scouleri subsp. hallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces tubular to narrowly clavate in flower, clavate, turbinate, or fusiform in fruit, (8-)10-16 × 3.5-7 mm; inflorescences with 3-12 flowering nodes; plants 20-80 cm; pedicels slender</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences erect, flowers in dense pseudowhorls of usually sessile, (2-)5-20-flowered cymes, both sessile and pedicellate flowers in each cyme; pedicels erect</description>
      <determination>53a Silene scouleri subsp. scouleri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences nodding, flowers usually paired at each node, all pedicellate; pedicels ± equaling calyx, often sharply deflexed at baseof calyx</description>
      <determination>53c Silene scouleri subsp. pringlei</determination>
    </key_statement>
  </key>
</bio:treatment>