<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1754" rank="genus">persicaria</taxon_name>
    <taxon_name authority="(Adanson) H. Gross" date="1913" rank="section">Tovara</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Int. Géogr. Bot.</publication_title>
      <place_in_publication>4: 27. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus persicaria;section tovara;</taxon_hierarchy>
    <other_info_on_name type="fna_id">316732</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Adanson" date="unknown" rank="section">Tovara</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Pl.</publication_title>
      <place_in_publication>2: 276, 612. 1763, name rejected</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Tovara;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="section">Antenoron</taxon_name>
    <taxon_hierarchy>section Antenoron;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, simple or branched distally, unarmed.</text>
      <biological_entity id="o1108" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: ocrea chartaceous, margins ciliate;</text>
      <biological_entity id="o1109" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1110" name="ocreum" name_original="ocrea" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o1111" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole not winged, not auriculate;</text>
      <biological_entity id="o1112" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1113" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade often with dark triangular or lunate blotch adaxially on leaves produced early in growing season, broadly lanceolate to ovate, base rounded to acute, margins entire.</text>
      <biological_entity id="o1114" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1115" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o1116" name="blotch" name_original="blotch" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="true" name="shape" src="d0_s3" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o1117" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1118" name="season" name_original="season" src="d0_s3" type="structure" />
      <biological_entity id="o1119" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o1120" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1115" id="r110" name="with" negation="false" src="d0_s3" to="o1116" />
      <relation from="o1116" id="r111" name="on" negation="false" src="d0_s3" to="o1117" />
      <relation from="o1117" id="r112" name="produced early in" negation="false" src="d0_s3" to="o1118" />
      <relation from="o1117" id="r113" name="produced early in" negation="false" src="d0_s3" to="o1119" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and axillary, spikelike, interrupted.</text>
      <biological_entity id="o1121" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers homostylous, articulation swollen;</text>
      <biological_entity id="o1122" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o1123" name="articulation" name_original="articulation" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth narrowly campanulate to urceolate;</text>
      <biological_entity id="o1124" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly campanulate" name="shape" src="d0_s6" to="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals 4, connate ca. 1/2 their length;</text>
      <biological_entity id="o1125" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character name="length" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 5;</text>
      <biological_entity id="o1126" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles persistent on achenes, 2, exserted, reflexed distally.</text>
      <biological_entity id="o1127" name="style" name_original="styles" src="d0_s9" type="structure">
        <character constraint="on achenes" constraintid="o1128" is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="position" notes="" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o1128" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32a.</number>
  <discussion>Species 3 (1 in the flora).</discussion>
  <references>
    <reference>Li, H. L. 1952b. The genus Tovara (Polygonaceae). Rhodora 50: 19–25.</reference>
    <reference>Mun, J. H. and C. W. Park. 1995. Flavonoid chemistry of Polygonum sect. Tovara (Polygonaceae): A systematic survey. Pl. Syst. Evol. 196: 153–159.</reference>
    <reference>Youngbae, S., S. Kim, and C. W. Park. 1997. A phylogenetic study of Polygonum sect. Tovara (Polygonaceae) based on ITS sequences of nuclear ribosomal DNA. J. Plant Biol. 40: 47–52.</reference>
  </references>
  
</bio:treatment>