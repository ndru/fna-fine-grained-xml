<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">301</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="species">cinereum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Voy. Sulphur,</publication_title>
      <place_in_publication>45. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species cinereum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060215</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, round, 6–15 × 10–20 (–25) dm, tomentulose, grayish.</text>
      <biological_entity id="o9005" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="round" value_original="round" />
        <character char_type="range_value" from="6" from_unit="dm" name="length" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="25" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="width" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, occasionally with persistent leaf-bases, up to 3/4 or more height of plant;</text>
      <biological_entity id="o9006" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o9007" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="3/4" />
      </biological_entity>
      <biological_entity id="o9008" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <relation from="o9006" id="r1007" modifier="occasionally" name="with" negation="false" src="d0_s1" to="o9007" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems absent;</text>
      <biological_entity constraint="caudex" id="o9009" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect to spreading, slender, solid, not fistulose, 1–4 dm, tomentulose.</text>
      <biological_entity id="o9010" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, 1 per node;</text>
      <biological_entity id="o9011" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character constraint="per node" constraintid="o9012" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9012" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.1–0.5 (–1) cm, tomentulose;</text>
      <biological_entity id="o9013" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate, 1.5–3 × 1–2.5 (–3) cm, white-tomentulose abaxially, less so and greenish adaxially, margins plane.</text>
      <biological_entity id="o9014" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="white-tomentulose" value_original="white-tomentulose" />
        <character is_modifier="false" modifier="less; adaxially" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o9015" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate, congested, 1–2.5 × 1–2.5 cm;</text>
      <biological_entity id="o9016" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentulose;</text>
      <biological_entity id="o9017" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts usually 3, scalelike, triangular, and 1–3 mm, or leaflike, ovate, and 7–20 × 5–18 mm.</text>
      <biological_entity id="o9018" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o9019" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 3–10 per cluster, narrowly turbinate, 3–5 × 2–3 mm, tomentulose;</text>
      <biological_entity id="o9020" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per cluster" from="3" name="quantity" src="d0_s11" to="10" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5, erect, 0.2–0.5 mm.</text>
      <biological_entity id="o9021" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 2.5–3 mm;</text>
      <biological_entity id="o9022" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white to pinkish, densely white-villous;</text>
      <biological_entity id="o9023" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pinkish" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s14" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximally, monomorphic, spatulate to narrowly obovate;</text>
      <biological_entity id="o9024" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s15" to="narrowly obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 2.5–3.5 mm;</text>
      <biological_entity id="o9025" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments subglabrous.</text>
      <biological_entity id="o9026" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 2–2.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 80.</text>
      <biological_entity id="o9027" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9028" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy beaches, coastal bluffs, mesas, canyon slopes, coast scrub and chaparral communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy beaches" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="canyon slopes" />
        <character name="habitat" value="coast scrub" />
        <character name="habitat" value="chaparral communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>81.</number>
  <other_name type="common_name">Coastal wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum cinereum occurs naturally along the Pacific Coast in Los Angeles, Santa Barbara, and Ventura counties, and also on Santa Rosa Island. It is infrequently planted along highways (and is now found in San Diego County) and hybridizes with E. fasciculatum var. foliolosum. The species is the food plant of the Bernardino dotted-blue butterfly (Euphilotes bernardino).</discussion>
  
</bio:treatment>