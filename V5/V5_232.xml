<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Ehrhart ex Hoffmann" date="1791" rank="species">palustris</taxon_name>
    <place_of_publication>
      <publication_title>Deutschl. Fl.</publication_title>
      <place_in_publication>1: 152. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species palustris;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242000919</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsine</taxon_name>
    <taxon_name authority="(Withering) Britton" date="unknown" rank="species">glauca</taxon_name>
    <taxon_hierarchy>genus Alsine;species glauca;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellaria</taxon_name>
    <taxon_name authority="Withering" date="unknown" rank="species">glauca</taxon_name>
    <taxon_hierarchy>genus Stellaria;species glauca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, with slender creeping rhizomes.</text>
      <biological_entity id="o27951" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="slender" id="o27952" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o27951" id="r3125" name="with" negation="false" src="d0_s0" to="o27952" />
    </statement>
    <statement id="d0_s1">
      <text>Stems straggling, with erect branches, smoothly 4-angled, (20–) 30–60 cm, glabrous.</text>
      <biological_entity id="o27953" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="straggling" value_original="straggling" />
        <character is_modifier="false" modifier="smoothly" name="shape" notes="" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27954" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o27953" id="r3126" name="with" negation="false" src="d0_s1" to="o27954" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o27955" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear-lanceolate, 1.5–5 cm × 1–4 mm, base cuneate, margins smooth, apex acute, glabrous, usually glaucous.</text>
      <biological_entity id="o27956" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27957" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o27958" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o27959" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, (1–) 2–21-flowered cymes;</text>
      <biological_entity id="o27960" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o27961" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="(1-)2-21-flowered" value_original="(1-)2-21-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts narrowly lanceolate, 2–7 mm, herbaceous or scarious with green midrib, not ciliate.</text>
      <biological_entity id="o27962" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
        <character constraint="with midrib" constraintid="o27963" is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" notes="" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27963" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels ascending, 30–100 mm, glabrous.</text>
      <biological_entity id="o27964" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s6" to="100" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 12–18 mm diam.;</text>
      <biological_entity id="o27965" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5, distinctly 3-veined, lanceolate, 6–8 mm, margins wide, scarious, apex acute, glabrous;</text>
      <biological_entity id="o27966" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s8" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27967" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="wide" value_original="wide" />
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o27968" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 5, 7–10 mm, 1.5–2 times as long as sepals;</text>
      <biological_entity id="o27969" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character constraint="sepal" constraintid="o27970" is_modifier="false" name="length" src="d0_s9" value="1.5-2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o27970" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 10;</text>
      <biological_entity id="o27971" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3, erect, 5–7 mm;</text>
      <biological_entity id="o27972" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas club-shaped.</text>
      <biological_entity id="o27973" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules green to straw colored, ovoid-oblong, 8–10 mm, ± equaling sepals, apex acute, opening by 6 valves;</text>
      <biological_entity id="o27974" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s13" to="straw colored" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid-oblong" value_original="ovoid-oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27975" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o27976" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27977" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
      <relation from="o27976" id="r3127" name="opening by" negation="false" src="d0_s13" to="o27977" />
    </statement>
    <statement id="d0_s14">
      <text>carpophore absent.</text>
      <biological_entity id="o27978" name="carpophore" name_original="carpophore" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds dark reddish-brown, round, 1.2–1.4 mm diam., tuberculate;</text>
      <biological_entity id="o27979" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="round" value_original="round" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="diameter" src="d0_s15" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s15" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>tubercles shallow, round.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 130–188 (Europe), ca. 198.</text>
      <biological_entity id="o27980" name="tubercle" name_original="tubercles" src="d0_s16" type="structure">
        <character is_modifier="false" name="depth" src="d0_s16" value="shallow" value_original="shallow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27981" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character char_type="range_value" from="130" name="quantity" src="d0_s17" to="188" />
        <character name="quantity" src="d0_s17" value="198" value_original="198" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hayfields and pastures subject to seasonal flooding</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hayfields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="seasonal flooding" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Que.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Marsh stitchwort</other_name>
  <discussion>Stellaria palustris is found along the Saint Lawrence estuary.</discussion>
  <references>
    <reference>McNeill, J. and J. N. Findlay 1972. Introduced perennial species of Stellaria in Quebec. Naturaliste Canad. 99: 59–60.</reference>
  </references>
  
</bio:treatment>