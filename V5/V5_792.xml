<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Small" date="1898" rank="species">clavatum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 50. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species clavatum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060219</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">trichopes</taxon_name>
    <taxon_name authority="Reveal" date="unknown" rank="variety">hooveri</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species trichopes;variety hooveri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, annual, (2–) 4–18 (–22) dm, glabrous, bright green.</text>
      <biological_entity id="o19500" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="22" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="18" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o19501" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o19502" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, hollow and fistulose, 0.5–3.5 (–5) dm, glabrous, minutely glandular proximally, infrequently also short-hispid.</text>
      <biological_entity id="o19503" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o19504" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="3.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; proximally" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s2" value="short-hispid" value_original="short-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o19505" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–6 cm, hirsute;</text>
      <biological_entity id="o19506" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade round to reniform, (0.5–) 1–2.5 (–4) × (0.5–) 1–2 (–3) cm, short-hirsute on both surfaces and greenish, margins plane.</text>
      <biological_entity id="o19507" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s5" to="reniform" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o19508" is_modifier="false" name="pubescence" src="d0_s5" value="short-hirsute" value_original="short-hirsute" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s5" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o19508" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o19509" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open, spreading to erect, 30–150 (–170) × 10–80 cm;</text>
      <biological_entity id="o19510" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s6" to="erect" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="170" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s6" to="150" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s6" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches usually fistulose, glabrous;</text>
      <biological_entity id="o19511" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–4 × 1–2 mm.</text>
      <biological_entity id="o19512" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles mostly spreading, straight, slightly bent distally, capillary, 1–4 cm, glabrous.</text>
      <biological_entity id="o19513" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s9" value="bent" value_original="bent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="capillary" value_original="capillary" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate, 1–1.5 (–1.8) × 0.6–0.9 (–1.2) mm, glabrous;</text>
      <biological_entity id="o19514" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 4, erect, 0.3–0.5 mm.</text>
      <biological_entity id="o19515" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1.5–2.5 mm;</text>
      <biological_entity id="o19516" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth yellow with greenish to reddish midribs, densely hirsute with coarse curved hairs;</text>
      <biological_entity id="o19517" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character constraint="with midribs" constraintid="o19518" is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character constraint="with hairs" constraintid="o19519" is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o19518" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="greenish" is_modifier="true" name="coloration" src="d0_s13" to="reddish" />
      </biological_entity>
      <biological_entity id="o19519" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="relief" src="d0_s13" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, narrowly ovate;</text>
      <biological_entity id="o19520" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted, 0.9–1.5 mm;</text>
      <biological_entity id="o19521" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous or sparsely pubescent proximally.</text>
      <biological_entity id="o19522" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes light-brown to brown, 3-gonous, 2–2.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 32.</text>
      <biological_entity id="o19523" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19524" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey flats and slopes, mixed grassland, chaparral, saltbush, and sagebrush communities, oak and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>150.</number>
  <other_name type="common_name">Hoover’s desert trumpet</other_name>
  <discussion>Eriogonum clavatum is the tall, slender member of the E. inflatum complex found on the Inner Coast, Peninsular, and Transverse ranges of southern California. It is rather common from Monterey and San Benito counties southward to Ventura, northwestern Los Angeles, and extreme southwestern Kern counties, and then disjunct in the Peninsular Ranges of Riverside and San Diego counties, where it just enters north-central Baja California, Mexico. It extends eastward onto desert ranges, where it rarely exceeds 6 dm in height, from southern Mono County and Inyo County south through western San Bernardino County to northwestern Riverside County. From Inyo County, the species occurs across southern Nye and northern Clark counties into western Lincoln County, Nevada.</discussion>
  
</bio:treatment>