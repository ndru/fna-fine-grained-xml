<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(Douglas ex Hooker) R. L. Hartman &amp; Rabeler" date="2004" rank="species">franklinii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 240. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species franklinii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060145</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Douglas ex Hooker" date="unknown" rank="species">franklinii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 101, plate 35. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species franklinii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± cespitose, bluish green, not glaucous, with woody base.</text>
      <biological_entity id="o5815" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5816" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o5815" id="r643" name="with" negation="false" src="d0_s0" to="o5816" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 3–10 (–15) cm, glabrous.</text>
      <biological_entity id="o5817" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves persistent;</text>
      <biological_entity id="o5818" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o5819" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves in 6–10 pairs, closely overlapping, not reduced;</text>
      <biological_entity id="o5820" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o5821" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" notes="" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5822" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <relation from="o5821" id="r644" name="in" negation="false" src="d0_s3" to="o5822" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades arcuate-spreading, needlelike, (0.6–) 1–2 cm × ca. 1 mm, ± rigid, herbaceous, apex spinose, glabrous, not glaucous.</text>
      <biological_entity id="o5823" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o5824" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="arcuate-spreading" value_original="arcuate-spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o5825" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 3–45+-flowered, usually congested, subcapitate cymes.</text>
      <biological_entity id="o5826" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-45+-flowered" value_original="3-45+-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o5827" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="subcapitate" value_original="subcapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels ca. 0.1–3 (–4) mm, glabrous.</text>
      <biological_entity id="o5828" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 1–3-veined, linear-lanceolate, (5–) 8.5–12 mm, not enlarging in fruit, margins narrow, apex acuminate, glabrous;</text>
      <biological_entity id="o5829" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5830" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="8.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
        <character constraint="in fruit" constraintid="o5831" is_modifier="false" modifier="not" name="size" src="d0_s7" value="enlarging" value_original="enlarging" />
      </biological_entity>
      <biological_entity id="o5831" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o5832" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o5833" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, oblong-lanceolate to narrowly spatulate, 7–9 mm, 0.8–1.1 times as long as sepals, apex rounded to blunt;</text>
      <biological_entity id="o5834" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5835" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s8" to="narrowly spatulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character constraint="sepal" constraintid="o5836" is_modifier="false" name="length" src="d0_s8" value="0.8-1.1 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o5836" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o5837" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nectaries not apparent.</text>
      <biological_entity id="o5838" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5839" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="apparent" value_original="apparent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules 2.3–3.3 mm, glabrous.</text>
      <biological_entity id="o5840" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, pyriform, 1.2–1.7 mm, tuberculate.</text>
      <biological_entity id="o5841" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s11" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Franklin’s sandwort</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences tightly congested cymes; pedicels 0.1-1(-3) mm; sepals 8.5-12 mm; petals 0.8-0.9 times as long as sepals</description>
      <determination>8a Eremogone franklinii var. franklinii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences somewhat congested cymes; pedicels 1-4 mm; sepals 5-8 mm; petals 1-1.1 times as long as sepals</description>
      <determination>8b Eremogone franklinii var. thompsonii</determination>
    </key_statement>
  </key>
</bio:treatment>