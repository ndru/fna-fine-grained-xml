<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Pederson" date="1961" rank="species">parva</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>57: 44, fig. 4. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species parva;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250012498</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial, forming mats, rhizomatous.</text>
      <biological_entity id="o2783" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2784" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o2783" id="r313" name="forming" negation="false" src="d0_s0" to="o2784" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, much-branched, rooting at lower nodes, 4-angled, 5–30 cm, glabrous or sparsely pubescent on 2 sides.</text>
      <biological_entity id="o2785" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character constraint="at lower nodes" constraintid="o2786" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="on sides" constraintid="o2787" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o2786" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o2787" name="side" name_original="sides" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile to subsessile;</text>
      <biological_entity id="o2788" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s2" to="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to obovate, 0.3–1.5 cm × 1–6 mm, base cuneate to spatulate, margins entire, apex obtuse to ± acute, tip blunt, glabrous or pubescent at base and stem nodes.</text>
      <biological_entity id="o2789" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2790" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="spatulate" />
      </biological_entity>
      <biological_entity id="o2791" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2792" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="more or less acute" />
      </biological_entity>
      <biological_entity id="o2793" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="at base; at base and stem nodes" constraintid="o2795, o2796" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2794" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o2795" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="stem" id="o2796" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with flowers solitary, axillary in distal nodes;</text>
      <biological_entity id="o2797" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="in distal nodes" constraintid="o2799" is_modifier="false" name="position" notes="" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o2798" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2799" name="node" name_original="nodes" src="d0_s4" type="structure" />
      <relation from="o2797" id="r314" name="with" negation="false" src="d0_s4" to="o2798" />
    </statement>
    <statement id="d0_s5">
      <text>bracts absent.</text>
      <biological_entity id="o2800" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels spreading, becoming reflexed in fruit, 2–10 mm, glandular-pubescent.</text>
      <biological_entity id="o2801" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character constraint="in fruit" constraintid="o2802" is_modifier="false" modifier="becoming" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o2802" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–4 mm diam.;</text>
      <biological_entity id="o2803" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 4–5, herbaceous with midrib, ovatelanceolate, 2.5–3 mm, margins convex, membranous, apex obtuse, glandular-pubescent;</text>
      <biological_entity id="o2804" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
        <character constraint="with midrib" constraintid="o2805" is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2805" name="midrib" name_original="midrib" src="d0_s8" type="structure" />
      <biological_entity id="o2806" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o2807" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 4–5, ca. 3 mm, equaling sepals;</text>
      <biological_entity id="o2808" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2809" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 4–5;</text>
      <biological_entity id="o2810" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3–4, ascending.</text>
      <biological_entity id="o2811" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules broadly ovoid, ca. 4.5 mm, longer than sepals, apex obtuse, opening by 6–8 valves;</text>
      <biological_entity id="o2812" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="4.5" value_original="4.5" />
        <character constraint="than sepals" constraintid="o2813" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2813" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o2814" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o2815" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
      <relation from="o2814" id="r315" name="opening by" negation="false" src="d0_s12" to="o2815" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore absent.</text>
      <biological_entity id="o2816" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds ca. 20, brown, round, ca. 1 mm diam., prominently papillate;</text>
      <biological_entity id="o2817" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="round" value_original="round" />
        <character name="diameter" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="prominently" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>papillae obtuse, taller than broad.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 34.</text>
      <biological_entity id="o2818" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="height" src="d0_s15" value="taller than broad" value_original="taller than broad" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2819" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Short grass in ditches and marshy areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="short grass" constraint="in ditches and marshy areas" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="marshy areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>less than 100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="100" from_unit="m" constraint="less than " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; La., Tex.; South America (Argentina, Brazil, Paraguay).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Paraguay)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Small starwort</other_name>
  <references>
    <reference>Brown, L. E. and S. J. Marcus. 1998. Notes on the flora of Texas with additions and other significant records. Sida 18: 315–324.</reference>
    <reference>Landry, G. P., W. D. Reese, and C. M. Allen. 1988. Stellaria parva Pederson new to North America. Phytologia 64: 497.</reference>
  </references>
  
</bio:treatment>