<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">350</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal in P. A. Munz" date="1968" rank="variety">chlorothamnus</taxon_name>
    <place_of_publication>
      <publication_title>in P. A. Munz, Suppl. Calif. Fl.,</publication_title>
      <place_in_publication>44. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety chlorothamnus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060542</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, erect to rounded, (4–) 5–10 (–12) × 5–12 dm.</text>
      <biological_entity id="o26174" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="4" from_unit="dm" name="atypical_length" src="d0_s0" to="5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="12" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="12" to_unit="dm" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="4" from_unit="dm" name="atypical_length" src="d0_s0" to="5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="12" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="length" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="12" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect, 1–2.5 dm, glabrous, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o26176" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26177" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o26176" id="r2903" name="without" negation="false" src="d0_s1" to="o26177" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in rather loose rosettes;</text>
      <biological_entity id="o26178" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26179" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rather" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o26178" id="r2904" name="in" negation="false" src="d0_s2" to="o26179" />
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate to narrowly elliptic, 0.5–2 × 0.3–1 cm, glabrous on both surfaces, margins plane.</text>
      <biological_entity id="o26180" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character constraint="on surfaces" constraintid="o26181" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26181" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o26182" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences compound-umbellate, branched 2–5 times;</text>
      <biological_entity id="o26183" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound-umbellate" value_original="compound-umbellate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="" src="d0_s4" value="2-5 times" value_original="2-5 times " />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches glabrous, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o26184" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26185" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o26186" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o26184" id="r2905" name="without" negation="false" src="d0_s5" to="o26185" />
      <relation from="o26185" id="r2906" name="part_of" negation="false" src="d0_s5" to="o26186" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–2.5 mm, lobes 1–1.5 mm.</text>
      <biological_entity id="o26187" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26188" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–6 mm;</text>
      <biological_entity id="o26189" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o26190" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy granitic flats and slopes, sagebrush communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy granitic flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-2600(-2900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2900" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107ee.</number>
  <other_name type="common_name">Sherwin Grade sulphur flower</other_name>
  <discussion>Variety chlorothamnus is closely related to var. subaridum and is known only from scattered populations on the eastern slope of the Sierra Nevada in Inyo, southern Mono, and northeastern Tulare counties. Frequently it grows with var. nevadense, and mixed collections may be encountered in herbaria. The plants do well in cultivation and certainly are worthy of a place in the garden.</discussion>
  
</bio:treatment>