<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">glandulosum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 21. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species glandulosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060296</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxytheca</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">glandulosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 19. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Oxytheca;species glandulosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="(J. T. Howell) Reveal" date="unknown" rank="species">carneum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species carneum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">glandulosum</taxon_name>
    <taxon_name authority="J. T. Howell" date="unknown" rank="variety">carneum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species glandulosum;variety carneum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trichopes</taxon_name>
    <taxon_name authority="(Nuttall) S. Stokes" date="unknown" rank="subspecies">glandulosum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species trichopes;subspecies glandulosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, annual, (0.5–) 1–2.5 dm, glandular, greenish or reddish green.</text>
      <biological_entity id="o20837" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish green" value_original="reddish green" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o20838" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o20839" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 0.3–0.7 dm, glandular.</text>
      <biological_entity id="o20840" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o20841" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="0.7" to_unit="dm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, often sheathing up stem 1–2 cm;</text>
      <biological_entity id="o20842" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="up stem" constraintid="o20843" is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o20843" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–2 cm, glandular;</text>
      <biological_entity id="o20844" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade broadly elliptic to oval, 0.5–1.5 × 0.5–1.5 cm, pilose-hirsutulous, slightly glandular and greenish on both surfaces, margins entire.</text>
      <biological_entity id="o20845" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s5" to="oval" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose-hirsutulous" value_original="pilose-hirsutulous" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
        <character constraint="on surfaces" constraintid="o20846" is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o20846" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o20847" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, diffuse, spreading and usually flat-topped, 5–20 × 5–30 cm;</text>
      <biological_entity id="o20848" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches not fistulose, glandular;</text>
      <biological_entity id="o20849" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 0.8–1.2 × 0.5–1 mm.</text>
      <biological_entity id="o20850" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s8" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles deflexed or nearly so, straight, slender, 0.2–0.5 cm at proximal nodes, 0.01–0.1 cm distally, sparsely glandular nearly entire length.</text>
      <biological_entity id="o20851" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="deflexed" value_original="deflexed" />
        <character name="orientation" src="d0_s9" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" constraint="at proximal nodes" constraintid="o20852" from="0.2" from_unit="cm" name="some_measurement" src="d0_s9" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.01" from_unit="cm" modifier="distally" name="some_measurement" notes="" src="d0_s9" to="0.1" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="nearly" name="length" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20852" name="node" name_original="nodes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres narrowly turbinate, 0.8–1.2 (–1.5) × 0.6–1 (–1.3) mm, glabrous;</text>
      <biological_entity id="o20853" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s10" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.3–0.5 mm.</text>
      <biological_entity id="o20854" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–1.8 mm;</text>
      <biological_entity id="o20855" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pinkish with dark reddish-brown to red midribs, densely pilose;</text>
      <biological_entity id="o20856" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="with midribs" constraintid="o20857" from="white" name="coloration" src="d0_s13" to="pinkish" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o20857" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark reddish-brown" is_modifier="true" name="coloration" src="d0_s13" to="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, narrowly lanceolate;</text>
      <biological_entity id="o20858" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted, 1–1.5 mm;</text>
      <biological_entity id="o20859" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o20860" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes black, 3-gonous, 1–1.3 mm, glabrous.</text>
      <biological_entity id="o20861" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly, often calcareous slopes, saltbush and creosote bush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to" modifier="gravelly" />
        <character name="habitat" value="sandy to calcareous slopes" modifier="often" />
        <character name="habitat" value="bush communities" modifier="saltbush and creosote" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>153.</number>
  <other_name type="common_name">Glandular wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum glandulosum is a localized species that varies from rare to common in southeastern Inyo and extreme northeastern San Bernardino counties, California, and in southern Nye, northern Clark, and southwestern Lincoln counties, Nevada. The name E. glandulosum may not be the earliest for the taxon. The type of E. cordatum Torrey &amp; Frémont (1845, as “cordalum”) was collected by Frémont most likely near the California-Nevada border, where E. trichopes, E. contiguum, and E. glandulosum occur. Because the description is not decisive, and because J. Torrey and A. Gray (1870) reported the type as missing, it is impossible to place the name unequivocally. Nonetheless, E. cordatum was reported to be glandular (“glaneous”) with the leaves “pubescent above, hairy underneath.” The glandular condition probably eliminates E. trichopes, while those leaf features probably exclude E. contiguum.</discussion>
  
</bio:treatment>