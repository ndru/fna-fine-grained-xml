<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">392</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="S. Stokes ex M. E. Jones" date="1903" rank="species">arizonicum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>11: 16. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species arizonicum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060185</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to slightly spreading, perennial, 2.5–5 (–6) × 3–8 dm, glabrous, glaucous.</text>
      <biological_entity id="o5514" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="slightly spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="length" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex compact to spreading;</text>
      <biological_entity id="o5515" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o5516" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 1–3 dm, glabrous or thinly floccose.</text>
      <biological_entity id="o5517" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o5518" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, sheathing up stems 0.5–15 cm;</text>
      <biological_entity id="o5519" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="up stems" constraintid="o5520" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o5520" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0.5–) 1–2.5 (–4) cm, densely pilose;</text>
      <biological_entity id="o5521" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to rounded, (0.5–) 1–2 (–2.3) × (0.5–) 1–2 (–2.5) cm, usually sparsely pilose and grayish to whitish on both surfaces, margins undulate.</text>
      <biological_entity id="o5522" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="rounded" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character char_type="range_value" constraint="on surfaces" constraintid="o5523" from="grayish" name="coloration" src="d0_s5" to="whitish" />
      </biological_entity>
      <biological_entity id="o5523" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o5524" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences narrowly cymose, open, 10–35 (–40) × 5–20 cm;</text>
      <biological_entity id="o5525" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches not fistulose, glabrous, glaucous;</text>
      <biological_entity id="o5526" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 0.3–0.8 (–1) × 0.2–0.8 mm.</text>
      <biological_entity id="o5527" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="length" src="d0_s8" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, straight, capillary to slender, 0.5–2.5 (–3) cm, glabrous.</text>
      <biological_entity id="o5528" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate, (0.8–) 1–1.6 (–2) × (0.8–) 1–1.2 (–1.5) mm, glabrous;</text>
      <biological_entity id="o5529" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_length" src="d0_s10" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_width" src="d0_s10" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.3–0.8 mm.</text>
      <biological_entity id="o5530" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers (1.2–) 1.5–2.5 mm;</text>
      <biological_entity id="o5531" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth yellowish to yellowish red or reddish with greenish or reddish to reddish-brown midribs, glabrous;</text>
      <biological_entity id="o5532" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="with midribs" constraintid="o5533" from="yellowish" name="coloration" src="d0_s13" to="yellowish red or reddish" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5533" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character char_type="range_value" from="reddish" is_modifier="true" name="coloration" src="d0_s13" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals dimorphic, those of outer whorl obovate with slightly enlarged, auriculate bases, those of inner whorl obovate;</text>
      <biological_entity id="o5534" name="tepal" name_original="tepals" src="d0_s14" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character constraint="with bases" constraintid="o5536" is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5535" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <biological_entity id="o5536" name="base" name_original="bases" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s14" value="enlarged" value_original="enlarged" />
        <character is_modifier="true" name="shape" src="d0_s14" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5537" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <relation from="o5534" id="r609" name="part_of" negation="false" src="d0_s14" to="o5535" />
      <relation from="o5534" id="r610" name="part_of" negation="false" src="d0_s14" to="o5537" />
    </statement>
    <statement id="d0_s15">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o5538" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments sparsely pilose proximally or glabrous.</text>
      <biological_entity id="o5539" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes dark-brown, 3-gonous, 1.5–2 mm, glabrous.</text>
      <biological_entity id="o5540" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun and Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" constraint=" -Aug-Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats and gravelly washes, mixed grassland, saltbush, creosote bush, and mesquite communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="gravelly washes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="mesquite communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>160.</number>
  <other_name type="common_name">Arizona wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum arizonicum is widely distributed in scattered and isolated populations across central Arizona from extreme eastern Mohave and southern Yavapai counties through northern Maricopa County to southern Gila and northern Pinal counties. It rarely is common in any location. Limited field observations suggest that some individuals in a population may be first-year flowering perennials.</discussion>
  
</bio:treatment>