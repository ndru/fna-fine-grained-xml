<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">449</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Quintaria</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1880" rank="species">spinosa</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 481. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus quintaria;species spinosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060097</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonella</taxon_name>
    <taxon_name authority="(S. Watson) Goodman" date="unknown" rank="species">spinosa</taxon_name>
    <taxon_hierarchy>genus Eriogonella;species spinosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants spreading to prostrate, 0.3–0.8 (–1) × 0.5–8 dm.</text>
      <biological_entity id="o12561" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="prostrate" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s0" to="0.8" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o12562" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–2 cm;</text>
      <biological_entity id="o12563" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade (0.3–) 0.5–1.5 (–2) × (3–) 5–10 (–12) mm, thinly pubescent adaxially, more densely so to tomentose abaxially.</text>
      <biological_entity id="o12564" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_length" src="d0_s3" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s3" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="thinly; adaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences greenish to reddish, mostly flat-topped and open to dense;</text>
      <biological_entity id="o12565" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s4" to="reddish" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 3, whorled, short-petiolate, linear-lanceolate to lanceolate, acerose, 0.5–1.5 cm × 3–8 (–10) mm, awns straight, 1–3.5 mm.</text>
      <biological_entity id="o12566" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12567" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres usually congested in small terminal clusters of 1–3 at node of dichotomies, (4–) 5-ribbed, weakly 3-angled, 2–2.5 mm, not corrugate, densely canescent;</text>
      <biological_entity id="o12568" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="in terminal clusters" constraintid="o12569" is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s6" value="(4-)5-ribbed" value_original="(4-)5-ribbed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s6" value="3-angled" value_original="3-angled" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o12569" name="cluster" name_original="clusters" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character char_type="range_value" constraint="at node" constraintid="o12570" from="1" modifier="of" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o12570" name="node" name_original="node" src="d0_s6" type="structure" />
      <biological_entity id="o12571" name="dichotomy" name_original="dichotomies" src="d0_s6" type="structure" />
      <relation from="o12570" id="r1375" name="part_of" negation="false" src="d0_s6" to="o12571" />
    </statement>
    <statement id="d0_s7">
      <text>teeth (4–) 5, essentially erect with longer, prominent, and thickened anterior one 2–4 mm, with straight awn 1–2.5 mm, remaining teeth smaller, 0.5–1 mm, with straight awns 0.3–0.8 mm.</text>
      <biological_entity id="o12572" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
        <character constraint="with anterior one" constraintid="o12573" is_modifier="false" modifier="essentially" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="anterior" id="o12573" name="one" name_original="one" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12574" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12575" name="tooth" name_original="teeth" src="d0_s7" type="structure" />
      <biological_entity id="o12576" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o12572" id="r1376" name="with" negation="false" src="d0_s7" to="o12574" />
      <relation from="o12572" id="r1377" name="remaining" negation="false" src="d0_s7" to="o12575" />
      <relation from="o12572" id="r1378" name="with" negation="false" src="d0_s7" to="o12576" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers 1, exserted;</text>
      <biological_entity id="o12577" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth, cylindric, 2.5–3.5 mm;</text>
      <biological_entity id="o12578" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals connate 1/2–2/3 their length, dimorphic, entire, those of outer whorl spreading, broadly obovate and rounded apically, those of inner whorl erect, narrowly oblanceolate, 1/2 length of outer ones, acute apically;</text>
      <biological_entity id="o12579" name="tepal" name_original="tepals" src="d0_s10" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="length" src="d0_s10" to="2/3" />
        <character is_modifier="false" name="growth_form" src="d0_s10" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character name="length" src="d0_s10" value="1/2 length of outer whorl ones" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12580" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o12581" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <relation from="o12579" id="r1379" name="part_of" negation="false" src="d0_s10" to="o12580" />
      <relation from="o12579" id="r1380" name="part_of" negation="false" src="d0_s10" to="o12581" />
    </statement>
    <statement id="d0_s11">
      <text>stamens slightly exserted;</text>
      <biological_entity id="o12582" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 2.5–3 mm, glabrous;</text>
      <biological_entity id="o12583" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers yellowish, oblong, 0.5–0.7 mm.</text>
      <biological_entity id="o12584" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 2.5–3 mm. 2n = (40), 44, (46).</text>
      <biological_entity id="o12585" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12586" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="atypical_quantity" src="d0_s14" value="40" value_original="40" />
        <character name="quantity" src="d0_s14" value="44" value_original="44" />
        <character name="atypical_quantity" src="d0_s14" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, saltbush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="saltbush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Mojave spineflower</other_name>
  <discussion>G. J. Goodman (1934) referred Chorizanthe spinosa to Eriogonella, but C. spinosa and C. membranacea, the type of Eriogonella, are well isolated from one another, and both are well removed from the remainder of the annual spineflowers. The Mojave spineflower is local and uncommon from southeastern Kern and southern Inyo counties, south into adjacent northeastern Los Angeles and northwestern San Bernardino counties to Antelope and Lucerne valleys.</discussion>
  
</bio:treatment>