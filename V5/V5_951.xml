<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="S. Watson" date="1877" rank="species">valida</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 271. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species valida;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060101</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect to spreading, 1–3 × 1–6 dm, villous.</text>
      <biological_entity id="o6949" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal or nearly so;</text>
      <biological_entity id="o6950" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character name="position" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–3 cm;</text>
      <biological_entity id="o6951" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly oblanceolate, 1–2.5 (–5) × 0.4–0.8 (–1.2) cm, usually villous.</text>
      <biological_entity id="o6952" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with secondary branches suppressed, grayish;</text>
      <biological_entity id="o6953" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o6954" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="suppressed" value_original="suppressed" />
      </biological_entity>
      <relation from="o6953" id="r756" name="with" negation="false" src="d0_s4" to="o6954" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, similar to proximal leaf-blades only reduced, short-petiolate, becoming linear and aciculate at distal nodes, acerose, 1–3 cm × 6–10 mm, awns absent.</text>
      <biological_entity id="o6955" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6956" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="only" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" modifier="becoming" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character constraint="at distal nodes" constraintid="o6957" is_modifier="false" name="coloration" src="d0_s5" value="aciculate" value_original="aciculate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6957" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o6958" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1, grayish, cylindric, not ventricose,3–4 (–4.5) mm, with white, scarious margins between teeth, finely corrugate, thinly pubescent;</text>
      <biological_entity id="o6959" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="arrangement_or_relief" notes="" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="between teeth" constraintid="o6961" id="o6960" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="between  teeth, ">
        <character is_modifier="true" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o6961" name="tooth" name_original="teeth" src="d0_s6" type="structure" />
      <relation from="o6959" id="r757" name="with" negation="false" src="d0_s6" to="o6960" />
    </statement>
    <statement id="d0_s7">
      <text>teeth erect, equal, 0.3–0.7 (–1) mm;</text>
      <biological_entity id="o6962" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns straight, with longer ones 0.7–1.3 mm and anterior one mostly 1.3 mm, these alternating with shorter, 0.5–1 (–1.2) mm ones.</text>
      <biological_entity id="o6963" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="position" src="d0_s8" value="anterior" value_original="anterior" />
        <character modifier="mostly" name="some_measurement" src="d0_s8" unit="mm" value="1.3" value_original="1.3" />
        <character constraint="with shorter , 0.5-1(-1.2) mm ones" is_modifier="false" name="arrangement" src="d0_s8" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity constraint="longer" id="o6964" name="one" name_original="ones" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
      <relation from="o6963" id="r758" name="with" negation="false" src="d0_s8" to="o6964" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers exserted;</text>
      <biological_entity id="o6965" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth bicolored with floral-tube white and tepals white to lavender or rose, cylindric, (4–) 5–6 mm, pubescent on proximal 1/2;</text>
      <biological_entity id="o6966" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character constraint="with floral-tube" constraintid="o6967" is_modifier="false" name="coloration" src="d0_s10" value="bicolored" value_original="bicolored" />
      </biological_entity>
      <biological_entity id="o6967" name="floral-tube" name_original="floral-tube" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o6968" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="lavender or rose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character constraint="on proximal 1/2" constraintid="o6969" is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6969" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>tepals connate 1/4 their length, dimorphic, oblong, truncate and erose to denticulate, sometimes individual lobes entire, 2-lobed or even cuspidate apically, those of outer lobes longer and wider than inner ones;</text>
      <biological_entity id="o6970" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="individual" id="o6971" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" modifier="even; apically" name="architecture_or_shape" src="d0_s11" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
        <character constraint="than inner lobes" constraintid="o6973" is_modifier="false" name="width" src="d0_s11" value="longer and wider" value_original="longer and wider" />
      </biological_entity>
      <biological_entity constraint="outer" id="o6972" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o6973" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o6971" id="r759" name="part_of" negation="false" src="d0_s11" to="o6972" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 9, included;</text>
      <biological_entity id="o6974" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="9" value_original="9" />
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 2–4.5 mm, glabrous;</text>
      <biological_entity id="o6975" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers pink to red or maroon, oblong, 0.6–0.8 (–1) mm.</text>
      <biological_entity id="o6976" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="red or maroon" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes light-brown, lenticular-globose, 3–4.5 mm.</text>
      <biological_entity id="o6977" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lenticular-globose" value_original="lenticular-globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy places, coastal grassland communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy places" />
        <character name="habitat" value="coastal grassland communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Sonoma spineflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Chorizanthe valida may be distinguished by the highly colored involucre. The teeth and bases of awns are bright red. The awns then quickly transform to a bright ivory and this color dominates nearly the length of each awn. In the more inland populations (now extirpated), the awns observed in the old collections appear to be a straw color. It is not known if this is an artifact of age or potentially significant. Sonoma spineflower is now known only from grassy fields south of Abbott’s Lagoon in the Point Reyes area of Marin County (L. Davis and R. J. Sherman 1990, 1992). The last collection from Sonoma County was made at Sebastopol in 1907. The type, collected in 1841, apparently was gathered near Fort Ross, also in Sonoma County. This species is federally listed as endangered.</discussion>
  
</bio:treatment>