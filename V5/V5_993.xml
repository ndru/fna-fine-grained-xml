<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">473</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">446</other_info_on_meta>
    <other_info_on_meta type="mention_page">475</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="A. Gray ex Bentham in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="genus">CENTROSTEGIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 27. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus centrostegia;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kentron, spur and stegion, roof, alluding to arched saccate spurs at base of involucre</other_info_on_name>
    <other_info_on_name type="fna_id">106062</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="R. Brown ex Bentham" date="unknown" rank="genus">Chorizanthe</taxon_name>
    <taxon_name authority="(A. Gray) Parry" date="unknown" rank="section">Centrostegia</taxon_name>
    <taxon_hierarchy>genus Chorizanthe;section Centrostegia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
      <biological_entity id="o9765" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o9766" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems arising directly from the root, erect to spreading, solid, not fistulose or disarticulating into ringlike segments, sparsely glandular.</text>
      <biological_entity id="o9767" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from root" constraintid="o9768" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character constraint="into segments" constraintid="o9769" is_modifier="false" name="architecture" src="d0_s2" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" notes="" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9768" name="root" name_original="root" src="d0_s2" type="structure" />
      <biological_entity id="o9769" name="segment" name_original="segments" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ringlike" value_original="ringlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually quickly deciduous, basal, rosulate;</text>
      <biological_entity id="o9770" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually quickly" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9771" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole present;</text>
      <biological_entity id="o9772" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong to broadly spatulate, margins entire.</text>
      <biological_entity id="o9773" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="broadly spatulate" />
      </biological_entity>
      <biological_entity id="o9774" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, cymose, uniparous due to suppression of secondaries;</text>
      <biological_entity id="o9775" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character constraint="of secondaries" constraintid="o9776" is_modifier="false" name="architecture" src="d0_s6" value="uniparous" value_original="uniparous" />
      </biological_entity>
      <biological_entity id="o9776" name="secondary" name_original="secondaries" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>branches dichotomous, not brittle or disarticulating into segments, solid, sparsely glandular;</text>
      <biological_entity id="o9777" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s7" value="brittle" value_original="brittle" />
        <character constraint="into segments" constraintid="o9778" is_modifier="false" name="architecture" src="d0_s7" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9778" name="segment" name_original="segments" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, positioned to side of node, connate ca. 1/4 their length, linear to linear-lanceolate, commonly acerose, awned, sparsely glandular.</text>
      <biological_entity id="o9779" name="bract" name_original="bracts" src="d0_s8" type="structure" constraint="node" constraint_original="node; node">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character name="length" src="d0_s8" value="1/4" value_original="1/4" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="linear-lanceolate" />
        <character is_modifier="false" modifier="commonly" name="shape" src="d0_s8" value="acerose" value_original="acerose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="awned" value_original="awned" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o9780" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o9781" name="node" name_original="node" src="d0_s8" type="structure" />
      <relation from="o9779" id="r1081" name="positioned to" negation="false" src="d0_s8" to="o9780" />
      <relation from="o9779" id="r1082" name="part_of" negation="false" src="d0_s8" to="o9781" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent.</text>
      <biological_entity id="o9782" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres 1 per node, tubular, prismatic, 3-angled, 3-awned basally on saccate lobes;</text>
      <biological_entity id="o9783" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character constraint="per node" constraintid="o9784" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-angled" value_original="3-angled" />
        <character constraint="on lobes" constraintid="o9785" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o9784" name="node" name_original="node" src="d0_s10" type="structure" />
      <biological_entity id="o9785" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, awn-tipped, awns straight.</text>
      <biological_entity id="o9786" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o9787" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 2 per involucre;</text>
      <biological_entity id="o9788" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character constraint="per involucre" constraintid="o9789" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9789" name="involucre" name_original="involucre" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>perianth white to pink, campanulate when open, cylindric when closed, pubescent proximally abaxially;</text>
      <biological_entity id="o9790" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pink" />
        <character is_modifier="false" modifier="when open" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="when closed" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="proximally abaxially; abaxially" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals 6, connate proximally, monomorphic, 2-lobed apically;</text>
      <biological_entity id="o9791" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 9;</text>
      <biological_entity id="o9792" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments free, glabrous;</text>
      <biological_entity id="o9793" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers pink to red, oblong.</text>
      <biological_entity id="o9794" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s17" to="red" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes mostly included, brown, not winged, 3-gonous, glabrous.</text>
      <biological_entity id="o9795" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s18" value="included" value_original="included" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s18" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o9796" name="seed" name_original="seeds" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>x = 19.</text>
      <biological_entity id="o9797" name="embryo" name_original="embryo" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o9798" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Red triangles</other_name>
  <discussion>Species 1.</discussion>
  <discussion>The involucres of Centrostegia are highly modified and unique within Chorizanthineae. First, the involucres have basal and terminal awns. Second, the tube of the involucre is composed of five connate bracts—as in other genera with tubular involucres—but here four bracts are connate their entire length as two pairs with each bract terminated by a short, flatteened, keeled, awned tooth. The fifth bract, positioned opposite the point of attachment on the branch, is terminated by a short, flattened, awned tooth that is equal in size to the others.</discussion>
  <references>
    <reference>Goodman, G. J. 1957. The genus Centrostegia, tribe Eriogoneae. Leafl. W. Bot. 8: 125–128.</reference>
  </references>
  
</bio:treatment>