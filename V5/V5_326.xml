<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">161</other_info_on_meta>
    <other_info_on_meta type="illustration_page">158</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dianthus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">barbatus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">barbatus</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus dianthus;species barbatus;subspecies barbatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060111</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not matted.</text>
      <biological_entity id="o31247" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple, 10–70 cm, glabrous.</text>
      <biological_entity id="o31248" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheath 3–6 mm, 1 (–2) times as long as stem diam.;</text>
      <biological_entity id="o31249" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o31250" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character constraint="stem" constraintid="o31251" is_modifier="false" name="length_or_diameter" src="d0_s2" value="1(-2) times as long as stem diam" />
      </biological_entity>
      <biological_entity id="o31251" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to ovate, (1.5–) 2.5–10 cm, green, margins finely ciliate.</text>
      <biological_entity id="o31252" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o31253" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o31254" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences usually dense, 5–20-flowered heads, or rarely flowers solitary, axillary;</text>
      <biological_entity id="o31255" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o31256" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="5-20-flowered" value_original="5-20-flowered" />
      </biological_entity>
      <biological_entity id="o31257" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts lanceolate, equaling or longer than calyx, herbaceous, apex acute;</text>
      <biological_entity id="o31258" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
        <character constraint="than calyx" constraintid="o31259" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o31259" name="calyx" name_original="calyx" src="d0_s5" type="structure" />
      <biological_entity id="o31260" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles 4 (or 6), green, ovate,3/4–11/4 times as long as calyx, herbaceous, apex long-aristate in distal 1/3 (–1/2).</text>
      <biological_entity id="o31261" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character constraint="calyx" constraintid="o31262" is_modifier="false" name="length" src="d0_s6" value="3/4-11/4 times as long as calyx" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o31262" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
      <biological_entity id="o31263" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character constraint="in distal 1/3(-1/2)" constraintid="o31264" is_modifier="false" name="shape" src="d0_s6" value="long-aristate" value_original="long-aristate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o31264" name="1/3(-1/2)" name_original="1/3(-1/2)" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.1–2 mm.</text>
      <biological_entity id="o31265" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers subsessile;</text>
      <biological_entity id="o31266" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx 40-veined, (10–) 12–19 (–21) mm, glabrous (lobe margins may ciliate), lobes ovate to short-tapered, 2–7 mm;</text>
      <biological_entity id="o31267" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="40-veined" value_original="40-veined" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="19" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="21" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="19" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31268" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="short-tapered" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals purple, red, spotted with white, or pink or white with (or without) dark center, bearded, 4–10 mm, apex dentate.</text>
      <biological_entity id="o31269" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="punct" value_original="punct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="spotted with white" value_original="spotted with white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="punct" value_original="punct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character constraint="with center" constraintid="o31270" is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s10" value="bearded" value_original="bearded" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31270" name="center" name_original="center" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o31271" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 10–13 mm, equaling calyx length.</text>
      <biological_entity id="o31272" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31273" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2–2.5 (–2.7) mm.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 30 (Europe).</text>
      <biological_entity id="o31274" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31275" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, roadsides, fallow fields, disturbed riverbanks, forest edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fallow fields" />
        <character name="habitat" value="disturbed riverbanks" />
        <character name="habitat" value="forest edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.S., Ont., Que., Sask.; Ala., Calif., Conn., Ga., Idaho, Ill., Ind., Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.C., Tenn., Tex., Vt., Va., Wash., W.Va., Wis.; Europe; e, sw Asia; introduced in Mexico, Central America, South America, se Asia (Java).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="se Asia (Java)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  <discussion>Subspecies barbatus is cultivated widely and both persists at old homesites and sometimes escapes to nearby fields, etc.</discussion>
  
</bio:treatment>