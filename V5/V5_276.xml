<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="1754" rank="genus">minuartia</taxon_name>
    <taxon_name authority="(Swartz) Hiern" date="1899" rank="species">stricta</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>37: 320. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus minuartia;species stricta;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060655</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spergula</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">stricta</taxon_name>
    <place_of_publication>
      <publication_title>Kongl. Vetensk. Acad. Nya Handl.</publication_title>
      <place_in_publication>20: 229. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spergula;species stricta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alsinanthe</taxon_name>
    <taxon_name authority="(Swartz) Reichenbach" date="unknown" rank="species">stricta</taxon_name>
    <taxon_hierarchy>genus Alsinanthe;species stricta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">stricta</taxon_name>
    <taxon_name authority="(Schleicher ex Lamarck &amp; de Candolle) B. Boivin" date="unknown" rank="variety">uliginosa</taxon_name>
    <taxon_hierarchy>genus Arenaria;species stricta;variety uliginosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Schleicher ex Lamarck &amp; de Candolle" date="unknown" rank="species">uliginosa</taxon_name>
    <taxon_hierarchy>genus Arenaria;species uliginosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose to mat-forming.</text>
      <biological_entity id="o8606" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="cespitose" name="growth_form" src="d0_s0" to="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots filiform to slightly thickened.</text>
      <biological_entity id="o8607" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="slightly" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect or procumbent, green, (0.8–) 3–12 cm, glabrous, internodes of all stems 0.5–10 times as long as leaves.</text>
      <biological_entity id="o8608" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="12" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8609" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o8611" is_modifier="false" name="length" src="d0_s2" value="0.5-10 times as long as leaves" />
      </biological_entity>
      <biological_entity id="o8610" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o8611" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o8609" id="r959" name="part_of" negation="false" src="d0_s2" to="o8610" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves barely if at all overlapping, connate proximally, with tight, scarious to herbaceous sheath 0.2–0.5 mm;</text>
      <biological_entity id="o8612" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="at all overlapping; proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o8613" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s3" value="tight" value_original="tight" />
        <character char_type="range_value" from="scarious" is_modifier="true" name="texture" src="d0_s3" to="herbaceous" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o8612" id="r960" name="with" negation="false" src="d0_s3" to="o8613" />
    </statement>
    <statement id="d0_s4">
      <text>blade straight to slightly outward curved, green, flat, 1-veined, occasionally 3-veined abaxially, linear to linear-oblong or subulate, (2–) 4–10 × (0.3–) 0.5–1.5 mm, flexuous, margins not thickened, scarious, smooth, apex green or purple, mostly rounded, slightly navicular, shiny, glabrous;</text>
      <biological_entity id="o8614" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="occasionally; abaxially" name="architecture" src="d0_s4" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong or subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s4" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o8615" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8616" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="navicular" value_original="navicular" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary leaves present among proximal cauline leaves.</text>
      <biological_entity constraint="axillary" id="o8617" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal cauline" id="o8618" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o8617" id="r961" name="present among" negation="false" src="d0_s5" to="o8618" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1–3 (–5) -flowered, open cymes or flowers solitary, terminal;</text>
      <biological_entity id="o8619" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-3(-5)-flowered" value_original="1-3(-5)-flowered" />
      </biological_entity>
      <biological_entity id="o8620" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o8621" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts subulate, herbaceous.</text>
      <biological_entity id="o8622" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.3–3 cm, glabrous.</text>
      <biological_entity id="o8623" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium disc-shaped;</text>
      <biological_entity id="o8624" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8625" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="disc--shaped" value_original="disc--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 1–3-veined, often prominently, becoming ribbed in fruit, broadly elliptic to ovate (herbaceous portion broadly elliptic to ovate), (1.5–) 2.5–3.2 mm, to 4 mm in fruit, apex green to purple, acute to acuminate, not hooded, glabrous;</text>
      <biological_entity id="o8626" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8627" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-3-veined" value_original="1-3-veined" />
        <character constraint="in fruit" constraintid="o8628" is_modifier="false" modifier="often prominently; prominently; becoming" name="architecture_or_shape" src="d0_s10" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="broadly elliptic" name="shape" notes="" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8629" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8628" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o8629" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity id="o8630" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="purple" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals lanceolate to spatulate or orbiculate, (0.6–) 0.8–1 times as long as sepals, apex rounded, entire, or petals rudimentary or absent.</text>
      <biological_entity id="o8631" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8632" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="spatulate or orbiculate" />
        <character constraint="sepal" constraintid="o8633" is_modifier="false" name="length" src="d0_s11" value="(0.6-)0.8-1 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o8633" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o8634" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8635" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules on stipe ca. 0.1–0.2 mm, ovoid, 2.5–3.2 mm, shorter than or equaling sepals.</text>
      <biological_entity id="o8636" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
        <character constraint="than or equaling sepals" constraintid="o8638" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8637" name="stipe" name_original="stipe" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8638" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o8636" id="r962" name="on" negation="false" src="d0_s12" to="o8637" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown or reddish-brown, orbiculate with radicle prolonged into rounded bump, somewhat compressed, 0.4–0.6 mm, smooth or obscurely low-tuberculate (50×).</text>
      <biological_entity id="o8640" name="radicle" name_original="radicle" src="d0_s13" type="structure">
        <character constraint="into bump" constraintid="o8641" is_modifier="false" name="length" src="d0_s13" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o8641" name="bump" name_original="bump" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 22 (Europe), 26, 30 (Europe).</text>
      <biological_entity id="o8639" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
        <character constraint="with radicle" constraintid="o8640" is_modifier="false" name="shape" src="d0_s13" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="somewhat" name="shape" notes="" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s13" value="low-tuberculate" value_original="low-tuberculate" />
        <character name="quantity" src="d0_s13" value="[50" value_original="[50" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8642" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
        <character name="quantity" src="d0_s14" value="26" value_original="26" />
        <character name="quantity" src="d0_s14" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, granitic gravels, sedge meadows, heath, alpine or arctic tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granitic gravels" modifier="moist" />
        <character name="habitat" value="sedge meadows" />
        <character name="habitat" value="heath" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="arctic tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Ont., Que., Yukon; Alaska, Calif., Colo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Bog stitchwort</other_name>
  <other_name type="common_name">rock sandwort</other_name>
  <other_name type="common_name">minuartie raide</other_name>
  <discussion>Minuartia stricta is circumpolar and sometimes has been included within M. michauxii (e.g., B. Maguire 1958; H. J. Scoggan 1978–1979, part 3).</discussion>
  <discussion>Plants from alpine sites in California are, in spite of smaller stature and often smaller floral parts, Minuartia stricta. Colorado populations appear to vary widely in habit, but floral and fruit features match M. stricta.</discussion>
  
</bio:treatment>