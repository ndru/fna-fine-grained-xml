<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="illustration_page">366</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="1813" rank="species">flavum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">flavum</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;species flavum;variety flavum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060288</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flavum</taxon_name>
    <taxon_name authority="(Bentham) S. Stokes" date="unknown" rank="subspecies">crassifolium</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species flavum;subspecies crassifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flavum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">linguifolium</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species flavum;variety linguifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">flavum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">muticum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species flavum;variety muticum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tight, compact mats, 1–3 (–5) dm wide.</text>
      <biological_entity id="o7284" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s0" value="tight" value_original="tight" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7285" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems mostly erect, 0.1–2 dm.</text>
      <biological_entity id="o7286" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s1" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades usually elliptic, 1–5 × 0.3–1.5 cm, densely tomentose abaxially, tomentose to floccose and greenish adaxially.</text>
      <biological_entity id="o7287" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s2" to="floccose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o7288" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches 4–20 cm.</text>
      <biological_entity id="o7289" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate to campanulate, 3–8 mm.</text>
      <biological_entity id="o7290" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 3–7 mm, including 0.3–1 mm stipelike base;</text>
      <biological_entity id="o7291" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7292" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="true" name="shape" src="d0_s6" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o7291" id="r782" name="including" negation="false" src="d0_s6" to="o7292" />
    </statement>
    <statement id="d0_s7">
      <text>perianth bright-yellow.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 80.</text>
      <biological_entity id="o7293" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7294" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey or sandy to gravelly flats and slopes, mixed grassland and sagebrush communities, montane conifer woodlands, high-elevation sagebrush communities, subalpine or alpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey or sandy to gravelly flats" />
        <character name="habitat" value="clayey or sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="montane conifer woodlands" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Colo., Mont., Nebr., N.Dak., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>133a.</number>
  <other_name type="common_name">Alpine golden wild buckwheat</other_name>
  <discussion>Variety flavum is widespread and rather common on the short-grass prairies of the Great Plains from Alberta, southwestern Manitoba, and Saskatchewan south through North Dakota, South Dakota, Nebraska, and eastern Colorado, westward across the plains and into the mountains of Montana and Wyoming. It occasionally is cultivated and, while slow-growing, it will, given time, form nice mats, with a fair profusion of inflorescences bearing bright yellow flowers.</discussion>
  
</bio:treatment>