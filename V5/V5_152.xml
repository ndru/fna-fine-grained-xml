<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="illustration_page">65</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">moehringia</taxon_name>
    <taxon_name authority="(Linnaeus) Fenzl" date="1833" rank="species">lateriflora</taxon_name>
    <place_of_publication>
      <publication_title>Vers. Darstell. Alsin.,</publication_title>
      <place_in_publication>18, 38. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus moehringia;species lateriflora;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200007053</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">lateriflora</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 423. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species lateriflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lateriflora</taxon_name>
    <taxon_name authority="H. St. John" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Arenaria;species lateriflora;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lateriflora</taxon_name>
    <taxon_name authority="H. St. John" date="unknown" rank="variety">taylorae</taxon_name>
    <taxon_hierarchy>genus Arenaria;species lateriflora;variety taylorae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lateriflora</taxon_name>
    <taxon_name authority="Blankinship" date="unknown" rank="variety">tenuicaulis</taxon_name>
    <taxon_hierarchy>genus Arenaria;species lateriflora;variety tenuicaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o17299" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes forming extensive network.</text>
      <biological_entity id="o17300" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="extensive" value_original="extensive" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="network" value_original="network" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending or decumbent, often branched, terete, 5–30 cm, uniformly retrorsely pubesescent.</text>
      <biological_entity id="o17301" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile or subsessile;</text>
      <biological_entity id="o17302" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.1–1 mm;</text>
      <biological_entity id="o17303" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1–3-veined, broadly elliptic to oblongelliptic or oblanceolate, 6–30 (–35) × (2–) 5–10 (–17) mm, margins granular to minutely serrulate-ciliate, apex obtuse or rounded.</text>
      <biological_entity id="o17304" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3-veined" value_original="1-3-veined" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s5" to="oblongelliptic or oblanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17305" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="granular to minutely" value_original="granular to minutely" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="serrulate-ciliate" value_original="serrulate-ciliate" />
      </biological_entity>
      <biological_entity id="o17306" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1–5-flowered;</text>
      <biological_entity id="o17307" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-5-flowered" value_original="1-5-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–3 mm, margins scarious.</text>
      <biological_entity id="o17308" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17309" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect, 3–30 mm.</text>
      <biological_entity id="o17310" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 5, midrib not keeled, ovate or obovate, herbaceous portion oblong to elliptic, 1.7–2.8 (–3) mm, margins narrow, apex mostly obtuse or rounded;</text>
      <biological_entity id="o17311" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17312" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o17313" name="midrib" name_original="midrib" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o17314" name="portion" name_original="portion" src="d0_s9" type="structure">
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s9" value="herbaceous" value_original="herbaceous" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="elliptic" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17315" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o17316" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 5, 3–6 mm, ca. 2 times as long as sepals.</text>
      <biological_entity id="o17317" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17318" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o17319" is_modifier="false" name="length" src="d0_s10" value="2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o17319" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules subglobose, 3–5 mm, 11/2–2 times as long as sepals.</text>
      <biological_entity id="o17320" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character constraint="sepal" constraintid="o17321" is_modifier="false" name="length" src="d0_s11" value="11/2-2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o17321" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds reniform, 1 mm, smooth.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 48.</text>
      <biological_entity id="o17322" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="reniform" value_original="reniform" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17323" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist or dry woodlands, meadows, gravelly shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="dry woodlands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="gravelly shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Colo., Conn., Idaho, Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Grove or blunt-leaf sandwort</other_name>
  <other_name type="common_name">moehringie ou sabline latériflore</other_name>
  <discussion>Four varieties of Moehringia laterifolia have been described based on variation in leaf width and pubescence; they have been little used, and the variation appears not to be correlated with geography.</discussion>
  
</bio:treatment>