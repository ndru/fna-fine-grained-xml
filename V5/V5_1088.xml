<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="(Willkomm) Rechinger f." date="1954" rank="subgenus">Platypodium</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not. Suppl.,</publication_title>
      <place_in_publication>3(3): 106. 1954</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus platypodium;</taxon_hierarchy>
    <other_info_on_name type="fna_id">316719</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Willkomm" date="unknown" rank="section">Platypodium</taxon_name>
    <place_of_publication>
      <publication_title>in H. M. Willkomm and J. M. C. Lange, Prodr. Fl. Hispan.</publication_title>
      <place_in_publication>1: 284. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rumex;section Platypodium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Pau" date="unknown" rank="subgenus">Bucephalophora</taxon_name>
    <taxon_hierarchy>subgenus Bucephalophora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Nyman" date="unknown" rank="section">Heterolapathum</taxon_name>
    <taxon_hierarchy>genus Rumex;section Heterolapathum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants synoecious, with basal rosette of leaves usually not persistent at maturity.</text>
      <biological_entity id="o17976" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="synoecious" value_original="synoecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17977" name="rosette" name_original="rosette" src="d0_s0" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="usually not" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o17978" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <relation from="o17976" id="r2003" name="with" negation="false" src="d0_s0" to="o17977" />
      <relation from="o17977" id="r2004" name="part_of" negation="false" src="d0_s0" to="o17978" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, spreading, or almost prostrate, occasionally erect.</text>
      <biological_entity id="o17979" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="almost" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades spatulate, lanceolate, or ovatelanceolate, rarely rounded, base cuneate.</text>
      <biological_entity id="o17980" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o17981" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels articulated in distal part, usually heteromorphic: some curved, swollen, and clavate, others straight, not swollen, and cylindric.</text>
      <biological_entity id="o17982" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character constraint="in distal part" constraintid="o17983" is_modifier="false" name="architecture" src="d0_s3" value="articulated" value_original="articulated" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s3" value="heteromorphic" value_original="heteromorphic" />
        <character is_modifier="false" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="shape" src="d0_s3" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17983" name="part" name_original="part" src="d0_s3" type="structure" />
      <biological_entity id="o17984" name="other" name_original="others" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers normally bisexual;</text>
      <biological_entity id="o17985" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="normally" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>outer tepals not spreading;</text>
      <biological_entity constraint="outer" id="o17986" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner tepals enlarged, often heteromorphic, (1.5–) 2–4 (–5) mm, equaling or slightly wider and longer than achenes, margins usually dentate;</text>
      <biological_entity constraint="inner" id="o17987" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="heteromorphic" value_original="heteromorphic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s6" value="wider" value_original="wider" />
        <character constraint="than achenes" constraintid="o17988" is_modifier="false" name="length_or_size" src="d0_s6" value="slightly wider and longer" value_original="slightly wider and longer" />
      </biological_entity>
      <biological_entity id="o17988" name="achene" name_original="achenes" src="d0_s6" type="structure" />
      <biological_entity id="o17989" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles very small, or absent.</text>
      <biological_entity id="o17990" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; s Europe, sw Asia, n Africa; occasionally introduced in other regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Europe" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="occasionally  in other regions" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>26d.</number>
  <discussion>Species 1.</discussion>
  <discussion>Sometimes subg. Platypodium is recognized as a separate genus, Bucephalophora, together with other segregates from Rumex in the broad sense (see Á. Löve and B. M. Kapoor 1967).</discussion>
  
</bio:treatment>