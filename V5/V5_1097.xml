<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">polygonella</taxon_name>
    <taxon_name authority="Meisner in A. P. de Candolle and A. L. P. P. de Candolle" date="1856" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 80. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus polygonella;species gracilis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060713</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">gracile</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 255. 1818,</place_in_publication>
      <other_info_on_pub>not Salisbury 1796</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;species gracile;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, dioecious, 8–16 dm.</text>
      <biological_entity id="o5922" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character char_type="range_value" from="8" from_unit="dm" name="some_measurement" src="d0_s0" to="16" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, simple or branched proximally and distally, glabrous.</text>
      <biological_entity id="o5923" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly fugaceous;</text>
      <biological_entity id="o5924" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s2" value="fugaceous" value_original="fugaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ocrea margins not ciliate;</text>
      <biological_entity constraint="ocrea" id="o5925" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade filiform to broadly spatulate or, rarely, obovate, (9–) 19–39 (–65) × 0.8–5 (–8) mm, base attenuate, margins not hyaline, apex obtuse, glabrous.</text>
      <biological_entity id="o5926" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s4" to="broadly spatulate" />
        <character name="shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s4" to="19" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="39" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="65" to_unit="mm" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s4" to="39" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5927" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o5928" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o5929" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (13–) 20–40 (–70) mm;</text>
      <biological_entity id="o5930" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="70" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ocreola encircling rachis, only the base adnate to rachis, apex acute.</text>
      <biological_entity id="o5931" name="ocreolum" name_original="ocreola" src="d0_s6" type="structure" />
      <biological_entity id="o5932" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o5933" name="base" name_original="base" src="d0_s6" type="structure">
        <character constraint="to rachis" constraintid="o5934" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o5934" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o5935" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o5931" id="r648" name="encircling" negation="false" src="d0_s6" to="o5932" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels spreading in anthesis, sharply reflexed in fruit, 0.1–0.3 mm, as long as subtending ocreola.</text>
      <biological_entity id="o5936" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="in anthesis" is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="in fruit" constraintid="o5937" is_modifier="false" modifier="sharply" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5937" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o5938" name="ocreolum" name_original="ocreola" src="d0_s7" type="structure" />
      <relation from="o5936" id="r649" name="subtending" negation="false" src="d0_s7" to="o5938" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers functionally unisexual;</text>
      <biological_entity id="o5939" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments dimorphic;</text>
      <biological_entity id="o5940" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s9" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers deep red;</text>
      <biological_entity id="o5941" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="depth" src="d0_s10" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles and stigmas less than 0.1 mm in anthesis.</text>
      <biological_entity id="o5942" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in anthesis" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5943" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in anthesis" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: outer tepals loosely appressed in anthesis and fruit, white, elliptic, 1.1–2 mm in anthesis, margins entire;</text>
      <biological_entity id="o5944" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5945" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character constraint="in fruit" constraintid="o5946" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" constraint="in anthesis" from="1.1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5946" name="fruit" name_original="fruit" src="d0_s12" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o5947" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner tepals loosely appressed in anthesis and fruit, white, elliptic, 1–2 mm in anthesis, margins entire.</text>
      <biological_entity id="o5948" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5949" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character constraint="in fruit" constraintid="o5950" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s13" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" constraint="in anthesis" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5950" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o5951" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: outer tepals loosely appressed in anthesis, usually spreading in fruit, white to pink, often drying red and streaked or dotted orange or brown, elliptic to oblong, 0.8–1.4 mm in anthesis, margins entire or obscurely erose;</text>
      <biological_entity id="o5952" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5953" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character constraint="in anthesis" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character constraint="in fruit" constraintid="o5954" is_modifier="false" modifier="usually" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s14" to="pink" />
        <character is_modifier="false" modifier="often" name="condition" src="d0_s14" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red and streaked or dotted orange or brown" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s14" to="oblong" />
        <character char_type="range_value" constraint="in anthesis" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5954" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
      <biological_entity id="o5955" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s14" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>inner tepals loosely appressed in anthesis and fruit, white to pink, often drying yellow, broadly elliptic to ovate, 1.8–3.6 mm in flower, margins entire or obscurely erose.</text>
      <biological_entity id="o5956" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5957" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character constraint="in fruit" constraintid="o5958" is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s15" to="pink" />
        <character is_modifier="false" modifier="often" name="condition" src="d0_s15" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s15" to="ovate" />
        <character char_type="range_value" constraint="in flower" constraintid="o5959" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5958" name="fruit" name_original="fruit" src="d0_s15" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s15" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o5959" name="flower" name_original="flower" src="d0_s15" type="structure" />
      <biological_entity id="o5960" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s15" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes included or exserted, reddish-brown to yellowbrown, (2–) 3-gonous, 2–3.4 × 1–1.4 mm, shiny, smooth.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 22, 24.</text>
      <biological_entity id="o5961" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s16" to="yellowbrown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="(2-)3-gonous" value_original="(2-)3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s16" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5962" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pinelands, sandhills, sandy roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="sandhills" />
        <character name="habitat" value="sandy roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Slender wireweed</other_name>
  
</bio:treatment>