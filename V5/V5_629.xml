<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1809" rank="species">latifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>13(2): Eriogonum no. 3. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species latifolium;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060361</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or herbs, often scapose, much-branched and matted, 2–7 × 5–20 dm, usually tomentose to floccose, rarely glabrous.</text>
      <biological_entity id="o22134" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="7" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="usually tomentose" name="pubescence" src="d0_s0" to="floccose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="2" from_unit="dm" name="length" src="d0_s0" to="7" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="width" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="usually tomentose" name="pubescence" src="d0_s0" to="floccose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, with persistent leaf-bases, up to 1/4 height of plant;</text>
      <biological_entity id="o22136" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity id="o22137" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0 height of plant" name="height" src="d0_s1" to="1/4 height of plant" />
      </biological_entity>
      <relation from="o22136" id="r2471" name="with" negation="false" src="d0_s1" to="o22137" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems matted;</text>
      <biological_entity constraint="caudex" id="o22138" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems often scapelike, erect to spreading or decumbent, usually stout, solid, not fistulose, 2–6 dm, usually tomentose to floccose, rarely glabrous.</text>
      <biological_entity id="o22139" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s3" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading or decumbent" />
        <character is_modifier="false" modifier="usually" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="6" to_unit="dm" />
        <character char_type="range_value" from="usually tomentose" name="pubescence" src="d0_s3" to="floccose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline;</text>
      <biological_entity id="o22140" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 2–6 (–10) cm, tomentose;</text>
      <biological_entity id="o22141" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblong to ovate, (1.5–) 2.5–5 × 1.5–4 cm, white-lanate to tawny-tomentose on both surfaces, or tomentose to floccose or glabrous and green adaxially, margins plane, occasionally crisped.</text>
      <biological_entity id="o22142" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s6" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" constraint="on surfaces" constraintid="o22143" from="white-lanate" name="pubescence" src="d0_s6" to="tawny-tomentose" />
        <character char_type="range_value" from="tomentose" name="pubescence" notes="" src="d0_s6" to="floccose or glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o22143" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o22144" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s6" value="crisped" value_original="crisped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences capitate to umbellate or cymose, 3–40 × 2–20 cm;</text>
      <biological_entity id="o22145" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character char_type="range_value" from="capitate" name="architecture" src="d0_s7" to="umbellate or cymose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches usually tomentose to floccose, rarely glabrous;</text>
      <biological_entity id="o22146" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually tomentose" name="pubescence" src="d0_s8" to="floccose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts usually 3, leaflike, oblong to ovate, and 5–20 × 5–15 mm proximally, scalelike, triangular, and 2–5 mm distally.</text>
      <biological_entity id="o22147" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" modifier="proximally" name="width" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="distally" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o22148" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres (3–) 5–20 per cluster, turbinate, 3.5–5 (–6) × 2–4 mm, tomentose or glabrous;</text>
      <biological_entity id="o22149" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" constraint="per cluster" from="5" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–6, erect, 0.3–0.6 mm.</text>
      <biological_entity id="o22150" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 3–3.5 mm;</text>
      <biological_entity id="o22151" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white to pink or rose, glabrous;</text>
      <biological_entity id="o22152" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pink or rose" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, monomorphic, obovate;</text>
      <biological_entity id="o22153" name="tepal" name_original="tepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens exserted, 3–6 mm;</text>
      <biological_entity id="o22154" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o22155" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 3.5–4 mm, glabrous.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o22156" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22157" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy coastal flats, slopes, bluffs, and mesas, coastal scrub and grassland communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy coastal flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="grassland communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-80(-200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="80" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>92.</number>
  <other_name type="common_name">Seaside wild buckwheat</other_name>
  <discussion>Eriogonum latifolium is found along the immediate coast of southwest Oregon (Curry County) and western California (Alameda, Contra Costa, Del Norte, Humboldt, Marin, Mendocino, Monterey, San Francisco, San Luis Obispo, San Mateo, Santa Cruz, and Sonoma counties). The species is rather variable as to size and aspect, these depending to a considerable degree on exposure to on-shore winds. The flowering stems are rarely glabrous, but plants with this expression are always intermixed with plants having tomentose to floccose stems. The brilliantly white-lanate, spreading shrubs become rather globose in shape under cultivation, and as a result make an attractive addition to the garden, especially as the flowers wither through various shades of pink to rose. The species should be used much more than at present in places where cool summer temperatures, good moisture, and sandy soils are available.</discussion>
  <discussion>A decoction consisting of the roots, leaves, and stems of Eriogonum latifolium was taken by various Native American people along the California coast for colds and coughs (B. R. Bocek 1984; D. E. Moerman 1986). V. K. Chestnut (1902) reported that the native people of Mendocino County, California, used a decoction of the roots for stomach pain, “female complaints,” and sore eyes. The species is the food plant for the bramble hairstreak butterfly (Callophrys viridis), Mormon metalmark (Apodemia mormo), western square-dotted blue (Euphilotes comstocki comstocki), and the federally endangered Smith’s dotted-blue (Euphilotes enoptes smithi).</discussion>
  
</bio:treatment>