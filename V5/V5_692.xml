<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="subgenus">Oligogonum</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">umbellatum</taxon_name>
    <taxon_name authority="Reveal" date="2004" rank="variety">mohavense</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>86: 149. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oligogonum;species umbellatum;variety mohavense;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060562</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading mats, 0.5–2 × 1–3 dm.</text>
      <biological_entity id="o26400" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o26401" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems spreading to erect, (0.3–) 0.5–1.5 (–2) dm, floccose, without one or more leaflike bracts ca. midlength.</text>
      <biological_entity id="o26402" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o26403" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="position" src="d0_s1" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o26402" id="r2927" name="without" negation="false" src="d0_s1" to="o26403" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly in tight rosettes;</text>
      <biological_entity id="o26404" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o26405" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s2" value="tight" value_original="tight" />
      </biological_entity>
      <relation from="o26404" id="r2928" name="in" negation="false" src="d0_s2" to="o26405" />
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly elliptic, 0.7–2.5 × 0.3–1 cm, thinly floccose on both surfaces or glabrous adaxially, margins plane.</text>
      <biological_entity id="o26406" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character constraint="on " constraintid="o26408" is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o26407" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o26408" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26409" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity id="o26410" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o26408" id="r2929" name="on" negation="false" src="d0_s3" to="o26409" />
      <relation from="o26408" id="r2930" name="on" negation="false" src="d0_s3" to="o26410" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences umbellate;</text>
      <biological_entity id="o26411" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="umbellate" value_original="umbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 2.5–8 cm, thinly floccose or glabrous, without a whorl of bracts ca. midlength;</text>
      <biological_entity id="o26412" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26413" name="whorl" name_original="whorl" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o26414" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <relation from="o26412" id="r2931" name="without" negation="false" src="d0_s5" to="o26413" />
      <relation from="o26413" id="r2932" name="part_of" negation="false" src="d0_s5" to="o26414" />
    </statement>
    <statement id="d0_s6">
      <text>involucral tubes 2–3 mm, lobes 1.5–3 mm.</text>
      <biological_entity id="o26415" name="tube" name_original="tubes" src="d0_s6" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s6" value="involucral" value_original="involucral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26416" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–7 mm;</text>
      <biological_entity id="o26417" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>perianth bright-yellow.</text>
      <biological_entity id="o26418" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="bright-yellow" value_original="bright-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, sagebrush communities, oak, pinyon-juniper, and montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>107s.</number>
  <other_name type="common_name">Mohave sulphur flower</other_name>
  <discussion>Variety mohavense is known only from the Black Rock and Wolf Hole mountains area on the Arizona Strip of Mohave County, Arizona. The rays or branchlets of the inflorescences are rather long (2.5–8 cm). The taxon is related to the much more widely distributed, late-season-flowering var. subaridum found to the north and west.</discussion>
  
</bio:treatment>