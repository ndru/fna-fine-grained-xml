<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">cithariforme</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>23: 266. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species cithariforme;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060216</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">gracile</taxon_name>
    <taxon_name authority="(S. Watson) Munz" date="unknown" rank="variety">cithariforme</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species gracile;variety cithariforme;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="(S. Watson) S. Stokes" date="unknown" rank="variety">cithariforme</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;variety cithariforme;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading to erect, 2–3 (–5) dm, glabrous or tomentose, greenish or reddish.</text>
      <biological_entity id="o16395" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: aerial flowering-stems erect, 0.5–1 dm, glabrous or tomentose.</text>
      <biological_entity id="o16396" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16397" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and sometimes cauline;</text>
      <biological_entity id="o16398" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: petiole 1–5 cm, tomentose to floccose, often slightly winged, blade oblanceolate or elliptic to ovate or nearly rounded, 1–2 × 0.5–1.5 (–2) cm, tomentose abaxially, floccose to glabrate and grayish to greenish adaxially;</text>
      <biological_entity constraint="basal" id="o16399" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16400" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s3" to="floccose" />
        <character is_modifier="false" modifier="often slightly" name="architecture" src="d0_s3" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o16401" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate or nearly rounded" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s3" to="glabrate" />
        <character char_type="range_value" from="grayish" modifier="adaxially" name="coloration" src="d0_s3" to="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline: petiole 0.5–2 cm, floccose, blade elliptic, 0.3–1 cm × 0.2–0.7 cm, similar to basal blade.</text>
      <biological_entity constraint="cauline" id="o16402" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16403" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o16404" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s4" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16405" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, distally uniparous due to suppression of secondary branches, open, 5–25 × 5–25 cm;</text>
      <biological_entity id="o16406" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character constraint="of secondary branches" constraintid="o16407" is_modifier="false" modifier="distally" name="architecture" src="d0_s5" value="uniparous" value_original="uniparous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o16407" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches usually inwardly curved, glabrous or tomentose;</text>
      <biological_entity id="o16408" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually inwardly" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 0.5–2.5 × 0.5–2 mm.</text>
      <biological_entity id="o16409" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s7" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles absent.</text>
      <biological_entity id="o16410" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres somewhat appressed to branches, turbinate, 2.5–3 × 1.5–2 mm, glabrous;</text>
      <biological_entity id="o16411" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="to branches" constraintid="o16412" is_modifier="false" modifier="somewhat" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16412" name="branch" name_original="branches" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 5, erect, 0.2–0.4 mm.</text>
      <biological_entity id="o16413" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1.5–2 mm;</text>
      <biological_entity id="o16414" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth white to rose, glabrous;</text>
      <biological_entity id="o16415" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="rose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, oblong-obovate;</text>
      <biological_entity id="o16416" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-obovate" value_original="oblong-obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o16417" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o16418" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes brown, 3-gonous, 1–1.5 mm.</text>
      <biological_entity id="o16419" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>214.</number>
  <other_name type="past_name">citharaeforme</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The graceful, upwardly curved branch segments of Eriogonum cithariforme generally are distinctive in older plants, although an occasional individual of E. davidsonii may have this feature. A distinction between E. cithariforme and E. roseum also is troublesome, as in young material the curved branch segments are not always obvious.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches tomentose</description>
      <determination>214a Eriogonum cithariforme var. cithariforme</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches glabrous</description>
      <determination>214b Eriogonum cithariforme var. agninum</determination>
    </key_statement>
  </key>
</bio:treatment>