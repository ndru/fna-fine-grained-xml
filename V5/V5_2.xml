<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="Willdenow ex Schultes in J. J. Roemer et al." date="1819" rank="genus">DRYMARIA</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer et al.,Syst. Veg.</publication_title>
      <place_in_publication>5: xxxi, 406. 1819 </place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus drymaria;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek drymos, forest, alluding to habitat of at least one species</other_info_on_name>
    <other_info_on_name type="fna_id">110972</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, caudices often branched.</text>
      <biological_entity id="o18432" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o18433" name="caudex" name_original="caudices" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender, elongate.</text>
      <biological_entity id="o18434" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems sprawling to erect, simple or branching proximally or throughout, terete.</text>
      <biological_entity id="o18435" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="sprawling" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite or appearing whorled, connate by membranous to thickened line, petiolate or sessile, stipulate (D. pachyphylla not stipulate);</text>
      <biological_entity id="o18436" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character name="arrangement" src="d0_s3" value="appearing" value_original="appearing" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="whorled" value_original="whorled" />
        <character constraint="by line" constraintid="o18437" is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
      </biological_entity>
      <biological_entity id="o18437" name="line" name_original="line" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules 2 per node, white to tan, simple or divided into segments, subulate to filiform, often minute, margins entire, apex acute to acuminate;</text>
      <biological_entity id="o18438" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o18439" name="quantity" src="d0_s4" value="2" value_original="2" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s4" to="tan" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character constraint="into segments" constraintid="o18440" is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character char_type="range_value" from="subulate" name="shape" notes="" src="d0_s4" to="filiform" />
        <character is_modifier="false" modifier="often" name="size" src="d0_s4" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o18439" name="node" name_original="node" src="d0_s4" type="structure" />
      <biological_entity id="o18440" name="segment" name_original="segments" src="d0_s4" type="structure" />
      <biological_entity id="o18441" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18442" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1–5-veined, linear to lanceolate, spatulate, ovate, reniform, or orbiculate, not succulent, apex rounded to acuminate.</text>
      <biological_entity id="o18443" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-5-veined" value_original="1-5-veined" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate spatulate ovate reniform or orbiculate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate spatulate ovate reniform or orbiculate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate spatulate ovate reniform or orbiculate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate spatulate ovate reniform or orbiculate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="lanceolate spatulate ovate reniform or orbiculate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o18444" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary, open to congested, bracteate cymes or umbelliform clusters or flowers solitary, axillary;</text>
      <biological_entity id="o18445" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="open" name="architecture" src="d0_s6" to="congested" />
      </biological_entity>
      <biological_entity id="o18446" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="umbelliform" value_original="umbelliform" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o18447" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts paired, scarious or central portion herbaceous.</text>
      <biological_entity id="o18448" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="central" id="o18449" name="portion" name_original="portion" src="d0_s7" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect to spreading or reflexed.</text>
      <biological_entity id="o18450" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="spreading or reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth and androecium hypogynous;</text>
      <biological_entity id="o18451" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18452" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o18453" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, distinct, white, lanceolate to oblong, ovate, or orbiculate, 1.5–4.8 (–5) mm, herbaceous, margins white to purple, scarious, apex acuminate to rounded, hooded or not;</text>
      <biological_entity id="o18454" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18455" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblong ovate or orbiculate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblong ovate or orbiculate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblong ovate or orbiculate" />
        <character char_type="range_value" from="4.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.8" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o18456" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="purple" />
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o18457" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
        <character name="architecture" src="d0_s10" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals (3–) 5, sometimes absent, white, claw narrow, tapering distally or with oblong or expanded, sessile or short-clawed trunk, auricles absent, blade apex divided into 2 or 4 lobes;</text>
      <biological_entity id="o18458" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18459" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o18460" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s11" value="with oblong or expanded , sessile or short-clawed trunk" />
      </biological_entity>
      <biological_entity id="o18461" name="trunk" name_original="trunk" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="true" name="size" src="d0_s11" value="expanded" value_original="expanded" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="shape" src="d0_s11" value="short-clawed" value_original="short-clawed" />
      </biological_entity>
      <biological_entity id="o18462" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="blade" id="o18463" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character constraint="into 2 or 4lobes" is_modifier="false" name="shape" src="d0_s11" value="divided" value_original="divided" />
      </biological_entity>
      <relation from="o18460" id="r2067" name="with" negation="false" src="d0_s11" to="o18461" />
    </statement>
    <statement id="d0_s12">
      <text>nectaries at base of filaments opposite sepals;</text>
      <biological_entity id="o18464" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18465" name="nectary" name_original="nectaries" src="d0_s12" type="structure" />
      <biological_entity id="o18466" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o18467" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o18468" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o18465" id="r2068" name="at" negation="false" src="d0_s12" to="o18466" />
      <relation from="o18466" id="r2069" name="part_of" negation="false" src="d0_s12" to="o18467" />
      <relation from="o18466" id="r2070" name="part_of" negation="false" src="d0_s12" to="o18468" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 5;</text>
      <biological_entity id="o18469" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18470" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct or briefly connate proximally;</text>
      <biological_entity id="o18471" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o18472" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="briefly; proximally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, occasionally 2, connate proximally for 1/2 of length, rarely to nearly distinct (D. cordata), filiform, 0.1–0.3 mm, glabrous proximally;</text>
      <biological_entity id="o18473" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o18474" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character modifier="occasionally" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character constraint="for" is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character name="length" src="d0_s15" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="rarely to nearly" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s15" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas 3, occasionally 2, linear along adaxial surfaces of styles (or branches), obscurely papillate (30×).</text>
      <biological_entity id="o18475" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o18476" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character modifier="occasionally" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character constraint="along adaxial surfaces" constraintid="o18477" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="obscurely" name="relief" notes="" src="d0_s16" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s16" value="[30" value_original="[30" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18477" name="surface" name_original="surfaces" src="d0_s16" type="structure" />
      <biological_entity id="o18478" name="style" name_original="styles" src="d0_s16" type="structure" />
      <relation from="o18477" id="r2071" name="part_of" negation="false" src="d0_s16" to="o18478" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules ellipsoid to globose, opening by (2–) 3 spreading to recurved valves;</text>
      <biological_entity id="o18479" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s17" to="globose" />
      </biological_entity>
      <biological_entity id="o18480" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s17" to="3" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="spreading" is_modifier="true" name="orientation" src="d0_s17" to="recurved" />
      </biological_entity>
      <relation from="o18480" id="r2072" name="opening by" negation="false" src="d0_s17" to="o18480" />
    </statement>
    <statement id="d0_s18">
      <text>carpophore absent.</text>
      <biological_entity id="o18481" name="carpophore" name_original="carpophore" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 3–25, tan, reddish-brown, dark-brown, black, or transparent (white embryo visible), horseshoe, snail-shell or teardrop-shaped, compressed laterally, at least somewhat, tuberculate, marginal wing absent, appendage absent.</text>
      <biological_entity id="o18482" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s19" to="25" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="shape" src="d0_s19" value="horseshoe" value_original="horseshoe" />
        <character is_modifier="false" name="shape" src="d0_s19" value="snail-shell" value_original="snail-shell" />
        <character is_modifier="false" name="shape" src="d0_s19" value="teardrop--shaped" value_original="teardrop--shaped" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="at-least somewhat; somewhat" name="relief" src="d0_s19" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o18483" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>x = (11), 12.</text>
      <biological_entity id="o18484" name="appendage" name_original="appendage" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o18485" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="atypical_quantity" src="d0_s20" value="11" value_original="11" />
        <character name="quantity" src="d0_s20" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, West Indies, Central America, South America; introduced in Asia (Indonesia), e, s Africa, Australia, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Asia (Indonesia)" establishment_means="introduced" />
        <character name="distribution" value="e" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Drymary</other_name>
  <discussion>Species 48 (9 in the flora).</discussion>
  <discussion>Drymaria arenarioides Willdenow ex Schultes, alfombrilla, is on the Federal Noxious Weed List. It is highly toxic to livestock and is native in northwestern Mexico, where it has been reported within a few miles of the United States border.</discussion>
  <discussion>J. A. Duke (1961) proposed an infraspecific classification for Drymaria consisting of 17 “informal” series that he did not validly publish. Some of Duke’s series were cited by M. Escamilla and V. Sosa (2000), apparently assuming that they were “real” series.</discussion>
  <references>
    <reference>Duke, J. A. 1961. Preliminary revision of the genus Drymaria. Ann. Missouri Bot. Gard. 48: 173–268.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades of cauline leaves ovate, deltate, cordate, reniform, orbiculate, or suborbiculate, 4-25 mm wide, base obtuse or cordate to rounded</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades of cauline leaves linear to oblong or lanceolate, 0.2-3 mm wide, base briefly obtuse to attenuate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems erect or ascending; petals 2-fid, lobe apex not notched; seeds 0.5-0.7 mm</description>
      <determination>4 Drymaria glandulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems prostrate or sprawling; petals 2- or 4-fid, if 2-fid, lobe apex distinctly notched or seeds 1-1.5 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants glabrous, glaucous, succulent; petals 4-fid; leaves appearing whorled, not stipulate; inflorescences umbelliform clusters</description>
      <determination>8 Drymaria pachyphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants often glandular in inflorescence, not glaucous, herbaceous; petals 2-fid; leaves opposite, stipules present or deciduous in part; inflorescences open cymes</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals with lobe apex rounded to acute, 1-veined, vein not branched; seeds 1-1.5 mm, tubercles rounded</description>
      <determination>1 Drymaria cordata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals with lobe apex distinctly notched, 1-veined, vein dichotomously branched;seeds 0.5-0.7 mm, tubercles conic</description>
      <determination>5 Drymaria laxiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cauline leaves appearing whorled, at least in part</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Cauline leaves opposite</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants viscid; stems prostrate; sepals stipitate-glandular; inflorescences 4-7-flowered, axillary and terminal cymes</description>
      <determination>9 Drymaria viscosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants glabrous or sparsely glandular; stems erect; sepals glabrous; inflorescences 3-30+-flowered, terminal cymes initially becoming racemose</description>
      <determination>7 Drymaria molluginea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 11/ 1/ 2 times as long as sepals</description>
      <determination>3 Drymaria effusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 1/ 2-1 times as long as sepals</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Herbaceous portion of sepals ± oblong, apex blunt or rounded, veins ± parallel, apically confluent</description>
      <determination>2 Drymaria depressa</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Herbaceous portion of sepals lanceolate, apex acute to acuminate, veins with lateral pair distinctly arcing outward</description>
      <determination>6 Drymaria leptophylla</determination>
    </key_statement>
  </key>
</bio:treatment>