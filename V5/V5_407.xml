<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Desfontaines" date="unknown" rank="species">pseudatocion</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Atlant.</publication_title>
      <place_in_publication>1: 353. 1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species pseudatocion;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060880</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
      <biological_entity id="o12424" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o12425" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems straggling to erect, much-branched, elongate, 20–70 cm, sparsely retrorse-puberulent.</text>
      <biological_entity id="o12426" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="straggling" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="retrorse-puberulent" value_original="retrorse-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2 per node, sessile with spatulate, broad, ciliate base, blade oblanceolate, 1.3–5 cm × 4–15 mm, apex acuminate, glabrous or sparsely setose.</text>
      <biological_entity id="o12427" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="per node" constraintid="o12428" name="quantity" src="d0_s3" value="2" value_original="2" />
        <character constraint="with base" constraintid="o12429" is_modifier="false" name="architecture" notes="" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o12428" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o12429" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="true" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12430" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12431" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose, open, compound, pedunculate;</text>
      <biological_entity id="o12432" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts leaflike, lanceolate, 3–15 mm, apex acuminate;</text>
      <biological_entity id="o12433" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12434" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>peduncle ascending, elongate, viscid stipitate-glandular.</text>
      <biological_entity id="o12435" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="coating" src="d0_s6" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels ascending, elongate, viscid stipitate-glandular.</text>
      <biological_entity id="o12436" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="coating" src="d0_s7" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx prominently 10-veined, clavate, with long, slender tube surrounding carpophore, 17–20 × 4–6 mm, veins parallel, green or purple with purple stipitate-glands, with pale commissures, lobes lanceolate, 2–3 mm, margins ciliate, apex acute;</text>
      <biological_entity id="o12437" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12438" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s8" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" notes="" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="slender" id="o12439" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o12440" name="carpophore" name_original="carpophore" src="d0_s8" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s8" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity id="o12441" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character constraint="with stipitate-glands" constraintid="o12442" is_modifier="false" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o12442" name="stipitate-gland" name_original="stipitate-glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o12443" name="commissure" name_original="commissures" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o12444" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12445" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12446" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o12438" id="r1363" name="with" negation="false" src="d0_s8" to="o12439" />
      <relation from="o12441" id="r1364" name="with" negation="false" src="d0_s8" to="o12443" />
    </statement>
    <statement id="d0_s9">
      <text>corolla bright pink, clawed, claw slightly longer than calyx, limb obovate, unlobed, ca. 1 cm, appendages 2, oblong, 2 mm, entire;</text>
      <biological_entity id="o12447" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12448" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright pink" value_original="bright pink" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o12449" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character constraint="than calyx" constraintid="o12450" is_modifier="false" name="length_or_size" src="d0_s9" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o12450" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o12451" name="limb" name_original="limb" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="unlobed" value_original="unlobed" />
        <character name="some_measurement" src="d0_s9" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12452" name="appendage" name_original="appendages" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens included in tip of calyx-tube;</text>
      <biological_entity id="o12453" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12454" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o12455" name="tip" name_original="tip" src="d0_s10" type="structure" />
      <biological_entity id="o12456" name="calyx-tube" name_original="calyx-tube" src="d0_s10" type="structure" />
      <relation from="o12454" id="r1365" name="included in" negation="false" src="d0_s10" to="o12455" />
      <relation from="o12454" id="r1366" name="included in" negation="false" src="d0_s10" to="o12456" />
    </statement>
    <statement id="d0_s11">
      <text>styles 3, exserted.</text>
      <biological_entity id="o12457" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12458" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid, slightly longer than calyx, opening by 6 recurved teeth;</text>
      <biological_entity id="o12459" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character constraint="than calyx" constraintid="o12460" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o12460" name="calyx" name_original="calyx" src="d0_s12" type="structure" />
      <biological_entity id="o12461" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o12459" id="r1367" name="opening by" negation="false" src="d0_s12" to="o12461" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore 9–10 mm, pubescent.</text>
      <biological_entity id="o12462" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds dull and very dark-brown, almost globose, inrolled like a clenched fist, 1–1.3 mm, sides with radiating wrinkles, finely tuberculate abaxially.</text>
      <biological_entity id="o12463" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s14" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="very" name="coloration" src="d0_s14" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character constraint="like fist" constraintid="o12464" is_modifier="false" name="shape_or_vernation" src="d0_s14" value="inrolled" value_original="inrolled" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12464" name="fist" name_original="fist" src="d0_s14" type="structure" />
      <biological_entity id="o12466" name="wrinkle" name_original="wrinkles" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="radiating" value_original="radiating" />
      </biological_entity>
      <relation from="o12465" id="r1368" name="with" negation="false" src="d0_s14" to="o12466" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 24 (Balearic Islands).</text>
      <biological_entity id="o12465" name="side" name_original="sides" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="finely; abaxially" name="relief" notes="" src="d0_s14" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12467" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Neglected gardens, roadsides, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="neglected gardens" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe (Balearic Islands); n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe (Balearic Islands)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>46.</number>
  <other_name type="past_name">pseudo-atocion</other_name>
  <discussion>Silene pseudatocion is occasionally grown in gardens and rarely occurs as a weed in California. It is similar to another garden escape, S. pendula, but it differs in having a calyx tube with a very long, slender base and unlobed petals.</discussion>
  
</bio:treatment>