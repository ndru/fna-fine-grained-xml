<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Warren L. Wagner</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Reichenbach" date="1828" rank="genus">WILHELMSIA</taxon_name>
    <place_of_publication>
      <publication_title>Consp. Regn. Veg.,</publication_title>
      <place_in_publication>206. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus wilhelmsia;</taxon_hierarchy>
    <other_info_on_name type="etymology">possibly for Christian Wilhelms, fl. 1819–1837, plant collector in the Caucasus</other_info_on_name>
    <other_info_on_name type="fna_id">134902</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Fischer ex Chamisso &amp; Schlechtendal" date="unknown" rank="genus">Merckia</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>1: 59. 1826,</place_in_publication>
      <other_info_on_pub>not Merkia Borkhausen 1792</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Merckia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
      <biological_entity id="o12660" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes somewhat fleshy, often with prominent nodal buds.</text>
      <biological_entity id="o12661" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="nodal" id="o12662" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o12661" id="r1390" modifier="often" name="with" negation="false" src="d0_s1" to="o12662" />
    </statement>
    <statement id="d0_s2">
      <text>Taproots usually not evident.</text>
      <biological_entity id="o12663" name="taproot" name_original="taproots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s2" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems prostrate to decumbent, flowering-stems erect or ascending, simple or branched, subterete, main-stems often rooting at nodes.</text>
      <biological_entity id="o12664" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation_or_growth_form" src="d0_s3" value="prostrate to decumbent" value_original="prostrate to decumbent" />
      </biological_entity>
      <biological_entity id="o12665" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subterete" value_original="subterete" />
      </biological_entity>
      <biological_entity id="o12666" name="main-stem" name_original="main-stems" src="d0_s3" type="structure">
        <character constraint="at nodes" constraintid="o12667" is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o12667" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves not connate proximally, sessile;</text>
      <biological_entity id="o12668" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="fusion" src="d0_s4" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 1-veined, elliptic-obovate to ovate-rhombic or ovate, slightly succulent, apex weakly acuminate.</text>
      <biological_entity id="o12669" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="elliptic-obovate" name="shape" src="d0_s5" to="ovate-rhombic or ovate" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o12670" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, solitary flowers;</text>
      <biological_entity id="o12671" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o12672" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts absent.</text>
      <biological_entity id="o12673" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect in fruit.</text>
      <biological_entity id="o12674" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o12675" is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o12675" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: perianth and androecium weakly perigynous;</text>
      <biological_entity id="o12676" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12677" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o12678" name="androecium" name_original="androecium" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="position" src="d0_s9" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium minute, cupshaped;</text>
      <biological_entity id="o12679" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12680" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 5, distinct, green, lanceolate to ovate or elliptic, usually navicular, 2–2.5 mm, herbaceous, margins white to pink or purple, scarious, apex obtuse to acute or short-acuminate, not hooded;</text>
      <biological_entity id="o12681" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12682" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="ovate or elliptic" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="navicular" value_original="navicular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s11" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o12683" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink or purple" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o12684" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute or short-acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="hooded" value_original="hooded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5, white, not or narrowly clawed, blade apex entire to weakly emarginate;</text>
      <biological_entity id="o12685" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12686" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="not; narrowly" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity constraint="blade" id="o12687" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s12" to="weakly emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>nectaries as mounds around filaments opposite sepals, swollen on adaxial side;</text>
      <biological_entity id="o12688" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o12689" name="nectary" name_original="nectaries" src="d0_s13" type="structure">
        <character constraint="on adaxial side" constraintid="o12693" is_modifier="false" name="shape" notes="" src="d0_s13" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o12690" name="mound" name_original="mounds" src="d0_s13" type="structure" />
      <biological_entity id="o12691" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o12692" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12693" name="side" name_original="side" src="d0_s13" type="structure" />
      <relation from="o12689" id="r1391" name="as" negation="false" src="d0_s13" to="o12690" />
      <relation from="o12690" id="r1392" name="around" negation="false" src="d0_s13" to="o12691" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 10, arising from narrow disc;</text>
      <biological_entity id="o12694" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o12695" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
        <character constraint="from disc" constraintid="o12696" is_modifier="false" name="orientation" src="d0_s14" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o12696" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments distinct;</text>
      <biological_entity id="o12697" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o12698" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>staminodes absent;</text>
      <biological_entity id="o12699" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o12700" name="staminode" name_original="staminodes" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary 3-locular when young (incompletely 3-celled by inward extension of margins of carpels in fruit);</text>
      <biological_entity id="o12701" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o12702" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="when young" name="architecture_or_structure_in_adjective_form" src="d0_s17" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 3, narrowly clavate, 2.5–3 mm, glabrous proximally;</text>
      <biological_entity id="o12703" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o12704" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 3, subterminal, minutely papillate (30×).</text>
      <biological_entity id="o12705" name="flower" name_original="flowers" src="d0_s19" type="structure" />
      <biological_entity id="o12706" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s19" value="subterminal" value_original="subterminal" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s19" value="papillate" value_original="papillate" />
        <character name="quantity" src="d0_s19" value="[30" value_original="[30" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Capsules subglobose, inflated, 6-lobed, eventually opening along false septa, sometimes irregularly, into 3, 2-toothed parts;</text>
      <biological_entity id="o12707" name="capsule" name_original="capsules" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s20" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s20" value="6-lobed" value_original="6-lobed" />
      </biological_entity>
      <biological_entity constraint="false" id="o12708" name="septum" name_original="septa" src="d0_s20" type="structure" />
      <biological_entity id="o12709" name="part" name_original="parts" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s20" value="2-toothed" value_original="2-toothed" />
      </biological_entity>
      <relation from="o12707" id="r1393" modifier="eventually" name="opening along" negation="false" src="d0_s20" to="o12708" />
      <relation from="o12707" id="r1394" modifier="eventually; sometimes irregularly; irregularly" name="into" negation="false" src="d0_s20" to="o12709" />
    </statement>
    <statement id="d0_s21">
      <text>carpophore absent.</text>
      <biological_entity id="o12710" name="carpophore" name_original="carpophore" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 8–16, dark red or brown, subpyriform, laterally compressed, smooth, marginal wing absent, appendage absent.</text>
      <biological_entity id="o12711" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s22" to="16" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s22" value="subpyriform" value_original="subpyriform" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s22" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o12712" name="wing" name_original="wing" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>x = 15.</text>
      <biological_entity id="o12713" name="appendage" name_original="appendage" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o12714" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America, ne Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
        <character name="distribution" value="ne Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Merckia</other_name>
  <discussion>Species 1.</discussion>
  <discussion>Wilhelmsia always has been treated as an isolated monospecific genus because of its inflated, partially septate ovary, except by American authors who have included it in Arenaria. The resemblance in habit between Honckenya and Wilhelmsia, previously presumed to represent convergence, has proven to reflect a close relationship based on recent molecular studies (M. Nepokroeff et al., unpubl.).</discussion>
  
</bio:treatment>