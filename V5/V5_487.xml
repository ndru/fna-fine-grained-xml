<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Reveal &amp; Brotherson" date="1968" rank="species">hylophilum</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>27: 190, fig. 2. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species hylophilum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060335</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">corymbosum</taxon_name>
    <taxon_name authority="(Reveal &amp; Brotherson) S. L. Welsh" date="unknown" rank="variety">hylophilum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species corymbosum;variety hylophilum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, spreading, not scapose, 2.5–4 × 3–5 dm, tomentose, greenish.</text>
      <biological_entity id="o7701" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="length" src="d0_s0" to="4" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="width" src="d0_s0" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, often with persistent leaf-bases, up to 1/4 or more height of plant;</text>
      <biological_entity id="o7702" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o7703" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="1/4" />
      </biological_entity>
      <biological_entity id="o7704" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <relation from="o7702" id="r840" modifier="often" name="with" negation="false" src="d0_s1" to="o7703" />
    </statement>
    <statement id="d0_s2">
      <text>caudex stems somewhat matted;</text>
      <biological_entity constraint="caudex" id="o7705" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="somewhat" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>aerial flowering-stems erect or nearly so, somewhat stout, solid, not fistulose, 0.5–1.5 dm, tomentose.</text>
      <biological_entity id="o7706" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="location" src="d0_s3" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s3" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="somewhat" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline and sheathing proximal 1/3 of stem, 1 per node;</text>
      <biological_entity id="o7707" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character constraint="of stem" constraintid="o7708" name="quantity" src="d0_s4" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o7708" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character constraint="per node" constraintid="o7709" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7709" name="node" name_original="node" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–1 (–1.8) cm, tomentose;</text>
      <biological_entity id="o7710" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear-lanceolate to lanceolate, 3.5–7 (–9) × 0.3–0.6 (–0.8) cm, densely white-tomentose abaxially, less so and greenish adaxially, margins plane and slightly revolute, rarely crenulate.</text>
      <biological_entity id="o7711" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s6" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="less; adaxially" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o7712" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences cymose, open, 3–8 × 3–10 cm;</text>
      <biological_entity id="o7713" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches dichotomous, tomentose;</text>
      <biological_entity id="o7714" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="dichotomous" value_original="dichotomous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 3, scalelike, triangular, and 2–3 mm, or leaflike, 1–2 cm, and similar to leaf-blades.</text>
      <biological_entity id="o7715" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7716" name="leaf-blade" name_original="leaf-blades" src="d0_s9" type="structure" />
      <relation from="o7715" id="r841" name="to" negation="false" src="d0_s9" to="o7716" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles absent.</text>
      <biological_entity id="o7717" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres 1 per node or 2–3 per cluster, turbinate, 3.5–4 × 2.5–3 mm;</text>
      <biological_entity id="o7718" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character constraint="per node" constraintid="o7719" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" constraint="per cluster" from="2" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="false" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7719" name="node" name_original="node" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>teeth 5–6, erect, 0.5–1 mm.</text>
      <biological_entity id="o7720" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="6" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers (3–) 3.5–4 (–4.5) mm;</text>
      <biological_entity id="o7721" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth white, glabrous;</text>
      <biological_entity id="o7722" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>tepals connate proximal 1/4, slightly dimorphic, those of outer whorl spatulate, 1-3–1.7 (–2) mm wide, those of inner whorl oblanceolate, 0.6–0.9 (–1.2) mm wide;</text>
      <biological_entity id="o7723" name="tepal" name_original="tepals" src="d0_s15" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s15" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3-1.7" from_inclusive="false" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="3-1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="width" src="d0_s15" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s15" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o7724" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <biological_entity constraint="inner" id="o7725" name="whorl" name_original="whorl" src="d0_s15" type="structure" />
      <relation from="o7723" id="r842" name="part_of" negation="false" src="d0_s15" to="o7724" />
      <relation from="o7723" id="r843" name="part_of" negation="false" src="d0_s15" to="o7725" />
    </statement>
    <statement id="d0_s16">
      <text>stamens included, 2–3 (–3.5) mm;</text>
      <biological_entity id="o7726" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o7727" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 2.5–3 mm, glabrous except for slightly papillate beak.</text>
      <biological_entity id="o7728" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7729" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="true" modifier="slightly" name="relief" src="d0_s18" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly slopes, sagebrush communities, pinyon-juniper or montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Gate Canyon wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum hylophilum is known from a few, scattered locations in the Gate Canyon area and just west along the rim of the Badland Cliffs in Duchesne County. The species is closely related to E. corymbosum and approaches the minor segregate of the latter known as var. erectum. It is possible that the Gate Canyon wild buckwheat has an evolutionary history involving that expression of E. corymbosum and the white- to cream-colored flower expression of E. brevicaule var. laxifolium. If so, E. hylophilum is a stable and persistent taxon of hybrid origin.</discussion>
  
</bio:treatment>