<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1870" rank="species">nutans</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">nutans</taxon_name>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species nutans;variety nutans;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060431</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">deflexum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="subspecies">ultrum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species deflexum;subspecies ultrum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nutans</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">brevipedicellatum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species nutans;variety brevipedicellatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Peduncles glandular.</text>
      <biological_entity id="o22181" name="peduncle" name_original="peduncles" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Involucres glandular.</text>
    </statement>
    <statement id="d0_s2">
      <text>2n = 80.</text>
      <biological_entity id="o22182" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22183" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats and slopes, saltbush, greasewood, sagebrush, and mountain mahogany communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="greasewood" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="mountain mahogany communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2300(-2800) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2800" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>179a.</number>
  <other_name type="common_name">Dugway wild buckwheat</other_name>
  <discussion>Variety nutans is rare to infrequent throughout its range. It is most commonly encountered in Nevada and just enters Arizona (Mohave County), California (Mono County), Oregon (Harney County), and western Utah (Beaver, Juab, Millard, Piute, Sanpete, Sevier, and Tooele counties). It has been found east of Wellington, Carbon County, Utah (Ripley &amp; Barneby 8640, CAS, NY), although it has not been seen there since 1947.</discussion>
  
</bio:treatment>