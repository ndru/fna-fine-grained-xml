<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Harold R. Hinds†,Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="genus">FAGOPYRUM</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 1. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus fagopyrum;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin fagus, beech, and Greek pyrus, wheat, alluding to resemblance of the achene to a beech-nut</other_info_on_name>
    <other_info_on_name type="fna_id">112620</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o11679" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, glabrous or puberulent.</text>
      <biological_entity id="o11680" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, cauline, alternate, petiolate (proximal leaves) or sessile (distal leaves);</text>
      <biological_entity id="o11681" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o11682" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ocrea persistent or deciduous, chartaceous;</text>
      <biological_entity id="o11683" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole base articulated;</text>
      <biological_entity constraint="petiole" id="o11684" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="articulated" value_original="articulated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade cordate, triangular, hastate, or sagittate, margins entire to sinuate.</text>
      <biological_entity id="o11685" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sagittate" value_original="sagittate" />
      </biological_entity>
      <biological_entity id="o11686" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s6" to="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o11687" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary, or terminal and axillary, racemelike or paniclelike, pedunculate.</text>
      <biological_entity id="o11688" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s8" value="racemelike" value_original="racemelike" />
        <character is_modifier="false" name="shape" src="d0_s8" value="paniclelike" value_original="paniclelike" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual or, rarely, bisexual and staminate on same plant, 2–6 per ocreate fascicle, heterostylous or homostylous, base stipelike;</text>
      <biological_entity id="o11689" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character name="reproduction" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character constraint="on plant" constraintid="o11690" is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="heterostylous" value_original="heterostylous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="homostylous" value_original="homostylous" />
      </biological_entity>
      <biological_entity id="o11690" name="plant" name_original="plant" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="per fascicle" constraintid="o11691" from="2" name="quantity" src="d0_s9" to="6" />
      </biological_entity>
      <biological_entity id="o11691" name="fascicle" name_original="fascicle" src="d0_s9" type="structure" />
      <biological_entity id="o11692" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="stipelike" value_original="stipelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth nonaccrescent, white, pale-pink, or green, broadly campanulate, glabrous;</text>
      <biological_entity id="o11693" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="nonaccrescent" value_original="nonaccrescent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals 5, distinct, petaloid, dimorphic, outer smaller than inner;</text>
      <biological_entity id="o11694" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11695" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character constraint="than inner tepals" constraintid="o11696" is_modifier="false" name="size" src="d0_s11" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11696" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 8;</text>
      <biological_entity id="o11697" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, free, glabrous;</text>
      <biological_entity id="o11698" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers white, pink, or red, oval to elliptic;</text>
      <biological_entity id="o11699" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character char_type="range_value" from="oval" name="shape" src="d0_s14" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3, reflexed, distinct;</text>
      <biological_entity id="o11700" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas capitate.</text>
      <biological_entity id="o11701" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes strongly exserted, brown to dark-brown or gray, sometimes mottled black, unwinged or essentially so, bluntly to sharply 3-gonous, glabrous.</text>
      <biological_entity id="o11702" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="strongly" name="position" src="d0_s17" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="dark-brown or gray" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s17" value="mottled black" value_original="mottled black" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="unwinged" value_original="unwinged" />
        <character name="architecture" src="d0_s17" value="essentially" value_original="essentially" />
        <character is_modifier="false" modifier="bluntly to sharply" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds: embryo folded.</text>
      <biological_entity id="o11703" name="seed" name_original="seeds" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>x = 8.</text>
      <biological_entity id="o11704" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity constraint="x" id="o11705" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia, e Africa; introduced elsewhere, cultivated in temperate regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="e Africa" establishment_means="introduced" />
        <character name="distribution" value="elsewhere" establishment_means="introduced" />
        <character name="distribution" value="cultivated in temperate regions worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="common_name">Buckwheat</other_name>
  <other_name type="common_name">sarrasin</other_name>
  <discussion>Species 16 (2 in the flora).</discussion>
  <discussion>Fagopyrum esculentum and F. tataricum are cultivated widely. In North America, they often escape, but populations generally are ephemeral.</discussion>
  <discussion>Archaeological evidence for the cultivation of buckwheat dates to 4600 bp in China and 3500 bp in Japan (O. Ohnishi 1998). Molecular studies indicate that Fagopyrum comprises two major clades, with F. esculentum and F. tataricum in the large-fruited “cymosum” group (O. Ohnishi and Y. Matsuoka 1996; Y. Yasui and O. Ohnishi 1998, 1998b; O. Ohsako and O. Ohnishi 2000).</discussion>
  <references>
    <reference>Ohnishi, O. 1998. Search for the wild ancestor of buckwheat III. The wild ancestor of cultivated common buckwheat, and of Tartary buckwheat. Econ. Bot. 52: 123–133.</reference>
    <reference>Ohnishi, O. and Y. Matsuoka. 1996. Search for the wild ancestor of buckwheat II. Taxonomy of Fagopyrum (Polygonaceae) species based on morphology, isozymes and cpDNA variability. Genes Genet. Systems 71: 383–390.</reference>
    <reference>Ohsako, T. and O. Ohnishi. 2000. Intra- and interspecific phylogeny of wild Fagopyrum (Polygonaceae) species based on nucleotide sequences of noncoding regions of chloroplast DNA. Amer. J. Bot. 87: 573–582.</reference>
    <reference>Yasui, Y. and O. Ohnishi. 1998. Phylogenetic relationships among Fagopyrum species revealed by the nucleotide sequences of the ITS region of the nuclear rRNA gene. Genes Genet. Systems 73: 201–210.</reference>
    <reference>Yasui, Y. and O. Ohnishi. 1998b. Interspecific relationships in Fagopyrum (Polygonaceae) revealed by the nucleotide sequences of the rbcL and accD genes and their intergenic region. Amer. J. Bot. 85: 1134–1142.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achene faces smooth, angles smooth; tepals (2.5-)3-5 mm; perianths, creamy white topale pink; inflorescences paniclelike, 1-4 cm, terminal and axillary</description>
      <determination>1 Fagopyrum esculentum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Achene faces irregularly rugose, angles often sinuate-dentate; tepals 1.5-3 mm; perianths,green with whitish margins; inflorescences racemelike, 2-10 cm, axillary</description>
      <determination>2 Fagopyrum tataricum</determination>
    </key_statement>
  </key>
</bio:treatment>