<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">brevicaule</taxon_name>
    <taxon_name authority="Reveal" date="2004" rank="variety">caelitum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>86: 123. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species brevicaule;variety caelitum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060204</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, 0.6–1.8 (–2) × 1–2.5 dm.</text>
      <biological_entity id="o20456" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="length" src="d0_s0" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s0" to="2.5" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems erect or nearly so, scapelike, 0.5–1.5 dm, densely floccose to tomentose.</text>
      <biological_entity id="o20457" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s1" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="densely floccose" name="pubescence" src="d0_s1" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade linear to linear-oblanceolate, 0.2–4.5 (–5) × (0.2–) 0.3–0.6 (–0.7) cm, tomentose abaxially, thinly floccose and bright green adaxially, margins plane or slightly thickened.</text>
      <biological_entity id="o20458" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o20459" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="linear-oblanceolate" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s2" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_width" src="d0_s2" to="0.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
      </biological_entity>
      <biological_entity id="o20460" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences capitate, 1–2 cm;</text>
      <biological_entity id="o20461" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="capitate" value_original="capitate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches absent.</text>
      <biological_entity id="o20462" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 3–7 per cluster, turbinate to turbinate-campanulate, 2–3.5 (–4) × 2–3 (–3.5) mm, floccose to tomentose.</text>
      <biological_entity id="o20463" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per cluster" from="3" name="quantity" src="d0_s5" to="7" />
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="turbinate-campanulate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s5" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 2.5–3.5 (–4) mm;</text>
      <biological_entity id="o20464" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth yellow, glabrous.</text>
      <biological_entity id="o20465" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky, mostly limestone soil, high-elevation sagebrush communities, subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky" />
        <character name="habitat" value="gravelly to limestone soil" modifier="mostly" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27f.</number>
  <other_name type="common_name">Wasatch Plateau wild buckwheat</other_name>
  <discussion>Variety caelitum is restricted to the limestone Flagstaff Formation on the high mesa tops of the Wasatch Mountains in Sanpete and northern Sevier counties.</discussion>
  
</bio:treatment>