<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">stellaria</taxon_name>
    <taxon_name authority="Goldie" date="1822" rank="species">longipes</taxon_name>
    <taxon_name authority="(Raup) C. C. Chinnappa &amp; J. K. Morton" date="1991" rank="subspecies">arenicola</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>93: 132. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus stellaria;species longipes;subspecies arenicola;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060935</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellaria</taxon_name>
    <taxon_name authority="Raup" date="unknown" rank="species">arenicola</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>17: 248, plate 196. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stellaria;species arenicola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longipes</taxon_name>
    <taxon_name authority="(Raup) B. Boivin" date="unknown" rank="variety">arenicola</taxon_name>
    <taxon_hierarchy>genus Stellaria;species longipes;variety arenicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming loose clumps, with elongate, straggling stems.</text>
      <biological_entity id="o16769" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16770" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o16771" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="straggling" value_original="straggling" />
      </biological_entity>
      <relation from="o16769" id="r1867" name="forming" negation="false" src="d0_s0" to="o16770" />
      <relation from="o16769" id="r1868" name="with" negation="false" src="d0_s0" to="o16771" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades narrowly lanceolate.</text>
      <biological_entity id="o16772" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Capsules straw colored.</text>
    </statement>
    <statement id="d0_s3">
      <text>2n = 52.</text>
      <biological_entity id="o16773" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="straw colored" value_original="straw colored" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16774" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Inland sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" modifier="inland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18b.</number>
  <other_name type="common_name">Lake Athabasca starwort</other_name>
  <discussion>Confined to the extensive mobile sand dunes on the south side of Lake Athabasca, subsp. arenicola is largely self-pollinating but is interfertile with subsp. longipes, with which it intergrades in its natural habitat.</discussion>
  <discussion>Very rarely, individual plants of subsp. longipes with straw-colored capsules are encountered in other localities. These are probably due to the presence of a recessive gene for capsule color that is of widespread but rare occurrence in these populations. Only on the Lake Athabasca sand dunes have selective pressures been sufficient for it to evolve into a distinct biotype.</discussion>
  
</bio:treatment>