<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">469</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="(Torrey) Torrey &amp; A. Gray" date="1870" rank="section">Acanthogonum</taxon_name>
    <taxon_name authority="(Torrey) Torrey &amp; A. Gray" date="1870" rank="species">corrugata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 198. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section acanthogonum;species corrugata;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060067</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acanthogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">corrugatum</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>5(2): 364. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Acanthogonum;species corrugatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 0.3–1.5 × 0.3–1 dm, thinly tomentose.</text>
      <biological_entity id="o9854" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="width" src="d0_s0" to="1" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal or nearly so;</text>
      <biological_entity id="o9855" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character name="position" src="d0_s1" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–2 (–3) cm;</text>
      <biological_entity id="o9856" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade round-ovate, (0.5–) 0.8–1.5 (–2) × (0.3–) 0.5–1.5 (–2) cm, thinly floccose to tomentose.</text>
      <biological_entity id="o9857" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round-ovate" value_original="round-ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_width" src="d0_s3" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="thinly floccose" name="pubescence" src="d0_s3" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with involucres in small clusters 0.5–1 cm diam., green to tan or reddish;</text>
      <biological_entity id="o9858" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="diam" notes="" src="d0_s4" to="tan or reddish" />
      </biological_entity>
      <biological_entity id="o9859" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <relation from="o9858" id="r1096" name="with" negation="false" src="d0_s4" to="o9859" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, linear to linear-lanceolate, acicular, 2–7 cm × 1–2.5 mm, awns slightly curved, 0.5–1 mm.</text>
      <biological_entity id="o9860" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9861" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1, green to tan, cylindric, 3-angled but 3-ribbed, 3–4 mm, markedly transverse corrugate, glabrate;</text>
      <biological_entity id="o9862" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s6" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="3-ribbed" value_original="3-ribbed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="markedly" name="dehiscence_or_orientation" src="d0_s6" value="transverse" value_original="transverse" />
        <character is_modifier="false" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>teeth 3, equal, 2–4.5 mm;</text>
      <biological_entity id="o9863" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns uncinate, 0.6–1 mm.</text>
      <biological_entity id="o9864" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 1, included to slightly exserted;</text>
      <biological_entity id="o9865" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="included" name="position" src="d0_s9" to="slightly exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth white, cylindric, 2–2.5 mm, thinly pubescent abaxially;</text>
      <biological_entity id="o9866" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="thinly; abaxially" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate ca. 2/3 their length, monomorphic, oblong, acute, entire apically;</text>
      <biological_entity id="o9867" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6, slightly exserted;</text>
      <biological_entity id="o9868" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 0.8–1 mm, glabrous;</text>
      <biological_entity id="o9869" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers cream, ovate, 0.4–0.5 mm.</text>
      <biological_entity id="o9870" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="cream" value_original="cream" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, lenticular, 2.5–3 mm. 2n = 38.</text>
      <biological_entity id="o9871" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9872" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, mixed grassland, saltbush, creosote bush, and sagebrush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="sagebrush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-70-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="70" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Wrinkled spineflower</other_name>
  <discussion>Chorizanthe corrugata is found mainly in the Mojave and Sonoran deserts. The narrow, transversely corrugated involucral tube is diagnostic. Some anomalous flowers with four or eight stamens have been seen but this condition was always associated with other flowers bearing the normal number.</discussion>
  
</bio:treatment>