<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="illustration_page">486</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Necker ex Campderá" date="unknown" rank="genus">emex</taxon_name>
    <taxon_name authority="Steinheil" date="1838" rank="species">australis</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>2, 9: 195, plate 7. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus emex;species australis;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242100050</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–4 (–6) dm.</text>
      <biological_entity id="o31109" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, decumbent, or ascending, base often reddish, branched proximally.</text>
      <biological_entity id="o31110" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o31111" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: ocrea loose, glabrous;</text>
      <biological_entity id="o31112" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o31113" name="ocreum" name_original="ocrea" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole (0.5–) 1–8 (–15) cm, glabrous;</text>
      <biological_entity id="o31114" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o31115" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subhastate to elliptic or ovate, 1–10 × 0.5–6 cm, base truncate to cuneate, apex obtuse to acute.</text>
      <biological_entity id="o31116" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31117" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="subhastate" name="shape" src="d0_s4" to="elliptic or ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31118" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="cuneate" />
      </biological_entity>
      <biological_entity id="o31119" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers 1–8 per ocreate fascicle;</text>
      <biological_entity id="o31120" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o31121" from="1" name="quantity" src="d0_s5" to="8" />
      </biological_entity>
      <biological_entity id="o31121" name="fascicle" name_original="fascicle" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>tepals narrowly oblong to oblanceolate, 1.5–2 mm.</text>
      <biological_entity id="o31122" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers 1–4 per ocreate fascicle;</text>
      <biological_entity id="o31123" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o31124" from="1" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o31124" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>outer tepals ovate to oblong, 4–6 mm in fruit, inner tepals broadly-triangular-ovate, 5–6 mm in fruit, apex mucronate.</text>
      <biological_entity constraint="outer" id="o31125" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong" />
        <character char_type="range_value" constraint="in fruit" constraintid="o31126" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31126" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity constraint="inner" id="o31127" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="broadly-triangular-ovate" value_original="broadly-triangular-ovate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o31128" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31128" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting perianths 7–9 × 9–10 mm, spines ascending or spreading, 5–10 mm, base tapering.</text>
      <biological_entity id="o31129" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o31130" name="perianth" name_original="perianths" src="d0_s9" type="structure" />
      <biological_entity id="o31131" name="spine" name_original="spines" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31132" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o31129" id="r3520" name="fruiting" negation="false" src="d0_s9" to="o31130" />
    </statement>
    <statement id="d0_s10">
      <text>Achenes 4–6 × 2–3 mm, shiny.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 20.</text>
      <biological_entity id="o31133" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31134" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, especially in sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="sandy soils" modifier="especially in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Africa (Republic of South Africa); introduced in West Indies (Trinidad), Europe, Asia (India, Pakistan, Taiwan), Africa (Kenya, Madagascar, Malawi, Tanzania, Zimbabwe), Pacific Islands (Hawaii), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Africa (Republic of South Africa)" establishment_means="native" />
        <character name="distribution" value="in West Indies (Trinidad)" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia (India)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Pakistan)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Taiwan)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Kenya)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Malawi)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Tanzania)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Zimbabwe)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Doublegee</other_name>
  <other_name type="common_name">southern threecornerjack</other_name>
  <other_name type="common_name">spiny emex</other_name>
  
</bio:treatment>