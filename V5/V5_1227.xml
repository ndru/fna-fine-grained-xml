<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Claude Lefèbvre,Xavier Vekemans</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">plumbaginaceae</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="genus">ARMERIA</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.</publication_title>
      <place_in_publication>1: 333. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plumbaginaceae;genus ARMERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Celtic ar mor, at seaside, alluding to habitat</other_info_on_name>
    <other_info_on_name type="fna_id">102621</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbs, perennial, scapose, acaulescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, rootstocks branched, woody.</text>
      <biological_entity constraint="plants" id="o29136" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
      </biological_entity>
      <biological_entity id="o29137" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in basal rosettes, sessile;</text>
      <biological_entity id="o29138" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o29139" name="rosette" name_original="rosettes" src="d0_s2" type="structure" />
      <relation from="o29138" id="r3281" name="in" negation="false" src="d0_s2" to="o29139" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear to linear-spatulate [lanceolate], narrowed or straight to base, margins entire.</text>
      <biological_entity id="o29140" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="linear-spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character constraint="to base" constraintid="o29141" is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o29141" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o29142" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Scapes glabrous or densely pubescent, sometimes rugose, enclosed by tubular leafless sheath at apex.</text>
      <biological_entity id="o29143" name="scape" name_original="scapes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s4" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o29144" name="sheath" name_original="sheath" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="tubular" value_original="tubular" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="leafless" value_original="leafless" />
      </biological_entity>
      <biological_entity id="o29145" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o29143" id="r3282" name="enclosed by" negation="false" src="d0_s4" to="o29144" />
      <relation from="o29143" id="r3283" name="at" negation="false" src="d0_s4" to="o29145" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary, apical, dense hemispheric heads of scorpioid-cymes, each surrounded by involucre of scarious bracts.</text>
      <biological_entity id="o29146" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s5" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o29147" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="true" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o29148" name="scorpioid-cyme" name_original="scorpioid-cymes" src="d0_s5" type="structure" />
      <biological_entity id="o29149" name="involucre" name_original="involucre" src="d0_s5" type="structure" />
      <biological_entity id="o29150" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o29147" id="r3284" name="part_of" negation="false" src="d0_s5" to="o29148" />
      <relation from="o29147" id="r3285" name="surrounded by" negation="false" src="d0_s5" to="o29149" />
      <relation from="o29147" id="r3286" name="surrounded by" negation="false" src="d0_s5" to="o29150" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels absent or present (short).</text>
      <biological_entity id="o29151" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers monomorphic or dimorphic (in pollen and stigma characteristics);</text>
      <biological_entity id="o29152" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="growth_form" src="d0_s7" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyx 10-ribbed, funnel-shaped;</text>
      <biological_entity id="o29153" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="10-ribbed" value_original="10-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tube usually pubescent on ribs only or all around, rarely glabrous, limbs membranaceous, awned or not;</text>
      <biological_entity id="o29154" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character constraint="on ribs" constraintid="o29155" is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="around; rarely" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29155" name="rib" name_original="ribs" src="d0_s9" type="structure" />
      <biological_entity id="o29156" name="limb" name_original="limbs" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="membranaceous" value_original="membranaceous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
        <character name="architecture_or_shape" src="d0_s9" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals slightly connate basally, white to deep purple;</text>
      <biological_entity id="o29157" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly; basally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="deep purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments adnate to base of corolla;</text>
      <biological_entity id="o29158" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character constraint="to base" constraintid="o29159" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o29159" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o29160" name="corolla" name_original="corolla" src="d0_s11" type="structure" />
      <relation from="o29159" id="r3287" name="part_of" negation="false" src="d0_s11" to="o29160" />
    </statement>
    <statement id="d0_s12">
      <text>anthers included;</text>
      <biological_entity id="o29161" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 5, free, hairy proximally;</text>
      <biological_entity id="o29162" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas linear, papillate or smooth.</text>
      <biological_entity id="o29163" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits dry, enclosed in persistent calyces, dehiscing transversely.</text>
      <biological_entity id="o29165" name="calyx" name_original="calyces" src="d0_s15" type="structure">
        <character is_modifier="true" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o29164" id="r3288" name="enclosed in" negation="false" src="d0_s15" to="o29165" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o29164" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s15" value="dry" value_original="dry" />
        <character is_modifier="false" modifier="transversely" name="dehiscence" src="d0_s15" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity constraint="x" id="o29166" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, s South America, Europe, w Asia (n Siberia), n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w Asia (n Siberia)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Thrift</other_name>
  <discussion>Species ca. 50 (1 in the flora).</discussion>
  <discussion>Armeria is known to be taxonomically difficult. Species concepts vary among authors. About 50 species can be recognized according to A. R. Pinto da Silva (1972).</discussion>
  <references>
    <reference>Bernis, F. 1952. Revisión del género Armeria Willd. con especial referencia a los grupos Ibéricos. Anales Inst. Bot. Cavanilles 11(2): 5–287.</reference>
    <reference>Lawrence, G. H. M. 1940. Armerias, native and cultivated. Gentes Herb. 4: 391–418.</reference>
    <reference>Lawrence, G. H. M. 1947. The genus Armeria in North America. Amer. Midl. Naturalist 37: 751–779.</reference>
    <reference>Lefèbvre, C. 1974. Population variation and taxonomy in Armeria maritima with special reference to heavy-metal tolerant populations. New Phytol. 73: 209–219.</reference>
    <reference>Lefèbvre, C. and X. Vekemans. 1995. A numerical taxonomic study of Armeria maritima (Plumbaginaceae) in North America and Greenland. Canad. J. Bot. 73: 1583–1595.</reference>
  </references>
  
</bio:treatment>