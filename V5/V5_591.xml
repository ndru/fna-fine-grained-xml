<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze in T. E. von Post and O. Kuntze" date="1903" rank="subgenus">Eucycla</taxon_name>
    <taxon_name authority="Bentham" date="1836" rank="species">fasciculatum</taxon_name>
    <taxon_name authority="(Nuttall) S. Stokes ex Abrams" date="1910" rank="variety">foliolosum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. New York Bot. Gard.</publication_title>
      <place_in_publication>6: 351. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus eucycla;species fasciculatum;variety foliolosum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060284</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">rosmarinifolium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="variety">foliolosum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>4: 16. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species rosmarinifolium;variety foliolosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculatum</taxon_name>
    <taxon_name authority="(Nuttall) S. Stokes" date="unknown" rank="subspecies">foliolosum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species fasciculatum;subspecies foliolosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculatum</taxon_name>
    <taxon_name authority="S. Stokes" date="unknown" rank="variety">obtusiflorum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species fasciculatum;variety obtusiflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, rounded to erect, 6–15 × (8–) 10–25 dm.</text>
      <biological_entity id="o11821" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="dm" name="length" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="8" from_unit="dm" name="atypical_width" src="d0_s0" to="10" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="width" src="d0_s0" to="25" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Aerial flowering-stems thinly tomentose or glabrous.</text>
      <biological_entity id="o11822" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear-oblanceolate to oblanceolate, 0.6–1.2 × 0.1–0.4 cm, densely white-tomentose abaxially, less so to green and floccose adaxially, margins plane usually tightly revolute.</text>
      <biological_entity id="o11823" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s2" to="oblanceolate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s2" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s2" to="0.4" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s2" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="usually tightly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o11824" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s2" value="plane" value_original="plane" />
      </biological_entity>
      <relation from="o11823" id="r1317" modifier="less" name="to" negation="false" src="d0_s2" to="o11824" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences open and mostly cymose;</text>
      <biological_entity id="o11825" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s3" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches thinly tomentose or glabrous.</text>
      <biological_entity id="o11826" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 3–4 × (1.5–) 2–2.5 mm, pubescent.</text>
      <biological_entity id="o11827" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perianths pubescent at least proximally.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 80.</text>
      <biological_entity id="o11828" name="perianth" name_original="perianths" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="at-least proximally; proximally" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11829" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, mixed grassland and chaparral communities, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60-1300(-1600) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="60" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1600" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>80d.</number>
  <other_name type="common_name">Coastal California buckwheat</other_name>
  <discussion>Variety foliolosum is widespread and common to abundant, often being a dominant shrub in the chaparral in the Coast Ranges of California (Kern, Los Angeles, Monterey, Orange, Riverside, San Benito, San Bernardino, San Diego, San Joaquin, San Luis Obispo, San Mateo, Santa Barbara, Santa Clara, Santa Cruz, Stanislaus, and Ventura counties). It is an octoploid and may be the product of an ancient hybridization involving the coastal var. fasciculatum and desert var. polifolium.</discussion>
  <discussion>Variety foliolosum is being widely planted by the California Department of Transportation along roadsides, where it has hybridized with E. cinereum. As a very unfortunate result, the distribution of var. foliolosum has now expanded into northern California (Alameda, Marin, San Francisco, Trinity, and likely other countries) and even into Oregon (Jackson County). In southern Arizona, var. foliolosum has been introduced as a roadside plant in Maricopa County, and has been found (Bierner 90-45, ARIZ, TEX) escaped along a roadside in Graham County. Since this is a potentially aggressive weedy shrub, efforts should be made to curtail its introduction into areas outside its native range.</discussion>
  
</bio:treatment>