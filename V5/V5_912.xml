<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="illustration_page">436</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Small" date="1898" rank="genus">acanthoscyphus</taxon_name>
    <taxon_name authority="(Parry) Small" date="1898" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 53. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus acanthoscyphus;species parishii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060001</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oxytheca</taxon_name>
    <taxon_name authority="Parry" date="unknown" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Davenport Acad. Nat. Sci.</publication_title>
      <place_in_publication>3: 176. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Oxytheca;species parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (0.5–) 1–6 × 0.5–4 dm.</text>
      <biological_entity id="o13728" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_length" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 1–7 × 0.5–2 cm.</text>
      <biological_entity id="o13729" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s1" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 1–5 dm;</text>
      <biological_entity id="o13730" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts 1–4 (–10) × 0.5–3 mm, awns 0.2–1.5 mm.</text>
      <biological_entity id="o13731" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13732" name="awn" name_original="awns" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (0.3–) 1–5 (–7.5) cm.</text>
      <biological_entity id="o13733" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 1.5–2 mm, awns ivory or reddish, 2–5 mm.</text>
      <biological_entity id="o13734" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13735" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="ivory" value_original="ivory" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth 2–2.5 mm;</text>
      <biological_entity id="o13736" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13737" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2–2.5 mm;</text>
      <biological_entity id="o13738" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13739" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.3–0.6 mm.</text>
      <biological_entity id="o13740" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13741" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 1.7–2 mm.</text>
      <biological_entity id="o13742" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="s Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucral awns 7-16, dark red, 3-4 mm; San Rafael Mountains, Topatopa Mountain, and Mount Pinos, Santa Barbara and Ventura counties, California</description>
      <determination>1d Acanthoscyphus parishii var. abramsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucral awns 4-36, ivory colored, 2-5 mm; San Bernardino and Ventura counties</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucral awns 4(-5), 2-3 mm; ne side of San Bernardino Mountains, San Bernardino County, California</description>
      <determination>1c Acanthoscyphus parishii var. goodmaniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Involucral awns 7-30(-36), 3-5 mm; se side of e San Bernardino County, and Ventura County, California</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucral awns (10-)13-30(-36); inflorescence bracts triangular, awns 0.2-0.5 mm; Pine Mountain, Ventura County, e to c San Bernardino Mountains, San Bernardino County, California</description>
      <determination>1a Acanthoscyphus parishii var. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucral awns 7-10; inflorescence bracts subulate, awns 0.5-1.5 mm; se side of e San Bernardino Mountains, San Bernardino County, California</description>
      <determination>1b Acanthoscyphus parishii var. cienegensis</determination>
    </key_statement>
  </key>
</bio:treatment>