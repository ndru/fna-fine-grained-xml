<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">399</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Reveal" date="1968" rank="species">scabrellum</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>55: 74. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species scabrellum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060490</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, annual, 1–3 (–5) dm, floccose and scabrellous, greenish to reddish or reddish-brown.</text>
      <biological_entity id="o18361" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="relief" src="d0_s0" value="scabrellous" value_original="scabrellous" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s0" to="reddish or reddish-brown" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o18362" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o18363" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, 0.5–1.5 dm, floccose and scabrellous.</text>
      <biological_entity id="o18364" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18365" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, sheathing up stems 1–3 cm;</text>
      <biological_entity id="o18366" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="up stems" constraintid="o18367" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o18367" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–4 (–5) cm, floccose;</text>
      <biological_entity id="o18368" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade cordate, 1–3 (–4) × 1–3 (–4) cm, densely white-tomentose abaxially, sparsely floccose and greenish to green adaxially, margins crisped and wavy.</text>
      <biological_entity id="o18369" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s5" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character char_type="range_value" from="greenish" modifier="adaxially" name="coloration" src="d0_s5" to="green" />
      </biological_entity>
      <biological_entity id="o18370" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open, spreading, 5–40 × 20–100 cm;</text>
      <biological_entity id="o18371" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="width" src="d0_s6" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches scabrellous;</text>
      <biological_entity id="o18372" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–1.5 × 0.4–0.9 mm.</text>
      <biological_entity id="o18373" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s8" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles absent.</text>
      <biological_entity id="o18374" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres horizontal, turbinate, 1.5–2.5 × 1.5–2 mm, scabrellous;</text>
      <biological_entity id="o18375" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.4–0.6 mm.</text>
      <biological_entity id="o18376" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–1.5 mm;</text>
      <biological_entity id="o18377" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white with greenish midribs, becoming pink to rose or deep red, pustulose;</text>
      <biological_entity id="o18378" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character constraint="with midribs" constraintid="o18379" is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="pink" modifier="becoming" name="coloration" notes="" src="d0_s13" to="rose or deep red" />
        <character is_modifier="false" name="relief" src="d0_s13" value="pustulose" value_original="pustulose" />
      </biological_entity>
      <biological_entity id="o18379" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals dimorphic, those of outer whorl obovate, those of inner whorl ovate;</text>
      <biological_entity id="o18380" name="tepal" name_original="tepals" src="d0_s14" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18381" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <biological_entity constraint="inner" id="o18382" name="whorl" name_original="whorl" src="d0_s14" type="structure" />
      <relation from="o18380" id="r2064" name="part_of" negation="false" src="d0_s14" to="o18381" />
      <relation from="o18380" id="r2065" name="part_of" negation="false" src="d0_s14" to="o18382" />
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted, 1–1.5 mm;</text>
      <biological_entity id="o18383" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o18384" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes light-brown to brown, 3-gonous, 2 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o18385" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s17" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18386" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Clayey to gravelly washes, flats, and slopes, saltbush, blackbrush, and sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clayey to gravelly washes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="blackbrush" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>174.</number>
  <other_name type="common_name">Westwater wild buckwheat</other_name>
  <discussion>Eriogonum scabrellum is widely scattered along the Colorado and San Juan river systems. It occurs from Grand Valley of Utah (Grand County) and Colorado (Mesa County), southward to the Lake Powell region of Utah (Garfield and Kane counties), and then to the Four Corners area of Utah (San Juan County), Colorado (Montezuma County), and New Mexico (San Juan County). It recently was collected in Chaco Canyon National Park in New Mexico, and just south of the town of Green River in Emery County, Utah.</discussion>
  
</bio:treatment>