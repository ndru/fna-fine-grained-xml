<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler,Ronald L. Hartman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/17 21:02:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Vasc. Gen.</publication_title>
      <place_in_publication>1: 132</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <place_in_publication>2: 96. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily Paronychioideae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20602</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [small shrubs], annual, biennial, or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, not rhizomatous.</text>
      <biological_entity id="o299" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending or erect, simple or branched.</text>
      <biological_entity id="o300" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves opposite, distalmost or all sometimes alternate, bases connate or not, sometimes petiolate, stipulate;</text>
      <biological_entity id="o301" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="position" src="d0_s3" value="distalmost" value_original="distalmost" />
        <character name="position" src="d0_s3" value="all" value_original="all" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o302" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character name="fusion" src="d0_s3" value="not" value_original="not" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="stipulate" value_original="stipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules ovate or deltate to lanceolate or spatulate, scarious;</text>
      <biological_entity id="o303" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s4" to="lanceolate or spatulate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade needlelike or often spatulate to elliptic or suborbiculate, seldom succulent.</text>
      <biological_entity id="o304" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="needlelike" value_original="needlelike" />
        <character name="architecture_or_shape" src="d0_s5" value="often" value_original="often" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="elliptic or suborbiculate" />
        <character is_modifier="false" modifier="seldom" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary cymes or flowers solitary;</text>
      <biological_entity id="o305" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o306" name="cyme" name_original="cymes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o307" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts foliaceous or usually scarious;</text>
      <biological_entity id="o308" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>involucel bracteoles absent.</text>
      <biological_entity constraint="involucel" id="o309" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels present or flowers sessile.</text>
      <biological_entity id="o310" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o311" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual or sometimes unisexual (the plant then dioecious or polygamodioecious);</text>
      <biological_entity id="o312" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth and androecium perigynous;</text>
      <biological_entity id="o313" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o314" name="androecium" name_original="androecium" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium usually cupshaped or cylindric or conic to urceolate;</text>
      <biological_entity id="o315" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="cylindric or conic" name="shape" src="d0_s12" to="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals (3–) 5, distinct or rarely connate proximally, apex often hooded or awned (awn often subapical);</text>
      <biological_entity id="o316" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s13" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="rarely; proximally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o317" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="hooded" value_original="hooded" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals absent;</text>
      <biological_entity id="o318" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens absent or 1–5, in 1 whorl arising from hypanthium rim;</text>
      <biological_entity id="o319" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
      <biological_entity id="o320" name="whorl" name_original="whorl" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character constraint="from hypanthium rim" constraintid="o321" is_modifier="false" name="orientation" src="d0_s15" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o321" name="rim" name_original="rim" src="d0_s15" type="structure" />
      <relation from="o319" id="r36" name="in" negation="false" src="d0_s15" to="o320" />
    </statement>
    <statement id="d0_s16">
      <text>staminodes absent or 5 (16–19 in Achyronychia);</text>
      <biological_entity id="o322" name="staminode" name_original="staminodes" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary 1-locular;</text>
      <biological_entity id="o323" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s17" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 1–3, distinct or sometimes connate proximally;</text>
      <biological_entity id="o324" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s18" to="3" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes; proximally" name="fusion" src="d0_s18" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas 2 or 3.</text>
      <biological_entity id="o325" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s19" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits utricles, indehiscent or sometimes opening by 3 or 8–10 valves;</text>
      <biological_entity constraint="fruits" id="o326" name="utricle" name_original="utricles" src="d0_s20" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s20" value="indehiscent" value_original="indehiscent" />
        <character name="dehiscence" src="d0_s20" value="sometimes" value_original="sometimes" />
      </biological_entity>
      <biological_entity id="o327" name="valve" name_original="valves" src="d0_s20" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s20" value="3" value_original="3" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s20" to="10" />
      </biological_entity>
      <relation from="o326" id="r37" name="opening by" negation="false" src="d0_s20" to="o327" />
    </statement>
    <statement id="d0_s21">
      <text>carpophore absent.</text>
      <biological_entity id="o328" name="carpophore" name_original="carpophore" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds 1, white to tan or brown to black, ovoid to reniform, not or slightly laterally compressed;</text>
      <biological_entity id="o329" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="1" value_original="1" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s22" to="tan or brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s22" to="reniform" />
        <character is_modifier="false" modifier="not; slightly laterally" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>embryo peripheral or central, curved or straight.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 7, 8, 9.</text>
      <biological_entity id="o330" name="embryo" name_original="embryo" src="d0_s23" type="structure">
        <character is_modifier="false" name="position" src="d0_s23" value="peripheral" value_original="peripheral" />
        <character is_modifier="false" name="position" src="d0_s23" value="central" value_original="central" />
        <character is_modifier="false" name="course" src="d0_s23" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s23" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="x" id="o331" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="7" value_original="7" />
        <character name="quantity" src="d0_s24" value="8" value_original="8" />
        <character name="quantity" src="d0_s24" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>s North America, South America (Andean region), Europe (Mediterranean region), Asia (Mediterranean region, e to India), Africa (Mediterranean region).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s North America" establishment_means="native" />
        <character name="distribution" value="South America (Andean region)" establishment_means="native" />
        <character name="distribution" value="Europe (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Asia (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="Asia (e to India)" establishment_means="native" />
        <character name="distribution" value="Africa (Mediterranean region)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>43b.</number>
  <other_name type="past_name">Paronychieae</other_name>
  <discussion>Genera 17, species ca. 200 (6 genera, 32 species in the flora).</discussion>
  <discussion>Paronychioideae is characterized by the presence of stipules, petaloid staminodes, and usually indehiscent utricles. It is of similar size to Polycarpoideae; about two-thirds of the species are found in Paronychia and Herniaria. Paronychioideae is sometimes segregated from Caryophyllaceae as Illecebraceae, due to emphasis on the utricle; molecular data does not support recognition of Illecebraceae (M. Nepokroeff et al. 2002; R. D. Smissen et al. 2002). While there are some features shared with Polycarpoideae (stipules, solanad type of embryogeny), floral reduction is more pronounced in this group.</discussion>
  <discussion>Tentatively, Corrigioleae (Telephium and Corrigiola) is included here. M. G. Gilbert (1987) proposed transferring this tribe to Molluginaceae, noting that the morphological anomalies within Caryophyllaceae, including alternate leaves, exhibited in these plants were reduced under such an alignment. M. Nepokroeff et al. (2002) retained the tribe within Caryophyllaceae, placed as a sister group to the rest of the family.</discussion>
  <references>
    <reference>Chaudhri, M. N. 1968. A revision of the Paronychiinae. Meded. Bot. Mus. Utrecht 285: 1–440.</reference>
  </references>
  
</bio:treatment>