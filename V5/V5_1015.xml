<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Necker ex Campderá" date="unknown" rank="genus">EMEX</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Rumex,</publication_title>
      <place_in_publication>56, plate 1, fig. 1. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus emex;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, ex, and Rumex, alluding to segregation from that genus</other_info_on_name>
    <other_info_on_name type="fna_id">111548</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o19525" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to erect, glabrous.</text>
      <biological_entity id="o19526" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, cauline, alternate, petiolate;</text>
      <biological_entity id="o19527" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o19528" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ocrea often deciduous, chartaceous;</text>
      <biological_entity id="o19529" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade subhastate, triangular, or ovate to ovate-oblong, margins entire to obscurely crenulate or dentate, sometimes undulate.</text>
      <biological_entity id="o19530" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subhastate" value_original="subhastate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovate-oblong" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovate-oblong" />
      </biological_entity>
      <biological_entity id="o19531" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="obscurely crenulate or dentate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and axillary, racemelike, pedunculate.</text>
      <biological_entity id="o19532" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="shape" src="d0_s6" value="racemelike" value_original="racemelike" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present on staminate flowers, absent or nearly so on pistillate flowers.</text>
      <biological_entity id="o19533" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="on flowers" constraintid="o19534" is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o19534" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="presence" notes="" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19535" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o19533" id="r2174" name="on" negation="false" src="d0_s7" to="o19535" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers unisexual, all plants having both staminate and pistillate flowers, the two types segregated in different inflorescences, base not stipelike;</text>
      <biological_entity id="o19536" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o19537" name="whole_organism" name_original="" src="" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19538" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19539" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o19540" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <relation from="o19537" id="r2175" name="having both" negation="false" src="d0_s8" to="o19538" />
      <relation from="o19537" id="r2176" name="segregated in different" negation="false" src="d0_s8" to="o19539" />
    </statement>
    <statement id="d0_s9">
      <text>perianth accrescent (pistillate) or nonaccrescent (staminate), greenish, campanulate, glabrous;</text>
      <biological_entity id="o19541" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" name="size" src="d0_s9" value="nonaccrescent" value_original="nonaccrescent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals (5–) 6, sepaloid, dimorphic, especially in pistillate flowers.</text>
      <biological_entity id="o19542" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s10" to="6" to_inclusive="false" />
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sepaloid" value_original="sepaloid" />
        <character is_modifier="false" name="growth_form" src="d0_s10" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o19543" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o19542" id="r2177" modifier="especially" name="in" negation="false" src="d0_s10" to="o19543" />
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers 1–8 per ocreate fascicle, terminal and axillary;</text>
      <biological_entity id="o19544" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o19545" from="1" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s11" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o19545" name="fascicle" name_original="fascicle" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>tepals distinct, subequal;</text>
      <biological_entity id="o19546" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 4–6;</text>
      <biological_entity id="o19547" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct, free, glabrous;</text>
      <biological_entity id="o19548" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers yellowish to reddish, elliptic to ovate.</text>
      <biological_entity id="o19549" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s15" to="reddish" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers 1–7 per ocreate fascicle, axillary;</text>
      <biological_entity id="o19550" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o19551" from="1" name="quantity" src="d0_s16" to="7" />
        <character is_modifier="false" name="position" notes="" src="d0_s16" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o19551" name="fascicle" name_original="fascicle" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>tepals connate basally, outer 3 each with apex ending in a stout spine in fruit, inner 3 erect, tuberculate;</text>
      <biological_entity id="o19552" name="tepal" name_original="tepals" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s17" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o19553" name="tepal" name_original="tepals" src="d0_s17" type="structure">
        <character constraint="with apex" constraintid="o19554" name="quantity" src="d0_s17" value="3" value_original="3" />
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="relief" src="d0_s17" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o19554" name="apex" name_original="apex" src="d0_s17" type="structure" />
      <biological_entity id="o19555" name="spine" name_original="spine" src="d0_s17" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o19556" name="fruit" name_original="fruit" src="d0_s17" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19557" name="tepal" name_original="tepals" src="d0_s17" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o19554" id="r2178" name="ending in a" negation="false" src="d0_s17" to="o19555" />
      <relation from="o19554" id="r2179" name="ending in a" negation="false" src="d0_s17" to="o19556" />
      <relation from="o19554" id="r2180" name="ending in a" negation="false" src="d0_s17" to="o19557" />
    </statement>
    <statement id="d0_s18">
      <text>styles 3, erect, distinct;</text>
      <biological_entity id="o19558" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigmas penicillate.</text>
      <biological_entity id="o19559" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="penicillate" value_original="penicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Achenes included in indurate perianth, brown, not winged, 3-gonous, glabrous.</text>
      <biological_entity id="o19560" name="achene" name_original="achenes" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s20" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s20" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19561" name="perianth" name_original="perianth" src="d0_s20" type="structure">
        <character is_modifier="true" name="texture" src="d0_s20" value="indurate" value_original="indurate" />
      </biological_entity>
      <relation from="o19560" id="r2181" name="included in" negation="false" src="d0_s20" to="o19561" />
    </statement>
    <statement id="d0_s21">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o19562" name="seed" name_original="seeds" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>x = 10.</text>
      <biological_entity id="o19563" name="embryo" name_original="embryo" src="d0_s21" type="structure">
        <character is_modifier="false" name="course" src="d0_s21" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o19564" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; tropical to subtropical or temperate regions, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="tropical to subtropical or temperate regions" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Both species of Emex are on the United States federal noxious weed list. Emex australis appears to be a more serious threat to agriculture and livestock production than does E. spinosa, based on experience in other parts of the world. The former is a serious agricultural pest in southern Australia, where it was introduced in the 1830s. Emex spinosa also was introduced there, apparently in the mid-1900s. The two species were formerly geographically isolated, but hybrids between them have recently been reported in Australia (E. Putievsky et al. 1980). Hybrids exhibit irregular meiosis and high sterility when self-pollinated; backcrosses with either parent often yield viable seeds.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting perianths 7-9 × 9-10 mm, spines 5-10 mm; inner tepals of pistillate flowers broadly triangular-ovate, apex mucronate</description>
      <determination>1 Emex australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting perianths 5-6 × 5-6 mm, spines 2-4 mm; inner tepals of pistillate flowers linear-lanceolate, apex acute</description>
      <determination>2 Emex spinosa</determination>
    </key_statement>
  </key>
</bio:treatment>