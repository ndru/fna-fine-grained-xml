<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sylvia M. Phillips;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wight &amp; Arn. ex Chiov." date="unknown" rank="genus">ACRACHNE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus acrachne</taxon_hierarchy>
  </taxon_identification>
  <number>17.28</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o16946" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to approximately 50 cm, erect or geniculate, not woody.</text>
      <biological_entity id="o16947" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="to approximately 50 cm" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o16948" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o16949" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades broadly linear.</text>
      <biological_entity id="o16950" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, panicles of spike¬like branches, exceeding the upper leaves;</text>
      <biological_entity id="o16951" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16952" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o16953" name="spike" name_original="spike" src="d0_s6" type="structure" />
      <biological_entity id="o16954" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity constraint="upper" id="o16955" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o16952" id="r2840" name="part_of" negation="false" src="d0_s6" to="o16953" />
      <relation from="o16952" id="r2841" name="like" negation="false" src="d0_s6" to="o16954" />
      <relation from="o16952" id="r2842" name="exceeding the" negation="false" src="d0_s6" to="o16955" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1.5-10 cm, subdigitate or in whorls along elongate rachises, axes flattened, with imbricate, subsessile spikelets, terminating in a rudimentary spikelet.</text>
      <biological_entity id="o16956" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subdigitate" value_original="subdigitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="in whorls" value_original="in whorls" />
      </biological_entity>
      <biological_entity id="o16957" name="whorl" name_original="whorls" src="d0_s7" type="structure" />
      <biological_entity id="o16958" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o16959" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o16960" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o16961" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o16956" id="r2843" name="in" negation="false" src="d0_s7" to="o16957" />
      <relation from="o16957" id="r2844" name="along" negation="false" src="d0_s7" to="o16958" />
      <relation from="o16959" id="r2845" name="with" negation="false" src="d0_s7" to="o16960" />
      <relation from="o16959" id="r2846" name="terminating in" negation="false" src="d0_s7" to="o16961" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets laterally compressed, with 3-25 florets;</text>
      <biological_entity id="o16963" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
      <relation from="o16962" id="r2847" name="with" negation="false" src="d0_s8" to="o16963" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation of the spikelets below the glumes, of the lemmas within the spikelets acropetal, spikelets falling wholly or in part after only a few lemmas have fallen, paleas persistent.</text>
      <biological_entity id="o16962" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o16964" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o16965" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <biological_entity id="o16966" name="lemma" name_original="lemmas" src="d0_s9" type="structure" />
      <biological_entity id="o16967" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="within the spikelets" name="development" src="d0_s9" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o16968" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="wholly" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="in part" value_original="in part" />
      </biological_entity>
      <biological_entity id="o16969" name="part" name_original="part" src="d0_s9" type="structure" />
      <biological_entity id="o16970" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="fallen" value_original="fallen" />
      </biological_entity>
      <biological_entity id="o16971" name="palea" name_original="paleas" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o16962" id="r2848" name="part_of" negation="false" src="d0_s9" to="o16964" />
      <relation from="o16962" id="r2849" name="below" negation="false" src="d0_s9" to="o16965" />
      <relation from="o16968" id="r2850" name="in" negation="false" src="d0_s9" to="o16969" />
      <relation from="o16969" id="r2851" name="after" negation="false" src="d0_s9" to="o16970" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes 1-veined, keeled, exceeded by the florets;</text>
      <biological_entity id="o16972" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o16973" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o16972" id="r2852" name="exceeded by the" negation="false" src="d0_s10" to="o16973" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 3-veined, strongly keeled, firmly membranous to cartilaginous, glabrous, cuspidate or awn-tipped.</text>
      <biological_entity id="o16974" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="firmly" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s11" value="membranous to cartilaginous" value_original="membranous to cartilaginous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits modified caryopses, pericarp hyaline, rupturing at maturity;</text>
      <biological_entity id="o16975" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o16976" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character is_modifier="true" name="development" src="d0_s12" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity id="o16977" name="pericarp" name_original="pericarp" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character constraint="at maturity" is_modifier="false" name="dehiscence" src="d0_s12" value="rupturing" value_original="rupturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>seeds deeply sulcate, ornamented, x = 9.</text>
      <biological_entity id="o16978" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s13" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="ornamented" value_original="ornamented" />
      </biological_entity>
      <biological_entity constraint="x" id="o16979" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Acrachne has four species, all of which are native to the Eastern Hemisphere. One species, Acrachne racemosa, which is widely distributed in the tropics, was recently found in southern California. The genus resembles Eleusine and Dactyloctenium in its fruits and ornamented seeds, but differs from both in its mode of disarticulation.</discussion>
  <references>
    <reference>Clifford, H.T. 1996. Etymological Dictionary of Grasses, Version 1.0 (CD-ROM). Expert Center for Taxonomic Identification, Amsterdam, The Netherlands</reference>
    <reference>Phillips, S.M. 1995. Flora of Ethiopia and Eritrea, vol. 7 (I. Hedberg and S. Edwards, eds.). National Herbarium, Biology Department, Science Faculty, Addis Ababa University, Addis Ababa, Ethiopia and Department of Systematic Botany, Uppsala University, Uppsala, Sweden. 420 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>