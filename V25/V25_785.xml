<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(Kunth) Swallen" date="unknown" rank="species">ramulosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species ramulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wolfii</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species wolfii</taxon_hierarchy>
  </taxon_identification>
  <number>70</number>
  <other_name type="common_name">Green muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o29270" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (3) 5-25 cm, erect, branching, but not rooting, basally;</text>
      <biological_entity id="o29271" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous, mostly smooth, some¬times scabridulous below the nodes.</text>
      <biological_entity id="o29272" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="below nodes" constraintid="o29273" is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o29273" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually shorter than the internodes, glabrous, smooth or scabridulous;</text>
      <biological_entity id="o29274" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o29275" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o29275" name="internode" name_original="internodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.5 mm, hyaline, truncate, ciliate, without lateral lobes;</text>
      <biological_entity id="o29276" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o29277" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <relation from="o29276" id="r5002" name="without" negation="false" src="d0_s4" to="o29277" />
    </statement>
    <statement id="d0_s5">
      <text>blades 0.5-3 cm long, 0.8-1.2 mm wide, involute or flat, glabrous abaxially, puberulent adaxially.</text>
      <biological_entity id="o29278" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s5" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles (1) 2-9 cm long, 0.6-2.7 cm wide, open, ovoid or deltoid, spikelets sparse;</text>
      <biological_entity id="o29279" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s6" to="2.7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity id="o29280" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches (0.5) 1-3.2 cm long, about 0.1 mm thick, ascending, spreading 20-100° from the rachises;</text>
      <biological_entity constraint="primary" id="o29281" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="3.2" to_unit="cm" />
        <character name="thickness" src="d0_s7" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character constraint="from rachises" constraintid="o29282" is_modifier="false" modifier="20-100°" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o29282" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 1-3 mm long, 0.5-1.5 mm thick, stiff.</text>
      <biological_entity id="o29283" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 0.8-1.3 mm, appressed or divaricate.</text>
      <biological_entity id="o29284" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes equal, 0.4-0.7 mm, glabrous, 1-veined, obtuse or subacute, unawned;</text>
      <biological_entity id="o29285" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 0.8-1.3 mm, oval, plump, mottled with greenish-black and greenish-white or ochroleucous patches, glabrous or shortly appressed-pubescent on the margins and midveins, apices acute, unawned;</text>
      <biological_entity id="o29286" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oval" value_original="oval" />
        <character is_modifier="false" name="size" src="d0_s11" value="plump" value_original="plump" />
        <character constraint="with patches" constraintid="o29287" is_modifier="false" name="coloration" src="d0_s11" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="on midveins" constraintid="o29289" is_modifier="false" modifier="shortly" name="pubescence" src="d0_s11" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o29287" name="patch" name_original="patches" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="greenish-black" value_original="greenish-black" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="greenish-white" value_original="greenish-white" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="ochroleucous" value_original="ochroleucous" />
      </biological_entity>
      <biological_entity id="o29288" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <biological_entity id="o29289" name="midvein" name_original="midveins" src="d0_s11" type="structure" />
      <biological_entity id="o29290" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas 0.7-1.3 mm, oval;</text>
      <biological_entity id="o29291" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oval" value_original="oval" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.2-0.3 mm, purplish.</text>
      <biological_entity id="o29292" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 0.5-1 mm, elliptic, brownish.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20.</text>
      <biological_entity id="o29293" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29294" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia ramulosa grows in open, well-drained areas including slopes, sandy meadows, washes, gravelly road cuts, and rock outcrops in yellow pine-oak forests and in open meadows of pine-fir forests, at elevations of 2100-3500 m. Its range includes the southwestern United States, Mexico, Guatemala, Costa Rica, and Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;N.Mex.;Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>