<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ECHINOCHLOA</taxon_name>
    <taxon_name authority="(A. Braun) H. Scholtz" date="unknown" rank="species">esculenta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus echinochloa;species esculenta</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Japanese millet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o24016" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 80-150 cm tall, 4-10 mm thick, glabrous.</text>
      <biological_entity id="o24017" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="height" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="thickness" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o24018" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules absent, ligule region sometimes pubescent;</text>
      <biological_entity id="o24019" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="ligule" id="o24020" name="region" name_original="region" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10-50 cm long, 5-25 mm wide.</text>
      <biological_entity id="o24021" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 7-30 cm, dense, rachis nodes densely hispid, hairs papillose-based, internodes scabrous;</text>
      <biological_entity id="o24022" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s5" to="30" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="rachis" id="o24023" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o24024" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o24025" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary branches 2-5 cm, erect or spreading, simple or branched, often incurved at maturity, nodes hispid, hairs papillose-based, internodes usually scabrous;</text>
      <biological_entity constraint="primary" id="o24026" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character constraint="at maturity" is_modifier="false" modifier="often" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity id="o24027" name="node" name_original="nodes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o24028" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o24029" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>longer pedicels 0.5-1 mm.</text>
      <biological_entity constraint="longer" id="o24030" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 3-4 mm long, 2-2.5 mm wide, not or only tardily disarticulating at maturity, obtuse to shortly acute, purplish to blackish-brown at maturity.</text>
      <biological_entity id="o24031" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="not; only tardily" name="architecture" src="d0_s8" value="disarticulating" value_original="disarticulating" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="shortly acute" />
        <character char_type="range_value" constraint="at maturity" from="purplish" name="coloration" src="d0_s8" to="blackish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Upper glumes narrower and shorter than the upper lemmas;</text>
      <biological_entity constraint="upper" id="o24032" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
        <character constraint="than the upper lemmas" constraintid="o24033" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="narrower and shorter" value_original="narrower and shorter" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24033" name="lemma" name_original="lemmas" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o24034" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower lemmas usually unawned;</text>
      <biological_entity constraint="lower" id="o24035" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower paleas shorter and narrower than the lemmas;</text>
      <biological_entity constraint="lower" id="o24036" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character constraint="than the lemmas" constraintid="o24037" is_modifier="false" name="width" src="d0_s12" value="shorter and narrower" value_original="shorter and narrower" />
      </biological_entity>
      <biological_entity id="o24037" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas longer and wider than the upper glumes, broadly ovate to ovate-orbicular, shortly apiculate, exposed distally at maturity;</text>
      <biological_entity constraint="upper" id="o24038" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character constraint="than the upper glumes" constraintid="o24039" is_modifier="false" name="width" src="d0_s13" value="longer and wider" value_original="longer and wider" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s13" to="ovate-orbicular" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s13" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24039" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 1-1.2 mm.</text>
      <biological_entity id="o24040" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 1.2-2.3 mm, brownish;</text>
      <biological_entity id="o24041" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryos 84-96% as long as the caryopses.</text>
      <biological_entity id="o24043" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure" />
      <relation from="o24042" id="r4071" modifier="84-96%" name="as long as" negation="false" src="d0_s16" to="o24043" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 54.</text>
      <biological_entity id="o24042" name="embryo" name_original="embryos" src="d0_s16" type="structure" />
      <biological_entity constraint="2n" id="o24044" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Echinochloa esculenta was derived from E. crus-galli in Japan, Korea, and China. It is cultivated for fodder, grain, or birdseed. It has sometimes been included in E. frumentacea, from which it differs in its brownish caryopses and longer pedicels. Hybrids between E. crus-galli and E. esculenta are fully fertile, but those with E. frumentacea are sterile.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Y.;Calif.;Pacific Islands (Hawaii);Fla.;Mo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>