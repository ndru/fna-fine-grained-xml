<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Chapm." date="unknown" rank="species">brachystachyus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species brachystachyus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Shortspike bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, open and ovate to obpyramidal above.</text>
      <biological_entity id="o29980" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s0" to="obpyramidal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 1.1-3.1 m;</text>
      <biological_entity id="o29981" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.1" from_unit="m" name="some_measurement" src="d0_s1" to="3.1" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes not glaucous;</text>
      <biological_entity id="o29982" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches arching.</text>
      <biological_entity id="o29983" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth;</text>
      <biological_entity id="o29984" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm, ciliate, cilia 0.6-1.5 mm;</text>
      <biological_entity id="o29985" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o29986" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 21-54 cm long, 2.3-6 mm wide, glabrous or sparsely pubescent, with spreading hairs.</text>
      <biological_entity id="o29987" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="21" from_unit="cm" name="length" src="d0_s6" to="54" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29988" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o29987" id="r5117" name="with" negation="false" src="d0_s6" to="o29988" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence units 12-190 per culm;</text>
      <biological_entity id="o29990" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>subtending sheaths (2.1) 2.4-3.5 (4.1) cm long, (2.3) 2.6-3 (3.8) mm wide;</text>
      <biological_entity constraint="inflorescence" id="o29989" name="unit" name_original="units" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o29990" from="12" name="quantity" src="d0_s7" to="190" />
        <character name="length" src="d0_s8" unit="cm" value="2.1" value_original="2.1" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29991" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o29989" id="r5118" name="subtending" negation="false" src="d0_s8" to="o29991" />
    </statement>
    <statement id="d0_s9">
      <text>peduncles (13) 20-31 (43) mm, with 2 (3) rames;</text>
      <biological_entity id="o29993" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
      <relation from="o29992" id="r5119" name="with" negation="false" src="d0_s9" to="o29993" />
    </statement>
    <statement id="d0_s10">
      <text>rames (1.2) 1.5-2.1 (2.6) cm, usually exserted at maturity, pubescence increasing in density distally within each internode.</text>
      <biological_entity id="o29992" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="13" value_original="13" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29994" name="internode" name_original="internode" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets (4.1) 4.4-4.6 (5) mm;</text>
      <biological_entity id="o29995" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4.1" value_original="4.1" />
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s11" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 1-1.5 mm;</text>
      <biological_entity constraint="callus" id="o29996" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keels of lower glumes scabrous only above the midpoint;</text>
      <biological_entity id="o29997" name="keel" name_original="keels" src="d0_s13" type="structure" constraint="glume" constraint_original="glume; glume">
        <character constraint="above midpoint" constraintid="o29999" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o29998" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o29999" name="midpoint" name_original="midpoint" src="d0_s13" type="structure" />
      <relation from="o29997" id="r5120" name="part_of" negation="false" src="d0_s13" to="o29998" />
    </statement>
    <statement id="d0_s14">
      <text>awns 2-11 mm;</text>
      <biological_entity id="o30000" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1, 1.4-2.4 mm, red.</text>
      <biological_entity id="o30001" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets vestigial or absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o30002" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30003" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon brachystachyus grows in sandy, often seasonally wet soils of flatwoods, savannahs, pond margins, and scrublands of the southeastern United States. It sometimes forms large populations, but does not invade disturbed sites as do some morphologically similar forms of A. virginicus var. virginicus.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.C.;N.C.;Fla.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>