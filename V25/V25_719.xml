<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(Poir.) Fernald" date="unknown" rank="species">frondosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species frondosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mexicana</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">commutata</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species mexicana;subspecies commutata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">frondosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">commutata</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species frondosa;forma commutata</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Wirestem muhly</other_name>
  <other_name type="common_name">Muhlenbergie feuillée</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, not cespitose.</text>
      <biological_entity id="o17783" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-110 cm tall, 0.7-1.8 mm thick, ascending, geniculate, or decumbent, bushy and much branched above;</text>
      <biological_entity id="o17784" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s2" to="110" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="thickness" src="d0_s2" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="bushy" value_original="bushy" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes smooth and shiny for most of their length, glabrous throughout.</text>
      <biological_entity id="o17785" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, margins hyaline;</text>
      <biological_entity id="o17786" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17787" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.7-1.7 mm, membranous, truncate, lacerate-ciliolate;</text>
      <biological_entity id="o17788" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s5" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="lacerate-ciliolate" value_original="lacerate-ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 4-18 cm long, 2-7 mm wide, flat, glabrous, smooth or scabridulous, those of the lateral branches similar to those of the primary culms.</text>
      <biological_entity id="o17789" name="blade" name_original="blades" src="d0_s6" type="structure" constraint="branch" constraint_original="branch; branch">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o17790" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o17791" name="culm" name_original="culms" src="d0_s6" type="structure" />
      <relation from="o17789" id="r2995" name="part_of" negation="false" src="d0_s6" to="o17790" />
      <relation from="o17789" id="r2996" name="part_of" negation="false" src="d0_s6" to="o17791" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-15 cm long, 0.3-2 cm wide, sometimes dense;</text>
      <biological_entity id="o17792" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary panicles common, partly included in the sheaths;</text>
      <biological_entity constraint="axillary" id="o17793" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o17794" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o17793" id="r2997" modifier="partly" name="included in the" negation="false" src="d0_s8" to="o17794" />
    </statement>
    <statement id="d0_s9">
      <text>primary branches 0.3-4.2 cm, ascending, appressed, occasionally diverging up to 30° from the rachises;</text>
      <biological_entity constraint="primary" id="o17795" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s9" to="4.2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o17796" is_modifier="false" modifier="0-30°" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o17796" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.4-5 mm, smooth or scabrous.</text>
      <biological_entity id="o17797" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.2-4 mm, imbricate along the branches.</text>
      <biological_entity id="o17798" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character constraint="along branches" constraintid="o17799" is_modifier="false" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o17799" name="branch" name_original="branches" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, 2-4 mm, 3/4 as long as to longer than the lemmas, 1-veined, unawned or awned, awns to 2 mm;</text>
      <biological_entity id="o17800" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character name="quantity" src="d0_s12" value="3/4" value_original="3/4" />
        <character constraint="than the lemmas" constraintid="o17801" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o17801" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o17802" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes acuminate to acute;</text>
      <biological_entity constraint="upper" id="o17803" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s13" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2.2-4 mm, narrowly lanceolate, hairy on the calluses, lower 1/3 of the midveins, and margins, hairs 0.3-0.8 mm, apices scabridulous, acuminate, unawned or awned, awns 0.1-13 mm;</text>
      <biological_entity id="o17804" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character constraint="on calluses" constraintid="o17805" is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="lower" value_original="lower" />
        <character constraint="of midveins" constraintid="o17806" name="quantity" src="d0_s14" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o17805" name="callus" name_original="calluses" src="d0_s14" type="structure" />
      <biological_entity id="o17806" name="midvein" name_original="midveins" src="d0_s14" type="structure" />
      <biological_entity id="o17807" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o17808" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17809" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o17810" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas 2.3-4 mm, narrowly lanceolate, acuminate;</text>
      <biological_entity id="o17811" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.3-0.6 mm, usually yellow, occasionally purplish.</text>
      <biological_entity id="o17812" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s16" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.6-1.9 mm, fusiform, brown.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o17813" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17814" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia frondosa grows in forest borders, thickets, clearings, alluvial plains, and disturbed sites within deciduous forests, at elevations of 20-1000 m. It grows only in southern Canada and the contiguous United States. The collection from Oregon was made in an artificial cranberry bog and was probably introduced with the plants.</discussion>
  <discussion>Plants with unawned or shortly (less than 4 mm) awned lemmas can be called M. frondosa forma frondosa, and those with lemma awns 4-13 mm long, M. frondosa forma commutata (Scribn.) Fernald.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>W.Va.;Del.;D.C.;Wis.;N.B.;Ont.;Que.;N.H.;N.J.;Tex.;La.;N.C.;Tenn.;N.Y.;Pa.;Conn.;Mass.;Maine;R.I.;Va.;Md.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Vt.;Ill.;Ga.;Ind.;Iowa;Ohio;Mo.;Minn.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>