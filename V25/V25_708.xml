<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">CALAMOVILFA</taxon_name>
    <taxon_name authority="(Torr.) Scribn." date="unknown" rank="species">brevipilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus calamovilfa;species brevipilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calamagrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">brevipilis</taxon_name>
    <taxon_hierarchy>genus calamagrostis;species brevipilis</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Pine-barren sandreed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes short, covered with the persisent bases of the foliage leaves.</text>
      <biological_entity id="o29712" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o29713" name="base" name_original="bases" src="d0_s0" type="structure" />
      <biological_entity constraint="foliage" id="o29714" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <relation from="o29712" id="r5069" name="covered with the persisent" negation="false" src="d0_s0" to="o29713" />
      <relation from="o29712" id="r5070" name="covered with the persisent" negation="false" src="d0_s0" to="o29714" />
    </statement>
    <statement id="d0_s1">
      <text>Culms to 1.5 m.</text>
      <biological_entity id="o29715" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths to 30 cm;</text>
      <biological_entity id="o29716" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules to 0.5 mm;</text>
      <biological_entity id="o29717" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades to 50 cm long, 2-5 mm wide.</text>
      <biological_entity id="o29718" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 8-40 cm long, 4-20 cm wide, open;</text>
      <biological_entity id="o29719" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s5" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches to 17 cm long, ascending to spreading.</text>
      <biological_entity id="o29720" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="17" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 4-5.8 mm.</text>
      <biological_entity id="o29721" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes straight, acute;</text>
      <biological_entity id="o29722" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 1.7-4.1 mm;</text>
      <biological_entity constraint="lower" id="o29723" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="4.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 3.3-5 mm;</text>
      <biological_entity constraint="upper" id="o29724" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>callus hairs 1/4 - 1/2 as long as the lemmas;</text>
      <biological_entity constraint="callus" id="o29725" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o29726" from="1/4" name="quantity" src="d0_s11" to="1/2" />
      </biological_entity>
      <biological_entity id="o29726" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas 4-5.4 mm, straight, slightly to markedly pubescent, acuminate;</text>
      <biological_entity id="o29727" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5.4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly to markedly" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas 3.8-5.3 mm, slightly to markedly pubescent;</text>
      <biological_entity id="o29728" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s13" to="5.3" to_unit="mm" />
        <character is_modifier="false" modifier="slightly to markedly" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 2.4-3.2 mm. 2n = unknown.</text>
      <biological_entity id="o29729" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s14" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29730" name="chromosome" name_original="" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Calamovilfa brevipilis grows in moist to dry pine barrens, savannahs, bogs, swamp edges, and pocosins. It is a common grass on the New Jersey pine barrens and locally common across the coastal plain of North Carolina, but rare at present in Virginia and South Carolina. The length of the ligule hairs tends to increase from 0.3 mm or less in the north to 0.5 mm at the southern end of its range.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.;S.C.;N.J.;Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>