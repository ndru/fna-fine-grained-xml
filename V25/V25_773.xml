<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">setifolia</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species setifolia</taxon_hierarchy>
  </taxon_identification>
  <number>58</number>
  <other_name type="common_name">Curlyleaf muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o14503" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-80 cm, slightly decumbent basally, erect above, not conspicuously branched, branches, when present, not geniculate and widely divergent;</text>
      <biological_entity id="o14504" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="slightly; basally" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not conspicuously" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o14505" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when present; not" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly glabrous, sometimes hirtellous below the nodes.</text>
      <biological_entity id="o14506" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o14507" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o14507" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter than the internodes, glabrous, margins whitish, rounded towards the base, not becoming spirally coiled when old;</text>
      <biological_entity id="o14508" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o14509" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14509" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o14510" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="whitish" value_original="whitish" />
        <character constraint="towards base" constraintid="o14511" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="when old" name="architecture" notes="" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity id="o14511" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 4-7 (10) mm, acuminate, lacerate;</text>
      <biological_entity id="o14512" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-20 (25) cm long, 0.2-1.2 mm wide, tightly involute, arcuate, scabrous abaxially, scabrous or hirtellous adaxially.</text>
      <biological_entity id="o14513" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="tightly" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="course_or_shape" src="d0_s6" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 8-20 (25) cm long, 2-5 cm wide, loosely contracted;</text>
      <biological_entity id="o14514" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-7 cm, capillary, diverging up to 70° from the rachises, naked basally;</text>
      <biological_entity constraint="primary" id="o14515" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
        <character constraint="from rachises" constraintid="o14516" is_modifier="false" modifier="0-70°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="basally" name="architecture" notes="" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o14516" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 3-20 mm.</text>
      <biological_entity id="o14517" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3.5-5 mm, stramineous to brown or purplish.</text>
      <biological_entity id="o14518" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s10" to="brown or purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal, 1.5-2.5 mm, shorter than the florets, thin, hyaline, glabrous;</text>
      <biological_entity id="o14519" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character constraint="than the florets" constraintid="o14520" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14520" name="floret" name_original="florets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes veinless, truncate or obtuse, often toothed or notched;</text>
      <biological_entity constraint="lower" id="o14521" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="veinless" value_original="veinless" />
        <character is_modifier="false" name="shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 1-veined or veinless, obtuse to acute, often mucronate, mucros shorter than 0.7 mm;</text>
      <biological_entity constraint="upper" id="o14522" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="veinless" value_original="veinless" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s13" to="acute" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s13" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o14523" name="mucro" name_original="mucros" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3.5-5 mm, narrowly lanceolate, calluses hairy, hairs to 0.6 mm, lemma bodies glabrous, smooth, shiny, apices acuminate, awned, awns 10-30 mm, clearly demarcated from the lemma bodies, flexuous;</text>
      <biological_entity id="o14524" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o14525" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o14526" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o14527" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity id="o14528" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o14529" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="clearly" name="course" src="d0_s14" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o14530" name="body" name_original="bodies" src="d0_s14" type="structure" />
      <relation from="o14529" id="r2406" modifier="clearly" name="demarcated from the" negation="false" src="d0_s14" to="o14530" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 3.5-5 mm, narrowly lanceolate, glabrous, acuminate;</text>
      <biological_entity id="o14531" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 2-2.6 mm, greenish.</text>
      <biological_entity id="o14532" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 2.4-3.2 mm, fusiform, brownish.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o14533" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s17" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14534" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia setifolia grows on calcareous rocky slopes, rock outcrops, and in desert grasslands, at elevations of 1000-2250 m. Its range extends from the southwestern United States into northern Mexico. It is similar to M. reverchonii, but differs in its narrower panicles, less widespread panicle branches, truncate to obtuse glumes, and longer lemma awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>