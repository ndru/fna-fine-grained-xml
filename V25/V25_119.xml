<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">78</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="(Hornem.) Link" date="unknown" rank="species">mexicana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species mexicana</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Mexican lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, without innovations.</text>
      <biological_entity id="o24267" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24268" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-130 cm, erect, sometimes geniculate, glabrous, sometimes with a ring of glandular depressions below the nodes.</text>
      <biological_entity id="o24269" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="130" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24270" name="ring" name_original="ring" src="d0_s2" type="structure" />
      <biological_entity constraint="glandular" id="o24271" name="depression" name_original="depressions" src="d0_s2" type="structure" />
      <biological_entity id="o24272" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o24269" id="r4125" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o24270" />
      <relation from="o24270" id="r4126" name="part_of" negation="false" src="d0_s2" to="o24271" />
      <relation from="o24270" id="r4127" name="below" negation="false" src="d0_s2" to="o24272" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths sometimes with glandular pits, pilose near the apices and on the collars, hairs to 4 mm, papillose-based;</text>
      <biological_entity id="o24273" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="near apices" constraintid="o24275" is_modifier="false" name="pubescence" notes="" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o24274" name="pit" name_original="pits" src="d0_s3" type="structure" />
      <biological_entity id="o24275" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity id="o24276" name="collar" name_original="collars" src="d0_s3" type="structure" />
      <biological_entity id="o24277" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o24273" id="r4128" name="with" negation="false" src="d0_s3" to="o24274" />
      <relation from="o24273" id="r4129" name="on" negation="false" src="d0_s3" to="o24276" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.5 mm, ciliate;</text>
      <biological_entity id="o24278" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 5-25 cm long, 2-7 (9) mm wide, flat, abaxial surfaces glabrous, adaxial surfaces scabridulous, occasionally pubescent near the base.</text>
      <biological_entity id="o24279" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24280" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24281" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character constraint="near base" constraintid="o24282" is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24282" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles (5) 10-40 cm long, (2) 4-18 cm wide, ovate, rachises angled and channeled;</text>
      <biological_entity id="o24283" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s6" to="18" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o24284" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s6" value="channeled" value_original="channeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 3-12 (15) cm, solitary to whorled, appressed or diverging to 80° from the rachises;</text>
      <biological_entity constraint="primary" id="o24285" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="solitary" name="arrangement" src="d0_s7" to="whorled" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o24286" is_modifier="false" modifier="80°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o24286" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>secondary branches somewhat appressed;</text>
      <biological_entity constraint="secondary" id="o24287" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pulvini glabrous;</text>
      <biological_entity id="o24288" name="pulvinus" name_original="pulvini" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 1-6 (7) mm, almost appressed to narrowly divergent, stiff.</text>
      <biological_entity id="o24289" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="almost" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="narrowly" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets (4) 5-10 (11) mm long, 0.7-2.4 mm wide, ovate to linear-lanceolate, gray-green to purplish, with 5-11 (15) florets;</text>
      <biological_entity id="o24291" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="15" value_original="15" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s11" to="11" />
      </biological_entity>
      <relation from="o24290" id="r4130" name="with" negation="false" src="d0_s11" to="o24291" />
    </statement>
    <statement id="d0_s12">
      <text>disarticulation acropetal.</text>
      <biological_entity id="o24290" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="length" src="d0_s11" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="linear-lanceolate" />
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s11" to="purplish" />
        <character is_modifier="false" name="development" src="d0_s12" value="acropetal" value_original="acropetal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes subequal, 0.7-2 (2.3) mm, ovate to lanceolate, membranous;</text>
      <biological_entity id="o24292" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 1.2-2.4 mm, ovate, membranous, glabrous or with a few hairs, gray-green, lateral-veins evident, often greenish, apices acute;</text>
      <biological_entity id="o24293" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="with a few hairs" value_original="with a few hairs" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o24294" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o24295" name="lateral-vein" name_original="lateral-veins" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="evident" value_original="evident" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s14" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o24296" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o24293" id="r4131" name="with" negation="false" src="d0_s14" to="o24294" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 1-2.2 mm, hyaline, keels scabrous, apices obtuse to truncate;</text>
      <biological_entity id="o24297" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o24298" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24299" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s15" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3, 0.2-0.5 mm, purplish.</text>
      <biological_entity id="o24300" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 0.5-0.8 (1) mm, ovoid to rectangular-prismatic, laterally compressed, shallowly to deeply grooved on the adaxial surface, striate, reddish-brown, distal 2/3 opaque.</text>
      <biological_entity constraint="adaxial" id="o24302" name="surface" name_original="surface" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 60.</text>
      <biological_entity id="o24301" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s17" to="rectangular-prismatic" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character constraint="on adaxial surface" constraintid="o24302" is_modifier="false" modifier="shallowly to deeply" name="architecture" src="d0_s17" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" notes="" src="d0_s17" value="striate" value_original="striate" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="position_or_shape" src="d0_s17" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s17" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="opaque" value_original="opaque" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24303" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis mexicana grows along roadsides, near cultivated fields, and in disturbed open areas, at 100-3000 m. It is native to the Americas, its native range extending from the southwestern United States through Mexico, Central and northern South America, to Argentina. Within the Flora region, it has been introduced beyond its native range, often becoming an established part of the flora.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Del.;Oreg.;Wis.;Iowa;N.Dak.;Fla.;N.Mex.;Tex.;Idaho;B.C.;Ont.;Nev.;Colo.;Calif.;Ind.;Maine;Ill.;Ariz.;Md.;Mass.;Utah;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets ovate to oblong in outline, 1.5-2.4mm wide; lower glumes 1.2-2.3 mm long; sum of the spikelet width and lower glume length 2.7-4.7 mm; culms and sheaths sometimes with glandular depressions</description>
      <determination>Eragrostis mexicana subsp. mexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets linear to linear-lanceolate, 0.7-1.4 wide; lower glumes 0.7-1.7 mm long; sum of the spikelet width and lower glume length 1.5-3.1 mm; culms and sheaths without glandular depressions</description>
      <determination>Eragrostis mexicana subsp. virescens</determination>
    </key_statement>
  </key>
</bio:treatment>