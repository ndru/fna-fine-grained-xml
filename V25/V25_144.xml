<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="(Pursh) Steud." date="unknown" rank="species">spectabilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species spectabilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eragrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spectabilis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">sparsihirsuta</taxon_name>
    <taxon_hierarchy>genus eragrostis;species spectabilis;variety sparsihirsuta</taxon_hierarchy>
  </taxon_identification>
  <number>27</number>
  <other_name type="common_name">Purple lovegrass</other_name>
  <other_name type="common_name">Eragrostide brillante</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations and short, knotty rhizomes less than 4 mm thick.</text>
      <biological_entity id="o7437" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7438" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o7439" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-70 (85) cm, erect, glabrous.</text>
      <biological_entity id="o7440" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="85" value_original="85" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths hairy on the margins and at the apices, hairs to 7 mm;</text>
      <biological_entity id="o7441" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="on the margins and at apices, hairs" constraintid="o7442, o7443" is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7442" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7443" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.2 mm;</text>
      <biological_entity id="o7444" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 10-32 cm long, 3-8 mm wide, flat to involute, both surfaces usually pilose, sometimes glabrous on both surfaces or glabrous abaxially and sparsely pilose adaxially, often with a line of hairs behind the ligules, hairs to 8 mm.</text>
      <biological_entity id="o7445" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="32" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity id="o7446" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character constraint="on " constraintid="o7450" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7447" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o7448" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o7449" name="line" name_original="line" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7450" name="line" name_original="line" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7451" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o7452" name="line" name_original="line" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7453" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o7454" name="line" name_original="line" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7455" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o7456" name="line" name_original="line" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7457" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o7458" name="ligule" name_original="ligules" src="d0_s5" type="structure" />
      <biological_entity id="o7459" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o7450" id="r1215" name="on" negation="false" src="d0_s5" to="o7451" />
      <relation from="o7450" id="r1216" modifier="on both surfaces or glabrous and sparsely pilose , with a line" name="on" negation="false" src="d0_s5" to="o7452" />
      <relation from="o7452" id="r1217" name="on" negation="false" src="d0_s5" to="o7453" />
      <relation from="o7452" id="r1218" modifier="on both surfaces or glabrous and sparsely pilose , with a line" name="on" negation="false" src="d0_s5" to="o7454" />
      <relation from="o7454" id="r1219" name="on" negation="false" src="d0_s5" to="o7455" />
      <relation from="o7454" id="r1220" modifier="on both surfaces or glabrous and sparsely pilose , with a line" name="on" negation="false" src="d0_s5" to="o7456" />
      <relation from="o7456" id="r1221" name="part_of" negation="false" src="d0_s5" to="o7457" />
      <relation from="o7456" id="r1222" name="behind" negation="false" src="d0_s5" to="o7458" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles (15) 25-45 (60) cm long, 15-35 cm wide, broadly ovate to oblong, open, basal portions sometimes included in the uppermost leaf-sheaths;</text>
      <biological_entity id="o7460" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="oblong" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7461" name="portion" name_original="portions" src="d0_s6" type="structure" />
      <biological_entity constraint="uppermost" id="o7462" name="sheath" name_original="leaf-sheaths" src="d0_s6" type="structure" />
      <relation from="o7461" id="r1223" modifier="sometimes" name="included in the" negation="false" src="d0_s6" to="o7462" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches (6) 12-20 cm long, diverging 20-90° from the rachises, capillary, naked below;</text>
      <biological_entity constraint="primary" id="o7463" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character constraint="from rachises" constraintid="o7464" is_modifier="false" modifier="20-90°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="capillary" value_original="capillary" />
        <character is_modifier="false" modifier="below" name="architecture" src="d0_s7" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o7464" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini hairy, hairs to 6 mm;</text>
      <biological_entity id="o7465" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7466" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1.5-17 mm, divergent or appressed.</text>
      <biological_entity id="o7467" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-7.5 mm long, 1-2 mm wide, linear-lanceolate, reddish-purple, sometimes olivaceous, with (4) 6-12 florets;</text>
      <biological_entity id="o7469" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="4" value_original="4" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
      <relation from="o7468" id="r1224" name="with" negation="false" src="d0_s10" to="o7469" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation basipetal, glumes persistent.</text>
      <biological_entity id="o7468" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="reddish-purple" value_original="reddish-purple" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="development" src="d0_s11" value="basipetal" value_original="basipetal" />
      </biological_entity>
      <biological_entity id="o7470" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal to equal, (1) 1.3-2.3 mm, lanceolate, membranous to chartaceous;</text>
      <biological_entity id="o7471" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s12" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s12" to="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas (1) 1.3-2.5 mm, ovate to lanceolate, leathery, 3-veined, apices acute;</text>
      <biological_entity id="o7472" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o7473" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas (1) 1.2-2.4 mm, membranous, keels sometimes shortly ciliate, apices obtuse to truncate;</text>
      <biological_entity id="o7474" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7475" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes shortly" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7476" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 3, 0.3-0.5 mm, purplish.</text>
      <biological_entity id="o7477" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 0.6-0.8 mm, ellipsoid, strongly flattened, adaxial surfaces with 2 prominent ridges separated by a groove, reddish-brown.</text>
      <biological_entity id="o7478" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o7480" name="ridge" name_original="ridges" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="true" name="prominence" src="d0_s16" value="prominent" value_original="prominent" />
        <character constraint="by groove" constraintid="o7481" is_modifier="false" name="arrangement" src="d0_s16" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o7481" name="groove" name_original="groove" src="d0_s16" type="structure" />
      <relation from="o7479" id="r1225" name="with" negation="false" src="d0_s16" to="o7480" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 20, 40, 42.</text>
      <biological_entity constraint="adaxial" id="o7479" name="surface" name_original="surfaces" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s16" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7482" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis spectabilis is native in the eastern portion of the Flora region, extending from southern Canada through the United States, Mexico, and Central America to Belize. It grows in fields and on the margins of woods, along roadsides, and in other disturbed sites, usually in sandy to clay loam soils, at 0-1830 m, and is associated with hardwood forests, Prosopsis-Acacia grasslands, and shortgrass prairies. A showy species, E. spectabilis is available commercially for planting as an ornamental.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;W.Va.;Del.;D.C.;Wis.;Iowa;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Colo.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Man.;Ont.;Que.;Ariz.;Md.;Ohio;Mo.;Minn.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>