<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">634</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="genus">CHRYSOPOGON</taxon_name>
    <taxon_name authority="(Spreng.) Chiov." date="unknown" rank="species">fulvus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus chrysopogon;species fulvus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Golden beardgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not stoloniferous.</text>
      <biological_entity id="o27069" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-80 (120) cm, geniculately ascending.</text>
      <biological_entity id="o27070" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="120" value_original="120" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="geniculately" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o27071" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o27072" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous;</text>
      <biological_entity id="o27073" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm, membranous, ciliolate;</text>
      <biological_entity id="o27074" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-30 cm long, 2-3 (9) mm wide, mostly glabrous or puberulous adaxially, bases sometimes with hispid hairs.</text>
      <biological_entity id="o27075" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="puberulous" value_original="puberulous" />
      </biological_entity>
      <biological_entity id="o27076" name="base" name_original="bases" src="d0_s6" type="structure" />
      <biological_entity id="o27077" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
      <relation from="o27076" id="r4602" name="with" negation="false" src="d0_s6" to="o27077" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-8 (16) cm long, 1.5-3 cm wide, ovate, with many branches;</text>
      <biological_entity id="o27078" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o27079" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="many" value_original="many" />
      </biological_entity>
      <relation from="o27078" id="r4603" name="with" negation="false" src="d0_s7" to="o27079" />
    </statement>
    <statement id="d0_s8">
      <text>branches 3-7 cm, sharply ascending, capillary, naked basal portions 2-6 cm, puberulous, terminating in a rame;</text>
      <biological_entity id="o27080" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="sharply" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
      </biological_entity>
      <biological_entity id="o27082" name="rame" name_original="rame" src="d0_s8" type="structure" />
      <relation from="o27081" id="r4604" name="terminating in a" negation="false" src="d0_s8" to="o27082" />
    </statement>
    <statement id="d0_s9">
      <text>rames with a triplet of spikelets.</text>
      <biological_entity constraint="basal" id="o27081" name="portion" name_original="portions" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="naked" value_original="naked" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulous" value_original="puberulous" />
      </biological_entity>
      <biological_entity id="o27083" name="triplet" name_original="triplet" src="d0_s9" type="structure" />
      <biological_entity id="o27084" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <relation from="o27081" id="r4605" name="rames with" negation="false" src="d0_s9" to="o27083" />
      <relation from="o27083" id="r4606" name="part_of" negation="false" src="d0_s9" to="o27084" />
    </statement>
    <statement id="d0_s10">
      <text>Sessile spikelets 3.5-5.2 (8) mm (including the callus);</text>
      <biological_entity id="o27085" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses 0.7-1.5 mm, sharp, setose, hairs 1.5-1.9 mm, golden;</text>
      <biological_entity id="o27086" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o27087" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden" value_original="golden" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes laterally compressed, smooth, hispidulous distally, acute;</text>
      <biological_entity constraint="lower" id="o27088" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes with a dorsal fringe of hairs, awns 4.1-5.3 (10) mm;</text>
      <biological_entity constraint="upper" id="o27089" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity constraint="dorsal" id="o27090" name="fringe" name_original="fringe" src="d0_s13" type="structure" />
      <biological_entity id="o27091" name="hair" name_original="hairs" src="d0_s13" type="structure" />
      <biological_entity id="o27092" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="some_measurement" src="d0_s13" to="5.3" to_unit="mm" />
      </biological_entity>
      <relation from="o27089" id="r4607" name="with" negation="false" src="d0_s13" to="o27090" />
      <relation from="o27090" id="r4608" name="part_of" negation="false" src="d0_s13" to="o27091" />
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas awned, awns 2-3 cm, slightly geniculate, column twisted, puberulous, hairs 0.2-0.4 mm.</text>
      <biological_entity constraint="upper" id="o27093" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o27094" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s14" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o27095" name="column" name_original="column" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="puberulous" value_original="puberulous" />
      </biological_entity>
      <biological_entity id="o27096" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pedicels 1-2.5 mm, setose on the edges, hairs 3^.9 mm.</text>
      <biological_entity id="o27097" name="pedicel" name_original="pedicels" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character constraint="on edges" constraintid="o27098" is_modifier="false" name="pubescence" src="d0_s15" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o27098" name="edge" name_original="edges" src="d0_s15" type="structure" />
      <biological_entity id="o27099" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets 2.5-8 mm;</text>
      <biological_entity id="o27100" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes muticous or awned, awns to 0.7 cm. 2n = 40.</text>
      <biological_entity constraint="lower" id="o27101" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o27102" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s17" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27103" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chrysopogon fulvus is native from southern India to Thailand, where it is considered a good forage grass. It was grown at the experiment station in Gainesville, Florida, and subsequently found in adjacent flatwoods as an escape.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>