<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">637</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Willemet" date="unknown" rank="genus">DICHANTHIUM</taxon_name>
    <taxon_name authority="(R. Br.) A. Camus" date="unknown" rank="species">sericeum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus dichanthium;species sericeum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Queensland bluegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted or cespitose.</text>
      <biological_entity id="o28846" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-120 cm;</text>
      <biological_entity id="o28847" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes densely pilose, hairs about 2 mm.</text>
      <biological_entity id="o28848" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28849" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths with scattered papillose-based hairs;</text>
      <biological_entity id="o28850" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
      <biological_entity id="o28851" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o28850" id="r4917" name="with" negation="false" src="d0_s4" to="o28851" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o28852" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-25 cm long, 2-5 mm wide.</text>
    </statement>
    <statement id="d0_s7">
      <text>Rames 1-7, 3-7 cm, subdigitate, often glaucous and white-villous, spikelet-bearing to the base, basal spikelet pairs consisting only of glumes;</text>
      <biological_entity id="o28853" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="subdigitate" value_original="subdigitate" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o28854" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28855" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <biological_entity id="o28856" name="glume" name_original="glumes" src="d0_s7" type="structure" />
      <relation from="o28855" id="r4918" name="consisting" negation="false" src="d0_s7" to="o28856" />
    </statement>
    <statement id="d0_s8">
      <text>internodes pubescent, hairs immediately below the nodes to 1.5 mm.</text>
      <biological_entity id="o28857" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28858" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <biological_entity id="o28859" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <relation from="o28858" id="r4919" modifier="to 1.5 mm" name="immediately below" negation="false" src="d0_s8" to="o28859" />
    </statement>
    <statement id="d0_s9">
      <text>Sessile bisexual spikelets 2.5-4.5 mm long, 1-1.4 mm wide;</text>
      <biological_entity id="o28860" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 5-10-veined, with 0.7-1.5 mm hairs on the basal 1/2 and about 3 mm papillose-based hairs on the distal portion of the keels and in a transverse subapical arch;</text>
      <biological_entity constraint="lower" id="o28861" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-10-veined" value_original="5-10-veined" />
        <character name="some_measurement" notes="" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o28862" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28863" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <biological_entity id="o28864" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o28865" name="arch" name_original="arch" src="d0_s10" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s10" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o28861" id="r4920" name="with" negation="false" src="d0_s10" to="o28862" />
      <relation from="o28862" id="r4921" name="on" negation="false" src="d0_s10" to="o28863" />
      <relation from="o28864" id="r4922" name="on the distal portion of the keels and in" negation="false" src="d0_s10" to="o28865" />
    </statement>
    <statement id="d0_s11">
      <text>awns 2-3.5 cm, twice-geniculate.</text>
      <biological_entity id="o28866" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2 times-geniculate" value_original="2 times-geniculate " />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicellate spikelets about 3 mm, usually sterile.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20.</text>
      <biological_entity id="o28867" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28868" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthium sericeum is an Australian species. There are two subspecies: D. sericeum (R. Br.) A. Camus subsp. sericeum is a perennial with sessile spikelets 4-4.5 mm long and to 1-1.4 mm wide, 9-10-veined lower glumes, and rames more than 4 cm long; D. sericeum subsp. humilius (J.M. Black) B.K. Simon is an annual, with sessile spikelets up to 4 mm long and about 1 mm wide, 5-7-veined lower glumes, and rames less than 4 cm long. Dichanthium sericeum subsp. sericeum is established in Texas and Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pacific Islands (Hawaii);Fla.;Tex.;Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>