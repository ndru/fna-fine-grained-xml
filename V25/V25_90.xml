<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">LEPTOCHLOA</taxon_name>
    <taxon_name authority="(Retz.) Ohwi" date="unknown" rank="species">panicea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus leptochloa;species panicea</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o2621" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (5) 13-150 cm, usually erect, compressed, branching;</text>
      <biological_entity id="o2622" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes hollow.</text>
      <biological_entity id="o2623" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths sparsely or densely hairy, particularly distally, hairs papillose-based;</text>
      <biological_entity id="o2624" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2625" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="particularly distally; distally" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.6-3.2 mm, membranous, truncate, erose;</text>
      <biological_entity id="o2626" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 6-25 cm long, 2-21 mm wide, glabrous or sparsely pilose on both surfaces.</text>
      <biological_entity id="o2627" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="21" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o2628" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o2628" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 8-30 cm, with 3-100 racemose branches;</text>
      <biological_entity id="o2629" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s6" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2630" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="100" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <relation from="o2629" id="r415" name="with" negation="false" src="d0_s6" to="o2630" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1-19 cm, ascending to reflexed.</text>
      <biological_entity id="o2631" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="19" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2-4 mm, distant to imbricate, green, magenta, or maroon, with 2-5 (6) florets.</text>
      <biological_entity id="o2632" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="distant" name="arrangement" src="d0_s8" to="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity id="o2633" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="6" value_original="6" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o2632" id="r416" name="with" negation="false" src="d0_s8" to="o2633" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes sometimes exceeding the florets, linear to narrowly elliptic, acute, attenuate, or aristate;</text>
      <biological_entity id="o2634" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="linear to narrowly" value_original="linear to narrowly" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity id="o2635" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o2634" id="r417" modifier="sometimes" name="exceeding the" negation="false" src="d0_s9" to="o2635" />
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 1.6-4 mm, linear to lanceolate;</text>
      <biological_entity constraint="lower" id="o2636" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1.6-3.6 mm, lanceolate;</text>
      <biological_entity constraint="upper" id="o2637" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 0.9-1.7 mm, glabrous or somewhat sericeous, acute to obtuse;</text>
      <biological_entity id="o2638" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s12" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s12" value="sericeous" value_original="sericeous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas glabrous or sericeous;</text>
      <biological_entity id="o2639" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 3, 0.2-0.3 mm.</text>
      <biological_entity id="o2640" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 0.8-1.2 mm long, 0.5-0.6 mm wide, nearly round in cross-section, with or without a ventral groove, apices acute to broadly obtuse.</text>
      <biological_entity id="o2641" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s15" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="0.6" to_unit="mm" />
        <character constraint="in cross-section" constraintid="o2642" is_modifier="false" modifier="nearly" name="shape" src="d0_s15" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o2642" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
      <biological_entity constraint="ventral" id="o2643" name="groove" name_original="groove" src="d0_s15" type="structure" />
      <biological_entity id="o2644" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="broadly obtuse" />
      </biological_entity>
      <relation from="o2641" id="r418" name="with or without" negation="false" src="d0_s15" to="o2643" />
    </statement>
  </description>
  <discussion>Leptochloa panicea is a cosmopolitan species that somewhat resembles L. chinensis, an aggressive weed that has not yet been found in the Flora region. It differs in its sparsely to densely hairy, rather than glabrous or almost glabrous, sheaths and blades. Two of its three subspecies grow in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.;Miss.;Tenn.;Fla.;Puerto Rico;N.J.;Ala.;Ariz.;Calif.;Ga.;Ill.;Kans.;Ky.;La.;Mo.;N.C.;N.Mex.;Okla.;S.C.;Tex.;S.Dak.;Pa.;Nev.;Va.;Colo.;Virgin Islands;Ind.;Mass.;Ohio;Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes linear to narrowly lanceolate, exceeding the florets; lemmas 0.9-1.2 mm long; caryopses without a ventral groove, often somewhat coarsely rugose, the apices broadly obtuse</description>
      <determination>Leptochloa panicea subsp. mucronata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes lanceolate to narrowly elliptic, not or only slightly exceeding the florets; lemmas 1.3-1.7 mm long; caryopses usually with a narrow, shallow ventral groove, smooth, the apices broadly obtuse to acute</description>
      <determination>Leptochloa panicea subsp. brachiata</determination>
    </key_statement>
  </key>
</bio:treatment>