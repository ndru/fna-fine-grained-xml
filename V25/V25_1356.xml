<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">PENNISETUM</taxon_name>
    <taxon_name authority="(Brongn.) Trin." date="unknown" rank="species">macrostachys</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus pennisetum;species macrostachys</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Pacific fountaingrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, or annual in temperate climates;</text>
      <biological_entity id="o25528" name="climate" name_original="climates" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="temperate" value_original="temperate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o25527" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="in climates" constraintid="o25528" is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 100-300 cm, erect, branching;</text>
      <biological_entity id="o25529" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s2" to="300" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o25530" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves burgundy;</text>
      <biological_entity id="o25531" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="burgundy" value_original="burgundy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths glabrous;</text>
      <biological_entity id="o25532" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.1-0.3 mm;</text>
      <biological_entity id="o25533" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 30-53.5 cm long, (15) 18-35 mm wide, flat, glabrous.</text>
      <biological_entity id="o25534" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s7" to="53.5" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="18" from_unit="mm" name="width" src="d0_s7" to="35" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles terminal, (15.5) 18-40 cm long, 32-50 mm wide, fully exerted from the sheaths, flexible, drooping, burgundy;</text>
      <biological_entity id="o25535" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character name="length" src="d0_s8" unit="cm" value="15.5" value_original="15.5" />
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s8" to="40" to_unit="cm" />
        <character char_type="range_value" from="32" from_unit="mm" name="width" src="d0_s8" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="fully" name="fragility" src="d0_s8" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="burgundy" value_original="burgundy" />
      </biological_entity>
      <biological_entity id="o25536" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o25535" id="r4309" modifier="fully" name="exerted from the" negation="false" src="d0_s8" to="o25536" />
    </statement>
    <statement id="d0_s9">
      <text>rachises terete, shortly pubescent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fascicles 17-22 per cm;</text>
      <biological_entity id="o25537" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="17" name="quantity" src="d0_s10" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fascicle axes 0.5-0.7 mm, with 1 spikelet;</text>
      <biological_entity constraint="fascicle" id="o25538" name="axis" name_original="axes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25539" name="spikelet" name_original="spikelet" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o25538" id="r4310" name="with" negation="false" src="d0_s11" to="o25539" />
    </statement>
    <statement id="d0_s12">
      <text>outer bristles 21-40, 1.2-22.3 mm, scabrous;</text>
      <biological_entity constraint="outer" id="o25540" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="21" name="quantity" src="d0_s12" to="40" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="22.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner bristles absent;</text>
      <biological_entity constraint="inner" id="o25541" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>primary bristles 20-23 mm, not noticeably longer than the other bristles, scabrous.</text>
      <biological_entity constraint="primary" id="o25542" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s14" to="23" to_unit="mm" />
        <character constraint="than the other bristles" constraintid="o25543" is_modifier="false" name="length_or_size" src="d0_s14" value="not noticeably longer" value_original="not noticeably longer" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25543" name="bristle" name_original="bristles" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Spikelets 4.4-4.9 mm, sessile or pedicellate, glabrous;</text>
      <biological_entity id="o25544" name="spikelet" name_original="spikelets" src="d0_s15" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s15" to="4.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pedicels to 0.1 mm;</text>
      <biological_entity id="o25545" name="pedicel" name_original="pedicels" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 1.1 - 1.3 mm, veinless;</text>
      <biological_entity constraint="lower" id="o25546" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="veinless" value_original="veinless" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper glumes 2.1 - 2.8 mm, usually about 1/2 as long as the spikelets, 1-3-veined;</text>
      <biological_entity constraint="upper" id="o25547" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s18" to="2.8" to_unit="mm" />
        <character constraint="as-long-as spikelets" constraintid="o25548" modifier="usually" name="quantity" src="d0_s18" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o25548" name="spikelet" name_original="spikelets" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>lower florets staminate (sterile);</text>
      <biological_entity constraint="lower" id="o25549" name="floret" name_original="florets" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lower lemmas 4-4.5 mm, 5-veined;</text>
      <biological_entity constraint="lower" id="o25550" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s20" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>lower paleas absent or to 2.6 mm;</text>
      <biological_entity constraint="lower" id="o25551" name="palea" name_original="paleas" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s21" value="0-2.6 mm" value_original="0-2.6 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>anthers absent or 1.4-1.6 mm;</text>
      <biological_entity id="o25552" name="anther" name_original="anthers" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s22" value="1.4-1.6 mm" value_original="1.4-1.6 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>upper lemmas 4.3-4.8 mm, 5-veined;</text>
      <biological_entity constraint="upper" id="o25553" name="lemma" name_original="lemmas" src="d0_s23" type="structure">
        <character char_type="range_value" from="4.3" from_unit="mm" name="some_measurement" src="d0_s23" to="4.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>anthers 1.6-1.8 mm. 2n = 68.</text>
      <biological_entity id="o25554" name="anther" name_original="anthers" src="d0_s24" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s24" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25555" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pennisetum macrostachys is native to the South Pacific. It is grown in the Flora region as an ornamental species, being sold as 'Burgundy Giant'.</discussion>
  
</bio:treatment>