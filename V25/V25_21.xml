<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Burmeist." date="unknown" rank="subfamily">ARUNDINOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ARUNDINEAE</taxon_name>
    <taxon_name authority="Adans." date="unknown" rank="genus">PHRAGMITES</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily arundinoideae;tribe arundineae;genus phragmites</taxon_hierarchy>
  </taxon_identification>
  <number>16.03</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous or stoloniferous, often forming dense stands.</text>
      <biological_entity id="o327" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o328" name="stand" name_original="stands" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o327" id="r43" modifier="often" name="forming" negation="false" src="d0_s1" to="o328" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 1-4 m tall, 0.5-1.5 cm thick, leafy;</text>
      <biological_entity id="o329" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="height" src="d0_s2" to="4" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="thickness" src="d0_s2" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes hollow.</text>
      <biological_entity id="o330" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, mostly glabrous;</text>
      <biological_entity id="o331" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths open;</text>
      <biological_entity id="o332" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o333" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat or folded.</text>
      <biological_entity id="o334" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, plumose panicles.</text>
      <biological_entity id="o335" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o336" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 2-8 florets, weakly laterally compressed, lower 1-2 florets staminate, distal 1-2 florets rudimen¬tary, remaining florets bisexual;</text>
      <biological_entity id="o337" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly laterally" name="shape" notes="" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o338" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
      <biological_entity constraint="lower" id="o339" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o340" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o341" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o342" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o337" id="r44" name="with" negation="false" src="d0_s9" to="o338" />
      <relation from="o341" id="r45" name="remaining" negation="false" src="d0_s9" to="o342" />
    </statement>
    <statement id="d0_s10">
      <text>rachilla segments sericeous;</text>
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes and below the florets.</text>
      <biological_entity constraint="rachilla" id="o343" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o344" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o343" id="r46" name="above the glumes and below" negation="false" src="d0_s11" to="o344" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, shorter than the florets, 1-3-veined, glabrous;</text>
      <biological_entity id="o345" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character constraint="than the florets" constraintid="o346" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o346" name="floret" name_original="florets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes much shorter than the upper glumes;</text>
      <biological_entity constraint="lower" id="o347" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="than the upper glumes" constraintid="o348" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="upper" id="o348" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>calluses pilose, hairs 6-12 mm;</text>
      <biological_entity id="o349" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o350" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 3-veined, glabrous, unawned;</text>
      <biological_entity id="o351" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 1-3.</text>
      <biological_entity id="o352" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses rarely maturing, x = 12.</text>
      <biological_entity id="o353" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="rarely" name="life_cycle" src="d0_s17" value="maturing" value_original="maturing" />
      </biological_entity>
      <biological_entity constraint="x" id="o354" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Phragmites is interpreted here as a monotypic genus that has a worldwide distribution. Some taxonomists (e.g., Clayton 1970; Koyama 1987; Scholz and Bohling 2000) recognize 3-4 segregate species. Recent work has identified two different genotypes in North America (Saltonstall 2002) that preliminary data suggest may be morphologicaly distinct (see http://www.invasiveplants.net/). How these genotypes relate to the various segregate species that have been recognized is not yet known.</discussion>
  <discussion>Plants of Phragmites are similar in overall appearance to Arundo, but the latter has subequal glumes, a glabrous rachilla, and hairy lemmas. Vegetatively, plants of Arundo, but not those of Phragmites, have a wedge-shaped, light to dark brown area at the base of the blades. They also tend to have thicker rhizomes, thicker and taller culms, and wider leaves than Phragmites, but there is some overlap. Phragmites is much more widely distributed than Arundo in North America.</discussion>
  <references>
    <reference>Clayton, W.D. 1967. Studies in the Gramineae. XIV. Kew Bull. 21:111-117</reference>
    <reference>Clayton, W.D. 1970. Flora of Tropical East Africa, Gramineae (Part 1). Crown Agents for Oversea Governments and Administrations, London, England. 176 pp.</reference>
    <reference>Hocking, EC, CM. Finlayson, and A.J. Chick. 1983. The biology of Australian weeds. 12. Phragmites australis Trin. ex Steud. J. Austral. Inst. Agric. Sci. 49:123-132</reference>
    <reference>Koyama, T. 1987. Grasses of Japan and Its Neighboring Regions: An Identification Manual. Kodansha, Ltd., Tokyo, Japan. 370 pp.</reference>
    <reference>Saltonstall, K. 2002. Cryptic invasion by a non-native genotype of the common reed, Phragmites australis, into North America. Proc. Natl. Acad. Sci. U.S.A. 99:2445-2449</reference>
    <reference>Scholz, H. and N. Bohling. 2000. Phragmites frutescens (Gramineae) re-visited: The discovery of an overlooked, woody grass in Greece, especially Crete. Vrilldenowia 30:251-261.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Va.;Del.;D.C;Wis.;W.Va.;Pacific Islands (Hawaii);Md.;Fla.;Wyo.;N.H.;N.Mex.;Ind.;La.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;N.C.;Nebr.;Tenn.;Pa.;R.I.;Nev.;Mass.;Maine;Vt.;Puerto Rico;Colo.;Miss.;Calif.;Ala.;Kans.;N.Dak.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Iowa;Ariz.;Idaho;Mich.;Ohio;Minn.;Mont.;Oreg.;S.C.;Tex.;Utah;Mo.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>