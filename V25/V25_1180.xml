<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Dichanthelium</taxon_name>
    <taxon_name authority="(L.) Gould" date="unknown" rank="species">dichotomum</taxon_name>
    <taxon_name authority="(Lam.) Freckmann &amp; Lelong" date="unknown" rank="subspecies">nitidum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section dichanthelium;species dichotomum;subspecies nitidum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants very similar in most respects to subsp.</text>
    </statement>
    <statement id="d0_s1">
      <text>microcarpon.</text>
    </statement>
    <statement id="d0_s2">
      <text>Fall phase freely branching from all nodes, reclining from masses of branchlets and numerous reduced, ciliate blades and secondary panicles.</text>
      <biological_entity id="o26889" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="from nodes" constraintid="o26890" is_modifier="false" modifier="freely" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26890" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character constraint="from masses" constraintid="o26891" is_modifier="false" name="orientation" src="d0_s2" value="reclining" value_original="reclining" />
      </biological_entity>
      <biological_entity id="o26891" name="mass" name_original="masses" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o26892" name="branchlet" name_original="branchlets" src="d0_s2" type="structure" />
      <biological_entity id="o26893" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o26894" name="panicle" name_original="panicles" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o26891" id="r4579" name="part_of" negation="false" src="d0_s2" to="o26892" />
    </statement>
    <statement id="d0_s3">
      <text>Cauline sheaths and blades usually glabrous, lower sheaths and blades sometimes sparsely pubescent.</text>
      <biological_entity constraint="cauline" id="o26895" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o26896" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26897" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26898" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets 1.8-2.5 mm (rarely longer), puberulent or pubescent.</text>
      <biological_entity id="o26899" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Lower glumes less than 1/3 as long as the spikelets, subacute;</text>
      <biological_entity constraint="lower" id="o26900" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o26901" from="0" name="quantity" src="d0_s5" to="1/3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity id="o26901" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>upper glumes and lower lemmas subequal;</text>
      <biological_entity constraint="upper" id="o26902" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o26903" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper florets 1.7-2 mm long, 0.7-1.0 mm wide, subobtuse.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o26904" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1.0" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subobtuse" value_original="subobtuse" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26905" name="chromosome" name_original="" src="d0_s8" type="structure" />
    </statement>
  </description>
  <discussion>Dichanthelium dichotomum subsp. nitidum grows in moist to wet areas, and the borders of swamps. It is primarily a coastal plain taxon, ranging from Virginia to southeastern Texas and Florida.</discussion>
  <discussion>Dichanthelium dichotomum subsp. nitidum is very similar to both subsp. microcarpon and subsp. mattamuskeetense, and intergrades with each occasionally.</discussion>
  
</bio:treatment>