<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">192</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">palmeri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species palmeri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dubioides</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species dubioides</taxon_hierarchy>
  </taxon_identification>
  <number>59</number>
  <other_name type="common_name">Palmer's muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o13656" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-100 cm tall, 2-4 mm thick, erect, rounded near the base, not conspicuously branched, not rooting at the lower nodes;</text>
      <biological_entity id="o13657" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="near base" constraintid="o13658" is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not conspicuously" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
        <character constraint="at lower nodes" constraintid="o13659" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o13658" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="lower" id="o13659" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly glabrous, sometimes puberulent below the nodes.</text>
      <biological_entity id="o13660" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o13661" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o13661" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths longer than the internodes, smooth or scabridulous, not becoming spirally coiled when old;</text>
      <biological_entity id="o13662" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o13663" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity id="o13663" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-3 mm, firm to membranous, truncate, ciliolate;</text>
      <biological_entity id="o13664" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="firm" name="texture" src="d0_s5" to="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 15-50 cm long, 1-3.5 mm wide, flat to involute, smooth or scabridulous abaxially, scabrous adaxially.</text>
      <biological_entity id="o13665" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="involute" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 15-35 cm long, 0.5-2 (3) cm wide;</text>
      <biological_entity id="o13666" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="35" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-8 cm, appressed;</text>
      <biological_entity constraint="primary" id="o13667" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1-6 mm, hispidulous.</text>
      <biological_entity id="o13668" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-4.3 mm, yellowish-brown to purplish.</text>
      <biological_entity id="o13669" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="yellowish-brown" name="coloration" src="d0_s10" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal, (1.7) 2-3.1 mm, about 3/4 as long as the florets, scabridulous distally, 1-veined, apices acute to acuminate, awned, awns to 1.5 mm;</text>
      <biological_entity id="o13670" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="1.7" value_original="1.7" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
        <character constraint="as-long-as florets" constraintid="o13671" name="quantity" src="d0_s11" value="3/4" value_original="3/4" />
        <character is_modifier="false" modifier="distally" name="relief" notes="" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o13671" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o13672" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o13673" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 3-4.3 mm, lanceolate, calluses hairy, hairs to 1.5 mm, lemma bodies scabridulous, apices acuminate, awned, awns 3-10 mm, straight;</text>
      <biological_entity id="o13674" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o13675" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o13676" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o13677" name="body" name_original="bodies" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o13678" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o13679" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas 2.9^1.2 mm, lanceolate, scabridulous, acute to acuminate;</text>
      <biological_entity id="o13680" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2.9" value_original="2.9" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1.2" value_original="1.2" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.5-2.5 mm, yellow to purple-tinged.</text>
      <biological_entity id="o13681" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2-3 mm, fusiform, brownish.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = unknown.</text>
      <biological_entity id="o13682" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13683" name="chromosome" name_original="" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Muhlenbergia palmeri grows in rocky drainages and in sandy soil along creeks, at elevations of 1000-2100 m. Its range extends from southern Arizona into northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>