<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Humb. &amp; Bonpl. ex Willd." date="unknown" rank="species">divaricata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species divaricata</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Poverty grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o9047" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-70 cm, erect or prostrate, unbranched or sparingly branched.</text>
      <biological_entity id="o9048" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves tending to basal;</text>
      <biological_entity id="o9049" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o9050" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o9049" id="r1487" name="tending to" negation="false" src="d0_s3" to="o9050" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths longer than the internodes, glabrous except at the summit;</text>
      <biological_entity id="o9051" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o9052" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
        <character constraint="except " constraintid="o9053" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9052" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o9053" name="summit" name_original="summit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>collars densely pilose;</text>
      <biological_entity id="o9054" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o9055" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 5-20 cm long, 1-2 mm wide, flat to loosely involute, glabrous.</text>
      <biological_entity id="o9056" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="loosely involute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences paniculate, 10-30 cm long, 6-25 cm wide, peduncles flattened and easily broken;</text>
      <biological_entity id="o9057" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s8" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9058" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="easily" name="condition_or_fragility" src="d0_s8" value="broken" value_original="broken" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachis nodes glabrous or with hairs, hairs to 0.5 mm;</text>
      <biological_entity constraint="rachis" id="o9059" name="node" name_original="nodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o9060" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o9061" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o9059" id="r1488" name="with" negation="false" src="d0_s9" to="o9060" />
    </statement>
    <statement id="d0_s10">
      <text>primary branches 5-13 cm, stiffly divaricate to reflexed, with axillary pulvini, usually naked on the basal 1/2;</text>
      <biological_entity constraint="primary" id="o9062" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="13" to_unit="cm" />
        <character is_modifier="false" modifier="stiffly" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character constraint="on basal 1/2" constraintid="o9064" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s10" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o9063" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure" />
      <biological_entity constraint="basal" id="o9064" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <relation from="o9062" id="r1489" name="with" negation="false" src="d0_s10" to="o9063" />
    </statement>
    <statement id="d0_s11">
      <text>secondary branches usually well-developed.</text>
      <biological_entity constraint="secondary" id="o9065" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="development" src="d0_s11" value="well-developed" value_original="well-developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets overlapping, usually appressed, sometimes divergent and the pedicels with axillary pulvini.</text>
      <biological_entity id="o9066" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o9067" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity constraint="axillary" id="o9068" name="pulvinus" name_original="pulvini" src="d0_s12" type="structure" />
      <relation from="o9067" id="r1490" name="with" negation="false" src="d0_s12" to="o9068" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes 8-12 mm, 1-veined, acuminate or shortly awned, awns to 4 mm;</text>
      <biological_entity id="o9069" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o9070" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses about 0.5 mm;</text>
      <biological_entity id="o9071" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 8-13 mm long, the terminal 2-3 mm with 4 or more twists when mature, narrowing to 0.1-0.2 mm wide just below the awns, junction with the awns not evident;</text>
      <biological_entity id="o9072" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s15" to="13" to_unit="mm" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s15" value="terminal" value_original="terminal" />
        <character char_type="range_value" constraint="with awns, junction" constraintid="o9073, o9074" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9073" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="true" name="life_cycle" src="d0_s15" value="mature" value_original="mature" />
        <character is_modifier="true" name="width" src="d0_s15" value="narrowing" value_original="narrowing" />
        <character is_modifier="true" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o9074" name="junction" name_original="junction" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="true" name="life_cycle" src="d0_s15" value="mature" value_original="mature" />
        <character is_modifier="true" name="width" src="d0_s15" value="narrowing" value_original="narrowing" />
        <character is_modifier="true" name="width" src="d0_s15" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o9075" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s15" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o9073" id="r1491" modifier="to 0.1-0.2 mm; to 0.1-0.2 mm" name="with" negation="false" src="d0_s15" to="o9075" />
      <relation from="o9074" id="r1492" modifier="to 0.1-0.2 mm; to 0.1-0.2 mm" name="with" negation="false" src="d0_s15" to="o9075" />
    </statement>
    <statement id="d0_s16">
      <text>awns (7) 10-20 mm, not disarticulating at maturity;</text>
      <biological_entity id="o9076" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="20" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>central awns almost straight to curved at the base, ascending to somewhat divergent distally;</text>
      <biological_entity constraint="central" id="o9077" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o9078" from="straight" name="course" src="d0_s17" to="curved" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s17" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="somewhat; distally" name="arrangement" src="d0_s17" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o9078" name="base" name_original="base" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lateral awns slightly thinner and from much to slightly shorter than the central awns, ascending to divergent;</text>
      <biological_entity constraint="lateral" id="o9079" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s18" value="thinner" value_original="thinner" />
        <character constraint="than the central awns" constraintid="o9080" is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="much to slightly shorter" value_original="much to slightly shorter" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s18" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="central" id="o9080" name="awn" name_original="awns" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 3, 0.8-1 mm.</text>
      <biological_entity id="o9081" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 8-10 mm, light-brown.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 22.</text>
      <biological_entity id="o9082" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s20" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9083" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aristida divaricata grows on dry hills and plains, especially in pinyon-juniper-grassland zones, from the southwestern United States through Mexico to Guatemala. It occasionally intergrades with A. havardii, but that species has lemma beaks that are straight or have only 1-2 twists, shorter primary branches, usually no secondary branches, and pedicels that more frequently have axillary pulvini so the spikelets are more frequently divergent than in A. divaricata.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans.;Okla.;Colo.;Calif.;N.Mex.;Tex.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>