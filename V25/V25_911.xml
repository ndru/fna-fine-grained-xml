<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">274</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Humb. &amp; Bonpl. ex Willd." date="unknown" rank="genus">AEGOPOGON</taxon_name>
    <taxon_name authority="(DC.) Trin." date="unknown" rank="species">tenellus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus aegopogon;species tenellus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aegopogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenellus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">abortivus</taxon_name>
    <taxon_hierarchy>genus aegopogon;species tenellus;variety abortivus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Fragile grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o9332" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 2-30 cm, bases often prostrate or decumbent, strongly branching.</text>
      <biological_entity id="o9333" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9334" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or sparsely hirsute;</text>
      <biological_entity id="o9335" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.7-1.5 mm, lacerate;</text>
      <biological_entity id="o9336" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-7 cm long, 1-2 mm wide, glabrous or puberulent.</text>
      <biological_entity id="o9337" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 2-6 cm.</text>
      <biological_entity id="o9338" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Lateral spikelets 1.5-2.3 mm, on 1-1.3 mm pedicels;</text>
      <biological_entity constraint="lateral" id="o9339" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9340" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <relation from="o9339" id="r1540" name="on" negation="false" src="d0_s7" to="o9340" />
    </statement>
    <statement id="d0_s8">
      <text>central spikelets 2.5-3.2 mm, on 0.3-0.6 mm pedicels.</text>
      <biological_entity constraint="central" id="o9341" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9342" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
      <relation from="o9341" id="r1541" name="on" negation="false" src="d0_s8" to="o9342" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 1.3-1.8 mm, flabellate, lobes rounded, awns 0.1-0.6 mm;</text>
      <biological_entity id="o9343" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flabellate" value_original="flabellate" />
      </biological_entity>
      <biological_entity id="o9344" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9345" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 2.5-3.2 mm, central awns 3-8 mm, lateral awns to 1 mm;</text>
      <biological_entity id="o9346" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o9347" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9348" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.5-0.8 mm. 2n = 20, 60.</text>
      <biological_entity id="o9349" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9350" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
        <character name="quantity" src="d0_s11" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aegopogon tenellus usually grows between 1550-2150 m in shady habitats of moist canyons, but it is sometimes found along roadsides and in other open areas. Its range extends from southern Arizona into northern South America. In some plants, the lateral spikelets are reduced (Aegopogon tenellus var. abortivus (E. Fourn.) Beetle), but such spikelets (and central spikelets with reduced awns) are also found in plants with normal spikelets, so taxonomic recognition is not warranted.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>