<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">382</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="Roth" date="unknown" rank="species">setigera</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species setigera</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>31</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants of indefinite duration.</text>
      <biological_entity id="o20537" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 120 cm tall, bases long-decumbent and rooting at the lower nodes.</text>
      <biological_entity id="o20538" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="height" src="d0_s1" to="120" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20539" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="long-decumbent" value_original="long-decumbent" />
        <character constraint="at lower nodes" constraintid="o20540" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o20540" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths with papillose-based hairs;</text>
      <biological_entity id="o20541" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o20542" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o20541" id="r3499" name="with" negation="false" src="d0_s2" to="o20542" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 2.5-3.5 mm;</text>
      <biological_entity id="o20543" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 4-28 cm long, 4-12 mm wide, scabrous, usually with some scattered papillose-based hairs on the base of the adaxial surfaces, sometimes with hairs all over.</text>
      <biological_entity id="o20544" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="28" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20545" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o20546" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o20547" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o20548" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o20544" id="r3500" modifier="usually" name="with" negation="false" src="d0_s4" to="o20545" />
      <relation from="o20545" id="r3501" name="on" negation="false" src="d0_s4" to="o20546" />
      <relation from="o20546" id="r3502" name="part_of" negation="false" src="d0_s4" to="o20547" />
      <relation from="o20544" id="r3503" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o20548" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles with 3-11 spikelike primary branches in 1-several whorls, rachises to 6 cm;</text>
      <biological_entity id="o20549" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity constraint="primary" id="o20550" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="11" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o20551" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="several" />
      </biological_entity>
      <biological_entity id="o20552" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <relation from="o20549" id="r3504" name="with" negation="false" src="d0_s5" to="o20550" />
      <relation from="o20550" id="r3505" name="in" negation="false" src="d0_s5" to="o20551" />
    </statement>
    <statement id="d0_s6">
      <text>primary branches 5-15 cm, axes wing-margined, wings more than 1/2 as wide as the midribs, lower and middle portions bearing spikelets in unequally pedicellate pairs;</text>
      <biological_entity constraint="primary" id="o20553" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20554" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity id="o20555" name="wing" name_original="wings" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character constraint="as-wide-as midribs" constraintid="o20556" name="quantity" src="d0_s6" value="/2" value_original="/2" />
      </biological_entity>
      <biological_entity id="o20556" name="midrib" name_original="midribs" src="d0_s6" type="structure" />
      <biological_entity constraint="lower and middle" id="o20557" name="portion" name_original="portions" src="d0_s6" type="structure" />
      <biological_entity id="o20558" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity id="o20559" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s6" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o20557" id="r3506" name="bearing" negation="false" src="d0_s6" to="o20558" />
      <relation from="o20557" id="r3507" name="in" negation="false" src="d0_s6" to="o20559" />
    </statement>
    <statement id="d0_s7">
      <text>secondary branches absent;</text>
      <biological_entity constraint="secondary" id="o20560" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>shorter pedicels 0.3-0.8 mm;</text>
      <biological_entity constraint="shorter" id="o20561" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>longer pedicels 1.7-2.7 mm.</text>
      <biological_entity constraint="longer" id="o20562" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.4-3.5 mm, homomorphic, ovate.</text>
      <biological_entity id="o20563" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="homomorphic" value_original="homomorphic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent or to 0.1 mm;</text>
      <biological_entity constraint="lower" id="o20564" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s11" value="0-0.1 mm" value_original="0-0.1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 0.2-1.3 mm, 1/6 - 1/3 as long as the spikelets, 1-3-veined, margins and apices with appressed, white hairs about 0.5 mm, truncate or bilobed;</text>
      <biological_entity constraint="upper" id="o20565" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o20566" from="1/6" name="quantity" src="d0_s12" to="1/3" />
      </biological_entity>
      <biological_entity id="o20566" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <biological_entity id="o20567" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o20568" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o20569" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <relation from="o20567" id="r3508" name="with" negation="false" src="d0_s12" to="o20569" />
      <relation from="o20568" id="r3509" name="with" negation="false" src="d0_s12" to="o20569" />
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas (5) 7-veined, veins smooth or scabrous only over the distal 1/3, unequally spaced, margins and lateral intercostal regions silky-ciliate;</text>
      <biological_entity constraint="lower" id="o20570" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="(5)7-veined" value_original="(5)7-veined" />
      </biological_entity>
      <biological_entity id="o20571" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character constraint="over margins" constraintid="o20572" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20572" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s13" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="1/3" value_original="1/3" />
        <character is_modifier="true" modifier="unequally" name="arrangement" src="d0_s13" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity constraint="lateral intercostal" id="o20573" name="region" name_original="regions" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="silky-ciliate" value_original="silky-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas tan or gray when immature, brown at maturity, acuminate;</text>
      <biological_entity constraint="upper" id="o20574" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s14" value="gray" value_original="gray" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.6-1.3 mm. 2n = 70, 72.</text>
      <biological_entity id="o20575" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20576" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="70" value_original="70" />
        <character name="quantity" src="d0_s15" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria setigera is native to southeastern Asia. It is now established in tropical America, growing in disturbed habitats in Florida and Central America, and probably in tropical South America. It has often been confused with D. sanguinalis.</discussion>
  <discussion>Plants in the Flora region belong to Digitaria setigera Roth var. setigera. Unlike plants of D. setigera var. calliblepharata (Henrard) Veldkamp, they do not have large, glassy hairs on their lower lemmas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Pacific Islands (Hawaii);Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>