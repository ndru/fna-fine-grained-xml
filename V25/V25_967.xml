<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="DC." date="unknown" rank="genus">DANTHONIA</taxon_name>
    <taxon_name authority="Bol." date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus danthonia;species californica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Danthonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">macounii</taxon_name>
    <taxon_hierarchy>genus danthonia;species macounii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Danthonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">americana</taxon_name>
    <taxon_hierarchy>genus danthonia;species californica;variety americana</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">California oatgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (10) 30-130 cm, disarticulating at the nodes at maturity.</text>
      <biological_entity id="o29160" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character constraint="at nodes" constraintid="o29161" is_modifier="false" name="architecture" src="d0_s0" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o29161" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous or pilose, upper sheaths usually glabrous or unevenly pilose;</text>
      <biological_entity id="o29162" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="upper" id="o29163" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 10-30 cm long, (1) 2-5 (6) mm wide, flat to rolled or involute, glabrous or pilose, uppermost cauline blades strongly divergent to reflexed at maturity.</text>
      <biological_entity id="o29164" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character name="width" src="d0_s2" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="rolled or involute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="uppermost cauline" id="o29165" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences usually racemose, with (2) 3-6 (10) widely-spreading spikelets;</text>
      <biological_entity id="o29166" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o29167" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s3" value="10" value_original="10" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="widely-spreading" value_original="widely-spreading" />
      </biological_entity>
      <relation from="o29166" id="r4975" name="with" negation="false" src="d0_s3" to="o29167" />
    </statement>
    <statement id="d0_s4">
      <text>branches flexible, strongly divergent to reflexed at maturity, pulvini usually present at the base;</text>
      <biological_entity id="o29168" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s4" value="divergent" value_original="divergent" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o29169" name="pulvinus" name_original="pulvini" src="d0_s4" type="structure">
        <character constraint="at base" constraintid="o29170" is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o29170" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pedicels on the lowest branch longer than the spikelets, often crinkled.</text>
      <biological_entity id="o29171" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="than the spikelets" constraintid="o29173" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o29172" name="branch" name_original="branch" src="d0_s5" type="structure">
        <character constraint="than the spikelets" constraintid="o29173" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o29173" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <relation from="o29171" id="r4976" name="on" negation="false" src="d0_s5" to="o29172" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets (10) 14-26 (30) mm.</text>
      <biological_entity id="o29174" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calluses of middle florets usually longer than wide, concave abaxially;</text>
      <biological_entity id="o29175" name="callus" name_original="calluses" src="d0_s7" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="usually longer than wide" value_original="usually longer than wide" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="middle" id="o29176" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <relation from="o29175" id="r4977" name="part_of" negation="false" src="d0_s7" to="o29176" />
    </statement>
    <statement id="d0_s8">
      <text>lemma bodies 5-10 mm, glabrous or sparsely pilose over the back, margins pubescent (rarely glabrous), apical teeth (2) 4-6 (7) mm, aristate;</text>
      <biological_entity constraint="lemma" id="o29177" name="body" name_original="bodies" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character constraint="over back" constraintid="o29178" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o29178" name="back" name_original="back" src="d0_s8" type="structure" />
      <biological_entity id="o29179" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="apical" id="o29180" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>awns (7) 8-12 mm;</text>
      <biological_entity id="o29181" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers to 4 mm.</text>
      <biological_entity id="o29182" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 2.5-4.2 mm long, 1.3-1.6 mm wide.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 36.</text>
      <biological_entity id="o29183" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s11" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29184" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Danthonia californica grows in prairies, meadows, and open woods. It has a disjunct distribution, one por¬tion of its range being located in western North America, the other in Chile. An introduced population has been found at Mansfield, Massachusetts.</discussion>
  <discussion>Plants with pilose foliage have been called Danthonia californica var. americana (Scribn.) Hitchc. and plants with sparsely pilose lemma backs D. californica var. macounii Hitchc, but the variation does not appear to be taxonomically significant.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mass.;N.Mex.;Wash.;Utah;Calif.;Oreg.;Alta.;B.C.;Sask.;Mont.;Wyo.;Colo.;Ariz.;Idaho;Nev.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>