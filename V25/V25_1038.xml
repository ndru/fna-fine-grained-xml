<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Soderstr." date="unknown" rank="subfamily">CENTOTHECOIDEAE</taxon_name>
    <taxon_name authority="Ridl." date="unknown" rank="tribe">CENTOTHECEAE</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="genus">CHASMANTHIUM</taxon_name>
    <taxon_name authority="(Poir.) H.O. Yates" date="unknown" rank="species">sessiliflorum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily centothecoideae;tribe centotheceae;genus chasmanthium;species sessiliflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Uniola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sessiliflora</taxon_name>
    <taxon_hierarchy>genus uniola;species sessiliflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chasmanthium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laxum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">sessiliflorum</taxon_name>
    <taxon_hierarchy>genus chasmanthium;species laxum;subspecies sessiliflorum</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Longleaf chasmanthium</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 60-150 cm, (1) 2-3.5 mm thick at the nodes, unbranched, leafy for 40% of their height.</text>
      <biological_entity id="o29401" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="thickness" src="d0_s0" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" constraint="at nodes" constraintid="o29402" from="2" from_unit="mm" name="thickness" src="d0_s0" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="40%" name="height" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o29402" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths pilose;</text>
      <biological_entity id="o29403" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>collars pilose;</text>
      <biological_entity id="o29404" name="collar" name_original="collars" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.2-0.3 mm, entire;</text>
      <biological_entity id="o29405" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (15) 20-50 cm long, 4.5-9.5 (15) mm wide, linear-lanceolate, sparsely pilose adaxially.</text>
      <biological_entity id="o29406" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s4" to="9.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="sparsely; adaxially" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (9) 20-70 cm, contracted or open, erect;</text>
      <biological_entity id="o29407" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="cm" value="9" value_original="9" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s5" to="70" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s5" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches tightly appressed or ascending to strongly divergent;</text>
      <biological_entity id="o29408" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="tightly" name="orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>axils of panicle branches glabrous or scabridulous at the edges;</text>
      <biological_entity id="o29409" name="axil" name_original="axils" src="d0_s7" type="structure" constraint="branch" constraint_original="branch; branch">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="at edges" constraintid="o29411" is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="panicle" id="o29410" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o29411" name="edge" name_original="edges" src="d0_s7" type="structure" />
      <relation from="o29409" id="r5016" name="part_of" negation="false" src="d0_s7" to="o29410" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.3-2.5 (5) mm.</text>
      <biological_entity id="o29412" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 4-10 mm long, 6-9 mm wide, with 4-7 (8) florets, lower 1 (2) florets sterile, fertile florets divergent to 80°.</text>
      <biological_entity id="o29413" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29414" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="8" value_original="8" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <biological_entity constraint="lower" id="o29415" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o29416" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o29417" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="0-80°" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <relation from="o29413" id="r5017" name="with" negation="false" src="d0_s9" to="o29414" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 1.2-2.7 mm, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o29418" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1.4-2.2 mm, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o29419" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses glabrous;</text>
      <biological_entity id="o29420" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>fertile lemmas 3.5-5.9 mm, usually curved or irregularly contorted, 7-9-veined, keels not winged, apices scabridulous;</text>
      <biological_entity id="o29421" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.9" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="irregularly; irregularly" name="arrangement_or_shape" src="d0_s13" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity id="o29422" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o29423" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 2.8-4 mm;</text>
      <biological_entity id="o29424" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (0.8) 1.3-1.6 mm, varying in length within a spikelet.</text>
      <biological_entity id="o29425" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="0.8" value_original="0.8" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character constraint="within spikelet" constraintid="o29426" is_modifier="false" name="length" src="d0_s15" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity id="o29426" name="spikelet" name_original="spikelet" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 2-2.5 mm, exposed at maturity.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 24.</text>
      <biological_entity id="o29427" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" name="prominence" src="d0_s16" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29428" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chasmanthium sessiliflorum grows in rich woods, meadows, and swamps, especially on the coastal plain. It grows throughout most of the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Okla.;Miss.;Tex.;La.;Mo.;Ala.;Tenn.;N.C.;S.C.;Ark.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>