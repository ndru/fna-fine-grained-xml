<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Panicum</taxon_name>
    <taxon_name authority="J. Presl" date="unknown" rank="species">hirticaule</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section panicum;species hirticaule</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pampinosum</taxon_name>
    <taxon_hierarchy>genus panicum;species pampinosum</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Roughstalked wltchgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous or hispid, hairs papillose-based.</text>
      <biological_entity id="o13082" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13083" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 11-110 cm, erect to decumbent;</text>
      <biological_entity id="o13084" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="11" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes shortly hirsute or glabrous.</text>
      <biological_entity id="o13085" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter than the internodes, greenish to purplish, glabrous or with papillose-based hairs, ciliate on 1 margin, glabrous on the other;</text>
      <biological_entity id="o13086" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o13087" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s4" to="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with papillose-based hairs" value_original="with papillose-based hairs" />
        <character constraint="on margin" constraintid="o13089" is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character constraint="on the other" is_modifier="false" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13087" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o13088" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o13089" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o13086" id="r2144" name="with" negation="false" src="d0_s4" to="o13088" />
    </statement>
    <statement id="d0_s5">
      <text>collars hirsute;</text>
      <biological_entity id="o13090" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 1.5-3.5 mm, of hairs;</text>
      <biological_entity id="o13091" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13092" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o13091" id="r2145" name="consists_of" negation="false" src="d0_s6" to="o13092" />
    </statement>
    <statement id="d0_s7">
      <text>blades 3-30 cm long, 3-30 mm wide, flat, usually hirsute or sparsely pubescent, hairs papillose-based, sometimes glabrous, bases rounded to cordate-clasping, margins ciliate, cilia papillose-based, apices acute.</text>
      <biological_entity id="o13093" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13094" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13095" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s7" value="cordate-clasping" value_original="cordate-clasping" />
      </biological_entity>
      <biological_entity id="o13096" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13097" name="cilium" name_original="cilia" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o13098" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 9-30 cm long, 5-8 cm wide, erect or nodding, partially included to well-exserted, rachises glabrous or sparsely hispid basally;</text>
      <biological_entity id="o13099" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s8" to="8" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="nodding" value_original="nodding" />
        <character char_type="range_value" from="partially included" name="position" src="d0_s8" to="well-exserted" />
      </biological_entity>
      <biological_entity id="o13100" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; basally" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches usually alternate to opposite, divergent, secondary branches and pedicels confined to the distal 2/3;</text>
      <biological_entity constraint="primary" id="o13101" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="usually alternate" name="arrangement" src="d0_s9" to="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o13102" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o13103" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pulvini inconspicuous;</text>
      <biological_entity id="o13104" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>secondary branches appressed;</text>
      <biological_entity constraint="secondary" id="o13105" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicels 9-27 mm, appressed.</text>
      <biological_entity id="o13106" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="27" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 1.9-4 mm long, 0.8-1 mm wide, ovoid to almost spherical, often reddish-brown, glabrous, veins prominent, scabridulous, apices abruptly acuminate.</text>
      <biological_entity id="o13107" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid to almost" value_original="ovoid to almost" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s13" value="spherical" value_original="spherical" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13108" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o13109" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="abruptly" name="width" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid to almost" value_original="ovoid to almost" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes 1.3-2.4 mm, 1/2 - 3/4 as long as the spikelets, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o13110" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o13111" from="1/2" name="quantity" src="d0_s14" to="3/4" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o13111" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 1.8-3.3 mm, 7-11-veined;</text>
      <biological_entity constraint="upper" id="o13112" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-11-veined" value_original="7-11-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o13113" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower lemmas similar to the upper glumes, 9-veined;</text>
      <biological_entity constraint="lower" id="o13114" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s17" value="9-veined" value_original="9-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13115" name="glume" name_original="glumes" src="d0_s17" type="structure" />
      <relation from="o13114" id="r2146" name="to" negation="false" src="d0_s17" to="o13115" />
    </statement>
    <statement id="d0_s18">
      <text>lower paleas 0.4-0.9 mm;</text>
      <biological_entity constraint="lower" id="o13116" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s18" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>upper florets 1.5-2.4 mm long, 0.4-0.8 mm wide, ellipsoid, smooth or conspicuously papillate, shiny, stramineous, often with a lunate scar at the base.</text>
      <biological_entity constraint="upper" id="o13117" name="floret" name_original="florets" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s19" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s19" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="conspicuously" name="relief" src="d0_s19" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o13118" name="scar" name_original="scar" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o13119" name="base" name_original="base" src="d0_s19" type="structure" />
      <relation from="o13117" id="r2147" modifier="often" name="with" negation="false" src="d0_s19" to="o13118" />
      <relation from="o13118" id="r2148" name="at" negation="false" src="d0_s19" to="o13119" />
    </statement>
  </description>
  <discussion>Panicum hirticaule grows in rocky or sandy soils in waste places, roadsides, ravines, and wet meadows along streams. Its range extends from southeastern California and southwestern Texas southward through Mexico, Central America, Cuba, and Hispaniola to western South America and Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Nev.;Calif.;Wash.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Blades rounded at the base, 3-16 mm wide; lower paleas less than 1/2 as long as the upper florets; panicles erect</description>
      <determination>Panicum hirticaule subsp. hirticaule</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Blades cordate, clasping at the base, 4-30 mm wide; lower paleas more than 1/2 as long as the upper florets; panicles often nodding.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Nodes, sheaths, and blades glabrous or sparsely pilose, hairs papillose-based; culms usually less than 70 cm tall; spikelets 3.2-4 mm long</description>
      <determination>Panicum hirticaule subsp. stramineum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Nodes, sheaths, and blades hirsute, hairs papillose-based; culms robust, usually more than 70 cm tall; spikelets 3-3.3 mm long</description>
      <determination>Panicum hirticaule subsp. sonorum</determination>
    </key_statement>
  </key>
</bio:treatment>