<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">103</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">airoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species airoides</taxon_hierarchy>
  </taxon_identification>
  <number>48</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations, without rhizomes, not glandular.</text>
      <biological_entity id="o6631" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6632" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o6633" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-110 cm, erect, glabrous below the nodes.</text>
      <biological_entity id="o6634" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="below nodes" constraintid="o6635" is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6635" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or pilose, hairs to 5 mm;</text>
      <biological_entity id="o6636" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o6637" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.2 mm;</text>
      <biological_entity id="o6638" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 8-22 mm long, (1) 2-4 (5) mm wide, flat to folded, glabrous abaxially, scabridulous adaxially.</text>
      <biological_entity id="o6639" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character name="width" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="folded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 18-70 cm long, 3-25 cm wide, diffuse, ovate;</text>
      <biological_entity id="o6640" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s6" to="70" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s6" to="25" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 4-20 cm, appressed or diverging 10-70° from the rachises, naked basally;</text>
      <biological_entity constraint="primary" id="o6641" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o6642" is_modifier="false" modifier="10-70°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="basally" name="architecture" notes="" src="d0_s7" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6642" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous;</text>
      <biological_entity id="o6643" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2.4-11 mm, divergent.</text>
      <biological_entity id="o6644" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 1.3-2 mm long, 0.8-1.8 mm wide, ovate to lanceolate, plumbeous, with 1-3 florets;</text>
      <biological_entity id="o6646" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <relation from="o6645" id="r1049" name="with" negation="false" src="d0_s10" to="o6646" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation acropetal, in the rachilla below the florets, glumes deciduous;</text>
      <biological_entity id="o6645" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="plumbeous" value_original="plumbeous" />
        <character is_modifier="false" name="development" src="d0_s11" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o6647" name="rachillum" name_original="rachilla" src="d0_s11" type="structure" />
      <biological_entity id="o6648" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o6649" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o6647" id="r1050" name="below" negation="false" src="d0_s11" to="o6648" />
    </statement>
    <statement id="d0_s12">
      <text>rachilla prolonged above the terminal floret.</text>
      <biological_entity id="o6650" name="rachillum" name_original="rachilla" src="d0_s12" type="structure">
        <character constraint="above terminal floret" constraintid="o6651" is_modifier="false" name="length" src="d0_s12" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o6651" name="floret" name_original="floret" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes lanceolate to ovate, membranous;</text>
      <biological_entity id="o6652" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.8-1 mm;</text>
      <biological_entity constraint="lower" id="o6653" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 1.1-1.4 mm;</text>
      <biological_entity constraint="upper" id="o6654" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 0.8-1.2 mm, ovate, membranous, plumbeous, keels and lateral-veins inconspicuous, apices obtuse;</text>
      <biological_entity id="o6655" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="plumbeous" value_original="plumbeous" />
      </biological_entity>
      <biological_entity id="o6656" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o6657" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o6658" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas 0.8-1.2 mm, membranous, bases not projecting beyond the lemmas, apices obtuse;</text>
      <biological_entity id="o6659" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o6660" name="base" name_original="bases" src="d0_s17" type="structure">
        <character constraint="beyond lemmas" constraintid="o6661" is_modifier="false" modifier="not" name="orientation" src="d0_s17" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o6661" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o6662" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 0.3-0.5 mm, purplish.</text>
      <biological_entity id="o6663" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s18" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 0.4-0.5 mm, ovoid, reticulate, reddish-brown.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36 (Davidse, pers. comm.).</text>
      <biological_entity id="o6664" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s19" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s19" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6665" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis airoides is a South American species that, in the Flora region, is known only from roadsides and disturbed sites in Brazos County, Texas. It is an enigmatic species, often treated as Sporobolus brasiliensis (Raddi) Hack., which it resembles in its chromosome base number of x = 9 and caryopsis morphology, but its frequent possession of spikelets with more than 1 floret and its mode of spikelet disarticulation argue for its retention in Eragrostis.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>