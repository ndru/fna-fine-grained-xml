<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ISCHAEMUM</taxon_name>
    <taxon_name authority="Salisb." date="unknown" rank="species">rugosum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus ischaemum;species rugosum</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Ribbed murainagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, or short-lived perennials.</text>
      <biological_entity id="o297" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o298" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-130 cm, erect or geniculate at the base, simple to strongly branched below;</text>
      <biological_entity id="o299" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="130" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o300" is_modifier="false" name="shape" src="d0_s1" value="geniculate" value_original="geniculate" />
        <character char_type="range_value" from="simple" modifier="below" name="architecture" notes="" src="d0_s1" to="strongly branched" />
      </biological_entity>
      <biological_entity id="o300" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes antrorsely pilose;</text>
      <biological_entity id="o301" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous.</text>
      <biological_entity id="o302" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline;</text>
      <biological_entity id="o303" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths mostly glabrous or sparsely pilose, margins ciliate distally;</text>
      <biological_entity id="o304" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o305" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>collars villous;</text>
      <biological_entity id="o306" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 1-5.5 mm;</text>
      <biological_entity id="o307" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 8-20 cm long, 7-15 mm wide, flat, usually pilose on both surfaces, sometimes with papillose-based hairs, occasionally glabrous.</text>
      <biological_entity id="o308" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s8" to="20" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character constraint="on surfaces" constraintid="o309" is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o309" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o310" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o308" id="r40" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o310" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescence units with 2 rames;</text>
      <biological_entity id="o312" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o311" id="r41" name="with" negation="false" src="d0_s9" to="o312" />
    </statement>
    <statement id="d0_s10">
      <text>rames 3-8 cm long, 3-4 mm wide;</text>
      <biological_entity constraint="inflorescence" id="o311" name="unit" name_original="units" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>internodes 2.5-3.5 mm, slightly clavate distally.</text>
      <biological_entity id="o313" name="internode" name_original="internodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; distally" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sessile spikelets 3-5 mm long, 1.8-2.2 mm wide;</text>
      <biological_entity id="o314" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s12" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses shortly pubescent;</text>
      <biological_entity id="o315" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes coriaceous, yellowish, not winged, coarsely transversely rugose with 4-5 ridges on the proximal 2/3 - 4/5, thickly chartaceous distally and tapering to the apices;</text>
      <biological_entity constraint="lower" id="o316" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s14" value="winged" value_original="winged" />
        <character constraint="with ridges" constraintid="o317" is_modifier="false" modifier="coarsely transversely" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="thickly; distally" name="pubescence_or_texture" notes="" src="d0_s14" value="chartaceous" value_original="chartaceous" />
        <character constraint="to apices" constraintid="o319" is_modifier="false" name="shape" src="d0_s14" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o317" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o318" name="2/3-4/5" name_original="2/3-4/5" src="d0_s14" type="structure" />
      <biological_entity id="o319" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <relation from="o317" id="r42" name="on" negation="false" src="d0_s14" to="o318" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes thickly chartaceous, ciliate, awned, awns 1.5-2 cm, geniculate below the middle;</text>
      <biological_entity constraint="upper" id="o320" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="thickly" name="pubescence_or_texture" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o321" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s15" to="2" to_unit="cm" />
        <character constraint="below middle awns" constraintid="o322" is_modifier="false" name="shape" src="d0_s15" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o322" name="awn" name_original="awns" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers about 2 mm.</text>
      <biological_entity id="o323" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pedicellate spikelets varying from 0.5 mm to equaling the sessile spikelets.</text>
      <biological_entity id="o325" name="spikelet" name_original="spikelets" src="d0_s17" type="structure">
        <character is_modifier="true" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 18, 20, 44.</text>
      <biological_entity id="o324" name="spikelet" name_original="spikelets" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pedicellate" value_original="pedicellate" />
        <character constraint="to spikelets" constraintid="o325" is_modifier="false" name="variability" src="d0_s17" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity constraint="2n" id="o326" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
        <character name="quantity" src="d0_s18" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ischaemum rugosum is native to southern Asia, and is now established in moist, tropical habitats around the world, including Mexico. It has been found in southern Texas and on chrome ore piles in Canton, Maryland, but is thought to have been eliminated from both areas. The U.S. Department of Agriculture considers it a noxious weed; plants found growing within the continental United States should be promptly reported to that agency.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>