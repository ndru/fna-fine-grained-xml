<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Panicum</taxon_name>
    <taxon_name authority="E. Fourn." date="unknown" rank="species">ghiesbreghtii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section panicum;species ghiesbreghtii</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Ghiesbreght's witchgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o9150" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-120 cm tall, 2-3 mm thick, decumbent to erect, branching from the base and the middle nodes;</text>
      <biological_entity id="o9151" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="height" src="d0_s2" to="120" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="erect" />
        <character constraint="from middle nodes" constraintid="o9153" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o9152" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="middle" id="o9153" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes pilose, hairs spreading;</text>
      <biological_entity id="o9154" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9155" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes hirsute, hairs papillose-based.</text>
      <biological_entity id="o9156" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o9157" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths usually shorter than the internodes, hirsute, lower sheaths more so than those above, hairs papillose-based;</text>
      <biological_entity id="o9158" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o9159" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o9159" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity constraint="lower" id="o9160" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o9161" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>collars densely pilose;</text>
      <biological_entity id="o9162" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.5-4 mm;</text>
      <biological_entity id="o9163" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 16-55 cm long, 0.5-14 mm wide, erect to ascending, abaxial surfaces hirsute, hairs papillose-based, adaxial surfaces densely pilose, midveins prominent and whitish, bases truncate, margins ciliate basally, apices attenuate.</text>
      <biological_entity id="o9164" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s8" to="55" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9165" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o9166" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9167" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9168" name="midvein" name_original="midveins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o9169" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o9170" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9171" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Terminal panicles 7-35 cm long, 5-23 cm wide, about 1/2 as wide as long, shortly exerted or partially included, lax, open;</text>
      <biological_entity constraint="terminal" id="o9172" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s9" to="35" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s9" to="23" to_unit="cm" />
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="false" modifier="shortly; partially" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>axillary panicles smaller, included basally;</text>
      <biological_entity constraint="axillary" id="o9173" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="basally" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>primary branches diverging, lower branches solitary, upper branches solitary to subverticillate;</text>
      <biological_entity constraint="primary" id="o9174" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9175" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>secondary branching primarily in the distal 1/3;</text>
      <biological_entity constraint="upper" id="o9176" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="solitary" name="arrangement" src="d0_s11" to="subverticillate" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="secondary" value_original="secondary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9177" name="1/3" name_original="1/3" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicels 1-4 mm, clavate, spreading to appressed.</text>
      <biological_entity id="o9178" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spikelets 2.6-3.4 mm long, 0.9-1.2 mm wide, ovoid, glabrous.</text>
      <biological_entity id="o9179" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s14" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Lower glumes 1.4-1.7 mm, to 1/2 as long as the spikelets, acute, 5-7-veined;</text>
      <biological_entity constraint="lower" id="o9180" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o9181" from="0" name="quantity" src="d0_s15" to="1/2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o9181" name="spikelet" name_original="spikelets" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>upper glumes and lower lemmas similar, exceeding the upper florets by 0.7-0.9 mm, 9-13-veined;</text>
      <biological_entity constraint="upper" id="o9182" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="9-13-veined" value_original="9-13-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o9183" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="9-13-veined" value_original="9-13-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o9184" name="floret" name_original="florets" src="d0_s16" type="structure" />
      <relation from="o9182" id="r1510" name="exceeding the" negation="false" src="d0_s16" to="o9184" />
      <relation from="o9183" id="r1511" name="exceeding the" negation="false" src="d0_s16" to="o9184" />
    </statement>
    <statement id="d0_s17">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o9185" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s17" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lower paleas 0.5-1.3 mm;</text>
      <biological_entity constraint="lower" id="o9186" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>upper lorets 1.7-2.3 mm long, 0.8-1.1 mm wide, smooth, ovoid.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o9187" name="paleum" name_original="palea" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s19" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s19" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9188" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Panicum ghiesbreghtii grows in low, moist ground, wet thickets, and savannahs, from southern Texas through Mexico, Central America, Cuba, and the West Indies to northern South America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>