<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">arizonica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species arizonica</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Arizona muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o25400" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-50 cm, erect to decumbent;</text>
      <biological_entity id="o25401" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes hispidulous or glabrous below the nodes.</text>
      <biological_entity id="o25402" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
        <character constraint="below nodes" constraintid="o25403" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25403" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths from slightly shorter to slightly longer than the internodes, rounded to somewhat flattened but not keeled, hispidulous basally, glabrous distally, margins hyaline, not becoming spirally coiled when old;</text>
      <biological_entity id="o25404" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="rounded to somewhat" value_original="rounded to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s4" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25406" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s4" value="shorter to slightly" value_original="shorter to slightly" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o25405" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s4" value="shorter to slightly" value_original="shorter to slightly" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o25407" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
      <relation from="o25404" id="r4280" name="from" negation="false" src="d0_s4" to="o25406" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm, hyaline, obtuse, minutely erose;</text>
      <biological_entity id="o25408" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 4-7 cm long, 0.8-2 mm wide, flat or folded, glabrous abaxially, scabridulous or hispidulous adaxially, midveins and margins conspicuous, thickened, white, and cartilaginous.</text>
      <biological_entity id="o25409" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o25410" name="midvein" name_original="midveins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity id="o25411" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s6" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-20 cm long, 4-15 cm wide, diffuse;</text>
      <biological_entity id="o25412" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s7" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-7.5 cm, capillary, diverging 40-90° from the rachises, naked basally;</text>
      <biological_entity constraint="primary" id="o25413" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="7.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
        <character constraint="from rachises" constraintid="o25414" is_modifier="false" modifier="40-90°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="basally" name="architecture" notes="" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o25414" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2-16 mm, flexuous.</text>
      <biological_entity id="o25415" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s9" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.1-3.1 mm.</text>
      <biological_entity id="o25416" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s10" to="3.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes equal, 1-1.5 mm, 1-veined, apices scabridulous, obtuse to acute, some¬times minutely erose, unawned;</text>
      <biological_entity id="o25417" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o25418" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2-3.1 mm, elliptic, purplish, appressed-pubescent on the lower 3/4 of the midveins and margins, hairs to 0.6 mm, apices scabrous, acute, minutely bifid, awned, awns 0.5-1.1 mm;</text>
      <biological_entity id="o25419" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character constraint="on lower 3/4" constraintid="o25420" is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25420" name="3/4" name_original="3/4" src="d0_s12" type="structure" />
      <biological_entity id="o25421" name="midvein" name_original="midveins" src="d0_s12" type="structure" />
      <biological_entity id="o25422" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o25423" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25424" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s12" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o25425" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
      <relation from="o25420" id="r4281" name="part_of" negation="false" src="d0_s12" to="o25421" />
      <relation from="o25420" id="r4282" name="part_of" negation="false" src="d0_s12" to="o25422" />
    </statement>
    <statement id="d0_s13">
      <text>paleas 2.1-3.2 mm, elliptic, glabrous, acute;</text>
      <biological_entity id="o25426" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s13" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.6-2.1 mm, purplish.</text>
      <biological_entity id="o25427" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s14" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 1.3-1.7 mm, fusiform, sulcate dorsally, brownish.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20.</text>
      <biological_entity id="o25428" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="dorsally" name="architecture" src="d0_s15" value="sulcate" value_original="sulcate" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25429" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia arizonica grows in sandy drainages and gravelly canyons, and on plateaus and rocky slopes in open desert grasslands, at elevations of 1220-2230 m. Its range extends from the southwestern United States into northwestern Mexico. Flowering is from August to October.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>