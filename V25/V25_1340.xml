<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">509</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">ERIOCHLOA</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">contracta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus eriochloa;species contracta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Prairie cupgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o8662" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-100 cm, erect or decumbent, sometimes rooting at the lower nodes;</text>
      <biological_entity id="o8663" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o8664" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8664" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes pilose or pubescent;</text>
      <biological_entity id="o8665" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes pubescent to puberulent.</text>
      <biological_entity id="o8666" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s4" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths sparsely to densely pubescent;</text>
      <biological_entity id="o8667" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.4-1.1 mm;</text>
      <biological_entity id="o8668" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 6-12 (22) cm long, 2-8 mm wide, linear, flat to conduplicate, straight, appressed to divergent, both surfaces sparsely to densely pubescent with short, evenly spaced hairs.</text>
      <biological_entity id="o8669" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="22" value_original="22" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s7" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o8670" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character constraint="with hairs" constraintid="o8671" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8671" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" modifier="evenly" name="arrangement" src="d0_s7" value="spaced" value_original="spaced" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 6-20 cm long, 0.3-1.2 cm wide;</text>
      <biological_entity id="o8672" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s8" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s8" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachises pilose, longer hairs 0.1-0.8 mm;</text>
      <biological_entity id="o8673" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="longer" id="o8674" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches 10-20 (28), 15-45 (60) mm long, 0.2-0.4 mm wide, appressed, pubescent to setose, not winged, with 8-16 mostly solitary spikelets, occasionally paired at the base of the branches;</text>
      <biological_entity id="o8675" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character name="atypical_quantity" src="d0_s10" value="28" value_original="28" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="20" />
        <character name="length" src="d0_s10" unit="mm" value="60" value_original="60" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="45" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s10" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s10" to="setose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
        <character constraint="at base" constraintid="o8677" is_modifier="false" modifier="occasionally" name="arrangement" notes="" src="d0_s10" value="paired" value_original="paired" />
      </biological_entity>
      <biological_entity id="o8676" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s10" to="16" />
        <character is_modifier="true" modifier="mostly" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o8677" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o8678" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <relation from="o8675" id="r1406" name="with" negation="false" src="d0_s10" to="o8676" />
      <relation from="o8677" id="r1407" name="part_of" negation="false" src="d0_s10" to="o8678" />
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.2-1 mm, variously hirsute below, apices with fewer than 10 hairs more than 0.5 mm long.</text>
      <biological_entity id="o8679" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="variously; below" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o8680" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o8681" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s11" to="10" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s11" upper_restricted="false" />
      </biological_entity>
      <relation from="o8680" id="r1408" name="with" negation="false" src="d0_s11" to="o8681" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets (3.1) 3.5-4.5 (5) mm long, 1.2-1.7 mm wide, lanceolate.</text>
      <biological_entity id="o8682" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="3.1" value_original="3.1" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Upper glumes as long as the lower lemmas, with sparsely appressed pubescence on the lower 2/3, scabrous or glabrous distally, 3-9-veined, acuminate and awned, awns 0.4-1 mm;</text>
      <biological_entity constraint="upper" id="o8683" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-9-veined" value_original="3-9-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8684" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity constraint="lower" id="o8685" name="2/3" name_original="2/3" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="sparsely" name="fixation_or_orientation" src="d0_s13" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="character" src="d0_s13" value="pubescence" value_original="pubescence" />
      </biological_entity>
      <biological_entity id="o8686" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o8683" id="r1409" name="as long as" negation="false" src="d0_s13" to="o8684" />
      <relation from="o8683" id="r1410" name="with" negation="false" src="d0_s13" to="o8685" />
    </statement>
    <statement id="d0_s14">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o8687" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 3-4.3 mm long, 1.2-1.7 mm wide, lanceolate, setose, 3-7-veined, acuminate, unawned or mucronate;</text>
      <biological_entity constraint="lower" id="o8688" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-7-veined" value_original="3-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower paleas absent;</text>
      <biological_entity constraint="lower" id="o8689" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas 2-2.5 mm, indurate, elliptic, 5-7-veined, acute to rounded and awned, awns 0.4-1.1 mm;</text>
      <biological_entity constraint="upper" id="o8690" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-7-veined" value_original="5-7-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s17" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o8691" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper paleas indurate, faintly rugose, blunt.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 36.</text>
      <biological_entity constraint="upper" id="o8692" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" name="texture" src="d0_s18" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="faintly" name="relief" src="d0_s18" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="shape" src="d0_s18" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8693" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eriochloa contracta grows in fields, ditches, and other disturbed areas. It is known only from the United States, being native and common in the central United States, and adventive to the east and southwest. It differs from E. acuminata in its tightly contracted, almost cylindrical panicles and longer lemma awns, but intermediate forms can be found. It can also be confused with first-year plants of the perennial E. punctata, which have glabrous leaves, narrower and more tapering spikelets, and longer lemma awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>D.C.;Fla.;N.Mex.;Tex.;La.;Ala.;Ind.;Tenn.;Nev.;Va.;Colo.;Calif.;Ark.;Ill.;Ariz.;Ont.;Ohio;Utah;Mo.;Kans.;Minn.;Nebr.;Okla.;Miss.;S.C.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>