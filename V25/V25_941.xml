<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Reeder;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Reeder" date="unknown" rank="tribe">ORCUTTIEAE</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="genus">ORCUTTIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe orcuttieae;genus orcuttia</taxon_hierarchy>
  </taxon_identification>
  <number>19.01</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>viscid-aromatic, pilose, sometimes sparsely so, producing long, juvenile, floating basal leaves.</text>
      <biological_entity id="o17392" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="odor" src="d0_s1" value="viscid-aromatic" value_original="viscid-aromatic" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17393" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="juvenile" value_original="juvenile" />
        <character is_modifier="true" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
      <relation from="o17392" id="r2922" modifier="sometimes sparsely; sparsely" name="producing" negation="false" src="d0_s1" to="o17393" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 3-35 cm, erect, ascending, or decumbent, sometimes becoming prostrate, not breaking apart at the nodes, usually branching only at the lower nodes.</text>
      <biological_entity id="o17394" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="sometimes becoming" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="at nodes" constraintid="o17395" is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
        <character constraint="at lower nodes" constraintid="o17396" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o17395" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity constraint="lower" id="o17396" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves without ligules, sometimes with a collar line visible at the junction of the sheath and blade, especially when dry;</text>
      <biological_entity id="o17397" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17398" name="ligule" name_original="ligules" src="d0_s3" type="structure" />
      <biological_entity constraint="collar" id="o17399" name="line" name_original="line" src="d0_s3" type="structure">
        <character constraint="at junction" constraintid="o17400" is_modifier="false" name="prominence" src="d0_s3" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o17400" name="junction" name_original="junction" src="d0_s3" type="structure" />
      <biological_entity id="o17401" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
      <biological_entity id="o17402" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o17397" id="r2923" name="without" negation="false" src="d0_s3" to="o17398" />
      <relation from="o17397" id="r2924" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o17399" />
      <relation from="o17400" id="r2925" name="part_of" negation="false" src="d0_s3" to="o17401" />
      <relation from="o17400" id="r2926" name="part_of" negation="false" src="d0_s3" to="o17402" />
    </statement>
    <statement id="d0_s4">
      <text>blades flat or becoming involute in drying.</text>
      <biological_entity id="o17403" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character constraint="in drying" is_modifier="false" modifier="becoming" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, clavate to capitate spikes, exserted at maturity, spikelets distichously arranged;</text>
      <biological_entity id="o17404" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="clavate" name="shape" src="d0_s5" to="capitate" />
        <character constraint="at maturity" is_modifier="false" name="position" notes="" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o17405" name="spike" name_original="spikes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>disarticulation tardy, above the glumes and between the florets.</text>
      <biological_entity id="o17406" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distichously" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o17407" name="floret" name_original="florets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets laterally compressed, with 4-40 florets.</text>
      <biological_entity id="o17408" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o17409" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
      <relation from="o17408" id="r2927" name="with" negation="false" src="d0_s7" to="o17409" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes irregularly 2-5-toothed;</text>
      <biological_entity id="o17410" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s8" value="2-5-toothed" value_original="2-5-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas deeply cleft and strongly 5-veined, veins terminating in prominent mucronate or awn-tipped teeth 1/3 - 1/2 or more as long as the lemma bodies, each tooth with an additional weaker vein on either side of a strong central vein, these extending about halfway to the base of the lemma;</text>
      <biological_entity id="o17411" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s9" value="cleft" value_original="cleft" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s9" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o17412" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o17413" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="awn-tipped" value_original="awn-tipped" />
        <character char_type="range_value" constraint="as-long-as lemma bodies" constraintid="o17414" from="1/3" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o17414" name="body" name_original="bodies" src="d0_s9" type="structure" />
      <biological_entity id="o17415" name="tooth" name_original="tooth" src="d0_s9" type="structure">
        <character constraint="to base" constraintid="o17419" is_modifier="false" name="position" notes="" src="d0_s9" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity id="o17416" name="vein" name_original="vein" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="additional" value_original="additional" />
        <character is_modifier="true" name="fragility" src="d0_s9" value="weaker" value_original="weaker" />
      </biological_entity>
      <biological_entity id="o17417" name="side" name_original="side" src="d0_s9" type="structure" />
      <biological_entity constraint="central" id="o17418" name="vein" name_original="vein" src="d0_s9" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s9" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o17419" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o17420" name="lemma" name_original="lemma" src="d0_s9" type="structure" />
      <relation from="o17412" id="r2928" name="terminating in" negation="false" src="d0_s9" to="o17413" />
      <relation from="o17415" id="r2929" name="with" negation="false" src="d0_s9" to="o17416" />
      <relation from="o17416" id="r2930" name="on" negation="false" src="d0_s9" to="o17417" />
      <relation from="o17417" id="r2931" name="part_of" negation="false" src="d0_s9" to="o17418" />
      <relation from="o17419" id="r2932" name="part_of" negation="false" src="d0_s9" to="o17420" />
    </statement>
    <statement id="d0_s10">
      <text>paleas well-developed, 2-veined;</text>
      <biological_entity id="o17421" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character is_modifier="false" name="development" src="d0_s10" value="well-developed" value_original="well-developed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-veined" value_original="2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lodicules absent;</text>
      <biological_entity id="o17422" name="lodicule" name_original="lodicules" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3, white or pinkish, exserted on long, slender, ribbonlike filaments at anthesis;</text>
      <biological_entity id="o17423" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pinkish" value_original="pinkish" />
      </biological_entity>
      <biological_entity id="o17424" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="true" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="ribbonlike" value_original="ribbonlike" />
      </biological_entity>
      <relation from="o17423" id="r2933" name="exserted on" negation="false" src="d0_s12" to="o17424" />
    </statement>
    <statement id="d0_s13">
      <text>styles 2, apical, elongate, filiform, stigmatic for 1/3 – 1/2 of their length;</text>
      <biological_entity id="o17425" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s13" value="apical" value_original="apical" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform" value_original="filiform" />
        <character constraint="for" is_modifier="false" name="structure_in_adjective_form" src="d0_s13" value="stigmatic" value_original="stigmatic" />
        <character char_type="range_value" from="1/3" name="length" src="d0_s13" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmatic hairs short, often sparse.</text>
      <biological_entity constraint="stigmatic" id="o17426" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" modifier="often" name="count_or_density" src="d0_s14" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses slightly compressed laterally, oblong to elliptic;</text>
      <biological_entity id="o17427" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly; laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryos 3/4 as long as to equaling the caryopses;</text>
      <biological_entity id="o17429" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character is_modifier="true" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>epiblast absent, x = 10, probably.</text>
      <biological_entity id="o17428" name="embryo" name_original="embryos" src="d0_s16" type="structure">
        <character constraint="to caryopses" constraintid="o17429" name="quantity" src="d0_s16" value="3/4" value_original="3/4" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o17430" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" unit=",probably" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orcuttia is a genus of five species, all of which are restricted to vernal pools and similar habitats in California and northern Baja California, Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemma teeth unequal, the central tooth the longest.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 6-7 mm long, the teeth terminating in awns at least 1 mm long; caryopses 2.3-2.5 mm long</description>
      <determination>1 Orcuttia viscida</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 4-5 mm long, the teeth sharp-pointed or with awns to 0.5 mm long; caryopses 1.3-1.8 mm long.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants sparingly hairy; culms usually prostrate; spikes clavate</description>
      <determination>2 Orcuttia californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants conspicuously hairy, grayish; culms erect or decumbent; spikes somewhat capitate</description>
      <determination>3 Orcuttia inaequalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemma teeth essentially equal in length.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Culms usually prostrate; caryopses 1.5-1.8 mm long</description>
      <determination>2 Orcuttia californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Culms erect, ascending, or decumbent; caryopses 2-3 mm long.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Culms 1-2 mm thick, branching only at the lower nodes; spikes congested, crowded towards the top; leaf blades 3-5 mm wide</description>
      <determination>4 Orcuttia pilosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Culms 0.5-1 mm thick, often branching from the upper nodes; spikes not congested, even towards the top; leaf blades 1.5-2 mm wide</description>
      <determination>5 Orcuttia tenuis</determination>
    </key_statement>
  </key>
</bio:treatment>