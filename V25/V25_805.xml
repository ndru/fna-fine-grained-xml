<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="Bisch." date="unknown" rank="species">cucullata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species cucullata</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Hooded windmill-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o14744" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-60 cm, erect.</text>
      <biological_entity id="o14745" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o14746" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.7-1 mm;</text>
      <biological_entity id="o14747" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 20 cm long, 2-4 mm wide, without basal hairs, glabrous or scabrous, upper cauline leaves often greatly reduced.</text>
      <biological_entity id="o14748" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14749" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity constraint="upper cauline" id="o14750" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o14748" id="r2455" name="without" negation="false" src="d0_s5" to="o14749" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 10-20 branches in several closely-spaced whorls;</text>
      <biological_entity id="o14751" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o14752" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o14753" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="several" value_original="several" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="closely-spaced" value_original="closely-spaced" />
      </biological_entity>
      <relation from="o14751" id="r2456" name="with" negation="false" src="d0_s6" to="o14752" />
      <relation from="o14752" id="r2457" name="in" negation="false" src="d0_s6" to="o14753" />
    </statement>
    <statement id="d0_s7">
      <text>branches 2-5 cm, spreading, with 14-18 spikelets per cm;</text>
      <biological_entity id="o14755" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" is_modifier="true" name="quantity" src="d0_s7" to="18" />
      </biological_entity>
      <biological_entity id="o14756" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o14754" id="r2458" name="with" negation="false" src="d0_s7" to="o14755" />
      <relation from="o14755" id="r2459" name="per" negation="false" src="d0_s7" to="o14756" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation beneath the glumes.</text>
      <biological_entity id="o14754" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o14757" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <relation from="o14754" id="r2460" name="beneath" negation="false" src="d0_s8" to="o14757" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 1 bisexual and 1 sterile floret.</text>
      <biological_entity id="o14758" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o14759" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o14758" id="r2461" name="with" negation="false" src="d0_s9" to="o14759" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 0.5-0.7 mm;</text>
      <biological_entity constraint="lower" id="o14760" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1-1.5 mm;</text>
      <biological_entity constraint="upper" id="o14761" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lowest lemmas 1.5-2 mm long, 0.7-1 mm wide, broadly elliptic, mostly glabrous but the keels and marginal veins appressed-pilose, obtuse, awned, awns 0.3-1.5 mm;</text>
      <biological_entity constraint="lowest" id="o14762" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14763" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pilose" value_original="appressed-pilose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o14764" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pilose" value_original="appressed-pilose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>second florets 1-1.5 mm long and about equally wide, conspicuously inflated, spherical, with the distal portion of the margins inrolled, not or inconspicuously bilobed, lobes less than 1/5 as long as the lemmas, midveins sometimes excurrent to 1.5 mm.</text>
      <biological_entity id="o14765" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14766" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="equally" name="width" src="d0_s13" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s13" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s13" value="spherical" value_original="spherical" />
        <character is_modifier="false" modifier="not; inconspicuously" name="architecture_or_shape" notes="" src="d0_s13" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14767" name="portion" name_original="portion" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s13" value="inrolled" value_original="inrolled" />
      </biological_entity>
      <biological_entity id="o14768" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o14769" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o14770" from="0" name="quantity" src="d0_s13" to="1/5" />
      </biological_entity>
      <biological_entity id="o14770" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o14771" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="excurrent" value_original="excurrent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o14766" id="r2462" name="with" negation="false" src="d0_s13" to="o14767" />
      <relation from="o14767" id="r2463" name="part_of" negation="false" src="d0_s13" to="o14768" />
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 0.9-1.2 mm long, about 0.5 mm wide, obovoid.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 40.</text>
      <biological_entity id="o14772" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s14" to="1.2" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14773" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloris cucullata is common along roadsides and in waste areas throughout much of Texas and adjacent portions of New Mexico and Mexico. Records from outside this area probably represent introductions. Chloris cucullata hybridizes with both C. andropogonoides and C. verticillata (see discussion under C. andropogonoides).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Okla.;N.Mex.;Tex.;Kans.;S.C.;Ark.;Ariz.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>