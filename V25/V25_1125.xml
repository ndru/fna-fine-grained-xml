<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J.K. Wipff;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">404</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">SACCIOLEPIS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus sacciolepis</taxon_hierarchy>
  </taxon_identification>
  <number>25.08</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, stoloniferous, or cespitose.</text>
      <biological_entity id="o5067" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-150 cm, not woody, branched above the base;</text>
      <biological_entity id="o5068" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character constraint="above base" constraintid="o5069" is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o5069" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes hollow.</text>
      <biological_entity id="o5070" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline;</text>
      <biological_entity id="o5071" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles sometimes present;</text>
      <biological_entity id="o5072" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous, sometimes ciliate;</text>
      <biological_entity id="o5073" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat or rolled, with or without cross venation.</text>
      <biological_entity id="o5074" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rolled" value_original="rolled" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, usually contracted, dense panicles, distal 1/2 of the rachises concealed by the spikelets;</text>
      <biological_entity id="o5075" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="usually" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o5076" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="true" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character constraint="of rachises" constraintid="o5077" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o5077" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character constraint="by spikelets" constraintid="o5078" is_modifier="false" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o5078" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>branches fused to the rachises or free and appressed to ascending;</text>
      <biological_entity id="o5079" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character constraint="to rachises" constraintid="o5080" is_modifier="false" name="fusion" src="d0_s9" value="fused" value_original="fused" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="free" value_original="free" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
      <biological_entity id="o5080" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels with discoid apices;</text>
      <biological_entity id="o5082" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s10" value="discoid" value_original="discoid" />
      </biological_entity>
      <relation from="o5081" id="r811" name="with" negation="false" src="d0_s10" to="o5082" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation below the glumes and below the upper florets.</text>
      <biological_entity id="o5081" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity constraint="upper" id="o5083" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o5081" id="r812" name="below the glumes and below" negation="false" src="d0_s11" to="o5083" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets bisexual, with 2 florets, rounded to acute;</text>
      <biological_entity id="o5084" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="rounded" name="shape" notes="" src="d0_s12" to="acute" />
      </biological_entity>
      <biological_entity id="o5085" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <relation from="o5084" id="r813" name="with" negation="false" src="d0_s12" to="o5085" />
    </statement>
    <statement id="d0_s13">
      <text>rachilla segments not swollen.</text>
      <biological_entity constraint="rachilla" id="o5086" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Glumes unequal, prominently veined, unawned;</text>
      <biological_entity id="o5087" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 3-7-veined;</text>
      <biological_entity constraint="lower" id="o5088" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper glumes as long as or exceeding the upper florets, distinctly saccate or gibbous, 5-13-veined;</text>
      <biological_entity constraint="upper" id="o5089" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s16" value="saccate" value_original="saccate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-13-veined" value_original="5-13-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5090" name="floret" name_original="florets" src="d0_s16" type="structure" />
      <relation from="o5089" id="r814" name="exceeding the" negation="false" src="d0_s16" to="o5090" />
    </statement>
    <statement id="d0_s17">
      <text>lower florets 0.8-1.9 mm, sterile or staminate, less than 1/2 as long as the spikelets;</text>
      <biological_entity constraint="lower" id="o5091" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s17" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o5092" from="0" name="quantity" src="d0_s17" to="1/2" />
      </biological_entity>
      <biological_entity id="o5092" name="spikelet" name_original="spikelets" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lower lemmas resembling the upper glumes but not saccate, sometimes with a transverse row of hairs, 5-9-veined, unawned;</text>
      <biological_entity constraint="lower" id="o5093" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s18" value="saccate" value_original="saccate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5094" name="glume" name_original="glumes" src="d0_s18" type="structure" />
      <biological_entity id="o5095" name="row" name_original="row" src="d0_s18" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s18" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o5096" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <relation from="o5093" id="r815" name="resembling the" negation="false" src="d0_s18" to="o5094" />
      <relation from="o5093" id="r816" modifier="sometimes" name="with" negation="false" src="d0_s18" to="o5095" />
      <relation from="o5095" id="r817" name="part_of" negation="false" src="d0_s18" to="o5096" />
    </statement>
    <statement id="d0_s19">
      <text>lower paleas present or absent, 0-2-veined;</text>
      <biological_entity constraint="lower" id="o5097" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="0-2-veined" value_original="0-2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>upper lemmas subcoriaceous to subindurate, dorsally compressed, glabrous, smooth, margins inrolled or flat, never hyaline, faintly 3-5-veined;</text>
      <biological_entity constraint="upper" id="o5098" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character char_type="range_value" from="subcoriaceous" name="texture" src="d0_s20" to="subindurate" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5099" name="margin" name_original="margins" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="shape" src="d0_s20" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="never" name="coloration" src="d0_s20" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s20" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>upper paleas similar to the lemmas, 2-veined;</text>
      <biological_entity constraint="upper" id="o5100" name="palea" name_original="paleas" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s21" value="2-veined" value_original="2-veined" />
      </biological_entity>
      <biological_entity id="o5101" name="lemma" name_original="lemmas" src="d0_s21" type="structure" />
      <relation from="o5100" id="r818" name="to" negation="false" src="d0_s21" to="o5101" />
    </statement>
    <statement id="d0_s22">
      <text>lodicules 2, fleshy, glabrous, x = 9.</text>
      <biological_entity id="o5102" name="lodicule" name_original="lodicules" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s22" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o5103" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sacciolepis is a genus of 30 species. It is represented throughout the tropics and subtropics, primarily in Africa. Two species grow in the Flora region. One is native; the other is an introduction that has become established. Most species grow along and in ponds, lakes, streams, ditches, and other moist areas. The prominently multi-veined, saccate upper glumes and contracted panicles distinguish Sacciolepis from all other grasses in the Flora region.</discussion>
  <references>
    <reference>Judziewicz, E.J. 1990. A new South American species of Sacciolepis (Poaceae: Panicoideae: Paniceae), with a summary of the genus in the New World. Syst. Bot. 15:415-420</reference>
    <reference>Simon, B.K. 1972. A revision to the genus Sacciolepis (Gramineae) in the "Flora Zambesiaca" area. Kew Bull. 27:387-406.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Md.;N.J.;Del.;Mass.;Maine;Okla.;Miss.;Tex.;La.;Mo.;Ala.;Tenn.;N.C.;S.C.;Va.;Ark.;Pacific Islands (Hawaii);Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Primary branches fused to the rachises for at least 3/4 of their length; lower branches 0.1-0.5 cm long; upper glumes 9-veined; paleas of the lower florets 0.5-1 mm long, to 1/2 as long as the lower lemmas</description>
      <determination>1 Sacciolepis indica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Primary branches ascending, free from the rachises; lower branches 0.4-11.5 cm long; upper glumes 11(12)-veined; paleas of the lower florets 2-4 mm long, 3/4 to almost as long as the lower lemmas</description>
      <determination>2 Sacciolepis striata</determination>
    </key_statement>
  </key>
</bio:treatment>