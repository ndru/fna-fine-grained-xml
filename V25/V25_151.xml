<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">palmeri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species palmeri</taxon_hierarchy>
  </taxon_identification>
  <number>34</number>
  <other_name type="common_name">Rio grande lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations and knotty bases, without rhizomes, not glandular.</text>
      <biological_entity id="o17608" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17609" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o17610" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
      <biological_entity id="o17611" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-90 (120) cm, glabrous below the nodes.</text>
      <biological_entity id="o17612" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="120" value_original="120" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character constraint="below nodes" constraintid="o17613" is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17613" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths villous and the hairs not papillose-based, or mostly glabrous, apices hairy, hairs to 5 mm, not papillose-based;</text>
      <biological_entity id="o17614" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o17615" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17616" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17617" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o17618" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (14) 20-35 cm long, 1-2.4 mm wide, involute, abaxial surfaces glabrous, adaxial surfaces scabridulous, sometimes sparsely hairy.</text>
      <biological_entity id="o17619" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="14" value_original="14" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17620" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17621" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 12-40 cm long, 4-20 cm wide, oblong, open;</text>
      <biological_entity id="o17622" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s6" to="20" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 2-20 cm, diverging 20-70° from the rachises, capillary;</text>
      <biological_entity constraint="primary" id="o17623" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character constraint="from rachises" constraintid="o17624" is_modifier="false" modifier="20-70°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="capillary" value_original="capillary" />
      </biological_entity>
      <biological_entity id="o17624" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous or sparsely hairy;</text>
      <biological_entity id="o17625" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels (0.4) 1-4 (14) mm, appressed or diverging, only the terminal pedicels on each branch longer than 4 mm.</text>
      <biological_entity id="o17626" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o17627" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o17628" name="branch" name_original="branch" src="d0_s9" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <relation from="o17627" id="r2960" name="on" negation="false" src="d0_s9" to="o17628" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-6 (7.3) mm long, 1-2 mm wide, linear-lanceolate, plumbeous, with 5-12 florets;</text>
      <biological_entity id="o17630" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
      <relation from="o17629" id="r2961" name="with" negation="false" src="d0_s10" to="o17630" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation acropetal, paleas persistent.</text>
      <biological_entity id="o17629" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="7.3" value_original="7.3" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="plumbeous" value_original="plumbeous" />
        <character is_modifier="false" name="development" src="d0_s11" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o17631" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes lanceolate to ovate, hyaline;</text>
      <biological_entity id="o17632" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1.1-1.8 mm;</text>
      <biological_entity constraint="lower" id="o17633" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.2-2.2 mm, exceeded by the basal lemmas;</text>
      <biological_entity constraint="upper" id="o17634" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17635" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o17634" id="r2962" name="exceeded by the" negation="false" src="d0_s14" to="o17635" />
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2-2.6 mm, ovate, membranous, hyaline towards the apices and margins, keels weak or strong, without glands, lateral-veins from inconspicuous to conspicuous, apices acute;</text>
      <biological_entity id="o17636" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character constraint="towards margins" constraintid="o17638" is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o17637" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o17638" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity id="o17639" name="keel" name_original="keels" src="d0_s15" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s15" value="weak" value_original="weak" />
        <character is_modifier="false" name="fragility" src="d0_s15" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o17640" name="gland" name_original="glands" src="d0_s15" type="structure" />
      <biological_entity id="o17641" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character char_type="range_value" from="inconspicuous" name="prominence" src="d0_s15" to="conspicuous" />
      </biological_entity>
      <biological_entity id="o17642" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o17639" id="r2963" name="without" negation="false" src="d0_s15" to="o17640" />
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.7-2.4 mm, hyaline, bases not projecting beyond the lemmas, apices truncate, often notched;</text>
      <biological_entity id="o17643" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s16" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o17644" name="base" name_original="bases" src="d0_s16" type="structure">
        <character constraint="beyond lemmas" constraintid="o17645" is_modifier="false" modifier="not" name="orientation" src="d0_s16" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o17645" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o17646" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s16" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 0.6-1.3 mm, yellowish to purplish.</text>
      <biological_entity id="o17647" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s17" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.6-0.8 mm, rectangular-prismatic to subellipsoid, laterally compressed, with a well-developed adaxial groove, faintly striate, opaque, reddish-brown.</text>
      <biological_entity constraint="adaxial" id="o17649" name="groove" name_original="groove" src="d0_s18" type="structure">
        <character is_modifier="true" name="development" src="d0_s18" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o17648" id="r2964" name="with" negation="false" src="d0_s18" to="o17649" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o17648" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="rectangular-prismatic" name="shape" src="d0_s18" to="subellipsoid" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="faintly" name="coloration_or_pubescence_or_relief" notes="" src="d0_s18" value="striate" value_original="striate" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17650" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis palmeri grows on rocky slopes and hills between 300-2150 m, generally in association with Pinus edulis, Juniperus monosperma, Bouteloua gracilis, and Prosopis. Its range extends from the southwestern United States into Mexico. It resembles E. erosa, but differs in its shorter lemmas and caryopses.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>