<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Soderstr." date="unknown" rank="subfamily">CENTOTHECOIDEAE</taxon_name>
    <taxon_name authority="Ridl." date="unknown" rank="tribe">CENTOTHECEAE</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="genus">CHASMANTHIUM</taxon_name>
    <taxon_name authority="(Michx.) H.O. Yates" date="unknown" rank="species">latifolium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily centothecoideae;tribe centotheceae;genus chasmanthium;species latifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Uniola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">latifolia</taxon_name>
    <taxon_hierarchy>genus uniola;species latifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 150 cm, 2-4 mm thick at the nodes, rarely branched, leafy for 80% of their height.</text>
      <biological_entity id="o17822" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" constraint="at nodes" constraintid="o17823" from="2" from_unit="mm" name="thickness" src="d0_s0" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="architecture" notes="" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="80%" name="height" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o17823" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o17824" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>collars glabrous;</text>
      <biological_entity id="o17825" name="collar" name_original="collars" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.7-1 mm, entire;</text>
      <biological_entity id="o17826" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (7) 9-18 (22) cm long, (4) 10-22 mm wide, lanceolate-fusiform, usually glabrous, sometimes pilose adaxially.</text>
      <biological_entity id="o17827" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="18" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate-fusiform" value_original="lanceolate-fusiform" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes; adaxially" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles (8) 10-25 (35) cm, open, lax;</text>
      <biological_entity id="o17828" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches nodding or drooping;</text>
      <biological_entity id="o17829" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="drooping" value_original="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>axils of panicle branches sparsely pilose;</text>
      <biological_entity id="o17830" name="axil" name_original="axils" src="d0_s7" type="structure" constraint="branch" constraint_original="branch; branch">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="panicle" id="o17831" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <relation from="o17830" id="r2998" name="part_of" negation="false" src="d0_s7" to="o17831" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 10-30 mm.</text>
      <biological_entity id="o17832" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 15-40 (50) mm long, 6-16 (20) mm wide, with 6-17 (26) florets, lower 1-3 florets sterile, fertile florets diverging to 45°.</text>
      <biological_entity id="o17833" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="length" src="d0_s9" unit="mm" value="50" value_original="50" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="40" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="20" value_original="20" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17834" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="26" value_original="26" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s9" to="17" />
      </biological_entity>
      <biological_entity constraint="lower" id="o17835" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o17836" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o17837" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="45°" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
      </biological_entity>
      <relation from="o17833" id="r2999" name="with" negation="false" src="d0_s9" to="o17834" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 4.2-9.1 mm, 5-7-veined;</text>
      <biological_entity constraint="lower" id="o17838" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s10" to="9.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 4.7-8.7 mm, 5-9-veined;</text>
      <biological_entity constraint="upper" id="o17839" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s11" to="8.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses pilose;</text>
      <biological_entity id="o17840" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>fertile lemmas 9-12.5 mm, straight, 11-15-veined, keels winged, wings scabrous to pilose their full length;</text>
      <biological_entity id="o17841" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="12.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="11-15-veined" value_original="11-15-veined" />
      </biological_entity>
      <biological_entity id="o17842" name="keel" name_original="keels" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o17843" name="wing" name_original="wings" src="d0_s13" type="structure">
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s13" to="pilose" />
        <character is_modifier="false" name="length" src="d0_s13" value="full" value_original="full" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 4.6-7.7 mm;</text>
      <biological_entity id="o17844" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" src="d0_s14" to="7.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (0.4) 0.6-2.6 (3.5) mm, the length varying within a spikelet.</text>
      <biological_entity id="o17845" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="2.6" to_unit="mm" />
        <character constraint="within spikelet" constraintid="o17846" is_modifier="false" name="length" src="d0_s15" value="varying" value_original="varying" />
      </biological_entity>
      <biological_entity id="o17846" name="spikelet" name_original="spikelet" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 2.9-5 mm, enclosed, rarely exposed at maturity.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 48.</text>
      <biological_entity id="o17847" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="enclosed" value_original="enclosed" />
        <character constraint="at maturity" is_modifier="false" modifier="rarely" name="prominence" src="d0_s16" value="exposed" value_original="exposed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17848" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chasmanthium latifolium grows along stream and river banks and in rich deciduous woods. It is the most widespread species of the genus, extending further west and east than any of the other four species. The map shows its verifiable range. Yates (1966b) reported seeing one specimen each from New Jersey, New Mexico, and Manitoba, but none of the specimens had clear locality information. In the absence of any other specimens from these regions, the locality data on these three specimens are regarded as probably erroneous.</discussion>
  <discussion>Flowering in Chasmanthium latifolium is sometimes cleistogamous.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;Wis.;W.Va.;Fla.;N.J.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;D.C.;N.Mex.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Md.;Kans.;Okla.;Ohio;Mo.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>