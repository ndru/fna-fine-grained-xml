<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Linda Bea Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">SPARTINA</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus spartina;species pectinata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spartina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">suttiei</taxon_name>
    <taxon_hierarchy>genus spartina;species pectinata;variety suttiei</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Prairie cordgrass</other_name>
  <other_name type="common_name">Spartine pectinee</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants strongly rhizomatous;</text>
      <biological_entity id="o11515" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes elongate, (2) 3-8 mm thick, purplish-brown or light-brown (drying white), scales closely imbricate.</text>
      <biological_entity id="o11516" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character name="thickness" src="d0_s1" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s1" to="8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purplish-brown" value_original="purplish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity id="o11517" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 250 cm tall, 2.5-11 mm thick, solitary or in small clumps, indurate.</text>
      <biological_entity id="o11518" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="height" src="d0_s2" to="250" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="thickness" src="d0_s2" to="11" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="in small clumps" value_original="in small clumps" />
        <character is_modifier="false" name="texture" notes="" src="d0_s2" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o11519" name="clump" name_original="clumps" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <relation from="o11518" id="r1893" name="in" negation="false" src="d0_s2" to="o11519" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, throats often pilose;</text>
      <biological_entity id="o11520" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11521" name="throat" name_original="throats" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-3 mm;</text>
      <biological_entity id="o11522" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 20-96 cm long, 5-15 mm wide, flat when fresh, becoming involute when dry, glabrous on both surfaces, margins strongly scabrous, blade of the second leaf below the panicles 32-96 cm long, 5-14 mm wide, usually involute.</text>
      <biological_entity id="o11523" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="96" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="when fresh" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when dry" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character constraint="on surfaces" constraintid="o11524" is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11524" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o11525" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o11526" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s5" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o11527" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o11528" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="32" from_unit="cm" name="length" src="d0_s5" to="96" to_unit="cm" />
      </biological_entity>
      <relation from="o11526" id="r1894" name="part_of" negation="false" src="d0_s5" to="o11527" />
      <relation from="o11526" id="r1895" name="below" negation="false" src="d0_s5" to="o11528" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-50 cm, not smooth in outline, with 5-50 branches;</text>
      <biological_entity id="o11529" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="50" to_unit="cm" />
        <character constraint="in outline" constraintid="o11530" is_modifier="false" modifier="not" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o11530" name="outline" name_original="outline" src="d0_s6" type="structure" />
      <biological_entity id="o11531" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="50" />
      </biological_entity>
      <relation from="o11529" id="r1896" name="with" negation="false" src="d0_s6" to="o11531" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1.5-15 cm, appressed to somewhat spreading, with 10-80 spikelets.</text>
      <biological_entity id="o11532" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed to somewhat" value_original="appressed to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o11533" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s7" to="80" />
      </biological_entity>
      <relation from="o11532" id="r1897" name="with" negation="false" src="d0_s7" to="o11533" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 10-25 mm.</text>
      <biological_entity id="o11534" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes shortly awned, glabrous or sparsely hispidulous;</text>
      <biological_entity id="o11535" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 5-10 mm, from 3/4 as long as to equaling the adjacent lemmas, keels hispid, apices awned;</text>
      <biological_entity constraint="lower" id="o11536" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11537" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="3/4" value_original="3/4" />
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o11538" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o11539" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <relation from="o11536" id="r1898" name="from" negation="false" src="d0_s10" to="o11537" />
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 10-25 mm (including the awn), exceeding the florets, glabrous or sparsely hispid, keels scabrous to hispid, trichomes about 0.3 mm, lateral-veins usually glabrous (rarely hispid), on either side of, and close to, the keels, apices awned, awns 3-8 mm;</text>
      <biological_entity constraint="upper" id="o11540" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o11541" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o11542" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s11" to="hispid" />
      </biological_entity>
      <biological_entity id="o11543" name="trichome" name_original="trichomes" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o11544" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11545" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity id="o11546" name="keel" name_original="keels" src="d0_s11" type="structure" />
      <biological_entity id="o11547" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11548" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o11540" id="r1899" name="exceeding the" negation="false" src="d0_s11" to="o11541" />
      <relation from="o11544" id="r1900" name="on" negation="false" src="d0_s11" to="o11545" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas glabrous, keels pectinate distally, apices bilobed, lobes 0.2-0.9 mm;</text>
      <biological_entity id="o11549" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11550" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s12" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <biological_entity id="o11551" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o11552" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 4-6 mm, well-filled, dehiscent.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 40, 40+1, 80.</text>
      <biological_entity id="o11553" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11554" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="40" value_original="40" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s14" upper_restricted="false" />
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
        <character name="quantity" src="d0_s14" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spartina pectinata is native to Canada and the United States, but it has been introduced at scattered locations on other continents. On the Atlantic coast, it grows in marshes, sloughs, and flood plains, being a common constituent of ice-scoured zones of the northeast and growing equally well in salt and fresh water habitats. In western North America, it grows in both wet and dry soils, including dry prairie habitats and along roads and railroads.</discussion>
  <discussion>Spartina pectinata is thought to be one of the parents of S. xcaespitosa, the other parent being S. patens.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C.;Wis.;W.Va.;Colo.;Alta.;Man.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;N.H.;N.Mex.;Tex.;La.;N.C.;Idaho;Tenn.;Pa.;Wyo.;R.I.;Va.;Mass.;Maine;Vt.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ind.;Iowa;B.C.;Minn.;Ohio;Oreg.;Md.;Utah;Mo.;Mich.;Mont.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>