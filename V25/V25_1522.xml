<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Cirillo" date="unknown" rank="genus">IMPERATA</taxon_name>
    <taxon_name authority="(L.) Raeusch." date="unknown" rank="species">cylindrica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus imperata;species cylindrica</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Cogongrass</other_name>
  <other_name type="common_name">Bladygrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (10) 30-95 (217) cm.</text>
      <biological_entity id="o15226" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="95" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ligules 0.2-3.5 mm;</text>
      <biological_entity id="o15227" name="ligule" name_original="ligules" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s1" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades to 150 cm long, (1) 3-11 (28) mm wide, linear-lanceolate, bases narrowed to the broad midrib, often with hairs on the margins.</text>
      <biological_entity id="o15228" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="150" to_unit="cm" />
        <character name="width" src="d0_s2" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o15229" name="base" name_original="bases" src="d0_s2" type="structure">
        <character constraint="to midrib" constraintid="o15230" is_modifier="false" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o15230" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o15231" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <biological_entity id="o15232" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <relation from="o15229" id="r2553" modifier="often" name="with" negation="false" src="d0_s2" to="o15231" />
      <relation from="o15231" id="r2554" name="on" negation="false" src="d0_s2" to="o15232" />
    </statement>
    <statement id="d0_s3">
      <text>Panicles 5.7-22.3 (52) cm, narrowly cylindrical;</text>
      <biological_entity id="o15233" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="52" value_original="52" />
        <character char_type="range_value" from="5.7" from_unit="cm" name="some_measurement" src="d0_s3" to="22.3" to_unit="cm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower branches 1-3.2 (7) cm, appressed.</text>
      <biological_entity constraint="lower" id="o15234" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3.2" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Callus hairs 9-16 mm;</text>
      <biological_entity constraint="callus" id="o15235" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>glumes 2.6-5.5 mm;</text>
      <biological_entity id="o15236" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower lemmas 1.4-4.5 mm;</text>
      <biological_entity constraint="lower" id="o15237" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>upper lemmas (0.7) 1.3-2.3 (3.4) mm;</text>
      <biological_entity constraint="upper" id="o15238" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="0.7" value_original="0.7" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 2, filaments not dilated at the base;</text>
      <biological_entity id="o15239" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15240" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character constraint="at base" constraintid="o15241" is_modifier="false" modifier="not" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o15241" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers (1.5) 2.2-4.2 mm, orange to brown;</text>
      <biological_entity id="o15242" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s10" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="orange" name="coloration" src="d0_s10" to="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 0.5-3.4 mm;</text>
      <biological_entity id="o15243" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 2.8-5.2 (8.3) mm, purple to brown.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 20, 40, 60.</text>
      <biological_entity id="o15244" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="8.3" value_original="8.3" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s12" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s12" to="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15245" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
        <character name="quantity" src="d0_s13" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Imperata cylindrica is the most variable species in the genus. Several varieties have been recognized but, although there are statistically significant differences between plants from different regions, identification to variety without knowledge of a plant's geographic origin is risky. All North American plants examined have had 2n = 20.</discussion>
  <discussion>Imperata cylindrica is one of the world's 10 worst weeds, and is listed as a noxious weed by the U.S. Department of Agriculture. It was introduced to Alabama by 1912, and has spread considerably through the southeastern United States since then. The cultivar 'Red Baron' is diminutive and non-weedy, but individual shoots may revert to the aggressive form. Such reversion is particularly common in plants grown from tissue culture.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Tex.;La.;Ala.;Ga.;Miss.;S.C.;Fla.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>