<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="tribe">PAPPOPHOREAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">PAPPOPHORUM</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">vaginatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe pappophoreae;genus pappophorum;species vaginatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pappophorum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mucronulatum</taxon_name>
    <taxon_hierarchy>genus pappophorum;species mucronulatum</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Whiplash pappusgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (40) 50-100 cm.</text>
      <biological_entity id="o22411" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths mostly glabrous, with a tuft of hairs at the throat;</text>
      <biological_entity id="o22412" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22413" name="tuft" name_original="tuft" src="d0_s1" type="structure" />
      <biological_entity id="o22414" name="hair" name_original="hairs" src="d0_s1" type="structure" />
      <biological_entity id="o22415" name="throat" name_original="throat" src="d0_s1" type="structure" />
      <relation from="o22412" id="r3811" name="with" negation="false" src="d0_s1" to="o22413" />
      <relation from="o22413" id="r3812" name="part_of" negation="false" src="d0_s1" to="o22414" />
      <relation from="o22413" id="r3813" name="at" negation="false" src="d0_s1" to="o22415" />
    </statement>
    <statement id="d0_s2">
      <text>blades 10-25 cm long, 2-5 mm wide, flat to involute, adaxial surfaces scabridulous.</text>
      <biological_entity id="o22416" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="involute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22417" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicles 10-25 cm, tightly contracted, usually white or tawny, rarely slightly purple-tinged.</text>
      <biological_entity id="o22418" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="tightly" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="tawny" value_original="tawny" />
        <character is_modifier="false" modifier="rarely slightly" name="coloration" src="d0_s3" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets with 1 (2) bisexual florets and 2 reduced florets.</text>
      <biological_entity id="o22419" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity id="o22421" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="true" name="reproduction" src="d0_s4" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o22419" id="r3814" name="with" negation="false" src="d0_s4" to="o22421" />
    </statement>
    <statement id="d0_s5">
      <text>Glumes (3) 4-4.5 mm, glabrous, acute;</text>
      <biological_entity id="o22422" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lowest lemma bodies 3-3.2 mm, midveins and margins pubescent from the base to about midlength, awns about twice as long as the lemma bodies, tending to spread at right angles when mature;</text>
      <biological_entity constraint="lemma" id="o22423" name="body" name_original="bodies" src="d0_s6" type="structure" constraint_original="lowest lemma">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22424" name="midvein" name_original="midveins" src="d0_s6" type="structure">
        <character constraint="from base" constraintid="o22426" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22425" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="from base" constraintid="o22426" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22426" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o22427" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character constraint="body" constraintid="o22428" is_modifier="false" name="length" src="d0_s6" value="2 times as long as the lemma bodies" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o22428" name="body" name_original="bodies" src="d0_s6" type="structure" />
      <biological_entity id="o22429" name="angle" name_original="angles" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spread" value_original="spread" />
      </biological_entity>
      <relation from="o22427" id="r3815" name="tending to" negation="false" src="d0_s6" to="o22429" />
    </statement>
    <statement id="d0_s7">
      <text>paleas longer than the lemma bodies.</text>
      <biological_entity constraint="lemma" id="o22431" name="body" name_original="bodies" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 60.</text>
      <biological_entity id="o22430" name="palea" name_original="paleas" src="d0_s7" type="structure">
        <character constraint="than the lemma bodies" constraintid="o22431" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22432" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pappophorum vaginatum grows in similar habitats to P. bicolor, the two species sometimes growing together. Its range extends from southern Arizona to Texas and northern Mexico and from Uruguay to Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;N.Y.;Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>