<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">666</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Spreng." date="unknown" rank="genus">CYMBOPOGON</taxon_name>
    <taxon_name authority="(DC.) Stapf" date="unknown" rank="species">citratus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus cymbopogon;species citratus</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Lemon grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o13350" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 200 cm, flexuous;</text>
      <biological_entity id="o13351" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="200" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes not swollen.</text>
      <biological_entity id="o13352" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal sheaths closely overlapping, gaping at maturity, forming somewhat flattened fans, glabrous, strongly glaucous;</text>
      <biological_entity constraint="basal" id="o13353" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s3" value="gaping" value_original="gaping" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o13354" name="fan" name_original="fans" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o13353" id="r2194" name="forming" negation="false" src="d0_s3" to="o13354" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-2 mm, truncate;</text>
      <biological_entity id="o13355" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 90 cm long, 6.5-15 mm wide.</text>
      <biological_entity id="o13356" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="90" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences to 60 cm, nodding;</text>
    </statement>
    <statement id="d0_s7">
      <text>rames 10-25 mm;</text>
      <biological_entity id="o13357" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>internodes and pedicels pilose on the margins and dorsal surface.</text>
      <biological_entity id="o13358" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character constraint="on dorsal surface" constraintid="o13361" is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o13359" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="on dorsal surface" constraintid="o13361" is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o13360" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity constraint="dorsal" id="o13361" name="surface" name_original="surface" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Sessile spikelets of heterogamous pairs 5-6 mm;</text>
      <biological_entity id="o13362" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13363" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <relation from="o13362" id="r2195" name="part_of" negation="false" src="d0_s9" to="o13363" />
    </statement>
    <statement id="d0_s10">
      <text>lower glumes shallowly concave below, flat distally, keels narrowly winged;</text>
      <biological_entity constraint="lower" id="o13364" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="shallowly; below" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="distally" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o13365" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper lemmas entire or bidentate, unawned or with a 1-2 mm awn.</text>
      <biological_entity constraint="upper" id="o13366" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o13367" name="awn" name_original="awn" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o13366" id="r2196" name="with" negation="false" src="d0_s11" to="o13367" />
    </statement>
    <statement id="d0_s12">
      <text>Pedicellate spikelets 4-4.5 mm, unawned.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 40, 60.</text>
      <biological_entity id="o13368" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13369" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
        <character name="quantity" src="d0_s13" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cymbopogon citratus is now known only in cultivation, even in Asia. Young shoots are used as a spice, and the oils are extracted for lemon oil. It has been grown in Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>