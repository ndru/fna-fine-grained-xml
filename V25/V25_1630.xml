<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">680</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Pers." date="unknown" rank="genus">HETEROPOGON</taxon_name>
    <taxon_name authority="(L.) P. Beauv. ex Roem. &amp; Schult." date="unknown" rank="species">contortus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus heteropogon;species contortus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">contortus</taxon_name>
    <taxon_hierarchy>genus andropogon;species contortus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Tanglehead</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o17332" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-150 cm, erect.</text>
      <biological_entity id="o17333" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth, reddish;</text>
      <biological_entity id="o17334" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.5-0.8 mm, cilia 0.2-0.5 mm;</text>
      <biological_entity id="o17335" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17336" name="cilium" name_original="cilia" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 10-15 cm long, 2-7 mm wide, flat or folded, glabrous or pubescent.</text>
    </statement>
    <statement id="d0_s5">
      <text>Rames 3-7 cm, secund, with 12-22, brown to reddish-brown, sessile-pedicellate spikelet pairs.</text>
      <biological_entity id="o17337" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="folded" value_original="folded" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="secund" value_original="secund" />
        <character char_type="range_value" from="brown" modifier="with 12-22" name="coloration" src="d0_s5" to="reddish-brown" />
      </biological_entity>
      <biological_entity id="o17338" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Homogamous spikelets 6-10 mm.</text>
      <biological_entity id="o17339" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="homogamous" value_original="homogamous" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heterogamous spikelets: sessile spikelets 5-10 mm, brown, awned;</text>
      <biological_entity id="o17340" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o17341" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calluses 1.8-2 mm, strigose;</text>
      <biological_entity id="o17342" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o17343" name="callus" name_original="calluses" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>awns 6-10 cm;</text>
      <biological_entity id="o17344" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o17345" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicellate spikelets 6-10 mm, unawned;</text>
      <biological_entity id="o17346" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o17347" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glumes ovatelanceolate, glabrous or with papillose-based hairs distally, without glandular pits, greenish to purplish-brown, becoming stramineous when dry.</text>
      <biological_entity id="o17348" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o17350" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o17351" name="pit" name_original="pits" src="d0_s11" type="structure" />
      <relation from="o17349" id="r2914" name="with" negation="false" src="d0_s11" to="o17350" />
      <relation from="o17349" id="r2915" name="without" negation="false" src="d0_s11" to="o17351" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 40, 50, 60.</text>
      <biological_entity id="o17349" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="with papillose-based hairs" value_original="with papillose-based hairs" />
        <character char_type="range_value" from="greenish" name="coloration" notes="" src="d0_s11" to="purplish-brown" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s11" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17352" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="50" value_original="50" />
        <character name="quantity" src="d0_s12" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Heteropogon contortus grows on rocky hills and canyons in the southern United States into Mexico, and worldwide in subtropical and tropical areas, occupying a variety of different habitats, including disturbed habitats. It is probably native to the eastern hemisphere but is now found in tropical and subtropical areas throughout the world.</discussion>
  <discussion>Heteropogon contortus is a valuable forage grass if continuously grazed so as to prevent the calluses from developing. It is also considered a weed, being able to establish itself in newly disturbed and poor soils.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.;Calif.;Puerto Rico;Virgin Islands;Pacific Islands (Hawaii);Ariz.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>