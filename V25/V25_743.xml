<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">173</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Buckley" date="unknown" rank="species">arenicola</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species arenicola</taxon_hierarchy>
  </taxon_identification>
  <number>31</number>
  <other_name type="common_name">Sand muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o10665" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (15) 20-60 (70) cm, somewhat decumbent, 1 or more nodes exposed;</text>
      <biological_entity id="o10666" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character name="quantity" src="d0_s2" unit="or morenodes" value="1" value_original="1" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes hispidulous below the nodes.</text>
      <biological_entity id="o10667" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character constraint="below nodes" constraintid="o10668" is_modifier="false" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o10668" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves somewhat basally concentrated, most blades not reaching more than (1/4) 1/2 of the plant height;</text>
      <biological_entity id="o10669" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat basally" name="arrangement_or_density" src="d0_s4" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity id="o10670" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="position_relational" src="d0_s4" value="reaching" value_original="reaching" />
        <character name="quantity" src="d0_s4" value="[1/4]" value_original="[1/4]" />
        <character constraint="of plant" constraintid="o10671" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o10671" name="plant" name_original="plant" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>sheaths usually a little shorter than the internodes, not keeled, scabridulous, margins hyaline, basal sheaths rounded, not becoming spirally coiled when old;</text>
      <biological_entity id="o10672" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o10673" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o10673" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity id="o10674" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10675" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s5" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 2-9 mm, hyaline, acute, lacerate, often with lateral lobes;</text>
      <biological_entity id="o10676" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10677" name="lobe" name_original="lobes" src="d0_s6" type="structure" />
      <relation from="o10676" id="r1761" modifier="often" name="with" negation="false" src="d0_s6" to="o10677" />
    </statement>
    <statement id="d0_s7">
      <text>blades 4-10 (16) cm long, 1-2.2 mm wide, not arcuate, flat, folded, or involute, scabrous, often glaucous, midveins and margins not thickened, green.</text>
      <biological_entity id="o10678" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o10679" name="midvein" name_original="midveins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o10680" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s7" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 12-30 cm long, 5-20 cm wide, diffuse;</text>
      <biological_entity id="o10681" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s8" to="20" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s8" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches 1-10 cm, diverging 30-80° from the rachises, naked basally;</text>
      <biological_entity constraint="primary" id="o10682" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character constraint="from rachises" constraintid="o10683" is_modifier="false" modifier="30-80°" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="basally" name="architecture" notes="" src="d0_s9" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o10683" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 1-4 (6) mm.</text>
      <biological_entity id="o10684" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.5-4.2 mm.</text>
      <biological_entity id="o10685" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes equal, 1.4-2.5 mm, 1-veined, apices scabridulous, acute to acuminate, minutely erose, unawned or awned, awns to 1 mm;</text>
      <biological_entity id="o10686" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o10687" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o10688" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.5-4.2 mm, narrowly elliptic, usually purplish, scabrous distally, appressed-pubescent on the lower 1/2 - 3/4 of the margins and midveins, apices acuminate, awned, awns 0.5-4.2 mm;</text>
      <biological_entity id="o10689" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.2" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character constraint="on lower 1/2-3/4" constraintid="o10690" is_modifier="false" name="pubescence" src="d0_s13" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10690" name="1/2-3/4" name_original="1/2-3/4" src="d0_s13" type="structure" />
      <biological_entity id="o10691" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o10692" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o10693" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o10694" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.2" to_unit="mm" />
      </biological_entity>
      <relation from="o10690" id="r1762" name="part_of" negation="false" src="d0_s13" to="o10691" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 2.5-3.5 mm, narrowly elliptic, intercostal region sparsely pubescent, apices acuminate, with 2 short (0.1-0.2 mm) awns;</text>
      <biological_entity id="o10695" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o10696" name="region" name_original="region" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10697" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o10698" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <relation from="o10697" id="r1763" name="with" negation="false" src="d0_s14" to="o10698" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-2.1 mm, greenish.</text>
      <biological_entity id="o10699" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 1.9-2.3 mm, fusiform, brownish.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 80, 82.</text>
      <biological_entity id="o10700" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s16" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10701" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="80" value_original="80" />
        <character name="quantity" src="d0_s17" value="82" value_original="82" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia arenicola grows on sandy mesas, limestone benches, and in valleys and open desert grasslands, at elevations of 600-2135 m. Its range extends from the southwestern United States to central Mexico. It also grows, as a disjunct, in northwestern Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans.;Okla.;Ariz.;Colo.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>