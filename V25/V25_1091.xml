<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ANTHENANTIA</taxon_name>
    <taxon_name authority="(Elliott) Schult." date="unknown" rank="species">rufa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus anthenantia;species rufa</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Purple silkyscale</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 60-120 cm, from knotty rhizomes.</text>
      <biological_entity id="o29731" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29732" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
      </biological_entity>
      <relation from="o29731" id="r5071" name="from" negation="false" src="d0_s0" to="o29732" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 30-60 cm, junction of the sheath and blade inconspicuous abaxially;</text>
      <biological_entity id="o29733" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29734" name="junction" name_original="junction" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s1" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o29735" name="sheath" name_original="sheath" src="d0_s1" type="structure" />
      <biological_entity id="o29736" name="blade" name_original="blade" src="d0_s1" type="structure" />
      <relation from="o29734" id="r5072" name="part_of" negation="false" src="d0_s1" to="o29735" />
      <relation from="o29734" id="r5073" name="part_of" negation="false" src="d0_s1" to="o29736" />
    </statement>
    <statement id="d0_s2">
      <text>blades 2-5 mm wide, in line with the sheaths, margins scabrous, otherwise glabrous or the adaxial surfaces inconspicuously pubescent to hirsute.</text>
      <biological_entity id="o29737" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29738" name="line" name_original="line" src="d0_s2" type="structure" />
      <biological_entity id="o29739" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o29740" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29741" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="inconspicuously pubescent" name="pubescence" src="d0_s2" to="hirsute" />
      </biological_entity>
      <relation from="o29737" id="r5074" name="in" negation="false" src="d0_s2" to="o29738" />
      <relation from="o29738" id="r5075" name="with" negation="false" src="d0_s2" to="o29739" />
    </statement>
    <statement id="d0_s3">
      <text>Panicles 8-16 cm long, 2-3 (5) cm wide;</text>
      <biological_entity id="o29742" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s3" to="16" to_unit="cm" />
        <character name="width" src="d0_s3" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches spreading to erect.</text>
      <biological_entity id="o29743" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 3-4 mm long, 1.3-1.8 mm wide.</text>
      <biological_entity id="o29744" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s5" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Upper glumes and lower lemmas pubescent, hairs 0.6-1.5 mm, eventually spreading, usually purple-tinged;</text>
      <biological_entity constraint="upper" id="o29745" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o29746" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29747" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="eventually" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 1-1.5 mm.</text>
      <biological_entity id="o29748" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Caryopses 1.2-1.4 mm. 2n = 20.</text>
      <biological_entity id="o29749" name="caryopsis" name_original="caryopses" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29750" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Anthenantia rufa grows in wet pine flatwoods and savannahs, sphagnous streamhead ecotones, and pitcher plant bogs on the southeastern coastal plain from eastern Texas to North Carolina.</discussion>
  
</bio:treatment>