<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">122</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="(Biehler) Hitchc." date="unknown" rank="species">clandestinus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species clandestinus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sporobolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">compositus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">clandestinus</taxon_name>
    <taxon_hierarchy>genus sporobolus;species compositus;variety clandestinus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sporobolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">clandestinus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">canovirens</taxon_name>
    <taxon_hierarchy>genus sporobolus;species clandestinus;variety canovirens</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Hidden dropseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, occasionally rhizomatous.</text>
      <biological_entity id="o15776" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-130 (150) cm tall, 1-4 mm thick, frequently glaucous.</text>
      <biological_entity id="o15777" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="cm" value="150" value_original="150" />
        <character char_type="range_value" from="40" from_unit="cm" name="height" src="d0_s2" to="130" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="frequently" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths with sparsely hairy apices, hairs to 3 mm, not conspicuously tufted;</text>
      <biological_entity id="o15778" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o15779" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15780" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not conspicuously" name="arrangement_or_pubescence" src="d0_s3" value="tufted" value_original="tufted" />
      </biological_entity>
      <relation from="o15778" id="r2644" name="with" negation="false" src="d0_s3" to="o15779" />
    </statement>
    <statement id="d0_s4">
      <text>uppermost sheaths 0.5-3 mm wide;</text>
      <biological_entity constraint="uppermost" id="o15781" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.1-0.4 mm;</text>
      <biological_entity id="o15782" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 4-23 cm long, 1.5-4 mm wide, flat or involute, abaxial surface glabrous or pilose, adaxial surface glabrous or scabridulous, margins glabrous.</text>
      <biological_entity id="o15783" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="23" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15784" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15785" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o15786" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal and axillary, 5-11 cm long, 0.04-0.2 (0.3) cm wide, with 10-40 spikelets per cm2, narrow, sometimes spikelike, included in the uppermost sheath;</text>
      <biological_entity id="o15787" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="11" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="0.04" from_unit="cm" name="width" src="d0_s7" to="0.2" to_unit="cm" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o15788" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
      <biological_entity id="o15789" name="cm2" name_original="cm2" src="d0_s7" type="structure" />
      <biological_entity constraint="uppermost" id="o15790" name="sheath" name_original="sheath" src="d0_s7" type="structure" />
      <relation from="o15787" id="r2645" name="with" negation="false" src="d0_s7" to="o15788" />
      <relation from="o15788" id="r2646" name="per" negation="false" src="d0_s7" to="o15789" />
      <relation from="o15787" id="r2647" name="included in the" negation="false" src="d0_s7" to="o15790" />
    </statement>
    <statement id="d0_s8">
      <text>lower nodes with 1-2 (3) branches;</text>
      <biological_entity constraint="lower" id="o15791" name="node" name_original="nodes" src="d0_s8" type="structure" />
      <biological_entity id="o15792" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <relation from="o15791" id="r2648" name="with" negation="false" src="d0_s8" to="o15792" />
    </statement>
    <statement id="d0_s9">
      <text>primary branches 0.4-5 cm, appressed, spikelet-bearing to the base;</text>
      <biological_entity constraint="primary" id="o15793" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s9" to="5" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o15794" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o15793" id="r2649" name="to" negation="false" src="d0_s9" to="o15794" />
    </statement>
    <statement id="d0_s10">
      <text>secondary branches appressed;</text>
      <biological_entity constraint="secondary" id="o15795" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pulvini glabrous;</text>
      <biological_entity id="o15796" name="pulvinus" name_original="pulvini" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicels 0.3-3.5 mm, appressed, glabrous or scabridulous.</text>
      <biological_entity id="o15797" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 4-9 (10) mm, stramineous to purplish-tinged.</text>
      <biological_entity id="o15798" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s13" to="purplish-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Glumes subequal, lanceolate, membranous to chartaceous, midveins usually greenish;</text>
      <biological_entity id="o15799" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s14" to="chartaceous" />
      </biological_entity>
      <biological_entity id="o15800" name="midvein" name_original="midveins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 1.5-6.2 mm;</text>
      <biological_entity constraint="lower" id="o15801" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper glumes (2) 2.5-5 (6.5) mm, slightly shorter or longer than the lemmas;</text>
      <biological_entity constraint="upper" id="o15802" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character constraint="than the lemmas" constraintid="o15803" is_modifier="false" name="length_or_size" src="d0_s16" value="slightly shorter or longer" value_original="slightly shorter or longer" />
      </biological_entity>
      <biological_entity id="o15803" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>lemmas (2.2) 3-7 (7.4) mm, lanceolate, chartaceous and opaque, minutely appressed-pubescent or scabridulous, occasionally 2-veined or 3-veined, acute to obtuse;</text>
      <biological_entity id="o15804" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s17" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="opaque" value_original="opaque" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s17" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="relief" src="d0_s17" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s17" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s17" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>paleas (2.2) 3-9 (10) mm, ovate to lanceolate, chartaceous;</text>
      <biological_entity id="o15805" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="9" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s18" to="lanceolate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s18" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 2.2-3.2 mm, yellow to orangish.</text>
      <biological_entity id="o15806" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s19" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s19" to="orangish" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits (1.5) 2.4-3.5 mm, ellipsoid, laterally flattened, often striate, reddish-brown;</text>
      <biological_entity id="o15807" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character name="atypical_some_measurement" src="d0_s20" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s20" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s20" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s20" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="often" name="coloration_or_pubescence_or_relief" src="d0_s20" value="striate" value_original="striate" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>pericarps loose, but neither gelatinous nor slipping from the seeds when wet.</text>
      <biological_entity id="o15809" name="seed" name_original="seeds" src="d0_s21" type="structure" />
      <relation from="o15808" id="r2650" name="slipping from the" negation="false" src="d0_s21" to="o15809" />
    </statement>
    <statement id="d0_s22">
      <text>2n = unknown.</text>
      <biological_entity id="o15808" name="pericarp" name_original="pericarps" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s21" value="loose" value_original="loose" />
        <character is_modifier="false" name="texture" src="d0_s21" value="gelatinous" value_original="gelatinous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15810" name="chromosome" name_original="" src="d0_s22" type="structure" />
    </statement>
  </description>
  <discussion>Sporobolus clandestinus grows primarily in sandy soils along the coast and, inland, along roadsides. In the southeastern United States, it is found in dry to mesic longleaf pine-oak-grass communities and cedar glades. Its range lies entirely within the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;D.C.;Wis.;W.Va.;Fla.;N.J.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;N.Y.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Conn.;Md.;Kans.;Okla.;Mo.;Miss.;Ky.;Mass.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>