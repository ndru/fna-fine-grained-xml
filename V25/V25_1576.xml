<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">653</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Spreng." date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species gracilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schizachyrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sericatum</taxon_name>
    <taxon_hierarchy>genus schizachyrium;species sericatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schizachyrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracile</taxon_name>
    <taxon_hierarchy>genus schizachyrium;species gracile</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Wire bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose.</text>
      <biological_entity id="o1400" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-60 cm, wiry, glabrous.</text>
      <biological_entity id="o1401" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths smooth;</text>
      <biological_entity id="o1402" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules to 1.4 mm;</text>
      <biological_entity id="o1403" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades to 45 cm long, to 4 mm wide, involute and filiform, or folded.</text>
      <biological_entity id="o1404" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s4" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence units 3-50+ per culm;</text>
      <biological_entity constraint="inflorescence" id="o1405" name="unit" name_original="units" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o1406" from="3" name="quantity" src="d0_s5" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1406" name="culm" name_original="culm" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>peduncles 2-13.2 cm, with 1 rame;</text>
      <biological_entity id="o1408" name="rame" name_original="rame" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o1407" id="r228" name="with" negation="false" src="d0_s6" to="o1408" />
    </statement>
    <statement id="d0_s7">
      <text>rames 2-4 cm, usually long-exserted at maturity;</text>
      <biological_entity id="o1407" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="13.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="position" src="d0_s7" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>internodes densely pubescent, hairs to 8 mm.</text>
      <biological_entity id="o1409" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o1410" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sessile spikelets 4-6 mm;</text>
      <biological_entity id="o1411" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes scabrous in the distal 1/2;</text>
      <biological_entity constraint="lower" id="o1412" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="in distal 1/2" constraintid="o1413" is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1413" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>awns 11-20 mm.</text>
      <biological_entity id="o1414" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s11" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicellate spikelets reduced to an awned or unawned glume, sterile.</text>
      <biological_entity id="o1416" name="glume" name_original="glume" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>2n = 40.</text>
      <biological_entity id="o1415" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character constraint="to glume" constraintid="o1416" is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1417" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon gracilis grows on oolite in openings and rocky margins of pine woodlands of southern Florida and the West Indies. Although not uncommon, it is frequently overlooked. It has sometimes been placed in Schizachyrium because of its solitary rames.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>