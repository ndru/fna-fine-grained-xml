<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Dichanthelium</taxon_name>
    <taxon_name authority="(L.) Gould" date="unknown" rank="species">dichotomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">microcarpon</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section dichanthelium;species dichotomum;subspecies microcarpon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dichotomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">ramulosum</taxon_name>
    <taxon_hierarchy>genus panicum;species dichotomum;variety ramulosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="Muhl. ex Elliott" date="unknown" rank="species">microcarpon</taxon_name>
    <taxon_hierarchy>genus panicum;species microcarpon</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 30-100 cm, slender, erect or geniculate;</text>
    </statement>
    <statement id="d0_s1">
      <text>fall phase freely branching from all nodes, reclining from masses of branchlets and numerous reduced, ciliate blades and secondary panicles;</text>
      <biological_entity id="o11368" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
        <character constraint="from nodes" constraintid="o11369" is_modifier="false" modifier="freely" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o11369" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character constraint="from masses" constraintid="o11370" is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
      </biological_entity>
      <biological_entity id="o11370" name="mass" name_original="masses" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o11371" name="branchlet" name_original="branchlets" src="d0_s1" type="structure" />
      <biological_entity id="o11372" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o11373" name="panicle" name_original="panicles" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o11370" id="r1862" name="part_of" negation="false" src="d0_s1" to="o11371" />
    </statement>
    <statement id="d0_s2">
      <text>nodes conspicuously bearded with retrorse hairs.</text>
      <biological_entity id="o11374" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character constraint="with hairs" constraintid="o11375" is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s2" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o11375" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually glabrous, lowermost sheaths sometimes sparsely pubescent, occasionally with whitish spots between the veins, ciliate along the margins;</text>
      <biological_entity id="o11376" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lowermost" id="o11377" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character constraint="between veins" constraintid="o11378" is_modifier="false" modifier="occasionally" name="coloration" src="d0_s3" value="whitish spots" value_original="whitish spots" />
        <character constraint="along margins" constraintid="o11379" is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11378" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o11379" name="margin" name_original="margins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blades 5-14 cm long, 5-14 mm wide, thin, spreading to reflexed, glabrous on both surfaces, bases with few-to-many papillose-based cilia.</text>
      <biological_entity id="o11380" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="reflexed" />
        <character constraint="on surfaces" constraintid="o11381" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11381" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o11382" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o11383" name="cilium" name_original="cilia" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few-to-many" value_original="few-to-many" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o11382" id="r1863" name="with" negation="false" src="d0_s4" to="o11383" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 5-12 cm, well-exserted, dense.</text>
      <biological_entity id="o11384" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s5" value="well-exserted" value_original="well-exserted" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 1.5-1.8 mm, usually glabrous, rarely slightly pubescent.</text>
      <biological_entity id="o11385" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely slightly" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Lower glumes usually less than 14 as long as the spikelets;</text>
      <biological_entity constraint="lower" id="o11386" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o11387" from="0" name="quantity" src="d0_s7" to="14" />
      </biological_entity>
      <biological_entity id="o11387" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>upper glumes usually shorter than the lower lemmas;</text>
      <biological_entity constraint="upper" id="o11388" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character constraint="than the lower lemmas" constraintid="o11389" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity constraint="lower" id="o11389" name="lemma" name_original="lemmas" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>upper florets 1.3-1.6 mm long, 0.6-0.8 mm wide, subacute.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="upper" id="o11390" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s9" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11391" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium dichotomum subsp. microcarpon grows in wet woods, swamps, and wetland borders. It is a widespread subspecies, extending from southern Michigan to Massachusetts and south to eastern Oklahoma and Texas and throughout the southeast to central Florida.</discussion>
  
</bio:treatment>