<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="(P. Beauv.) Kunth" date="unknown" rank="species">junceus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species junceus</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Piney woods dropseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o20979" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (30) 40-100 cm.</text>
      <biological_entity id="o20980" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths rounded below, margins and apices sometimes sparsely ciliate;</text>
      <biological_entity id="o20981" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="below" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20982" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20983" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.2 mm;</text>
      <biological_entity id="o20984" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (6) 10-30 cm long, 0.8-2 mm wide, flat to tightly involute, glabrous abaxially, scabridulous adaxially, margins scabrous, apices pungent.</text>
      <biological_entity id="o20985" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="tightly involute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o20986" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o20987" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="odor_or_shape" src="d0_s5" value="pungent" value_original="pungent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 7-28 cm long, 2-6 cm wide, open, pyramidal;</text>
      <biological_entity id="o20988" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="28" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower nodes with 3 or more branches;</text>
      <biological_entity constraint="lower" id="o20989" name="node" name_original="nodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.7-4.5 cm, spreading 20-100° from the rachis, whorled or verticellate, without spikelets on the lower 1/8–1/2;</text>
      <biological_entity constraint="primary" id="o20990" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="4.5" to_unit="cm" />
        <character constraint="from rachis" constraintid="o20991" is_modifier="false" modifier="20-100°" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s8" value="whorled" value_original="whorled" />
        <character name="arrangement" src="d0_s8" value="verticellate" value_original="verticellate" />
      </biological_entity>
      <biological_entity id="o20991" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o20992" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o20993" name="1/8-1/2" name_original="1/8-1/2" src="d0_s8" type="structure" />
      <relation from="o20990" id="r3577" name="without" negation="false" src="d0_s8" to="o20992" />
      <relation from="o20992" id="r3578" name="on" negation="false" src="d0_s8" to="o20993" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches appressed;</text>
      <biological_entity constraint="secondary" id="o20994" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.4-2.5 mm, appressed, scabridulous.</text>
      <biological_entity id="o20995" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="relief" src="d0_s10" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.6-3.8 mm, purplish-red.</text>
      <biological_entity id="o20996" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s11" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish-red" value_original="purplish-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, linear-lanceolate to lanceolate or ovate, hyaline to membranous;</text>
      <biological_entity id="o20997" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s12" to="lanceolate or ovate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 0.9-3 mm;</text>
      <biological_entity constraint="lower" id="o20998" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 2.6-3.8 mm, as long as or longer than the florets;</text>
      <biological_entity constraint="upper" id="o20999" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s14" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21001" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o21000" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o20999" id="r3579" name="as long as" negation="false" src="d0_s14" to="o21001" />
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2-3.6 mm, ovate, membranous, glabrous, acute;</text>
      <biological_entity id="o21002" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2-3.6 mm, ovate, membranous;</text>
      <biological_entity id="o21003" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.4-2 mm, purplish.</text>
      <biological_entity id="o21004" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits 1.4-1.8 mm, ellipsoid, somewhat laterally flattened, somewhat rugulose, reddish-brown.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o21005" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s18" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="somewhat laterally" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="somewhat" name="relief" src="d0_s18" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21006" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Sporobolus junceus grows in openings in pine and hardwood forests, coastal prairies, and pine barrens, usually in sandy to loamy soils, at 2-400 m. Its range lies entirely within the southern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Okla.;Miss.;Tex.;La.;Ala.;Tenn.;N.C.;S.C.;Ark.;Ga.;Ariz.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>