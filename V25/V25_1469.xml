<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Munro ex Morong &amp; Britton" date="unknown" rank="species">intermedium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species intermedium</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>27</number>
  <other_name type="common_name">Intermediate paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>shortly rhizomatous.</text>
      <biological_entity id="o2332" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 200 cm, erect;</text>
      <biological_entity id="o2333" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="200" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o2334" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o2335" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2-3 mm;</text>
      <biological_entity id="o2336" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 57 cm long, 2-3 cm wide, flat, glabrous below, appressed-pubescent above.</text>
      <biological_entity id="o2337" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="57" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="3" to_unit="cm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="below" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 60-100 racemosely arranged branches;</text>
      <biological_entity id="o2338" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2339" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s7" to="100" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o2338" id="r388" name="with" negation="false" src="d0_s7" to="o2339" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1-13 cm, divergent to spreading, often arcuate;</text>
      <biological_entity id="o2340" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="13" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.9-1.2 mm wide, winged, margins scabrous, long-pubescent.</text>
      <biological_entity constraint="branch" id="o2341" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o2342" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="long-pubescent" value_original="long-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2-2.4 mm long, 0.9-1.2 mm wide, paired, divergent to spreading from the branch axes, elliptic to ovate, glabrous or pubescent, stramineous, sometimes partially purple.</text>
      <biological_entity id="o2343" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character constraint="from branch axes" constraintid="o2344" is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s10" to="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="sometimes partially" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="branch" id="o2344" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o2345" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes smooth, 3-veined, margins entire, sparsely short-pubescent, at least distally;</text>
      <biological_entity constraint="upper" id="o2346" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o2347" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas smooth, lacking ribs over the veins, 3-veined, margins entire, glabrous or shortly pubescent;</text>
      <biological_entity constraint="lower" id="o2348" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o2349" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o2350" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o2351" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o2349" id="r389" name="over" negation="false" src="d0_s13" to="o2350" />
    </statement>
    <statement id="d0_s14">
      <text>upper florets stramineous to white.</text>
      <biological_entity constraint="upper" id="o2352" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s14" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 1.5-1.7 mm, golden brown.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20, 40.</text>
      <biological_entity id="o2353" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="golden brown" value_original="golden brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2354" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum intermedium is an introduced roadside weed in the Flora region. It is found in Mexico and South America, but not in Central America (Pohl and Davidse 1994).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>