<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">495</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">UROCHLOA</taxon_name>
    <taxon_name authority="(Sw.) B.F. Hansen &amp; Wunderlin" date="unknown" rank="species">fusca</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus urochloa;species fusca</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Urochloa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculata</taxon_name>
    <taxon_hierarchy>genus urochloa;species fasciculata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">reticulatum</taxon_name>
    <taxon_hierarchy>genus panicum;species fasciculatum;variety reticulatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculatum</taxon_name>
    <taxon_hierarchy>genus panicum;species fasciculatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachiaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fasciculata</taxon_name>
    <taxon_hierarchy>genus brachiaria;species fasciculata</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Browntop signalgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o24304" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-120 cm, geniculate;</text>
      <biological_entity id="o24305" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or shortly pilose.</text>
      <biological_entity id="o24306" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or hispid, margins ciliate;</text>
      <biological_entity id="o24307" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o24308" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-1.5 mm;</text>
      <biological_entity id="o24309" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-33 cm long, 5-20 mm wide, glabrous or sparsely pilose on both surfaces, margins smooth or scabrous;</text>
      <biological_entity id="o24310" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="33" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o24311" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o24311" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity id="o24312" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>collars pubescent.</text>
      <biological_entity id="o24313" name="collar" name_original="collars" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 5-15 cm long, 2-8 cm wide, simple, with 5-30 spikelike primary branches in more than 2 ranks;</text>
      <biological_entity id="o24314" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s8" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="primary" id="o24315" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o24316" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <relation from="o24314" id="r4132" name="with" negation="false" src="d0_s8" to="o24315" />
      <relation from="o24315" id="r4133" name="in" negation="false" src="d0_s8" to="o24316" />
    </statement>
    <statement id="d0_s9">
      <text>primary branches 2-10 cm, appressed to divergent, axils glabrous, axes 0.3-0.5 mm wide, triquetrous, scabrous or sparsely pilose;</text>
      <biological_entity constraint="primary" id="o24317" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o24318" name="axil" name_original="axils" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24319" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triquetrous" value_original="triquetrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>secondary branches usually present on the lower primary branches, pedicels scabrous and pubescent, shorter than the spikelets.</text>
      <biological_entity constraint="secondary" id="o24320" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character constraint="on lower primary branches" constraintid="o24321" is_modifier="false" modifier="usually" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lower primary" id="o24321" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <biological_entity id="o24322" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character constraint="than the spikelets" constraintid="o24323" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24323" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2-3.4 mm long, 1.2-1.8 mm wide, obovoid, yellowish to reddish-brown or bronze-colored at maturity, mostly paired, in 2-4 rows, appressed to the branches.</text>
      <biological_entity id="o24324" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" constraint="at maturity" from="yellowish" name="coloration" src="d0_s11" to="reddish-brown or bronze-colored" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
        <character constraint="to branches" constraintid="o24326" is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o24325" name="row" name_original="rows" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
      <biological_entity id="o24326" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <relation from="o24324" id="r4134" name="in" negation="false" src="d0_s11" to="o24325" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes scarcely separate, rachilla internodes short, not pronounced;</text>
      <biological_entity id="o24327" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s12" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity constraint="rachilla" id="o24328" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="pronounced" value_original="pronounced" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1-1.5 mm, at least lA as long as the spikelets, glabrous, (1) 3-5-veined;</text>
      <biological_entity constraint="lower" id="o24329" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="(1)3-5-veined" value_original="(1)3-5-veined" />
      </biological_entity>
      <biological_entity id="o24330" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o24329" id="r4135" modifier="at-least" name="as long as" negation="false" src="d0_s13" to="o24330" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes (2) 2.2-3.1 mm, glabrous, 7-9-veined, cross venation evident throughout;</text>
      <biological_entity constraint="upper" id="o24331" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-9-veined" value_original="7-9-veined" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="throughout" name="prominence" src="d0_s14" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower florets usually staminate, sometimes sterile;</text>
      <biological_entity constraint="lower" id="o24332" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 2-3.1 mm, usually glabrous, 7-veined, cross venation evident throughout;</text>
      <biological_entity constraint="lower" id="o24333" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="7-veined" value_original="7-veined" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cross" value_original="cross" />
        <character is_modifier="false" modifier="throughout" name="prominence" src="d0_s16" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower paleas present;</text>
      <biological_entity constraint="lower" id="o24334" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper lemmas 1.8-2.9 mm long, 1.1-1.7 mm wide, apices acute to rounded, mucronate;</text>
      <biological_entity constraint="upper" id="o24335" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s18" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s18" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24336" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s18" to="rounded" />
        <character is_modifier="false" name="shape" src="d0_s18" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 1-1.6 mm.</text>
      <biological_entity id="o24337" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 1-1.7 mm;</text>
      <biological_entity id="o24338" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s20" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>hila punctiform.</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 18, 36.</text>
      <biological_entity id="o24339" name="hilum" name_original="hila" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="punctiform" value_original="punctiform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24340" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="18" value_original="18" />
        <character name="quantity" src="d0_s22" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Urochloa fusca grows from the southern United States to Peru, Paraguay, and Argentina, usually in moist, often disturbed areas at low elevations. It frequently occurs as a weed, but is occasionally grown for forage and grain.</discussion>
  <discussion>Plants having smaller, more compact panicles and larger (2.4-3.4 mm), mostly yellowish spikelets have been referred to as Urochloa fusca var. reticulata (Torr.) B.F. Hansen &amp; Wunderlin. This variety is mainly found in the southwestern United States, but has been intro¬duced into other areas, including Australia. Urochloa fusca (Sw.) B.F. Hansen &amp; Wunderlin var. fusca has generally larger, more open panicles and smaller (2-2.5 mm), reddish-brown or bronze-colored spikelets. Much intergradation is reported between the two varieties. Further investigation is needed to establish that their recognition is warranted.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;La.;Puerto Rico;Virgin Islands;Ala.;Ga.;Ariz.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>