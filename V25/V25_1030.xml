<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Chapm." date="unknown" rank="species">condensata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species condensata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>28</number>
  <other_name type="common_name">Big threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o27951" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bases knotty, bleached, not rhizomatous.</text>
      <biological_entity id="o27952" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 70-150 cm tall, 3-6 mm thick at the base, erect, rarely branched.</text>
      <biological_entity id="o27953" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="height" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" constraint="at base" constraintid="o27954" from="3" from_unit="mm" name="thickness" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o27954" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o27955" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o27956" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths usually longer than the internodes, remaining intact at maturity, glabrous or appressed pilose;</text>
      <biological_entity id="o27957" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o27958" is_modifier="false" name="length_or_size" src="d0_s4" value="usually longer" value_original="usually longer" />
        <character constraint="at maturity" is_modifier="false" name="condition" src="d0_s4" value="intact" value_original="intact" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27958" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>collars glabrous or pilose at the sides;</text>
      <biological_entity id="o27959" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at sides" constraintid="o27960" is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27960" name="side" name_original="sides" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules less than 0.5 mm;</text>
      <biological_entity id="o27961" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades (10) 15-30 cm long, 1.5-3 mm wide, flat at the base, involute toward the apex, straight to somewhat lax at maturity, glabrous abaxially, yellowish-green when fresh, drying stramineous or darker.</text>
      <biological_entity id="o27962" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="at base" constraintid="o27963" is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character constraint="toward apex" constraintid="o27964" is_modifier="false" name="shape_or_vernation" notes="" src="d0_s7" value="involute" value_original="involute" />
        <character is_modifier="false" name="course" notes="" src="d0_s7" value="straight" value_original="straight" />
        <character constraint="at maturity" is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="when fresh" name="coloration" src="d0_s7" value="yellowish-green" value_original="yellowish-green" />
        <character is_modifier="false" name="condition" src="d0_s7" value="drying" value_original="drying" />
        <character is_modifier="false" name="condition" src="d0_s7" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="condition" src="d0_s7" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o27963" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o27964" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences paniculate, (15) 20-55 cm long, 2-4 cm wide;</text>
      <biological_entity id="o27965" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="paniculate" value_original="paniculate" />
        <character name="length" src="d0_s8" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s8" to="55" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nodes glabrous or with short, straight hairs;</text>
      <biological_entity id="o27966" name="node" name_original="nodes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="with short , straight hairs" />
      </biological_entity>
      <biological_entity id="o27967" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o27966" id="r4758" name="with" negation="false" src="d0_s9" to="o27967" />
    </statement>
    <statement id="d0_s10">
      <text>primary branches (4) 5-20 cm, appressed to narrowly ascending, without axillary pulvini, naked below, with 5-15 overlapping spikelets distally.</text>
      <biological_entity constraint="primary" id="o27968" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="20" to_unit="cm" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s10" to="narrowly ascending" />
        <character is_modifier="false" modifier="below" name="architecture" notes="" src="d0_s10" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o27969" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure" />
      <biological_entity id="o27970" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="15" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <relation from="o27968" id="r4759" name="without" negation="false" src="d0_s10" to="o27969" />
      <relation from="o27968" id="r4760" name="with" negation="false" src="d0_s10" to="o27970" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes 6-10 (12) mm, 1-veined, 1-keeled, awns less than 4 mm, brownish;</text>
      <biological_entity id="o27971" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="1-keeled" value_original="1-keeled" />
      </biological_entity>
      <biological_entity id="o27972" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes from 3/4 as long as to 1 mm longer than the upper glumes;</text>
      <biological_entity constraint="lower" id="o27973" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character modifier="from" name="quantity" src="d0_s12" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity constraint="upper" id="o27975" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="upper" id="o27974" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o27974" id="r4761" name="as long as" negation="false" src="d0_s12" to="o27975" />
    </statement>
    <statement id="d0_s13">
      <text>calluses 1-2 mm;</text>
      <biological_entity id="o27976" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 5-8 mm, often reddish-mottled, apices not strongly twisted, junction with the awns not evident;</text>
      <biological_entity id="o27977" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s14" value="reddish-mottled" value_original="reddish-mottled" />
      </biological_entity>
      <biological_entity id="o27978" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not strongly" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o27979" name="junction" name_original="junction" src="d0_s14" type="structure" />
      <biological_entity id="o27980" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s14" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o27979" id="r4762" name="with" negation="false" src="d0_s14" to="o27980" />
    </statement>
    <statement id="d0_s15">
      <text>awns about equally thick, divergent, spirally contorted at the base but usually not with distinct coils, not disarticulating at maturity;</text>
      <biological_entity id="o27981" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="equally" name="width" src="d0_s15" value="thick" value_original="thick" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="divergent" value_original="divergent" />
        <character constraint="at base" constraintid="o27982" is_modifier="false" modifier="spirally" name="arrangement_or_shape" src="d0_s15" value="contorted" value_original="contorted" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s15" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o27982" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o27983" name="coil" name_original="coils" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o27982" id="r4763" modifier="usually not" name="with" negation="false" src="d0_s15" to="o27983" />
    </statement>
    <statement id="d0_s16">
      <text>central awns 10-15 mm;</text>
      <biological_entity constraint="central" id="o27984" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s16" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lateral awns 8-13 mm;</text>
      <biological_entity constraint="lateral" id="o27985" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, about 2 mm.</text>
      <biological_entity id="o27986" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s18" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 4-5 mm, chestnut-colored.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = unknown.</text>
      <biological_entity id="o27987" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="chestnut-colored" value_original="chestnut-colored" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27988" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Aristida condensata grows on sandy hills, and in pine and oak barrens in the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;N.C.;Ala.;S.C.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>