<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">703</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ZEA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">mays</taxon_name>
    <taxon_name authority="H.H. litis &amp; Doebley" date="unknown" rank="subspecies">parviglumis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus zea;species mays;subspecies parviglumis</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Balsas teosinte</other_name>
  <other_name type="common_name">Guerrero teosinte</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (0.5) 2-4 m, unbranched or branched above the middle, thinner than in subsp.</text>
      <biological_entity id="o3657" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="m" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="above middle" constraintid="o3658" is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o3658" name="middle" name_original="middle" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>mexicana.</text>
    </statement>
    <statement id="d0_s2">
      <text>Leaves pubescent.</text>
    </statement>
    <statement id="d0_s3">
      <text>Fruitcases 5-8 mm long, 3-5 mm wide.</text>
      <biological_entity id="o3660" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Caryopses concealed.</text>
      <biological_entity id="o3661" name="caryopsis" name_original="caryopses" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="concealed" value_original="concealed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Staminate panicles with (2) 10-100 (235) slender, often drooping branches;</text>
      <biological_entity id="o3662" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o3663" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="235" value_original="235" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s5" to="100" />
        <character is_modifier="true" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s5" value="drooping" value_original="drooping" />
      </biological_entity>
      <relation from="o3662" id="r587" name="with" negation="false" src="d0_s5" to="o3663" />
    </statement>
    <statement id="d0_s6">
      <text>spikelets 4.6-7.2 mm, distant.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 20.</text>
      <biological_entity id="o3664" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" src="d0_s6" to="7.2" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="distant" value_original="distant" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3665" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Zea mays subsp. parviglumis, which has the smallest fruitcases of all the wild taxa, is endemic to the Pacific slope of southern Mexico, from Oaxaca to Jalisco, being most abundant in the Balsas River drainage. It grows in highly seasonal, sunny thorn scrub, and open tropical deciduous forests and savannahs, at elevations of (450)600-1400(1950) m. One of its higher elevation populations appears to be the ancestor of subsp. mays. In the southern United States, Z. mays subsp. parviglumis is grown as part of breeding programs. In its native habitat, it tends to be seasonally isolated from subsp. mays, flowering a few weeks later, but the two sometimes form abundant hybrids in local areas.</discussion>
  
</bio:treatment>