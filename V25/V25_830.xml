<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Neil Snow;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">228</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Steud." date="unknown" rank="genus">SCHEDONNARDUS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus schedonnardus</taxon_hierarchy>
  </taxon_identification>
  <number>17.40</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o3514" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 8-55 cm, sometimes geniculate and branched basally, usually curving distally;</text>
      <biological_entity id="o3515" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="55" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually; distally" name="course" src="d0_s2" value="curving" value_original="curving" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes minutely retrorsely pubescent, mostly solid.</text>
      <biological_entity id="o3516" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="minutely retrorsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s3" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o3517" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3518" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths compressed-keeled, closed, glabrous, margins scarious;</text>
      <biological_entity id="o3519" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="compressed-keeled" value_original="compressed-keeled" />
        <character is_modifier="false" name="condition" src="d0_s5" value="closed" value_original="closed" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3520" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 1-3.5 mm, membranous, lanceolate;</text>
      <biological_entity id="o3521" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 1-12 cm long, 0.7-2 mm wide, stiff, usually folded, often spirally twisted, midrib well-developed, margins thick and whitish.</text>
      <biological_entity id="o3522" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="often spirally" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o3523" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character is_modifier="false" name="development" src="d0_s7" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <biological_entity id="o3524" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, panicles of widely spaced, racemosely arranged, spikelike branches, exceeding the upper leaves;</text>
      <biological_entity id="o3525" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o3526" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
      <biological_entity id="o3527" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="widely" name="arrangement" src="d0_s8" value="spaced" value_original="spaced" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="upper" id="o3528" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o3526" id="r574" name="part_of" negation="false" src="d0_s8" to="o3527" />
      <relation from="o3526" id="r575" name="exceeding the" negation="false" src="d0_s8" to="o3528" />
    </statement>
    <statement id="d0_s9">
      <text>branches strongly divergent, with distant to slightly imbricate, closely appressed spikelets.</text>
      <biological_entity id="o3529" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o3530" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="distant" is_modifier="true" name="arrangement" src="d0_s9" to="slightly imbricate" />
        <character is_modifier="true" modifier="closely" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o3529" id="r576" name="with" negation="false" src="d0_s9" to="o3530" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-5.5 mm, mostly sessile, compressed laterally, with 1 floret;</text>
      <biological_entity id="o3531" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o3532" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o3531" id="r577" name="with" negation="false" src="d0_s10" to="o3532" />
    </statement>
    <statement id="d0_s11">
      <text>florets bisexual;</text>
    </statement>
    <statement id="d0_s12">
      <text>disarticulation at the base of the panicle and above the glumes.</text>
      <biological_entity id="o3533" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o3534" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <relation from="o3533" id="r578" name="at the base of the panicle and above" negation="false" src="d0_s12" to="o3534" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes lanceolate, unequal, 1-veined;</text>
      <biological_entity id="o3535" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas usually exceeding the glumes, 3-veined, unawned or shortly awned;</text>
      <biological_entity id="o3536" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o3537" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <relation from="o3536" id="r579" modifier="usually" name="exceeding the" negation="false" src="d0_s14" to="o3537" />
    </statement>
    <statement id="d0_s15">
      <text>paleas subequal to the lemmas;</text>
      <biological_entity id="o3538" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character constraint="to lemmas" constraintid="o3539" is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o3539" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 3;</text>
      <biological_entity id="o3540" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>styles 2.</text>
      <biological_entity id="o3541" name="style" name_original="styles" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses fusiform, x = 10.</text>
      <biological_entity id="o3542" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity constraint="x" id="o3543" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schedonnardus is a monotypic North American genus that grows in the prairies and central plains of Canada, the United States, and northwestern Mexico. It has also been found, as a recent introduction, in California and Argentina. It is not known if it is established in California.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;Man.;Sask.;Wyo.;Nev.;Colo.;N.Mex.;Tex.;La.;Utah;Calif.;Minn.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Mont.;Pacific Islands (Hawaii);Ill.;Miss.;Iowa;Ariz.;Mo.;Ark.;Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>