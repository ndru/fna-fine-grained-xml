<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="(L.) Koeler" date="unknown" rank="species">filiformis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species filiformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Syntherisma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">filiformis</taxon_name>
    <taxon_hierarchy>genus syntherisma;species filiformis</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Slender crabgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, or short-lived perennials;</text>
      <biological_entity id="o2122" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o2121" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (10) 25-150 cm, erect or decumbent, branching, sometimes rooting at the lower nodes;</text>
      <biological_entity id="o2123" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character constraint="at lower nodes" constraintid="o2124" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o2124" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes 3-6.</text>
      <biological_entity id="o2125" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths keeled, basal sheaths usually with papillose-based hairs, rarely glabrous;</text>
      <biological_entity id="o2126" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2127" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2128" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o2127" id="r351" name="with" negation="false" src="d0_s4" to="o2128" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-1.5 mm;</text>
      <biological_entity id="o2129" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-18 cm long, 1-6 mm wide, flat or involute, glabrous, scabrous, or pilose.</text>
      <biological_entity id="o2130" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles with 2-7 spikelike primary branches, these digitate or the rachises to 1 cm;</text>
      <biological_entity id="o2131" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity constraint="primary" id="o2132" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o2133" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o2131" id="r352" name="with" negation="false" src="d0_s7" to="o2132" />
    </statement>
    <statement id="d0_s8">
      <text>longest primary branches 20-25 cm, axes triquetrous, not winged, with spikelets in unequally pedicellate groups of 3 (-5) on the basal ½ (J. Wipff, pers. comm.).</text>
      <biological_entity constraint="longest primary" id="o2134" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s8" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2135" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triquetrous" value_original="triquetrous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o2136" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o2137" name="group" name_original="groups" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="3" from_inclusive="false" modifier="of" name="atypical_quantity" src="d0_s8" to="5" />
        <character constraint="on basal axes" constraintid="o2138" modifier="of" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2138" name="axis" name_original="axes" src="d0_s8" type="structure" />
      <relation from="o2135" id="r353" name="with" negation="false" src="d0_s8" to="o2136" />
      <relation from="o2136" id="r354" name="in" negation="false" src="d0_s8" to="o2137" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 1.3-2.8 mm.</text>
      <biological_entity id="o2139" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes absent or to 0.1 mm;</text>
      <biological_entity constraint="lower" id="o2140" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="0-0.1 mm" value_original="0-0.1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1-2 mm long, from 3/4 to almost as long as the spikelets, almost glabrous or sparsely to densely pubescent with clavate to capitate hairs (use 20x magnification), glume apices rounded;</text>
      <biological_entity constraint="upper" id="o2141" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" constraint="with hairs" constraintid="o2143" from="glabrous or" modifier="almost" name="pubescence" notes="" src="d0_s11" to="sparsely densely pubescent" />
      </biological_entity>
      <biological_entity id="o2142" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o2143" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="clavate" is_modifier="true" name="shape" src="d0_s11" to="capitate" />
      </biological_entity>
      <biological_entity constraint="glume" id="o2144" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o2141" id="r355" name="as long as" negation="false" src="d0_s11" to="o2142" />
    </statement>
    <statement id="d0_s12">
      <text>lower lemmas equaling the spikelets, glabrous or glandular-pubescent, 5-7-veined, veins unequally spaced, outer 3 veins on each side closer to each other than the midvein to the inner lateral-veins;</text>
      <biological_entity constraint="lower" id="o2145" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o2146" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o2147" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="unequally" name="arrangement" src="d0_s12" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2148" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o2149" name="side" name_original="side" src="d0_s12" type="structure">
        <character constraint="other-than midvein" constraintid="o2150" is_modifier="false" name="arrangement" src="d0_s12" value="closer" value_original="closer" />
      </biological_entity>
      <biological_entity id="o2150" name="midvein" name_original="midvein" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o2151" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure" />
      <relation from="o2148" id="r356" name="on" negation="false" src="d0_s12" to="o2149" />
      <relation from="o2150" id="r357" name="to" negation="false" src="d0_s12" to="o2151" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas 1.3-2 mm, apiculate, dark-brown at maturity;</text>
      <biological_entity constraint="upper" id="o2152" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 0.3-0.6 mm. 2n = 36, 54.</text>
      <biological_entity id="o2153" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2154" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
        <character name="quantity" src="d0_s14" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria filiformis grows throughout the warmer parts of the eastern United States, van filifomis the most widespread of its varieties, extending into Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;D.C.;W.Va.;Fla.;N.H.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Ala.;Mass.;R.I.;Ark.;Vt.;Ill.;Ga.;Ind.;Iowa;Md.;Kans.;Okla.;Ohio;Mo.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower lemmas glabrous</description>
      <determination>Digitaria filiformis var. laeviglumis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower lemmas pubescent.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Basal leaf sheaths glabrous; cauline blades about 1 mm wide, folded or involute</description>
      <determination>Digitaria filiformis var. dolichophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Basal leaf sheaths with papillose-based hairs; cauline blades 1-6 mm wide, flat.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets 1.3-1.9 mm long; panicle branches 3-13 cm long; culms 10-80 cm tall</description>
      <determination>Digitaria filiformis var. filiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets 2-2.8 mm long; panicle branches 10-25 cm long; plants 75-150 cm tall</description>
      <determination>Digitaria filiformis var. villosa</determination>
    </key_statement>
  </key>
</bio:treatment>