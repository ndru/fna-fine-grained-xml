<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">CALAMOVILFA</taxon_name>
    <taxon_name authority="(Hook.) Scribn." date="unknown" rank="species">longifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">longifolia</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus calamovilfa;species longifolia;variety longifolia</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Sheaths usually glabrous, sometimes sparsely pubescent, rarely densely pubescent.</text>
      <biological_entity id="o26658" name="sheath" name_original="sheaths" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Panicles to 55.5 cm long, usually 6.7-13.9 times as long as wide;</text>
      <biological_entity id="o26659" name="panicle" name_original="panicles" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="55.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s1" value="6.7-13.9" value_original="6.7-13.9" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches to 23 cm long, erect or ascending, at least in the upper 1/3 of the panicle.</text>
      <biological_entity id="o26660" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="23" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="upper" id="o26661" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o26662" name="panicle" name_original="panicle" src="d0_s2" type="structure" />
      <relation from="o26660" id="r4529" modifier="at-least" name="in" negation="false" src="d0_s2" to="o26661" />
      <relation from="o26661" id="r4530" name="part_of" negation="false" src="d0_s2" to="o26662" />
    </statement>
    <statement id="d0_s3">
      <text>Spikelets usually without a brownish cast, relatively closely imbricate.</text>
      <biological_entity id="o26663" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="relatively closely" name="arrangement" notes="" src="d0_s3" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o26664" name="cast" name_original="cast" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
      </biological_entity>
      <relation from="o26663" id="r4531" name="without" negation="false" src="d0_s3" to="o26664" />
    </statement>
  </description>
  <discussion>Calamovilfa longifolia var. longifolia is a characteristic grass on the drier prairies of the interior plains, from southern Canada to northern New Mexico, with reports from southern Arizona. It also grows, as an adventive, in Washington, Wisconsin, Michigan, and Missouri.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;Iowa;Idaho;Kans.;Mich.;Minn.;Mo.;Mont.;N.Dak.;Nebr.;N.Mex.;S.Dak.;Wyo.;Wash.;Alta.;B.C.;Man.;Ont.;Sask.;Wis.;Ill.;Ind.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>