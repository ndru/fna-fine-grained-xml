<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Chapm." date="unknown" rank="species">arctatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species arctatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Pinewoods bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or somewhat rhizomatous, upper portion dense, oblong to ovate.</text>
      <biological_entity id="o18030" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="upper" id="o18031" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s0" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 90-170 cm;</text>
      <biological_entity id="o18032" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s1" to="170" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes occasionally somewhat glaucous just below the nodes;</text>
      <biological_entity id="o18033" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="just below nodes" constraintid="o18034" is_modifier="false" modifier="occasionally somewhat" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o18034" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>branches straight, erect to ascending.</text>
      <biological_entity id="o18035" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth, rarely some¬what scabrous;</text>
      <biological_entity id="o18036" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-0.9 mm, sometimes ciliate, cilia to 0.5 mm;</text>
      <biological_entity id="o18037" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18038" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 15-35 cm long, 3-8 mm wide, glabrous or densely pubescent, hairs spreading.</text>
      <biological_entity id="o18039" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18040" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence units 5-45 per culm;</text>
      <biological_entity id="o18042" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>subtending sheaths (3.3) 3.8-6.8 (9) cm long, (2.5) 3.2-4 (5) mm wide;</text>
      <biological_entity constraint="inflorescence" id="o18041" name="unit" name_original="units" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o18042" from="5" name="quantity" src="d0_s7" to="45" />
        <character name="length" src="d0_s8" unit="cm" value="3.3" value_original="3.3" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="length" src="d0_s8" to="6.8" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18043" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o18041" id="r3035" name="subtending" negation="false" src="d0_s8" to="o18043" />
    </statement>
    <statement id="d0_s9">
      <text>peduncles (9) 26-66 (115) mm, with 2 (4) rames;</text>
      <biological_entity id="o18045" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <relation from="o18044" id="r3036" name="with" negation="false" src="d0_s9" to="o18045" />
    </statement>
    <statement id="d0_s10">
      <text>rames (2.2) 2.6-4.3 (5.3) cm, usually exserted at maturity, pubescence either evenly distributed or more dense distally within each internode.</text>
      <biological_entity id="o18044" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="26" from_unit="mm" name="some_measurement" src="d0_s9" to="66" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="2.6" from_unit="cm" name="some_measurement" src="d0_s10" to="4.3" to_unit="cm" />
        <character constraint="at " constraintid="o18046" is_modifier="false" modifier="usually" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o18046" name="internode" name_original="internode" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="character" src="d0_s10" value="pubescence" value_original="pubescence" />
        <character is_modifier="true" modifier="either evenly" name="arrangement" src="d0_s10" value="distributed" value_original="distributed" />
        <character is_modifier="true" name="density" src="d0_s10" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o18047" name="internode" name_original="internode" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="character" src="d0_s10" value="pubescence" value_original="pubescence" />
        <character is_modifier="true" modifier="either evenly" name="arrangement" src="d0_s10" value="distributed" value_original="distributed" />
        <character is_modifier="true" name="density" src="d0_s10" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o18046" id="r3037" name="at" negation="false" src="d0_s10" to="o18047" />
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets (4.3) 4.9-5.4 (6.1) mm;</text>
      <biological_entity id="o18048" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4.3" value_original="4.3" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="some_measurement" src="d0_s11" to="5.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 1.5-2.5 mm;</text>
      <biological_entity constraint="callus" id="o18049" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keels of lower glumes scabrous from below the midpoint;</text>
      <biological_entity id="o18050" name="keel" name_original="keels" src="d0_s13" type="structure" constraint="glume" constraint_original="glume; glume">
        <character constraint="below midpoint" constraintid="o18052" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18051" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o18052" name="midpoint" name_original="midpoint" src="d0_s13" type="structure" />
      <relation from="o18050" id="r3038" name="part_of" negation="false" src="d0_s13" to="o18051" />
    </statement>
    <statement id="d0_s14">
      <text>awns 5-16 mm;</text>
      <biological_entity id="o18053" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1 (3), 2-3.5 mm, red.</text>
      <biological_entity id="o18054" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets vestigial or absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o18055" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18056" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon arctatus grows in flatwoods, bogs, and scrublands of southern Alabama and Florida. Its flowering appears to be stimulated by fire but, unlike other members of sect. Leptopogon in the Flora region, the effect lasts only one or two years, the plants then remaining vegetative until the next fire occurs. It is similar to A. ternarius, but differs in its long, usually solitary anther and shorter spikelets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.;Fla.;Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>