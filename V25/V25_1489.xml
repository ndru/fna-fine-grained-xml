<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">597</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Trin. ex Schltdl." date="unknown" rank="species">lividum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species lividum</taxon_hierarchy>
  </taxon_identification>
  <number>38</number>
  <other_name type="common_name">Longtom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>decumbent or cespitose.</text>
      <biological_entity id="o10769" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-97 cm, erect;</text>
      <biological_entity id="o10770" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="97" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o10771" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o10772" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2.2-4.7 mm;</text>
      <biological_entity id="o10773" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s5" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 38 cm long, 2.3-6.2 mm wide, flat, glabrous or pubescent.</text>
      <biological_entity id="o10774" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="38" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s6" to="6.2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 3-11 racemosely arranged branches;</text>
      <biological_entity id="o10775" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o10776" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="11" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o10775" id="r1771" name="with" negation="false" src="d0_s7" to="o10776" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1.5-4 cm, divergent, occasionally arcuate, terminating in a spikelet;</text>
      <biological_entity id="o10777" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="occasionally" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity id="o10778" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
      <relation from="o10777" id="r1772" name="terminating in a" negation="false" src="d0_s8" to="o10778" />
    </statement>
    <statement id="d0_s9">
      <text>branch axes 1.5-2 mm wide, broadly winged, glabrous or sparsely pubescent, margins scabrous, usually slightly conduplicate, occasionally purple.</text>
      <biological_entity constraint="branch" id="o10779" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10780" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="usually slightly" name="arrangement_or_vernation" src="d0_s9" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" modifier="occasionally" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.2-2.6 mm long, 1.2-1.5 mm wide, paired, imbricate, appressed to divergent from the branch axes, elliptic to obovate, stramineous (rarely purple-spotted), margins scabrous apically.</text>
      <biological_entity id="o10781" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s10" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character constraint="from branch axes" constraintid="o10782" is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s10" to="obovate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="branch" id="o10782" name="axis" name_original="axes" src="d0_s10" type="structure" />
      <biological_entity id="o10783" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="apically" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o10784" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes and lower lemmas glabrous, 5-veined, margins entire;</text>
      <biological_entity constraint="upper" id="o10785" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o10786" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o10787" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas lacking ribs over the veins;</text>
      <biological_entity constraint="lower" id="o10788" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o10789" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o10790" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <relation from="o10789" id="r1773" name="over" negation="false" src="d0_s13" to="o10790" />
    </statement>
    <statement id="d0_s14">
      <text>upper florets white to pale.</text>
      <biological_entity constraint="upper" id="o10791" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2-2.2 mm, brown.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 40, 60.</text>
      <biological_entity id="o10792" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10793" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
        <character name="quantity" src="d0_s16" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum lividum grows in fresh and brackish marshes and ditches. It is native from the Gulf coast of the United States southward through Mexico and Central America to Cuba and Argentina. Plants of P. modestum with pale upper florets may be mistaken for P. lividum, but will have ligules that are only 1-2.3 mm long.</discussion>
  <discussion>Zuloaga and Morrone regard Paspalum lividum as a synonym of P. denticulatum Trin. (http://mobot.mobot.org/W3T/SearclVnwgc.html December 9, 2002).</discussion>
  
</bio:treatment>