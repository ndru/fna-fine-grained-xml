<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">696</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">TRIPSACUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Tripsacum</taxon_name>
    <taxon_name authority="Porter ex Vasey" date="unknown" rank="species">floridanum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus tripsacum;section tripsacum;species floridanum</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Florida gamagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with short, thick rhizomes.</text>
      <biological_entity id="o9510" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9511" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o9510" id="r1565" name="with" negation="false" src="d0_s0" to="o9511" />
    </statement>
    <statement id="d0_s1">
      <text>Culms to 1 m tall, to 2 mm thick, usually solitary or in small clumps.</text>
      <biological_entity id="o9512" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="height" src="d0_s1" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="in small clumps" value_original="in small clumps" />
      </biological_entity>
      <biological_entity id="o9513" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o9512" id="r1566" name="in" negation="false" src="d0_s1" to="o9513" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o9514" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 60 cm long, 1-7 (15) mm wide, involute or folded, glabrous.</text>
      <biological_entity id="o9515" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="60" to_unit="cm" />
        <character name="width" src="d0_s3" unit="mm" value="15" value_original="15" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="folded" value_original="folded" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Terminal inflorescences erect, with 1-2 rames.</text>
      <biological_entity constraint="terminal" id="o9516" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o9517" name="rame" name_original="rames" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <relation from="o9516" id="r1567" name="with" negation="false" src="d0_s4" to="o9517" />
    </statement>
    <statement id="d0_s5">
      <text>Pistillate spikelets 3.5-4.5 mm wide.</text>
      <biological_entity id="o9518" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Staminate spikelets sessile-pedicellate;</text>
      <biological_entity id="o9519" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikelets 5-7 mm;</text>
      <biological_entity id="o9520" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>glumes coriaceous, acute;</text>
      <biological_entity id="o9521" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels to 2 mm long, to 0.5 mm wide, triangular in cross-section.</text>
      <biological_entity id="o9523" name="cross-section" name_original="cross-section" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 36.</text>
      <biological_entity id="o9522" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s9" to="0.5" to_unit="mm" />
        <character constraint="in cross-section" constraintid="o9523" is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9524" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tripsacum floridanum grows along roadsides and in pine woods, often in wet soils, of Florida and Cuba. It is grown as an ornamental, but it reseeds rather too readily under some conditions. Reports of T. flori¬danum from Texas are based on narrow-bladed speci¬mens of T. dactyloides.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>