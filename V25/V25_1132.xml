<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">410</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Freckmann &amp; Lelong" date="unknown" rank="section">Pedicellata</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="species">nodatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section pedicellata;species nodatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nodatum</taxon_name>
    <taxon_hierarchy>genus panicum;species nodatum</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Sarita panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually cespitose, rarely rhizomatous.</text>
      <biological_entity id="o1856" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes absent.</text>
      <biological_entity constraint="basal" id="o1857" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-65 cm, decumbent to ascending even in spring, with hard, cormlike bases;</text>
      <biological_entity id="o1858" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character char_type="range_value" from="decumbent" modifier="even in spring; in spring" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
      <biological_entity id="o1859" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="cormlike" value_original="cormlike" />
      </biological_entity>
      <relation from="o1858" id="r307" name="with" negation="false" src="d0_s2" to="o1859" />
    </statement>
    <statement id="d0_s3">
      <text>nodes puberulent to sparsely pubescent;</text>
      <biological_entity id="o1860" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="sparsely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes scabrous-puberulent to papillose-hirsute;</text>
    </statement>
    <statement id="d0_s5">
      <text>fall phase with geniculate to decumbent culms, developing divaricate branches from the midculm nodes before the primary panicles mature.</text>
      <biological_entity id="o1861" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="scabrous-puberulent" name="pubescence" src="d0_s4" to="papillose-hirsute" />
      </biological_entity>
      <biological_entity id="o1862" name="phase" name_original="phase" src="d0_s5" type="structure" />
      <biological_entity id="o1863" name="culm" name_original="culms" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="geniculate" value_original="geniculate" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s5" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o1864" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="development" src="d0_s5" value="developing" value_original="developing" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
      </biological_entity>
      <biological_entity constraint="midculm" id="o1865" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity constraint="primary" id="o1866" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o1861" id="r308" name="fall" negation="false" src="d0_s5" to="o1862" />
      <relation from="o1864" id="r309" name="from" negation="false" src="d0_s5" to="o1865" />
      <relation from="o1865" id="r310" name="before" negation="false" src="d0_s5" to="o1866" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 8-14;</text>
      <biological_entity constraint="cauline" id="o1867" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths not overlapping, puberulent to papillose-hirsute, margins ciliate;</text>
      <biological_entity id="o1868" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s7" to="papillose-hirsute" />
      </biological_entity>
      <biological_entity id="o1869" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules 0.1-1 mm, of hairs;</text>
      <biological_entity id="o1870" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1871" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <relation from="o1870" id="r311" name="consists_of" negation="false" src="d0_s8" to="o1871" />
    </statement>
    <statement id="d0_s9">
      <text>blades 3-9 cm long, 4-8 mm wide, thick, firm, puberulent, sides parallel above the rounded to truncate bases, margins with papillose-based cilia.</text>
      <biological_entity id="o1872" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s9" to="9" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s9" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o1873" name="side" name_original="sides" src="d0_s9" type="structure">
        <character constraint="above bases" constraintid="o1874" is_modifier="false" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o1874" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="rounded to truncate" value_original="rounded to truncate" />
      </biological_entity>
      <biological_entity id="o1875" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity id="o1876" name="cilium" name_original="cilia" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1875" id="r312" name="with" negation="false" src="d0_s9" to="o1876" />
    </statement>
    <statement id="d0_s10">
      <text>Primary panicles 3-13 cm long, 2-8 cm wide, exserted;</text>
      <biological_entity constraint="primary" id="o1877" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="13" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s10" to="8" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>branches ascending to divaricate at maturity;</text>
      <biological_entity id="o1878" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character constraint="at maturity" is_modifier="false" name="arrangement" src="d0_s11" value="divaricate" value_original="divaricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicels appressed.</text>
      <biological_entity id="o1879" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 3.4-4.4 mm long, 1.3-1.6 mm wide, narrowly obovoid-obpyriform, finely pubescent, hairs papillose-based, bases long, narrow.</text>
      <biological_entity id="o1880" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="length" src="d0_s13" to="4.4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="obovoid-obpyriform" value_original="obovoid-obpyriform" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o1881" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o1882" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes 1.5-2 mm, attached about 0.2 mm below the upper glumes, partly or completely encircling the pedicels;</text>
      <biological_entity constraint="lower" id="o1883" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="fixation" src="d0_s14" value="attached" value_original="attached" />
        <character constraint="below upper glumes" constraintid="o1884" name="some_measurement" src="d0_s14" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1884" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o1885" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <relation from="o1883" id="r313" modifier="completely; partly" name="encircling the" negation="false" src="d0_s14" to="o1885" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes about 0.3 mm shorter than the upper florets, purplish at the bases;</text>
      <biological_entity constraint="upper" id="o1886" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.3" value_original="0.3" />
        <character constraint="than the upper florets" constraintid="o1887" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character constraint="at bases" constraintid="o1888" is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1887" name="floret" name_original="florets" src="d0_s15" type="structure" />
      <biological_entity id="o1888" name="base" name_original="bases" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o1889" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets with pointed, puberulent apices.</text>
      <biological_entity id="o1891" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="pointed" value_original="pointed" />
        <character is_modifier="true" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o1890" id="r314" name="with" negation="false" src="d0_s17" to="o1891" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18 (J. Wipff, pers. comm., 2001).</text>
      <biological_entity constraint="upper" id="o1890" name="floret" name_original="florets" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o1892" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium nodatum grows in oak savannahs near the Gulf coast from Texas to northeastern Mexico. The primary panicles are produced from April into June (sometimes late August to November) and are at least partly open-pollinated; the secondary panicles are produced from May into fall and are at least partly cleistogamous.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>