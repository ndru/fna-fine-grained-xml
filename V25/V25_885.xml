<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="(Desv.) A. Gray" date="unknown" rank="subgenus">Chondrosum</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_name authority="(Feath.) Wipff &amp; S.D. Jones" date="unknown" rank="subspecies">pectinata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus chondrosum;species hirsuta;subspecies pectinata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bouteloua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>genus bouteloua;species pectinata</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Tall grama</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants without rhizomes or stolons.</text>
      <biological_entity id="o15757" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15758" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o15759" name="stolon" name_original="stolons" src="d0_s0" type="structure" />
      <relation from="o15757" id="r2638" name="without" negation="false" src="d0_s0" to="o15758" />
      <relation from="o15757" id="r2639" name="without" negation="false" src="d0_s0" to="o15759" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 35-75 cm, erect, densely tufted, usually unbranched;</text>
      <biological_entity id="o15760" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s1" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes 3-4;</text>
      <biological_entity id="o15761" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous.</text>
      <biological_entity id="o15762" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o15763" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o15764" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths mostly pubescent.</text>
      <biological_entity id="o15765" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles racemose, with (2) 3-6 branches on (3) 6-18 cm rachises.</text>
      <biological_entity id="o15766" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o15767" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o15768" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s6" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="6" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s6" to="18" to_unit="cm" />
      </biological_entity>
      <relation from="o15766" id="r2640" name="with" negation="false" src="d0_s6" to="o15767" />
      <relation from="o15767" id="r2641" name="on" negation="false" src="d0_s6" to="o15768" />
    </statement>
    <statement id="d0_s7">
      <text>Anthers 2-3.2 mm;</text>
      <biological_entity id="o15769" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachilla segments subtending second florets with a distal tuft of hairs.</text>
      <biological_entity constraint="rachilla" id="o15770" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o15771" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o15772" name="tuft" name_original="tuft" src="d0_s8" type="structure" />
      <biological_entity id="o15773" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <relation from="o15771" id="r2642" name="with" negation="false" src="d0_s8" to="o15772" />
      <relation from="o15772" id="r2643" name="part_of" negation="false" src="d0_s8" to="o15773" />
    </statement>
    <statement id="d0_s9">
      <text>Caryopses 1.5-2.6 mm. 2n = 20.</text>
      <biological_entity id="o15774" name="caryopsis" name_original="caryopses" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15775" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua hirsuta subsp. pectinata grows in well-drained, relatively undisturbed, calcareous soils, usually on thin-soiled limestone outcrops, at 60-500 m. Its range extends from southern Oklahoma to central Texas. Although restricted in its geographic distribution, where subsp. pectinata is sympatric with subsp. hirsuta, swarms of morphologically intermediate plants are found (Wipff and Jones 1996).</discussion>
  
</bio:treatment>