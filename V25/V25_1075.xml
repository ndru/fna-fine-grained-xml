<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="(Vasey) Fernald" date="unknown" rank="species">simpsonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species simpsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>20</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants of indefinite duration;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o26685" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 80-120 cm, erect or decumbent and rooting at the lower nodes, not branching at the aerial nodes.</text>
      <biological_entity id="o26686" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o26687" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="at nodes" constraintid="o26688" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26687" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o26688" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths hirsute, with papillose-based hairs, those of the innovation sheaths compressed-keeled;</text>
      <biological_entity id="o26689" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="compressed-keeled" value_original="compressed-keeled" />
      </biological_entity>
      <biological_entity id="o26690" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="innovation" id="o26691" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <relation from="o26689" id="r4539" name="with" negation="false" src="d0_s3" to="o26690" />
      <relation from="o26689" id="r4540" name="part_of" negation="false" src="d0_s3" to="o26691" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o26692" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 7-30 cm long, 3-5 mm wide, flat, pilose above and below.</text>
      <biological_entity id="o26693" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="below" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 6-8 spikelike primary branches on 4-6 cm rachises;</text>
      <biological_entity id="o26694" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o26695" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s6" to="8" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o26696" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <relation from="o26694" id="r4541" name="with" negation="false" src="d0_s6" to="o26695" />
      <relation from="o26695" id="r4542" name="on" negation="false" src="d0_s6" to="o26696" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches 8-13 cm, axes triquetrous, narrowly winged, wings less than 1/2 as wide as the midribs, lower and middle portions of the branches bearing spikelets in appressed, unequally pedicellate pairs;</text>
      <biological_entity constraint="primary" id="o26697" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s7" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26698" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triquetrous" value_original="triquetrous" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o26699" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="as-wide-as midribs" constraintid="o26700" from="0" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity id="o26700" name="midrib" name_original="midribs" src="d0_s7" type="structure" />
      <biological_entity constraint="lower and middle" id="o26701" name="portion" name_original="portions" src="d0_s7" type="structure" />
      <biological_entity id="o26702" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o26703" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o26704" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o26701" id="r4543" name="part_of" negation="false" src="d0_s7" to="o26702" />
      <relation from="o26701" id="r4544" name="bearing" negation="false" src="d0_s7" to="o26703" />
      <relation from="o26701" id="r4545" name="in" negation="false" src="d0_s7" to="o26704" />
    </statement>
    <statement id="d0_s8">
      <text>secondary branches rarely present;</text>
      <biological_entity constraint="secondary" id="o26705" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>axillary inflorescences not present;</text>
      <biological_entity constraint="axillary" id="o26706" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels not adnate to the branch axes.</text>
      <biological_entity id="o26707" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="to branch axes" constraintid="o26708" is_modifier="false" modifier="not" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o26708" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets about 3 mm, elliptic-lanceolate, acute.</text>
      <biological_entity id="o26709" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s11" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes absent or minute and hyaline;</text>
      <biological_entity constraint="lower" id="o26710" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s12" value="minute" value_original="minute" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 7-9-veined, glabrous or obscurely pubescent;</text>
      <biological_entity constraint="upper" id="o26711" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-9-veined" value_original="7-9-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower lemmas 7-9-veined, glabrous or obscurely pubescent;</text>
      <biological_entity constraint="lower" id="o26712" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-9-veined" value_original="7-9-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper lemmas elliptic, yellow or gray, becoming purple at maturity, slightly apiculate.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o26713" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="gray" value_original="gray" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26714" name="chromosome" name_original="" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Digitaria simpsonii is a rare species, known only from sandy fields in Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>