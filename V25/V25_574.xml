<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">353</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Sanchez-Ken &amp; L.G. Clark" date="unknown" rank="tribe">GYNERIEAE</taxon_name>
    <taxon_name authority="Willd. ex P. Beauv." date="unknown" rank="genus">GYNERIUM</taxon_name>
    <taxon_name authority="(Aublet) P. Beauv." date="unknown" rank="species">sagittatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe gynerieae;genus gynerium;species sagittatum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Wildcane</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 2-10 (15) m tall, 1-4 cm thick.</text>
      <biological_entity id="o6294" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="height" src="d0_s0" unit="m" value="15" value_original="15" />
        <character char_type="range_value" from="2" from_unit="m" name="height" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="cm" name="thickness" src="d0_s0" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths distichous, narrowing toward the apices, sometimes with a line of deciduous hairs on the back below the articulation with the blade;</text>
      <biological_entity id="o6295" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="distichous" value_original="distichous" />
        <character constraint="toward apices" constraintid="o6296" is_modifier="false" name="width" src="d0_s1" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o6296" name="apex" name_original="apices" src="d0_s1" type="structure" />
      <biological_entity id="o6297" name="line" name_original="line" src="d0_s1" type="structure" />
      <biological_entity id="o6298" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o6299" name="back" name_original="back" src="d0_s1" type="structure" />
      <biological_entity id="o6300" name="articulation" name_original="articulation" src="d0_s1" type="structure" />
      <biological_entity id="o6301" name="blade" name_original="blade" src="d0_s1" type="structure" />
      <relation from="o6295" id="r997" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o6297" />
      <relation from="o6297" id="r998" name="part_of" negation="false" src="d0_s1" to="o6298" />
      <relation from="o6297" id="r999" name="on" negation="false" src="d0_s1" to="o6299" />
      <relation from="o6299" id="r1000" name="below" negation="false" src="d0_s1" to="o6300" />
      <relation from="o6300" id="r1001" name="with" negation="false" src="d0_s1" to="o6301" />
    </statement>
    <statement id="d0_s2">
      <text>ligules 0.5-2 mm;</text>
      <biological_entity id="o6302" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades long-attenuate, (0.4) 1.5-2 m long, 2-10 cm wide, in a flat, fan-shaped arrangement, margins serrate.</text>
      <biological_entity id="o6303" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-attenuate" value_original="long-attenuate" />
        <character name="length" src="d0_s3" unit="m" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="1.5" from_unit="m" name="length" src="d0_s3" to="2" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6304" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="true" name="shape" src="d0_s3" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="true" name="character" src="d0_s3" value="arrangement" value_original="arrangement" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <relation from="o6303" id="r1002" name="in" negation="false" src="d0_s3" to="o6304" />
    </statement>
    <statement id="d0_s4">
      <text>Panicles 0.5-1.5 (2) m.</text>
      <biological_entity id="o6305" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="m" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s4" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate spikelets 8-11 mm;</text>
      <biological_entity id="o6306" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lower glumes 2.5-4 mm, lanceolate, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o6307" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 7-11 mm, linear-subulate, curved when mature, 3-veined;</text>
      <biological_entity constraint="upper" id="o6308" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" modifier="when mature" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 4.5-7 mm;</text>
      <biological_entity id="o6309" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>paleas 1-2 mm, lanceolate.</text>
      <biological_entity id="o6310" name="palea" name_original="paleas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate spikelets 3-4 mm;</text>
      <biological_entity id="o6311" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>glumes (1.5) 2-3 mm, hyaline, acute;</text>
      <biological_entity id="o6312" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 3-4 mm, lanceolate, acute to acuminate;</text>
      <biological_entity id="o6313" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas to 2.5 mm, hyaline;</text>
      <biological_entity id="o6314" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.5-2 mm, purplish.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 44.</text>
      <biological_entity id="o6315" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6316" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gynerium sagittatum is grown as an ornamental in subtropical portions of the Flora region. Even when vegetative, it can be identified by its height, the absence of blades on the lower leaves, the strongly distichous, fan-shaped arrangement of the distal leaf blades, and the wide midveins of the blades. It does not flower when grown outdoors in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>