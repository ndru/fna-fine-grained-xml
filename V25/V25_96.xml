<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">LEPTOCHLOA</taxon_name>
    <taxon_name authority="(Scribn.) Beal" date="unknown" rank="species">viscida</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus leptochloa;species viscida</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Sonoran sprangletop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o7353" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (3) 10-60 cm, prostrate or erect, round or somewhat compressed, often highly branched;</text>
      <biological_entity id="o7354" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="often highly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes hollow.</text>
      <biological_entity id="o7355" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous (rarely sparsely pilose near the base), sometimes with a sticky exudate;</text>
      <biological_entity id="o7356" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7357" name="exudate" name_original="exudate" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="sticky" value_original="sticky" />
      </biological_entity>
      <relation from="o7356" id="r1204" modifier="sometimes" name="with" negation="false" src="d0_s3" to="o7357" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1.2-2.5 mm, truncate, erose to lacerate;</text>
      <biological_entity id="o7358" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-15 cm long, 1.2-5.5 mm wide, glabrous abaxially and adaxially.</text>
      <biological_entity id="o7359" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s5" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 2-17 cm, with 5-23 racemose branches;</text>
      <biological_entity id="o7360" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="17" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7361" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="23" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <relation from="o7360" id="r1205" name="with" negation="false" src="d0_s6" to="o7361" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1-2.5 (3.5) cm, stiff, often included in the upper leaf-sheaths.</text>
      <biological_entity id="o7362" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7363" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <relation from="o7362" id="r1206" modifier="often" name="included in the" negation="false" src="d0_s7" to="o7363" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 4.5-7.5 mm, more or less imbricate, magenta or green, with 2-6 florets.</text>
      <biological_entity id="o7364" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o7365" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <relation from="o7364" id="r1207" name="with" negation="false" src="d0_s8" to="o7365" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes triangular, acute;</text>
      <biological_entity id="o7366" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 1.6-2 mm, acute;</text>
      <biological_entity constraint="lower" id="o7367" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 2-2.9 mm;</text>
      <biological_entity constraint="upper" id="o7368" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2.4-3.5 mm, ovate, membranous, sericeous along the lower veins, lateral-veins pronounced, apices acute, obtuse, or truncate, awned, awns 0.5-1.5 mm;</text>
      <biological_entity id="o7369" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character constraint="along lower veins" constraintid="o7370" is_modifier="false" name="pubescence" src="d0_s12" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7370" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o7371" name="lateral-vein" name_original="lateral-veins" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <biological_entity id="o7372" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o7373" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas minutely scabrous along the veins;</text>
      <biological_entity id="o7374" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character constraint="along veins" constraintid="o7375" is_modifier="false" modifier="minutely" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o7375" name="vein" name_original="veins" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 3, 0.4-0.5 mm.</text>
      <biological_entity id="o7376" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 1.2-1.6 mm long, 0.4-0.5 mm wide, narrowly elliptic to obovate, transversely elliptic in cross-section.</text>
      <biological_entity id="o7378" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 40.</text>
      <biological_entity id="o7377" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s15" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s15" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s15" to="obovate" />
        <character constraint="in cross-section" constraintid="o7378" is_modifier="false" modifier="transversely" name="arrangement_or_shape" src="d0_s15" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7379" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leptochloa viscida is a Sonoran Desert species that occurs from southern California to southwestern New Mexico and south into adjacent Mexico. It differs from L. fusca subsp. fascicularis, which grows in the same region, in its consistently short-awned lemmas, smaller panicles, often prostrate and much-branched growth habit, and often reddish florets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>