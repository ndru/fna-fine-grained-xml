<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">273</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Humb. &amp; Bonpl. ex Willd." date="unknown" rank="genus">AEGOPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus aegopogon</taxon_hierarchy>
  </taxon_identification>
  <number>17.50</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>synoecious;</text>
    </statement>
    <statement id="d0_s2">
      <text>tufted, cespitose, or sprawling.</text>
      <biological_entity id="o17492" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="synoecious" value_original="synoecious" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s2" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="sprawling" value_original="sprawling" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 2-30 cm.</text>
      <biological_entity id="o17493" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths open;</text>
      <biological_entity id="o17494" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous;</text>
      <biological_entity id="o17495" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat.</text>
      <biological_entity id="o17496" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, racemelike, 1-sided panicles, usually exceeding the upper leaves;</text>
      <biological_entity id="o17497" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s7" value="racemelike" value_original="racemelike" />
      </biological_entity>
      <biological_entity id="o17498" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity constraint="upper" id="o17499" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o17497" id="r2939" modifier="usually" name="exceeding the" negation="false" src="d0_s7" to="o17499" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.4-0.8 mm, not appressed to the rachis, with 3 spikelets, bases sharply curved, strigose, axes not extending beyond the distal spikelets;</text>
      <biological_entity id="o17500" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
        <character constraint="to rachis" constraintid="o17501" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o17501" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o17502" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o17503" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sharply" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17505" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <relation from="o17500" id="r2940" name="with" negation="false" src="d0_s8" to="o17502" />
      <relation from="o17504" id="r2941" name="extending beyond the" negation="false" src="d0_s8" to="o17505" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation at the base of the branches.</text>
      <biological_entity id="o17504" name="axis" name_original="axes" src="d0_s8" type="structure" />
      <biological_entity id="o17506" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o17507" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o17504" id="r2942" name="at" negation="false" src="d0_s9" to="o17506" />
      <relation from="o17506" id="r2943" name="part_of" negation="false" src="d0_s9" to="o17507" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets with 1 floret;</text>
      <biological_entity id="o17508" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o17509" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17508" id="r2944" name="with" negation="false" src="d0_s10" to="o17509" />
    </statement>
    <statement id="d0_s11">
      <text>lateral spikelets pedicellate, staminate or sterile, varying from rudimentary to as large as the central spikelet;</text>
      <biological_entity constraint="lateral" id="o17510" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="variability" src="d0_s11" value="varying" value_original="varying" />
        <character constraint="as-large-as central spikelet" constraintid="o17511" is_modifier="false" name="prominence" src="d0_s11" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity constraint="central" id="o17511" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>central spikelets sessile or pedicellate, laterally compressed, bisexual.</text>
      <biological_entity constraint="central" id="o17512" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes exceeded by the florets, cuneate, truncate to bilobed, 1-veined, awned from the midveins, sometimes also from the lateral lobes;</text>
      <biological_entity id="o17513" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s13" value="truncate to bilobed" value_original="truncate to bilobed" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character constraint="from midveins" constraintid="o17515" is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o17514" name="floret" name_original="florets" src="d0_s13" type="structure" />
      <biological_entity id="o17515" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity constraint="lateral" id="o17516" name="lobe" name_original="lobes" src="d0_s13" type="structure" />
      <relation from="o17513" id="r2945" name="exceeded by" negation="false" src="d0_s13" to="o17514" />
      <relation from="o17513" id="r2946" modifier="sometimes" name="from" negation="false" src="d0_s13" to="o17516" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3-veined, central veins and sometimes the lateral-veins extended into awns, central awns always the longest;</text>
      <biological_entity id="o17517" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="central" id="o17518" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character constraint="into awns" constraintid="o17520" is_modifier="false" name="size" src="d0_s14" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="central" id="o17519" name="lateral-vein" name_original="lateral-veins" src="d0_s14" type="structure">
        <character constraint="into awns" constraintid="o17520" is_modifier="false" name="size" src="d0_s14" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o17520" name="awn" name_original="awns" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o17521" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" name="length" src="d0_s14" value="longest" value_original="longest" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas almost as long as the lemmas, 2-keeled, 2-awned;</text>
      <biological_entity id="o17522" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="2-awned" value_original="2-awned" />
      </biological_entity>
      <biological_entity id="o17523" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <relation from="o17522" id="r2947" name="as long as" negation="false" src="d0_s15" to="o17523" />
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2;</text>
      <biological_entity id="o17524" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3;</text>
      <biological_entity id="o17525" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 2.</text>
      <biological_entity id="o17526" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses about twice as large as the embryos;</text>
      <biological_entity id="o17527" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character constraint="embryo" constraintid="o17528" is_modifier="false" name="size_or_quantity" src="d0_s19" value="2 times as large as the embryos" />
      </biological_entity>
      <biological_entity id="o17528" name="embryo" name_original="embryos" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>hila punctate, x = 10.</text>
      <biological_entity id="o17529" name="hilum" name_original="hila" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s20" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="x" id="o17530" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aegopogon is an American genus of four species that extends from the southwestern United States to Peru, Bolivia, and northern Argentina. One species is native in the Flora region.</discussion>
  <references>
    <reference>Beetle, A.A. 1948. The genus Aegopogon Humbold. &amp; Bonpl. Univ. Wyoming Publ. 13:17-23.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>