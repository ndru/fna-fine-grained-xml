<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Reeder;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Reeder" date="unknown" rank="tribe">ORCUTTIEAE</taxon_name>
    <taxon_name authority="Reeder" date="unknown" rank="genus">TUCTORIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe orcuttieae;genus tuctoria</taxon_hierarchy>
  </taxon_identification>
  <number>19.02</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>viscid-aromatic, more or less hairy throughout, not producing juvenile floating leaves.</text>
      <biological_entity id="o11934" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="odor" src="d0_s1" value="viscid-aromatic" value_original="viscid-aromatic" />
        <character is_modifier="false" modifier="more or less; throughout" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11935" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="juvenile" value_original="juvenile" />
        <character is_modifier="true" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
      <relation from="o11934" id="r1959" name="producing" negation="true" src="d0_s1" to="o11935" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-15 (30) cm, simple or branching at the upper nodes, erect or ascending, often rather fragile, readily breaking apart at the nodes.</text>
      <biological_entity id="o11936" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="at upper nodes" constraintid="o11937" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often rather" name="fragility" src="d0_s2" value="fragile" value_original="fragile" />
        <character constraint="at nodes" constraintid="o11938" is_modifier="false" modifier="readily" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity constraint="upper" id="o11937" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o11938" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves without ligules, with little or no distinction between sheath and blade;</text>
      <biological_entity id="o11939" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="between sheath and blade" constraintid="o11941-o11942" id="o11940" name="ligule" name_original="ligules" src="d0_s3" type="structure" constraint_original="between  sheath and  blade, " />
      <biological_entity id="o11941" name="sheath" name_original="sheath" src="d0_s3" type="structure" />
      <biological_entity id="o11942" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o11939" id="r1960" name="without" negation="false" src="d0_s3" to="o11940" />
    </statement>
    <statement id="d0_s4">
      <text>blades flat, becoming involute when dry.</text>
      <biological_entity id="o11943" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when dry" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, clavate spikes, partially included or exserted at maturity, spikelets spirally arranged;</text>
      <biological_entity id="o11944" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o11945" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="clavate" value_original="clavate" />
        <character is_modifier="false" modifier="partially" name="position" src="d0_s5" value="included" value_original="included" />
        <character constraint="at maturity" is_modifier="false" name="position" src="d0_s5" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>disarticulation tardy, above the glumes and between the florets.</text>
      <biological_entity id="o11946" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o11947" name="floret" name_original="florets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets laterally compressed, with 5-40 florets.</text>
      <biological_entity id="o11948" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o11949" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
      <relation from="o11948" id="r1961" name="with" negation="false" src="d0_s7" to="o11949" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes irregularly short-toothed or entire;</text>
      <biological_entity id="o11950" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s8" value="short-toothed" value_original="short-toothed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas (3) 4-7 mm, 11-17-veined, not translucent between the veins, entire or denticulate, usually with a central mucro;</text>
      <biological_entity id="o11951" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="11-17-veined" value_original="11-17-veined" />
        <character constraint="between veins" constraintid="o11952" is_modifier="false" modifier="not" name="coloration_or_reflectance" src="d0_s9" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o11952" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity constraint="central" id="o11953" name="mucro" name_original="mucro" src="d0_s9" type="structure" />
      <relation from="o11951" id="r1962" modifier="usually" name="with" negation="false" src="d0_s9" to="o11953" />
    </statement>
    <statement id="d0_s10">
      <text>paleas subequal to or slightly shorter than the lemmas;</text>
      <biological_entity id="o11954" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="than the lemmas" constraintid="o11955" from="subequal" name="size" src="d0_s10" to="or slightly shorter" value="subequal to or slightly shorter" />
      </biological_entity>
      <biological_entity id="o11955" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lodicules 2, 0.1-0.5 mm, sometimes fused to the paleas;</text>
      <biological_entity id="o11956" name="lodicule" name_original="lodicules" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
        <character constraint="to paleas" constraintid="o11957" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s11" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o11957" name="palea" name_original="paleas" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers 3, exserted on long, slender, ribbonlike filaments at anthesis;</text>
      <biological_entity id="o11958" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o11959" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="true" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="ribbonlike" value_original="ribbonlike" />
      </biological_entity>
      <relation from="o11958" id="r1963" name="exserted on" negation="false" src="d0_s12" to="o11959" />
    </statement>
    <statement id="d0_s13">
      <text>styles 2, apical, long, filiform, stigmatic for 1/3 – 1/2 of their length;</text>
      <biological_entity id="o11960" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s13" value="apical" value_original="apical" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform" value_original="filiform" />
        <character constraint="for" is_modifier="false" name="structure_in_adjective_form" src="d0_s13" value="stigmatic" value_original="stigmatic" />
        <character char_type="range_value" from="1/3" name="length" src="d0_s13" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmatic hairs short, often sparse.</text>
      <biological_entity constraint="stigmatic" id="o11961" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" modifier="often" name="count_or_density" src="d0_s14" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses laterally compressed, pyriform to oblong, pericarp not viscid;</text>
      <biological_entity id="o11962" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="pyriform" name="shape" src="d0_s15" to="oblong" />
      </biological_entity>
      <biological_entity id="o11963" name="pericarp" name_original="pericarp" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="coating" src="d0_s15" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryos visible through the pericarp, brown, from 3/4 as long as to nearly equaling the caryopses;</text>
      <biological_entity id="o11965" name="pericarp" name_original="pericarp" src="d0_s16" type="structure" />
      <biological_entity id="o11966" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3/4" value_original="3/4" />
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s16" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o11964" id="r1964" name="from" negation="false" src="d0_s16" to="o11966" />
    </statement>
    <statement id="d0_s17">
      <text>epiblasts present, x = 10.</text>
      <biological_entity id="o11964" name="embryo" name_original="embryos" src="d0_s16" type="structure">
        <character constraint="through pericarp" constraintid="o11965" is_modifier="false" name="prominence" src="d0_s16" value="visible" value_original="visible" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o11967" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tuctoria has three species, all of which grow in vernal pools or similar habitats, two in the Central Valley of California and one T. fragilis (Swallen) Reeder, in Baja California Sur, Mexico. Both species found in the Flora region are endangered by loss of habitat to urbanization and agriculture.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikes exserted from the upper leaf sheaths at maturity; lemmas more or less truncate; caryopses about 2 mm long, minutely rugose</description>
      <determination>1 Tuctoria greenei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikes partially included in the upper leaf sheaths at maturity; lemmas tapering gradually to a mucronate apex; caryopses about 3 mm long, smooth</description>
      <determination>2 Tuctoria mucronata</determination>
    </key_statement>
  </key>
</bio:treatment>