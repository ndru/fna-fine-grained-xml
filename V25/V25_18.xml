<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Burmeist." date="unknown" rank="subfamily">ARUNDINOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ARUNDINEAE</taxon_name>
    <taxon_name authority="Makino ex Honda" date="unknown" rank="genus">HAKONECHLOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily arundinoideae;tribe arundineae;genus hakonechloa</taxon_hierarchy>
  </taxon_identification>
  <number>16.02</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely cespitose, rhizomatous and stoloniferous.</text>
      <biological_entity id="o12769" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-90 cm, erect or geniculate at the base.</text>
      <biological_entity id="o12770" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o12771" is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o12771" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o12772" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o12773" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial ligules present, composed of a line of hairs across the collar;</text>
      <biological_entity constraint="abaxial" id="o12774" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12775" name="line" name_original="line" src="d0_s5" type="structure" />
      <biological_entity id="o12776" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o12777" name="collar" name_original="collar" src="d0_s5" type="structure" />
      <relation from="o12774" id="r2093" name="composed of a" negation="false" src="d0_s5" to="o12775" />
      <relation from="o12774" id="r2094" name="composed of a" negation="false" src="d0_s5" to="o12776" />
      <relation from="o12774" id="r2095" name="composed of a" negation="false" src="d0_s5" to="o12777" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial ligules membranous and sparsely ciliate, sometimes lacerate, cilia subequal to the base;</text>
      <biological_entity constraint="adaxial" id="o12778" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o12779" name="cilium" name_original="cilia" src="d0_s6" type="structure">
        <character constraint="to base" constraintid="o12780" is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o12780" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blades flat, linear-lanceolate, resupinate, in living plants the glaucous-green adaxial surface facing downwards and the bright green abaxial surface facing upwards.</text>
      <biological_entity id="o12781" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
      </biological_entity>
      <biological_entity id="o12782" name="plant" name_original="plants" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="living" value_original="living" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12783" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="glaucous-green" value_original="glaucous-green" />
        <character is_modifier="false" name="position_relational" src="d0_s7" value="facing" value_original="facing" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12784" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="bright green" value_original="bright green" />
      </biological_entity>
      <relation from="o12781" id="r2096" name="in" negation="false" src="d0_s7" to="o12782" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles not plumose.</text>
      <biological_entity id="o12785" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets pedicellate, somewhat laterally compressed, with 5-10 florets;</text>
      <biological_entity id="o12786" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="somewhat laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o12787" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <relation from="o12786" id="r2097" name="with" negation="false" src="d0_s9" to="o12787" />
    </statement>
    <statement id="d0_s10">
      <text>rachilla segments conspicuously pilose;</text>
    </statement>
    <statement id="d0_s11">
      <text>disarticulation at the base of the rachilla segment and below each spikelet.</text>
      <biological_entity constraint="rachilla" id="o12788" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o12789" name="spikelet" name_original="spikelet" src="d0_s11" type="structure" />
      <relation from="o12788" id="r2098" name="at the base of the rachilla segment and below" negation="false" src="d0_s11" to="o12789" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, lanceolate, unawned;</text>
      <biological_entity id="o12790" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 1.5-2 mm, strigose, hairs 1-1.5 mm;</text>
      <biological_entity id="o12791" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o12792" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas chartaceous, 3-veined, margins with papillose-based hairs near the base, apices inconspicuously bidentate, awned from between the teeth;</text>
      <biological_entity id="o12793" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s14" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o12794" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o12795" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o12796" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o12797" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="shape" src="d0_s14" value="bidentate" value_original="bidentate" />
        <character constraint="between teeth" constraintid="o12798" is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o12798" name="tooth" name_original="teeth" src="d0_s14" type="structure" />
      <relation from="o12794" id="r2099" name="with" negation="false" src="d0_s14" to="o12795" />
      <relation from="o12795" id="r2100" name="near" negation="false" src="d0_s14" to="o12796" />
    </statement>
    <statement id="d0_s15">
      <text>awns 3-5 mm, straight;</text>
      <biological_entity id="o12799" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2-keeled.</text>
      <biological_entity id="o12800" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses glabrous, x = 10.</text>
      <biological_entity id="o12801" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o12802" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hakonechloa is a monotypic genus, endemic to Japan, but grown as an ornamental in the Flora region. The resupination of the blades is not evident on herbarium specimens.</discussion>
  <references>
    <reference>Koyama, T. 1987. Grasses of Japan and Its Neighboring Regions: An Identification Manual. Kodansha, Ltd., Tokyo, Japan. 370 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>