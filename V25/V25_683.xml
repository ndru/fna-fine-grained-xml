<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">131</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">nealleyi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species nealleyi</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Gypgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, bases hard and knotty, not rhizomatous.</text>
      <biological_entity id="o7042" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7043" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-50 (60) cm tall, 0.7-1.2 mm thick.</text>
      <biological_entity id="o7044" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="cm" value="60" value_original="60" />
        <character char_type="range_value" from="10" from_unit="cm" name="height" src="d0_s2" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="thickness" src="d0_s2" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths rounded below, occasionally glabrous, usually villous to tomentose along the margins and back, with soft, kinky hairs to 4 mm;</text>
      <biological_entity id="o7045" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="below" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" constraint="along margins; along the margins and back" constraintid="o7047" from="usually villous" name="pubescence" src="d0_s3" to="tomentose" />
      </biological_entity>
      <biological_entity id="o7046" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o7047" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o7048" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o7045" id="r1134" name="with" negation="false" src="d0_s3" to="o7048" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o7049" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (0.6) 1.5-6 (7) cm long, 1-1.5 mm wide, involute, stiff, spreading at right angles to the culm, glabrous abaxially, scabridulous adaxially, margins smooth.</text>
      <biological_entity id="o7050" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="0.6" value_original="0.6" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
        <character constraint="at angles" constraintid="o7051" is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o7051" name="angle" name_original="angles" src="d0_s5" type="structure" />
      <biological_entity id="o7052" name="culm" name_original="culm" src="d0_s5" type="structure" />
      <biological_entity id="o7053" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o7051" id="r1135" name="to" negation="false" src="d0_s5" to="o7052" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 3-10 cm long, (0.3) 1-5 (6) cm wide, longer than wide, ultimately open, subovate, lower portion often included in the uppermost sheath;</text>
      <biological_entity id="o7054" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer than wide" value_original="longer than wide" />
        <character is_modifier="false" modifier="ultimately" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subovate" value_original="subovate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7055" name="portion" name_original="portion" src="d0_s6" type="structure" />
      <biological_entity constraint="uppermost" id="o7056" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
      <relation from="o7055" id="r1136" modifier="often" name="included in the" negation="false" src="d0_s6" to="o7056" />
    </statement>
    <statement id="d0_s7">
      <text>lower nodes with 1-2 branches;</text>
      <biological_entity constraint="lower" id="o7057" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o7058" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <relation from="o7057" id="r1137" name="with" negation="false" src="d0_s7" to="o7058" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-5 cm, appressed or spreading to 90° from the rachis;</text>
      <biological_entity constraint="primary" id="o7059" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character constraint="from rachis" constraintid="o7060" is_modifier="false" modifier="90°" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o7060" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches appressed or spreading, without spikelets on the lower 1/8 – 1/4;</text>
      <biological_entity constraint="secondary" id="o7061" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o7062" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity constraint="lower" id="o7063" name="1/8-1/4" name_original="1/8-1/4" src="d0_s9" type="structure" />
      <relation from="o7061" id="r1138" name="without" negation="false" src="d0_s9" to="o7062" />
      <relation from="o7062" id="r1139" name="on" negation="false" src="d0_s9" to="o7063" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.2-2 mm, appressed or spreading.</text>
      <biological_entity id="o7064" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 1.4-2.1 mm, purplish.</text>
      <biological_entity id="o7065" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s11" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, linear-lanceolate to ovate, membranous;</text>
      <biological_entity id="o7066" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 0.5-1.1 mm;</text>
      <biological_entity constraint="lower" id="o7067" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.3-2 mm, from slightly shorter than to subequal to the florets;</text>
      <biological_entity constraint="upper" id="o7068" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character constraint="than to subequal to the florets" constraintid="o7069" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o7069" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.4-2.1 mm, ovate, membranous, glabrous, acute;</text>
      <biological_entity id="o7070" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.4-2.1 mm, ovate, membranous;</text>
      <biological_entity id="o7071" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.7-1 mm, purplish.</text>
      <biological_entity id="o7072" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits 0.7-1 mm, orangish to whitish.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o7073" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s18" to="1" to_unit="mm" />
        <character char_type="range_value" from="orangish" name="coloration" src="d0_s18" to="whitish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7074" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sporobolus nealleyi grows in sandy and gravelly soils, usually in those derived from gypsum, or near alkaline habitats associated with desert grasslands. It is known only from the southwestern United States, where it grows at 700-3000 m.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Colo.;N.Mex.;Tex.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>