<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">612</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">SACCHARUM</taxon_name>
    <taxon_name authority="(Michx.) Pers." date="unknown" rank="species">brevibarbe</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus saccharum;species brevibarbe</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Shortbeard plumegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous.</text>
      <biological_entity id="o896" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 0.8-2.5 m;</text>
      <biological_entity id="o897" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.8" from_unit="m" name="some_measurement" src="d0_s1" to="2.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous or pubescent.</text>
      <biological_entity id="o898" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths not ciliate;</text>
      <biological_entity id="o899" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o900" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o901" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades usually 40-60 cm long, 7-25 mm wide, glabrous.</text>
      <biological_entity id="o902" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="length" src="d0_s6" to="60" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 45-75 cm, usually glabrous, occasionally pubescent or minutely pilose;</text>
      <biological_entity id="o903" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s7" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>panicles 3-10 cm wide, linear or oblong;</text>
      <biological_entity id="o904" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachises (10) 30-50 cm, glabrous or sparsely pilose;</text>
      <biological_entity id="o905" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s9" to="50" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lowest nodes glabrous or sparsely pilose;</text>
      <biological_entity constraint="lowest" id="o906" name="node" name_original="nodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>primary branches 7-14 cm, appressed;</text>
    </statement>
    <statement id="d0_s12">
      <text>rame internodes 4-6 mm, with hairs.</text>
      <biological_entity constraint="primary" id="o907" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s11" to="14" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o908" name="internode" name_original="internodes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o909" name="hair" name_original="hairs" src="d0_s12" type="structure" />
      <relation from="o908" id="r157" name="with" negation="false" src="d0_s12" to="o909" />
    </statement>
    <statement id="d0_s13">
      <text>Sessile spikelets 6.5-10.5 mm long, 1.2-1.5 mm wide, purple or straw-colored.</text>
      <biological_entity id="o910" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s13" to="10.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Callus hairs 3-7 mm, from shorter than to equaling the spikelets, white to straw-colored or brown;</text>
      <biological_entity constraint="callus" id="o911" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character constraint="than to equaling the spikelets" constraintid="o912" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="white" modifier="from" name="coloration" src="d0_s14" to="straw-colored or brown" />
      </biological_entity>
      <biological_entity id="o912" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 5-veined, smooth basally, scabrous distally;</text>
      <biological_entity constraint="lower" id="o913" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 5.5-8 mm, not or indistinctly veined, initially entire, sometimes becoming bifid, teeth 2-2.5 mm;</text>
      <biological_entity constraint="lower" id="o914" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not; indistinctly" name="architecture" src="d0_s16" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="initially" name="architecture_or_shape" src="d0_s16" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes becoming" name="architecture_or_shape" src="d0_s16" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o915" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas 5.5-8 mm, 0.9-1 times as long as the lower lemmas, 3-veined, entire or bifid;</text>
      <biological_entity constraint="upper" id="o916" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
        <character constraint="lemma" constraintid="o917" is_modifier="false" name="length" src="d0_s17" value="0.9-1 times as long as the lower lemmas" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity constraint="lower" id="o917" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>awns 10-22 mm, always flattened below, sometimes spirally coiled;</text>
      <biological_entity id="o918" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s18" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="always; below" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="sometimes spirally" name="architecture" src="d0_s18" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>lodicule veins sometimes extending as hairlike projec¬tions;</text>
      <biological_entity constraint="lodicule" id="o919" name="vein" name_original="veins" src="d0_s19" type="structure" />
      <biological_entity id="o920" name="¬" name_original="¬" src="d0_s19" type="structure">
        <character is_modifier="true" name="shape" src="d0_s19" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o919" id="r158" name="extending as" negation="false" src="d0_s19" to="o920" />
    </statement>
    <statement id="d0_s20">
      <text>anthers 2.</text>
      <biological_entity id="o921" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Pedicels 3-4 mm, with hairs.</text>
      <biological_entity id="o922" name="pedicel" name_original="pedicels" src="d0_s21" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s21" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o923" name="hair" name_original="hairs" src="d0_s21" type="structure" />
      <relation from="o922" id="r159" name="with" negation="false" src="d0_s21" to="o923" />
    </statement>
    <statement id="d0_s22">
      <text>Pedicellate spikelets similar to the sessile spikelets.</text>
      <biological_entity id="o925" name="spikelet" name_original="spikelets" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o924" id="r160" name="to" negation="false" src="d0_s22" to="o925" />
    </statement>
    <statement id="d0_s23">
      <text>2n = 60.</text>
      <biological_entity id="o924" name="spikelet" name_original="spikelets" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o926" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Saccharum brevibarbe grows only in the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Tenn.;Okla.;Miss.;Tex.;La.;Del.;Ala.;N.C.;S.C.;Va.;Ark.;Ill.;Ga.;Ky.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Awns 15-22 mm long, straight or sinuous at the base; upper lemmas of the sessile spikelets entire at maturity</description>
      <determination>Saccharum brevibarbe var. brevibarbe</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Awns 10-18 mm long, spirally coiled at the base, usually with 2-4 coils; upper lemmas of the sessile spikelets bifid at maturity, teeth about 2-2.5 mm long</description>
      <determination>Saccharum brevibarbe var. contortum</determination>
    </key_statement>
  </key>
</bio:treatment>