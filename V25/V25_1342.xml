<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">ERIOCHLOA</taxon_name>
    <taxon_name authority="Vasey &amp; Scribn." date="unknown" rank="species">lemmonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus eriochloa;species lemmonii</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Canyon cupgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o23324" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-80 cm, erect or decumbent, sometimes rooting at the lower nodes;</text>
      <biological_entity id="o23325" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o23326" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23326" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes densely pubescent to pilose;</text>
      <biological_entity id="o23327" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="densely pubescent" name="pubescence" src="d0_s3" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes pubescent to pilose.</text>
      <biological_entity id="o23328" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s4" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths from conspicuously inflated to not inflated, glabrous or pubescent to pilose;</text>
      <biological_entity id="o23329" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character char_type="range_value" from="conspicuously inflated" name="shape" src="d0_s5" to="not inflated" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o23330" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 5-15 cm long, 6-20 mm wide, lanceolate, flat, straight, diverging or ascending, velvety pubescent adaxially.</text>
      <biological_entity id="o23331" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="velvety" value_original="velvety" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 5-15 cm long, 0.5-4 cm wide, spreading or contracted;</text>
      <biological_entity id="o23332" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachises hairy;</text>
      <biological_entity id="o23333" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches (2) 3-8 (10), 1-4 cm long, 0.4-0.6 mm wide, appressed or reflexed and spreading, velvety pubescent, not winged, with 10-14 spikelets, spikelets in unequally pedicellate pairs at the middle of the branches, solitary distally;</text>
      <biological_entity id="o23334" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character name="atypical_quantity" src="d0_s10" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s10" value="10" value_original="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="8" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s10" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="velvety" value_original="velvety" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o23335" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s10" to="14" />
      </biological_entity>
      <biological_entity id="o23336" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_arrangement_or_growth_form" notes="" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o23337" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o23338" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o23339" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <relation from="o23334" id="r3960" name="with" negation="false" src="d0_s10" to="o23335" />
      <relation from="o23336" id="r3961" name="in" negation="false" src="d0_s10" to="o23337" />
      <relation from="o23337" id="r3962" name="at" negation="false" src="d0_s10" to="o23338" />
      <relation from="o23338" id="r3963" name="part_of" negation="false" src="d0_s10" to="o23339" />
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.5-1 mm, pilose, apices hairy or glabrous.</text>
      <biological_entity id="o23340" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o23341" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3-4.5 (4.9) mm long, 1.2-1.7 mm wide, elliptic.</text>
      <biological_entity id="o23342" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="4.9" value_original="4.9" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Upper glumes equaling the lower lemmas, nearly glabrous or sparsely to densely pilose, elliptic, 5-7-veined, acute, unawned;</text>
      <biological_entity constraint="upper" id="o23343" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="nearly glabrous or" name="pubescence" notes="" src="d0_s13" to="sparsely densely pilose" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-7-veined" value_original="5-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23344" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower lemmas 2.7-4 mm long, 1.2-1.7 mm wide, elliptic, setose to pilose, 5-veined, acute, unawned;</text>
      <biological_entity constraint="lower" id="o23345" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="setose" name="pubescence" src="d0_s14" to="pilose" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower paleas 1-4 mm, hyaline;</text>
      <biological_entity constraint="lower" id="o23346" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers absent or 3;</text>
      <biological_entity id="o23347" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas 2.3-3.3 mm, elliptic, indurate, dull, rough, occasionally with a few long hairs, acute to rounded, sometimes mucronate;</text>
      <biological_entity constraint="upper" id="o23348" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s17" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="texture" src="d0_s17" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s17" value="rough" value_original="rough" />
        <character char_type="range_value" from="acute" name="shape" notes="" src="d0_s17" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s17" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o23349" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s17" value="long" value_original="long" />
      </biological_entity>
      <relation from="o23348" id="r3964" modifier="occasionally" name="with" negation="false" src="d0_s17" to="o23349" />
    </statement>
    <statement id="d0_s18">
      <text>upper paleas indurate.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 36.</text>
      <biological_entity constraint="upper" id="o23350" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" name="texture" src="d0_s18" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23351" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eriochloa lemmonii, a rare species, grows in canyons and on rocky slopes in Pima County, Arizona, Hidalgo County, New Mexico, and adjacent Mexico. The record from Tennessee reflects an introduction. It is not known if the species has persisted in the region.</discussion>
  <discussion>Eriochloa lemmonii may hybridize with E. acuminata, from which it differs in the frequent presence of lower paleas, raised veins of the upper glumes and lower lemmas, broad, velvety pubescent leaf blades, and blunt spikelets. Reports of E. lemmonii from Texas may be based on hybrids between the two species.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>