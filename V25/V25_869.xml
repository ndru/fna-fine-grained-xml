<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Bouteloua</taxon_name>
    <taxon_name authority="(Michx.) Torr." date="unknown" rank="species">curtipendula</taxon_name>
    <taxon_name authority="Gould &amp; Kapadia" date="unknown" rank="variety">caespitosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus bouteloua;species curtipendula;variety caespitosa</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, often with a knotty base, not or shortly rhizomatous.</text>
      <biological_entity id="o20305" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not; shortly" name="architecture" notes="" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20306" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
      </biological_entity>
      <relation from="o20305" id="r3453" modifier="often" name="with" negation="false" src="d0_s0" to="o20306" />
    </statement>
    <statement id="d0_s1">
      <text>Culms in large or small clumps, stiffly erect.</text>
      <biological_entity id="o20307" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o20308" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o20307" id="r3454" name="in" negation="false" src="d0_s1" to="o20308" />
    </statement>
    <statement id="d0_s2">
      <text>Blades usually narrow, but at least some over 2.5 mm wide.</text>
      <biological_entity id="o20309" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
        <character modifier="at least" name="width" src="d0_s2" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicles with 12-80 branches, averaging 2-7 spikelets per branch.</text>
      <biological_entity id="o20310" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <biological_entity id="o20311" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s3" to="80" />
      </biological_entity>
      <biological_entity id="o20312" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o20313" name="branch" name_original="branch" src="d0_s3" type="structure" />
      <relation from="o20310" id="r3455" name="with" negation="false" src="d0_s3" to="o20311" />
      <relation from="o20310" id="r3456" name="averaging" negation="false" src="d0_s3" to="o20312" />
      <relation from="o20310" id="r3457" name="per" negation="false" src="d0_s3" to="o20313" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes and lemmas bronze or stramineous to green, or various shades of purple;</text>
      <biological_entity id="o20314" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s4" to="green" />
        <character is_modifier="false" name="variability" src="d0_s4" value="various" value_original="various" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o20315" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s4" to="green" />
        <character is_modifier="false" name="variability" src="d0_s4" value="various" value_original="various" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers usually yellow or orange, occasionally red or purple.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 58-103.</text>
      <biological_entity id="o20316" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20317" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="58" name="quantity" src="d0_s6" to="103" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua curtipendula var. caespitosa grows on loose, sandy or rocky, well drained limestone soils at 200-2500 m in the southwestern United States, Mexico, and South America. It frequently grows, and may hybridize, with B. warnockii.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Calif.;Colo.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>