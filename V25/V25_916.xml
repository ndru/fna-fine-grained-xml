<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">HILARIA</taxon_name>
    <taxon_name authority="(Torr.) Benth." date="unknown" rank="species">jamesii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus hilaria;species jamesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pleuraphis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">jamesii</taxon_name>
    <taxon_hierarchy>genus pleuraphis;species jamesii</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Galleta</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>strongly rhizomatous or stoloniferous.</text>
      <biological_entity id="o27276" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-65 cm, erect, bases much branched;</text>
      <biological_entity id="o27277" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o27278" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes usually pilose or villous, sometimes glabrous;</text>
      <biological_entity id="o27279" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower internodes glabrous.</text>
      <biological_entity constraint="lower" id="o27280" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths glabrous, sometimes slightly scabrous;</text>
      <biological_entity id="o27281" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes slightly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>collars pilose at the edges;</text>
      <biological_entity id="o27282" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character constraint="at edges" constraintid="o27283" is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27283" name="edge" name_original="edges" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>ligules 1.5-5 mm, often laciniate;</text>
      <biological_entity id="o27284" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="laciniate" value_original="laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades 2-20 cm long, 2-4 mm wide, involute and curled when dry, sparsely villous behind the ligules, abaxial surfaces scabridulous, adaxial surfaces scabrous.</text>
      <biological_entity id="o27285" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s8" value="curled" value_original="curled" />
        <character constraint="behind ligules" constraintid="o27286" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27286" name="ligule" name_original="ligules" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o27287" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27288" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles 2-6 cm;</text>
    </statement>
    <statement id="d0_s10">
      <text>fascicles 6-8 mm.</text>
      <biological_entity id="o27289" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lateral spikelets with 3 staminate florets;</text>
      <biological_entity constraint="lateral" id="o27290" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o27291" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o27290" id="r4640" name="with" negation="false" src="d0_s11" to="o27291" />
    </statement>
    <statement id="d0_s12">
      <text>glumes thin, membranous, lanceolate or parallel-sided, not conspicuously fused at the base, apices acute to rounded, often ciliate, veins rarely excurrent;</text>
      <biological_entity id="o27292" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="parallel-sided" value_original="parallel-sided" />
        <character constraint="at base" constraintid="o27293" is_modifier="false" modifier="not conspicuously" name="fusion" src="d0_s12" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o27293" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o27294" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="rounded" />
        <character is_modifier="false" modifier="often" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27295" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s12" value="excurrent" value_original="excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes dorsally awned, awns exceeding the apices;</text>
      <biological_entity constraint="lower" id="o27296" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="dorsally" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o27297" name="awn" name_original="awns" src="d0_s13" type="structure" />
      <biological_entity id="o27298" name="apex" name_original="apices" src="d0_s13" type="structure" />
      <relation from="o27297" id="r4641" name="exceeding the" negation="false" src="d0_s13" to="o27298" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 3, about 5 mm.</text>
      <biological_entity id="o27299" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Central spikelets with 1 bisexual floret;</text>
      <biological_entity constraint="central" id="o27300" name="spikelet" name_original="spikelets" src="d0_s15" type="structure" />
      <biological_entity id="o27301" name="floret" name_original="floret" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o27300" id="r4642" name="with" negation="false" src="d0_s15" to="o27301" />
    </statement>
    <statement id="d0_s16">
      <text>glumes with excurrent veins forming distinct awns;</text>
      <biological_entity id="o27302" name="glume" name_original="glumes" src="d0_s16" type="structure" />
      <biological_entity id="o27303" name="vein" name_original="veins" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o27304" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o27302" id="r4643" name="with" negation="false" src="d0_s16" to="o27303" />
      <relation from="o27303" id="r4644" name="forming" negation="false" src="d0_s16" to="o27304" />
    </statement>
    <statement id="d0_s17">
      <text>lemmas exceeding the glumes, ciliate, the midveins sometimes excurrent.</text>
      <biological_entity id="o27305" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s17" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27306" name="glume" name_original="glumes" src="d0_s17" type="structure" />
      <relation from="o27305" id="r4645" name="exceeding the" negation="false" src="d0_s17" to="o27306" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18, 36.</text>
      <biological_entity id="o27307" name="midvein" name_original="midveins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s17" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27308" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hilaria jamesii is endemic to the southwestern United States, and grows in deserts, canyons, and dry plains. It has medium grazing value but low palatability. It is usually less pubescent than H. rigida, the difference being most marked on the lower cauline nodes.</discussion>
  
</bio:treatment>