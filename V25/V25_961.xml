<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">302</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="DC." date="unknown" rank="genus">DANTHONIA</taxon_name>
    <taxon_name authority="(L.) DC." date="unknown" rank="species">decumbens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus danthonia;species decumbens</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sieglingia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">decumbens</taxon_name>
    <taxon_hierarchy>genus sieglingia;species decumbens</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Mountain heath-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 8-60 cm, usually erect, sometimes decumbent, not disarticulating.</text>
      <biological_entity id="o3488" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous or pilose;</text>
      <biological_entity id="o3489" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 5-15 cm long, 0.5-4 mm wide, usually flat, glabrous or sparsely pilose.</text>
      <biological_entity id="o3490" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences with up to 15 spikelets;</text>
      <biological_entity id="o3491" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o3492" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <relation from="o3491" id="r566" name="with" negation="false" src="d0_s3" to="o3492" />
    </statement>
    <statement id="d0_s4">
      <text>branches erect;</text>
      <biological_entity id="o3493" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower branches with 1-3 spikelets.</text>
      <biological_entity constraint="lower" id="o3494" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o3495" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <relation from="o3494" id="r567" name="with" negation="false" src="d0_s5" to="o3495" />
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 6-15 mm;</text>
      <biological_entity id="o3496" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>florets usually cleistogamous, rarely chasmogamous.</text>
      <biological_entity id="o3497" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s7" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s7" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calluses of middle florets from as long as to a little longer than wide, convex abaxially;</text>
      <biological_entity id="o3498" name="callus" name_original="calluses" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="than from as-long-as to a little longer wide" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="middle" id="o3499" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o3498" id="r568" name="part_of" negation="false" src="d0_s8" to="o3499" />
    </statement>
    <statement id="d0_s9">
      <text>lemma bodies 5-6 mm, margins glabrous or pubescent for most of their length, scabrous apically, apices with acute teeth, teeth often scabrous, sometimes scabridulous, mucronate, not awned, from between the teeth;</text>
      <biological_entity constraint="lemma" id="o3500" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3501" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="apically" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3502" name="apex" name_original="apices" src="d0_s9" type="structure" />
      <biological_entity id="o3503" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o3504" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s9" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o3505" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
      <relation from="o3502" id="r569" name="with" negation="false" src="d0_s9" to="o3503" />
      <relation from="o3504" id="r570" name="from" negation="false" src="d0_s9" to="o3505" />
    </statement>
    <statement id="d0_s10">
      <text>palea veins swollen at the base, forming pulvini;</text>
      <biological_entity constraint="palea" id="o3506" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o3507" is_modifier="false" name="shape" src="d0_s10" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o3507" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o3508" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure" />
      <relation from="o3506" id="r571" name="forming" negation="false" src="d0_s10" to="o3508" />
    </statement>
    <statement id="d0_s11">
      <text>anthers of the cleistogamous florets 0.2-0.4 mm, those of the chasmogamous florets about 2 mm.</text>
      <biological_entity id="o3509" name="anther" name_original="anthers" src="d0_s11" type="structure" constraint="floret" constraint_original="floret; floret">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3510" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o3511" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <relation from="o3509" id="r572" name="part_of" negation="false" src="d0_s11" to="o3510" />
      <relation from="o3509" id="r573" name="part_of" negation="false" src="d0_s11" to="o3511" />
    </statement>
    <statement id="d0_s12">
      <text>Caryopses 2.1-2.5 mm long, 1.1-1.8 mm wide.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 24, 36, 124.</text>
      <biological_entity id="o3512" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3513" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit=",,1" value="24" value_original="24" />
        <character name="quantity" src="d0_s13" unit=",,1" value="36" value_original="36" />
        <character name="quantity" src="d0_s13" unit=",,1" value="124" value_original="124" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Danthonia decumbens grows throughout most of Europe, the Caucasus, and northern Turkey, and is now established on the west and east coasts of North America. It grows in heathlands, sandy or rocky meadows, clearings, and sometimes along roadsides. The species is sometimes placed in the monotypic genus Sieglingia, as Sieglingia decumbens (L.) Bernh.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.;Nfld. and Labr. (Labr.);N.S.;Calif.;Oreg.;Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>