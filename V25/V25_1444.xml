<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">572</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">laeve</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species laeve</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longipilum</taxon_name>
    <taxon_hierarchy>genus paspalum;species longipilum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laeve</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pilosum</taxon_name>
    <taxon_hierarchy>genus paspalum;species laeve;variety pilosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laeve</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">circulare</taxon_name>
    <taxon_hierarchy>genus paspalum;species laeve;variety circulare</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laeve</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">australe</taxon_name>
    <taxon_hierarchy>genus paspalum;species laeve;variety australe</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">circulare</taxon_name>
    <taxon_hierarchy>genus paspalum;species circulare</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Field paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>shortly rhizomatous.</text>
      <biological_entity id="o12149" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-120 cm, erect;</text>
      <biological_entity id="o12150" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or pubescent.</text>
      <biological_entity id="o12151" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o12152" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1.5-3.8 mm;</text>
      <biological_entity id="o12153" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 37 cm long, 2-9.3 mm wide, flat, glabrous or pubescent.</text>
      <biological_entity id="o12154" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="37" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="9.3" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 1-6 racemosely arranged branches;</text>
      <biological_entity id="o12155" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o12156" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="6" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o12155" id="r2003" name="with" negation="false" src="d0_s7" to="o12156" />
    </statement>
    <statement id="d0_s8">
      <text>branches 2-10.9 cm, diverging to spreading (rarely erect), persistent;</text>
      <biological_entity id="o12157" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="10.9" to_unit="cm" />
        <character char_type="range_value" from="diverging" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.6-1.3 mm wide, glabrous, margins scabrous, terminating in a spikelet.</text>
      <biological_entity constraint="branch" id="o12158" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12159" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12160" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o12159" id="r2004" name="terminating in" negation="false" src="d0_s9" to="o12160" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.3-3.3 mm long, 2-2.7 mm wide, solitary, appressed to the branch axes, elliptic to obovate or nearly orbicular, glabrous, stramineous.</text>
      <biological_entity id="o12161" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s10" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character constraint="to branch axes" constraintid="o12162" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s10" to="obovate or nearly orbicular" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="branch" id="o12162" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o12163" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3-veined, lower lemmas 5-veined;</text>
      <biological_entity constraint="upper" id="o12164" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="lower" id="o12165" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper florets pale to stramineous.</text>
      <biological_entity constraint="upper" id="o12166" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s13" to="stramineous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses about 2 mm, white to yellowbrown.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20, 58, 70, 80.</text>
      <biological_entity id="o12167" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="yellowbrown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12168" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
        <character name="quantity" src="d0_s15" value="58" value_original="58" />
        <character name="quantity" src="d0_s15" value="70" value_original="70" />
        <character name="quantity" src="d0_s15" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum laeve is restricted to the eastern United States. It grows at the edges of forests and in disturbed areas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;D.C.;W.Va.;Fla.;N.J.;Tex.;La.;Tenn.;N.Y.;Pa.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Conn.;Md.;Kans.;Okla.;Mass.;Ohio;Mo.;Mich.;Miss.;Ky.;N.C.;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>