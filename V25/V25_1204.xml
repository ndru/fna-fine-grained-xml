<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">442</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc.) Freckmann &amp; Lelong" date="unknown" rank="section">Angustifolia</taxon_name>
    <taxon_name authority="(Desv. ex Poir.) Gould &amp; C.A. Clark" date="unknown" rank="species">aciculare</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section angustifolia;species aciculare</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aciculare</taxon_name>
    <taxon_hierarchy>genus panicum;species aciculare</taxon_hierarchy>
  </taxon_identification>
  <number>27</number>
  <other_name type="common_name">Narrow-leaved panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants grayish-green, cespitose, with caudices.</text>
      <biological_entity id="o16031" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish-green" value_original="grayish-green" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16032" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o16031" id="r2689" name="with" negation="false" src="d0_s0" to="o16032" />
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes poorly differentiated;</text>
      <biological_entity constraint="basal" id="o16033" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="poorly" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades usually large, ovate to lanceolate, often transitional to the cauline blades.</text>
      <biological_entity id="o16034" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s2" value="large" value_original="large" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
        <character constraint="to cauline blades" constraintid="o16035" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="transitional" value_original="transitional" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16035" name="blade" name_original="blades" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Culms 15-75 cm, erect;</text>
      <biological_entity id="o16036" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes glabrous or sparsely pubescent;</text>
      <biological_entity id="o16037" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes glabrous or puberulent to pilose basally;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase with erect to spreading culms, extensively branched from the mid and upper culm nodes, eventually producing flabellate clusters of reduced, flat or involute blades.</text>
      <biological_entity id="o16038" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="puberulent" modifier="basally" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
      <biological_entity id="o16039" name="phase" name_original="phase" src="d0_s6" type="structure" />
      <biological_entity id="o16040" name="culm" name_original="culms" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" is_modifier="true" name="orientation" src="d0_s6" to="spreading" />
        <character constraint="from mid upper culm nodes" constraintid="o16041" is_modifier="false" modifier="extensively" name="architecture" src="d0_s6" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="culm" id="o16041" name="node" name_original="nodes" src="d0_s6" type="structure" constraint_original="mid and upper culm">
        <character is_modifier="false" modifier="eventually" name="shape" src="d0_s6" value="flabellate" value_original="flabellate" />
      </biological_entity>
      <biological_entity id="o16042" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="true" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
      <relation from="o16038" id="r2690" name="fall" negation="false" src="d0_s6" to="o16039" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 3-7;</text>
      <biological_entity constraint="cauline" id="o16043" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths shorter than the internodes, glabrous or with soft, ascending, papillose-based hairs;</text>
      <biological_entity id="o16044" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character constraint="than the internodes" constraintid="o16045" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="with soft , ascending , papillose-based hairs" />
      </biological_entity>
      <biological_entity id="o16045" name="internode" name_original="internodes" src="d0_s8" type="structure" />
      <biological_entity id="o16046" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s8" value="soft" value_original="soft" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o16044" id="r2691" name="with" negation="false" src="d0_s8" to="o16046" />
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.5-2 mm, of hairs;</text>
      <biological_entity id="o16047" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16048" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <relation from="o16047" id="r2692" name="consists_of" negation="false" src="d0_s9" to="o16048" />
    </statement>
    <statement id="d0_s10">
      <text>lower blades 4-16 cm long, 3-9 mm wide, stiffly ascending to erect, glabrous or sparsely pilose to pubescent, with prominent raised veins, flat or longitudinally wrinkled, blades of the flag leaves often greatly reduced, often involute.</text>
      <biological_entity constraint="lower" id="o16049" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="16" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="stiffly ascending" name="orientation" src="d0_s10" to="erect" />
        <character char_type="range_value" from="sparsely pilose" name="pubescence" src="d0_s10" to="pubescent" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="longitudinally" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity id="o16050" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o16051" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often greatly" name="size" src="d0_s10" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s10" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o16052" name="flag" name_original="flag" src="d0_s10" type="structure" />
      <biological_entity id="o16053" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <relation from="o16049" id="r2693" name="with" negation="false" src="d0_s10" to="o16050" />
      <relation from="o16051" id="r2694" name="part_of" negation="false" src="d0_s10" to="o16052" />
      <relation from="o16051" id="r2695" name="part_of" negation="false" src="d0_s10" to="o16053" />
    </statement>
    <statement id="d0_s11">
      <text>Primary panicles 2-10 cm long, 0.5-7 cm wide, open or contracted, well-exserted.</text>
      <biological_entity constraint="primary" id="o16054" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s11" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="open" value_original="open" />
        <character is_modifier="false" name="condition_or_size" src="d0_s11" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="position" src="d0_s11" value="well-exserted" value_original="well-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 1.7-3.6 mm long, 1.2-1.8 mm wide, obovoid to ellipsoid, biconvex in side view, glabrous or pubescent, bases narrow to attenuate, apices blunt or pointed to beaked.</text>
      <biological_entity id="o16055" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s12" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s12" to="ellipsoid" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s12" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16056" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o16057" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="pointed" name="shape" src="d0_s12" to="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes thin, weakly veined, about 1/3 as long as the spikelets, attached to 0.5 mm below upper glumes, clasping at the base, broadly triangular to rounded;</text>
      <biological_entity constraint="lower" id="o16058" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s13" value="veined" value_original="veined" />
        <character constraint="as-long-as spikelets" constraintid="o16059" name="quantity" src="d0_s13" value="1/3" value_original="1/3" />
        <character constraint="below upper glumes" constraintid="o16060" is_modifier="false" name="fixation" notes="" src="d0_s13" value="attached" value_original="attached" />
        <character constraint="at base" constraintid="o16061" is_modifier="false" name="architecture_or_fixation" notes="" src="d0_s13" value="clasping" value_original="clasping" />
        <character char_type="range_value" from="broadly triangular" name="shape" notes="" src="d0_s13" to="rounded" />
      </biological_entity>
      <biological_entity id="o16059" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <biological_entity id="o16060" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s13" value="upper" value_original="upper" />
      </biological_entity>
      <biological_entity id="o16061" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes with 5-9 prominent veins;</text>
      <biological_entity constraint="upper" id="o16062" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o16063" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s14" to="9" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o16062" id="r2696" name="with" negation="false" src="d0_s14" to="o16063" />
    </statement>
    <statement id="d0_s15">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o16064" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper florets apiculate.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 18.</text>
      <biological_entity constraint="upper" id="o16065" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16066" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium aciculare grows in sandy, open areas in the southeastern United States, the West Indies and the Caribbean, southern Mexico, Central America, and northern South America. It has not been reported from northern Mexico. The primary panicles are open-pollinated (sometimes briefly) and develop from April to June; the secondary panicles are cleistogamous and develop from May into late fall.</discussion>
  <discussion>The subspecies are often distinct when growing together, perhaps maintained by the predominant autogamy, but they are more difficult to separate over wider geographic areas. Rare, partly fertile putative hybrids with Dichanthelium consanguineum, D. acuminatum, D. ovale, D. portoricense, and (possibly) D. dichotomum apparently lead to some intergradation with these species.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Md.;N.J.;Okla.;Miss.;Tex.;La.;Del.;Ala.;D.C.;Tenn.;N.C.;S.C.;Va.;Ark.;Ga.;Ky.;Fla.;Ind.;N.Y.;Pa.;W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Primary panicles usually contracted; branches appearing 1-sided; culms sparsely pubescent to almost glabrous</description>
      <determination>Dichanthelium aciculare subsp. neurantbum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Primary panicles not contracted; branches not appearing 1-sided; culms usually pubescent, at least on the lower internodes.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets 1.7-2.3 mm long, with blunt apices</description>
      <determination>Dichanthelium aciculare subsp. aciculare</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets 2.4-3.6 mm long, with pointed or beaked apices.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets 2.4-3 mm long, not strongly attenuate at the base; lower glumes attached less than 0.2 mm below the upper glumes</description>
      <determination>Dichanthelium aciculare subsp. angustifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Spikelets 2.9-3.6 mm long, strongly attenuate at the base; lower glumes attached 0.3-0.5 mm below the upper glumes</description>
      <determination>Dichanthelium aciculare subsp. fusiforme</determination>
    </key_statement>
  </key>
</bio:treatment>