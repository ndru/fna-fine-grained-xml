<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">592</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">setaceum</taxon_name>
    <taxon_name authority="(Nash) D.J. Banks" date="unknown" rank="variety">stramineum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species setaceum;variety stramineum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stramineum</taxon_name>
    <taxon_hierarchy>genus paspalum;species stramineum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ciliatifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">stramineum</taxon_name>
    <taxon_hierarchy>genus paspalum;species ciliatifolium;variety stramineum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bushii</taxon_name>
    <taxon_hierarchy>genus paspalum;species bushii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Yellow sand paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect to spreading.</text>
      <biological_entity id="o25496" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o25497" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o25498" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades to 30 cm long, 3.3-13.5 mm wide, lax to somewhat stiff, glabrous or with a few hairs along the midrib, sometimes pubescent, yellow-green to dark green, margins scabrous, ciliate.</text>
      <biological_entity id="o25499" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="width" src="d0_s2" to="13.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="somewhat" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="with a few hairs" value_original="with a few hairs" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s2" to="dark green" />
      </biological_entity>
      <biological_entity id="o25500" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o25501" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o25502" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o25499" id="r4304" name="with" negation="false" src="d0_s2" to="o25500" />
      <relation from="o25500" id="r4305" name="along" negation="false" src="d0_s2" to="o25501" />
    </statement>
    <statement id="d0_s3">
      <text>Panicle branches 4-12 cm;</text>
      <biological_entity constraint="panicle" id="o25503" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branch axes 0.6-1.1 mm wide.</text>
      <biological_entity constraint="branch" id="o25504" name="axis" name_original="axes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s4" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 1.7-2.4 mm long, 1.5-2.1 mm wide, obovate to suborbicular, pubescent or occasionally glabrous;</text>
      <biological_entity id="o25505" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s5" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="suborbicular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lower lemmas without an evident midvein;</text>
      <biological_entity constraint="lower" id="o25506" name="lemma" name_original="lemmas" src="d0_s6" type="structure" />
      <biological_entity id="o25507" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o25506" id="r4306" name="without" negation="false" src="d0_s6" to="o25507" />
    </statement>
    <statement id="d0_s7">
      <text>upper florets 1.7-2.1 mm.</text>
      <biological_entity constraint="upper" id="o25508" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s7" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum setaceum var. stramineum grows at the edges of forests and in disturbed areas with sandy soil. Its range extends from the central plains and eastern United States to Mexico, Bermuda, and the West Indies.</discussion>
  
</bio:treatment>