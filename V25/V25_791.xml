<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="Arechav." date="unknown" rank="species">berroi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species berroi</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o22554" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-80 cm.</text>
      <biological_entity id="o22555" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o22556" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules ciliate;</text>
      <biological_entity id="o22557" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-15 cm long, 1.5-2 mm wide, glabrous or sparsely pilose near the base.</text>
      <biological_entity id="o22558" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="near base" constraintid="o22559" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o22559" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 2-4 branches, these entangled for most of their length, separable only with difficulty, forming a narrow, cylindrical, spikelike inflorescence, individual branches visibly distinct only at the tips;</text>
      <biological_entity id="o22560" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character constraint="for" is_modifier="false" name="length" notes="" src="d0_s6" value="entangled" value_original="entangled" />
        <character constraint="with difficulty" constraintid="o22562" is_modifier="false" name="fixation" src="d0_s6" value="separable" value_original="separable" />
      </biological_entity>
      <biological_entity id="o22561" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o22562" name="difficulty" name_original="difficulty" src="d0_s6" type="structure" />
      <biological_entity id="o22563" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="shape" src="d0_s6" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="individual" id="o22564" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character constraint="at tips" constraintid="o22565" is_modifier="false" modifier="visibly" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o22565" name="tip" name_original="tips" src="d0_s6" type="structure" />
      <relation from="o22560" id="r3844" name="with" negation="false" src="d0_s6" to="o22561" />
      <relation from="o22560" id="r3845" name="forming a" negation="false" src="d0_s6" to="o22563" />
    </statement>
    <statement id="d0_s7">
      <text>branches 3-12 cm, tightly appressed and adherent, with 9-12 spikelets per cm.</text>
      <biological_entity id="o22566" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o22567" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
      <biological_entity id="o22568" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o22566" id="r3846" name="with" negation="false" src="d0_s7" to="o22567" />
      <relation from="o22567" id="r3847" name="per" negation="false" src="d0_s7" to="o22568" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets imbricate, with 1 bisexual and 3 sterile florets.</text>
      <biological_entity id="o22569" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o22570" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o22569" id="r3848" name="with" negation="false" src="d0_s8" to="o22570" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 1.5-2 mm long, about 0.3 mm wide;</text>
      <biological_entity constraint="lower" id="o22571" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 2.1-2.6 mm long, 0.3-0.6 mm wide;</text>
      <biological_entity constraint="upper" id="o22572" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" src="d0_s10" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lowest lemmas 2.7-3.5 mm, ovate, margins and keels hairy, hairs to 2 mm, awns 2.7-3.4 mm;</text>
      <biological_entity constraint="lowest" id="o22573" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22574" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22575" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22576" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>second florets about 1.9 mm, glabrous, awned;</text>
      <biological_entity id="o22577" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s11" to="3.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22578" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.9" value_original="1.9" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>distal florets unawned.</text>
      <biological_entity constraint="distal" id="o22579" name="floret" name_original="florets" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.2-1.8 mm long, 0.5-0.7 mm wide, trigonous.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 40.</text>
      <biological_entity id="o22580" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s14" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22581" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloris berroi is native to the Rio de la Plata region of Argentina and Uruguay. It has been cultivated at scattered locations in the United States (Hitchcock 1951), but is not known to be established in the Flora region.</discussion>
  
</bio:treatment>