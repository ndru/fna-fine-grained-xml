<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Bouteloua</taxon_name>
    <taxon_name authority="(Kunth) Benth. ex S. Watson" date="unknown" rank="species">chondrosoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus bouteloua;species chondrosoides</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Sprucetop grama</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, without rhizomes or stolons.</text>
      <biological_entity id="o15159" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15160" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o15161" name="stolon" name_original="stolons" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms (10) 30-60 cm, erect, unbranched.</text>
      <biological_entity id="o15162" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o15163" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15164" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths mostly glabrous, margins often long-ciliate distally;</text>
      <biological_entity id="o15165" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15166" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; distally" name="architecture_or_pubescence_or_shape" src="d0_s4" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-0.6 mm, of hairs;</text>
      <biological_entity id="o15167" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15168" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o15167" id="r2536" name="consists_of" negation="false" src="d0_s5" to="o15168" />
    </statement>
    <statement id="d0_s6">
      <text>blades 1-10 cm long, 1-2.5 (3) mm wide, flat, glaucous, bases with papillose-based hairs on the margins, similar hairs sometimes present on either or both surfaces.</text>
      <biological_entity id="o15169" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o15170" name="base" name_original="bases" src="d0_s6" type="structure" />
      <biological_entity id="o15171" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o15172" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o15173" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character constraint="on surfaces" constraintid="o15174" is_modifier="false" modifier="sometimes" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15174" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <relation from="o15170" id="r2537" name="with" negation="false" src="d0_s6" to="o15171" />
      <relation from="o15171" id="r2538" name="on" negation="false" src="d0_s6" to="o15172" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2.5-6 cm, with 3-8 (10) branches;</text>
      <biological_entity id="o15175" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15176" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="10" value_original="10" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <relation from="o15175" id="r2539" name="with" negation="false" src="d0_s7" to="o15176" />
    </statement>
    <statement id="d0_s8">
      <text>branches (8) 10-15 mm, densely pubescent, with 8-12 spikelets, axes extending to 5 mm beyond the base of the terminal spikelets, apices entire;</text>
      <biological_entity id="o15177" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15178" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <biological_entity id="o15179" name="axis" name_original="axes" src="d0_s8" type="structure" />
      <biological_entity id="o15180" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="terminal" id="o15181" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <relation from="o15177" id="r2540" name="with" negation="false" src="d0_s8" to="o15178" />
      <relation from="o15179" id="r2541" name="beyond" negation="false" src="d0_s8" to="o15180" />
      <relation from="o15180" id="r2542" name="part_of" negation="false" src="d0_s8" to="o15181" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation at the base of the branches.</text>
      <biological_entity id="o15182" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15183" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o15184" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o15182" id="r2543" name="at" negation="false" src="d0_s9" to="o15183" />
      <relation from="o15183" id="r2544" name="part_of" negation="false" src="d0_s9" to="o15184" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets appressed, all alike, 7-7.5 mm, with 1 bisexual and 1 rudimentary floret.</text>
      <biological_entity id="o15185" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="variability" src="d0_s10" value="alike" value_original="alike" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15186" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o15185" id="r2545" name="with" negation="false" src="d0_s10" to="o15186" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes evidently hairy;</text>
      <biological_entity id="o15187" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="evidently" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 2.5-4.5 mm;</text>
      <biological_entity constraint="lower" id="o15188" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 4.5-6.5 mm;</text>
      <biological_entity constraint="upper" id="o15189" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lowest lemmas 4.7-6.2 mm, hairy distally, 3-lobed, lobes unawned or shortly awned;</text>
      <biological_entity constraint="lowest" id="o15190" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s14" to="6.2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o15191" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lowest paleas 5-7.2 mm, pubescent along the veins and on the margins, bifid, veins excurrent as short awns;</text>
      <biological_entity constraint="lowest" id="o15192" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.2" to_unit="mm" />
        <character constraint="along veins" constraintid="o15193" is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s15" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o15193" name="vein" name_original="veins" src="d0_s15" type="structure" />
      <biological_entity id="o15194" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity id="o15195" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character constraint="as awns" constraintid="o15196" is_modifier="false" name="architecture" src="d0_s15" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o15196" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
      </biological_entity>
      <relation from="o15192" id="r2546" name="on" negation="false" src="d0_s15" to="o15194" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 2.8-4 mm, yellow;</text>
      <biological_entity id="o15197" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets rudimentary, glabrous, 3-awned, awns scabrous, sometimes arising from a short but evident awn column, central awns sometimes with a membranous margin, awns scabrous.</text>
      <biological_entity constraint="upper" id="o15198" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o15199" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s17" value="scabrous" value_original="scabrous" />
        <character constraint="from awn column" constraintid="o15200" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s17" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="awn" id="o15200" name="column" name_original="column" src="d0_s17" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="true" name="prominence" src="d0_s17" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="central" id="o15201" name="awn" name_original="awns" src="d0_s17" type="structure" />
      <biological_entity id="o15202" name="margin" name_original="margin" src="d0_s17" type="structure">
        <character is_modifier="true" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o15203" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s17" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o15201" id="r2547" name="with" negation="false" src="d0_s17" to="o15202" />
    </statement>
    <statement id="d0_s18">
      <text>Caryopses about 2.5 mm long, about 0.9 mm wide.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 20, 22, 40.</text>
      <biological_entity id="o15204" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character name="length" src="d0_s18" unit="mm" value="2.5" value_original="2.5" />
        <character name="width" src="d0_s18" unit="mm" value="0.9" value_original="0.9" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15205" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
        <character name="quantity" src="d0_s19" value="22" value_original="22" />
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua chondrosoides grows on dry, rocky slopes and grassy plateaus at 200-2500 m. Its range extends from southern Arizona and western Texas to Costa Rica. It resembles B. eludens in having pubescent panicle branches, but B. eludens usually has 12-16 branches 5-11 mm long with 2-6 spikelets whereas B. chrondrosoides usually has 3-8 branches 10-15 mm long with 8-12 spikelets per branch.</discussion>
  
</bio:treatment>