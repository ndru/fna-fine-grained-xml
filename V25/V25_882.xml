<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="(Desv.) A. Gray" date="unknown" rank="subgenus">Chondrosum</taxon_name>
    <taxon_name authority="(Kunth) Lag. ex Griffiths" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus chondrosum;species gracilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chondrosum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracile</taxon_name>
    <taxon_hierarchy>genus chondrosum;species gracile</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bouteloua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">oligostachya</taxon_name>
    <taxon_hierarchy>genus bouteloua;species oligostachya</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Blue grama</other_name>
  <other_name type="common_name">Eyelash grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually densely cespitose, often with short, stout rhizomes.</text>
      <biological_entity id="o18684" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18685" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 24-70 cm, not woody basally, erect, geniculate, or decumbent and rooting at the lower nodes, not branched from the aerial nodes;</text>
      <biological_entity id="o18686" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="24" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" modifier="not; basally" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o18687" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="from nodes" constraintid="o18688" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18687" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o18688" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes usually 2-3, glabrous or puberulent;</text>
      <biological_entity id="o18689" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower internodes glabrous.</text>
      <biological_entity constraint="lower" id="o18690" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves mainly basal;</text>
      <biological_entity id="o18691" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o18692" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sheaths glabrous or sparsely hirsute;</text>
      <biological_entity id="o18693" name="sheath" name_original="sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.1-0.4 mm, of hairs, often with marginal tufts of long hairs;</text>
      <biological_entity id="o18694" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18695" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity constraint="marginal" id="o18696" name="tuft" name_original="tufts" src="d0_s7" type="structure" />
      <biological_entity id="o18697" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
      </biological_entity>
      <relation from="o18694" id="r3152" name="consists_of" negation="false" src="d0_s7" to="o18695" />
      <relation from="o18694" id="r3153" modifier="often" name="with" negation="false" src="d0_s7" to="o18696" />
      <relation from="o18696" id="r3154" name="part_of" negation="false" src="d0_s7" to="o18697" />
    </statement>
    <statement id="d0_s8">
      <text>blades 2-12 (19) cm long, 0.5-2.5 mm wide, flat to involute at maturity, hairs usually present basally.</text>
      <biological_entity id="o18698" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="19" value_original="19" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" constraint="at maturity" from="flat" name="shape" src="d0_s8" to="involute" />
      </biological_entity>
      <biological_entity id="o18699" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually; basally" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Panicles with 1-3 (6) branches, these racemose on 2-8.5 (12.5) cm rachises or digitate;</text>
      <biological_entity id="o18700" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character constraint="on rachises" constraintid="o18702" is_modifier="false" name="arrangement" notes="" src="d0_s9" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o18701" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="6" value_original="6" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o18702" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s9" unit="cm" value="12.5" value_original="12.5" />
        <character char_type="range_value" from="2" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s9" to="8.5" to_unit="cm" />
      </biological_entity>
      <relation from="o18700" id="r3155" name="with" negation="false" src="d0_s9" to="o18701" />
    </statement>
    <statement id="d0_s10">
      <text>branches 13-50 (75) mm, persistent, arcuate, scabrous, without papillose-based hairs, with 40-130 spikelets, terminating in a spikelet;</text>
      <biological_entity id="o18704" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o18705" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s10" to="130" />
      </biological_entity>
      <biological_entity id="o18706" name="spikelet" name_original="spikelet" src="d0_s10" type="structure" />
      <relation from="o18703" id="r3156" name="without" negation="false" src="d0_s10" to="o18704" />
      <relation from="o18703" id="r3157" name="with" negation="false" src="d0_s10" to="o18705" />
      <relation from="o18703" id="r3158" name="terminating in a" negation="false" src="d0_s10" to="o18706" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o18703" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="75" value_original="75" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s10" to="50" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="course_or_shape" src="d0_s10" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o18707" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <relation from="o18703" id="r3159" name="above" negation="false" src="d0_s11" to="o18707" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets pectinate, with 1 bisexual and 1 rudimentary floret.</text>
      <biological_entity id="o18708" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <biological_entity id="o18709" name="floret" name_original="floret" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="true" name="prominence" src="d0_s12" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o18708" id="r3160" name="with" negation="false" src="d0_s12" to="o18709" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes mostly glabrous or scabrous, midveins sometimes with papillose-based hairs;</text>
      <biological_entity id="o18710" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o18711" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o18712" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o18711" id="r3161" name="with" negation="false" src="d0_s13" to="o18712" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 1.5-3.5 mm;</text>
      <biological_entity constraint="lower" id="o18713" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 3.5-6 mm;</text>
      <biological_entity constraint="upper" id="o18714" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lowest lemmas 3.5-6 mm, pubescent at least basally, 5-lobed, central and lateral lobes veined and awned, awns 1-3 mm, central awns flanked by 2 membranous lobes;</text>
      <biological_entity constraint="lowest" id="o18715" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="at-least basally; basally" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" src="d0_s16" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity constraint="central and lateral" id="o18716" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o18717" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o18718" name="awn" name_original="awns" src="d0_s16" type="structure" />
      <biological_entity id="o18719" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="true" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o18718" id="r3162" name="flanked by" negation="false" src="d0_s16" to="o18719" />
    </statement>
    <statement id="d0_s17">
      <text>lower paleas about 5 mm, shallowly bilobed, veins excurrent for less than 1 mm;</text>
      <biological_entity constraint="lower" id="o18720" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s17" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o18721" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character constraint="for 0-1 mm" is_modifier="false" name="architecture" src="d0_s17" value="excurrent" value_original="excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>rachilla segments subtending second florets with a distal tuft of hairs;</text>
      <biological_entity constraint="rachilla" id="o18722" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o18723" name="floret" name_original="florets" src="d0_s18" type="structure" />
      <biological_entity constraint="distal" id="o18724" name="tuft" name_original="tuft" src="d0_s18" type="structure" />
      <biological_entity id="o18725" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <relation from="o18723" id="r3163" name="with" negation="false" src="d0_s18" to="o18724" />
      <relation from="o18724" id="r3164" name="part_of" negation="false" src="d0_s18" to="o18725" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 1.7-2.9 mm, yellow or purple;</text>
      <biological_entity id="o18726" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s19" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>upper florets sterile, 0.9-3 mm, lobed almost to the base, lobes rounded, 3-awned, awns equal, 1-3 mm.</text>
      <biological_entity constraint="upper" id="o18727" name="floret" name_original="florets" src="d0_s20" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s20" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
        <character constraint="to base" constraintid="o18728" is_modifier="false" name="shape" src="d0_s20" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o18728" name="base" name_original="base" src="d0_s20" type="structure" />
      <biological_entity id="o18729" name="lobe" name_original="lobes" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o18730" name="awn" name_original="awns" src="d0_s20" type="structure">
        <character is_modifier="false" name="variability" src="d0_s20" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Caryopses 2.5-3 mm long, about 0.5 mm wide.</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 20, 28, 35, 40, 42, 60, 61, 77, 84.</text>
      <biological_entity id="o18731" name="caryopsis" name_original="caryopses" src="d0_s21" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s21" to="3" to_unit="mm" />
        <character name="width" src="d0_s21" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18732" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="20" value_original="20" />
        <character name="quantity" src="d0_s22" value="28" value_original="28" />
        <character name="quantity" src="d0_s22" value="35" value_original="35" />
        <character name="quantity" src="d0_s22" value="40" value_original="40" />
        <character name="quantity" src="d0_s22" value="42" value_original="42" />
        <character name="quantity" src="d0_s22" value="60" value_original="60" />
        <character name="quantity" src="d0_s22" value="61" value_original="61" />
        <character name="quantity" src="d0_s22" value="77" value_original="77" />
        <character name="quantity" src="d0_s22" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua gracilis grows in pure stands in mixed prairie associations and disturbed habitats, usually on rocky or clay soils and mainly at elevations of 300-3000 m. Its native range extends from Canada to central Mexico; records from the eastern portion of the Flora represent introductions.</discussion>
  <discussion>Bouteloua gracilis is an important native forage species and also an attractive ornamental.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;B.C.;Man.;Ont.;Sask.;Wis.;Wyo.;Conn.;Fla.;S.C.;N.Mex.;Tex.;N.Y.;Nev.;Colo.;Calif.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ill.;Iowa;Ariz.;Idaho;Maine;Mass.;Ohio;Utah;Mo.;Minn.;Mich.;Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>