<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Phil." date="unknown" rank="genus">SCLEROPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus scleropogon</taxon_hierarchy>
  </taxon_identification>
  <number>17.13</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually monoecious, less frequently dioecious, occasionally synoecious;</text>
    </statement>
    <statement id="d0_s2">
      <text>bear¬ing wiry, often arching, stolons with 5-15 cm internodes, sometimes also weakly rhizomatous.</text>
      <biological_entity id="o7075" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s1" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" modifier="less frequently" name="reproduction" src="d0_s1" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s1" value="synoecious" value_original="synoecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7076" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s2" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o7077" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes; weakly" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o7075" id="r1140" name="bear ¬ ing" negation="false" src="d0_s2" to="o7076" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o7078" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7079" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths short, strongly veined, basal leaves commonly hispid or villous;</text>
      <biological_entity id="o7080" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s4" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7081" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="commonly" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules of hairs;</text>
      <biological_entity id="o7082" name="ligule" name_original="ligules" src="d0_s5" type="structure" />
      <biological_entity id="o7083" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o7082" id="r1141" name="consists_of" negation="false" src="d0_s5" to="o7083" />
    </statement>
    <statement id="d0_s6">
      <text>blades firm, flat or folded.</text>
      <biological_entity id="o7084" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="firm" value_original="firm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, usually exceeding the upper leaves, spikelike racemes or contracted panicles with few spikelets, in bisexual plants staminate and pistillate florets in the same spikelet with the staminate florets below the pistillate florets or in separate spikelets, bisexual florets occasionally produced;</text>
      <biological_entity id="o7085" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7086" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="upper" id="o7087" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7088" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
        <character is_modifier="true" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o7089" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o7090" name="plant" name_original="plants" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7091" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7092" name="spikelet" name_original="spikelet" src="d0_s7" type="structure" />
      <biological_entity id="o7093" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7094" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7095" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity id="o7096" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o7085" id="r1142" modifier="usually" name="exceeding" negation="false" src="d0_s7" to="o7086" />
      <relation from="o7085" id="r1143" modifier="usually" name="exceeding" negation="false" src="d0_s7" to="o7087" />
      <relation from="o7085" id="r1144" modifier="usually" name="exceeding" negation="false" src="d0_s7" to="o7088" />
      <relation from="o7085" id="r1145" name="with" negation="false" src="d0_s7" to="o7089" />
      <relation from="o7085" id="r1146" name="in" negation="false" src="d0_s7" to="o7090" />
      <relation from="o7091" id="r1147" name="in" negation="false" src="d0_s7" to="o7092" />
      <relation from="o7092" id="r1148" name="with" negation="false" src="d0_s7" to="o7093" />
      <relation from="o7094" id="r1149" name="in" negation="false" src="d0_s7" to="o7095" />
    </statement>
    <statement id="d0_s8">
      <text>branches not pectinate;</text>
    </statement>
    <statement id="d0_s9">
      <text>disarticu¬lation above the glumes and below the lowest pistillate floret in a spikelet, florets falling together, lowest floret with a bearded, sharp-pointed callus.</text>
      <biological_entity id="o7097" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o7098" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7099" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="together" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o7100" name="floret" name_original="floret" src="d0_s9" type="structure" />
      <biological_entity id="o7101" name="callus" name_original="callus" src="d0_s9" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s9" value="bearded" value_original="bearded" />
        <character is_modifier="true" name="shape" src="d0_s9" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
      <relation from="o7097" id="r1150" name="above the glumes and below" negation="false" src="d0_s9" to="o7098" />
      <relation from="o7100" id="r1151" name="with" negation="false" src="d0_s9" to="o7101" />
    </statement>
    <statement id="d0_s10">
      <text>Staminate spikelets with 5-10 (20) florets;</text>
      <biological_entity id="o7102" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7103" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="20" value_original="20" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <relation from="o7102" id="r1152" name="with" negation="false" src="d0_s10" to="o7103" />
    </statement>
    <statement id="d0_s11">
      <text>glumes membranous, pale, 1-3-veined, acuminate;</text>
      <biological_entity id="o7104" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 3-veined, similar to the glumes, unawned or awned, awns to 3 mm;</text>
      <biological_entity id="o7105" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o7106" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o7107" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o7105" id="r1153" name="to" negation="false" src="d0_s12" to="o7106" />
    </statement>
    <statement id="d0_s13">
      <text>paleas shorter than the lemmas, often conspicuously so.</text>
      <biological_entity id="o7108" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character constraint="than the lemmas" constraintid="o7109" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7109" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Pistillate spikelets appressed to the branch axes, usually the 3-5 lower florets functional, upper florets reduced to awns;</text>
      <biological_entity id="o7110" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character constraint="to branch axes" constraintid="o7111" is_modifier="false" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="branch" id="o7111" name="axis" name_original="axes" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" modifier="usually" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <biological_entity constraint="lower" id="o7112" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="function" src="d0_s14" value="functional" value_original="functional" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7113" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character constraint="to awns" constraintid="o7114" is_modifier="false" name="size" src="d0_s14" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o7114" name="awn" name_original="awns" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>glumes acuminate, strongly 3-veined, occasionally with a few fine accessory veins;</text>
      <biological_entity id="o7115" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="accessory" id="o7116" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="few" value_original="few" />
        <character is_modifier="true" name="width" src="d0_s15" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o7115" id="r1154" modifier="occasionally" name="with" negation="false" src="d0_s15" to="o7116" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas narrow, 3-veined, veins extending into awns, awns (30) 50-100 (150) mm, spreading or reflexed at maturity, x = 10.</text>
      <biological_entity id="o7117" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s16" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o7118" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity id="o7119" name="awn" name_original="awns" src="d0_s16" type="structure" />
      <biological_entity id="o7120" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="30" value_original="30" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" src="d0_s16" to="100" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="x" id="o7121" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
      <relation from="o7118" id="r1155" name="extending into" negation="false" src="d0_s16" to="o7119" />
    </statement>
  </description>
  <discussion>Scleropogon is a monotypic American genus with a disjunct distribution.</discussion>
  <references>
    <reference>Reeder, J.R. and L.J. Toolin. 1987. Scleropogon (Gramineae), a monotypic genus with disjunct distribution. Phytologia 62:267-275.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Calif.;Colo.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>