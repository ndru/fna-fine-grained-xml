<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">arizonica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species arizonica</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Arizona threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, occasionally with rhizomes.</text>
      <biological_entity id="o10627" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10628" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-80 (100) cm, erect, unbranched.</text>
      <biological_entity id="o10629" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="100" value_original="100" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o10630" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10631" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths usually longer than the internodes, mostly glabrous, throat sometimes with hairs, not disintegrating into threadlike fibers;</text>
      <biological_entity id="o10632" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o10633" is_modifier="false" name="length_or_size" src="d0_s4" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10633" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o10634" name="throat" name_original="throat" src="d0_s4" type="structure">
        <character constraint="into fibers" constraintid="o10636" is_modifier="false" modifier="not" name="dehiscence" notes="" src="d0_s4" value="disintegrating" value_original="disintegrating" />
      </biological_entity>
      <biological_entity id="o10635" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o10636" name="fiber" name_original="fibers" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="thread-like" value_original="threadlike" />
      </biological_entity>
      <relation from="o10634" id="r1754" name="with" negation="false" src="d0_s4" to="o10635" />
    </statement>
    <statement id="d0_s5">
      <text>collars glabrous or with hairs at the sides;</text>
      <biological_entity id="o10637" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o10638" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o10639" name="side" name_original="sides" src="d0_s5" type="structure" />
      <relation from="o10637" id="r1755" name="with" negation="false" src="d0_s5" to="o10638" />
      <relation from="o10638" id="r1756" name="at" negation="false" src="d0_s5" to="o10639" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o10640" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 10-25 (30) cm long, 1-3 mm wide, usually flat, often curling like wood shavings when mature, glabrous.</text>
      <biological_entity id="o10641" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character constraint="like wood" constraintid="o10642" is_modifier="false" modifier="often" name="course" src="d0_s7" value="curling" value_original="curling" />
      </biological_entity>
      <biological_entity id="o10642" name="wood" name_original="wood" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="when mature" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences spikelike panicles, 10-25 cm long, 1-3 cm wide;</text>
      <biological_entity id="o10643" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10644" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>nodes glabrous or with straight, about 0.5 mm hairs;</text>
      <biological_entity id="o10645" name="node" name_original="nodes" src="d0_s9" type="structure">
        <character constraint="with straight" is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="with straight" value_original="with straight" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o10646" name="hair" name_original="hairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>primary branches 2-6 cm, appressed, without axillary pulvini, with 2-8 spikelets.</text>
      <biological_entity constraint="primary" id="o10647" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o10648" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure" />
      <biological_entity id="o10649" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
      <relation from="o10647" id="r1757" name="without" negation="false" src="d0_s10" to="o10648" />
      <relation from="o10647" id="r1758" name="with" negation="false" src="d0_s10" to="o10649" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes 10-15 (18) mm, brownish, acuminate to awned, awns to 3 mm;</text>
      <biological_entity id="o10650" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="18" value_original="18" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s11" to="awned" />
      </biological_entity>
      <biological_entity id="o10651" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes slightly shorter than to equaling the upper glumes, 1-2-veined;</text>
      <biological_entity constraint="lower" id="o10652" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="than to equaling the upper glumes" constraintid="o10653" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-2-veined" value_original="1-2-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o10653" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses 1-1.8 mm;</text>
      <biological_entity id="o10654" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 12-18 mm, glabrous, rarely sparsely pilose, terminating in a 3-6 mm twisted column, junction with the awns not conspicuous;</text>
      <biological_entity id="o10655" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o10656" name="column" name_original="column" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o10657" name="junction" name_original="junction" src="d0_s14" type="structure" />
      <biological_entity id="o10658" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o10655" id="r1759" name="terminating" negation="false" src="d0_s14" to="o10656" />
      <relation from="o10657" id="r1760" name="with" negation="false" src="d0_s14" to="o10658" />
    </statement>
    <statement id="d0_s15">
      <text>awns 20-35 mm, straight to curved basally, ascending distally, not disarticulating at maturity;</text>
      <biological_entity id="o10659" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s15" to="35" to_unit="mm" />
        <character char_type="range_value" from="straight" modifier="basally" name="course" src="d0_s15" to="curved" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>central awns 20-35 mm;</text>
      <biological_entity constraint="central" id="o10660" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s16" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lateral awns slightly shorter than the central awns;</text>
      <biological_entity constraint="lateral" id="o10661" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character constraint="than the central awns" constraintid="o10662" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity constraint="central" id="o10662" name="awn" name_original="awns" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 1.3-1.9 mm. 2n = 22.</text>
      <biological_entity id="o10663" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s18" to="1.9" to_unit="mm" unit=",1.-1.9 mm" />
        <character name="some_measurement" src="d0_s18" unit=",1.-1.9 mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10664" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aristida arizonica grows in pine, pine-oak, and pinyon-juniper woodlands from the southwestern United States to southern Mexico. It may be confused with A. purpurea var. nealleyi, but differs in having flat, curly leaf blades and longer awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Colo.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>