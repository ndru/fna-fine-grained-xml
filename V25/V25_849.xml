<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">CYNODON</taxon_name>
    <taxon_name authority="Clayton &amp; J.R. Harlan" date="unknown" rank="species">aethiopicus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus cynodon;species aethiopicus</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Ethiopian dogstooth grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous, not rhizomatous;</text>
      <biological_entity id="o9026" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons stout, woody, lying flat on the ground.</text>
      <biological_entity id="o9027" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character constraint="on ground" constraintid="o9028" is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o9028" name="ground" name_original="ground" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-100 cm tall, 2-6 mm thick, becoming woody.</text>
      <biological_entity id="o9029" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="height" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o9030" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules about 0.3 mm, membranous, ciliolate;</text>
      <biological_entity id="o9031" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-25 cm long, 3-7 mm wide, glabrous or sparsely pubescent, glaucous.</text>
      <biological_entity id="o9032" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 4-10 (20) branches;</text>
      <biological_entity id="o9033" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o9034" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="20" value_original="20" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <relation from="o9033" id="r1483" name="with" negation="false" src="d0_s6" to="o9034" />
    </statement>
    <statement id="d0_s7">
      <text>branches 3.5-7 cm, in (1) 2-5 whorls, stiff, usually red or purple, axes triquetrous.</text>
      <biological_entity id="o9035" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o9036" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o9037" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <relation from="o9035" id="r1484" name="in" negation="false" src="d0_s7" to="o9036" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2-3 mm.</text>
      <biological_entity id="o9038" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes equaling to slightly exceeding the florets;</text>
      <biological_entity id="o9039" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o9040" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o9039" id="r1485" modifier="slightly" name="exceeding the" negation="false" src="d0_s9" to="o9040" />
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 2-2.2 mm;</text>
      <biological_entity constraint="lower" id="o9041" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1.7-2.6 mm;</text>
      <biological_entity constraint="upper" id="o9042" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s11" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2.1-2.6 mm, keels not winged, glabrous or with a few scattered hairs.</text>
      <biological_entity id="o9043" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9045" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o9044" id="r1486" name="with" negation="false" src="d0_s12" to="o9045" />
    </statement>
    <statement id="d0_s13">
      <text>2n =18, 36.</text>
      <biological_entity id="o9044" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with a few scattered hairs" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9046" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynodon aethiopicus is native to the East African rift. It is now established along the canal bank in the Santa Ana National Wildlife Refuge in Texas, and is expected to spread. The cultivar 'McCaleb' has been released as a forage grass for use in Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pacific Islands (Hawaii);Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>