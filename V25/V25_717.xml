<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">156</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(Scribn.) Bush" date="unknown" rank="species">×curtisetosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species ×curtisetosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">schreberi</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">curtisetosa</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species schreberi;variety curtisetosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">curtisetosa</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species curtisetosa</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>occasionally rhizomatous.</text>
      <biological_entity id="o29677" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-70 cm tall, less than 3 mm thick, erect, branched above;</text>
      <biological_entity id="o29678" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes smooth, shiny for most of their length, scabridulous or glabrous below the nodes.</text>
      <biological_entity id="o29679" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character constraint="below nodes" constraintid="o29680" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29680" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, margins hyaline, old sheaths not flattened, papery, or spirally coiled;</text>
      <biological_entity id="o29681" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29682" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o29683" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="old" value_original="old" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="texture" src="d0_s4" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="spirally" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-1.1 mm, membranous, truncate, sometimes ciliolate;</text>
      <biological_entity id="o29684" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-8.5 cm long, 2-5 mm wide, flat, smooth or scabridulous.</text>
      <biological_entity id="o29685" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4.2-16.5 cm long, 0.2-1.5 cm wide, mostly exserted from the sheath;</text>
      <biological_entity id="o29686" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.2" from_unit="cm" name="length" src="d0_s7" to="16.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29687" name="sheath" name_original="sheath" src="d0_s7" type="structure" />
      <relation from="o29686" id="r5066" modifier="mostly" name="exserted from the" negation="false" src="d0_s7" to="o29687" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.8-7.2 cm, ascending to appressed;</text>
      <biological_entity constraint="primary" id="o29688" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="7.2" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.6-3 mm, strigose.</text>
      <biological_entity id="o29689" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.2-3.4 mm.</text>
      <biological_entity id="o29690" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes shorter than the florets, veins scabridulous, unawned or awned, awns to 0.5 mm;</text>
      <biological_entity id="o29691" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="than the florets" constraintid="o29692" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o29692" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o29693" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29694" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 0.4-1.5 mm, veinless (rarely 1-veined), usually truncate to rounded, occasionally acute, sometimes notched;</text>
      <biological_entity constraint="lower" id="o29695" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="veinless" value_original="veinless" />
        <character char_type="range_value" from="usually truncate" name="shape" src="d0_s12" to="rounded" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s12" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 0.8-1.9 mm, 1 (2) -veined, acute to acuminate;</text>
      <biological_entity constraint="upper" id="o29696" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1(2)-veined" value_original="1(2)-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2.2-3 (3.4) mm, lanceolate, hairy on the calluses and lower portion of the margins and midveins, hairs shorter than 1.5 mm, apices scabridulous, acuminate, awned, awns 0.5-4 mm, straight;</text>
      <biological_entity id="o29697" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="3.4" value_original="3.4" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character constraint="on lower portion" constraintid="o29699" is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o29698" name="callus" name_original="calluses" src="d0_s14" type="structure" />
      <biological_entity constraint="lower" id="o29699" name="portion" name_original="portion" src="d0_s14" type="structure" />
      <biological_entity id="o29700" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o29701" name="midvein" name_original="midveins" src="d0_s14" type="structure" />
      <biological_entity id="o29702" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s14" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o29703" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o29704" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o29699" id="r5067" name="part_of" negation="false" src="d0_s14" to="o29700" />
      <relation from="o29699" id="r5068" name="part_of" negation="false" src="d0_s14" to="o29701" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 2.2-3.1 (3.4) mm, lanceolate, intercostal region shortly pilose on the lower 1/2, apices acuminate;</text>
      <biological_entity id="o29705" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="3.4" value_original="3.4" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o29706" name="region" name_original="region" src="d0_s15" type="structure">
        <character constraint="on lower 1/2" constraintid="o29707" is_modifier="false" modifier="shortly" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o29707" name="1/2" name_original="1/2" src="d0_s15" type="structure" />
      <biological_entity id="o29708" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers usually not developed, occasionally 1 or 2 present, 0.3-0.9 mm, yellow.</text>
      <biological_entity id="o29709" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually not" name="development" src="d0_s16" value="developed" value_original="developed" />
        <character modifier="occasionally" name="quantity" src="d0_s16" unit="or present" value="1" value_original="1" />
        <character modifier="occasionally" name="quantity" src="d0_s16" unit="or present" value="2" value_original="2" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.4-1.6 mm, fusiform, brown.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = unknown.</text>
      <biological_entity id="o29710" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s17" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29711" name="chromosome" name_original="" src="d0_s18" type="structure" />
    </statement>
  </description>
  <discussion>Muhlenbergia ×curtisetosa grows in abandoned fields and forest openings, often near bogs, at elevations of 20-300 m. It may be a hybrid between M. schreberi (which contributes the short glumes) and either of two rhizomatous species, M. frondosa and M. tenuiflora.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.;Iowa;Ind.;Ont.;Tex.;Ohio;Mo.;Pa.;Ill.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>