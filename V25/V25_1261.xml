<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Repentia</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">repens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section repentia;species repens</taxon_hierarchy>
  </taxon_identification>
  <number>17</number>
  <other_name type="common_name">Torpedo grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, forming extensive colonies, rhizomes long, to 5 mm thick, branching, scaly, sharply pointed.</text>
      <biological_entity id="o19364" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19365" name="colony" name_original="colonies" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o19366" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s1" value="pointed" value_original="pointed" />
      </biological_entity>
      <relation from="o19364" id="r3265" name="forming" negation="false" src="d0_s1" to="o19365" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-90 cm tall, 1.8-2.8 mm thick, erect, rigid, simple or branching from the lower and middle nodes;</text>
      <biological_entity id="o19367" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="90" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="thickness" src="d0_s2" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="from lower" constraintid="o19368" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o19368" name="lower" name_original="lower" src="d0_s2" type="structure" />
      <biological_entity constraint="middle" id="o19369" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or sparsely hispid;</text>
      <biological_entity id="o19370" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous.</text>
      <biological_entity id="o19371" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths generally shorter than the internodes, not keeled, lower nodes glabrous or hispid, hairs papillose-based, particularly near the summits;</text>
      <biological_entity id="o19372" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o19373" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="generally shorter" value_original="generally shorter" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o19373" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity constraint="lower" id="o19374" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o19375" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o19376" name="summit" name_original="summits" src="d0_s5" type="structure" />
      <relation from="o19375" id="r3266" modifier="particularly" name="near" negation="false" src="d0_s5" to="o19376" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o19377" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 3-25 cm long, 2-8 mm wide, often distichous, flat to slightly involute, firm, adaxial surfaces pilose basally, glabrous or sparsely pubescent abaxially.</text>
      <biological_entity id="o19378" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s7" value="distichous" value_original="distichous" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="slightly involute" />
        <character is_modifier="false" name="texture" src="d0_s7" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19379" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 3-24 cm long, usually less than 5 cm wide, open;</text>
      <biological_entity id="o19380" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="24" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="usually" name="width" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches 2-11 cm, alternate, few, stiffly ascending to spreading;</text>
      <biological_entity constraint="primary" id="o19381" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="11" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character char_type="range_value" from="stiffly ascending" name="orientation" src="d0_s9" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 1-6 mm, subappressed.</text>
      <biological_entity id="o19382" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="subappressed" value_original="subappressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.2-2.8 mm long, 0.8-1.3 mm wide, ellipsoid-ovoid, pale green, acute, upper glumes and lower lemmas sometimes separating (gaping) beyond the florets.</text>
      <biological_entity id="o19383" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s11" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid-ovoid" value_original="ellipsoid-ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="upper" id="o19384" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="beyond florets" constraintid="o19386" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s11" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o19385" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character constraint="beyond florets" constraintid="o19386" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s11" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o19386" name="floret" name_original="florets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 0.5-1 mm, 1/5 – 2/5 as long as the spikelets, glabrous, faintly 1-5-veined, subtruncate to broadly acute;</text>
      <biological_entity constraint="lower" id="o19387" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o19388" from="1/5" name="quantity" src="d0_s12" to="2/5" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s12" value="1-5-veined" value_original="1-5-veined" />
        <character char_type="range_value" from="subtruncate" name="shape" src="d0_s12" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o19388" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes and lower lemmas glabrous, extending 0.1-0.5 mm beyond the upper florets, scarcely separated;</text>
      <biological_entity constraint="upper" id="o19389" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="beyond the upper florets; scarcely" name="arrangement" src="d0_s13" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o19390" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="beyond the upper florets; scarcely" name="arrangement" src="d0_s13" value="separated" value_original="separated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 7-11-veined, shorter than the lower lemmas, acute to short-acuminate;</text>
      <biological_entity constraint="upper" id="o19391" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-11-veined" value_original="7-11-veined" />
        <character constraint="than the lower lemmas" constraintid="o19392" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s14" to="short-acuminate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19392" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o19393" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 7-11-veined;</text>
      <biological_entity constraint="lower" id="o19394" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="7-11-veined" value_original="7-11-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower paleas 1.9-2.1 mm, oblong;</text>
      <biological_entity constraint="lower" id="o19395" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s17" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper florets 1.8-2.7 mm long, 0.7-1.3 mm wide, broadly ellipsoid, broadest at or above the middle, glabrous, shiny, smooth, apices rounded.</text>
      <biological_entity constraint="upper" id="o19396" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s18" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s18" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="width" src="d0_s18" value="broadest" value_original="broadest" />
        <character is_modifier="false" name="width" src="d0_s18" value="above the middle florets" value_original="above the middle florets" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="middle" id="o19397" name="floret" name_original="florets" src="d0_s18" type="structure" />
      <relation from="o19396" id="r3267" name="above" negation="false" src="d0_s18" to="o19397" />
    </statement>
    <statement id="d0_s19">
      <text>2n - 36, 40, 45, 54.</text>
      <biological_entity id="o19398" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
        <character name="quantity" src="d0_s19" value="45" value_original="45" />
        <character name="quantity" src="d0_s19" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum repens grows on open, moist, sandy beaches and the shores of lakes and ponds, occasionally extend¬ing out into or onto the water. It is mostly, but not exclu¬sively, coastal. It grows on tropical and subtropical coasts throughout the world and may have been introduced to the Americas from elsewhere. Small plants having small, dense panicles of purplish spikelets with longer, subacute lower glumes have been named Panicum gouinii E. Fourn., but they intergrade with more typical plants and do not seem to merit taxonomic recognition.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Miss.;Tex.;La.;Calif.;N.C.;Ala.;Pacific Islands (Hawaii);Ga.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>