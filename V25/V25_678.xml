<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">126</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="Munro ex Scribn." date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Big alkali sacaton</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o29899" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 90-250 cm, stout.</text>
      <biological_entity id="o29900" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s2" to="250" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths rounded below, shiny, glabrous, rarely sparsely hairy apically, hairs to 6 mm;</text>
      <biological_entity id="o29901" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="below" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely; apically" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o29902" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o29903" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 20-70 cm long, 3-10 mm wide, flat (rarely involute), glabrous abaxially, scabrous adaxially, margins scabrous;</text>
      <biological_entity id="o29904" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="70" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flag blades ascending.</text>
      <biological_entity id="o29905" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o29906" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 20-60 cm long, 12-26 cm wide, open, broadly lanceolate, exserted;</text>
      <biological_entity id="o29907" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s7" to="60" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="width" src="d0_s7" to="26" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 1.5-10 cm, spreading 20-70° from the rachis;</text>
      <biological_entity constraint="primary" id="o29908" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character constraint="from rachis" constraintid="o29909" is_modifier="false" modifier="20-70°" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o29909" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches appressed, spikelet-bearing to the base;</text>
      <biological_entity constraint="secondary" id="o29910" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o29911" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o29910" id="r5102" name="to" negation="false" src="d0_s9" to="o29911" />
    </statement>
    <statement id="d0_s10">
      <text>pulvini glabrous;</text>
      <biological_entity id="o29912" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.2-0.5 mm, mostly appressed.</text>
      <biological_entity id="o29913" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 1.5-2.5 mm, crowded, purplish or greenish.</text>
      <biological_entity id="o29914" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes unequal, lanceolate to ovate, membranous;</text>
      <biological_entity id="o29915" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.5-1 mm, often appearing veinless;</text>
      <biological_entity constraint="lower" id="o29916" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s14" value="veinless" value_original="veinless" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 0.8-2 mm, 2/3 or more as long as the florets;</text>
      <biological_entity constraint="upper" id="o29917" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character constraint="as-long-as florets" constraintid="o29918" name="quantity" src="d0_s15" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o29918" name="floret" name_original="florets" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas 1.2-2.5 mm, ovate, membranous, glabrous, acute to obtuse;</text>
      <biological_entity id="o29919" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s16" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas 1.1-2.5 mm, ovate, membranous, glabrous;</text>
      <biological_entity id="o29920" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s17" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 1.1-1.3 mm, yellowish to purplish.</text>
      <biological_entity id="o29921" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s18" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits 1-1.4 mm, ellipsoid, reddish-brown or blackish, striate.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity id="o29922" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s19" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29923" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sporobolus wrightii grows in moist clay flats and on rocky slopes near saline habitats, at elevations of 5-1800 m. Its range extends to central Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Calif.;S.C.;Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>