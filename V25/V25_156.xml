<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="Scribn. ex Beal" date="unknown" rank="species">erosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species erosa</taxon_hierarchy>
  </taxon_identification>
  <number>39</number>
  <other_name type="common_name">Chihuahua lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations, without rhizomes, not glandular.</text>
      <biological_entity id="o11636" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11637" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o11638" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 70-110 cm, erect, glabrous below the nodes.</text>
      <biological_entity id="o11639" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="below nodes" constraintid="o11640" is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11640" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths hairy at the apices and sometimes on the upper margins, hairs to 4 mm, not papillose-based;</text>
      <biological_entity id="o11641" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="at apices" constraintid="o11642" is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11642" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity constraint="upper" id="o11643" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o11644" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o11641" id="r1914" modifier="sometimes" name="on" negation="false" src="d0_s3" to="o11643" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o11645" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (8) 12-30 cm long, 1.5-3.8 mm wide, flat to involute, abaxial surfaces glabrous, adaxial surfaces scabridulous, glabrous or sparsely hairy, hairs to 4 mm.</text>
      <biological_entity id="o11646" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11647" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11648" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11649" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 25-45 cm long, (5) 12-30 cm wide, broadly ovate, open;</text>
      <biological_entity id="o11650" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="12" from_unit="cm" name="width" src="d0_s6" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches mostly 4-20 cm, diverging 20-90° from the rachises, capillary, sinuous;</text>
      <biological_entity constraint="primary" id="o11651" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character constraint="from rachises" constraintid="o11652" is_modifier="false" modifier="20-90°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="course" src="d0_s7" value="sinuous" value_original="sinuous" />
      </biological_entity>
      <biological_entity id="o11652" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous or hairy;</text>
      <biological_entity id="o11653" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1-18 mm, appressed or divergent, proximal spikelets on each branch usually with pedicels shorter than 5 mm.</text>
      <biological_entity id="o11654" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11655" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o11656" name="branch" name_original="branch" src="d0_s9" type="structure" />
      <biological_entity id="o11657" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s9" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <relation from="o11655" id="r1915" name="on" negation="false" src="d0_s9" to="o11656" />
      <relation from="o11656" id="r1916" name="with" negation="false" src="d0_s9" to="o11657" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 5-9 mm long, 1-3 mm wide, lanceolate, plumbeous, with 5-12 florets;</text>
      <biological_entity id="o11659" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
      <relation from="o11658" id="r1917" name="with" negation="false" src="d0_s10" to="o11659" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation acropetal, glumes first, then the lemmas, paleas persistent.</text>
      <biological_entity id="o11658" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="plumbeous" value_original="plumbeous" />
        <character is_modifier="false" name="development" src="d0_s11" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o11660" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o11661" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity id="o11662" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes lanceolate to ovate, membranous;</text>
      <biological_entity id="o11663" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1.3-2.4 mm;</text>
      <biological_entity constraint="lower" id="o11664" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.6-2.6 mm;</text>
      <biological_entity constraint="upper" id="o11665" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s14" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2.4-3 mm, ovate, mostly membranous, hyaline near the margins and apices, lateral-veins inconspicuous, apices acute;</text>
      <biological_entity id="o11666" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="mostly" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character constraint="near apices" constraintid="o11668" is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o11667" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity id="o11668" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o11669" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o11670" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.5-3 mm, hyaline, narrower than the lemmas, apices obtuse to truncate;</text>
      <biological_entity id="o11671" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
        <character constraint="than the lemmas" constraintid="o11672" is_modifier="false" name="width" src="d0_s16" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o11672" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o11673" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s16" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 0.6-1.7 mm, purplish.</text>
      <biological_entity id="o11674" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.8-1.6 mm, subellipsoid, terete to somewhat laterally compressed, with a well-developed adaxial groove, faintly striate, opaque, reddish-brown.</text>
      <biological_entity constraint="adaxial" id="o11676" name="groove" name_original="groove" src="d0_s18" type="structure">
        <character is_modifier="true" name="development" src="d0_s18" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o11675" id="r1918" name="with" negation="false" src="d0_s18" to="o11676" />
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o11675" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s18" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="subellipsoid" value_original="subellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="terete to somewhat" value_original="terete to somewhat" />
        <character is_modifier="false" name="shape" src="d0_s18" value="terete to somewhat" value_original="terete to somewhat" />
        <character is_modifier="false" modifier="somewhat; laterally" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="faintly" name="coloration_or_pubescence_or_relief" notes="" src="d0_s18" value="striate" value_original="striate" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11677" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Eragrostis erosa grows on rocky slopes and hills, at 1200-2300 m, often in association with Pinus edulis, Juniperus monosperma, and Bouteloua gracilis. Its range extends from New Mexico and western Texas to northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>