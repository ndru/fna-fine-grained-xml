<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="(Walter) Michx." date="unknown" rank="species">serotina</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species serotina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>13</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>often mat-forming.</text>
      <biological_entity id="o18773" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-30 cm, decumbent and rooting at the lower nodes.</text>
      <biological_entity id="o18774" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o18775" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18775" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths conspicuously and densely hairy, longer hairs 1.5-2.5 mm, papillose-based, shorter hairs about 0.5 mm, not papillose-based;</text>
      <biological_entity id="o18776" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="longer" id="o18777" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o18778" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1.5-2.5 mm;</text>
      <biological_entity id="o18779" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-9 cm long, 3-8 mm wide, conspicuously hairy on both surfaces, longer hairs 1.5-2.5 mm, papillose-based, shorter hairs about 0.5 mm, not papillose-based.</text>
      <biological_entity id="o18780" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o18781" is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18781" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="longer" id="o18782" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o18783" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 2-9 spikelike primary branches, digitate or on rachises to 4 cm;</text>
      <biological_entity id="o18784" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>primary branch axes 3-10 cm, wing-margined, wings wider than the midribs, lower and middle portions bearing spikelets in groups of 3;</text>
      <biological_entity constraint="branch" id="o18785" name="axis" name_original="axes" src="d0_s7" type="structure" constraint_original="primary branch">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity id="o18786" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character constraint="than the midribs" constraintid="o18787" is_modifier="false" name="width" src="d0_s7" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o18787" name="midrib" name_original="midribs" src="d0_s7" type="structure" />
      <biological_entity constraint="lower and middle" id="o18788" name="portion" name_original="portions" src="d0_s7" type="structure" />
      <biological_entity id="o18789" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o18790" name="group" name_original="groups" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <relation from="o18788" id="r3173" name="bearing" negation="false" src="d0_s7" to="o18789" />
      <relation from="o18788" id="r3174" name="in" negation="false" src="d0_s7" to="o18790" />
    </statement>
    <statement id="d0_s8">
      <text>secondary branches rarely present;</text>
      <biological_entity constraint="secondary" id="o18791" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>shortest pedicels 0.5-0.8 mm;</text>
      <biological_entity constraint="shortest" id="o18792" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>midlength pedicels 1.5-2 mm;</text>
      <biological_entity constraint="midlength" id="o18793" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>longest pedicels 3-3.5 mm, adnate to the branch axes basally.</text>
      <biological_entity constraint="longest" id="o18794" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character constraint="to branch axes" constraintid="o18795" is_modifier="false" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o18795" name="axis" name_original="axes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets homomorphic, 1.5-1.8 mm, lanceolate.</text>
      <biological_entity id="o18796" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="homomorphic" value_original="homomorphic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o18797" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1/6-1/3 as long as the spikelets, margins and apices with appressed white hairs, hairs about 0.3 mm;</text>
      <biological_entity constraint="upper" id="o18798" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="as-long-as apices" constraintid="o18801" from="1/6" name="quantity" src="d0_s14" to="1/3" />
      </biological_entity>
      <biological_entity id="o18799" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity id="o18800" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o18801" name="apex" name_original="apices" src="d0_s14" type="structure" />
      <biological_entity id="o18802" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o18803" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <relation from="o18801" id="r3175" name="with" negation="false" src="d0_s14" to="o18802" />
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 7-veined, veins equally spaced, appressed-pubescent between the inner lateral-veins and on the margins, hairs 0.3-0.5 mm, minutely verrucose (use 50x magnification);</text>
      <biological_entity constraint="lower" id="o18804" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity id="o18805" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="equally" name="arrangement" src="d0_s15" value="spaced" value_original="spaced" />
        <character constraint="between the inner lateral-veins and on the margins , hairs 0.3-0.5 mm" is_modifier="false" name="pubescence" src="d0_s15" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="verrucose" value_original="verrucose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas yellow or tan at maturity.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n - unknown.</text>
      <biological_entity constraint="upper" id="o18806" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s16" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria serotina is native to the coastal plain of the southeastern United States. It has also been found in Cuba, possibly as an introduction, and on a ballast dump in Philadelphia, Pennsylvania. Its densely hairy sheath and short, densely hairy blades make this one of the more distinctive species of Digitaria in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;N.C.;Ga.;La.;Del.;Ala.;Pa.;Miss.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>