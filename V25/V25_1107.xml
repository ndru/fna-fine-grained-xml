<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">394</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ECHINOCHLOA</taxon_name>
    <taxon_name authority="(Kunth) Hitchc." date="unknown" rank="species">polystachya</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus echinochloa;species polystachya</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Creeping river grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o18979" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 1-2 m tall, to 1 cm thick, erect or decumbent and rooting at the lower nodes, upper portion sometimes floating distally;</text>
      <biological_entity id="o18980" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="height" src="d0_s2" to="2" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="cm" name="thickness" src="d0_s2" to="1" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o18981" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18981" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity constraint="upper" id="o18982" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or antrorsely villous.</text>
      <biological_entity id="o18983" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths mostly glabrous, minutely puberulent, or hispid, hairs papillose-based, throat hispid;</text>
      <biological_entity id="o18984" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o18985" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o18986" name="throat" name_original="throat" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules present on the lower leaves, 1-5 mm, of stiff hairs;</text>
      <biological_entity id="o18987" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character constraint="on lower leaves" constraintid="o18988" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18988" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o18989" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o18987" id="r3209" name="consists_of" negation="false" src="d0_s5" to="o18989" />
    </statement>
    <statement id="d0_s6">
      <text>blades 15-70 cm long, 5-13 mm wide, glabrous.</text>
      <biological_entity id="o18990" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="70" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 13-45 cm, erect, rachis nodes hispid, hairs 3-6.5 mm, papillose-based, internodes scabrous;</text>
      <biological_entity id="o18991" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s7" to="45" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="rachis" id="o18992" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o18993" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o18994" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 4-10 cm, subverticillate, ascending, nodes hispid, hairs 2.5-4 mm, papillose-based, internodes scabrous;</text>
      <biological_entity constraint="primary" id="o18995" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="subverticillate" value_original="subverticillate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o18996" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o18997" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o18998" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>secondary branches short, spikelets subsessile, in clusters.</text>
      <biological_entity constraint="secondary" id="o18999" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o19000" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4-7 mm, hispid, hairs appressed, disarticulating at maturity.</text>
      <biological_entity id="o19001" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o19002" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s10" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes at least 1/2 as long as the spikelets;</text>
      <biological_entity constraint="lower" id="o19003" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o19004" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o19004" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o19005" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas apiculate or awned, awns to 18 mm;</text>
      <biological_entity constraint="lower" id="o19006" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o19007" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower paleas subequal to the lower lemmas, often purple;</text>
      <biological_entity constraint="lower" id="o19008" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="to lower lemmas" constraintid="o19009" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="often" name="coloration_or_density" notes="" src="d0_s14" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19009" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers of lower florets 1.5-3.6 mm, orange;</text>
      <biological_entity id="o19010" name="anther" name_original="anthers" src="d0_s15" type="structure" constraint="floret" constraint_original="floret; floret">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19011" name="floret" name_original="florets" src="d0_s15" type="structure" />
      <relation from="o19010" id="r3210" name="part_of" negation="false" src="d0_s15" to="o19011" />
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas 2.5-5 mm, elliptic or narrowly ovate, apices obtuse, with a membranous, soon-withering tip;</text>
      <biological_entity constraint="upper" id="o19012" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o19013" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o19014" name="tip" name_original="tip" src="d0_s16" type="structure">
        <character is_modifier="true" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="life_cycle" src="d0_s16" value="soon-withering" value_original="soon-withering" />
      </biological_entity>
      <relation from="o19013" id="r3211" name="with" negation="false" src="d0_s16" to="o19014" />
    </statement>
    <statement id="d0_s17">
      <text>anthers of upper florets shorter than those of the lower florets.</text>
      <biological_entity id="o19015" name="anther" name_original="anthers" src="d0_s17" type="structure" constraint="floret" constraint_original="floret; floret" />
      <biological_entity constraint="upper" id="o19016" name="floret" name_original="florets" src="d0_s17" type="structure" />
      <relation from="o19015" id="r3212" name="part_of" negation="false" src="d0_s17" to="o19016" />
    </statement>
    <statement id="d0_s18">
      <text>Caryopses to 3 mm. 2n = 54.</text>
      <biological_entity id="o19017" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19018" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Echinochloa polystachya grows in coastal marshes, often in standing water, from Texas, Louisiana, and Florida south through Mexico and the Caribbean islands to Argentina. Two varieties exist. Echinochloa polystachya var. polystachya has glabrous culms and leaf sheaths; Echinochloa polystachya var. spectabilis (Nees ex Trin.) Mart. Crov. has swollen, pubescent cauline nodes and pubescent leaf sheaths.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Fla.;Tex.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>