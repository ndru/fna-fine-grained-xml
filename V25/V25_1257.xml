<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">469</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="(Hitchc.) Honda" date="unknown" rank="section">Dichotomiflora</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">dichotomiflorum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">dichotomiflorum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section dichotomiflora;species dichotomiflorum;subspecies dichotomiflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dichotomiflorum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">geniculatum</taxon_name>
    <taxon_hierarchy>genus panicum;species dichotomiflorum;variety geniculatum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Panic d'automne dressé</other_name>
  <other_name type="common_name">Panic à fleures dichotom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 5-200 cm.</text>
      <biological_entity id="o13992" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths glabrous or sparsely pilose, not hispid with papillose-based hairs.</text>
      <biological_entity id="o13993" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character constraint="with hairs" constraintid="o13994" is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o13994" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels usually less than 3 mm and shorter than the spikelets.</text>
      <biological_entity id="o13995" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character constraint="than the spikelets" constraintid="o13996" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o13996" name="spikelet" name_original="spikelets" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Spikelets 2.3-3.8 mm, tapered from below the middle to the acuminate apices;</text>
      <biological_entity id="o13997" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s3" to="3.8" to_unit="mm" />
        <character constraint="from middle" constraintid="o13998" is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13998" name="middle" name_original="middle" src="d0_s3" type="structure" />
      <biological_entity id="o13999" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o13998" id="r2302" name="to" negation="false" src="d0_s3" to="o13999" />
    </statement>
    <statement id="d0_s4">
      <text>upper glumes and lower lemmas subcoriaceous.</text>
      <biological_entity constraint="upper" id="o14000" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o14001" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum dichotomiflorum subsp. dichotomiflorum is the most common of the three subspecies and is found throughout the range of the species. In the past, members of this subspecies have been treated as two different taxa, var. geniculatum (Alph. Wood) Fernald and var. dichotomiflorum, with more erect, slender plants having fewer long-exserted panicles with slender, ascending branches and less crowded spikelets being placed in var. dichotomiflorum. Such plants are more common in the southern part of the subspecies' range, but the traits are poorly correlated and the differences are at least in part affected by photoperiod, nighttime temperatures, and the time of seed germination.</discussion>
  
</bio:treatment>