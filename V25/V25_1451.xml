<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">575</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Flüggé" date="unknown" rank="species">notatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species notatum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">notatum</taxon_name>
    <taxon_name authority="Doll" date="unknown" rank="variety">latiflorum</taxon_name>
    <taxon_hierarchy>genus paspalum;species notatum;variety latiflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">notatum</taxon_name>
    <taxon_name authority="Parodi" date="unknown" rank="variety">saurae</taxon_name>
    <taxon_hierarchy>genus paspalum;species notatum;variety saurae</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Bahiagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o3270" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-110 cm, erect;</text>
      <biological_entity id="o3271" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o3272" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o3273" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm;</text>
      <biological_entity id="o3274" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-31 cm long, 2-10 mm wide, flat or conduplicate, glabrous or pubescent.</text>
      <biological_entity id="o3275" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="31" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, usually composed of a digitate pair of branches, 1-3 additional branches sometimes present below the terminal pair;</text>
      <biological_entity id="o3276" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" modifier="usually" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <biological_entity id="o3277" name="pair" name_original="pair" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o3278" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o3279" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="additional" value_original="additional" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3280" name="pair" name_original="pair" src="d0_s7" type="structure" />
      <relation from="o3276" id="r533" modifier="usually" name="composed of a" negation="false" src="d0_s7" to="o3277" />
      <relation from="o3276" id="r534" modifier="usually" name="composed of a" negation="false" src="d0_s7" to="o3278" />
      <relation from="o3279" id="r535" modifier="sometimes" name="present" negation="false" src="d0_s7" to="o3280" />
    </statement>
    <statement id="d0_s8">
      <text>branches 3-15 cm, diverging to erect;</text>
      <biological_entity id="o3281" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="15" to_unit="cm" />
        <character char_type="range_value" from="diverging" name="orientation" src="d0_s8" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.7-1.8 mm wide, narrowly winged, glabrous, margins scabrous, terminating in a spikelet, distal spikelets sometimes reduced.</text>
      <biological_entity constraint="branch" id="o3282" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3283" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o3284" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o3285" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o3283" id="r536" name="terminating in" negation="false" src="d0_s9" to="o3284" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.5-4 mm long, 2-2.8 mm wide, solitary, appressed to the branch axes, broadly elliptic to ovate or obovate, glabrous, light stramineous to white, apices obtuse to broadly acute.</text>
      <biological_entity id="o3286" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character constraint="to branch axes" constraintid="o3287" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="broadly elliptic" name="shape" notes="" src="d0_s10" to="ovate or obovate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="light stramineous" name="coloration" src="d0_s10" to="white" />
      </biological_entity>
      <biological_entity constraint="branch" id="o3287" name="axis" name_original="axes" src="d0_s10" type="structure" />
      <biological_entity id="o3288" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="broadly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o3289" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes glabrous, 5-veined;</text>
      <biological_entity constraint="upper" id="o3290" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas 5-veined, margins inrolled;</text>
      <biological_entity constraint="lower" id="o3291" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o3292" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s13" value="inrolled" value_original="inrolled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper florets light yellow to white.</text>
      <biological_entity constraint="upper" id="o3293" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="light yellow" name="coloration" src="d0_s14" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2-3 mm, white.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20, 30, 40.</text>
      <biological_entity id="o3294" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3295" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="30" value_original="30" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum notatum is native from Mexico through the Caribbean and Central America to Brazil and northern Argentina. It was introduced to the United States for forage, turf, and erosion control. It is now established, generally being found in disturbed areas and at the edges of forests in the southeastern United States.</discussion>
  <discussion>Paspalum notatum is sometimes treated as having distinct varieties. They are not recognized here because the variation among them is continuous. A number of cultivars have been developed for use as turf grasses; among these cultivars are 'Common Bahiagrass', 'Pensacola Bahiagrass', and 'Argentine Bahiagrass'.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tenn.;N.J.;Puerto Rico;Virgin Islands;Okla.;Miss.;Tex.;La.;Calif.;N.C.;Ala.;Ark.;Pacific Islands (Hawaii);S.C.;Va.;Ill.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>