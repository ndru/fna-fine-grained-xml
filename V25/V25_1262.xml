<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">472</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Repentia</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">coloratum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section repentia;species coloratum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Kleingrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, usu¬ally with short, knotty rhizomes.</text>
      <biological_entity id="o6349" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6350" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-140 cm tall, 1.5-2.5 mm thick, usually erect, rarely decumbent, firm;</text>
      <biological_entity id="o6351" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s2" to="140" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="texture" src="d0_s2" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or puberulent;</text>
      <biological_entity id="o6352" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous.</text>
      <biological_entity id="o6353" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths shorter than the internodes, glabrous or hispid, hairs papillose-based, rounded basally;</text>
      <biological_entity id="o6354" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o6355" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o6355" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity id="o6356" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-2 mm;</text>
      <biological_entity id="o6357" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 10-30 cm long, 2-8 mm wide, flat, glabrous or sparsely hirsute on 1 or both surfaces.</text>
      <biological_entity id="o6358" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o6359" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o6359" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 4-25 (40) cm long, 3-14 cm wide, exerted, lax;</text>
      <biological_entity id="o6360" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="40" value_original="40" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s8" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s8" to="14" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches 3-14 cm, opposite and alternate, ascending, glabrous, branching in the distal M;</text>
      <biological_entity constraint="primary" id="o6361" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6362" name="m" name_original="m" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 1-4 mm, appressed or spreading.</text>
      <biological_entity id="o6363" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.5-3.5 mm long, 1-1.2 mm wide, narrowly ovoid to ellipsoid, glabrous, acute.</text>
      <biological_entity id="o6364" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="narrowly ovoid" name="shape" src="d0_s11" to="ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 1-1.5 mm, about 1/3 as long as the spikelets, glabrous, 1-3-veined, acute;</text>
      <biological_entity constraint="lower" id="o6365" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
        <character constraint="as-long-as spikelets" constraintid="o6366" name="quantity" src="d0_s12" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6366" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes slightly exceeding the lower lemmas, glabrous, acute, scarcely separated from the lower lemmas;</text>
      <biological_entity constraint="upper" id="o6367" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character constraint="from lower lemmas" constraintid="o6369" is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s13" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="lower" id="o6368" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity constraint="lower" id="o6369" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <relation from="o6367" id="r1008" modifier="slightly" name="exceeding the" negation="false" src="d0_s13" to="o6368" />
    </statement>
    <statement id="d0_s14">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o6370" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas similar to the upper glumes;</text>
      <biological_entity constraint="lower" id="o6371" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <biological_entity constraint="upper" id="o6372" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <relation from="o6371" id="r1009" name="to" negation="false" src="d0_s15" to="o6372" />
    </statement>
    <statement id="d0_s16">
      <text>lower paleas 2-3 mm, oblong;</text>
      <biological_entity constraint="lower" id="o6373" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets 2-2.5 mm long, 0.8-1 mm wide, ellipsoid, widest below the middle, glabrous, smooth, shiny, apices lightly beaked.</text>
      <biological_entity constraint="upper" id="o6374" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s17" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character constraint="below middle florets" constraintid="o6375" is_modifier="false" name="width" src="d0_s17" value="widest" value_original="widest" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="middle" id="o6375" name="floret" name_original="florets" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18, 36, 41, 42, 43, 45, 54, 63 (United States material apparently usually tetraploid, with 2n = 36).</text>
      <biological_entity id="o6376" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="lightly" name="architecture_or_shape" src="d0_s17" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6377" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
        <character name="quantity" src="d0_s18" value="41" value_original="41" />
        <character name="quantity" src="d0_s18" value="42" value_original="42" />
        <character name="quantity" src="d0_s18" value="43" value_original="43" />
        <character name="quantity" src="d0_s18" value="45" value_original="45" />
        <character name="quantity" src="d0_s18" value="54" value_original="54" />
        <character name="quantity" src="d0_s18" value="63" value_original="63" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum coloratum is an African species that has been widely introduced into tropical and subtropical regions around the world. It is now established in the Flora region, growing in open, usually wet ground; it is also occasionally cultivated as a forage grass.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pacific Islands (Hawaii);N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>