<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">192</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">lindheimeri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species lindheimeri</taxon_hierarchy>
  </taxon_identification>
  <number>60</number>
  <other_name type="common_name">Lindheimer's muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o22357" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-150 cm, stout, erect, not rooting at the lower nodes;</text>
      <biological_entity id="o22358" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at lower nodes" constraintid="o22359" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22359" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly glabrous, sometimes puberulent below the nodes.</text>
      <biological_entity id="o22360" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o22361" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o22361" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter or longer than the internodes, glabrous, basal sheaths laterally compressed, keeled, not becoming spirally coiled when old;</text>
      <biological_entity id="o22362" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character constraint="than the internodes" constraintid="o22363" is_modifier="false" name="length_or_size" src="d0_s4" value="shorter or longer" value_original="shorter or longer" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22363" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o22365" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 10-35 mm, firm and brown basally, membranous distally, acuminate;</text>
      <biological_entity id="o22366" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="35" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="distally" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 25-55 cm long, 2-5 mm wide, flat or folded, firm, scabridulous abaxially, scabrous and shortly pubescent adaxially.</text>
      <biological_entity id="o22367" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s6" to="55" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="texture" src="d0_s6" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="shortly; adaxially" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 15-50 cm long, 0.6-3 cm wide, often purplish-tinged;</text>
      <biological_entity id="o22368" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purplish-tinged" value_original="purplish-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-7 cm, appressed or strongly ascending, rarely spreading as much as 20° from the rachises;</text>
      <biological_entity constraint="primary" id="o22369" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="from rachises" constraintid="o22370" is_modifier="false" modifier="20°" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o22370" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.5-1.2 mm, scabrous.</text>
      <biological_entity id="o22371" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.4-3.5 mm, light grayish.</text>
      <biological_entity id="o22372" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="light grayish" value_original="light grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes equal, 2-3.5 mm, shorter than or equal to the florets, scabrous or smooth, 1-veined, obtuse to acute, occasionally bifid and the teeth to 0.3 mm, unawned, rarely mucronate, mucros less than 0.2 mm;</text>
      <biological_entity id="o22373" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character constraint="than or equal to the florets" constraintid="o22374" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s11" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o22374" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o22375" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o22376" name="mucro" name_original="mucros" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2.4-3.5 mm, lanceolate, scabrous or smooth, rarely puberulent near the base, apices obtuse to acute, unawned or awned, awns to 4 mm, straight;</text>
      <biological_entity id="o22377" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="near base" constraintid="o22378" is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o22378" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o22379" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o22380" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas 2.4-3.5 mm, lanceolate, obtuse;</text>
      <biological_entity id="o22381" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.1-1.5 mm, purplish.</text>
      <biological_entity id="o22382" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 1.2-1.6 mm, fusiform, reddish-brown.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20, 26.</text>
      <biological_entity id="o22383" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22384" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia lindheimeri grows in sandy draws to rocky, calcareous soils, generally in open areas, at elevations of 150-500 m. It is an uncommon species throughout its range, which includes northern Mexico in addition to southern Texas, but it is also grown as an ornamental. It differs from the closely related M. longiligula in its compressed-keeled basal sheaths, grayish spikelets, and, when present, bifid glume apices.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>