<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Pilg.) B.K. Simon &amp; S.W.L. Jacobs" date="unknown" rank="genus">MEGATHYRSUS</taxon_name>
    <taxon_name authority="(Jacq.) B.K. Simon &amp; S.W.L. Jacobs" date="unknown" rank="species">maximus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus megathyrsus;species maximus</taxon_hierarchy>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Guinea grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with short, thick rhizomes.</text>
      <biological_entity id="o8273" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8274" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (60) 100-250 cm tall, about 10 mm thick, mostly erect, sometimes geniculate and rooting at the lower nodes;</text>
      <biological_entity id="o8275" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="cm" value="60" value_original="60" />
        <character char_type="range_value" from="100" from_unit="cm" name="height" src="d0_s2" to="250" to_unit="cm" />
        <character name="thickness" src="d0_s2" unit="mm" value="10" value_original="10" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character constraint="at lower nodes" constraintid="o8276" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8276" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes pubescent or glabrous.</text>
      <biological_entity id="o8277" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths usually shorter than the internodes, glabrous or pubescent, sometimes with papillose-based hairs, margins sometimes ciliate;</text>
      <biological_entity id="o8278" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o8279" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8279" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o8280" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o8281" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o8278" id="r1339" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o8280" />
    </statement>
    <statement id="d0_s5">
      <text>collars densely pubescent, hairs appressed or divergent;</text>
      <biological_entity id="o8282" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8283" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 1-3 mm;</text>
      <biological_entity id="o8284" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades (15) 30-75 (100) cm long, 10-35 mm wide, flat, erect or ascending, glabrous or pubescent, sometimes with appressed papillose-based hairs, margins scabrous, sometimes ciliate basally, midveins conspicuous, sunken, whitish.</text>
      <biological_entity id="o8285" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s7" to="75" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="35" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8286" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o8287" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes; basally" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8288" name="midvein" name_original="midveins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="sunken" value_original="sunken" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o8285" id="r1340" modifier="sometimes" name="with" negation="false" src="d0_s7" to="o8286" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 20-65 cm, about 1/3 as wide as long, open, rachises smooth or scabrous;</text>
      <biological_entity id="o8289" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s8" to="65" to_unit="cm" />
        <character name="quantity" src="d0_s8" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o8290" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches usually more than 20, 12-40 cm, axes 0.4-0.6 mm wide, not winged, ascending, those of the lower node (s) verticillate and pilose at the base, upper axils glabrous, lower branches naked basally;</text>
      <biological_entity constraint="primary" id="o8291" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" upper_restricted="false" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s9" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8292" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="verticillate" value_original="verticillate" />
        <character constraint="at base" constraintid="o8294" is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8293" name="node" name_original="node" src="d0_s9" type="structure" />
      <biological_entity id="o8294" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="upper" id="o8295" name="axil" name_original="axils" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8296" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s9" value="naked" value_original="naked" />
      </biological_entity>
      <relation from="o8292" id="r1341" name="part_of" negation="false" src="d0_s9" to="o8293" />
    </statement>
    <statement id="d0_s10">
      <text>secondary and tertiary branches well-developed;</text>
      <biological_entity constraint="secondary and tertiary" id="o8297" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="development" src="d0_s10" value="well-developed" value_original="well-developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.5-1.5 mm, unequal, straight or curved, glabrous or with a single setaceous hair near the apex.</text>
      <biological_entity id="o8298" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="with a single setaceous hair" />
      </biological_entity>
      <biological_entity id="o8299" name="hair" name_original="hair" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="single" value_original="single" />
        <character is_modifier="true" name="shape" src="d0_s11" value="setaceous" value_original="setaceous" />
      </biological_entity>
      <biological_entity id="o8300" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <relation from="o8298" id="r1342" name="with" negation="false" src="d0_s11" to="o8299" />
      <relation from="o8299" id="r1343" name="near" negation="false" src="d0_s11" to="o8300" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 2.7-3.6 mm long, 0.9-1.1 mm wide, oblong-ellipsoid, usually glabrous (rarely densely covered with papillose-based hairs), solitary, paired (or in triplets), usually appressed to the branch axes.</text>
      <biological_entity id="o8301" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s12" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s12" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-ellipsoid" value_original="oblong-ellipsoid" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s12" value="paired" value_original="paired" />
        <character constraint="to branch axes" constraintid="o8302" is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="branch" id="o8302" name="axis" name_original="axes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes scarcely separate, rachilla between the glumes not pronounced;</text>
      <biological_entity id="o8303" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s13" value="separate" value_original="separate" />
      </biological_entity>
      <biological_entity constraint="between glumes" constraintid="o8305" id="o8304" name="rachillum" name_original="rachilla" src="d0_s13" type="structure" constraint_original="between  glumes, ">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <biological_entity id="o8305" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.8-1.2 mm, 1-3-veined, obtuse or truncate, glabrous;</text>
      <biological_entity constraint="lower" id="o8306" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s14" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 2.1-3.5 mm, 5-veined, glabrous;</text>
      <biological_entity constraint="upper" id="o8307" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 2.1-3.5 mm, subequal, glabrous, 5-veined, without cross venation, acute, muticous or mucronate;</text>
      <biological_entity constraint="lower" id="o8308" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="shape" notes="" src="d0_s16" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s16" value="muticous" value_original="muticous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o8309" name="vein" name_original="venation" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="cross" value_original="cross" />
      </biological_entity>
      <relation from="o8308" id="r1344" name="without" negation="false" src="d0_s16" to="o8309" />
    </statement>
    <statement id="d0_s17">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o8310" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper lemmas 1.9-2.4 mm, ellipsoid, pale, glabrous, apices acute, mucronulate;</text>
      <biological_entity constraint="upper" id="o8311" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s18" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale" value_original="pale" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8312" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s18" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 1.2-2.2 mm. 2n = 18, 32, 36, 44, 48.</text>
      <biological_entity id="o8313" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s19" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8314" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="18" value_original="18" />
        <character name="quantity" src="d0_s19" value="32" value_original="32" />
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
        <character name="quantity" src="d0_s19" value="44" value_original="44" />
        <character name="quantity" src="d0_s19" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Megathyrsus maximus is an important forage grass that is native to Africa. In the Flora region, it grows in fields, waste places,   stream   banks,   and hammocks.  It is cultivated widely as a forage grass at low elevations, especially near the coast, and often escapes.</discussion>
  <discussion>There are usually two varieties recognized. Only Megathyrsus maximus (Jacq.) R.D. Webster var. maxima, which has glabrous spikelets, is known in the Flora area. Specimens with densely pubescent spikelets belong to U. maxima var. trichoglumis (Robyns) R.D. Webster.</discussion>
  
</bio:treatment>