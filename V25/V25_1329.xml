<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">UROCHLOA</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="species">panicoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus urochloa;species panicoides</taxon_hierarchy>
  </taxon_identification>
  <number>14</number>
  <other_name type="common_name">Liverseed grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o27169" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (5) 10-55 (100) cm, erect to decumbent, usually rooting at the lower nodes;</text>
      <biological_entity id="o27170" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="55" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character constraint="at lower nodes" constraintid="o27171" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o27171" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes pubescent;</text>
      <biological_entity id="o27172" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous or sparsely pubescent.</text>
      <biological_entity id="o27173" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths hispid, margins ciliate distally;</text>
      <biological_entity id="o27174" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o27175" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-1.5 mm;</text>
      <biological_entity id="o27176" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (2) 5-25 cm long, 5-18 mm wide, both surfaces usually with papillose-based hairs, rarely glabrous, bases rounded to subcordate, ciliate, with papillose-based hairs.</text>
      <biological_entity id="o27177" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27178" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27179" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o27180" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="subcordate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27181" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o27178" id="r4622" name="with" negation="false" src="d0_s6" to="o27179" />
      <relation from="o27180" id="r4623" name="with" negation="false" src="d0_s6" to="o27181" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3.5-10 cm long, 2-7 (10) cm wide, with 2-7 (10) spikelike primary branches in 2 ranks;</text>
      <biological_entity id="o27182" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o27183" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="10" value_original="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o27184" name="rank" name_original="ranks" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o27182" id="r4624" name="with" negation="false" src="d0_s7" to="o27183" />
      <relation from="o27183" id="r4625" name="in" negation="false" src="d0_s7" to="o27184" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 1-7 cm, axes about 0.9-1.2 mm wide, flat, scabrous and ciliate with papillose-based hairs;</text>
      <biological_entity constraint="primary" id="o27185" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27186" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character constraint="with hairs" constraintid="o27187" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o27187" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>secondary branches rarely present;</text>
      <biological_entity constraint="secondary" id="o27188" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels shorter than the spikelets, frequently with 1-5 long hairs.</text>
      <biological_entity id="o27189" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than the spikelets" constraintid="o27190" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o27190" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o27191" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
      </biological_entity>
      <relation from="o27189" id="r4626" modifier="frequently" name="with" negation="false" src="d0_s10" to="o27191" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.5-4.5 (5.5) mm long, about 1.5-2 mm wide, ellipsoid, solitary, in 2 rows, appressed to the branches.</text>
      <biological_entity id="o27192" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character name="length" src="d0_s11" unit="mm" value="5.5" value_original="5.5" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character constraint="to branches" constraintid="o27194" is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o27193" name="row" name_original="rows" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o27194" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <relation from="o27192" id="r4627" name="in" negation="false" src="d0_s11" to="o27193" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes scarcely separated;</text>
      <biological_entity id="o27195" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s12" value="separated" value_original="separated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1-1.6 mm, 1/4-1/3 (1/2) as long as the spikelets, 3-5-veined, clasping the base of the spikelets, glabrous;</text>
      <biological_entity constraint="lower" id="o27196" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.6" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o27197" from="1/4" name="quantity" src="d0_s13" to="1/31/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o27197" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <biological_entity id="o27198" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s13" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27199" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o27198" id="r4628" name="part_of" negation="false" src="d0_s13" to="o27199" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 3.2-4.3 (5) mm, 9-11 (13) -veined, glabrous;</text>
      <biological_entity constraint="upper" id="o27200" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s14" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="9-11(13)-veined" value_original="9-11(13)-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o27201" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 3.2-4.3 (5) mm long, glabrous, 5 (-7) -veined;</text>
      <biological_entity constraint="lower" id="o27202" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="length" src="d0_s16" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s16" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5(-7)-veined" value_original="5(-7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower paleas present;</text>
      <biological_entity constraint="lower" id="o27203" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper lemmas 2.6-3.5 mm long, about 1.5 mm wide, apices rounded, awned, awns (0.3) 0.6-1 mm;</text>
      <biological_entity constraint="upper" id="o27204" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" src="d0_s18" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s18" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o27205" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o27206" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 0.8-1 mm.</text>
      <biological_entity id="o27207" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 2-2.5 mm. 2n = 30, 36, 48.</text>
      <biological_entity id="o27208" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27209" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="30" value_original="30" />
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
        <character name="quantity" src="d0_s20" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Urochloa panicoides is native to Africa, and is considered a noxious weed by the U.S. Department of Agriculture. In the Western Hemisphere, it has been introduced into the southern United States, Mexico, and Argentina. Within the Flora region, it has been reported from disturbed sites in the southern and Gulf Coast areas of Texas (Wipff et al. 1993), but it is expected to spread. Populations from New Mexico have been destroyed.</discussion>
  <discussion>Urochloa panicoides has morphological forms with glabrous, pubescent, or setosely-fringed spikelets. Although the three forms have been recognized taxonomically, Clayton and Renvoize (1982) state that they appear to be of no taxonomic significance. Only the glabrous form is known to occur in the Flora area.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>