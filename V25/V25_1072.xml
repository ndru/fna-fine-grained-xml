<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="species">violascens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species violascens</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>17</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or of indefinite duration.</text>
      <biological_entity id="o13310" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="of indefinite" value_original="of indefinite" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13311" name="indefinite" name_original="indefinite" src="d0_s0" type="structure" />
      <relation from="o13310" id="r2187" name="part_of" negation="false" src="d0_s0" to="o13311" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-60 cm, erect, usually not branching from the upper nodes;</text>
      <biological_entity id="o13312" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from upper nodes" constraintid="o13313" is_modifier="false" modifier="usually not" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13313" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>nodes 3-4.</text>
      <biological_entity id="o13314" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or sparsely pubescent;</text>
      <biological_entity id="o13315" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.6-2.5 mm;</text>
      <biological_entity id="o13316" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1.5-9 cm long, 3-5 mm wide, glabrous, with papillose-based hairs basally.</text>
      <biological_entity id="o13317" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13318" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o13317" id="r2188" name="with" negation="false" src="d0_s5" to="o13318" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 2-7 spikelike primary branches in 1-2 verticils;</text>
      <biological_entity id="o13319" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o13320" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o13321" name="verticil" name_original="verticils" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <relation from="o13319" id="r2189" name="with" negation="false" src="d0_s6" to="o13320" />
      <relation from="o13320" id="r2190" name="in" negation="false" src="d0_s6" to="o13321" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches 3-12 cm, erect to ascend¬ing, axes 0.6-1 mm wide, wing-margined, wings at least 1/2 as wide as the midribs, lower and middle portions of the branches bearing spikelets in groups of 3 (4, 5);</text>
      <biological_entity constraint="primary" id="o13322" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character constraint="to ing" constraintid="o13323" is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o13323" name="ing" name_original="ing" src="d0_s7" type="structure" />
      <biological_entity id="o13324" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity id="o13325" name="wing" name_original="wings" src="d0_s7" type="structure">
        <character constraint="as-wide-as midribs" constraintid="o13326" name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o13326" name="midrib" name_original="midribs" src="d0_s7" type="structure" />
      <biological_entity constraint="lower and middle" id="o13327" name="portion" name_original="portions" src="d0_s7" type="structure" />
      <biological_entity id="o13328" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o13329" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o13330" name="group" name_original="groups" src="d0_s7" type="structure">
        <character modifier="of 3 ( 4" name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <relation from="o13327" id="r2191" name="part_of" negation="false" src="d0_s7" to="o13328" />
      <relation from="o13327" id="r2192" name="bearing" negation="false" src="d0_s7" to="o13329" />
      <relation from="o13327" id="r2193" name="in" negation="false" src="d0_s7" to="o13330" />
    </statement>
    <statement id="d0_s8">
      <text>secondary branches rarely present;</text>
      <biological_entity constraint="secondary" id="o13331" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>axillary inflorescences absent.</text>
      <biological_entity constraint="axillary" id="o13332" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 1.2-1.7 mm, homomorphic, narrowly elliptic.</text>
      <biological_entity id="o13333" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="homomorphic" value_original="homomorphic" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent or a veinless, membranous rim;</text>
      <biological_entity constraint="lower" id="o13334" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="veinless" value_original="veinless" />
      </biological_entity>
      <biological_entity id="o13335" name="rim" name_original="rim" src="d0_s11" type="structure">
        <character is_modifier="true" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 1.2-1.4 mm, 1/2 as long as to almost equaling the upper lemmas, 3-veined, appressed-pubescent, hairs minutely verrucose;</text>
      <biological_entity constraint="upper" id="o13336" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
        <character constraint="as-long-as upper lemmas" constraintid="o13337" name="quantity" src="d0_s12" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13337" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="almost" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o13338" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s12" value="verrucose" value_original="verrucose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas 1.2-1.7 mm, 5-7-veined, veins equally spaced, region between the 2 inner lateral-veins and the margins appressed-pubescent, hairs 0.3-0.5 mm, smooth or minutely verrucose (use 50x magnification), verrucose hairs most abundant near the lemma bases;</text>
      <biological_entity constraint="lower" id="o13339" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o13340" name="vein" name_original="veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="equally" name="arrangement" src="d0_s13" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity constraint="between lateral-vein and margins" constraintid="o13342-o13343" id="o13341" name="region" name_original="region" src="d0_s13" type="structure" constraint_original="between  lateral-vein and  margins, ">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13342" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13343" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o13344" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s13" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity id="o13345" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="relief" src="d0_s13" value="verrucose" value_original="verrucose" />
        <character constraint="near lemma bases" constraintid="o13346" is_modifier="false" name="quantity" src="d0_s13" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o13346" name="base" name_original="bases" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas light-brown when immature, dark-brown at maturity;</text>
      <biological_entity constraint="upper" id="o13347" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s14" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.4-0.6 mm. 2n = 36.</text>
      <biological_entity id="o13348" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13349" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria violascens is a weedy species that is native to tropical regions of the Eastern Hemisphere. It is now established in the Flora region, primarily in the south-eastern United States, and in Mexico and Central America. It grows in disturbed sites.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Mass.;Miss.;Tex.;Ind.;La.;Ala.;Tenn.;N.C.;S.C.;Pacific Islands (Hawaii);Ga.;Okla.;Ky.;Fla.;Ark.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>