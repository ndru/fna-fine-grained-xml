<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">631</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="treatment_page">652</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="A. Gray in J. C. Ives" date="1861" rank="species">newberryi</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Ives, Rep. Colorado R.</publication_title>
      <place_in_publication>4: 6. 1861</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species newberryi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094919</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physaria</taxon_name>
    <taxon_name authority="(Hooker) A. Gray" date="unknown" rank="species">didymocarpa</taxon_name>
    <taxon_name authority="(A. Gray) M. E. Jones" date="unknown" rank="variety">newberryi</taxon_name>
    <taxon_hierarchy>genus Physaria;species didymocarpa;variety newberryi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o10793" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched, (branches often covered with persistent leaf-bases, cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>densely (silvery) pubescent, trichomes rays fused at least 1/2 their length.</text>
      <biological_entity id="o10794" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="trichomes" id="o10795" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="at least" name="fusion" src="d0_s2" value="fused" value_original="fused" />
        <character name="length" src="d0_s2" value="1/2" value_original="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems several from base, ascending to erect (arising laterally, unbranched), 0.5–1 (–2.5) dm.</text>
      <biological_entity id="o10796" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o10797" is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s3" to="erect" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="1" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o10797" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (ascending to erect, petiole slender);</text>
      <biological_entity constraint="basal" id="o10798" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to obovate, 3–8 cm, (base tapering to petiole), margins incised or dentate with broad teeth, (apex acute to obtuse).</text>
      <biological_entity id="o10799" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10800" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character constraint="with teeth" constraintid="o10801" is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o10801" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: blade linear-oblanceolate to oblanceolate, 1–2 cm, margins entire.</text>
      <biological_entity constraint="cauline" id="o10802" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o10803" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10804" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes dense (elongated or not in fruit, 2.5–8.5 (–10) cm).</text>
      <biological_entity id="o10806" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <relation from="o10805" id="r757" name="in" negation="false" src="d0_s7" to="o10806" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (divaricate, straight), 5–11 (–15) mm, (rigid, fruits not pendent on arching pedicels).</text>
      <biological_entity id="o10805" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character name="length" src="d0_s7" value="not" value_original="not" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" notes="" src="d0_s7" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10807" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o10805" id="r758" name="fruiting" negation="false" src="d0_s8" to="o10807" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (greenish yellow), lanceolate, 6–8.5 mm, (saccate and cucullate);</text>
      <biological_entity id="o10808" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10809" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals spatulate to narrowly oblanceolate, 7–10 (–12) mm.</text>
      <biological_entity id="o10810" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10811" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s10" to="narrowly oblanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits didymous, sides curved and angular, highly inflated, 6–16 × 8–12 mm, (papery, apical sinus broad and concave);</text>
      <biological_entity id="o10812" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="didymous" value_original="didymous" />
      </biological_entity>
      <biological_entity id="o10813" name="side" name_original="sides" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="angular" value_original="angular" />
        <character is_modifier="false" modifier="highly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves (retaining seeds after dehiscence, distinctly 2-keeled on side away from replum), pubescent, trichomes appressed;</text>
      <biological_entity id="o10814" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10815" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum linear to linear-lanceolate, as wide as or wider than fruit, apex acute;</text>
      <biological_entity id="o10816" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s13" to="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o10818" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o10817" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o10819" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o10816" id="r759" name="as wide as" negation="false" src="d0_s13" to="o10818" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 4–8 per ovary;</text>
      <biological_entity id="o10820" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10821" from="4" name="quantity" src="d0_s14" to="8" />
      </biological_entity>
      <biological_entity id="o10821" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 2–9 mm, (usually not exceeding sinus).</text>
      <biological_entity id="o10822" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds slightly flattened, (ovate).</text>
      <biological_entity id="o10823" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <other_name type="common_name">Newberry twinpod</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Physaria newberryi, with its unusual fruits, can be confused with 15. P. chambersii. In P. chambersii, the sides of the fruit are flat, the style always exceeds the top, or shoulders, of the fruit, and shoulders form an angle that does not curve in toward the style. In P. newberryi, the sides of the fruit are concave, the styles are shorter than shoulders of the silicle (except in subsp. yesicola), and shoulders of the silicle form a curved, inward arching crown on the fruit.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles less than 4 mm, shorter than fruit sinuses.</description>
      <determination>57a Physaria newberryi subsp. newberryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 5-9 mm, longer than fruit sinuses.</description>
      <determination>57b Physaria newberryi subsp. yesicola</determination>
    </key_statement>
  </key>
</bio:treatment>