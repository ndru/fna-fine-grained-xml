<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">553</other_info_on_meta>
    <other_info_on_meta type="illustration_page">541</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">euclidieae</taxon_name>
    <taxon_name authority="W. T. Aiton in W. Aiton and W. T. Aiton" date="unknown" rank="genus">euclidium</taxon_name>
    <taxon_name authority="(Linnaeus) W. T. Aiton in W. Aiton and W. T. Aiton" date="1812" rank="species">syriacum</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>4: 74. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe euclidieae;genus euclidium;species syriacum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009537</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Anastatica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">syriaca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 2: 895. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Anastatica;species syriaca;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bunias</taxon_name>
    <taxon_name authority="(Linnaeus) M. Bieberstein" date="unknown" rank="species">syriaca</taxon_name>
    <taxon_hierarchy>genus Bunias;species syriaca;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scabrous throughout, trichomes to 1 mm.</text>
      <biological_entity id="o507" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence_or_relief" src="d0_s0" value="scabrous" value_original="scabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o508" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often ascending, (rigid), usually branched basally and near middle, (0.4–) 1–4 (–4.5) dm.</text>
      <biological_entity id="o509" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually; basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s1" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o510" name="middle" name_original="middle" src="d0_s1" type="structure" />
      <relation from="o509" id="r34" name="near" negation="false" src="d0_s1" to="o510" />
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves similar to cauline.</text>
      <biological_entity constraint="basal" id="o511" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o512" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o511" id="r35" name="to" negation="false" src="d0_s2" to="o512" />
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves petiolate [(0.2–) 0.5–2 (–2.5) cm] or (distal) sessile or subsessile;</text>
      <biological_entity constraint="cauline" id="o513" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblong, oblong-lanceolate, or elliptic, (1–) 1.5–7 (–9) cm × (3–) 7–20 (–30) mm (smaller distally), base cuneate, margins usually entire, dentate, or repand, rarely pinnatifid, apex acute or obtuse.</text>
      <biological_entity id="o514" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s4" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o515" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o516" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels appressed to rachis, 0.5–1 (–1.2) mm (ca. 1/2 as wide as fruit).</text>
      <biological_entity id="o517" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o518" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <biological_entity id="o519" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o517" id="r36" name="fruiting" negation="false" src="d0_s5" to="o518" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 0.6–0.9 × 0.2–0.4 mm, sparsely pubescent;</text>
      <biological_entity id="o520" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o521" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s6" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 0.9–1.3 × 0.1–0.2 mm, claw 0.4–0.6 mm;</text>
      <biological_entity id="o522" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o523" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s7" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o524" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 0.5–0.8 mm;</text>
      <biological_entity id="o525" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o526" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 0.1–0.2 mm.</text>
      <biological_entity id="o527" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o528" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits erect, 0.2–0.3 cm × 1.5–2 mm, 2-seeded;</text>
      <biological_entity id="o529" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s10" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-seeded" value_original="2-seeded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>replum expanded, to 1.5 mm wide basally, narrowed to apex;</text>
      <biological_entity id="o530" name="replum" name_original="replum" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="expanded" value_original="expanded" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="basally" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character constraint="to apex" constraintid="o531" is_modifier="false" name="shape" src="d0_s11" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o531" name="apex" name_original="apex" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style curved away from rachis, subconical, 1–1.8 mm, sparsely pubescent.</text>
      <biological_entity id="o532" name="style" name_original="style" src="d0_s12" type="structure">
        <character constraint="away-from rachis" constraintid="o533" is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="subconical" value_original="subconical" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o533" name="rachis" name_original="rachis" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.3–1.7 × 0.8–1.2 mm. 2n = 14.</text>
      <biological_entity id="o534" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s13" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o535" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, roadsides, flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Colo., Idaho, Mass., Oreg., Utah, Wash., Wyo.; Europe; Asia; introduced also in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Euclidium syriacum is known in the flora area from relatively few collections. The Massachusetts record, Knowlton s.n. (GH), was collected nearly a century ago; it is not known if the species has persisted in that state.</discussion>
  
</bio:treatment>