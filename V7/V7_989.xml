<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">605</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Rollins" date="1979" rank="genus">dimorphocarpa</taxon_name>
    <taxon_name authority="(Rafinesque) Rollins" date="1993" rank="species">candicans</taxon_name>
    <place_of_publication>
      <publication_title>Cruciferae Continental N. Amer.,</publication_title>
      <place_in_publication>361. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus dimorphocarpa;species candicans</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095138</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iberis</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">candicans</taxon_name>
    <place_of_publication>
      <publication_title>Atlantic J.</publication_title>
      <place_in_publication>1: 146. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Iberis;species candicans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dimorphocarpa</taxon_name>
    <taxon_name authority="(Payson) Rollins" date="unknown" rank="species">palmeri</taxon_name>
    <taxon_hierarchy>genus Dimorphocarpa;species palmeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">wislizeni</taxon_name>
    <taxon_name authority="Payson" date="unknown" rank="variety">palmeri</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species wislizeni;variety palmeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials.</text>
      <biological_entity id="o2200" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched proximally, branched distally, (3–) 4–8 (–10) dm.</text>
      <biological_entity id="o2202" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s1" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole 1–4 (–6) cm;</text>
      <biological_entity constraint="basal" id="o2203" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2204" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to oblong or ovate, (2–) 4–8 (–10) cm × (10–) 15–25 (–40) mm, base cuneate to obtuse, margins dentate.</text>
      <biological_entity constraint="basal" id="o2205" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2206" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblong or ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s3" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2207" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity id="o2208" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o2209" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually ovate to narrowly oblong, rarely lanceolate, base obtuse to truncate, margins entire, sometimes repand.</text>
      <biological_entity id="o2210" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s5" to="narrowly oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o2211" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse to truncate" value_original="obtuse to truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels divaricate, (8–) 10–16 (–20) mm.</text>
      <biological_entity id="o2212" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2213" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o2212" id="r194" name="fruiting" negation="false" src="d0_s6" to="o2213" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals 3.5–5 × 1–1.5 mm, pubescent abaxially;</text>
      <biological_entity id="o2214" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2215" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white or lavender, (7–) 8–10 (–12) × 4–6 (–7) mm, attenuate to claw, claw 2–3 mm, not expanded basally;</text>
      <biological_entity id="o2216" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2217" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s8" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character constraint="to claw" constraintid="o2218" is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o2218" name="claw" name_original="claw" src="d0_s8" type="structure" />
      <biological_entity id="o2219" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not; basally" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments lavender or white, 3–4 mm;</text>
      <biological_entity id="o2220" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2221" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 1–1.5 mm.</text>
      <biological_entity id="o2222" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2223" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits: each valve suborbicular or orbicular, (7–) 8–10 × (6–) 7–10 mm, base rounded, apex truncate, with or without narrow margin beyond indurated part surrounding seeds, glabrous or pubescent;</text>
      <biological_entity id="o2224" name="fruit" name_original="fruits" src="d0_s11" type="structure" />
      <biological_entity id="o2225" name="valve" name_original="valve" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s11" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" src="d0_s11" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2226" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o2227" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2228" name="margin" name_original="margin" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o2229" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s11" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <relation from="o2227" id="r195" name="with or without" negation="false" src="d0_s11" to="o2228" />
    </statement>
    <statement id="d0_s12">
      <text>style (0.3–) 0.6–1 (–1.2) mm.</text>
      <biological_entity id="o2230" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o2231" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds suborbicular-ovoid, 3–4 × 2.5–3 mm. 2n = 18.</text>
      <biological_entity id="o2232" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular-ovoid" value_original="suborbicular-ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2233" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy hills and plains, prairies, sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy hills" />
        <character name="habitat" value="plains" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., N.Mex., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>