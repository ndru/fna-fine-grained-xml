<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="treatment_page">542</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="(Hooker) Wettstein" date="1889" rank="species">menziesii</taxon_name>
    <place_of_publication>
      <publication_title>Oesterr. Bot. Z.</publication_title>
      <place_in_publication>39: 283. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species menziesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095009</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hesperis</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">menziesii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 60. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hesperis;species menziesii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="A. Heller" date="unknown" rank="species">grandiflorus</taxon_name>
    <taxon_hierarchy>genus Cheiranthus;species grandiflorus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived).</text>
      <biological_entity id="o37918" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes of leaves 2–5 (–7) -rayed on adaxial leaf surface.</text>
      <biological_entity id="o37920" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o37921" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="leaf" id="o37922" name="surface" name_original="surface" src="d0_s2" type="structure" constraint_original="adaxial leaf" />
      <relation from="o37920" id="r2560" name="part_of" negation="false" src="d0_s2" to="o37921" />
      <relation from="o37920" id="r2561" name="on" negation="false" src="d0_s2" to="o37922" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched or branched distally, 0.2–2.5 (–3.5) dm.</text>
      <biological_entity id="o37923" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s3" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade (fleshy or not), spatulate, 2–10 cm × 5–15 mm, base cuneate, margins dentate, entire, or lobed, apex obtuse.</text>
      <biological_entity constraint="basal" id="o37924" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37925" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o37926" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o37927" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o37928" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) sessile or short-petiolate;</text>
      <biological_entity constraint="cauline" id="o37929" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade margins entire or dentate.</text>
      <biological_entity constraint="blade" id="o37930" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes elongated slightly in fruit.</text>
      <biological_entity id="o37932" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels horizontal to divaricate, slender, narrower than fruit, 4–15 mm.</text>
      <biological_entity id="o37931" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o37932" is_modifier="false" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o37934" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o37933" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o37934" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <relation from="o37931" id="r2562" name="fruiting" negation="false" src="d0_s8" to="o37933" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong, 7–14 mm, lateral pair saccate basally;</text>
      <biological_entity id="o37935" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37936" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, usually suborbicular, rarely obovate, 15–30 × 6–14 mm, claw 10–15 mm, apex rounded;</text>
      <biological_entity id="o37937" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o37938" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37939" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37940" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filaments 10–13 mm;</text>
      <biological_entity id="o37941" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o37942" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 3–4 mm.</text>
      <biological_entity id="o37943" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o37944" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits widely spreading or divaricate, linear, straight, not torulose, 3–14 cm × 2–4 mm, terete when green, becoming latiseptate when dry, not striped;</text>
      <biological_entity id="o37945" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s13" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="when green" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s13" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s13" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves with obscure midvein, pubescent outside, trichomes (2 or) 3 or 4 (or 6) -rayed, glabrous inside;</text>
      <biological_entity id="o37946" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o37947" name="midvein" name_original="midvein" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o37948" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character name="quantity" src="d0_s14" value="4-rayed" value_original="4-rayed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o37946" id="r2563" name="with" negation="false" src="d0_s14" to="o37947" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 32–74 per ovary;</text>
      <biological_entity id="o37949" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o37950" from="32" name="quantity" src="d0_s15" to="74" />
      </biological_entity>
      <biological_entity id="o37950" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style cylindrical, slender, 0.3–2 mm, sparsely pubescent;</text>
      <biological_entity id="o37951" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma 2-lobed, lobes as long as wide.</text>
      <biological_entity id="o37952" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o37953" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds oblong (compressed), 1.8–2.8 (–3.5) × 1–2 mm;</text>
    </statement>
    <statement id="d0_s19">
      <text>winged distally (wing narrow all around).</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity id="o37954" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s18" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s18" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37955" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stabilized coastal sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sand dunes" modifier="stabilized" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erysimum menziesii is restricted to the coasts of Humboldt, Mendocino, and Monterey counties. R. A. Price (1993) divided the species into four subspecies, three of which were invalidly published.</discussion>
  
</bio:treatment>