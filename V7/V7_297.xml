<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Staria S. Vanderpool</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="genus">OXYSTYLIS</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont, Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>312. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus OXYSTYLIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oxys, sharp, and stylos, pillar, alluding to style</other_info_on_name>
    <other_info_on_name type="fna_id">123531</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual (unpleasantly scented).</text>
      <biological_entity id="o9710" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched, or sparsely branched laterally;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or glabrate.</text>
      <biological_entity id="o9711" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparsely; laterally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules 3–8-palmatifid, minute;</text>
      <biological_entity id="o9712" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9713" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="3-8-palmatifid" value_original="3-8-palmatifid" />
        <character is_modifier="false" name="size" src="d0_s3" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole without pulvinus (slightly longer than leaflets);</text>
      <biological_entity id="o9714" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9715" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o9716" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o9715" id="r685" name="without" negation="false" src="d0_s4" to="o9716" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3.</text>
      <biological_entity id="o9717" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9718" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary (from distal leaves), racemes (flat-topped);</text>
      <biological_entity id="o9719" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o9720" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o9721" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers actinomorphic;</text>
      <biological_entity id="o9722" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent, distinct, equal (each often subtending a nectary);</text>
      <biological_entity id="o9723" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals equal;</text>
      <biological_entity id="o9724" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6;</text>
      <biological_entity id="o9725" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted on a spheroidal gynophore, glabrous;</text>
      <biological_entity id="o9726" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9727" name="gynophore" name_original="gynophore" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="spheroidal" value_original="spheroidal" />
      </biological_entity>
      <relation from="o9726" id="r686" name="inserted on a" negation="false" src="d0_s12" to="o9727" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (ellipsoid), coiling as pollen released;</text>
      <biological_entity id="o9728" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o9729" is_modifier="false" name="shape" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o9729" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore sharply deflexed in fruit (stout, style spinelike in fruit).</text>
      <biological_entity id="o9730" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o9731" is_modifier="false" modifier="sharply" name="orientation" src="d0_s14" value="deflexed" value_original="deflexed" />
      </biological_entity>
      <biological_entity id="o9731" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits schizocarps (didymous, deflexed), indehiscent, obovoid.</text>
      <biological_entity constraint="fruits" id="o9732" name="schizocarp" name_original="schizocarps" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 2, reniform, not arillate.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10.</text>
      <biological_entity id="o9733" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o9734" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Spiny-caper</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>