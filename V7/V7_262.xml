<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1819" rank="genus">polanisia</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="species">dodecandra</taxon_name>
    <taxon_name authority="H. H. Iltis" date="1969" rank="subspecies">riograndensis</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>14: 116, fig. 1[top]. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus polanisia;species dodecandra;subspecies riograndensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095185</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaflets: blade apex rounded.</text>
      <biological_entity id="o11349" name="leaflet" name_original="leaflets" src="d0_s0" type="structure" />
      <biological_entity constraint="blade" id="o11350" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescence bracts ovate to orbiculate.</text>
      <biological_entity constraint="inflorescence" id="o11351" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="orbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petals pink, rose, or purple, (6–) 8–14 (–17) mm.</text>
      <biological_entity id="o11352" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple" value_original="purple" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="17" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s2" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stamens (6–) 7–17 mm, equaling or slightly longer than petals.</text>
      <biological_entity id="o11353" name="stamen" name_original="stamens" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="17" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character constraint="than petals" constraintid="o11354" is_modifier="false" name="length_or_size" src="d0_s3" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o11354" name="petal" name_original="petals" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Seeds tuberculate-rugose.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 20.</text>
      <biological_entity id="o11355" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="tuberculate-rugose" value_original="tuberculate-rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11356" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>River banks, coastal dunes, open woodlands, mesquite, and semi-desert</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="river banks" />
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="open woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="common_name">Rio Grande clammyweed</other_name>
  <discussion>Subspecies riograndensis is found along the lower Rio Grande in Texas and adjacent Mexico.</discussion>
  
</bio:treatment>