<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Staria S. Vanderpool,Hugh H. Iltis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">205</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="genus">PERITOMA</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>1: 2371824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus PERITOMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek peri, all around, and tome, cutting, perhaps alluding to dehiscence of fruit</other_info_on_name>
    <other_info_on_name type="fna_id">124591</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="unranked">Atalanta</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 73. 1818,</place_in_publication>
      <other_info_on_pub>not Atalantia Corrêa 1805</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Cleome;unranked Atalanta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Greene" date="unknown" rank="genus">Celome</taxon_name>
    <taxon_hierarchy>genus Celome;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Isomeris</taxon_name>
    <taxon_hierarchy>genus Isomeris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or shrubs, annual or (weak) perennial.</text>
      <biological_entity id="o41212" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely or profusely branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous, or glabrate, or glandular-pubescent.</text>
      <biological_entity id="o41214" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="profusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules scalelike, bristlelike, or absent;</text>
      <biological_entity id="o41215" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o41216" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bristlelike" value_original="bristlelike" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole with pulvinus basally or distally;</text>
      <biological_entity id="o41217" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o41218" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o41219" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o41218" id="r2786" modifier="distally" name="with" negation="false" src="d0_s4" to="o41219" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3 or 5, (conduplicate and flat).</text>
      <biological_entity id="o41220" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o41221" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" unit="or" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary (from distal leaves), racemes (flat-topped or elongated);</text>
      <biological_entity id="o41222" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o41223" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts usually present.</text>
      <biological_entity id="o41224" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers zygomorphic;</text>
      <biological_entity id="o41225" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent or deciduous, distinct or partly connate (1/3–1/2 of lengths), equal (each often subtending a nectary);</text>
      <biological_entity id="o41226" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="partly" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals equal;</text>
      <biological_entity id="o41227" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6;</text>
      <biological_entity id="o41228" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted on cylindric androgynophore (usually expanded adaxially into a gibbous or flattened appendage), glabrous;</text>
      <biological_entity id="o41229" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o41230" name="androgynophore" name_original="androgynophore" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <relation from="o41229" id="r2787" name="inserted on" negation="false" src="d0_s12" to="o41230" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (linear), coiling as pollen released;</text>
      <biological_entity id="o41231" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o41232" is_modifier="false" name="shape" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o41232" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore usually recurved in fruit (sometimes reflexed).</text>
      <biological_entity id="o41233" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o41234" is_modifier="false" modifier="usually" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o41234" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules (erect to pendent), dehiscent, usually oblong (obovoid, subglobose, or fusiform in P. arborea).</text>
      <biological_entity constraint="fruits" id="o41235" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 5–38, globose, obovoid, triangular, or horseshoe-shaped, not arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10.</text>
      <biological_entity id="o41236" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s16" to="38" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="horseshoe--shaped" value_original="horseshoe--shaped" />
        <character is_modifier="false" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="horseshoe--shaped" value_original="horseshoe--shaped" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o41237" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Bee-plant</other_name>
  <discussion>Species 6 (6 in the flora).</discussion>
  <discussion>Whether included in Cleome or treated as a separate genus, Peritoma comprises mostly distinct, western North American species, perhaps related to African genera. It is best treated as a taxon equivalent in rank to its three derivative genera, Cleomella, Oxystylis, and Wislizenia (2n = 40). These can be arranged in a much studied fruit and seed reduction series correlated with increasing aridity (H. H. Iltis 1955, 1956, 1957; K. Bremer and H. Wanntorp 1978; S. Keller 1979; S. S. Vanderpool et al. 1991). Some botanists may object to inclusion of the well-established Isomeris arborea in Peritoma. It is the only long-lived woody shrub species in the North American Cleomaceae. Except for the larger size, the flowers are basically identical (as are the fruits and seeds) to those of species such as P. lutea, a fact appreciated long ago by E. L. Greene when that notorious splitter lumped Isomeris with its relatives in Cleome.</discussion>
  <discussion>With six sharply distinct species (H. H. Iltis 1957), Peritoma is an exceptionally robust and relatively ancient genus, usually characterized by the rather thick trifoliolate and glabrous leaves, yellow (except a rich purple in P. multicaulis and P. serrulata) petals, and, usually, well-developed nectary discs.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs; capsules usually inflated, 6-12 mm diam.</description>
      <determination>1 Peritoma arborea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; capsules not inflated, 1.5-12 mm diam</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 5 (proximally)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 3 (throughout)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 15-40 mm; gynophore 5-17 mm in fruit; petals light yellow, 5-8 mm; stamens 10-15 mm; plants (15-)25-30 cm.</description>
      <determination>2 Peritoma lutea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules 40-60 mm; gynophore 15-25 mm in fruit; petals golden yellow, 10-13 mm; stamens 20-30 mm; plants 50-100(-200) cm.</description>
      <determination>3 Peritoma jonesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals golden yellow; capsules pendent, glandular-pubescent.</description>
      <determination>6 Peritoma platycarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals purple, pink, rose, or white; capsules erect or reflexed, glabrous</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets 0.6-1.5 cm wide; petioles 1.5-3.5 cm; petals 7-12 mm; capsules 23-76 mm.</description>
      <determination>4 Peritoma serrulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets 0.1 cm wide; petioles 0.5-1.5 cm; petals 4-5 mm; capsules 15-25 mm.</description>
      <determination>5 Peritoma multicaulis</determination>
    </key_statement>
  </key>
</bio:treatment>