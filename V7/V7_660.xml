<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="illustration_page">436</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Bunge" date="1833" rank="genus">orychophragmus</taxon_name>
    <taxon_name authority="(Linnaeus) O. E. Schulz" date="1916" rank="species">violaceus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>54(Beibl. 119): 56. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus orychophragmus;species violaceus</taxon_hierarchy>
    <other_info_on_name type="fna_id">200009631</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brassica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">violacea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 667. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brassica;species violacea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (0.6–) 1.5–6 (–9) dm, often branched distally, glabrous or sparsely to densely pilose.</text>
      <biological_entity id="o13368" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="9" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s0" to="sparsely densely pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole (1–) 2–8 (–11) cm;</text>
      <biological_entity constraint="basal" id="o13369" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13370" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="11" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade or terminal lobe cordate, reniform, broadly ovate, or suborbicular, (0.4–) 1.5–10 (–14) cm × (3–) 10–40 (–70) mm, base usually cordate, rarely obtuse, margins coarsely crenate with teeth ending in apiculae, apex acute or obtuse;</text>
      <biological_entity constraint="basal" id="o13371" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13372" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s2" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13373" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s2" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13374" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o13375" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="with teeth" constraintid="o13376" is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o13376" name="tooth" name_original="teeth" src="d0_s2" type="structure" />
      <biological_entity id="o13377" name="apicula" name_original="apiculae" src="d0_s2" type="structure" />
      <biological_entity id="o13378" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o13376" id="r931" name="ending in" negation="false" src="d0_s2" to="o13377" />
    </statement>
    <statement id="d0_s3">
      <text>lobes (0 or) 1–6 each side, sessile or petiolulate (to 3 × 2 cm), glabrous or pilose.</text>
      <biological_entity constraint="basal" id="o13379" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13380" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o13381" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves sessile or petiolate;</text>
      <biological_entity constraint="cauline" id="o13382" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>(distal) blade (0.5–) 2–9 (–15) cm × (2–) 10–60 (–90) mm, base sometimes auriculate or amplexicaul, margins dentate or entire, apex acute or acuminate (auricles to 3 × 4 cm);</text>
      <biological_entity id="o13383" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13384" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o13385" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13386" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lobes (0 or) 1–4 each side, sessile or petiolulate, glabrous or pilose.</text>
      <biological_entity id="o13388" name="side" name_original="side" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels (0.6–) 0.8–2 (–3) cm, narrower than fruit, glabrous or pilose.</text>
      <biological_entity id="o13387" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character constraint="than fruit" constraintid="o13390" is_modifier="false" name="width" src="d0_s7" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o13389" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o13390" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o13387" id="r932" name="fruiting" negation="false" src="d0_s7" to="o13389" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals connivent, (6–) 8–13 (–16) × 1.5–2.5 mm;</text>
      <biological_entity id="o13391" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13392" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="connivent" value_original="connivent" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s8" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (12–) 16–25 (–32) × (4–) 5–9 (–11) mm;</text>
      <biological_entity id="o13393" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13394" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_length" src="d0_s9" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="32" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 8–18 mm;</text>
      <biological_entity id="o13395" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13396" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers (3–) 4–6 (–8) mm.</text>
      <biological_entity id="o13397" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13398" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits (3–) 4.5–11 (–13) cm × 1.5–3 mm;</text>
      <biological_entity id="o13399" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s12" to="4.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s12" to="13" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s12" to="11" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style (0.3–) 0.7–3 (–5.5) cm;</text>
      <biological_entity id="o13400" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s13" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma slightly to distinctly 2-lobed.</text>
      <biological_entity id="o13401" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly to distinctly" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2–3 (–3.5) × 1–2 mm. 2n = 24.</text>
      <biological_entity id="o13402" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13403" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Railroad tracks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="railroad tracks" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Va.; Asia (China, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>This is the first report of Orychophragmus violaceus as naturalized in North America, though I have seen the plant cultivated as an ornamental in multiple places. The record is based on Wright 3145 (GH), a collection made on 12 May 1987 in the woods along Southern Railroad tracks in Richmond, Virginia; it is the very same collection on which R. C. Rollins (1993) based his record of Moricandia arvensis (Linnaeus) de Candolle for North America. The species appears to be spreading in other locations in the neighboring areas (Rollins).</discussion>
  
</bio:treatment>