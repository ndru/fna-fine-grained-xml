<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Torrey" date="1837" rank="genus">leavenworthia</taxon_name>
    <taxon_name authority="Torrey" date="1837" rank="species">aurea</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>4: 88, plate 5. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus leavenworthia;species aurea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094619</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (when present) 1–3 dm.</text>
      <biological_entity id="o18169" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole 1.8–3.9 cm;</text>
      <biological_entity constraint="basal" id="o18170" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o18171" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s1" to="3.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 3.5–6.5 (–8) cm, lobes 1–3 on each side, margins shallowly dentate, terminal lobe suborbicular, 6–12 × 5–11 mm (equal to or slightly longer than wide), considerably larger than lateral lobes, margins usually shallowly dentate, sometimes slightly lobed.</text>
      <biological_entity constraint="basal" id="o18172" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o18173" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s2" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18174" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o18175" from="1" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <biological_entity id="o18175" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o18176" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o18177" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="11" to_unit="mm" />
        <character constraint="than lateral lobes" constraintid="o18178" is_modifier="false" name="size" src="d0_s2" value="considerably larger" value_original="considerably larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18178" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity id="o18179" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting pedicels: solitary flowers 30–90 mm;</text>
      <biological_entity id="o18181" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <biological_entity id="o18182" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s3" to="90" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>racemes 30–60 mm.</text>
      <biological_entity id="o18184" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o18185" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s4" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals widely spreading, oblong-linear, 2.9–4.6 × 1–1.9 mm;</text>
      <biological_entity id="o18186" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18187" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="length" src="d0_s5" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals spreading, pale-yellow, obcordate to broadly obovate, (5–) 6–9 × 1.5–4.5 mm, claw bright-yellow, 1.6–2.8 mm, apex shallowly emarginate, apical notch 0.1–0.4 (–0.6) mm deep;</text>
      <biological_entity id="o18188" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18189" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="obcordate" name="shape" src="d0_s6" to="broadly obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s6" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18190" name="claw" name_original="claw" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s6" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18191" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s6" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o18192" name="notch" name_original="notch" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s6" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments: median 3–4.5 mm, lateral 1.3–2.4 mm;</text>
      <biological_entity id="o18193" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="median" value_original="median" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.6–1.1 mm.</text>
      <biological_entity id="o18194" name="filament" name_original="filaments" src="d0_s8" type="structure" />
      <biological_entity id="o18195" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits oblong, 2–3.1 cm × 3–5 mm, smooth, latiseptate, (margined);</text>
      <biological_entity id="o18196" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="3.1" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves thin;</text>
      <biological_entity id="o18197" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 6–14 per ovary;</text>
      <biological_entity id="o18198" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o18199" from="6" name="quantity" src="d0_s11" to="14" />
      </biological_entity>
      <biological_entity id="o18199" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style (1.5–) 2–3 mm.</text>
      <biological_entity id="o18200" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–4.2 mm diam.;</text>
      <biological_entity id="o18201" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s13" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>wing 0.2–0.3 mm wide;</text>
      <biological_entity id="o18202" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryo straight.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 48.</text>
      <biological_entity id="o18203" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18204" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pastures, roadsides, shallow limestone soils, seeps, creek margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="shallow limestone soils" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="margins" modifier="creek" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Leavenworthia aurea is known only from Choctaw and McCurtain counties.</discussion>
  
</bio:treatment>