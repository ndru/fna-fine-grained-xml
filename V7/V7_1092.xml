<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">620</other_info_on_meta>
    <other_info_on_meta type="mention_page">650</other_info_on_meta>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Nuttall) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">ludoviciana</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 325. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species ludoviciana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094883</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alyssum</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">ludovicianum</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 63. 1818,</place_in_publication>
      <other_info_on_pub>based on Myagrum argenteum Pursh, Fl. Amer. Sept. 2: 434. 1813, not A. argenteum Vitman 1790</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Alyssum;species ludovicianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Nuttall) S. Watson" date="unknown" rank="species">ludoviciana</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species ludoviciana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="unknown" rank="species">ludoviciana</taxon_name>
    <taxon_hierarchy>genus Vesicaria;species ludoviciana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o13938" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes (sessile or short-stalked), 4–7-rayed, rays usually furcate, sometimes bifurcate, (rough-tuberculate throughout).</text>
      <biological_entity id="o13939" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13940" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o13941" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems few from base, erect with outer usually decumbent, 1–3.5 (–5) dm.</text>
      <biological_entity id="o13942" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o13943" is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character constraint="with outer decumbent" constraintid="o13944" is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="3.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o13943" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="outer" id="o13944" name="decumbent" name_original="decumbent" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="usually" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (erect);</text>
      <biological_entity constraint="basal" id="o13945" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly lanceolate to linear or (outer) oblanceolate, (1–) 2–6 (–9) cm, margins usually entire, rarely shallowly dentate, (inner involute, outer usually flat, base usually with some simple trichomes).</text>
      <biological_entity id="o13946" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="linear or oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13947" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely shallowly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: blade narrowly oblanceolate to linear, (1–) 2–4 (–8) cm, margins flat or involute.</text>
      <biological_entity constraint="cauline" id="o13948" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o13949" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13950" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes compact, (elongated and loose in fruit, densely-flowered).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (usually recurved), (5–) 10–20 (–25) mm.</text>
      <biological_entity id="o13951" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13952" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o13951" id="r973" name="fruiting" negation="false" src="d0_s8" to="o13952" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong to broadly elliptic, 4–7 (–8) mm, (lateral pair subsaccate, median pair cucullate);</text>
      <biological_entity id="o13953" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13954" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="broadly elliptic" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals oblanceolate or obovate, (5–) 6.5–9.5 (–11) mm, (claw undifferentiated from blade, or blade gradually narrowed to claw).</text>
      <biological_entity id="o13955" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13956" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s10" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits subglobose or obovoid, usually inflated, sometimes slightly compressed, (3–) 4–6 mm;</text>
      <biological_entity id="o13957" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves densely pubescent, trichomes spreading, usually pubescent inside, trichomes appressed, sessile;</text>
      <biological_entity id="o13958" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13959" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13960" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules (4–) 8–12 (–16) per ovary;</text>
      <biological_entity id="o13961" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s13" to="8" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="16" />
        <character char_type="range_value" constraint="per ovary" constraintid="o13962" from="8" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
      <biological_entity id="o13962" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style 3–4.5 (–6.5) mm.</text>
      <biological_entity id="o13963" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds slightly flattened.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 10, 20, 30.</text>
      <biological_entity id="o13964" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13965" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul(-Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy or gravelly soils, steep hillsides, prairie pastures, clay slopes, limestone outcrops, sand dunes, open plains, sandy bluffs, rocky flats, white tuff sands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
        <character name="habitat" value="steep hillsides" />
        <character name="habitat" value="prairie pastures" />
        <character name="habitat" value="clay slopes" />
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="open plains" />
        <character name="habitat" value="sandy bluffs" />
        <character name="habitat" value="rocky flats" />
        <character name="habitat" value="white tuff sands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Ill., Iowa, Kans., Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Okla., S.Dak., Utah, Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50.</number>
  <other_name type="common_name">Foothill bladderpod</other_name>
  <discussion>Material previously reported as Physaria ludoviciana from Canada (Alberta, Manitoba, Saskatchewan) is here included in 6a. P. arenosa subsp. arenosa. Lesquerella argentea (Pursh) MacMillan is a later homonym that has been used for P. ludoviciana.</discussion>
  
</bio:treatment>