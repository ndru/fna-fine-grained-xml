<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">690</other_info_on_meta>
    <other_info_on_meta type="treatment_page">692</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="(B. L. Robinson) Rydberg" date="1907" rank="genus">hesperidanthus</taxon_name>
    <taxon_name authority="(Rollins) Al-Shehbaz" date="2005" rank="species">suffrutescens</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>10: 50. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus hesperidanthus;species suffrutescens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094977</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">suffrutescens</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Carnegie Mus.</publication_title>
      <place_in_publication>26: 224. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thelypodium;species suffrutescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Glaucocarpum</taxon_name>
    <taxon_name authority="(Rollins) Rollins" date="unknown" rank="species">suffrutescens</taxon_name>
    <taxon_hierarchy>genus Glaucocarpum;species suffrutescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schoenocrambe</taxon_name>
    <taxon_name authority="(Rollins) S. L. Welsh &amp; Chatterley" date="unknown" rank="species">suffrutescens</taxon_name>
    <taxon_hierarchy>genus Schoenocrambe;species suffrutescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(suffrutescent, woody proximally).</text>
      <biological_entity id="o37418" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems several from caudex, erect to ascending, (few-branched distally), 1–3.5 dm.</text>
      <biological_entity id="o37419" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from caudex" constraintid="o37420" is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="3.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o37420" name="caudex" name_original="caudex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves sessile or petiolate, (to 11 mm);</text>
      <biological_entity constraint="cauline" id="o37421" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to lanceolate or elliptic, (0.7–) 1–2.5 cm × 3–10 mm, base cuneate to attenuate, margins entire or, rarely, obscurely denticulate, apex acute or obtuse.</text>
      <biological_entity id="o37422" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="lanceolate or elliptic" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37423" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <biological_entity id="o37424" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o37425" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes 5+-flowered, (sometimes 1 or 2 flowers bracteate).</text>
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels ascending, straight or curved upward, 3–10 (–12) mm.</text>
      <biological_entity id="o37426" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5+-flowered" value_original="5+-flowered" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37427" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o37426" id="r2516" name="fruiting" negation="false" src="d0_s6" to="o37427" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals yellowish green, 4–6 × 1–1.5 mm;</text>
      <biological_entity id="o37428" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o37429" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, 7–11 × 1.5–2.5 mm, claw undifferentiated from blade;</text>
      <biological_entity id="o37430" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o37431" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37432" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character constraint="from blade" constraintid="o37433" is_modifier="false" name="prominence" src="d0_s8" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <biological_entity id="o37433" name="blade" name_original="blade" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>filaments 2–3 mm;</text>
      <biological_entity id="o37434" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37435" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers oblong, 0.7–0.9 mm;</text>
      <biological_entity id="o37436" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o37437" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>gynophore 0.4–0.8 mm.</text>
      <biological_entity id="o37438" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o37439" name="gynophore" name_original="gynophore" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits straight, latiseptate, 1–2 cm × 1.2–2.5 mm;</text>
      <biological_entity id="o37440" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="latiseptate" value_original="latiseptate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s12" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o37441" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o37442" from="8" name="quantity" src="d0_s13" to="16" />
      </biological_entity>
      <biological_entity id="o37442" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style cylindrical, (0.5–) 1–2 mm;</text>
      <biological_entity id="o37443" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma flat, entire.</text>
      <biological_entity id="o37444" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s15" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1.5–1.9 × 0.9–1.3 mm.</text>
      <biological_entity id="o37445" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s16" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed desert shrubs, shale barrens, decomposed chip-rock, limy shale</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed desert shrubs" />
        <character name="habitat" value="shale barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Hesperidanthus suffrutescens is restricted to the Green River Formation in Duchesne and Uintah counties.</discussion>
  
</bio:treatment>