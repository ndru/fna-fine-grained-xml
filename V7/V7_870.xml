<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">547</other_info_on_meta>
    <other_info_on_meta type="illustration_page">541</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">euclidieae</taxon_name>
    <taxon_name authority="Sternberg &amp; Hoppe" date="1815" rank="genus">braya</taxon_name>
    <taxon_name authority="Richardson in J. Franklin et al." date="1823" rank="species">glabella</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin et al., Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>743. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe euclidieae;genus braya;species glabella</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095020</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Braya</taxon_name>
    <taxon_name authority="Sternberg &amp; Hoppe" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="(Richardson) S. Watson" date="unknown" rank="variety">glabella</taxon_name>
    <taxon_hierarchy>genus Braya;species alpina;variety glabella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scapose;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely pubescent, trichomes simple, 2-forked or 3-forked.</text>
      <biological_entity id="o42239" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o42240" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-forked" value_original="2-forked" />
        <character is_modifier="false" name="shape" src="d0_s1" value="3-forked" value_original="3-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or few to several from base, ascending or erect, rarely decumbent to prostrate, (0.1–) 0.3–1.7 (–2.3) dm.</text>
      <biological_entity id="o42241" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s2" value="few to several" value_original="few to several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="rarely decumbent" name="growth_form_or_orientation" src="d0_s2" to="prostrate" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2.3" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="1.7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o42242" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o42241" id="r2845" name="from" negation="false" src="d0_s2" to="o42242" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: blade (often somewhat fleshy), linear-oblanceolate to broadly spatulate, (0.4–) 0.8–6 (–7.9) cm × (0.3–) 0.6–4 (–6) mm, base (membranous), broadly expanded near point of attachment, margins usually entire, sometimes weakly dentate with 1 or 2 teeth per side, apex obtuse, (often with a tuft of long, simple hairs, surfaces sparsely to moderately pubescent).</text>
      <biological_entity constraint="basal" id="o42243" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" notes="" src="d0_s3" to="broadly spatulate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s3" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="7.9" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s3" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42244" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o42245" name="base" name_original="base" src="d0_s3" type="structure">
        <character constraint="near point" constraintid="o42246" is_modifier="false" modifier="broadly" name="size" src="d0_s3" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o42246" name="point" name_original="point" src="d0_s3" type="structure" />
      <biological_entity id="o42247" name="attachment" name_original="attachment" src="d0_s3" type="structure" />
      <biological_entity id="o42248" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="per side" constraintid="o42249" is_modifier="false" modifier="sometimes weakly" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o42249" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o42250" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o42246" id="r2846" name="part_of" negation="false" src="d0_s3" to="o42247" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 0 or 1 (or a leaflike bract subtending proximalmost pedicel).</text>
    </statement>
    <statement id="d0_s5">
      <text>(Racemes elongated or not in fruit.) Fruiting pedicels ascending-erect to divaricate, (0.9–) 1.9–7.5 (–8.6) mm.</text>
      <biological_entity constraint="cauline" id="o42251" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s4" unit="or" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending-erect" value_original="ascending-erect" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8.6" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s5" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42252" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o42251" id="r2847" name="fruiting" negation="false" src="d0_s5" to="o42252" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals (1.6–) 1.9–3.7 × (0.7–) 1–2 mm;</text>
      <biological_entity id="o42253" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o42254" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_length" src="d0_s6" to="1.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s6" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" src="d0_s6" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white or purplish (broadly obovate or spatulate), (2.1–) 2.4–4.5 (–4.7) × (0.7–) 1–3 (–3.2) mm, (apex rounded);</text>
      <biological_entity id="o42255" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o42256" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="atypical_length" src="d0_s7" to="2.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" src="d0_s7" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 1.7–2.2 mm;</text>
      <biological_entity id="o42257" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o42258" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers oblong, 0.3–0.5 mm.</text>
      <biological_entity id="o42259" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o42260" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits oval-elliptic, oblongelliptic, oblong, or oblong-lanceoloid, sometimes slightly torulose, (straight or somewhat curved), (0.3–) 0.5–1.2 (–1.5) cm × (0.8–) 1.1–3 (–3.6) mm;</text>
      <biological_entity id="o42261" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oval-elliptic" value_original="oval-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-lanceoloid" value_original="oblong-lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-lanceoloid" value_original="oblong-lanceoloid" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s10" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s10" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s10" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s10" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_width" src="d0_s10" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves sparsely to densely pubescent or glabrous, trichomes simple or 2 (or 3) -forked;</text>
      <biological_entity id="o42262" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o42263" name="trichome" name_original="trichomes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-forked" value_original="2-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>septum margin not expanded or not basally;</text>
      <biological_entity constraint="septum" id="o42264" name="margin" name_original="margin" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s12" value="expanded" value_original="expanded" />
        <character name="size" src="d0_s12" value="not basally" value_original="not basally" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules (5–) 16–20 per ovary;</text>
      <biological_entity id="o42265" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s13" to="16" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o42266" from="16" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
      <biological_entity id="o42266" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style (0.3–) 0.5–1.8 (–2) mm;</text>
      <biological_entity id="o42267" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma entire or strongly 2-lobed, (narrow or broad).</text>
      <biological_entity id="o42268" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s15" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds biseriate, oblong, (0.7–) 0.9–1.6 × 0.4–0.8 (–0.9) mm.</text>
      <biological_entity id="o42269" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_length" src="d0_s16" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s16" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s16" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Nunavut, Que., Yukon; Alaska, Colo., Mont., Wyo.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>Braya glabella is extremely variable, sometimes even within a population, and the species has often been split into taxa based primarily on fruit attributes. When the species is examined from its entire range, the perceived morphological gaps blur into a bewildering array of overlapping forms. On the basis of morphological and molecular evidence (S. I. Warwick et al. 2004) it is difficult to justify the recognition of more than one species in this very plastic group, but it does seem useful to divide the species into three fairly distinctive subspecies. Some populations will be readily separable into one of these subspecies, but others will likely defy unequivocal placement, particularly those from areas where the ranges of the subspecies meet.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits oblong to narrowly oblong-lanceoloid, 3.5-8.3 times as long as wide; racemes often loosely elongated in fruit.</description>
      <determination>2a Braya glabella subsp. glabella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits oval-elliptic to oblong-elliptic, rarely broadly oblong-lanceoloid, 2.5-3.7 times as long as wide; racemes not elongated in fruit, often compact</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems decumbent to prostrate, sometimes weakly ascending; leaf blades not fleshy, to 6 mm wide; fruits 0.8-1.2 cm; styles 0.8-1.8 mm.</description>
      <determination>2b Braya glabella subsp. prostrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems ascending to erect; leaf blades often fleshy, to 4 mm wide; fruits 0.5-1 cm; styles 0.5- 1.2 mm.</description>
      <determination>2c Braya glabella subsp. purpurascens</determination>
    </key_statement>
  </key>
</bio:treatment>