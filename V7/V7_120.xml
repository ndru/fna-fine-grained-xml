<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
    <other_info_on_meta type="illustration_page">118</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="C. R. Ball" date="1921" rank="species">pseudomonticola</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>22: 321. 1921</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species pseudomonticola</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445840</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="species">barclayi</taxon_name>
    <taxon_name authority="(C. R. Ball) Kelso" date="unknown" rank="variety">pseudomonticola</taxon_name>
    <taxon_hierarchy>genus Salix;species barclayi;variety pseudomonticola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–6 m.</text>
      <biological_entity id="o41096" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches redbrown or yellowbrown, not or weakly glaucous, (slightly or highly glossy), glabrous or glabrescent;</text>
      <biological_entity id="o41097" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o41098" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="not; weakly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="punct" value_original="punct" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellow-green, redbrown, or brownish, glabrous, pilose, or densely villous, (inner membranaceous bud-scale layer free, separating from outer layer).</text>
      <biological_entity id="o41099" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o41100" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules foliaceous, (prominent, 5–9–15 mm), apex rounded to acute, sometimes acuminate;</text>
      <biological_entity id="o41101" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o41102" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o41103" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (usually reddish), shallowly grooved, or convex to flat adaxially, 6–20 mm, short-silky or velvety adaxially;</text>
      <biological_entity id="o41104" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture" notes="" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="convex" modifier="adaxially" name="shape" src="d0_s4" to="flat" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="velvety" value_original="velvety" />
      </biological_entity>
      <biological_entity id="o41105" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade broadly to narrowly elliptic, or ovate to broadly obovate, (25–) 30–86 (–118) × 12–51 mm, 1.4–3 times as long as wide, base convex, rounded, cuneate, subcordate, cordate, or sometimes irregularly lobed, margins flat, serrulate or crenate, apex acute, acuminate, or convex, abaxial surface glaucous, glabrous, pubescent, or pilose, hairs wavy, adaxial slightly glossy or dull, glabrous, puberulent, pubescent, or pilose, midrib hairy;</text>
      <biological_entity id="o41106" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o41107" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" modifier="narrowly" name="shape" src="d0_s5" to="broadly obovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="broadly obovate" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_length" src="d0_s5" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="86" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="118" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s5" to="86" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s5" to="51" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1.4-3" value_original="1.4-3" />
      </biological_entity>
      <biological_entity id="o41108" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="sometimes irregularly" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o41109" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o41110" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o41111" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o41112" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41113" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o41114" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o41115" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o41116" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade reddish, glabrous or pubescent abaxially, hairs white, sometimes also ferruginous.</text>
      <biological_entity id="o41117" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o41118" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41119" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="ferruginous" value_original="ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering before leaves emerge;</text>
      <biological_entity id="o41121" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout, 16–39 × 10–12 mm, flowering branchlet 0 mm;</text>
      <biological_entity id="o41120" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="before leaves" constraintid="o41121" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely or moderately densely flowered, slender to globose, 17–73 × 8–20 mm, flowering branchlet 0–5 mm;</text>
      <biological_entity id="o41122" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="0" value_original="0" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41123" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s10" value="or" value_original="or" />
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown or black, 1–2.4 mm, apex rounded or acute, abaxially hairy, hairs straight.</text>
      <biological_entity constraint="floral" id="o41124" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41125" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o41126" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary oblong, 0.3–1 mm;</text>
      <biological_entity id="o41127" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41128" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate less than 1/2 their lengths, glabrous;</text>
      <biological_entity id="o41129" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o41130" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s13" to="1/2" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers purple turning yellow, 0.4–0.5 mm.</text>
      <biological_entity id="o41131" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o41132" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong, flask-shaped, 0.3–0.8 mm, shorter than stipe;</text>
      <biological_entity id="o41133" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41134" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character constraint="than stipe" constraintid="o41135" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o41135" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.5–0.8–3 mm;</text>
      <biological_entity id="o41136" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41137" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8-3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform or obclavate, glabrous, beak gradually tapering to styles;</text>
      <biological_entity id="o41138" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41139" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obclavate" value_original="obclavate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o41140" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character constraint="to styles" constraintid="o41141" is_modifier="false" modifier="gradually" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o41141" name="style" name_original="styles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 18 per ovary;</text>
      <biological_entity id="o41142" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41143" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character constraint="per ovary" constraintid="o41144" name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
      <biological_entity id="o41144" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles (0.5–) 0.7–1.8 mm;</text>
      <biological_entity id="o41145" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41146" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s19" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or 2 plump lobes, 0.1–0.21–0.29 mm.</text>
      <biological_entity id="o41147" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41148" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o41149" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.21-0.29" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41149" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o41150" name="lobe" name_original="lobes" src="d0_s20" type="structure">
        <character is_modifier="true" name="size" src="d0_s20" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 4–7 mm. 2n = 38.</text>
      <biological_entity id="o41151" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s21" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o41152" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr-early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist fens in drainageways in white spruce forests, treed bogs, balsam poplar forests, floodplains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="drainageways" modifier="fens in" constraint="in white" />
        <character name="habitat" value="forests" modifier="white spruce" />
        <character name="habitat" value="treed bogs" />
        <character name="habitat" value="balsam poplar forests" />
        <character name="habitat" value="floodplains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Que., Sask., Yukon; Alaska, Idaho, Minn., Mont., S.Dak., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  <other_name type="common_name">False mountain willow</other_name>
  <discussion>Salix pseudomonticola is characterized by precocious flowering; catkins sessile; juvenile leaf blades, petioles, and proximal midribs reddish; stipules prominent; and leaves and branchlets sparsely hairy. Branches older than two years have a distinctive pattern, which consists of a series of longitudinal splits in epidermis produced as the branch expands. The edge of epidermis around the split, where it has separated from the branch, is yellow and contrasts with the red-brown branch to which the epidermis still adheres.</discussion>
  <discussion>Vegetative specimens of Salix pseudomonticola with yellow-brown branches can be confused with S. famelica. They may be separated by their juvenile leaf margins prominently and closely gland-dotted; stipules usually prominent, sometimes early deciduous; leaves broader (1.4–3 times as long as wide versus 2.6–7 in S. famelica); and petioles slender and often longer in relation to blade length. The possibility of hybridization needs study.</discussion>
  <discussion>Vegetative specimens of Salix pseudomonticola can be distinguished from S. pyrifolia by juvenile leaves reddish and almost always with some ferruginous hairs, versus yellowish-green and glabrous or with white hairs, and mature leaves usually dull adaxially versus glossy.</discussion>
  
</bio:treatment>