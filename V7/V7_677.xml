<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="treatment_page">447</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">camelineae</taxon_name>
    <taxon_name authority="(de Candolle) Heynhold in F. Holl and G. Heynhold" date="unknown" rank="genus">ARABIDOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>in F. Holl and G. Heynhold, Fl. Sachsen</publication_title>
      <place_in_publication>1: 538. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe camelineae;genus ARABIDOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Arabis and Greek opsis, resembling</other_info_on_name>
    <other_info_on_name type="fna_id">102392</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials;</text>
      <biological_entity id="o35886" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>(stoloniferous or with woody caudex);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous or pubescent, trichomes simple, mixed with stalked, 1–3-forked ones.</text>
      <biological_entity id="o35887" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o35889" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="with ones" constraintid="o35890" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o35890" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="stalked" value_original="stalked" />
        <character is_modifier="true" name="shape" src="d0_s3" value="1-3-forked" value_original="1-3-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, ascending, or decumbent, unbranched or branched distally, (usually glabrous distally).</text>
      <biological_entity id="o35891" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate, sessile, or subsessile;</text>
      <biological_entity id="o35892" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate, petiolate, blade margins entire, toothed, or pinnately lobed;</text>
      <biological_entity constraint="basal" id="o35893" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o35894" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline blade margins usually entire or dentate, rarely lyrate.</text>
      <biological_entity constraint="blade" id="o35895" name="margin" name_original="margins" src="d0_s8" type="structure" constraint_original="cauline blade">
        <character is_modifier="true" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="lyrate" value_original="lyrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (few to several-flowered), not elongated in fruit.</text>
      <biological_entity id="o35897" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending, divaricate, or slightly reflexed, slender.</text>
      <biological_entity id="o35896" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o35897" is_modifier="false" modifier="not" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o35898" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o35896" id="r2403" name="fruiting" negation="false" src="d0_s10" to="o35898" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect or ascending, usually oblong (or ovate), (lateral pair sometimes saccate or subsaccate basally);</text>
      <biological_entity id="o35899" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o35900" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, lavender, or purplish [pink, purple], obovate, spatulate, or oblanceolate, claw differentiated from blade or not, (apex obtuse or emarginate);</text>
      <biological_entity id="o35901" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o35902" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o35903" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o35904" is_modifier="false" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o35904" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens slightly tetradynamous;</text>
      <biological_entity id="o35905" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o35906" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o35907" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o35908" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong or ovate, (apex obtuse);</text>
      <biological_entity id="o35909" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o35910" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands confluent, subtending bases of stamens.</text>
      <biological_entity id="o35911" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o35912" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o35913" name="base" name_original="bases" src="d0_s16" type="structure" />
      <biological_entity id="o35914" name="stamen" name_original="stamens" src="d0_s16" type="structure" />
      <relation from="o35912" id="r2404" name="subtending" negation="false" src="d0_s16" to="o35913" />
      <relation from="o35912" id="r2405" name="part_of" negation="false" src="d0_s16" to="o35914" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques, dehiscent, shortly stipitate or subsessile, linear, smooth or somewhat torulose, terete or latiseptate;</text>
      <biological_entity constraint="fruits" id="o35915" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves (papery), each not veined or midvein prominent or obscure, glabrous;</text>
      <biological_entity id="o35916" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o35917" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o35918" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o35919" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 15–80 per ovary;</text>
      <biological_entity id="o35921" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>(style obsolete or distinct, to 1 mm);</text>
      <biological_entity id="o35920" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o35921" from="15" name="quantity" src="d0_s21" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate.</text>
      <biological_entity id="o35922" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds usually uniseriate, plump or flattened, not winged or margined, oblong, ovoid, or ellipsoid;</text>
      <biological_entity id="o35923" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s24" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s24" value="plump" value_original="plump" />
        <character is_modifier="false" name="shape" src="d0_s24" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="margined" value_original="margined" />
        <character is_modifier="false" name="shape" src="d0_s24" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (minutely reticulate), mucilaginous or not when wetted;</text>
      <biological_entity id="o35924" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s25" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons usually accumbent, rarely incumbent.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 5, 8.</text>
      <biological_entity id="o35925" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s26" value="accumbent" value_original="accumbent" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_orientation" src="d0_s26" value="incumbent" value_original="incumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o35926" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="5" value_original="5" />
        <character name="quantity" src="d0_s27" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, n, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Mouse-ear or thale cress</other_name>
  <discussion>Species 10 (4 in the flora).</discussion>
  <discussion>The limits of Arabidopsis have been the subject of long controversy, and more than 50 species were placed in the genus (I. A. Al-Shehbaz et al. 1999). The delimitation of the genus was based primarily on the presence of branched trichomes, linear fruits, and accumbent or incumbent cotyledons. That combination of characteristics evolved independently multiple times in Brassicaceae. Extensive molecular data and critical evaluation of morphology have shown that Arabidopsis is polyphyletic (S. L. O’Kane and Al-Shehbaz 2003). Nine of the ten species in the genus are native to Europe; only A. arenicola is endemic to North America.</discussion>
  <references>
    <reference>Kane, S. L. and I. A. Al-Shehbaz. 1997. A synopsis of Arabidopsis (Brassicaceae). Novon 7: 323–327.</reference>
    <reference>Price, R. A., J. D. Palmer, and I. A. Al-Shehbaz. 1994. Systematic relationship of Arabidopsis: A molecular and morphological approach. In: E. M. Meyerowitz and C. R. Somerville, eds. 1994. Arabidopsis. New York. Pp. 7–19.</reference>
    <reference>Kane, S. L. and I. A. Al-Shehbaz. 2003. Phylogenetic position and generic limits of Arabidopsis (Brassicaceae) based on sequences of nuclear ribosomal DNA. Ann. Missouri Bot. Gard. 90: 603–612.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; petals 2-3.5(-4) mm; fruits 0.5-0.8 mm wide; seeds 0.3-0.5 mm.</description>
      <determination>4 Arabidopsis thaliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Biennials or perennials; petals 4-10 mm; fruits 0.8-2.5 mm wide; seeds 0.8-1.4 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruits terete or slightly flattened, (0.8-)1-2(-2.8) cm × 1.5-2.2(-2.5) mm; cotyledons incumbent.</description>
      <determination>1 Arabidopsis arenicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruits flattened, (1.5-)2-4(-4.5) cm × 0.8-1.8(-2) mm; cotyledons accumbent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Basal leaf blade margins pinnatisect or pinnatipartite; petals with 2 lateral teeth on claws.</description>
      <determination>2 Arabidopsis arenosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Basal leaf blade margins entire, dentate, lyrate, or lyrate-pinnatifid; petals without teeth on claws.</description>
      <determination>3 Arabidopsis lyrata</determination>
    </key_statement>
  </key>
</bio:treatment>