<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="W. T. Aiton in W. Aiton and W. T. Aiton" date="unknown" rank="genus">BARBAREA</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>4: 109. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus BARBAREA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Saint Barbara, fourth-century, or perhaps alluding to being the only plants available for food on Saint Barbara’s Day (4 December)</other_info_on_name>
    <other_info_on_name type="fna_id">103484</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Dulac" date="unknown" rank="genus">Campe</taxon_name>
    <taxon_hierarchy>genus Campe;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials [annuals];</text>
    </statement>
    <statement id="d0_s1">
      <text>(rhizomatous or with woody caudex);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous or sparsely pubescent.</text>
      <biological_entity id="o16687" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect [prostrate], branched distally, (angular [not angular]).</text>
      <biological_entity id="o16689" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate and sessile;</text>
      <biological_entity id="o16690" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate or not, (and proximal cauline) petiolate, blade margins usually entire, crenate or lobed, rarely dentate or repand;</text>
      <biological_entity constraint="basal" id="o16691" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o16692" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline sessile, blade (base auriculate or amplexicaul) margins entire, dentate, or lobed.</text>
      <biological_entity constraint="cauline" id="o16693" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o16694" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, several-flowered), considerably [slightly] elongated in fruit, (rachis striate).</text>
      <biological_entity id="o16696" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels (sometimes absent), erect to divaricate, slender or stout.</text>
      <biological_entity id="o16695" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o16696" is_modifier="false" modifier="considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s10" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o16697" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o16695" id="r1146" name="fruiting" negation="false" src="d0_s10" to="o16697" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (sometimes persistent), erect [spreading], oblong [ovate, linear], lateral pair saccate or not basally, (apex often cucullate);</text>
      <biological_entity id="o16698" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
        <character name="architecture_or_shape" src="d0_s11" value="not basally" value_original="not basally" />
      </biological_entity>
      <biological_entity id="o16699" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals yellow or pale-yellow [creamy white], spatulate or oblanceolate, (longer than sepals), claw obscurely differentiated from blade, (apex obtuse or rounded);</text>
      <biological_entity id="o16700" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16701" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o16702" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o16703" is_modifier="false" modifier="obscurely" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o16703" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o16704" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16705" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments (yellow), not dilated basally;</text>
      <biological_entity id="o16706" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" notes="" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o16707" name="filament" name_original="filaments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong, (apex obtuse);</text>
      <biological_entity id="o16708" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o16709" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands (4): lateral annular, median toothlike.</text>
      <biological_entity constraint="nectar" id="o16710" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character name="atypical_quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s16" value="annular" value_original="annular" />
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character is_modifier="false" name="shape" src="d0_s16" value="toothlike" value_original="toothlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques, sessile or shortly stipitate, usually linear, rarely elliptic-linear, smooth or torulose, terete, 4-angled, or latiseptate;</text>
      <biological_entity constraint="fruits" id="o16711" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s17" value="elliptic-linear" value_original="elliptic-linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein and distinct marginal veins, usually glabrous, rarely pubescent;</text>
      <biological_entity id="o16712" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16713" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o16714" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o16712" id="r1147" name="with" negation="false" src="d0_s18" to="o16713" />
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o16715" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o16716" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 16–52 per ovary;</text>
      <biological_entity id="o16717" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o16718" from="16" name="quantity" src="d0_s21" to="52" />
      </biological_entity>
      <biological_entity id="o16718" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style obsolete or distinct;</text>
      <biological_entity id="o16719" name="style" name_original="style" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" name="fusion" src="d0_s22" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate, (sometimes slightly 2-lobed).</text>
      <biological_entity id="o16720" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds uniseriate [sub-biseriate], plump or slightly flattened, not winged [winged or margined], oblong, ovoid, or orbicular;</text>
      <biological_entity id="o16721" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s24" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s24" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s24" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s24" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="orbicular" value_original="orbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (reticulate or, rarely, tuberculate), not mucilaginous when wetted;</text>
      <biological_entity id="o16722" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 8.</text>
      <biological_entity id="o16723" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s26" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o16724" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia, n Africa, Australia</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <other_name type="common_name">Wintercress</other_name>
  <other_name type="common_name">scurvygrass</other_name>
  <other_name type="common_name">rocket</other_name>
  <other_name type="common_name">uplandcress</other_name>
  <other_name type="common_name">corn-mustard</other_name>
  <discussion>Species 22 (4 in the flora).</discussion>
  <discussion>Barbarea is a difficult genus much in need of systematic and phylogenetic studies throughout its range. Although some of its species are easily recognizable, some complexes, especially the widespread or weedy taxa, remain problematic. It is likely that hybridization is involved, but no studies have confirmed that.</discussion>
  <discussion>The determination of flowering material is not always possible, and most workers have relied heavily on the distalmost leaves to separate species. Both Barbarea orthoceras and B. verna are said to have pinnatisect to pinnatifid distalmost leaves, whereas B. stricta and B. vulgaris are said to have undivided, entire, or dentate leaves. This separation can be misleading because B. orthoceras sometimes has entire or dentate distal leaves, whereas in some B. vulgaris the distal leaves are deeply divided. In B. vulgaris, the style length and its thickness in relation to the fruits are useful in separating it from the remaining species, though, on rare occasions, both B. orthoceras and B. stricta have slender styles to 2 mm. In these cases, both B. orthoceras and B. stricta can be separated from B. vulgaris by the ciliate auricles of cauline leaves and subapically pubescent sepals. Both sepals and auricles of distalmost leaves are always glabrous in B. vulgaris. Although all the Eurasian specimens of B. stricta that I examined have pubescent auricles and sepal apices, the number of trichomes can be quite variable and ranges from one to many. Most of the naturalized North American plants of B. stricta have glabrescent or glabrous sepals. However, that species can be further distinguished by having petals 2.5–4.5 × 0.5–1(–1.2) mm, the smallest and narrowest among the four species growing in North America. In the absence of mature fruits, one needs to be aware of the variation in the other characters.</discussion>
  <references>
    <reference>Fernald, M. L. 1909. The North American species of Barbarea. Rhodora. 11: 134–141.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits (4.5-)5.3-7(-8) cm; ovules (34-)38-48(-52) per ovary; fruiting pedicels as broad as fruit; cauline leaf blades pinnatifid to pinnatisect.</description>
      <determination>4 Barbarea verna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits (0.7-)1.5-4(-4.5) cm; ovules 16-36 per ovary; fruiting pedicels narrower than fruit; cauline leaf blades undivided or lyrate-pinnatifid</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles (1-)1.5-3(-3.5) mm, slender; auricles of cauline leaves glabrous; fruits usually not appressed to rachises.</description>
      <determination>1 Barbarea vulgaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 0.2-1.5(-2) mm, sometimes stout; auricles of cauline leaves usually sparsely pubescent, rarely glabrous, sometimes margins ciliate; fruits (erect to erect-ascending) sometimes appressed to rachises</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits (1.2-)1.8-2.8(-3) cm; ovules (16-)20-28 per ovary; petals 2.5-4.5 × 0.5-1 (-1.2) mm; cauline leaf blade margins dentate.</description>
      <determination>2 Barbarea stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits (2.5-)3.1-4(-4.5) cm; ovules (24-)26-36 per ovary; petals 5-7(-8) × (1.5-)2-3 mm; cauline leaf blade margins incised or pinnatifid.</description>
      <determination>3 Barbarea orthoceras</determination>
    </key_statement>
  </key>
</bio:treatment>