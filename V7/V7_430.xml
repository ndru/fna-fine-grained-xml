<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Rollins &amp; R. A. Price" date="1988" rank="species">monoensis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>12: 22, figs. 1f–j, 3. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species monoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094710</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(usually cespitose);</text>
      <biological_entity id="o25372" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple or branched (poorly developed, with persistent leaf remains);</text>
    </statement>
    <statement id="d0_s3">
      <text>sometimes scapose.</text>
      <biological_entity id="o25373" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.05–) 0.1–0.4 dm, usually pubescent throughout, rarely sparsely pubescent or glabrous distally, trichomes simple and 2-rayed, (often crisped), 0.1–0.6 mm.</text>
      <biological_entity id="o25374" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s4" to="0.4" to_unit="dm" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25375" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>subsessile or shortly petiolate;</text>
      <biological_entity constraint="basal" id="o25376" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole margin ciliate or not;</text>
      <biological_entity constraint="petiole" id="o25377" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character name="architecture_or_pubescence_or_shape" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade narrowly oblanceolate, (0.3–) 0.5–1.6 (–2) cm × (1–) 1.5–3 (–4) mm, margins usually entire, rarely subapically denticulate, (ciliate or not), surfaces pubescent with simple and stalked, 2-rayed trichomes, 0.1–0.7 mm, (midvein obscure abaxially).</text>
      <biological_entity id="o25378" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s8" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s8" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s8" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25379" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely subapically" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o25380" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o25381" is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25381" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0–2 (or 3);</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o25382" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade ovate to oblong, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o25383" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="oblong" />
      </biological_entity>
      <biological_entity id="o25384" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o25385" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o25386" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25386" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes (3–) 6–13 (–17) -flowered, ebracteate or proximalmost 1 (or 2) flowers bracteate, not or slightly elongated in fruit;</text>
      <biological_entity id="o25387" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="(3-)6-13(-17)-flowered" value_original="(3-)6-13(-17)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="position" src="d0_s12" value="proximalmost" value_original="proximalmost" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25388" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o25389" is_modifier="false" modifier="not; slightly" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o25389" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, usually pubescent as stem, rarely glabrous.</text>
      <biological_entity id="o25391" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels divaricate-ascending, straight, 1–2.5 (–4) mm, usually pubescent as stem, rarely glabrous.</text>
      <biological_entity id="o25390" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o25391" is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25392" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o25393" name="stem" name_original="stem" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o25390" id="r1735" name="fruiting" negation="false" src="d0_s14" to="o25392" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals (persistent), oblong, 1–1.5 mm, subapically sparsely pubescent, (trichomes simple);</text>
      <biological_entity id="o25394" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="subapically sparsely" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25395" name="sepal" name_original="sepals" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>petals white, spatulate, 1.5–2 × 0.5–0.6 mm;</text>
      <biological_entity id="o25396" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o25397" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s16" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s16" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.15–0.2 mm.</text>
      <biological_entity id="o25398" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o25399" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="some_measurement" src="d0_s17" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits ovoid to subellipsoid, plane, slightly inflated basally, (2–) 3–5 × (1.2–) 1.5–2.5 mm;</text>
      <biological_entity id="o25400" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s18" to="subellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s18" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s18" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_width" src="d0_s18" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves usually puberulent, rarely glabrous, trichomes simple, 0.05–0.2 mm;</text>
      <biological_entity id="o25401" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s19" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25402" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s19" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 12–20 per ovary;</text>
      <biological_entity id="o25403" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o25404" from="12" name="quantity" src="d0_s20" to="20" />
      </biological_entity>
      <biological_entity id="o25404" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.1–0.2 mm.</text>
      <biological_entity id="o25405" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s21" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ovoid, 0.6–0.8 × 0.4–0.6 mm.</text>
      <biological_entity id="o25406" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s22" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s22" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3600-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>64.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba monoensis appears to be polyploid (M. D. Windham, unpubl.), and its morphological similarity to D. fladnizensis (R. C. Rollins 1993) may indicate that the latter is one of its progenitors. The species is known from the White Mountains of Mono County.</discussion>
  
</bio:treatment>