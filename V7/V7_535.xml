<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="treatment_page">382</other_info_on_meta>
    <other_info_on_meta type="illustration_page">376</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Rollins) W. A. Weber" date="1982" rank="species">gunnisoniana</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>51: 370. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species gunnisoniana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094544</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">gunnisoniana</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>43: 434, fig. 3. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species gunnisoniana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>(cespitose);</text>
    </statement>
    <statement id="d0_s3">
      <text>sexual;</text>
      <biological_entity id="o7885" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>caudex often woody.</text>
      <biological_entity id="o7886" name="caudex" name_original="caudex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stems usually 2–6 per caudex branch, arising from margin of rosette near ground surface, 0.8–2 (–2.5) dm, densely pubescent proximally, trichomes short-stalked, 2–4-rayed, 0.08–0.3 mm, mixed with fewer simple ones, usually sparsely pubescent distally, rarely glabrous.</text>
      <biological_entity id="o7887" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per caudex branch" constraintid="o7888" from="2" name="quantity" src="d0_s5" to="6" />
        <character constraint="from margin" constraintid="o7889" is_modifier="false" name="orientation" notes="" src="d0_s5" value="arising" value_original="arising" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s5" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" notes="" src="d0_s5" to="2" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o7888" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o7889" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity id="o7890" name="rosette" name_original="rosette" src="d0_s5" type="structure" />
      <biological_entity id="o7891" name="ground" name_original="ground" src="d0_s5" type="structure" />
      <biological_entity id="o7892" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o7893" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.08" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
        <character constraint="with ones" constraintid="o7894" is_modifier="false" name="arrangement" src="d0_s5" value="mixed" value_original="mixed" />
        <character is_modifier="false" modifier="usually sparsely; distally" name="pubescence" notes="" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7894" name="one" name_original="ones" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o7889" id="r571" name="part_of" negation="false" src="d0_s5" to="o7890" />
      <relation from="o7889" id="r572" name="near" negation="false" src="d0_s5" to="o7891" />
      <relation from="o7889" id="r573" name="near" negation="false" src="d0_s5" to="o7892" />
    </statement>
    <statement id="d0_s6">
      <text>Basal leaves: blade linear-oblanceolate, 1–3 (–4) mm wide, margins entire, ciliate proximally, trichomes to 0.8 mm, surfaces densely pubescent, trichomes short-stalked, 3–6-rayed, 0.08–0.3 mm.</text>
      <biological_entity constraint="basal" id="o7895" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o7896" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7897" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7898" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7899" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7900" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.08" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: 3–9, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o7901" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
      <biological_entity id="o7902" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <relation from="o7901" id="r574" name="concealing" negation="true" src="d0_s7" to="o7902" />
    </statement>
    <statement id="d0_s8">
      <text>blade auricles 0.2–1 mm, surfaces of distalmost leaves glabrous or sparsely pubescent.</text>
      <biological_entity constraint="cauline" id="o7903" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="blade" id="o7904" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7905" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o7906" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o7905" id="r575" name="part_of" negation="false" src="d0_s8" to="o7906" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes 7–15-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate, straight or slightly curved, 4–7 mm, glabrous or sparsely pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o7907" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="7-15-flowered" value_original="7-15-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s9" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o7908" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o7909" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7910" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o7907" id="r576" name="fruiting" negation="false" src="d0_s10" to="o7908" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers divaricate-ascending at anthesis;</text>
      <biological_entity id="o7911" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s11" value="divaricate-ascending" value_original="divaricate-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals pubescent;</text>
      <biological_entity id="o7912" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals white or lavender, 4–6 × 1–2 mm, glabrous;</text>
      <biological_entity id="o7913" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o7914" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits horizontal to divaricate-descending, not appressed to rachis, not secund, straight to slightly curved, edges parallel, 2.5–4 cm × 1–1.5 mm;</text>
      <biological_entity id="o7915" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s15" to="divaricate-descending" />
        <character constraint="to rachis" constraintid="o7916" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s15" value="secund" value_original="secund" />
        <character char_type="range_value" from="straight" name="course" src="d0_s15" to="slightly curved" />
      </biological_entity>
      <biological_entity id="o7916" name="rachis" name_original="rachis" src="d0_s15" type="structure" />
      <biological_entity id="o7917" name="edge" name_original="edges" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s15" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves glabrous;</text>
      <biological_entity id="o7918" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 36–54 per ovary;</text>
      <biological_entity id="o7919" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o7920" from="36" name="quantity" src="d0_s17" to="54" />
      </biological_entity>
      <biological_entity id="o7920" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.1–0.2 mm.</text>
      <biological_entity id="o7921" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate, 1–1.2 × 0.8–1 mm;</text>
      <biological_entity id="o7922" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s19" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>wing continuous, 0.1–0.15 mm wide.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 14.</text>
      <biological_entity id="o7923" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s20" to="0.15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7924" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and knolls with sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="with sagebrush" />
        <character name="habitat" value="knolls" constraint="with sagebrush" />
        <character name="habitat" value="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <discussion>Boechera gunnisoniana is most similar to B. glareosa; it is distinguished from that species by having trichomes smaller (0.08–0.3 versus 0.3–0.6 mm) and more highly divided (mostly 2–4-rayed versus simple and 2-rayed) proximally on stems, and fewer (36–54 versus 50–80) ovules per ovary. The two species are allopatric, with B. glareosa known only from eastern Utah and extreme northwestern Colorado and B. gunnisoniana apparently endemic to Gunnison County in west-central Colorado.</discussion>
  
</bio:treatment>