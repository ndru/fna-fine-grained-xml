<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">descurainieae</taxon_name>
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="genus">descurainia</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) O. E. Schulz in H. G. A. Engler" date="1924" rank="species">adenophora</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>86[IV,105]: 321. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe descurainieae;genus descurainia;species adenophora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095052</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sophia</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">adenophora</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 127. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sophia;species adenophora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Descurainia</taxon_name>
    <taxon_name authority="(Greene) O. E. Schulz" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) Detling" date="unknown" rank="subspecies">adenophora</taxon_name>
    <taxon_hierarchy>genus Descurainia;species obtusa;subspecies adenophora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular (at least distally);</text>
    </statement>
    <statement id="d0_s2">
      <text>finely pubescent, often canescent, trichomes dendritic, sometimes mixed with simple ones.</text>
      <biological_entity id="o34313" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o34314" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
        <character constraint="with ones" constraintid="o34315" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o34315" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched basally, branched distally, 4.5–13 dm.</text>
      <biological_entity id="o34316" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="4.5" from_unit="dm" name="some_measurement" src="d0_s3" to="13" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: petiole 1–3 cm;</text>
      <biological_entity constraint="basal" id="o34317" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o34318" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade pinnate, oblanceolate to obovate or ovate in outline, 2–10 cm, lateral lobes (2–5 pairs), oblanceolate to lanceolate, (4–12 × 1–5 mm),margins entire or serrate to crenate, (apex obtuse).</text>
      <biological_entity constraint="basal" id="o34319" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o34320" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" constraint="in outline" constraintid="o34321" from="oblanceolate" name="shape" src="d0_s5" to="obovate or ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34321" name="outline" name_original="outline" src="d0_s5" type="structure" />
      <biological_entity constraint="lateral" id="o34322" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o34323" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="serrate to crenate" value_original="serrate to crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves sessile or shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o34324" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade smaller distally, distal lobes often narrower, surfaces densely pubescent.</text>
      <biological_entity id="o34325" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o34326" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="width" src="d0_s7" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o34327" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o34329" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate, straight, 13–31 mm.</text>
      <biological_entity id="o34328" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o34329" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34330" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o34328" id="r2304" name="fruiting" negation="false" src="d0_s9" to="o34330" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ascending, greenish to yellowish, oblong, 2–2.9 mm, pubescent, (trichomes dendritic, mixed with glandular papillae);</text>
      <biological_entity id="o34331" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34332" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals oblanceolate, 1.8–2.6 × 0.5–0.7mm;</text>
      <biological_entity id="o34333" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34334" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s11" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>median filaments 1.8–2.4 mm;</text>
      <biological_entity id="o34335" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="median" id="o34336" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.3–0.5 mm.</text>
      <biological_entity id="o34337" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34338" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits divaricate to erect, linear, slightly torulose, 8–16 (–20) × 1–1.3 mm, (abruptly acute at both ends);</text>
      <biological_entity id="o34339" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="20" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s14" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with distinct midvein, (sparsely pubescent or glabrescent);</text>
      <biological_entity id="o34340" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o34341" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o34340" id="r2305" name="with" negation="false" src="d0_s15" to="o34341" />
    </statement>
    <statement id="d0_s16">
      <text>septum not veined;</text>
      <biological_entity id="o34342" name="septum" name_original="septum" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 48–64 per ovary;</text>
      <biological_entity id="o34343" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o34344" from="48" name="quantity" src="d0_s17" to="64" />
      </biological_entity>
      <biological_entity id="o34344" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.1–0.2 mm, glabrous.</text>
      <biological_entity id="o34345" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds biseriate, light-brown, ellipsoid, 0.9–1.1 × 0.5–0.6 mm. 2n = 42.</text>
      <biological_entity id="o34346" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s19" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s19" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34347" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open forests, sandy grounds, gravelly flats, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open forests" />
        <character name="habitat" value="sandy grounds" />
        <character name="habitat" value="gravelly flats" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex.; Mexico (Baja California, Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Both L. E. Detling (1939) and R. C. Rollins (1993) treated Descurainia adenophora as a subspecies of D. obtusa, but the differences are so substantial that they should be recognized as distinct species. From the latter, D. adenophora is distinguished by being hexaploid (versus diploid) with densely glandular (versus eglandular) distal parts, longer sepals (2–2.9 versus 1–2 mm) and petals (1.8–2.6 versus 1.2–2 mm), longer fruiting pedicels (13–31 versus 6–15 mm), biseriate (versus uniseriate) seeds, and more ovules (42–64 versus 16–40) per ovary.</discussion>
  
</bio:treatment>