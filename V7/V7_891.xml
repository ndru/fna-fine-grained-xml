<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">555</other_info_on_meta>
    <other_info_on_meta type="treatment_page">556</other_info_on_meta>
    <other_info_on_meta type="illustration_page">554</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">eutremeae</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="genus">eutrema</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="species">edwardsii</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>9, plate A. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe eutremeae;genus eutrema;species edwardsii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009540</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eutrema</taxon_name>
    <taxon_name authority="Turczaninow" date="unknown" rank="species">edwardsii</taxon_name>
    <taxon_name authority="(Rollins) W. A. Weber" date="unknown" rank="subspecies">penlandii</taxon_name>
    <taxon_hierarchy>genus Eutrema;species edwardsii;subspecies penlandii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eutrema</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">labradoricum</taxon_name>
    <taxon_hierarchy>genus Eutrema;species labradoricum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eutrema</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">penlandii</taxon_name>
    <taxon_hierarchy>genus Eutrema;species penlandii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(roots fleshy).</text>
      <biological_entity id="o14594" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or few from caudex, erect, unbranched, (0.3–) 0.8–3 (–4.5) dm.</text>
      <biological_entity id="o14595" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="from caudex" constraintid="o14596" is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.8" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o14596" name="caudex" name_original="caudex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>rosulate;</text>
      <biological_entity constraint="basal" id="o14597" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–4.5 (–6) cm;</text>
      <biological_entity id="o14598" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (somewhat fleshy), ovate, lanceolate, or oblong, (0.3–) 0.8–2 (–2.5) cm × (1–) 4–14 (–18) mm, (base truncate, obtuse, or cuneate, sometimes oblique), margins entire, apex obtuse.</text>
      <biological_entity id="o14599" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s6" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="18" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14600" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14601" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 3–7 (–10);</text>
    </statement>
    <statement id="d0_s8">
      <text>petiolate or (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o14602" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade ovate, lanceolate, oblong, or linear, (0.7–) 1–3 (–4) cm × (1–) 3–10 (–14) mm, base cuneate, margins entire, apex subacute.</text>
      <biological_entity id="o14603" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s9" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s9" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s9" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14604" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o14605" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14606" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o14608" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels divaricate to ascending, (curved-ascending or straight), (1.5–) 3–10 (–15) mm.</text>
      <biological_entity id="o14607" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o14608" is_modifier="false" modifier="considerably" name="length" src="d0_s10" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14609" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o14607" id="r1019" name="fruiting" negation="false" src="d0_s11" to="o14609" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals ovate, 1.5–3 × 1–1.5 mm, (margins membranous);</text>
      <biological_entity id="o14610" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14611" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spatulate, 3–5 × 1.5–3 mm;</text>
      <biological_entity id="o14612" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14613" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments 2–2.5 mm;</text>
      <biological_entity id="o14614" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o14615" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, 0.2–0.4 mm;</text>
      <biological_entity id="o14616" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o14617" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore usually 0.2–1 mm, rarely obsolete.</text>
      <biological_entity id="o14618" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o14619" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s16" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits stipitate, not torulose, linear to narrowly oblong, (0.7–) 1–2 (–2.5) cm × 2–3 mm slightly 4-angled;</text>
      <biological_entity id="o14620" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s17" to="narrowly oblong" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s17" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s17" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s17" to="2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves (cuneate basally and distally), each with prominent midvein;</text>
      <biological_entity id="o14621" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o14622" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o14621" id="r1020" name="with" negation="false" src="d0_s18" to="o14622" />
    </statement>
    <statement id="d0_s19">
      <text>septum mostly perforate;</text>
      <biological_entity id="o14623" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s19" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules (6–) 8–12 (–14) per ovary;</text>
      <biological_entity id="o14624" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s20" to="8" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="14" />
        <character char_type="range_value" constraint="per ovary" constraintid="o14625" from="8" name="quantity" src="d0_s20" to="12" />
      </biological_entity>
      <biological_entity id="o14625" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.2–1 mm.</text>
      <biological_entity id="o14626" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds usually oblong, rarely ovoid, (1.5–) 2–3 × (0.7–) 1–1.5 mm. 2n = 18, 28, 42, 56.</text>
      <biological_entity id="o14627" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s22" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s22" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" src="d0_s22" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s22" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14628" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="18" value_original="18" />
        <character name="quantity" src="d0_s22" value="28" value_original="28" />
        <character name="quantity" src="d0_s22" value="42" value_original="42" />
        <character name="quantity" src="d0_s22" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Jun-early Aug, fruiting Jul-early Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="early Jun" />
        <character name="fruiting time" char_type="range_value" to="early Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra, talus slopes, glaciated hills, grassy margins of streams, wet areas of peat ridges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="hills" modifier="glaciated" />
        <character name="habitat" value="grassy margins" constraint="of streams , wet areas of peat ridges" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="wet areas" constraint="of peat ridges" />
        <character name="habitat" value="peat ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T., Nunavut, Que., Yukon; Alaska, Colo.; Asia (Mongolia, Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Asia (Mongolia)" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Plants of Eutrema edwardsii from Alaska, northern Canada, and Greenland grow at elevations of 0–1900 m; those of Colorado, treated by R. C. Rollins (1993) as E. penlandii, grow at 3700–3900 m.</discussion>
  <discussion>An examination of collections of Eutrema edwardsii from its entire range amply demonstrates that it is a highly variable species that can easily accommodate E. penlandii. Although the latter is known from the high mountains of Park County (Colorado), its plants are indistinguishable from those of E. edwardsii from Greenland, and the higher latitudes in Alaska, British Columbia, Northwest Territories, Nunavut, and Yukon. In general, plants of E. edwardsii from lower latitudes tend to be far more robust than those of higher latitudes. R. C. Rollins (1993) indicated that E. penlandii is distinguished from E. edwardsii by having stems less than 10 (versus 10–45) cm, lingulate (versus obovate) petals, fruiting pedicels less that 4 (versus 5–10) mm, infructescence less than 5 (versus to 20) cm, and cordate (versus truncate or cuneate) basal leaves. In fact, the petals of E. penlandii are spatulate, just as those of most plants of E. edwardsii, and the basal leaves in E. penlandii (e.g., Penland 3909 and Weber 13315, both at GH) are cuneate. Furthermore, some plants from Greenland (e.g., Raup et al. 465, GH) are almost identical to those of E. penlandii, and they are only 3–8 cm with narrowly obovate petals, fruiting pedicels 2–4 mm, and infructescence 2–3 cm. Other examples of E. edwardsii that show some or all of the above aspects of E. penlandii could be cited. Weber treated E. penlandii as a subspecies of E. edwardsii, but the only difference I see between the two is geographic. Such distinction is unacceptable because the widely disjunct populations of E. edwardsii within Russia would have to be treated that way too.</discussion>
  <discussion>As Eutrema penlandii, E. edwardsii is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>