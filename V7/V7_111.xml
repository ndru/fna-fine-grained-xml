<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="Andersson" date="1858" rank="species">myrtillifolia</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>15: 132. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species myrtillifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445799</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–0.6 (–1) m, (forming clones by layering).</text>
      <biological_entity id="o37286" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (decumbent);</text>
      <biological_entity id="o37287" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branches gray-brown, redbrown, or yellowbrown, not to strongly glaucous (dull or slightly glossy), pubescent;</text>
      <biological_entity id="o37288" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="not to strongly" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets gray-brown, redbrown, or yellowbrown, sparsely pubescent.</text>
      <biological_entity id="o37289" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules rudimentary, foliaceous, or absent on early ones, foliaceous on late ones (0.2–1.8 (–5) mm);</text>
      <biological_entity id="o37290" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o37291" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character constraint="on ones" constraintid="o37292" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="on ones" constraintid="o37293" is_modifier="false" name="architecture" notes="" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o37292" name="one" name_original="ones" src="d0_s4" type="structure" />
      <biological_entity id="o37293" name="one" name_original="ones" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" name="quantity" src="d0_s4" to="1.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole deeply to shallowly grooved adaxially, 1.5–8 mm, glabrous or pubescent adaxially;</text>
      <biological_entity id="o37294" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o37295" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="deeply to shallowly; adaxially" name="architecture" src="d0_s5" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade (sometimes amphistomatous), elliptic, narrowly elliptic, obovate, or broadly obovate, 17–74 × 8–30 mm, 1.2–4.5 times as long as wide, base cuneate, convex, or subcordate, margins flat, serrulate, crenulate, or sinuate, apex acute, convex, or acuminate, abaxial surface not glaucous, glabrous, adaxial slightly glossy, glabrous;</text>
      <biological_entity id="o37296" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s6" to="74" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.2-4.5" value_original="1.2-4.5" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o37297" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o37298" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o37299" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o37300" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o37301" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o37302" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins crenate;</text>
      <biological_entity id="o37303" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o37304" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade reddish or yellowish green, glabrous.</text>
      <biological_entity id="o37305" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o37306" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o37308" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminate stout, 11.5–39 × 5–14 mm, flowering branchlet 0.5–6 mm;</text>
      <biological_entity id="o37307" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character constraint="as leaves" constraintid="o37308" is_modifier="false" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate moderately densely flowered or densely flowered, slender or stout, 16–46 (–50 in fruit) × 4–15 mm, flowering branchlet 1.5–12 mm;</text>
      <biological_entity id="o37309" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37310" name="branchlet" name_original="branchlet" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s11" value="flowered" value_original="flowered" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s11" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>floral bract brown, black, tawny, or bicolor, 0.4–1.1 mm, apex retuse or acute, abaxially hairy throughout or proximally, hairs curly or wavy.</text>
      <biological_entity constraint="floral" id="o37311" name="bract" name_original="bract" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37312" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially; throughout; throughout; proximally" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o37313" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="curly" value_original="curly" />
        <character is_modifier="false" name="shape" src="d0_s12" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers: adaxial nectary oblong, ovate, or square, 0.2–0.34–0.4 mm;</text>
      <biological_entity id="o37314" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o37315" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="square" value_original="square" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.34-0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct, glabrous;</text>
      <biological_entity id="o37316" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o37317" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers purple turning yellow, 0.3–0.6 mm.</text>
      <biological_entity id="o37318" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o37319" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: adaxial nectary square, oblong, or ovate, 0.2–0.4 mm, shorter than stipe;</text>
      <biological_entity id="o37320" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o37321" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
        <character constraint="than stipe" constraintid="o37322" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o37322" name="stipe" name_original="stipe" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>stipe 0.6–1.7 mm;</text>
      <biological_entity id="o37323" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37324" name="stipe" name_original="stipe" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary pyriform, glabrous, beak gradually tapering to or slightly bulged below styles;</text>
      <biological_entity id="o37325" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37326" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o37327" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="gradually; slightly" name="shape" src="d0_s18" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o37328" name="style" name_original="styles" src="d0_s18" type="structure" />
      <relation from="o37327" id="r2510" name="bulged" negation="false" src="d0_s18" to="o37328" />
    </statement>
    <statement id="d0_s19">
      <text>ovules (6–) 10–14 per ovary;</text>
      <biological_entity id="o37329" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37330" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s19" to="10" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o37331" from="10" name="quantity" src="d0_s19" to="14" />
      </biological_entity>
      <biological_entity id="o37331" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>styles connate or distinct 1/2 their lengths, 0.3–0.7 mm;</text>
      <biological_entity id="o37332" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37333" name="style" name_original="styles" src="d0_s20" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
        <character name="lengths" src="d0_s20" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s20" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or 2 plump lobes, 0.16–0.23–0.32 mm.</text>
      <biological_entity id="o37334" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37335" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s21" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o37336" is_modifier="false" modifier="abaxially" name="relief" src="d0_s21" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.16" from_unit="mm" name="some_measurement" notes="" src="d0_s21" to="0.23-0.32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37336" name="tip" name_original="tip" src="d0_s21" type="structure">
        <character is_modifier="true" name="shape" src="d0_s21" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o37337" name="lobe" name_original="lobes" src="d0_s21" type="structure">
        <character is_modifier="true" name="size" src="d0_s21" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 4–6 mm. 2n = 38.</text>
      <biological_entity id="o37338" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s22" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o37339" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early May-late Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jul" from="early May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Treed bogs, fens, stream banks, subalpine spruce thickets, Pinus contorta woods, sand dunes, coal spoils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="treed bogs" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="thickets" modifier="subalpine spruce" />
        <character name="habitat" value="pinus contorta woods" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="coal spoils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., Nunavut, Ont., Sask., Yukon; Alaska, Colo., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>56.</number>
  <other_name type="common_name">Low blueberry willow</other_name>
  <discussion>Salix myrtillifolia occurs in Nunavut on Akimiski Island in James Bay.</discussion>
  <discussion>The complex of species related to Salix myrtillifolia includes S. arizonica, S. ballii, S. boothii, and S. pseudomyrsinites. Two are diploid (S. arizonica and S. myrtillifolia), and two are tetraploid (S. boothii and S. pseudomyrsinites); the chromosome number of S. ballii is unknown. They have been treated taxonomically in different ways, but are relatively distinct in their morphology, ecology, and geography. Salix myrtillifolia has outlying populations represented by single collections each from Colorado, Quebec, and Wyoming. Specimens attributed to this species from the Gaspe Peninsula, Quebec, and the Northern Peninsula, Newfoundland, all have evidence of leaf glaucescence and are S. ballii. See 57. S. pseudomyrsinites and 58. S. ballii for more description.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix myrtillifolia forms natural hybrids with S. candida.</discussion>
  
</bio:treatment>