<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="illustration_page">376</other_info_on_meta>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
    <other_info_on_meta type="illustration_page">366</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(M. E. Jones) Al-Shehbaz" date="2003" rank="species">glaucovalvula</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>13: 385. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species glaucovalvula</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094549</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">glaucovalvula</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>8: 40. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species glaucovalvula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>short to long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>sexual;</text>
      <biological_entity id="o36942" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex usually not woody.</text>
      <biological_entity id="o36943" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, (0.6–) 1–4.5 dm, densely pubescent proximally, trichomes short-stalked, 4–8-rayed, 0.1–0.4 mm, similarly pubescent distally.</text>
      <biological_entity id="o36944" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="per caudex branch" constraintid="o36945" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from center" constraintid="o36946" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="4.5" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o36945" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o36946" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o36947" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o36948" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o36949" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o36950" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="similarly; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o36946" id="r2487" name="part_of" negation="false" src="d0_s4" to="o36947" />
      <relation from="o36946" id="r2488" name="near" negation="false" src="d0_s4" to="o36948" />
      <relation from="o36946" id="r2489" name="near" negation="false" src="d0_s4" to="o36949" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade linear to linear-oblanceolate, 2–4 (–6) mm wide, margins entire, usually ciliate at petiole base, trichomes (simple or spurred), to 1.5 mm, surfaces densely pubescent, trichomes short-stalked, 4–8-rayed, 0.1–0.4 mm.</text>
      <biological_entity constraint="basal" id="o36951" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o36952" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36953" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="at petiole base" constraintid="o36954" is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o36954" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o36955" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36956" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o36957" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 6–10, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o36958" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o36959" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o36958" id="r2490" name="concealing" negation="true" src="d0_s6" to="o36959" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles absent, surfaces of distalmost leaves densely pubescent.</text>
      <biological_entity constraint="cauline" id="o36960" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o36961" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o36962" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o36963" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o36962" id="r2491" name="part_of" negation="false" src="d0_s7" to="o36963" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes (5–) 10–25-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels reflexed, strongly curved at base, 2–10 mm, pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o36964" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(5-)10-25-flowered" value_original="(5-)10-25-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o36965" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o36966" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o36967" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o36964" id="r2492" name="fruiting" negation="false" src="d0_s9" to="o36965" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers divaricate at anthesis;</text>
      <biological_entity id="o36968" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o36969" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals light purple to lavender, 6–9 × 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o36970" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="light purple" name="coloration" src="d0_s12" to="lavender" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o36971" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits strongly reflexed, sometimes appressed to rachis, often secund, straight, edges parallel, (1.8–) 2.5–4.5 cm × 5–8 mm;</text>
      <biological_entity id="o36972" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character constraint="to rachis" constraintid="o36973" is_modifier="false" modifier="sometimes" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="often" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o36973" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o36974" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="atypical_length" src="d0_s14" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s14" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o36975" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 24–62 per ovary;</text>
      <biological_entity id="o36976" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o36977" from="24" name="quantity" src="d0_s16" to="62" />
      </biological_entity>
      <biological_entity id="o36977" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.2–0.6 mm.</text>
      <biological_entity id="o36978" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds biseriate, 5–6 × 4–5 mm;</text>
      <biological_entity id="o36979" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s18" value="biseriate" value_original="biseriate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s18" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 1.8–2.5 mm wide.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o36980" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s19" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36981" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and gravelly soil, usually under shelter of desert shrubs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="shelter" modifier="usually" constraint="of desert shrubs" />
        <character name="habitat" value="desert shrubs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion>Boechera glaucovalvula, a distinctive sexual diploid that has the widest fruits in the genus, is known from Inyo, Mono, Riverside, and San Bernardino counties, California, and Clark and Nye counties, Nevada.</discussion>
  
</bio:treatment>