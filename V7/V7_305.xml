<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker,Hugh H. Iltis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">TARENAYA</taxon_name>
    <place_of_publication>
      <publication_title>Sylva Tellur.,</publication_title>
      <place_in_publication>111. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus TARENAYA</taxon_hierarchy>
    <other_info_on_name type="etymology">Origin obscure</other_info_on_name>
    <other_info_on_name type="fna_id">132319</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="(Rafinesque) H. H. Iltis" date="unknown" rank="section">Tarenaya</taxon_name>
    <taxon_hierarchy>genus Cleome;section Tarenaya;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs [shrubs], annual.</text>
      <biological_entity id="o27817" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely to profusely branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or glandular-pubescent (sometimes spiny).</text>
      <biological_entity id="o27818" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to profusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipular spines present [absent];</text>
      <biological_entity id="o27819" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="stipular" id="o27820" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole with pulvinus basally or distally, (petiolule base adnate, forming pulvinar disc; spines present);</text>
      <biological_entity id="o27821" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27822" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o27823" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o27822" id="r1867" modifier="distally" name="with" negation="false" src="d0_s4" to="o27823" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets [1 or] (3) 5 or 7 [–11] (with tiny prickles terminating teeth).</text>
      <biological_entity id="o27824" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o27825" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="11" />
        <character name="quantity" src="d0_s5" value="7" value_original="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary (from distal leaves), racemes (flat-topped or elongated);</text>
      <biological_entity id="o27826" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o27827" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present [absent].</text>
      <biological_entity id="o27828" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (often appearing unisexual due to incomplete development), zygomorphic;</text>
      <biological_entity id="o27829" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent, distinct, equal (each often subtending a nectary);</text>
      <biological_entity id="o27830" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals equal;</text>
      <biological_entity id="o27831" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6;</text>
      <biological_entity id="o27832" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted on a discoid or conical androgynophore, glabrous;</text>
      <biological_entity id="o27833" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27834" name="androgynophore" name_original="androgynophore" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="discoid" value_original="discoid" />
        <character is_modifier="true" name="shape" src="d0_s12" value="conical" value_original="conical" />
      </biological_entity>
      <relation from="o27833" id="r1868" name="inserted on a" negation="false" src="d0_s12" to="o27834" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (linear), coiling as pollen released;</text>
      <biological_entity id="o27835" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o27836" is_modifier="false" name="shape" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o27836" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore recurved in fruit.</text>
      <biological_entity id="o27837" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o27838" is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o27838" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, dehiscent, oblong.</text>
      <biological_entity constraint="fruits" id="o27839" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 10–30+, triangular to subglobose, not arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10.</text>
      <biological_entity id="o27840" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s16" to="30" upper_restricted="false" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s16" to="subglobose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o27841" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; South America; introduced worldwide in tropical and warm-temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="worldwide in tropical and warm-temperate regions" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Spiderflower</other_name>
  <discussion>Species 33 (2 in the flora).</discussion>
  <discussion>Traditionally included in a broad circumscription of Cleome, Tarenaya is distinguished by its stipular and petiolar spines, absence of arils, and seeds with a large cleft cavity. All species are native to tropical America, with the exception of T. afrospina (H. H. Iltis) H. H. Iltis, of western Africa. Tarenaya hassleriana, long known as C. spinosa (and commonly by the misapplied name C. houtteana) and more recently as C. hassleriana, is a popular garden ornamental and, probably, the most widely distributed member of the family.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals, ovary, and capsules glabrous; petals usually pink or purple, sometimes white.</description>
      <determination>1 Tarenaya hassleriana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals, ovary, and capsules ± glandular-pubescent; petals white or greenish white.</description>
      <determination>2 Tarenaya spinosa</determination>
    </key_statement>
  </key>
</bio:treatment>