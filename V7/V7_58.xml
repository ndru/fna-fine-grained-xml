<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="illustration_page">65</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Dumortier" date="1826" rank="section">chamaetia</taxon_name>
    <taxon_name authority="Hooker" date="1838" rank="species">nivalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 152. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section chamaetia;species nivalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445803</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nivalis</taxon_name>
    <taxon_name authority="(Rydberg) C. K. Schneider" date="unknown" rank="variety">saximontana</taxon_name>
    <taxon_hierarchy>genus Salix;species nivalis;variety saximontana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reticulata</taxon_name>
    <taxon_name authority="(Hooker) Á. Löve, D. Löve &amp; B. M. Kapoor" date="unknown" rank="subspecies">nivalis</taxon_name>
    <taxon_hierarchy>genus Salix;species reticulata;subspecies nivalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reticulata</taxon_name>
    <taxon_name authority="(Rydberg) Kelso" date="unknown" rank="variety">saximontana</taxon_name>
    <taxon_hierarchy>genus Salix;species reticulata;variety saximontana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.01–0.04 m, (dwarf, forming clonal mats by rhizomes).</text>
      <biological_entity id="o31034" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.01" from_unit="m" name="some_measurement" src="d0_s0" to="0.04" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems trailing or erect;</text>
      <biological_entity id="o31035" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellowbrown or redbrown, glabrous or pubescent;</text>
      <biological_entity id="o31036" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown or redbrown, glabrous or pilose.</text>
      <biological_entity id="o31037" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules absent or rudimentary;</text>
      <biological_entity id="o31038" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31039" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5–7 mm (sometimes glandular distally or throughout);</text>
      <biological_entity id="o31040" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o31041" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade hypostomatous, (veins impressed-reticulate, 2 pairs of secondary-veins arising at or close to base, arcing toward apex,), elliptic to broadly elliptic, 6–22 × 4–15 mm, 1.1–2.8 times as long as wide, base convex, rounded, subcordate, or cuneate, margins slightly revolute, entire (glandular-dotted), apex convex, rounded, or retuse, abaxial surface glabrous or with long-silky hairs, adaxial slightly glossy, glabrous;</text>
      <biological_entity id="o31042" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o31043" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="hypostomatous" value_original="hypostomatous" />
        <character char_type="range_value" from="elliptic" name="arrangement_or_shape" src="d0_s6" to="broadly elliptic" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.1-2.8" value_original="1.1-2.8" />
      </biological_entity>
      <biological_entity id="o31044" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o31045" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o31046" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31047" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="with long-silky hairs" value_original="with long-silky hairs" />
      </biological_entity>
      <biological_entity id="o31048" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s6" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31049" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o31047" id="r2078" name="with" negation="false" src="d0_s6" to="o31048" />
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o31050" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o31051" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade glabrous.</text>
      <biological_entity id="o31052" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o31053" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 7–19 × 2.5–6 mm, flowering branchlet 0.5–17 mm;</text>
      <biological_entity id="o31054" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="19" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31055" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely to loosely flowered (4–17 flowers), stout, subglobose or globose, 7–21 × 2–9 mm, flowering branchlet 1–10 mm;</text>
      <biological_entity id="o31056" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s10" to="densely loosely flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="21" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31057" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract tawny or light rose, 0.8–1.8 mm, apex rounded, entire, abaxially glabrous.</text>
      <biological_entity id="o31058" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o31059" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="light rose" value_original="light rose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31060" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary 0.5–1.3 mm, adaxial nectary narrowly oblong, oblong, or square, 0.5–1.2 mm, nectaries connate and cupshaped;</text>
      <biological_entity id="o31061" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31062" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31063" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31064" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous or hairy basally;</text>
      <biological_entity id="o31065" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o31066" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ellipsoid or shortly cylindrical, 0.4–0.6 mm.</text>
      <biological_entity id="o31067" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o31068" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: abaxial nectary (0–) 0.2–0.5 mm, adaxial nectary oblong, 0.2–1 mm, longer than stipe, nectaries distinct or connate and shallowly cupshaped;</text>
      <biological_entity id="o31069" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31070" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31071" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character constraint="than stipe" constraintid="o31072" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o31072" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
      <biological_entity id="o31073" name="nectary" name_original="nectaries" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s15" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stipe 0–0.8 mm;</text>
      <biological_entity id="o31074" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31075" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary obturbinate, short-silky, hairs flattened, beak abruptly tapering to styles;</text>
      <biological_entity id="o31076" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31077" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="obturbinate" value_original="obturbinate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="short-silky" value_original="short-silky" />
      </biological_entity>
      <biological_entity id="o31078" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o31079" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character constraint="to styles" constraintid="o31080" is_modifier="false" modifier="abruptly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o31080" name="style" name_original="styles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–10 per ovary;</text>
      <biological_entity id="o31081" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31082" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31083" from="8" name="quantity" src="d0_s18" to="10" />
      </biological_entity>
      <biological_entity id="o31083" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles distinct to connate 1/2 their lengths, 0.2–0.4 mm;</text>
      <biological_entity id="o31084" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31085" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="distinct" name="fusion" src="d0_s19" to="connate" />
        <character name="lengths" src="d0_s19" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s19" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded tip, 0.2–0.26–0.36 mm.</text>
      <biological_entity id="o31086" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o31087" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o31088" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.26-0.36" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31088" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 3–4 mm. 2n = 38.</text>
      <biological_entity id="o31089" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s21" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31090" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun-late Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Aug" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra, cirques, lake basins, rocky slopes and ridges, fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine tundra" />
        <character name="habitat" value="cirques" />
        <character name="habitat" value="lake basins" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Dwarf snow willow</other_name>
  <discussion>Because geographic overlap is small and evidence of intergradation is tenuous, Salix nivalis is best treated as a species separate from S. reticulata; S. nivalis was previously treated as a subspecies of S. reticulata (G. W. Argus 1986b, 1991).</discussion>
  
</bio:treatment>