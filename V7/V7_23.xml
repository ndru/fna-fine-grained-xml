<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">36</other_info_on_meta>
    <other_info_on_meta type="mention_page">37</other_info_on_meta>
    <other_info_on_meta type="treatment_page">34</other_info_on_meta>
    <other_info_on_meta type="illustration_page">35</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="Kimura" date="1928" rank="subgenus">protitea</taxon_name>
    <taxon_name authority="Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">humboldtianae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">caroliniana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 226. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus protitea;section humboldtianae;species caroliniana</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242417196</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Shuttleworth ex Andersson" date="unknown" rank="species">longipes</taxon_name>
    <taxon_name authority="(Andersson) C. K. Schneider" date="unknown" rank="variety">venulosa</taxon_name>
    <taxon_hierarchy>genus Salix;species longipes;variety venulosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longipes</taxon_name>
    <taxon_name authority="(Bebb) C. K. Schneider" date="unknown" rank="variety">wardii</taxon_name>
    <taxon_hierarchy>genus Salix;species longipes;variety wardii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 5–10 m.</text>
      <biological_entity id="o28914" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches ± brittle at base, gray-brown to redbrown, glabrous, villous, or tomentose;</text>
      <biological_entity id="o28915" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28916" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o28917" is_modifier="false" modifier="more or less" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character char_type="range_value" from="gray-brown" name="coloration" notes="" src="d0_s1" to="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o28917" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellowbrown to redbrown, glabrous, sparsely or densely villous or tomentose.</text>
      <biological_entity id="o28918" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o28919" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s2" to="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="or" value_original="or" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules rudimentary or foliaceous on early ones, foliaceous on late ones, apex convex to acute;</text>
      <biological_entity id="o28920" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28921" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character constraint="on ones" constraintid="o28922" is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
        <character constraint="on ones" constraintid="o28923" is_modifier="false" name="architecture" notes="" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o28922" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o28923" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o28924" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (with spherical glands distally), (3–) 4.5–14 (–22) mm, tomentose or pilose adaxially;</text>
      <biological_entity id="o28925" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s4" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s4" to="22" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28926" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade lorate or lanceolate to narrowly lanceolate, (50–) 75–115 (–220) × 10–22 (–35) mm, 5–10 times as long as wide, base usually convex or cuneate, sometimes rounded to cordate, margins serrate or serrulate, apex acuminate, acute, or caudate, abaxial surface glabrous or sparsely tomentose on midribs, hairs white and/or ferruginous, wavy, adaxial highly glossy, glabrous or pilose, hairs white and/or ferruginous;</text>
      <biological_entity id="o28927" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o28928" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="narrowly lanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="atypical_length" src="d0_s5" to="75" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="115" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="220" to_unit="mm" />
        <character char_type="range_value" from="75" from_unit="mm" name="length" src="d0_s5" to="115" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="22" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="5-10" value_original="5-10" />
      </biological_entity>
      <biological_entity id="o28929" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="rounded" modifier="sometimes" name="shape" src="d0_s5" to="cordate" />
      </biological_entity>
      <biological_entity id="o28930" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o28931" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="caudate" value_original="caudate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28932" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="on midribs" constraintid="o28933" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o28933" name="midrib" name_original="midribs" src="d0_s5" type="structure" />
      <biological_entity id="o28934" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ferruginous" value_original="ferruginous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28935" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28936" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ferruginous" value_original="ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o28937" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o28938" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade glabrous, or moderately densely tomentose or silky abaxially, hairs white and ferruginous.</text>
      <biological_entity id="o28939" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o28940" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately densely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately densely" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity id="o28941" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white and ferruginous" value_original="white and ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins: staminate 28–97 × 5–11 mm, flowering branchlet 4–25 mm;</text>
      <biological_entity id="o28942" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="28" from_unit="mm" name="length" src="d0_s8" to="97" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28943" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate 33–93 × 7–15 mm, flowering branchlet 3–35 mm;</text>
      <biological_entity id="o28944" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="33" from_unit="mm" name="length" src="d0_s9" to="93" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28945" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>floral bract 1–3 mm, apex acute or rounded, entire or erose, abaxially sparsely hairy, hairs wavy;</text>
      <biological_entity id="o28946" name="catkin" name_original="catkins" src="d0_s10" type="structure" />
      <biological_entity constraint="floral" id="o28947" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28948" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o28949" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate bract deciduous after flowering.</text>
      <biological_entity id="o28950" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity id="o28951" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character constraint="after flowering" is_modifier="false" name="duration" src="d0_s11" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary 0.3–0.5 mm, adaxial nectary oblong to narrowly oblong, 0.3–0.6 mm, nectaries distinct;</text>
      <biological_entity id="o28952" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28953" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28954" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="narrowly oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28955" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 4–7;</text>
      <biological_entity id="o28956" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o28957" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments (sometimes connate less than 1/2 their lengths), hairy basally;</text>
      <biological_entity id="o28958" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="basally" name="pubescence" notes="" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o28959" name="filament" name_original="filaments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.4–0.6 mm.</text>
      <biological_entity id="o28960" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o28961" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: adaxial nectary oblong, square, or ovate, 0.3–0.7 mm;</text>
      <biological_entity id="o28962" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28963" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stipe 1.3–5.3 mm;</text>
      <biological_entity id="o28964" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28965" name="stipe" name_original="stipe" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s17" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary pyriform to obclavate, beak slightly bulged below styles;</text>
      <biological_entity id="o28966" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28967" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character char_type="range_value" from="pyriform" name="shape" src="d0_s18" to="obclavate" />
      </biological_entity>
      <biological_entity id="o28968" name="beak" name_original="beak" src="d0_s18" type="structure" />
      <biological_entity id="o28969" name="style" name_original="styles" src="d0_s18" type="structure" />
      <relation from="o28968" id="r1950" name="bulged" negation="false" src="d0_s18" to="o28969" />
    </statement>
    <statement id="d0_s19">
      <text>ovules 12–16 per ovary;</text>
      <biological_entity id="o28970" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28971" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o28972" from="12" name="quantity" src="d0_s19" to="16" />
      </biological_entity>
      <biological_entity id="o28972" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>styles (sometimes distinct distally), 0.1–0.2 mm;</text>
      <biological_entity id="o28973" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28974" name="style" name_original="styles" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>stigmas 0.16–0.2–0.28 mm.</text>
      <biological_entity id="o28975" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o28976" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.16" from_unit="mm" name="some_measurement" src="d0_s21" to="0.2-0.28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 4–6 mm.</text>
      <biological_entity id="o28977" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s22" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (south) Dec-early May, (north) mid Apr-early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="south" to="early May" from="Dec" />
        <character name="flowering time" char_type="range_value" modifier="north" to="early Jun" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alluvial woods on floodplains, swamps, hummocks, marshes, wet interdunal depressions, rocky or gravelly streambeds, ditches, canals, usually on calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alluvial woods" constraint="on floodplains , swamps , hummocks , marshes , wet interdunal depressions , rocky or gravelly streambeds , ditches , canals , usually" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="hummocks" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="wet interdunal depressions" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="gravelly streambeds" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="canals" />
        <character name="habitat" value="calcareous substrates" modifier="usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., D.C., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Miss., Mo., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; Mexico (Nuevo León); West Indies (Cuba); Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Carolina or coastal plain willow</other_name>
  <discussion>Hybrids:</discussion>
  <discussion>Salix caroliniana forms natural hybrids with S. nigra. Hybrids with S. amygdaloides have been reported (N. M. Glatfelter 1898); no convincing specimens have been seen.</discussion>
  <discussion>Salix caroliniana × S. nigra: This hybrid is characterized by stipes to 1.3 mm and leaves glaucous; it probably occurs wherever the two parents come into contact. In the southeastern United States, it occurs from northern Florida to West Virginia and Maryland with intergradation mainly on the Atlantic coastal plain from northern Florida and southern Georgia (G. W. Argus 1986). Reports (N. M. Glatfelter 1898) of it from the vicinity of St. Louis, Missouri, are unconfirmed.</discussion>
  
</bio:treatment>