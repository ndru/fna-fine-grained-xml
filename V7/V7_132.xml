<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="treatment_page">125</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="A. Kerner" date="1860" rank="section">nigricantes</taxon_name>
    <taxon_name authority="Salisbury" date="1796" rank="species">myrsinifolia</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Stirp. Chap. Allerton,</publication_title>
      <place_in_publication>394. 1796</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section nigricantes;species myrsinifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094918</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">nigricans</taxon_name>
    <taxon_hierarchy>genus Salix;species nigricans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branches dark redbrown or gray-brown, not glaucous, glabrous or hairy, (peeled wood smooth or striate with relatively few, short striae);</text>
      <biological_entity id="o15005" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o15006" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark redbrown" value_original="dark redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets redbrown, moderately to very densely pubescent or velvety.</text>
      <biological_entity id="o15007" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o15008" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="moderately to very; very densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="velvety" value_original="velvety" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules usually foliaceous, sometimes minute rudiments on early ones, foliaceous on late ones, (ca. 4 mm), apex acute;</text>
      <biological_entity id="o15009" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15010" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o15011" name="rudiment" name_original="rudiments" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="sometimes" name="size" src="d0_s2" value="minute" value_original="minute" />
        <character constraint="on ones" constraintid="o15013" is_modifier="false" name="architecture" notes="" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o15012" name="one" name_original="ones" src="d0_s2" type="structure" />
      <biological_entity id="o15013" name="one" name_original="ones" src="d0_s2" type="structure" />
      <biological_entity id="o15014" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o15011" id="r1048" name="on" negation="false" src="d0_s2" to="o15012" />
    </statement>
    <statement id="d0_s3">
      <text>petiole convex to flat, or shallowly grooved adaxially, 3.5–12 (–15) mm, villous to puberulent adaxially;</text>
      <biological_entity id="o15015" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15016" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s3" to="flat" />
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="villous" modifier="adaxially" name="pubescence" src="d0_s3" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade (sometimes hemiamphistomatous), broadly obovate, elliptic, broadly elliptic, or subcircular, 24–52 (–100) × 12–45 mm, base concave, rounded, subcordate, cordate, or cuneate, margins sometimes slightly revolute, serrulate, or crenulate to subentire, apex abruptly acuminate or acute, abaxial surface glaucous (tip often not glaucous), sparsely to moderately densely puberulent, or silky to glabrescent, hairs appressed or spreading, straight or wavy, adaxial slightly glossy, glabrescent or sparsely to moderately densely puberulent, pubescent, or short-silky (especially midrib);</text>
      <biological_entity id="o15017" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcircular" value_original="subcircular" />
        <character char_type="range_value" from="52" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="24" from_unit="mm" name="length" src="d0_s4" to="52" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o15018" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o15019" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o15020" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="crenulate" name="shape" src="d0_s4" to="subentire" />
        <character char_type="range_value" from="crenulate" name="shape" src="d0_s4" to="subentire" />
      </biological_entity>
      <biological_entity id="o15021" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15022" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent or silky" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent or silky" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent or silky" />
      </biological_entity>
      <biological_entity id="o15023" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15024" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character char_type="range_value" from="glabrescent or" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent pubescent or short-silky" />
        <character char_type="range_value" from="glabrescent or" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent pubescent or short-silky" />
        <character char_type="range_value" from="glabrescent or" name="pubescence" src="d0_s4" to="sparsely moderately densely puberulent pubescent or short-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal blade margins serrulate, crenulate, or entire;</text>
      <biological_entity id="o15025" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="blade" id="o15026" name="margin" name_original="margins" src="d0_s5" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>juvenile blade sometimes reddish, long-silky, villous, tomentose (at least on midrib), or glabrous abaxially, hairs usually white, rarely somewhat ferruginous.</text>
      <biological_entity id="o15027" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15028" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15029" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely somewhat" name="coloration" src="d0_s6" value="ferruginous" value_original="ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o15031" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>staminate (densely flowered), stout or subglobose, 17–35 mm, flowering branchlet 1–3 mm;</text>
      <biological_entity id="o15030" name="catkin" name_original="catkins" src="d0_s7" type="structure">
        <character constraint="as leaves" constraintid="o15031" is_modifier="false" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate densely flowered, stout, 9–11 (–30) (–80 in fruit) mm, flowering branchlet 2–5.5 mm;</text>
      <biological_entity id="o15032" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15033" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s9" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>floral bract pale-brown, 1–1.9 (–2.8) mm, apex acute, convex, or rounded, abaxially sparsely hairy, hairs straight or wavy.</text>
      <biological_entity constraint="floral" id="o15034" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-brown" value_original="pale-brown" />
        <character char_type="range_value" from="1.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15035" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15036" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: adaxial nectary 0.5–0.7 mm;</text>
      <biological_entity id="o15037" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15038" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct;</text>
      <biological_entity id="o15039" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o15040" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers purple turning yellow, (ellipsoid or shortly cylindrical), 0.5–0.8 mm.</text>
      <biological_entity id="o15041" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o15042" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: adaxial nectary ovate, square, or flask-shaped, 0.4–0.6 (–1) mm;</text>
      <biological_entity id="o15043" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15044" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" name="shape" src="d0_s14" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary pyriform, pubescent throughout or in patches or streaks, or glabrous (hairs refractive), beak gradually tapering to styles;</text>
      <biological_entity id="o15045" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15046" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="in patches or streaks" value_original="in patches or streaks" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15047" name="patch" name_original="patches" src="d0_s15" type="structure" />
      <biological_entity id="o15048" name="streak" name_original="streaks" src="d0_s15" type="structure" />
      <biological_entity id="o15049" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character constraint="to styles" constraintid="o15050" is_modifier="false" modifier="gradually" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o15050" name="style" name_original="styles" src="d0_s15" type="structure" />
      <relation from="o15046" id="r1049" name="in" negation="false" src="d0_s15" to="o15047" />
      <relation from="o15046" id="r1050" name="in" negation="false" src="d0_s15" to="o15048" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 12–14 per ovary;</text>
      <biological_entity id="o15051" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15052" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o15053" from="12" name="quantity" src="d0_s16" to="14" />
      </biological_entity>
      <biological_entity id="o15053" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 0.6–1.5 mm.</text>
      <biological_entity id="o15054" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15055" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules 6–10 mm. 2n = 114.</text>
      <biological_entity id="o15056" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s18" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15057" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="114" value_original="114" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Mar-mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jun" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>75.</number>
  <other_name type="common_name">Dark-leaved willow</other_name>
  <discussion>Salix myrsinifolia may be naturalized in the vicinity of Ottawa, Ontario, but that needs confirmation.</discussion>
  
</bio:treatment>