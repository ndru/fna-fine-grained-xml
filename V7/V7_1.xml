<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>George W. Argus,James E. Eckenwalder,Robert W. Kiger</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">8</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="treatment_page">3</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">SALICACEAE</taxon_name>
    <taxon_hierarchy>family SALICACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10787</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, heterophyllous or not, sometimes clonal, forming clones by root shoots, rhizomes, layering, or stem fragmentation;</text>
      <biological_entity id="o7233" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="heterophyllous" value_original="heterophyllous" />
        <character name="architecture" src="d0_s0" value="not" value_original="not" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="heterophyllous" value_original="heterophyllous" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o7235" name="clone" name_original="clones" src="d0_s0" type="structure" />
      <biological_entity constraint="root" id="o7236" name="shoot" name_original="shoots" src="d0_s0" type="structure" />
      <biological_entity id="o7237" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="layering" value_original="layering" />
      </biological_entity>
      <relation from="o7233" id="r522" name="forming" negation="false" src="d0_s0" to="o7235" />
      <relation from="o7233" id="r523" name="forming" negation="false" src="d0_s0" to="o7235" />
      <relation from="o7233" id="r524" name="by" negation="false" src="d0_s0" to="o7236" />
      <relation from="o7233" id="r525" name="by" negation="false" src="d0_s0" to="o7236" />
    </statement>
    <statement id="d0_s1">
      <text>glabrous or glabrescent to pubescent;</text>
    </statement>
    <statement id="d0_s2">
      <text>branching monopodial or sympodial.</text>
      <biological_entity id="o7238" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s1" to="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monopodial" value_original="monopodial" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sympodial" value_original="sympodial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect to pendent;</text>
    </statement>
    <statement id="d0_s4">
      <text>branched.</text>
      <biological_entity id="o7239" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent, deciduous or marcescent, alternate (opposite or subopposite in Salix purpurea), spirally arranged, simple;</text>
      <biological_entity id="o7240" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules present or not;</text>
      <biological_entity id="o7241" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole present;</text>
      <biological_entity id="o7242" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade margins toothed or entire, sometimes glandular.</text>
      <biological_entity constraint="blade" id="o7243" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles present or absent.</text>
      <biological_entity id="o7244" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences racemose or spicate, usually catkins, unbranched, sometimes fasciculate or racemelike-cymes, flowering before or as leaves emerge or year-round;</text>
      <biological_entity id="o7245" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="spicate" value_original="spicate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement" src="d0_s10" value="fasciculate" value_original="fasciculate" />
        <character constraint="before leaves" constraintid="o7248" is_modifier="false" name="life_cycle" notes="" src="d0_s10" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="year-round" value_original="year-round" />
      </biological_entity>
      <biological_entity id="o7246" name="catkin" name_original="catkins" src="d0_s10" type="structure" />
      <biological_entity id="o7247" name="racemelike-cyme" name_original="racemelike-cymes" src="d0_s10" type="structure" />
      <biological_entity id="o7248" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>floral bract (1) subtending each flower, displaced onto pedicel or distinct, scalelike, apex entire, toothed, or laciniate;</text>
      <biological_entity constraint="floral" id="o7249" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character name="atypical_quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s11" value="subtending" value_original="subtending" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o7250" name="flower" name_original="flower" src="d0_s11" type="structure" />
      <biological_entity id="o7251" name="pedicel" name_original="pedicel" src="d0_s11" type="structure" />
      <biological_entity id="o7252" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="laciniate" value_original="laciniate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o7249" id="r526" name="displaced onto" negation="false" src="d0_s11" to="o7251" />
    </statement>
    <statement id="d0_s12">
      <text>bract subtending pistillate flower deciduous or persistent.</text>
      <biological_entity id="o7253" name="bract" name_original="bract" src="d0_s12" type="structure" />
      <biological_entity constraint="subtending" id="o7254" name="flower" name_original="flower" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicels present or absent.</text>
      <biological_entity id="o7255" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers usually unisexual, sometimes bisexual, usually staminate and pistillate on different plants;</text>
      <biological_entity id="o7256" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s14" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o7257" is_modifier="false" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7257" name="plant" name_original="plants" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>sepals present or absent, or perianth modified into 1 or 2 nectaries, or a non-nectariferous disc;</text>
      <biological_entity id="o7258" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7259" name="perianth" name_original="perianth" src="d0_s15" type="structure">
        <character constraint="into non-nectariferous disc" constraintid="o7260" is_modifier="false" name="development" src="d0_s15" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity constraint="non-nectariferous" id="o7260" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 1–60 (–70);</text>
      <biological_entity id="o7261" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="70" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments distinct or connate basally, slender;</text>
      <biological_entity id="o7262" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character is_modifier="false" name="size" src="d0_s17" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers longitudinally dehiscent;</text>
      <biological_entity id="o7263" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" src="d0_s18" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary 1, 2–7 [–10] -carpellate, 1–7 [–10] -locular;</text>
    </statement>
    <statement id="d0_s20">
      <text>placentation usually parietal, sometimes axile on intruded, fused placentae;</text>
      <biological_entity id="o7264" name="ovary" name_original="ovary" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="2-7[-10]-carpellate" value_original="2-7[-10]-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s19" value="1-7[-10]-locular" value_original="1-7[-10]-locular" />
        <character is_modifier="false" modifier="usually" name="placentation" src="d0_s20" value="parietal" value_original="parietal" />
        <character constraint="on placentae" constraintid="o7265" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s20" value="axile" value_original="axile" />
      </biological_entity>
      <biological_entity id="o7265" name="placenta" name_original="placentae" src="d0_s20" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s20" value="intruded" value_original="intruded" />
        <character is_modifier="true" name="fusion" src="d0_s20" value="fused" value_original="fused" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 1–25 per ovary;</text>
      <biological_entity id="o7266" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o7267" from="1" name="quantity" src="d0_s21" to="25" />
      </biological_entity>
      <biological_entity id="o7267" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style 1 per carpel, distinct or connate;</text>
      <biological_entity id="o7268" name="style" name_original="style" src="d0_s22" type="structure">
        <character constraint="per carpel" constraintid="o7269" name="quantity" src="d0_s22" value="1" value_original="1" />
        <character is_modifier="false" name="fusion" src="d0_s22" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s22" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o7269" name="carpel" name_original="carpel" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>stigmas 2–4, truncate, notched-capitate, or 2-lobed or 3-lobed.</text>
      <biological_entity id="o7270" name="stigma" name_original="stigmas" src="d0_s23" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s23" to="4" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="notched-capitate" value_original="notched-capitate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="shape" src="d0_s23" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s23" value="notched-capitate" value_original="notched-capitate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" name="shape" src="d0_s23" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Fruits capsular, baccate, or drupaceous.</text>
      <biological_entity id="o7271" name="fruit" name_original="fruits" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s24" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="baccate" value_original="baccate" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="drupaceous" value_original="drupaceous" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="baccate" value_original="baccate" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="drupaceous" value_original="drupaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Seeds sometimes surrounded by arillate coma of relatively long, silky hairs;</text>
      <biological_entity id="o7272" name="seed" name_original="seeds" src="d0_s25" type="structure" />
      <biological_entity id="o7273" name="coma" name_original="coma" src="d0_s25" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s25" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o7274" name="hair" name_original="hairs" src="d0_s25" type="structure">
        <character is_modifier="true" modifier="relatively" name="length_or_size" src="d0_s25" value="long" value_original="long" />
        <character is_modifier="true" name="pubescence" src="d0_s25" value="silky" value_original="silky" />
        <character is_modifier="true" name="architecture" src="d0_s25" value="arillate" value_original="arillate" />
      </biological_entity>
      <relation from="o7272" id="r527" modifier="sometimes" name="surrounded by" negation="false" src="d0_s25" to="o7273" />
      <relation from="o7272" id="r528" modifier="sometimes" name="surrounded by" negation="false" src="d0_s25" to="o7274" />
    </statement>
    <statement id="d0_s26">
      <text>endosperm scant or absent.</text>
      <biological_entity id="o7275" name="endosperm" name_original="endosperm" src="d0_s26" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s26" value="scant" value_original="scant" />
        <character is_modifier="false" name="presence" src="d0_s26" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>0</number>
  <other_name type="common_name">Willow Family</other_name>
  <discussion>Genera 50+, species ca. 1000 (4 genera, 123 species in the flora).</discussion>
  <discussion>Taxonomic placement of the Salicaceae and the genera included in it have varied greatly. Some botanists (H. G. A. Engler and K. Prantl 1887–1915) treated it as a primitive member of the Dicotyledoneae and grouped it with other families having simple, apetalous, unisexual flowers arranged in catkins, the “Amentiferae.” At about the same time, others (C. E. Bessey 1915) took a different view, regarding the simple flowers as the result of reduction, and placed the taxa in Caryophyllales. As early as 1905, H. Hallier could see that there were similarities between Salicaceae and Flacourtiaceae; at the time, he was vigorously challenged by E. Gilg (1915). A. D. J. Meeuse (1975) summarized evidence for a close relationship between these families, including wood anatomy, phytochemistry, host-parasite relationships (including rust fungi), and morphology. He concluded that the Salicaceae could be combined with the Flacourtiaceae, “perhaps as a tribe.” A. Cronquist (1988) and R. F. Thorne (1992b) placed the Salicaceae, in a narrow sense, in Violales near Flacourtiaceae.</discussion>
  <discussion>Molecular studies support a close relationship between Salicaceae and Flacourtiaceae in Malpighiales and show that Flacourtiaceae, in a broad sense, is paraphyletic. Based on a study of plastid rbcL DNA sequences, Salix and Populus were nested within a subset of 52 genera of Flacourtiaceae (M. W. Chase et al. 2002). Chase et al. proposed moving some genera from broadly circumscribed Flacourtiaceae to Salicaceae. Other studies, based on different gene sequences, came to the same conclusion (O. I. Nandi et al. 1998; V. Savolainen et al. 2000; K. W. Hilu et al. 2003; Angiosperm Phylogeny Group 2003). The discovery of the extinct fossil genus Pseudosalix (L. D. Boucher et al. 2003), from the Eocene Green River Formation of Utah, provided further support for placing some members of Flacourtiaceae in Salicaceae. The well-preserved Pseudosalix fossils, in which reproductive structures are directly associated with the leaves, occur intermixed with Populus fossils. The leaves are slender and have salicoid teeth, inflorescences are cymose, flowers are unisexual, pedicellate, tetrasepalous, and 3- or 4-carpellate, and seeds are comose, i.e., having characteristics intermediate between Salicaceae and Flacourtiaceae.</discussion>
  <discussion>The presence, in both families, of salicoid teeth is often cited in support of their close relationship (W. S. Judd 1997b; O. Nandi et al. 1998; M. W. Chase et al. 2002; H. P. Wilkinson 2007). Salicoid teeth were first recognized and defined as having the tip of the medial vein (seta) of the tooth retained as a dark, but not opaque, non-deciduous spherical callosity fused to the tooth apex and were reported to occur in Salicaceae and Idesia of the Flacourtiaceae (L. J. Hickey and J. A. Wolfe 1975). Nandi et al. reported that a broad survey of angiosperm leaves showed that salicoid teeth occur outside of Flacourtiaceae and Salicaceae only in Tetracentraceae.</discussion>
  <discussion>Isozyme and cytological evidence show that Populus and Salix are ancient polyploids (D. E. Soltis and P. S. Soltis 1990; Wang R. and Wang J. 1991). All Salix and Populus species contain salicin (R. T. Palo 1984).</discussion>
  <discussion>The genera often included in Salicaceae, in the narrow sense, are Chosenia, Populus, Salix (A. K. Skvortsov 1999), and, sometimes, Toisusu. Molecular studies (E. Leskinen and C. Alström-Rapaport 1999; T. Azuma et al. 2000) show that Chosenia is nested within Salix. H. Ohashi (2001) treated Toisusu as Salix subg. Pleuradinea Kimura and Chosenia as Salix subg. Chosenia (Nakai) H. Ohashi.</discussion>
  <references>
    <reference>Fisher, M. J. 1928. The morphology and anatomy of the flowers of the Salicaceae. Amer. J. Bot. 15: 307–326, 372–395.</reference>
    <reference>Floderus, B. G. O. 1923. Om Grönlands salices. (On the Salicaceae of Greenland.) Meddel. Grønland 63: 61–204.</reference>
    <reference>Judd, W. S. 1997b. The Flacourtiaceae in the southeastern United States. Harvard Pap. Bot. 10: 65–79.</reference>
    <reference>Leskinen, E. and C. Alström-Rapaport. 1999. Molecular phylogeny of Salicaceae and closely related Flacourtiaceae: Evidence from 5.8 S, ITS 1 and ITS 2 of the rDNA. Pl. Syst. Evol. 215: 209–227.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers in catkins; sepals absent; fruits capsules</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers not in catkins; sepals present; fruits drupes or berries</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Buds 3-10-scaled (usually resinous); leaf blades usually less than 2 times as long as wide, venation ± palmate (basal secondary veins strong, paired, except in Populus angustifolia); stipules caducous; catkins pendulous, sessile; floral bracts: apex deeply or shallowly cut, pistillate floral bracts deciduous after flowering; flowers without nectaries (with a non-glandular, cup- or saucer-like disc); stamens 6-60(-70); stigmas 2-4; capsules 2-4-valved, narrowly ovoid to spherical.</description>
      <determination>1 Populus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Buds 1-scaled (oily in Salix barrattiana); leaf blades often more than 2 times as long as wide, venation usually pinnate; stipules persistent or absent; catkins erect, spreading, or ± pendulous, sessile or terminating flowering branchlets; floral bracts: apex entire, erose, 2-fid, or irregularly toothed, pistillate floral bracts persistent or deciduous after flowering; flowers: perianth reduced to adaxial nectary (rarely also with abaxial nectary, then distinct or connate into shallow cup); stamens 1, 2, or 3-10; stigmas 2; capsules 2-valved, obclavate to ovoid or ellipsoid.</description>
      <determination>2 Salix</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in racemelike cymes or solitary; fruits drupes, 18-25 mm</description>
      <determination>3 Flacourtia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in fascicles; fruits berries, 4-7 mm.</description>
      <determination>4 Xylosma</determination>
    </key_statement>
  </key>
</bio:treatment>