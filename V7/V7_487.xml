<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="1941" rank="species">zionensis</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Drabas W. N. Amer.,</publication_title>
      <place_in_publication>49, plate 2, fig. 16. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species zionensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094767</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">asprella</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) S. L. Welsh &amp; Reveal" date="unknown" rank="variety">zionensis</taxon_name>
    <taxon_hierarchy>genus Draba;species asprella;variety zionensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose);</text>
      <biological_entity id="o36540" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with persistent leaf-bases, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o36541" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.5–1.5 (–1.8) dm, usually sparsely pubescent proximally, usually glabrous distally, rarely throughout, trichomes simple, 0.3–0.8 mm, often with smaller, 2–4-rayed ones.</text>
      <biological_entity id="o36542" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="usually sparsely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="usually; distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o36543" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely throughout; throughout" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36544" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o36543" id="r2464" modifier="often" name="with" negation="false" src="d0_s4" to="o36544" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o36545" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole base sometimes ciliate (margin not ciliate, trichomes simple and 2-rayed, 0.2–0.5 mm);</text>
      <biological_entity constraint="petiole" id="o36546" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade spatulate to obovate, 0.7–3.5 cm × 3–10 (–12) mm, margins usually entire, rarely obscurely dentate (near apex), surfaces sparsely pubescent with stalked, (2–) 4 (or 5) -rayed trichomes, 0.05–0.5 mm, (midvein obscure).</text>
      <biological_entity id="o36547" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s8" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36548" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely obscurely" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o36549" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o36550" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o36550" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o36551" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes 14–36-flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o36552" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="14-36-flowered" value_original="14-36-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o36553" is_modifier="false" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o36553" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels ascending to divaricate-ascending, straight, (not expanded basally), (4–) 5–12 (–15) mm, glabrous.</text>
      <biological_entity id="o36554" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s12" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o36555" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <relation from="o36554" id="r2465" name="fruiting" negation="false" src="d0_s12" to="o36555" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals ovate, 2–3 mm, glabrous or sparsely pubescent, (trichomes simple and short-stalked, 2–4-rayed);</text>
      <biological_entity id="o36556" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o36557" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals orange-yellow (fading pale-yellow), broadly spatulate to subovate, 5–6 × 1.8–2.8 mm;</text>
      <biological_entity id="o36558" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o36559" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="orange-yellow" value_original="orange-yellow" />
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s14" to="subovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, 0.4–0.5 mm.</text>
      <biological_entity id="o36560" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o36561" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits lanceolate to linear-lanceolate or ovate, plane (not curved), flattened, 6–12 (–17) × 2–3 mm;</text>
      <biological_entity id="o36562" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s16" to="linear-lanceolate or ovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="17" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s16" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves glabrous;</text>
      <biological_entity id="o36563" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 12–20 per ovary;</text>
      <biological_entity id="o36564" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o36565" from="12" name="quantity" src="d0_s18" to="20" />
      </biological_entity>
      <biological_entity id="o36565" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.4–1 mm.</text>
      <biological_entity id="o36566" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds oblong, 1–1.3 × 0.6–0.8 mm. 2n = 26.</text>
      <biological_entity id="o36567" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s20" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s20" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o36568" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone (rarely limestone) rock outcrops and sandy slopes in pinyon-juniper or pine communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone ( limestone ) rock outcrops" modifier="rarely" />
        <character name="habitat" value="sandy slopes" constraint="in pinyon-juniper or pine communities" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="pine communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>121.</number>
  <discussion>R. C. Rollins (1993) treated Draba zionensis as a variety of D. asprella, but its true relationships appear to lie with two other southern Utah endemics, D. sobolifera and D. subalpina. Draba zionensis is easily distinguished from D. subalpina by having orange-yellow (versus white) petals, and from D. asprella and D. sobolifera by its glabrous (versus pubescent) pedicels and stems distally. Nearly all populations of the species are found in and around Zion National Park in southwestern Utah (Iron, Kane, and Washington counties). A specimen supposedly from the Deep Creek Mountains (Juab County) may be mislabeled.</discussion>
  
</bio:treatment>