<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="treatment_page">472</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="(Michaux) Alph. Wood" date="1870" rank="species">diphylla</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Bot. Fl.,</publication_title>
      <place_in_publication>37. 1870</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species diphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416229</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dentaria</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">diphylla</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 30. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dentaria;species diphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dentaria</taxon_name>
    <taxon_name authority="Stokes" date="unknown" rank="species">bifolia</taxon_name>
    <taxon_hierarchy>genus Dentaria;species bifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dentaria</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">incisa</taxon_name>
    <taxon_hierarchy>genus Dentaria;species incisa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually glabrous, rarely sparsely pubescent.</text>
      <biological_entity id="o17343" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes (unsegmented), cylindrical, 2–10 mm diam., (somewhat uniform, fleshy, not fragile, with dentate leaf-scars).</text>
      <biological_entity id="o17344" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched, (1.2–) 1.5–3.5 (–4) dm, rarely sparsely pubescent distally.</text>
      <biological_entity id="o17345" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="3.5" to_unit="dm" />
        <character is_modifier="false" modifier="rarely sparsely; distally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Rhizomal leaves 3-foliolate, (5.5–) 8–22 (–26) cm, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="rhizomal" id="o17346" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="26" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s4" to="22" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17347" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (3–) 4.5–16 (–20) cm;</text>
      <biological_entity id="o17348" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="4.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s5" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets subsessile or petiolulate, blade often similar to terminal, base sometimes oblique;</text>
      <biological_entity constraint="lateral" id="o17349" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o17350" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17351" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="orientation_or_shape" src="d0_s6" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflet (petiolule 0.5–1.2 cm), blade ovate-elliptic to broadly ovate, (2–) 3.5–8 (–10) cm × (5–) 20–65 (–80) mm, base cuneate to obtuse, margins coarsely crenate or dentate, (surfaces puberulent, trichomes to 0.1 mm).</text>
      <biological_entity constraint="terminal" id="o17352" name="leaflet" name_original="leaflet" src="d0_s7" type="structure" />
      <biological_entity id="o17353" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate-elliptic" name="shape" src="d0_s7" to="broadly ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s7" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s7" to="8" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s7" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s7" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17354" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
      <biological_entity id="o17355" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 2 (or 3) [(sub) opposite], 3-foliolate, (similar to rhizomal leaves), petiolate, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="cauline" id="o17356" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o17357" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole (1–) 2–4.5 cm, base not auriculate;</text>
      <biological_entity id="o17358" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17359" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets similar to terminal;</text>
      <biological_entity constraint="lateral" id="o17360" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>terminal leaflet subsessile or petiolulate (0.2–1 cm), blade broadly elliptic to ovate, (2–) 4–8 (–10) cm × 10–50 mm, margins coarsely dentate or crenate, (margins minutely puberulent).</text>
      <biological_entity constraint="terminal" id="o17361" name="leaflet" name_original="leaflet" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o17362" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s11" to="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s11" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s11" to="8" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s11" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17363" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s11" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels ascending to divaricate, (10–) 15–30 (–36) mm.</text>
      <biological_entity id="o17364" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="36" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17365" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o17364" id="r1184" name="fruiting" negation="false" src="d0_s13" to="o17365" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, (4–) 5–8 × 2–3 mm, lateral pair slightly saccate basally;</text>
      <biological_entity id="o17366" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o17367" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly; basally" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white or pink to purple, obovate to oblanceolate, (7–) 9–15 (–17) × (3–) 4–7 mm, (short-clawed, apex rounded);</text>
      <biological_entity id="o17368" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o17369" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s15" to="purple" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s15" to="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s15" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s15" to="17" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s15" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s15" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments: median pairs 5–8 mm, lateral pair 3.5–6 mm;</text>
      <biological_entity id="o17370" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers linear to oblong, 2.5–3 mm.</text>
      <biological_entity id="o17371" name="filament" name_original="filaments" src="d0_s17" type="structure" />
      <biological_entity id="o17372" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s17" to="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits linear-lanceolate, 1.5–4 cm × 1.5–2.5 mm;</text>
      <biological_entity id="o17373" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s18" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 10–14 per ovary;</text>
      <biological_entity id="o17374" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17375" from="10" name="quantity" src="d0_s19" to="14" />
      </biological_entity>
      <biological_entity id="o17375" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 4–8 (–10) mm.</text>
      <biological_entity id="o17376" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s20" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s20" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds (rarely produced) brown, oblong, 2–2.2 × 1.2–1.5 mm (cotyledons incumbent).</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 96.</text>
      <biological_entity id="o17377" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s21" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17378" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded bottoms and ravines, cliffs, bluffs, ledges, shaded slopes, meadows, moist fields, alluvial banks, rich woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded bottoms" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="shaded slopes" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="moist fields" />
        <character name="habitat" value="alluvial banks" />
        <character name="habitat" value="rich woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ark., Conn., Ga., Ill., Ind., Ky., Maine, Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  
</bio:treatment>