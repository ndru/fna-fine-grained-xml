<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">671</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">Smelowskieae</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Syst. Evol.</publication_title>
      <place_in_publication>259: 111. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe Smelowskieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20868</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [annuals];</text>
    </statement>
    <statement id="d0_s1">
      <text>eglandular.</text>
      <biological_entity id="o5371" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes stalked, usually dendritic, stellate, or forked, sometimes mostly simple.</text>
      <biological_entity id="o5372" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="stalked" value_original="stalked" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" name="shape" src="d0_s2" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="sometimes mostly" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves [rarely absent], petiolate, subsessile, or sessile;</text>
      <biological_entity constraint="cauline" id="o5373" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade base not auriculate, margins usually pinnately or palmately lobed, sometimes entire [dentate].</text>
      <biological_entity constraint="blade" id="o5374" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o5375" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes usually ebracteate, often elongated in fruit.</text>
      <biological_entity id="o5376" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o5377" is_modifier="false" modifier="often" name="length" src="d0_s5" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o5377" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers actinomorphic;</text>
      <biological_entity id="o5378" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals suberect to spreading [erect], lateral pair not saccate basally;</text>
      <biological_entity id="o5379" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="suberect" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s7" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, pink, or purple [yellow], claw present, distinct;</text>
      <biological_entity id="o5380" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o5381" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments unappendaged, not winged;</text>
      <biological_entity id="o5382" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="unappendaged" value_original="unappendaged" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollen 3-colpate.</text>
      <biological_entity id="o5383" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-colpate" value_original="3-colpate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits silicles or siliques, dehiscent, unsegmented, 4-angled, angustiseptate, terete, or subterete [latiseptate];</text>
      <biological_entity id="o5384" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angustiseptate" value_original="angustiseptate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
      </biological_entity>
      <biological_entity id="o5385" name="silicle" name_original="silicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angustiseptate" value_original="angustiseptate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
      </biological_entity>
      <biological_entity id="o5386" name="silique" name_original="siliques" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angustiseptate" value_original="angustiseptate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 4–30 [–numerous] per ovary;</text>
      <biological_entity id="o5387" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o5388" from="4" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
      <biological_entity id="o5388" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style distinct [obsolete];</text>
      <biological_entity id="o5389" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma entire [rarely slightly 2-lobed].</text>
      <biological_entity id="o5390" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds uniseriate [biseriate];</text>
      <biological_entity id="o5391" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s15" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>cotyledons accumbent or incumbent.</text>
      <biological_entity id="o5392" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="accumbent" value_original="accumbent" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>cc.</number>
  <discussion>Genus 1, species 25 (7 in the flora).</discussion>
  
</bio:treatment>