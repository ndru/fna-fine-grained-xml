<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">728</other_info_on_meta>
    <other_info_on_meta type="treatment_page">735</other_info_on_meta>
    <other_info_on_meta type="illustration_page">730</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="A. Nelson" date="1911" rank="species">milleflorum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>52: 263. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species milleflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094961</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="(Hooker) Endlicher" date="unknown" rank="species">laciniatum</taxon_name>
    <taxon_name authority="(A. Nelson) Payson" date="unknown" rank="variety">milleflorum</taxon_name>
    <taxon_hierarchy>genus Thelypodium;species laciniatum;variety milleflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>somewhat glaucous, glabrous (except petioles).</text>
      <biological_entity id="o23181" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (hollow), branched distally, (1.8–) 4.5–13 (–21) dm.</text>
      <biological_entity id="o23182" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.8" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="21" to_unit="dm" />
        <character char_type="range_value" from="4.5" from_unit="dm" name="some_measurement" src="d0_s2" to="13" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline): petiole (1–) 1.8–9.5 (–13) cm, ciliate;</text>
      <biological_entity constraint="basal" id="o23183" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23184" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s3" to="9.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade often oblong, sometimes ovate or lanceolate, (3.8–) 6–23 (–28) cm × (9–) 20–65 (–100) mm, margins usually dentate, sometimes sinuate, rarely repand.</text>
      <biological_entity constraint="basal" id="o23185" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23186" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="atypical_length" src="d0_s4" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="28" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="23" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="65" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23187" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves petiolate;</text>
      <biological_entity constraint="cauline" id="o23188" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade lanceolate, similar to basal, smaller distally, margins dentate or entire.</text>
      <biological_entity id="o23189" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="position" src="d0_s6" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o23190" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes dense, slightly elongated in fruit (to 10 dm, flower buds narrowly oblong).</text>
      <biological_entity id="o23192" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels erect distally, strongly curved upward, stout, (1.5–) 2.5–5 (–7) mm, slightly flattened at base.</text>
      <biological_entity id="o23191" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character constraint="in fruit" constraintid="o23192" is_modifier="false" modifier="slightly" name="length" src="d0_s7" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o23193" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o23194" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o23191" id="r1595" name="fruiting" negation="false" src="d0_s8" to="o23193" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals erect, oblong to linear-oblong, (4–) 4.5–8 (–9) × 1–2 mm;</text>
      <biological_entity id="o23195" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23196" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="linear-oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s9" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, spatulate to oblanceolate, (7–) 9–15 (–16) × 1–2 mm, claw strongly differentiated from blade [(3–) 3.5–5.5 (–6) mm, widest at base];</text>
      <biological_entity id="o23197" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23198" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s10" to="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s10" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23199" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character constraint="from blade" constraintid="o23200" is_modifier="false" modifier="strongly" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o23200" name="blade" name_original="blade" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectar glands confluent, median and lateral;</text>
      <biological_entity id="o23201" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="nectar" id="o23202" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="confluent" value_original="confluent" />
        <character is_modifier="false" name="position" src="d0_s11" value="median" value_original="median" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments subequal, (6–) 7–14.5 (–15.5) mm;</text>
      <biological_entity id="o23203" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23204" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="15.5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="14.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers exserted, linear to narrowly oblong, 2.5–4.5 (–6) mm, circinately coiled, (apiculate);</text>
      <biological_entity id="o23205" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23206" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s13" to="narrowly oblong" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="circinately" name="architecture" src="d0_s13" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>gynophore (0.5–) 1–4 (–6) mm.</text>
      <biological_entity id="o23207" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o23208" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits erect, (subappressed to rachis), torulose, somewhat tortuous, terete, (1.8–) 3.3–8 (–10) cm × (0.7–) 1–1.5 (–1.8) mm;</text>
      <biological_entity id="o23209" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s15" value="tortuous" value_original="tortuous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="atypical_length" src="d0_s15" to="3.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s15" to="10" to_unit="cm" />
        <character char_type="range_value" from="3.3" from_unit="cm" name="length" src="d0_s15" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_width" src="d0_s15" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 50–78 per ovary;</text>
      <biological_entity id="o23210" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o23211" from="50" name="quantity" src="d0_s16" to="78" />
      </biological_entity>
      <biological_entity id="o23211" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style usually cylindrical, rarely subclavate, (0.5–) 0.7–1.5 (–3) mm.</text>
      <biological_entity id="o23212" name="style" name_original="style" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s17" value="subclavate" value_original="subclavate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds (0.7–) 1–1.5 (–2) × 0.5–0.7 (–1) mm.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 26.</text>
      <biological_entity id="o23213" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_length" src="d0_s18" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s18" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s18" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s18" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23214" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand dunes, sagebrush and desert shrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="desert shrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Nev., Oreg., Utah, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  
</bio:treatment>