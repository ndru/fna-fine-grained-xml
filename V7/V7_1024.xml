<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">617</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="treatment_page">628</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="G. A. Mulligan" date="1966" rank="species">bellii</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>44: 1662, fig. 1, plate 1, fig. 3. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species bellii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095094</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o3305" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple, (relatively large);</text>
    </statement>
    <statement id="d0_s2">
      <text>densely (silvery) pubescent, trichomes (sessile, appressed), rays furcate, fused at base.</text>
      <biological_entity id="o3306" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o3307" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o3308" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character constraint="at base" constraintid="o3309" is_modifier="false" name="fusion" src="d0_s2" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o3309" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base, decumbent to nearly prostrate, 0.5–1.3 dm.</text>
      <biological_entity id="o3310" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o3311" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="decumbent" name="growth_form_or_orientation" notes="" src="d0_s3" to="nearly prostrate" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="1.3" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o3311" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (strongly rosulate; shortly petiolate);</text>
      <biological_entity constraint="basal" id="o3312" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade broadly obovate, 1.5–7.5 (width 7.5–26 mm, base gradually tapering to petiole), margins shallowly dentate, (apex obtuse).</text>
      <biological_entity id="o3313" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s5" to="7.5" />
      </biological_entity>
      <biological_entity id="o3314" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: blade oblanceolate to broadly obovate, 1–2.5 cm, margins entire.</text>
      <biological_entity constraint="cauline" id="o3315" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o3316" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="broadly obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3317" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes dense.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (divaricate-ascending to widely spreading, slightly sigmoid to curved), 7–12 mm.</text>
      <biological_entity id="o3318" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3319" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o3318" id="r255" name="fruiting" negation="false" src="d0_s8" to="o3319" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (pale-yellow or yellow-green), narrowly lanceolate to narrowly deltate, 4–8 mm;</text>
      <biological_entity id="o3320" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" notes="" src="d0_s9" to="narrowly deltate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3321" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, broadly spatulate to obovate, 9–13 mm, (not clawed).</text>
      <biological_entity id="o3322" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3323" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits didymous, slightly flattened (contrary to replum) to uncompressed, 4–9 × 2–8 mm, (strongly coriaceous, apical and basal sinuses narrow, deep);</text>
      <biological_entity id="o3324" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="didymous" value_original="didymous" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves (retaining seeds after dehiscence), pubescent, trichomes appressed;</text>
      <biological_entity id="o3325" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o3326" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum narrowly oblanceolate to narrowly linear-oblong, as wide as or wider than fruit, apex obtuse;</text>
      <biological_entity id="o3327" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s13" to="narrowly linear-oblong" />
      </biological_entity>
      <biological_entity id="o3329" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o3328" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o3330" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o3327" id="r256" name="as wide as" negation="false" src="d0_s13" to="o3329" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o3331" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character constraint="per ovary" constraintid="o3332" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o3332" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style more than 3 mm.</text>
      <biological_entity id="o3333" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds compressed.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 8.</text>
      <biological_entity id="o3334" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3335" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun(-Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dark shale, road cuts, ridge crests, washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dark shale" />
        <character name="habitat" value="road cuts" />
        <character name="habitat" value="ridge crests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Bell’s or Front Range twinpod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Physaria bellii is often found in shale and limestone soils of the Fountain/Ingleside, Lykins, Niobrara, and Pierre formations. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>