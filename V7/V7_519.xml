<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="treatment_page">376</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="Windham &amp; Al-Shehbaz" date="2006" rank="species">evadens</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 66. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species evadens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094560</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>(cespitose);</text>
    </statement>
    <statement id="d0_s3">
      <text>sexual;</text>
      <biological_entity id="o21252" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>caudex woody.</text>
      <biological_entity id="o21253" name="caudex" name_original="caudex" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 1–2.5 dm, densely pubescent proximally, trichomes short-stalked, 2–5-rayed, 0.2–0.6 mm, and sparse, simple ones to 1 mm, glabrous distally.</text>
      <biological_entity id="o21254" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character constraint="per caudex branch" constraintid="o21255" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="from center" constraintid="o21256" is_modifier="false" name="orientation" notes="" src="d0_s5" value="arising" value_original="arising" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s5" to="2.5" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o21255" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o21256" name="center" name_original="center" src="d0_s5" type="structure" />
      <biological_entity id="o21257" name="rosette" name_original="rosette" src="d0_s5" type="structure" />
      <biological_entity id="o21258" name="ground" name_original="ground" src="d0_s5" type="structure" />
      <biological_entity id="o21259" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o21260" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o21256" id="r1476" name="part_of" negation="false" src="d0_s5" to="o21257" />
      <relation from="o21256" id="r1477" name="near" negation="false" src="d0_s5" to="o21258" />
      <relation from="o21256" id="r1478" name="near" negation="false" src="d0_s5" to="o21259" />
    </statement>
    <statement id="d0_s6">
      <text>Basal leaves: blade narrowly oblanceolate, 1.5–3 mm wide, margins entire, ciliate along petiole, trichomes (simple), to 1 mm, surfaces densely pubescent, trichomes short-stalked, 4–8-rayed, 0.1–0.2 mm.</text>
      <biological_entity constraint="basal" id="o21261" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o21262" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21263" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character constraint="along petiole" constraintid="o21264" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21264" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o21265" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21266" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21267" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: 3–5, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o21268" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o21269" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <relation from="o21268" id="r1479" name="concealing" negation="true" src="d0_s7" to="o21269" />
    </statement>
    <statement id="d0_s8">
      <text>blade auricles 0.5–1 mm, surfaces of distalmost leaves pubescent.</text>
      <biological_entity constraint="cauline" id="o21270" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="blade" id="o21271" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21272" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o21273" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o21272" id="r1480" name="part_of" negation="false" src="d0_s8" to="o21273" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes 10–22-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate-ascending, straight, 4–8 mm, sparsely pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o21274" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-22-flowered" value_original="10-22-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o21275" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o21276" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21277" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o21274" id="r1481" name="fruiting" negation="false" src="d0_s10" to="o21275" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o21278" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals pubescent;</text>
      <biological_entity id="o21279" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals white, 3–4 × 0.5–0.8 mm, glabrous;</text>
      <biological_entity id="o21280" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o21281" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits divaricate-ascending, not appressed to rachis, not secund, straight, edges parallel, 3–4 cm × 1.2–1.5 mm;</text>
      <biological_entity id="o21282" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character constraint="to rachis" constraintid="o21283" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s15" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o21283" name="rachis" name_original="rachis" src="d0_s15" type="structure" />
      <biological_entity id="o21284" name="edge" name_original="edges" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s15" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves glabrous;</text>
      <biological_entity id="o21285" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 46–52 per ovary;</text>
      <biological_entity id="o21286" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o21287" from="46" name="quantity" src="d0_s17" to="52" />
      </biological_entity>
      <biological_entity id="o21287" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style ca. 0.1 mm.</text>
      <biological_entity id="o21288" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="some_measurement" src="d0_s18" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate, 1–1.1 × 0.9–1 mm;</text>
      <biological_entity id="o21289" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s19" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>wing continuous, ca. 0.1 mm wide.</text>
      <biological_entity id="o21290" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="continuous" value_original="continuous" />
        <character name="width" src="d0_s20" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <discussion>Boechera evadens is a sexual taxon that was originally interpreted as a disjunct population of Arabis (Boechera) fernaldiana, but is easily distinguished from that species by having much shorter (3–4 versus 7–12 mm) petals, larger (0.2–0.6 versus 0.04–0.1 mm) trichomes proximally on stems, and shorter (3–4 versus 4–7.5 cm) fruits (see M. D. Windham and I. A. Al-Shehbaz 2006 for additional distinctions). It is known only from the holotype specimen collected near Sherman Pass in Tulare County.</discussion>
  
</bio:treatment>