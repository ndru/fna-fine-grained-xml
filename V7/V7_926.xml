<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="treatment_page">576</other_info_on_meta>
    <other_info_on_meta type="illustration_page">569</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">austrinum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>468, 1331. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species austrinum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095028</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">austrinum</taxon_name>
    <taxon_name authority="Thellung" date="unknown" rank="variety">orbiculare</taxon_name>
    <taxon_hierarchy>genus Lepidium;species austrinum;variety orbiculare;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lasiocarpum</taxon_name>
    <taxon_name authority="(Thellung) C. L. Hitchcock" date="unknown" rank="variety">orbiculare</taxon_name>
    <taxon_hierarchy>genus Lepidium;species lasiocarpum;variety orbiculare;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>often densely hirsute, (trichomes cylindrical).</text>
      <biological_entity id="o30428" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often densely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="often densely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems often simple from base, erect, branched distally, (1.5–) 2–6.7 (–9.4) dm.</text>
      <biological_entity id="o30430" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o30431" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6.7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="9.4" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="6.7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o30431" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (later withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>rosulate;</text>
      <biological_entity constraint="basal" id="o30432" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (0.7–) 1.5–4.5 cm;</text>
      <biological_entity id="o30433" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade pinnatifid, 2–8.3 cm × 9–26 mm, margins (of lobes) entire or dentate.</text>
      <biological_entity id="o30434" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8.3" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30435" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o30436" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate to nearly linear, 1–4.5 (–6.2) cm × 3–10 (–17) mm, base attenuate to cuneate, not auriculate, margins entire or dentate.</text>
      <biological_entity id="o30437" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="nearly linear" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="6.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30438" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s8" to="cuneate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o30439" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes much-elongated in fruit;</text>
      <biological_entity id="o30440" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o30441" is_modifier="false" name="length" src="d0_s9" value="much-elongated" value_original="much-elongated" />
      </biological_entity>
      <biological_entity id="o30441" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>rachis pubescent, trichomes curved, with fewer and longer, straight ones.</text>
      <biological_entity id="o30442" name="rachis" name_original="rachis" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30444" name="one" name_original="ones" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character is_modifier="true" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o30443" id="r2045" name="with" negation="false" src="d0_s10" to="o30444" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels usually divaricate, rarely horizontal, straight or slightly recurved, (terete), (2.5–) 3–4.1 (–4.7) × 0.2 mm, puberulent adaxially.</text>
      <biological_entity id="o30443" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s11" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s11" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o30445" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o30443" id="r2046" name="fruiting" negation="false" src="d0_s11" to="o30445" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals oblong, 0.8–1 × 0.2–0.4 mm;</text>
      <biological_entity id="o30446" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30447" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s12" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals (sometimes absent), white, oblanceolate, 0.4–1.6 × 0.1–0.8 mm, claw absent;</text>
      <biological_entity id="o30448" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="length" src="d0_s13" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30449" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <biological_entity id="o30450" name="claw" name_original="claw" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 2, median;</text>
      <biological_entity id="o30451" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o30452" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments 0.8–1 mm;</text>
      <biological_entity id="o30453" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o30454" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.1–0.2 mm.</text>
      <biological_entity id="o30455" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o30456" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits elliptic-obovate to obovate-orbicular, 2.4–3.2 × 1.8–2.5 mm, apically winged, apical notch 0.2–0.5 mm deep;</text>
      <biological_entity id="o30457" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="elliptic-obovate" name="shape" src="d0_s17" to="obovate-orbicular" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s17" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s17" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o30458" name="notch" name_original="notch" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s17" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves thin, smooth, not veined, sparsely puberulent, (trichomes often antrorsely appressed, sometimes restricted to margin);</text>
      <biological_entity id="o30459" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="width" src="d0_s18" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>style 0.05–0.1 mm, included in apical notch.</text>
      <biological_entity id="o30460" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s19" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o30461" name="notch" name_original="notch" src="d0_s19" type="structure" />
      <relation from="o30460" id="r2047" name="included in" negation="false" src="d0_s19" to="o30461" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds ovate, 1.4–1.6 × 0.7–0.9 mm.</text>
      <biological_entity id="o30462" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s20" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s20" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed grounds, railroad tracks and embankments, fields, knolls, stream banks, waste areas, open banks, roadsides, sandy terraces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grounds" modifier="disturbed" />
        <character name="habitat" value="railroad tracks" />
        <character name="habitat" value="embankments" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="open banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy terraces" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., La., Miss., N.Mex., Okla., Tex.; Mexico (Coahuila, Nuevo León, San Luis Potosí).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>