<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">530</other_info_on_meta>
    <other_info_on_meta type="treatment_page">563</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="tribe">iberideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">IBERIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 648. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 292. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe iberideae;genus IBERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Name used by Dioscorides for an Iberian plant</other_info_on_name>
    <other_info_on_name type="fna_id">116309</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or subshrubs [biennials];</text>
      <biological_entity id="o33682" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or pubescent.</text>
      <biological_entity id="o33683" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o33684" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect or decumbent, often branched distally.</text>
      <biological_entity id="o33685" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline and sometimes basal;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o33686" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate or not, sessile [petiolate], blade (somewhat fleshy), margins entire [dentate to pinnatifid];</text>
      <biological_entity constraint="basal" id="o33687" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o33688" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o33689" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline petiolate or sessile, blade margins entire, dentate, or pinnatifid.</text>
      <biological_entity constraint="cauline" id="o33690" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o33691" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose), elongated or not in fruit.</text>
      <biological_entity id="o33693" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <relation from="o33692" id="r2266" name="in" negation="false" src="d0_s8" to="o33693" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels spreading, divaricate, descending, or ascending, slender.</text>
      <biological_entity id="o33692" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character name="length" src="d0_s8" value="not" value_original="not" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="descending" value_original="descending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="descending" value_original="descending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o33694" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o33692" id="r2267" name="fruiting" negation="false" src="d0_s9" to="o33694" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ascending [erect], ovate or oblong;</text>
      <biological_entity id="o33695" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33696" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals (zygomorphic, outer [abaxial] pair larger than inner [adaxial] pair), white or pink to purple, obovate [oblanceolate], claw often distinct;</text>
      <biological_entity id="o33697" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="pink" name="coloration" notes="" src="d0_s11" to="purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o33698" name="petal" name_original="petals" src="d0_s11" type="structure" />
      <biological_entity id="o33699" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o33700" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o33701" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o33702" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33703" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate or oblong, (apex obtuse);</text>
      <biological_entity id="o33704" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o33705" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands (4), lateral, 1 on each side of lateral stamen.</text>
      <biological_entity id="o33706" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o33707" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o33708" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o33708" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity constraint="lateral" id="o33709" name="stamen" name_original="stamen" src="d0_s15" type="structure" />
      <relation from="o33708" id="r2268" name="part_of" negation="false" src="d0_s15" to="o33709" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits sessile, (winged), suborbicular or ovate [obcordate], not torulose, keeled, strongly angustiseptate;</text>
      <biological_entity id="o33710" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s16" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves not veined, (winged), glabrous;</text>
      <biological_entity id="o33711" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o33712" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete;</text>
      <biological_entity id="o33713" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 2 per ovary;</text>
      <biological_entity id="o33714" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character constraint="per ovary" constraintid="o33715" name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o33715" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>stigma capitate, entire or 2-lobed.</text>
      <biological_entity id="o33716" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s21" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s21" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds flattened, often winged, ovate [orbicular to reniform];</text>
      <biological_entity id="o33717" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat mucilaginous or not when wetted;</text>
      <biological_entity id="o33718" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s23" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 7, 8, 9, 11.</text>
      <biological_entity id="o33719" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s24" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o33720" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="7" value_original="7" />
        <character name="quantity" src="d0_s25" value="8" value_original="8" />
        <character name="quantity" src="d0_s25" value="9" value_original="9" />
        <character name="quantity" src="d0_s25" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>63.</number>
  <other_name type="common_name">Candytuft</other_name>
  <discussion>Species 27 (3 in the flora).</discussion>
  <discussion>Iberis is a well-defined genus readily distinguished by having often corymbose infructescences, zygomorphic flowers, angustiseptate and often distally winged fruits, and exclusively accumbent cotyledons. N. H. Holmgren (2005b) erroneously indicated that the cotyledons in Iberis are incumbent.</discussion>
  <discussion>All three species treated here are ornamentals that sometimes escape from cultivation. Their distributions are based on verified records, but it is very likely that the species are more widely naturalized than the records show.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials or subshrubs (with sterile shoots).</description>
      <determination>2 Iberis sempervirens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals (without sterile shoots)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Racemes considerably elongated in fruit; abaxial petals 5-8 mm; styles 0.8-2 mm; cauline leaf blades: margins pinnatifid or dentate.</description>
      <determination>1 Iberis amara</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Racemes not elongated in fruit; abaxial petals 10-16 mm; styles 2-4.5 mm; cauline leaf blades: margins entire.</description>
      <determination>3 Iberis umbellata</determination>
    </key_statement>
  </key>
</bio:treatment>