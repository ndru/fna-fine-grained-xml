<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
    <other_info_on_meta type="illustration_page">169</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">caricaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carica</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">papaya</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1036. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caricaceae;genus carica;species papaya</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200014477</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ca. 6 m.</text>
      <biological_entity id="o42040" name="whole_organism" name_original="" src="" type="structure">
        <character name="some_measurement" src="d0_s0" unit="m" value="6" value_original="6" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to ca. 20 cm diam., bark green to gray or brown, leaf-scars prominent, smooth.</text>
      <biological_entity id="o42041" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o42042" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s1" to="gray or brown" />
      </biological_entity>
      <biological_entity id="o42043" name="leaf-scar" name_original="leaf-scars" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves drooping proximally, erect or spreading distally;</text>
      <biological_entity id="o42044" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s2" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 35–70 cm, hollow;</text>
      <biological_entity id="o42045" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s3" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade round in general contour, 20–60 cm diam., shallowly to deeply palmately 5–9-lobed, lobes lanceolate to ovate, margins entire or pinnately divided into lanceolate to ovate lobes.</text>
      <biological_entity id="o42046" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="contour" src="d0_s4" value="round" value_original="round" />
        <character char_type="range_value" from="20" from_unit="cm" name="diameter" src="d0_s4" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly to deeply palmately" name="shape" src="d0_s4" value="5-9-lobed" value_original="5-9-lobed" />
      </biological_entity>
      <biological_entity id="o42047" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
      <biological_entity id="o42048" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="into lobes" constraintid="o42049" is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o42049" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 15–60 cm;</text>
      <biological_entity id="o42050" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s5" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts ± rhombic.</text>
      <biological_entity id="o42051" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="rhombic" value_original="rhombic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers sessile;</text>
      <biological_entity id="o42052" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calyx green, short-tubular, ca. 1.5 mm, teeth triangular to linear;</text>
      <biological_entity id="o42053" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-tubular" value_original="short-tubular" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o42054" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla white, 2–3.5 cm, lobes narrowly oblong, ca. 1.3 cm;</text>
      <biological_entity id="o42055" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o42056" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s9" unit="cm" value="1.3" value_original="1.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens yellow.</text>
      <biological_entity id="o42057" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers subsessile;</text>
      <biological_entity id="o42058" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calyx yellow, short-tubular, 5–10 mm, teeth triangular to linear;</text>
      <biological_entity id="o42059" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s12" value="short-tubular" value_original="short-tubular" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42060" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s12" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals lanceolate-oblong, white to pale-yellow, 3–5 cm;</text>
      <biological_entity id="o42061" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate-oblong" value_original="lanceolate-oblong" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pale-yellow" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s13" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary ellipsoidal to rounded, 2–3 cm.</text>
      <biological_entity id="o42062" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s14" to="rounded" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s14" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Berries hanging from trunk near summit, green to orange, ellipsoidal to oblong, rounded distally, 8–45 cm.</text>
      <biological_entity id="o42063" name="berry" name_original="berries" src="d0_s15" type="structure">
        <character constraint="from trunk" constraintid="o42064" is_modifier="false" name="orientation" src="d0_s15" value="hanging" value_original="hanging" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s15" to="orange" />
        <character char_type="range_value" from="ellipsoidal" name="shape" src="d0_s15" to="oblong" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s15" to="45" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o42064" name="trunk" name_original="trunk" src="d0_s15" type="structure" />
      <biological_entity id="o42065" name="summit" name_original="summit" src="d0_s15" type="structure" />
      <relation from="o42064" id="r2830" name="near" negation="false" src="d0_s15" to="o42065" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds blackish.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 18.</text>
      <biological_entity id="o42066" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="blackish" value_original="blackish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o42067" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting year round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, waste places, hummocks, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="hummocks" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Fla.; Central America; nw South America; introduced also pantropically.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="nw South America" establishment_means="native" />
        <character name="distribution" value="also pantropically" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Papaya</other_name>
  <other_name type="common_name">papaw</other_name>
  <other_name type="common_name">pawpaw</other_name>
  <other_name type="common_name">lechosa</other_name>
  <discussion>Carica papaya is widely cultivated in tropical regions worldwide for its large melonlike fruit, one of the most popular tropical fruits, and occasionally as an ornamental. Papaya fruit is a good source of calcium, and vitamins A and C. Except for the ripe fruit, all parts of the plant possess a milky sap that contains the proteolytic enzymes papain and chymopapain, widely used to tenderize meat. Immature fruit, leaves, and flowers are also used as a cooked vegetable. Papaya also has industrial and pharmaceutical applications, including use in chewing gums, brewing, drugs for digestive ailments, treatment of gangrenous wounds, use in the textile industry, and use in production of soaps and shampoos.</discussion>
  <discussion>The species is cultivated in extreme southern Texas, where occasional seedlings arise from seeds discarded in waste places or dumps but are never persistent.</discussion>
  
</bio:treatment>