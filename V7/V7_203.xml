<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter C. Holmes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">CARICACEAE</taxon_name>
    <taxon_hierarchy>family CARICACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10161</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [rarely herbs], wood soft, sap milky.</text>
      <biological_entity id="o9267" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
      <biological_entity id="o9268" name="wood" name_original="wood" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o9269" name="sap" name_original="sap" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="milky" value_original="milky" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
    </statement>
    <statement id="d0_s2">
      <text>usually unbranched.</text>
      <biological_entity id="o9270" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate (borne at branch tips), palmately lobed [simple];</text>
      <biological_entity id="o9271" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules absent;</text>
      <biological_entity id="o9272" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present;</text>
      <biological_entity id="o9273" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade margins entire or lobed.</text>
      <biological_entity constraint="blade" id="o9274" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences usually axillary, paniculate [cymose-paniculate, cymose, or racemose];</text>
      <biological_entity id="o9275" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts present.</text>
      <biological_entity id="o9276" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels present or absent.</text>
      <biological_entity id="o9277" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers usually unisexual, rarely bisexual, staminate and pistillate usually on different plants, 5-merous;</text>
      <biological_entity id="o9278" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o9279" is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="5-merous" value_original="5-merous" />
      </biological_entity>
      <biological_entity id="o9279" name="plant" name_original="plants" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>calyces rotate, campanulate, or tubular, 5-toothed.</text>
      <biological_entity id="o9280" name="calyx" name_original="calyces" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rotate" value_original="rotate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="5-toothed" value_original="5-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: corolla funnelform [tubular, salverform], tube elongate, 5-lobed, lobes oblong to linear [ovate];</text>
      <biological_entity id="o9281" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9282" name="corolla" name_original="corolla" src="d0_s12" type="structure" />
      <biological_entity id="o9283" name="tube" name_original="tube" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity id="o9284" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10, in 2 series, borne at orifice of corolla-tube, alternating longer and shorter;</text>
      <biological_entity id="o9285" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9286" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="alternating" value_original="alternating" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9287" name="series" name_original="series" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9288" name="orifice" name_original="orifice" src="d0_s13" type="structure" />
      <biological_entity id="o9289" name="corolla-tube" name_original="corolla-tube" src="d0_s13" type="structure" />
      <relation from="o9286" id="r667" name="in" negation="false" src="d0_s13" to="o9287" />
      <relation from="o9286" id="r668" name="borne at" negation="false" src="d0_s13" to="o9288" />
      <relation from="o9286" id="r669" name="part_of" negation="false" src="d0_s13" to="o9289" />
    </statement>
    <statement id="d0_s14">
      <text>anthers dehiscing by longitudinal slits, introrse, distinct or connate basally, connective often projecting beyond anther sacs;</text>
      <biological_entity id="o9290" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9291" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="by slits" constraintid="o9292" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s14" value="introrse" value_original="introrse" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o9292" name="slit" name_original="slits" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o9293" name="connective" name_original="connective" src="d0_s14" type="structure">
        <character constraint="beyond anther sacs" constraintid="o9294" is_modifier="false" modifier="often" name="orientation" src="d0_s14" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="anther" id="o9294" name="sac" name_original="sacs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovary vestigial or absent.</text>
      <biological_entity id="o9295" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9296" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: petals distinct or connate basally, oblong to linear;</text>
      <biological_entity id="o9297" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9298" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s16" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary (1–) 5-carpellate, 1-locular;</text>
      <biological_entity id="o9299" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9300" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="(1-)5-carpellate" value_original="(1-)5-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s17" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>placentation parietal;</text>
      <biological_entity id="o9301" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="placentation" src="d0_s18" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 100+, anatropous, bitegmic;</text>
      <biological_entity id="o9302" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9303" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s19" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s19" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="bitegmic" value_original="bitegmic" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>styles 0 or 1;</text>
      <biological_entity id="o9304" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9305" name="style" name_original="styles" src="d0_s20" type="structure">
        <character name="presence" src="d0_s20" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s20" unit="or" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigmas 5, divided into 2 or more lobes.</text>
      <biological_entity id="o9306" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9307" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="5" value_original="5" />
        <character constraint="into 2 or morelobes" is_modifier="false" name="shape" src="d0_s21" value="divided" value_original="divided" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits: berries.</text>
      <biological_entity id="o9308" name="fruit" name_original="fruits" src="d0_s22" type="structure" />
      <biological_entity id="o9309" name="berry" name_original="berries" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>Seeds brown to black, ovoid to compressed, smooth or warty;</text>
      <biological_entity id="o9310" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s23" to="black" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s23" to="compressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s23" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s23" value="warty" value_original="warty" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>aril gelatinous;</text>
      <biological_entity id="o9311" name="aril" name_original="aril" src="d0_s24" type="structure">
        <character is_modifier="false" name="texture" src="d0_s24" value="gelatinous" value_original="gelatinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>embryo linear, cotyledons flat, broad.</text>
      <biological_entity id="o9312" name="embryo" name_original="embryo" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s25" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o9313" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s25" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s25" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Central America, South America, w Africa; tropical regions; introduced pantropically.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="w Africa" establishment_means="introduced" />
        <character name="distribution" value="tropical regions" establishment_means="native" />
        <character name="distribution" value="pantropically" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>0</number>
  <other_name type="common_name">Papaya Family</other_name>
  <discussion>Genera 6, species ca. 30 (1 in the flora).</discussion>
  <discussion>Caricaceae consists mainly of soft-wood trees containing little secondary xylem. Any “wood” present is usually produced from phloem. Economically, the most important plant is Carica papaya, the source of papaya fruit. The fruits of Jarilla Rusby and Jacaratia A. de Candolle are locally grown and eaten in Mexico. Papaya has great variation in the inflorescence, especially in the pistillate flowers, probably as a result of being under extensive cultivation.</discussion>
  <discussion>Caricaceae may be related to Passifloraceae, or to Cucurbitaceae.</discussion>
  
</bio:treatment>