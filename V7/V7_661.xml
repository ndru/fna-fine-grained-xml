<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Suzanne I. Warwick</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">RAPHANUS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 669. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 300. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus RAPHANUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek raphanos, radish</other_info_on_name>
    <other_info_on_name type="fna_id">127983</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Greuter &amp; Burdet" date="unknown" rank="genus">Quidproquo</taxon_name>
    <taxon_hierarchy>genus Quidproquo;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(roots slender or fleshy, size, shape, and color variable in cultivated forms);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous or pubescent.</text>
      <biological_entity id="o29120" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, unbranched or branched.</text>
      <biological_entity id="o29122" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate or subsessile;</text>
      <biological_entity id="o29123" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal not rosulate, petiolate, blade margins lyrately lobed or pinnatifid to pinnatisect;</text>
      <biological_entity constraint="basal" id="o29124" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o29125" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s7" to="pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline shortly petiolate or subsessile, blade (base not auriculate), margins dentate or lobed, (smaller and fewer-lobed than basal).</text>
      <biological_entity constraint="cauline" id="o29126" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o29127" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o29128" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, several-flowered), usually greatly elongated in fruit.</text>
      <biological_entity id="o29130" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate, ascending, or spreading [reflexed].</text>
      <biological_entity id="o29129" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o29130" is_modifier="false" modifier="usually greatly" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o29131" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o29129" id="r1964" name="fruiting" negation="false" src="d0_s10" to="o29131" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect, narrowly oblong [linear], lateral pair slightly saccate basally;</text>
      <biological_entity id="o29132" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o29133" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, creamy white, yellow, pink, or purple [lilac] (usually with darker veins), broadly obovate [suborbicular], claw differentiated from blade, (± longer than sepals, apex obtuse or emarginate [rounded]);</text>
      <biological_entity id="o29134" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o29135" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o29136" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o29137" is_modifier="false" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o29137" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens strongly tetradynamous;</text>
      <biological_entity id="o29138" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o29139" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o29140" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o29141" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong or oblong-linear, (apex obtuse);</text>
      <biological_entity id="o29142" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o29143" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-linear" value_original="oblong-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands (4), median pair present.</text>
      <biological_entity id="o29144" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o29145" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character name="atypical_quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques or silicles, indehiscent, sessile, segments 2, (lomentaceous, often breaking into 1-seeded units), cylindrical, fusiform, lanceolate, or ovoid, [linear, oblong, ellipsoid], smooth or torulose to strongly moniliform, (constricted or not between seeds), terete or polygonal;</text>
      <biological_entity id="o29146" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29147" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29148" name="silicle" name_original="silicles" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>(valvular segment seedless, rudimentary, or aborted, nearly as wide as pedicel; terminal segment several-seeded, corky);</text>
      <biological_entity id="o29149" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s17" value="torulose to strongly moniliform punct terete or polygonal" />
        <character char_type="range_value" from="torulose" name="shape" src="d0_s17" to="strongly moniliform punct terete or polygonal" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves glabrous, antrorsely scabrous, or hispid;</text>
      <biological_entity id="o29150" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s19" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>replum and septum not differentiated;</text>
      <biological_entity id="o29151" name="replum" name_original="replum" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s20" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o29152" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s20" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 2–22 per ovary;</text>
      <biological_entity id="o29154" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>(style slender);</text>
      <biological_entity id="o29153" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o29154" from="2" name="quantity" src="d0_s21" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate, slightly 2-lobed.</text>
      <biological_entity id="o29155" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s23" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds uniseriate, plump, not winged, oblong, ovoid, or globose [subglobose];</text>
      <biological_entity id="o29156" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s24" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s24" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s24" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s24" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (nearly smooth to reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o29157" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons conduplicate.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 9.</text>
      <biological_entity id="o29158" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s26" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity constraint="x" id="o29159" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia; introduced also nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="also nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Radish</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Natural hybridization between Raphanus raphanistrum and R. sativus has been known since 1788, and the hybrid has been named R. ×micranthus (Uechtritz) O. E. Schulz. The transfer of some of the weedy characters from R. raphanistrum to R. sativus through natural hybridization may have played a major role in converting the latter from a crop plant into a successful weed near the coastal areas of central California (C. A. Panetsos and H. G. Baker 1968). Raphanus confusus (Greuter &amp; Burdet) Al-Shehbaz &amp; Warwick is known from Asia (Israel, Lebanon).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals pale or creamy white; fruits (2.5-)3-8(-11) mm wide, strongly constricted between seeds and usually breaking, strongly ribbed, beak narrowly conical.</description>
      <determination>1 Raphanus raphanistrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually purple or pink, sometimes white; fruits (5-)7-13(-15) mm wide, rarely slightly constricted between seeds and usually not breaking, not ribbed, beak narrowly to broadly conical to linear.</description>
      <determination>2 Raphanus sativus</determination>
    </key_statement>
  </key>
</bio:treatment>