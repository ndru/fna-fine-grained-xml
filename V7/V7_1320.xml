<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">salix</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">×fragilis</taxon_name>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus salix;section salix;species ×fragilis</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Salix ×fragilis Linnaeus: The hybrid white willow, S. alba Linnaeus × S. euxina I.</text>
    </statement>
    <statement id="d0_s1">
      <text>Belyaeva, a European introduction, the most commonly cultivated and naturalized tree-willow in the flora area.</text>
      <biological_entity id="o23992" name="hybrid" name_original="hybrid" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" modifier="commonly" name="condition" src="d0_s1" value="cultivated" value_original="cultivated" />
      </biological_entity>
      <biological_entity id="o23993" name="flora" name_original="flora" src="d0_s1" type="structure" />
      <biological_entity id="o23994" name="area" name_original="area" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>It characterized by: trees, 3–20 m, stems erect or drooping;</text>
      <biological_entity id="o23995" name="whole-organism" name_original="" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" notes="" src="d0_s2" to="20" to_unit="m" />
      </biological_entity>
      <biological_entity id="o23996" name="tree" name_original="trees" src="d0_s2" type="structure" />
      <biological_entity id="o23997" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="drooping" value_original="drooping" />
      </biological_entity>
      <relation from="o23996" id="r1635" name="characterized by" negation="false" src="d0_s2" to="o23996" />
    </statement>
    <statement id="d0_s3">
      <text>branches highly brittle at base;</text>
      <biological_entity id="o23999" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o24000" is_modifier="false" modifier="highly" name="fragility" src="d0_s3" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity id="o24000" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o23999" id="r1636" name="characterized by" negation="false" src="d0_s3" to="o23999" />
    </statement>
    <statement id="d0_s4">
      <text>petioles with spherical or foliaceous glands distally, pilose or villous adaxially;</text>
      <biological_entity id="o24001" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o24002" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <biological_entity id="o24003" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="spherical" value_original="spherical" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o24002" id="r1637" name="characterized by" negation="false" src="d0_s4" to="o24002" />
      <relation from="o24002" id="r1638" name="with" negation="false" src="d0_s4" to="o24003" />
    </statement>
    <statement id="d0_s5">
      <text>largest medial leaf-blade amphistomatous, very narrowly elliptic or narrowly elliptic, margins uniformly serrate or serrulate, abaxial surface glaucous, both surfaces sparsely long-silky to glabrescent, adaxial surface slightly glossy or dull;</text>
      <biological_entity id="o24004" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="very narrowly" name="arrangement_or_shape" notes="" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly; narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o24005" name="leaf-blade" name_original="leaf-blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="amphistomatous" value_original="amphistomatous" />
      </biological_entity>
      <biological_entity id="o24006" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="uniformly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24007" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o24008" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparsely long-silky" name="pubescence" src="d0_s5" to="glabrescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24009" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
      </biological_entity>
      <relation from="o24005" id="r1639" name="characterized by" negation="false" src="d0_s5" to="o24005" />
    </statement>
    <statement id="d0_s6">
      <text>juvenile leaves at first densely long-silky soon glabrous;</text>
      <biological_entity id="o24010" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24011" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <relation from="o24011" id="r1640" name="characterized by" negation="false" src="d0_s6" to="o24011" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate bract deciduous after flowering;</text>
      <biological_entity id="o24013" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character constraint="after flowering" is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <relation from="o24013" id="r1641" name="characterized by" negation="false" src="d0_s7" to="o24013" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 2;</text>
      <biological_entity id="o24015" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o24015" id="r1642" name="characterized by" negation="false" src="d0_s8" to="o24015" />
    </statement>
    <statement id="d0_s9">
      <text>anthers yellow;</text>
      <biological_entity id="o24017" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o24017" id="r1643" name="characterized by" negation="false" src="d0_s9" to="o24017" />
    </statement>
    <statement id="d0_s10">
      <text>pistillate adaxial nectary shorter than or equal to stipe;</text>
      <biological_entity id="o24018" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character constraint="than or equal to stipe" constraintid="o24020" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24019" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character constraint="than or equal to stipe" constraintid="o24020" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24020" name="stipe" name_original="stipe" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <relation from="o24019" id="r1644" name="characterized by" negation="false" src="d0_s10" to="o24019" />
    </statement>
    <statement id="d0_s11">
      <text>stipe 0.3–0.5 mm;</text>
      <biological_entity id="o24022" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <relation from="o24022" id="r1645" name="characterized by" negation="false" src="d0_s11" to="o24022" />
    </statement>
    <statement id="d0_s12">
      <text>ovary pyriform, glabrous;</text>
      <biological_entity id="o24023" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24024" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <relation from="o24024" id="r1646" name="characterized by" negation="false" src="d0_s12" to="o24024" />
    </statement>
    <statement id="d0_s13">
      <text>ovules 6–12 per ovary;</text>
      <biological_entity id="o24026" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o24027" from="6" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
      <biological_entity id="o24027" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
      <relation from="o24026" id="r1647" name="characterized by" negation="false" src="d0_s13" to="o24026" />
    </statement>
    <statement id="d0_s14">
      <text>styles 0.4–1 mm;</text>
      <biological_entity id="o24029" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o24029" id="r1648" name="characterized by" negation="false" src="d0_s14" to="o24029" />
    </statement>
    <statement id="d0_s15">
      <text>capsules 4.5–6 mm;</text>
      <biological_entity id="o24031" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o24031" id="r1649" name="characterized by" negation="false" src="d0_s15" to="o24031" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 57, 76.</text>
      <biological_entity constraint="2n" id="o24033" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="57" value_original="57" />
        <character name="quantity" src="d0_s16" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering in late May–early June.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early June" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Ga., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., Ohio, Oreg., Pa., R.I., S.Dak., Tenn., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Individual trees can persist for years by trunk suckering and spread vegetatively by shoot fragmentation along stream margins, shingle and sand beaches, sedge meadows, hardwood forests, and sand pits.</discussion>
  <discussion>A study of Salix ×fragilis in Colorado (as S. ×rubens) showed that 2172 of 2175 trees were pistillate. Occasionally seed was set, possibly fertilized by S. alba (P. B. Shafroth et al. 1994). There are at least five clones of S. ×fragilis (as S. ×rubens) in cultivation (T. Berg in B. Jonsell and T. Karlsson 2000+, vol. 1); the pistillate are sterile but the staminate produce viable pollen. The hybrid plants are often misidentified as S. “fragilis” or as S. nigra. In the flora area, reproduction of the hybrid seems to be mainly by stem fragmentation.</discussion>
  <discussion>Prior to the lectotypification of Salix fragilis Linnaeus and the description of S. euxina (I. V. Belyaeva 2009), the name S. “fragilis” was often inadvertently used for both the pure species and for its hybrids with S. alba. Thus all herbarium specimens under the names “fragilis” and “×rubens” need to be revised.</discussion>
  <discussion>Salix ×fragilis can be separated from S. euxina by having branches and branchlets hairy or glabrescent in age versus glabrous; leaf blades not glaucous abaxially versus glaucous; leaves amphistomatous versus hypostomatous or with stomata only along veins and at apex; and pistillate catkins slender and loosely flowered versus stout and moderately densely flowered.</discussion>
  <discussion>Several molecular studies have been designed to understand the nature of this hybrid. H. Beissmann et al. (1997), using AFLP markers, were able to recognize three clusters: Salix alba, S. euxina (as S. fragilis), and S. ×fragilis (as S. ×rubens); but a study by K. De Cock et al. (2003), also using AFLP markers, was unable to resolve S. alba and S. ×fragilis (as S. ×rubens). They recommended the use of experimental hybridization to study the genesis of this hybrid. Molecular and genetic studies by L. L. Triest (2001) and coworkers concluded that in modern open agricultural situations in Belgium, hybridization was of low occurrence, and that morphologically intermediate plants were not necessarily genetically intermediate. These studies saw different facets of the question. Clearly there are three entities, S. alba, S. euxina, and their hybrid but, because S. euxina may be rare outside of cultivation, natural hybridization may not occur and the question of whether S. ×fragilis can be backcrossed with S. alba remains to be studied. The specimens used in these molecular studies require reidentification.</discussion>
  
</bio:treatment>