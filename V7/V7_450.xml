<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Adams ex de Candolle in A. P. de Candolle" date="1821" rank="species">pilosa</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle, Syst. Nat.</publication_title>
      <place_in_publication>2: 336. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species pilosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094795</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="(Adams ex de Candolle) O. E. Schulz" date="unknown" rank="variety">pilosa</taxon_name>
    <taxon_hierarchy>genus Draba;species alpina;variety pilosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Adams ex de Candolle" date="unknown" rank="species">aspera</taxon_name>
    <taxon_hierarchy>genus Draba;species aspera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(not stoloniferous);</text>
      <biological_entity id="o17503" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (covered with persistent, dry leaves and midveins);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o17504" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.4–1.7 dm, glabrous or pubescent, trichomes simple, 0.2–0.9 mm, and fewer, 2–4-rayed, 0.1–0.6 mm.</text>
      <biological_entity id="o17505" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s4" to="1.7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17506" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="fewer" value_original="fewer" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o17507" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole (base thickened), ciliate throughout;</text>
      <biological_entity id="o17508" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade linear to linear-oblanceolate, 0.5–1.5 (–2.2) cm × 1–2.5 (–4) mm, margins entire (thickened, ciliate, trichomes simple and 2-rayed, to 1.1 mm), surfaces pubescent, abaxially with simple trichomes, 0.3–1 mm, and 2–4-rayed ones, 0.1–0.5 mm, adaxially similar, or with only simple trichomes, (midvein prominent, thickened).</text>
      <biological_entity id="o17509" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s9" to="linear-oblanceolate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s9" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17510" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17511" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17512" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o17513" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="only" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o17511" id="r1192" modifier="abaxially" name="with" negation="false" src="d0_s9" to="o17512" />
      <relation from="o17511" id="r1193" modifier="adaxially" name="with" negation="false" src="d0_s9" to="o17513" />
    </statement>
    <statement id="d0_s10">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o17514" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Racemes (2–) 4–12-flowered, ebracteate, usually elongated in fruit;</text>
      <biological_entity id="o17515" name="raceme" name_original="racemes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="(2-)4-12-flowered" value_original="(2-)4-12-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o17516" is_modifier="false" modifier="usually" name="length" src="d0_s11" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o17516" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rachis not flexuous, glabrous or pubescent as stem.</text>
      <biological_entity id="o17518" name="stem" name_original="stem" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate-ascending, usually straight, rarely curved upward, 6–13 mm, glabrous or pubescent as stem.</text>
      <biological_entity id="o17517" name="rachis" name_original="rachis" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="as stem" constraintid="o17518" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17519" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <biological_entity id="o17520" name="stem" name_original="stem" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o17517" id="r1194" name="fruiting" negation="false" src="d0_s13" to="o17519" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals ovate, 2–3.5 mm, pubescent, (trichomes simple and short-stalked, 2-rayed);</text>
      <biological_entity id="o17521" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o17522" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals pale to bright-yellow, obovate, 3.5–6 × 2–3.5 mm;</text>
      <biological_entity id="o17523" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o17524" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s15" to="bright-yellow" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers ovate, 0.4–0.5 mm.</text>
      <biological_entity id="o17525" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o17526" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits elliptic to lanceolate, 5–11 × 3–4 mm, plane, flattened;</text>
      <biological_entity id="o17527" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s17" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s17" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s17" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves glabrous or puberulent with simple and short-stalked, 2-rayed trichomes, 0.07–0.3 mm;</text>
      <biological_entity id="o17528" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character constraint="with trichomes" constraintid="o17529" is_modifier="false" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="0.07" from_unit="mm" name="some_measurement" notes="" src="d0_s18" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17529" name="trichome" name_original="trichomes" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 12–20 per ovary;</text>
      <biological_entity id="o17530" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17531" from="12" name="quantity" src="d0_s19" to="20" />
      </biological_entity>
      <biological_entity id="o17531" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.4–0.9 mm (stigma about as wide as style).</text>
      <biological_entity id="o17532" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s20" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds ovoid, 1.2–1.4 × 0.8–1 mm.</text>
      <biological_entity id="o17533" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s21" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry gravelly slopes, sandy places, wet tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry gravelly slopes" />
        <character name="habitat" value="sandy places" />
        <character name="habitat" value="wet tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut, Yukon; Alaska; e Asia (ne Russian Far East, n Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (ne Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (n Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>84.</number>
  <discussion>O. E. Schulz (1927) reduced Draba pilosa to a variety of the decaploid D. alpina and cited North American collections. The former species was not listed by C. L. Hitchcock (1941) or R. C. Rollins (1993). From D. alpina, D. pilosa is easily distinguished by having strongly thickened and persistent (versus not thickened) midveins and margins, and usually narrower leaf blades [1–2.5(–5) versus 2.5–6(–9) mm wide].</discussion>
  <discussion>As recognized herein, Draba pilosa is broadly circumscribed to include perhaps two or three closely related taxa. All are scapose plants with large, yellow flowers and prominent, persistent midveins and petioles. Some Alaskan forms (e.g., Parker 7596, ALA), which grow in moist heath habitats, have leaves 3–5 mm wide, whereas the majority have narrower leaf blades rarely reaching 2.5 mm in width. Most plants assigned to this species have leaves with exclusively simple trichomes and often glabrous scapes. Others (e.g., Chesemore &amp; Davies 13, Murray 3371, Parker &amp; Batten 8954, all at ALA) have leaf blade surfaces and scapes with 2–4-rayed trichomes and blade margins ciliate with simple trichomes. One collection (Walker s.n., ALA) has a mixture of plants of both trichome types but no intermediates were found. Detailed molecular and cytological studies are needed on this complex to determine if more than one taxon is represented.</discussion>
  
</bio:treatment>