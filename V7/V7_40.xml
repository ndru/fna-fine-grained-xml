<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">salix</taxon_name>
    <taxon_name authority="Argus" date="1973" rank="section">Maccallianae</taxon_name>
    <place_of_publication>
      <publication_title>Salix Alaska Yukon,</publication_title>
      <place_in_publication>38. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus salix;section Maccallianae</taxon_hierarchy>
    <other_info_on_name type="fna_id">317498</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–5 m.</text>
      <biological_entity id="o24344" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches flexible at base.</text>
      <biological_entity id="o24345" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24346" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o24347" is_modifier="false" name="fragility" src="d0_s1" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity id="o24347" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules on late ones and vigorous shoots foliaceous or minute rudiments, apex acute;</text>
      <biological_entity id="o24348" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24349" name="stipule" name_original="stipules" src="d0_s2" type="structure" />
      <biological_entity id="o24350" name="one" name_original="ones" src="d0_s2" type="structure" />
      <biological_entity id="o24351" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s2" value="vigorous" value_original="vigorous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o24352" name="rudiment" name_original="rudiments" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o24353" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o24349" id="r1666" name="on" negation="false" src="d0_s2" to="o24350" />
    </statement>
    <statement id="d0_s3">
      <text>petiole not glandular distally;</text>
      <biological_entity id="o24354" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24355" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not; distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade hypostomatous or amphistomatous, abaxial surface not glaucous;</text>
      <biological_entity id="o24356" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o24357" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="hypostomatous" value_original="hypostomatous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="amphistomatous" value_original="amphistomatous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24358" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>juvenile blade densely short-silky or tomentose abaxially, hairs white and ferruginous.</text>
      <biological_entity id="o24359" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o24360" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o24361" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white and ferruginous" value_original="white and ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate floral bracts persistent after flowering.</text>
      <biological_entity constraint="floral" id="o24362" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="after flowering" is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: stamens 2;</text>
      <biological_entity id="o24363" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24364" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.8–1 mm.</text>
      <biological_entity id="o24365" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24366" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers: adaxial nectary shorter than stipe;</text>
      <biological_entity id="o24367" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24368" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character constraint="than stipe" constraintid="o24369" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o24369" name="stipe" name_original="stipe" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stipe 0.8–2 mm;</text>
      <biological_entity id="o24370" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24371" name="stipe" name_original="stipe" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary densely villous.</text>
      <biological_entity id="o24372" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24373" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>n North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="n North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b4.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>