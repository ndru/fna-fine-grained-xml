<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">alysseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ALYSSUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 650. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 293. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe alysseae;genus ALYSSUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, a-, not or without, and lyssa, rabies or madness; name used for plants reputed in ancient times as remedy for hydrophobia, cure for madness, and calmative for anger</other_info_on_name>
    <other_info_on_name type="fna_id">101238</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials [biennials, subshrubs];</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
      <biological_entity id="o17880" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>trichomes sessile, stellate, with 2–6 minute basal branches (branches as many as 3–25), rays branched or not, sometimes trichomes simple [lepidote].</text>
      <biological_entity id="o17882" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17883" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="true" name="size" src="d0_s2" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o17884" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character name="architecture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o17885" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o17882" id="r1221" name="with" negation="false" src="d0_s2" to="o17883" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, ascending, or decumbent, unbranched or branched.</text>
      <biological_entity id="o17886" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o17887" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate or not, petiolate or sessile, blade margins entire;</text>
      <biological_entity constraint="basal" id="o17888" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o17889" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline petiolate or sessile, blade (base cuneate or attenuate), margins entire.</text>
      <biological_entity constraint="cauline" id="o17890" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17891" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o17892" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (few to several-flowered, sometimes corymbose or paniculate).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending, divaricate, or reflexed, slender or stout.</text>
      <biological_entity id="o17893" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o17894" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o17893" id="r1222" name="fruiting" negation="false" src="d0_s9" to="o17894" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ovate or oblong, lateral pair not saccate;</text>
      <biological_entity id="o17895" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17896" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals yellow or white [rarely pink], suborbicular, spatulate, oblanceolate, linear-oblanceolate, or, obovate (apex obtuse or emarginate);</text>
      <biological_entity id="o17897" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17898" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o17899" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17900" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not winged, uni or bilaterally winged, appendaged, or toothed;</text>
      <biological_entity id="o17901" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o17902" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="bilaterally" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="appendaged" value_original="appendaged" />
        <character is_modifier="false" name="shape" src="d0_s13" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate or oblong;</text>
      <biological_entity id="o17903" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o17904" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands (4), 1 on each side of lateral stamen, median glands absent;</text>
      <biological_entity id="o17905" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o17906" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="4" value_original="4" />
        <character constraint="on side" constraintid="o17907" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17907" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity constraint="lateral" id="o17908" name="stamen" name_original="stamen" src="d0_s15" type="structure" />
      <biological_entity constraint="median" id="o17909" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o17907" id="r1223" name="part_of" negation="false" src="d0_s15" to="o17908" />
    </statement>
    <statement id="d0_s16">
      <text>(placentation apical or parietal).</text>
      <biological_entity id="o17910" name="flower" name_original="flowers" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits sessile, ovate-oblong, obovate, or elliptic [obcordate, rarely globose], usually strongly flattened, latiseptate, rarely inflated;</text>
      <biological_entity id="o17911" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="usually strongly" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each not veined (smooth), pubescent or glabrous;</text>
      <biological_entity id="o17912" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum (visible), rounded;</text>
      <biological_entity id="o17913" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete, (membranous, translucent, veinless);</text>
      <biological_entity id="o17914" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 1 or 2 [or 4–8] per ovary;</text>
      <biological_entity id="o17915" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s21" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o17916" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate.</text>
      <biological_entity id="o17917" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds biseriate or aseriate, flattened, winged or not, orbicular or suborbicular to ovoid;</text>
      <biological_entity id="o17918" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s23" value="aseriate" value_original="aseriate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character name="architecture" src="d0_s23" value="not" value_original="not" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s23" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (smooth or minutely reticulate), mucilaginous or not when wetted;</text>
      <biological_entity id="o17919" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s24" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons accumbent or incumbent.</text>
      <biological_entity id="o17920" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s25" value="accumbent" value_original="accumbent" />
        <character is_modifier="false" name="arrangement" src="d0_s25" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, se Europe, Asia, n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="se Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Madwort</other_name>
  <discussion>Species ca. 170 (6 in the flora).</discussion>
  <discussion>Alyssum has five introduced and one native species in North America. It is taxonomically difficult and is centered in Turkey and adjacent countries. For the determination of most species, both flowers and mature fruits are needed. Alyssum has been split into nine or more segregates; the segregates are based on the presence of staminal appendages, petal color, and number of ovules per ovary. In the absence of thorough molecular studies on Alyssum and its immediate relatives, it is more practical to delimit the genus broadly, as done here.</discussion>
  <references>
    <reference>Dudley, T. R. 1964. Studies in Alyssum: Near Eastern representatives and their allies, I. J. Arnold Arbor. 45: 57–100.</reference>
    <reference>Dudley, T. R. 1964b. Synopsis of the genus Alyssum. J. Arnold Arbor. 45: 358–373.</reference>
    <reference>Dudley, T. R. 1965. Studies in Alyssum: Near Eastern representatives and their allies, II. Section Meniocus and section Psilonema. J. Arnold Arbor. 46: 181–217.</reference>
    <reference>Dudley, T. R. 1966. Ornamental madworts (Alyssum) and the correct name of the goldentuft alyssum. Arnoldia (Jamaica Plain) 26: 33–48.</reference>
    <reference>Dudley, T. R. 1968. Alyssum (Cruciferae) introduced in North America. Rhodora 70: 298–300.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials; ovules 1 or 2 per ovary</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; ovules 2 per ovary</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades broadly oblanceolate, obovate-spatulate, or obovate; stems 0.7-1.5(-2) dm; ovules (1 or) 2 per ovary; seeds not winged or margined, 1-1.7 mm.</description>
      <determination>5 Alyssum obovatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades narrowly oblanceolate to linear; stems (2.5-)3-6(-7) dm; ovules 1 per ovary; seeds broadly winged, 3-3.8 mm.</description>
      <determination>6 Alyssum murale</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits usually glabrous, rarely sparsely pubescent (when young).</description>
      <determination>2 Alyssum desertorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits pubescent throughout</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals persistent; filaments not appendaged, toothed, or winged (slender).</description>
      <determination>1 Alyssum alyssoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals caducous; filaments at least some appendaged, toothed, or winged (expanded basally)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits orbicular; fruiting pedicels divaricate.</description>
      <determination>3 Alyssum simplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits ovate-oblong; fruiting pedicels ascending to suberect.</description>
      <determination>4 Alyssum szowitsianum</determination>
    </key_statement>
  </key>
</bio:treatment>