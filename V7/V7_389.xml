<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="treatment_page">301</other_info_on_meta>
    <other_info_on_meta type="illustration_page">299</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1880" rank="species">corrugata</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 430. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species corrugata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094740</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="species">vestita</taxon_name>
    <taxon_hierarchy>genus Draba;species vestita;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived);</text>
      <biological_entity id="o41659" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple (covered with persistent leaves);</text>
    </statement>
    <statement id="d0_s3">
      <text>not scapose.</text>
      <biological_entity id="o41661" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems branched, (0.3–) 0.4–1.7 (–2.5) dm, pubescent throughout, trichomes simple and long-stalked, 2-rayed, 0.4–1.4 mm, with smaller, 2–4-rayed ones, 0.1–0.6 mm, (simple ones usually fewer distally).</text>
      <biological_entity id="o41662" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s4" to="1.7" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41663" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-stalked" value_original="long-stalked" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41664" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o41663" id="r2809" name="with" negation="false" src="d0_s4" to="o41664" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o41665" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole base and margin ciliate, (trichomes simple, 0.6–2 mm);</text>
      <biological_entity constraint="petiole" id="o41666" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o41667" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade oblong to narrowly oblanceolate, (0.5–) 1–2.2 (–4.5) cm × 2–5 mm, margins entire, surfaces densely pubescent, abaxially with stalked, 2–4-rayed trichomes, 0.4–1.2 mm, (simple trichomes often along midvein), adaxially with mostly simple and long-stalked, 2-rayed trichomes, 0.6–1.3 mm, sometimes with 3-rayed or 4-rayed ones.</text>
      <biological_entity id="o41668" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="narrowly oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s9" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s9" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41669" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o41670" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41671" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o41672" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="mostly" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="long-stalked" value_original="long-stalked" />
      </biological_entity>
      <relation from="o41670" id="r2810" modifier="abaxially" name="with" negation="false" src="d0_s9" to="o41671" />
      <relation from="o41670" id="r2811" modifier="adaxially" name="with" negation="false" src="d0_s9" to="o41672" />
    </statement>
    <statement id="d0_s10">
      <text>Cauline leaves (4–) 6–10 (–13);</text>
    </statement>
    <statement id="d0_s11">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o41673" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="6" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="13" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="10" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>blade oblong to ovate, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o41674" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="ovate" />
      </biological_entity>
      <biological_entity id="o41675" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o41676" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character constraint="as basal" constraintid="o41677" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41677" name="basal" name_original="basal" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Racemes (10–) 18–55 (–67) -flowered, ebracteate or proximalmost flowers bracteate, slightly or considerably elongated in fruit;</text>
      <biological_entity id="o41678" name="raceme" name_original="racemes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="(10-)18-55(-67)-flowered" value_original="(10-)18-55(-67)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o41679" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o41680" is_modifier="false" modifier="slightly; considerably" name="length" src="d0_s13" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o41680" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o41682" name="stem" name_original="stem" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruiting pedicels divaricate to ascending, straight, 2–6 (–8) mm, pubescent as stem.</text>
      <biological_entity id="o41681" name="rachis" name_original="rachis" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s14" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o41682" is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41683" name="pedicel" name_original="pedicels" src="d0_s15" type="structure" />
      <biological_entity id="o41684" name="stem" name_original="stem" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o41681" id="r2812" name="fruiting" negation="false" src="d0_s15" to="o41683" />
    </statement>
    <statement id="d0_s16">
      <text>Flowers: sepals oblong, 2–2.7 mm, pubescent, (trichomes short-stalked, 2–4-rayed);</text>
      <biological_entity id="o41685" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o41686" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals yellow, linear, 2–3.5 × 0.2–0.5 mm;</text>
      <biological_entity id="o41687" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o41688" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s17" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers oblong, 0.6–0.8 mm (exserted).</text>
      <biological_entity id="o41689" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o41690" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits elliptic to oblong or linear to oblongelliptic, slightly twisted or plane, flattened, 5–13 (–17) × 2–3 (–4) mm;</text>
      <biological_entity id="o41691" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s19" to="oblong or linear" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s19" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s19" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s19" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s19" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s19" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s19" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>valves pubescent, trichomes short-stalked, cruciform, 0.1–0.5 mm, (sometimes with 2-rayed or 3-rayed ones);</text>
      <biological_entity id="o41692" name="valve" name_original="valves" src="d0_s20" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s20" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41693" name="trichome" name_original="trichomes" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="shape" src="d0_s20" value="cruciform" value_original="cruciform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s20" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 16–28 per ovary;</text>
      <biological_entity id="o41694" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o41695" from="16" name="quantity" src="d0_s21" to="28" />
      </biological_entity>
      <biological_entity id="o41695" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style 1.4–3.4 mm.</text>
      <biological_entity id="o41696" name="style" name_original="style" src="d0_s22" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s22" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 1–1.2 × 0.6–0.8 mm.</text>
      <biological_entity id="o41697" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s23" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s23" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine fellfields, talus, open pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine fellfields" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="open pine woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba corrugata was broadly circumscribed by R. C. Rollins (1993) to include three varieties that we treat as distinct species. For a discussion of species limits and distinguishing features, see I. A. Al-Shehbaz and M. D. Windham (2007). Draba corrugata, in the strict sense, is known from the San Antonio, San Bernardino, and San Gabriel mountains in Los Angeles and San Bernardino counties.</discussion>
  
</bio:treatment>