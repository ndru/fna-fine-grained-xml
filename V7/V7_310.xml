<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">220</other_info_on_meta>
    <other_info_on_meta type="illustration_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">hemiscola</taxon_name>
    <taxon_name authority="(Linnaeus) Rafinesque" date="1838" rank="species">aculeata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">aculeata</taxon_name>
    <taxon_hierarchy>family cleomaceae;genus hemiscola;species aculeata;variety aculeata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095301</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–50 cm.</text>
      <biological_entity id="o25166" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely to moderately branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrate to glandular-pubescent.</text>
      <biological_entity id="o25167" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s2" to="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipular spines (recurved) 1–3 mm;</text>
      <biological_entity id="o25168" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="stipular" id="o25169" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–6 cm (glandular-pubescent);</text>
      <biological_entity id="o25170" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o25171" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3, blade lanceolate-elliptic to ovate or rhombic, 2–4 (–6) × 1.5–3 (–4) cm, margins serrulate-denticulate, apex acute or obtuse, surfaces glandular-pubescent abaxially, glandular adaxially.</text>
      <biological_entity id="o25172" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o25173" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o25174" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate-elliptic" name="shape" src="d0_s5" to="ovate or rhombic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25175" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate-denticulate" value_original="serrulate-denticulate" />
      </biological_entity>
      <biological_entity id="o25176" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o25177" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="adaxially" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes 1–20 cm (10–40 cm in fruit);</text>
      <biological_entity id="o25178" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts unifoliate, elliptic, 10–20 mm.</text>
      <biological_entity id="o25179" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 10–25 mm, (glandular-pubescent).</text>
      <biological_entity id="o25180" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals persistent, green, lanceolate, 2–6 × 0.8–1.3 mm, margins entire, glandular-pubescent;</text>
      <biological_entity id="o25181" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25182" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25183" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, oblong to ovate, 5–10 × 2–3 mm;</text>
      <biological_entity id="o25184" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25185" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens purple, 5–10 (–25) mm;</text>
      <biological_entity id="o25186" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o25187" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 2.5–4 mm;</text>
      <biological_entity id="o25188" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o25189" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore 1–3 mm in fruit;</text>
      <biological_entity id="o25190" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o25191" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o25192" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25192" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovary 4–6 mm;</text>
      <biological_entity id="o25193" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o25194" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 0.1 mm.</text>
      <biological_entity id="o25195" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o25196" name="style" name_original="style" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules (15–) 25–40 (–65) × 2.5–4 mm, glabrous.</text>
      <biological_entity id="o25197" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s16" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="65" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s16" to="40" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 10–20, yellowish (black), obovoid, 2.4–2.8 × 1.9–2.1 mm, transversely rugose.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity id="o25198" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s17" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s17" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="width" src="d0_s17" to="2.1" to_unit="mm" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s17" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25199" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed roadsides, vacant lots, waste areas, gravel pits, lakeshores, streambeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed roadsides" />
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="gravel pits" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Tex.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>