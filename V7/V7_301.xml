<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cleome</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="species">rutidosperma</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>1: 241. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus cleome;species rutidosperma</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242412694</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Schumacher &amp; Thonning" date="unknown" rank="species">ciliata</taxon_name>
    <taxon_hierarchy>genus Cleome;species ciliata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 30–100 cm.</text>
      <biological_entity id="o38072" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely branched, (often decumbent);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or glabrescent to slightly scabrous (sometimes glandular-pubescent).</text>
      <biological_entity id="o38074" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s2" to="slightly scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules 0–0.5 mm;</text>
      <biological_entity id="o38075" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o38076" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (winged proximally), 0.5–3.5 cm;</text>
      <biological_entity id="o38077" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o38078" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3, blade oblanceolate to rhombic-elliptic, 1–3.5 × 0.5–1.7 cm, margins entire or serrulate-ciliate, apex usually acute to obtuse, sometimes acuminate, surfaces with curved hairs on veins abaxially, glabrous adaxially.</text>
      <biological_entity id="o38079" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o38080" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o38081" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="rhombic-elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o38082" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="serrulate-ciliate" value_original="serrulate-ciliate" />
      </biological_entity>
      <biological_entity id="o38083" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o38084" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o38085" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o38086" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <relation from="o38084" id="r2565" name="with" negation="false" src="d0_s5" to="o38085" />
      <relation from="o38085" id="r2566" name="on" negation="false" src="d0_s5" to="o38086" />
    </statement>
    <statement id="d0_s6">
      <text>Racemes 2–4 cm (8–15 cm in fruit);</text>
      <biological_entity id="o38087" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts trifoliate, 10–35 mm.</text>
      <biological_entity id="o38088" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="trifoliate" value_original="trifoliate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 11–21 mm (18–30 mm in fruit).</text>
      <biological_entity id="o38089" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s8" to="21" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals yellow, lanceolate, 2.5–4 × 0.2–0.3 mm, margins denticulate, ciliate, glabrous;</text>
      <biological_entity id="o38090" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o38091" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38092" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white or purple-speckled (2 central ones with yellow transverse band abaxially), oblong to narrowly ovate, 7–10 × 1.5–2.3 mm;</text>
      <biological_entity id="o38093" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38094" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple-speckled" value_original="purple-speckled" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="narrowly ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens yellow, 5–7 mm;</text>
      <biological_entity id="o38095" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38096" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1–2 mm;</text>
      <biological_entity id="o38097" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o38098" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore 4–12 mm in fruit;</text>
      <biological_entity id="o38099" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o38100" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o38101" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38101" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovary 2–3 mm, glabrous;</text>
      <biological_entity id="o38102" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o38103" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 0.5–1.4 mm.</text>
      <biological_entity id="o38104" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o38105" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules 40–70 × 3–4 mm.</text>
      <biological_entity id="o38106" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s16" to="70" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 4–25, reddish-brown to black, reniform, 1–1.5 mm, arillate.</text>
      <biological_entity id="o38107" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s17" to="25" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="arillate" value_original="arillate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering ± year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, vacant lots, canal banks, lawn edges in sun or shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="canal banks" />
        <character name="habitat" value="lawn edges" constraint="in sun or shade" />
        <character name="habitat" value="sun" />
        <character name="habitat" value="shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla., S.C.; tropical Asia; Africa; introduced also in Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" value="tropical Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Fringed spiderflower</other_name>
  <discussion>Cleome rutidosperma has sometimes been misidentified as Hemiscola aculeata (Cleome aculeata); it lacks the nodal spines of that species.</discussion>
  
</bio:treatment>