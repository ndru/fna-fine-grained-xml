<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">729</other_info_on_meta>
    <other_info_on_meta type="treatment_page">731</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="1895" rank="species">eucosmum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 175. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species eucosmum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094925</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived);</text>
    </statement>
    <statement id="d0_s2">
      <text>glaucous, glabrous (except petioles).</text>
      <biological_entity id="o31863" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems branched distally, 2–10 dm.</text>
      <biological_entity id="o31865" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: petiole (0.9–) 1.4–3 (–4.5) cm, ciliate;</text>
      <biological_entity constraint="basal" id="o31866" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31867" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually oblanceolate to oblong or lanceolate, rarely ovate or elliptic, (2.8–) 3.5–8.8 (–11) cm × (7–) 10–25 (–35) mm, margins often entire or repand, sometimes sinuate.</text>
      <biological_entity constraint="basal" id="o31868" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o31869" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually oblanceolate" name="shape" src="d0_s5" to="oblong or lanceolate rarely ovate or elliptic" />
        <character char_type="range_value" from="usually oblanceolate" name="shape" src="d0_s5" to="oblong or lanceolate rarely ovate or elliptic" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="atypical_length" src="d0_s5" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="11" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s5" to="8.8" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31870" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (ascending);</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o31871" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade lanceolate to oblong, 1.8–4.5 (–6) cm × 5–16 (–24) mm, (base amplexicaul to strongly auriculate), margins entire.</text>
      <biological_entity id="o31872" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="oblong" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s8" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="24" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31873" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes lax, slightly elongated in fruit, (flower buds oblong-linear).</text>
      <biological_entity id="o31875" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels horizontal to divaricate, usually straight, rarely slightly incurved, slender, (2.5–) 3–5.5 (–6.5) mm, slightly flattened at base.</text>
      <biological_entity id="o31874" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character constraint="in fruit" constraintid="o31875" is_modifier="false" modifier="slightly" name="length" src="d0_s9" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o31876" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o31877" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely slightly" name="orientation" src="d0_s10" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o31874" id="r2134" name="fruiting" negation="false" src="d0_s10" to="o31876" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect, linear-oblong, 5–7 (–8) × (0.8–) 1–1.5 (–1.8) mm;</text>
      <biological_entity id="o31878" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o31879" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s11" value="linear-oblong" value_original="linear-oblong" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_width" src="d0_s11" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals dark purple, spatulate to oblanceolate, (6.6–) 7.5–10 (–11.5) × 1–1.8 (–2) mm, margins not crisped, claw differentiated from blade, [slender, (3–) 3.5–5 (–5.5) mm, narrowest at base];</text>
      <biological_entity id="o31880" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31881" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s12" to="oblanceolate" />
        <character char_type="range_value" from="6.6" from_unit="mm" name="atypical_length" src="d0_s12" to="7.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31882" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o31883" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o31884" is_modifier="false" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o31884" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>nectar glands confluent, subtending bases of stamens;</text>
      <biological_entity id="o31885" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectar" id="o31886" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o31887" name="base" name_original="bases" src="d0_s13" type="structure" />
      <biological_entity id="o31888" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <relation from="o31886" id="r2135" name="subtending" negation="false" src="d0_s13" to="o31887" />
      <relation from="o31886" id="r2136" name="part_of" negation="false" src="d0_s13" to="o31888" />
    </statement>
    <statement id="d0_s14">
      <text>filaments subequal, (4.5–) 6–9 (–10) mm;</text>
      <biological_entity id="o31889" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o31890" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers exserted, linear to narrowly oblong, 2.5–4 (–4.5) mm, circinately coiled;</text>
      <biological_entity id="o31891" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o31892" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s15" to="narrowly oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="circinately" name="architecture" src="d0_s15" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore (1–) 2.5–6 (–7.5) mm.</text>
      <biological_entity id="o31893" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o31894" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits divaricate to ascending, torulose, straight or slightly incurved, terete, (2–) 2.4–5 (–6.5) cm × 0.7–1 (–1.3) mm;</text>
      <biological_entity id="o31895" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s17" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s17" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s17" to="2.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s17" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s17" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 44–58 per ovary;</text>
      <biological_entity id="o31896" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31897" from="44" name="quantity" src="d0_s18" to="58" />
      </biological_entity>
      <biological_entity id="o31897" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style usually cylindrical, rarely subclavate, 0.5–1.5 (–2) mm.</text>
      <biological_entity id="o31898" name="style" name_original="style" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s19" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s19" value="subclavate" value_original="subclavate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 0.7–1.5 × 0.5–0.8 mm.</text>
      <biological_entity id="o31899" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s20" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s20" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shady slopes and canyons, pinyon-juniper and oak woodland communities, stream beds, streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shady slopes" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="oak woodland communities" />
        <character name="habitat" value="stream beds" />
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Thelypodium eucosmum is known only from Baker, Grant, and Wheeler counties. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>