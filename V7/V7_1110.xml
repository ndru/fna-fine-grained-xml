<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="treatment_page">656</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Rydberg) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">ovalifolia</taxon_name>
    <taxon_name authority="(Goodman) O’Kane &amp; Al-Shehbaz" date="2002" rank="subspecies">alba</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 326. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species ovalifolia;subspecies alba</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095245</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">ovalifolia</taxon_name>
    <taxon_name authority="Goodman" date="unknown" rank="variety">alba</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>38: 239. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species ovalifolia;variety alba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(A. Gray) S. Watson" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="(Goodman) C. Clark" date="unknown" rank="subspecies">alba</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species engelmannii;subspecies alba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ovalifolia</taxon_name>
    <taxon_name authority="(Goodman) Rollins &amp; E. A. Shaw" date="unknown" rank="subspecies">alba</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species ovalifolia;subspecies alba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ovalifolia</taxon_name>
    <taxon_name authority="(Goodman) B. L. Turner" date="unknown" rank="variety">alba</taxon_name>
    <taxon_hierarchy>genus Physaria;species ovalifolia;variety alba;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex usually simple.</text>
      <biological_entity id="o232" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually few from base, erect or outer decumbent, 1.5–2.5 dm.</text>
      <biological_entity id="o233" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from base" constraintid="o234" is_modifier="false" modifier="usually" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s1" value="outer" value_original="outer" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o234" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: blade (outer) often broadly elliptic, base narrowing gradually to petiole, margins sinuate to dentate (or blade deltate, less than 1 cm, base abruptly narrowed to petiole).</text>
      <biological_entity constraint="basal" id="o235" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o236" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often broadly" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o237" name="base" name_original="base" src="d0_s2" type="structure">
        <character constraint="to petiole" constraintid="o238" is_modifier="false" name="width" src="d0_s2" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o238" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o239" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="sinuate" name="shape" src="d0_s2" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes usually elongated.</text>
      <biological_entity id="o240" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s3" value="elongated" value_original="elongated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petals white, (9–) 11–15 mm, often 2 times as long as sepals.</text>
      <biological_entity id="o242" name="sepal" name_original="sepals" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n = 12.</text>
      <biological_entity id="o241" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character constraint="sepal" constraintid="o242" is_modifier="false" name="length" src="d0_s4" value="2 times as long as sepals" />
      </biological_entity>
      <biological_entity constraint="2n" id="o243" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May, usually in Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="range_value" modifier="usually" to="Apr" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limey and gravelly knolls, grassland hills, limestone hillsides and breaks, gypsum, shale, rocky calcareous soils, stony areas, prairie pastures, limestone roadcuts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly knolls" modifier="limey and" />
        <character name="habitat" value="grassland hills" />
        <character name="habitat" value="limestone hillsides" />
        <character name="habitat" value="breaks" />
        <character name="habitat" value="gypsum" />
        <character name="habitat" value="shale" />
        <character name="habitat" value="rocky calcareous" />
        <character name="habitat" value="soils" />
        <character name="habitat" value="stony areas" />
        <character name="habitat" value="prairie pastures" />
        <character name="habitat" value="limestone roadcuts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>62b.</number>
  
</bio:treatment>