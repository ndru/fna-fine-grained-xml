<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="1824" rank="genus">cleomella</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">hillmanii</taxon_name>
    <taxon_name authority="(S. L. Welsh) P. K. Holmgren" date="2004" rank="variety">goodrichii</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>56: 105. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus cleomella;species hillmanii;variety goodrichii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095305</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleomella</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">palmeriana</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">goodrichii</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>46: 263. 1986 (as palmerana)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cleomella;species palmeriana;variety goodrichii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleomella</taxon_name>
    <taxon_name authority="Payson" date="unknown" rank="species">macbrideana</taxon_name>
    <taxon_hierarchy>genus Cleomella;species macbrideana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Pedicels 4.5–8.5 (–10) mm in fruit;</text>
      <biological_entity id="o9375" name="pedicel" name_original="pedicels" src="d0_s0" type="structure">
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o9376" from="4.5" from_unit="mm" name="some_measurement" src="d0_s0" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9376" name="fruit" name_original="fruit" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>gynophore 3.5–6.5 mm in fruit (about as long as capsule).</text>
      <biological_entity id="o9377" name="gynophore" name_original="gynophore" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o9378" from="3.5" from_unit="mm" name="some_measurement" src="d0_s1" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9378" name="fruit" name_original="fruit" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Capsules (5–) 6.5–10.5 mm diam. 2n = 34.</text>
      <biological_entity id="o9379" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s2" to="6.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="diameter" src="d0_s2" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9380" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open usually alkaline meadows and flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open alkaline meadows" modifier="dry usually" />
        <character name="habitat" value="flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <discussion>Following P. K. Holmgren and A. Cronquist (2005), Cleomella macbrideana is treated as a synonym of var. goodrichii. That entity is disjunct from the main body of the species range; its closest morphological match is with this variety.</discussion>
  
</bio:treatment>