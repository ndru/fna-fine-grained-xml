<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="C. K. Schneider in C. S. Sargent" date="1916" rank="section">diplodictyae</taxon_name>
    <taxon_name authority="A. K. Skvortsov" date="1966" rank="species">sphenophylla</taxon_name>
    <place_of_publication>
      <publication_title>Spisok Rast. Gerb. Fl. S.S.S.R. Bot. Inst. Vsesoyuzn. Akad. Nauk</publication_title>
      <place_in_publication>16: 62. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section diplodictyae;species sphenophylla</taxon_hierarchy>
    <other_info_on_name type="fna_id">242445877</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sphenophylla</taxon_name>
    <taxon_name authority="A. K. Skvortsov" date="unknown" rank="subspecies">pseudotorulosa</taxon_name>
    <taxon_hierarchy>genus Salix;species sphenophylla;subspecies pseudotorulosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.03–0.12 m, not clonal or forming clones by layering.</text>
      <biological_entity id="o26242" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.03" from_unit="m" name="some_measurement" src="d0_s0" to="0.12" to_unit="m" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" src="d0_s0" value="forming" value_original="forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems trailing and rooting;</text>
      <biological_entity id="o26243" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellowbrown or brownish, glabrous;</text>
      <biological_entity id="o26244" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown, glabrous.</text>
      <biological_entity id="o26245" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules absent or rudimentary;</text>
      <biological_entity id="o26246" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o26247" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 4–25 mm, (glabrous or pilose adaxially);</text>
      <biological_entity id="o26248" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o26249" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade hypostomatous, narrowly elliptic, broadly elliptic, obovate, or very broadly obovate, 19–52 × 10–28 mm, 1–3 times as long as wide, base cuneate or convex, margins flat or slightly revolute, entire, apex convex, retuse, or rounded, abaxial surface glabrous, pilose or sparsely long-silky to glabrescent, hairs straight or wavy, adaxial slightly glossy, glabrous or pilose;</text>
      <biological_entity id="o26250" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o26251" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="hypostomatous" value_original="hypostomatous" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s6" to="52" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="28" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1-3" value_original="1-3" />
      </biological_entity>
      <biological_entity id="o26252" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o26253" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o26254" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26255" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="sparsely long-silky" name="pubescence" src="d0_s6" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o26256" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26257" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o26258" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o26259" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade (reddish), very sparsely long-silky abaxially.</text>
      <biological_entity id="o26260" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="very sparsely; abaxially" name="pubescence" notes="" src="d0_s8" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o26261" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 21–53 × 7–13 mm, flowering branchlet 8–20 mm;</text>
      <biological_entity id="o26262" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s9" to="53" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26263" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate loosely to densely flowered, slender or stout, 32–79 × 7–18 mm, flowering branchlet 4–27 mm;</text>
      <biological_entity id="o26264" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s10" to="loosely densely flowered" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s10" value="stout" value_original="stout" />
        <character char_type="range_value" from="32" from_unit="mm" name="length" src="d0_s10" to="79" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26265" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="27" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown or black, 1.1–2 mm, apex rounded, entire, abaxially hairy or ciliate, hairs straight.</text>
      <biological_entity id="o26266" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o26267" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26268" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26269" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary absent, adaxial nectary oblong, 0.6–1 mm;</text>
      <biological_entity id="o26270" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26271" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26272" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct;</text>
      <biological_entity id="o26273" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26274" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ellipsoid or shortly cylindrical, 0.4–0.6 mm.</text>
      <biological_entity id="o26275" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26276" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong or ovate, 0.7–1.6 mm, equal to or longer than stipe;</text>
      <biological_entity id="o26277" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26278" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s15" value="equal" value_original="equal" />
        <character constraint="than stipe" constraintid="o26279" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o26279" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.5–1.4 mm;</text>
      <biological_entity id="o26280" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26281" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary obclavate or pyriform, glabrous, or patchy or streaky pilose or villous, especially on beak, beak gradually tapering to or slightly bulged below styles;</text>
      <biological_entity id="o26282" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26283" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="obclavate" value_original="obclavate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="density" src="d0_s17" value="patchy" value_original="patchy" />
        <character name="density" src="d0_s17" value="streaky" value_original="streaky" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26284" name="beak" name_original="beak" src="d0_s17" type="structure" />
      <biological_entity id="o26285" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="gradually; slightly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o26286" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o26283" id="r1780" modifier="especially" name="on" negation="false" src="d0_s17" to="o26284" />
      <relation from="o26285" id="r1781" modifier="below" name="bulged" negation="false" src="d0_s17" to="o26286" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 10–18 per ovary;</text>
      <biological_entity id="o26287" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26288" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o26289" from="10" name="quantity" src="d0_s18" to="18" />
      </biological_entity>
      <biological_entity id="o26289" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.6–1.8 mm;</text>
      <biological_entity id="o26290" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26291" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s19" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with pointed tip, or slenderly cylindrical, 0.32–0.5–0.68 mm.</text>
      <biological_entity id="o26292" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o26293" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o26294" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.32" from_unit="mm" name="some_measurement" src="d0_s20" to="0.5-0.68" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26294" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 4–12 mm. 2n = 38, 57.</text>
      <biological_entity id="o26295" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s21" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26296" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
        <character name="quantity" src="d0_s21" value="57" value_original="57" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jun-late Jul (early Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jul" from="mid Jun" />
        <character name="flowering time" char_type="atypical_range" to="early Aug" from="mid Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stony or gravelly substrates on talus, rocky outcrops, dry, stony tundra, sandy and moss tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony" constraint="on talus , rocky outcrops , dry , stony tundra , sandy and moss tundra" />
        <character name="habitat" value="gravelly substrates" constraint="on talus , rocky outcrops , dry , stony tundra , sandy and moss tundra" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="stony tundra" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="moss tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; e Asia (Chukotka, Russian Far East, e Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Chukotka)" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (e Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>42.</number>
  <other_name type="common_name">Wedge-leaf willow</other_name>
  <discussion>The patchy or streaky indumentum on the ovaries of some plants suggests that they may be hybrids.</discussion>
  
</bio:treatment>