<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="treatment_page">455</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">camelineae</taxon_name>
    <taxon_name authority="Desvaux" date="unknown" rank="genus">NESLIA</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Agric.</publication_title>
      <place_in_publication>3: 162. 1815</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe camelineae;genus NESLIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For J. A. N. de Nesle, eighteenth-century French gardener at Poitiers</other_info_on_name>
    <other_info_on_name type="fna_id">122184</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>mostly pubescent, trichomes short-stalked, forked or substellate, mixed (on stem) with simple ones.</text>
      <biological_entity id="o38690" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o38691" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" name="shape" src="d0_s2" value="substellate" value_original="substellate" />
        <character constraint="with ones" constraintid="o38692" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o38692" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched basally, branched distally.</text>
      <biological_entity id="o38693" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o38694" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal not rosulate, shortly petiolate, blade margins entire, dentate, or denticulate;</text>
      <biological_entity constraint="basal" id="o38695" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o38696" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline blade (base sagittate or strongly auriculate), margins usually entire, rarely denticulate.</text>
      <biological_entity constraint="cauline" id="o38697" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o38698" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose, several-flowered, forming panicles), considerably elongated in fruit.</text>
      <biological_entity id="o38700" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, slender.</text>
      <biological_entity id="o38699" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o38700" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o38701" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o38699" id="r2609" name="fruiting" negation="false" src="d0_s9" to="o38701" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect, oblong-ovate (pubescent);</text>
      <biological_entity id="o38702" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38703" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals yellow, spatulate, (longer than sepals), claw not differentiated from blade, (apex obtuse);</text>
      <biological_entity id="o38704" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38705" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o38706" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o38707" is_modifier="false" modifier="not" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o38707" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly tetradynamous;</text>
      <biological_entity id="o38708" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o38709" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o38710" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o38711" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate, (apex obtuse);</text>
      <biological_entity id="o38712" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o38713" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands lateral, 1 on each side of lateral stamen.</text>
      <biological_entity id="o38714" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o38715" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o38716" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o38716" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity constraint="lateral" id="o38717" name="stamen" name_original="stamen" src="d0_s15" type="structure" />
      <relation from="o38716" id="r2610" name="part_of" negation="false" src="d0_s15" to="o38717" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits silicles, nutletlike, indehiscent, subsessile, woody, compressed globose or sublenticular (readily detached from pedicel at maturity, apex truncate [umbonate]);</text>
      <biological_entity constraint="fruits" id="o38718" name="silicle" name_original="silicles" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="nutletlike" value_original="nutletlike" />
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="texture" src="d0_s16" value="woody" value_original="woody" />
        <character is_modifier="false" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="sublenticular" value_original="sublenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves (1-seeded), prominently reticulate, glabrous;</text>
      <biological_entity id="o38719" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture_or_coloration_or_relief" src="d0_s17" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>replum rounded (obscured by valve margin);</text>
      <biological_entity id="o38720" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete;</text>
      <biological_entity id="o38721" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 2–4 per ovary;</text>
      <biological_entity id="o38723" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>(style distinct, cylindrical, readily caducous at fruit maturity, leaving umbo or apicula);</text>
      <biological_entity id="o38722" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o38723" from="2" name="quantity" src="d0_s20" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate.</text>
      <biological_entity id="o38724" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate, plump, not winged, ovoid;</text>
      <biological_entity id="o38725" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (minutely reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o38726" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 7.</text>
      <biological_entity id="o38727" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o38728" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, n Africa; introduced also in South America (Argentina), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="also in South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>35.</number>
  <discussion>Species 1.</discussion>
  <discussion>Some authors recognize two species in Neslia, while others recognize only N. paniculata with two subspecies somewhat separated geographically, though intermediates are common in areas of overlap (P. W. Ball 1961). The sole difference between them is whether the fruit apex is truncate (subsp. paniculata) or apiculate [subsp. thracica (Velenovský) Bornmüller or N. apiculata Fischer, C. A. Meyer &amp; Avé-Lallemant].</discussion>
  <references>
    <reference>Ball, P. W. 1961. The taxonomic status of Neslia paniculata (L.) Desv. and N. apiculata Fisch., Mey. &amp; Avé-Lall. Feddes Repert. Spec. Nov. Regni Veg. 64: 11–13.</reference>
  </references>
  
</bio:treatment>