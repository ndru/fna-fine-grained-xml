<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="mention_page">521</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">descurainieae</taxon_name>
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="genus">descurainia</taxon_name>
    <taxon_name authority="(Greene) O. E. Schulz in H. G. A. Engler" date="1924" rank="species">obtusa</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>86[IV,105]: 321. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe descurainieae;genus descurainia;species obtusa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095084</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sophia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">obtusa</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 96. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sophia;species obtusa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="(Greene) A. Nelson &amp; J. F. Macbride" date="unknown" rank="species">obtusum</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species obtusum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular or eglandular;</text>
    </statement>
    <statement id="d0_s2">
      <text>finely pubescent, often canescent, trichomes dendritic, sometimes mixed with simple ones.</text>
      <biological_entity id="o21897" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o21898" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
        <character constraint="with ones" constraintid="o21899" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o21899" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched basally or branched proximally and/or distally, 4–12 (–15) dm.</text>
      <biological_entity id="o21900" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" modifier="distally" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" modifier="distally" name="some_measurement" src="d0_s3" to="12" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: petiole 0.5–3.7 cm;</text>
      <biological_entity constraint="basal" id="o21901" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21902" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade pinnate, oblanceolate to obovate or ovate in outline, 1–6 cm, lateral lobes (2–5 pairs), oblanceolate to linear or narrowly lanceolate, (7–25 × 2–10 mm), margins usually entire or serrate, rarely incised, (apex obtuse).</text>
      <biological_entity constraint="basal" id="o21903" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21904" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" constraint="in outline" constraintid="o21905" from="oblanceolate" name="shape" src="d0_s5" to="obovate or ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21905" name="outline" name_original="outline" src="d0_s5" type="structure" />
      <biological_entity constraint="lateral" id="o21906" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="linear or narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o21907" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves sessile or shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o21908" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade smaller distally, distal lobes often narrower, surfaces densely pubescent.</text>
      <biological_entity id="o21909" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21910" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="width" src="d0_s7" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o21911" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o21913" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending to divaricate, straight, 6–15 mm.</text>
      <biological_entity id="o21912" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o21913" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21914" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o21912" id="r1523" name="fruiting" negation="false" src="d0_s9" to="o21914" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals spreading or sometimes ascending, greenish to yellowish, oblong, 1–2 mm, densely pubescent, (trichomes dendritic, sometimes mixed with glandular papillae);</text>
      <biological_entity id="o21915" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21916" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s10" to="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals oblanceolate, 1–2 × 0.5–0.7 mm (equaling or shorter than sepals);</text>
      <biological_entity id="o21917" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21918" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>median filaments 1.4–2 mm;</text>
      <biological_entity id="o21919" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="median" id="o21920" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.2–0.3 mm.</text>
      <biological_entity id="o21921" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21922" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits divaricate to suberect, linear, slightly torulose, 10–20 (–23) × 0.7–1 mm, (acute at both ends);</text>
      <biological_entity id="o21923" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="23" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s14" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with distinct midvein, (sparsely to densely pubescent);</text>
      <biological_entity id="o21924" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o21925" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o21924" id="r1524" name="with" negation="false" src="d0_s15" to="o21925" />
    </statement>
    <statement id="d0_s16">
      <text>septum not veined;</text>
      <biological_entity id="o21926" name="septum" name_original="septum" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 16–40 per ovary;</text>
      <biological_entity id="o21927" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o21928" from="16" name="quantity" src="d0_s17" to="40" />
      </biological_entity>
      <biological_entity id="o21928" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.1–0.2 mm, glabrous.</text>
      <biological_entity id="o21929" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate or biseriate, light-brown, oblong, 0.7–1.1 × 0.5–0.6 mm. 2n = 14.</text>
      <biological_entity id="o21930" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s19" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s19" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s19" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21931" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep(-Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly grounds, sandy areas, disturbed sites, open forests, plateaus, abandoned mine areas, dry streams and washes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly grounds" />
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="plateaus" />
        <character name="habitat" value="mine areas" modifier="abandoned" />
        <character name="habitat" value="dry streams" />
        <character name="habitat" value="washes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., N.Mex.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>As circumscribed here, Descurainia obtusa is a relatively uniform, diploid species. It probably was involved as a parent of D. adenophora, which is a hexaploid readily distinguished by characters discussed thereunder.</discussion>
  
</bio:treatment>