<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">190</other_info_on_meta>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
    <other_info_on_meta type="illustration_page">187</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">resedaceae</taxon_name>
    <taxon_name authority="Cambessèdes in V. Jacquemont" date="unknown" rank="genus">oligomeris</taxon_name>
    <taxon_name authority="(Vahl) J. F. Macbride" date="1918" rank="species">linifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>53: 13. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family resedaceae;genus oligomeris;species linifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009755</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Reseda</taxon_name>
    <taxon_name authority="Vahl" date="unknown" rank="species">linifolia</taxon_name>
    <place_of_publication>
      <publication_title>in J. W. Hornemann, Hort. Bot. Hafn.</publication_title>
      <place_in_publication>2: 501. 1815</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Reseda;species linifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually annual, rarely perennial, 8–40 cm.</text>
      <biological_entity id="o27753" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade linear to oblanceolate, 0.8–5 × 0.05–0.2 cm, sometimes with hyaline basal teeth 0.2–0.4 mm, apex obtuse or acute, surfaces glabrous or scabrid.</text>
      <biological_entity id="o27754" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o27755" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="oblanceolate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.05" from_unit="cm" name="width" src="d0_s1" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27756" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s1" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27757" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o27758" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="scabrid" value_original="scabrid" />
      </biological_entity>
      <relation from="o27755" id="r1863" modifier="sometimes" name="with" negation="false" src="d0_s1" to="o27756" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 2–12 (–25) cm;</text>
      <biological_entity id="o27759" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts persistent, linear to lanceolate, 1–1.5 mm.</text>
      <biological_entity id="o27760" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals lanceolate to deltate, 0.7–1.5 (–2) mm;</text>
      <biological_entity id="o27761" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o27762" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="deltate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals whitish, ovate, slightly shorter than sepals, 0.7–1 (–1.7) mm, usually distinct, sometimes barely connate basally;</text>
      <biological_entity id="o27763" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o27764" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character constraint="than sepals" constraintid="o27765" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="slightly shorter" value_original="slightly shorter" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes barely; basally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o27765" name="sepal" name_original="sepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>filaments 1.5 (–2) mm, barely connate basally;</text>
      <biological_entity id="o27766" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o27767" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="barely; basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 0.3–0.5 mm.</text>
      <biological_entity id="o27768" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o27769" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules sessile or subsessile, compressed-subglobose, 1.7–3 × 2–3.4 mm, apically 4-toothed, teeth acute, 0.5–0.7 mm.</text>
      <biological_entity id="o27770" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="compressed-subglobose" value_original="compressed-subglobose" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.4" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s8" value="4-toothed" value_original="4-toothed" />
      </biological_entity>
      <biological_entity id="o27771" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds black or brown, 0.5–0.7 mm. 2n = 30.</text>
      <biological_entity id="o27772" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27773" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan-)Feb–Jul(-Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pastures, desert washes, sand dunes, creosote scrub, mesquite thickets, volcanic soil, sometimes in brackish or alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pastures" />
        <character name="habitat" value="desert washes" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="creosote scrub" />
        <character name="habitat" value="mesquite thickets" />
        <character name="habitat" value="volcanic soil" />
        <character name="habitat" value="brackish" modifier="sometimes" />
        <character name="habitat" value="alkaline soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-60-1200(-1400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="60" from_unit="m" constraint="- " />
        <character name="elevation" char_type="atypical_range" to="1400" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex., Tex., Utah; n Mexico (Baja California, Baja California Sur, Chihuahua, Coahuila, Durango, Nuevo León, Sonora, Zacatecas); sw, c Asia; n Africa; n Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="n Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="n Mexico (Zacatecas)" establishment_means="native" />
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="n Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Flaxleaf-whitepuff</other_name>
  <other_name type="common_name">desert-spike</other_name>
  <discussion>The native status of Oligomeris linifolia in North America has long been controversial; its presence represents the most remarkable disjunction in the family. Recent molecular research (S. Martín-Bravo et al. 2009) strongly suggests that it is native here.</discussion>
  <discussion>Oligomeris linifolia has been reported to be toxic to cattle. Occurrence of this species in Utah is based on the following collection: “southern Utah,” 1877, E. Palmer 47 (US, WIS). “Cambess” has appeared in literature as a common name for O. linifolia, apparently resulting from confusion with an abbreviation of the author Cambessèdes. “Lineleaf whitepuff” has also appeared as a common name for this species; the epithet linifolia should be translated as “flax-leaved.”</discussion>
  <discussion>SELECTED REFERENCE Martín-Bravo, S. et al. 2009. Is Oligomeris (Resedaceae) indigenous to Noth America: Molecular evidence for a natural colonization from the Old World. Amer. J. Bot. 96: 507–518.</discussion>
  
</bio:treatment>