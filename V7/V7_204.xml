<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">caricaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">CARICA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1036. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 458. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caricaceae;genus CARICA</taxon_hierarchy>
    <other_info_on_name type="etymology">Alluding to imagined resemblance of leaves or fruits to those of a fig, Ficus carica, erroneously thought to be from Caria in southwestern Asia Minor</other_info_on_name>
    <other_info_on_name type="fna_id">105651</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees relatively short-lived.</text>
      <biological_entity id="o42433" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="relatively" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded distally on branches;</text>
      <biological_entity id="o42435" name="branch" name_original="branches" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>glabrous.</text>
      <biological_entity id="o42434" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="on branches" constraintid="o42435" is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: staminate 100+-flowered, elongate;</text>
      <biological_entity id="o42436" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="100+-flowered" value_original="100+-flowered" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate 1–several-flowered.</text>
      <biological_entity id="o42437" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-several-flowered" value_original="1-several-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers each borne in axil of bract.</text>
      <biological_entity id="o42438" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o42439" name="axil" name_original="axil" src="d0_s5" type="structure" />
      <biological_entity id="o42440" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <relation from="o42438" id="r2853" name="borne in" negation="false" src="d0_s5" to="o42439" />
      <relation from="o42438" id="r2854" name="borne in" negation="false" src="d0_s5" to="o42440" />
    </statement>
    <statement id="d0_s6">
      <text>Berries slightly 5-angled.</text>
    </statement>
    <statement id="d0_s7">
      <text>x = 9.</text>
      <biological_entity id="o42441" name="berry" name_original="berries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="5-angled" value_original="5-angled" />
      </biological_entity>
      <biological_entity constraint="x" id="o42442" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Fla.; Central America, South America; introduced also pantropically.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also pantropically" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Papaya</other_name>
  <discussion>Species, in the traditional sense, ca. 20 (1 in the flora).</discussion>
  <discussion>V. M. Badillo (2000) considered Carica to consist of only one species (C. papaya), others being reassigned to the genus Vasconcellea A. Saint-Hilaire.</discussion>
  <discussion>Some species of Carica in the traditional sense are grown for their edible fruits or sweet and juicy seed coverings (arils), the most important being C. papaya.</discussion>
  <discussion>Species, in the traditional sense, ca. 20 (1 in the flora)</discussion>
  
</bio:treatment>