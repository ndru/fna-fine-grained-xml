<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">369</other_info_on_meta>
    <other_info_on_meta type="mention_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">412</other_info_on_meta>
    <other_info_on_meta type="treatment_page">400</other_info_on_meta>
    <other_info_on_meta type="illustration_page">399</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(M. E. Jones ex S. Watson) W. A. Weber" date="1982" rank="species">pulchra</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>51: 370. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species pulchra</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094505</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="M. E. Jones ex S. Watson" date="unknown" rank="species">pulchra</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>22: 468. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species pulchra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>sexual;</text>
      <biological_entity id="o21329" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex woody.</text>
      <biological_entity id="o21330" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 1 per caudex branch, arising from center of leaf tufts, elevated above ground surface on woody base, (1.5–) 3–7.5 dm, densely pubescent proximally, trichomes short-stalked, usually 4–7-rayed, 0.1–0.3 mm, similarly pubescent distally.</text>
      <biological_entity id="o21331" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="per caudex branch" constraintid="o21332" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from center" constraintid="o21333" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character constraint="above surface" constraintid="o21335" is_modifier="false" name="prominence" notes="" src="d0_s4" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="7.5" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o21332" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o21333" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o21334" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
      <biological_entity id="o21335" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o21336" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o21337" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="similarly; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o21333" id="r1486" name="part_of" negation="false" src="d0_s4" to="o21334" />
      <relation from="o21335" id="r1487" name="on" negation="false" src="d0_s4" to="o21336" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade linear to linear-oblanceolate, 1–3 mm wide, margins entire, not ciliate, surfaces densely pubescent, trichomes short-stalked, 4–9-rayed, 0.1–0.3 mm.</text>
      <biological_entity constraint="basal" id="o21338" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21339" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21340" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21341" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21342" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 10–30, often concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o21343" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
      <biological_entity id="o21344" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o21343" id="r1488" modifier="proximally" name="often concealing" negation="false" src="d0_s6" to="o21344" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles absent or, rarely, to 0.5 mm, surfaces of distalmost leaves pubescent.</text>
      <biological_entity constraint="cauline" id="o21345" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o21346" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" value="," value_original="," />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21347" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o21348" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o21347" id="r1489" name="part_of" negation="false" src="d0_s7" to="o21348" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 8–25-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels reflexed, abruptly recurved at base, otherwise straight, 8–16 mm, pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o21349" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="8-25-flowered" value_original="8-25-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o21350" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o21351" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="abruptly" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="otherwise" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21352" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o21349" id="r1490" name="fruiting" negation="false" src="d0_s9" to="o21350" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers descending at anthesis;</text>
      <biological_entity id="o21353" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="descending" value_original="descending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o21354" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals usually purple, rarely white, 9–16 × 2–4 (–5) mm, sparsely pubescent or glabrous (trichomes scattered abaxially);</text>
      <biological_entity id="o21355" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o21356" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits strongly reflexed, often appressed to rachis, sometimes somewhat secund, straight, edges parallel, 3.3–8 cm × 2.5–4 mm;</text>
      <biological_entity id="o21357" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character constraint="to rachis" constraintid="o21358" is_modifier="false" modifier="often" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="sometimes somewhat" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o21358" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o21359" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3.3" from_unit="cm" name="length" src="d0_s14" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves pubescent throughout;</text>
      <biological_entity id="o21360" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 68–106 per ovary;</text>
      <biological_entity id="o21361" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o21362" from="68" name="quantity" src="d0_s16" to="106" />
      </biological_entity>
      <biological_entity id="o21362" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.1–0.3 mm.</text>
      <biological_entity id="o21363" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds biseriate, 1.7–2.8 × 1.5–2.2 mm;</text>
      <biological_entity id="o21364" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s18" value="biseriate" value_original="biseriate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s18" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s18" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 0.25–0.65 mm wide.</text>
      <biological_entity id="o21365" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="width" src="d0_s19" to="0.65" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, gravelly or sandy slopes in chaparral, sagebrush and desert scrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" constraint="in chaparral , sagebrush and desert scrub communities" />
        <character name="habitat" value="gravelly" constraint="in chaparral , sagebrush and desert scrub communities" />
        <character name="habitat" value="sandy slopes" constraint="in chaparral , sagebrush and desert scrub communities" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="desert scrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>80.</number>
  <discussion>Boechera pulchra is a distinctive diploid separated from related sexual species (B. formosa and B. lincolnensis) by abruptly recurved fruiting pedicels and purple flowers, and from apomictic hybrids (e.g., B. xylopoda) by having fruits densely pubescent throughout.</discussion>
  
</bio:treatment>