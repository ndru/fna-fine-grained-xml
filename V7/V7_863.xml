<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="mention_page">544</other_info_on_meta>
    <other_info_on_meta type="treatment_page">543</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="(S. Watson ex Coville) Abrams in L. Abrams and R. S. Ferris" date="1944" rank="species">perenne</taxon_name>
    <place_of_publication>
      <publication_title>in L. Abrams and R. S. Ferris, Ill. Fl. Pacific States</publication_title>
      <place_in_publication>2: 318. 1944</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species perenne</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095024</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="unknown" rank="species">asperum</taxon_name>
    <taxon_name authority="S. Watson ex Coville" date="unknown" rank="variety">perenne</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>7: 70. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erysimum;species asperum;variety perenne;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="(S. Watson ex Coville) Greene" date="unknown" rank="species">perennis</taxon_name>
    <taxon_hierarchy>genus Cheiranthus;species perennis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheirinia</taxon_name>
    <taxon_name authority="(A. Heller) A. Heller" date="unknown" rank="species">nevadensis</taxon_name>
    <taxon_hierarchy>genus Cheirinia;species nevadensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="(Douglas ex Hooker) Greene" date="unknown" rank="species">capitatum</taxon_name>
    <taxon_name authority="(S. Watson ex Coville) R. J. Davis" date="unknown" rank="variety">perenne</taxon_name>
    <taxon_hierarchy>genus Erysimum;species capitatum;variety perenne;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="A. Heller" date="unknown" rank="species">nevadense</taxon_name>
    <taxon_hierarchy>genus Erysimum;species nevadense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or, rarely, biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex slender).</text>
      <biological_entity id="o35268" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes of leaves 2–5-rayed.</text>
      <biological_entity id="o35270" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o35271" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o35270" id="r2365" name="part_of" negation="false" src="d0_s2" to="o35271" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched or branched (few to several) basally, 0.4–6.5 dm.</text>
      <biological_entity id="o35272" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s3" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade spatulate to broadly oblanceolate, 2.5–7 cm × 3–10 mm, base attenuate, margins dentate or subentire, apex often obtuse, (surfaces pubescent adaxially, trichomes 2 or 3–5-rayed).</text>
      <biological_entity constraint="basal" id="o35273" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o35274" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="broadly oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35275" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o35276" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o35277" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o35278" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade margins often entire.</text>
      <biological_entity constraint="blade" id="o35279" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o35281" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels divaricate-ascending, slender, narrower than fruit, 4–12 mm.</text>
      <biological_entity id="o35280" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o35281" is_modifier="false" modifier="considerably" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o35283" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o35282" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o35283" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <relation from="o35280" id="r2366" name="fruiting" negation="false" src="d0_s8" to="o35282" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals linear-oblong to oblong, 8–12 mm, lateral pair saccate basally;</text>
      <biological_entity id="o35284" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35285" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, broadly obovate to suborbicular, 15–22 × 3.5–6 mm, claw 8–14 mm, apex rounded;</text>
      <biological_entity id="o35286" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o35287" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s10" to="suborbicular" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35288" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35289" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filaments 7–14 mm;</text>
      <biological_entity id="o35290" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o35291" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 3–4 mm.</text>
      <biological_entity id="o35292" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o35293" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits erect to ascending, narrowly linear, straight, torulose, 3.8–14 cm × 1.2–3 mm, latiseptate, not striped;</text>
      <biological_entity id="o35294" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="ascending" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s13" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3.8" from_unit="cm" name="length" src="d0_s13" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s13" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves with prominent midvein, pubescent outside, trichomes 2 or 3 (or 4) -rayed, glabrous inside;</text>
      <biological_entity id="o35295" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o35296" name="midvein" name_original="midvein" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o35297" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character name="quantity" src="d0_s14" value="3-rayed" value_original="3-rayed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o35295" id="r2367" name="with" negation="false" src="d0_s14" to="o35296" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 26–44 per ovary;</text>
      <biological_entity id="o35298" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o35299" from="26" name="quantity" src="d0_s15" to="44" />
      </biological_entity>
      <biological_entity id="o35299" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style cylindrical, slender, (1.5–) 2–5.5 mm, sparsely pubescent;</text>
      <biological_entity id="o35300" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma subentire to slightly 2-lobed, lobes as long as wide.</text>
      <biological_entity id="o35301" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s17" to="slightly 2-lobed" />
      </biological_entity>
      <biological_entity id="o35302" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ovoid, 2–3.4 × 1–2 mm;</text>
    </statement>
    <statement id="d0_s19">
      <text>not winged or, rarely, winged distally.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity id="o35303" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s18" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s18" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s19" value="winged" value_original="winged" />
        <character name="architecture" src="d0_s19" value="," value_original="," />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35304" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine fellfields, decomposing marble, gravelly ground and knolls, rocky slopes, talus, granitic sand</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine fellfields" />
        <character name="habitat" value="decomposing marble" />
        <character name="habitat" value="gravelly ground" />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="granitic sand" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <discussion>Erysimum perenne is a high alpine species of the western sierras in California from Fresno, Inyo, and Madera counties northward into Plumas, Siskiyou, and Trinity counties. Its range in Nevada appears to be restricted to Douglas and Washoe counties.</discussion>
  <discussion>The limits of Erysimum perenne have been controversial, and it is with some hesitation that I recognize it as a species. G. B. Rossbach (1958) accepted it as a distinct species, R. A. Price (1993) transferred it (invalidly) to a subspecies of E. capitatum, R. C. Rollins (1993) treated it as a variety of E. capitatum, and N. H. Holmgren (2005b) treated the name as a synonym of E. capitatum. It is readily distinguished from E. capitatum by having torulose (versus not torulose) and flattened (versus 4-angled or flattened) fruits, slender (versus stout or, rarely, slender) and longer styles (1.5–)2–5.5 mm (versus 0.2–2.5(–3) mm), and yellow (versus orange to, rarely, yellow) petals. Where the two species are allopatric, they remain consistently distinct, but at lower elevations, where their ranges overlap, the distinction becomes blurred. In such areas of overlap, one finds fruit variation ranging from distinctly torulose to non-torulose, as well as continuity in the other characters above.</discussion>
  
</bio:treatment>