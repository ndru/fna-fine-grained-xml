<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">314</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Munz &amp; I. M. Johnston" date="1929" rank="species">jaegeri</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>56: 164. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species jaegeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094699</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(densely cespitose);</text>
      <biological_entity id="o19088" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (densely covered with persistent leaves, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o19089" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.05–) 0.15–0.5 (–0.6) dm, hirsute throughout, trichomes simple, 0.1–0.8 mm, and 2–4-rayed, 0.0.5–0.4 mm.</text>
      <biological_entity id="o19090" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.15" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.6" to_unit="dm" />
        <character char_type="range_value" from="0.15" from_unit="dm" name="some_measurement" src="d0_s4" to="0.5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o19091" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="basal" id="o19092" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate to ovate, 0.4–1.5 cm × 1.5–3.5 mm, margins entire, (ciliate, trichomes simple, 0.3–1.1 mm), surfaces pubescent abaxially with stalked, (2–) 4–6-rayed trichomes, 0.1–5 mm, (midvein obscure), adaxially glabrous proximally, sparsely pubescent distally with mostly simple trichomes.</text>
      <biological_entity id="o19093" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19094" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19095" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o19096" is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="adaxially; proximally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character constraint="with trichomes" constraintid="o19097" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19096" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o19097" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="mostly" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o19098" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes 3–12 (–18) -flowered, ebracteate, slightly elongated in fruit;</text>
      <biological_entity id="o19099" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-12(-18)-flowered" value_original="3-12(-18)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o19100" is_modifier="false" modifier="slightly" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o19100" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, hirsute as stem.</text>
      <biological_entity id="o19102" name="stem" name_original="stem" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels ascending, straight, 2–6 (–8) mm, hirsute as stem.</text>
      <biological_entity id="o19101" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o19102" is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o19103" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o19104" name="stem" name_original="stem" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o19101" id="r1303" name="fruiting" negation="false" src="d0_s12" to="o19103" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals oblong, 2.5–3 mm, hirsute, (trichomes simple and stalked, 2–4-rayed);</text>
      <biological_entity id="o19105" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19106" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals white, spatulate, 4.5–6 × 1.5–2 mm;</text>
      <biological_entity id="o19107" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19108" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s14" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong, 0.8–1 mm.</text>
      <biological_entity id="o19109" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19110" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits ovate to elliptic, plane, slightly flattened, 4–8 (–11) × 2.5–4.5 mm;</text>
      <biological_entity id="o19111" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s16" to="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s16" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves pubescent, trichomes stalked (2–) 4-rayed, 0.05–0.5 mm;</text>
      <biological_entity id="o19112" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19113" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o19114" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o19115" from="8" name="quantity" src="d0_s18" to="16" />
      </biological_entity>
      <biological_entity id="o19115" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style (1.2–) 2–4 (–4.5) mm.</text>
      <biological_entity id="o19116" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s19" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds oblong, 1.4–2 × 0.9–1 mm. 2n = 54.</text>
      <biological_entity id="o19117" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s20" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s20" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19118" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone outcrops and gravelly soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="gravelly soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2900-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="2900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>51.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba jaegeri is superficially similar to D. hitchcockii, a narrow endemic of the Lost River Range in central Idaho. Both species are cespitose perennials with relatively large, white flowers, and a chromosome number (2n = 54) otherwise unknown in Draba (M. D. Windham 2004). Draba jaegeri is readily distinguished from D. hitchcockii by having fruits pubescent with (2–)4-rayed trichomes, styles (1.2–)2–4(–4.5) mm, fruiting pedicels 2–6(–8) mm, and spatulate petals 1.5–2 mm wide. By contrast, D. hitchcockii has fruits pubescent with mostly simple and 2-rayed trichomes (with 3- or 4-rayed ones), styles (0.8–)1–1.7(–2) mm, fruiting pedicels (2–)4–13(–18) mm, and obovate petals 2–3.5 mm wide. Draba jaegeri is known only from the Charleston Mountains in Clark County.</discussion>
  
</bio:treatment>