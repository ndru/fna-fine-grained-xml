<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">609</other_info_on_meta>
    <other_info_on_meta type="illustration_page">606</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Hooker &amp; Harvey" date="1845" rank="genus">lyrocarpa</taxon_name>
    <taxon_name authority="Hooker &amp; Harvey" date="1845" rank="species">coulteri</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>4: 76, plate 4. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus lyrocarpa;species coulteri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095155</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lyrocarpa</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">coulteri</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">apiculata</taxon_name>
    <taxon_hierarchy>genus Lyrocarpa;species coulteri;variety apiculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lyrocarpa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">coulteri</taxon_name>
    <taxon_name authority="(S. Watson) Rollins" date="unknown" rank="variety">palmeri</taxon_name>
    <taxon_hierarchy>genus Lyrocarpa;species coulteri;variety palmeri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lyrocarpa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palmeri</taxon_name>
    <taxon_hierarchy>genus Lyrocarpa;species palmeri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants suffrutescent;</text>
    </statement>
    <statement id="d0_s1">
      <text>moderately to densely pubescent throughout.</text>
      <biological_entity id="o28140" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suffrutescent" value_original="suffrutescent" />
        <character is_modifier="false" modifier="moderately to densely; throughout" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems straw-colored, woody (at least proximally), many-branched, (2–) 3–9 (–11) dm.</text>
      <biological_entity id="o28141" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="many-branched" value_original="many-branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="11" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="9" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves: petiole 0.5–2.5 (–4) cm (shorter distally);</text>
      <biological_entity constraint="cauline" id="o28142" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28143" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades lanceolate, oblanceolate, or ovate in outline, (1–) 2–9 (–12) cm × (5–) 10–50 (–70) mm (smaller, less divided distally), base attenuate to cuneate, or truncate to hastate, margins usually pinnatisect to pinnatifid, or runcinate to dentate, rarely repand, teeth and lobes apiculate or not, sparsely to densely pubescent, nearly canescent.</text>
      <biological_entity constraint="cauline" id="o28144" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28145" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character constraint="in outline" constraintid="o28146" is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o28146" name="outline" name_original="outline" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" notes="alterIDs:o28146" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o28146" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" notes="alterIDs:o28146" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" notes="alterIDs:o28146" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" notes="alterIDs:o28146" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" notes="alterIDs:o28146" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28147" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s4" to="cuneate or truncate" />
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s4" to="cuneate or truncate" />
      </biological_entity>
      <biological_entity id="o28148" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually pinnatisect" name="shape" src="d0_s4" to="pinnatifid or runcinate" />
        <character char_type="range_value" from="usually pinnatisect" name="shape" src="d0_s4" to="pinnatifid or runcinate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels divaricate to ascending or, rarely, reflexed, 2–7 (–11) mm.</text>
      <biological_entity id="o28149" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character name="architecture_or_shape" src="d0_s4" value="not" value_original="not" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28150" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28151" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o28149" id="r1894" name="fruiting" negation="false" src="d0_s5" to="o28151" />
      <relation from="o28150" id="r1895" name="fruiting" negation="false" src="d0_s5" to="o28151" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals (6–) 8–10 (–11) × 1–1.5 mm;</text>
      <biological_entity id="o28152" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o28153" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s6" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals (twisted with age), (13–) 16–25 × 1–3 mm, claw 7–12 mm;</text>
      <biological_entity id="o28154" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" notes="" src="d0_s7" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" notes="" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28155" name="petal" name_original="petals" src="d0_s7" type="structure" />
      <biological_entity id="o28156" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 4–6 mm;</text>
      <biological_entity id="o28157" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o28158" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 2.7–3.5 mm.</text>
      <biological_entity id="o28159" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28160" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits (8–) 10–25 × (8–) 10–16 mm, base obtuse, apex deeply emarginate, (sinus to 8 mm deep), retuse, or truncate;</text>
      <biological_entity id="o28161" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28162" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o28163" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="punct" value_original="punct" />
        <character is_modifier="false" name="shape" src="d0_s10" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style obsolete or to 0.5 mm.</text>
      <biological_entity id="o28164" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="obsolete" value_original="obsolete" />
        <character name="prominence" src="d0_s11" value="0-0.5 mm" value_original="0-0.5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2–3 mm diam. 2n = 36, 42.</text>
      <biological_entity id="o28165" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28166" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Sep-mid Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Apr" from="late Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly or rocky slopes, desert washes, dry streambeds, sandy plains, shady ravines, granitic hills, mesa slopes, thorn scrub, stony ridges, gravelly arroyo beds, in open sun or under shrubs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" />
        <character name="habitat" value="desert washes" />
        <character name="habitat" value="dry streambeds" />
        <character name="habitat" value="sandy plains" />
        <character name="habitat" value="shady ravines" />
        <character name="habitat" value="granitic hills" />
        <character name="habitat" value="mesa slopes" />
        <character name="habitat" value="thorn scrub" />
        <character name="habitat" value="stony ridges" />
        <character name="habitat" value="gravelly arroyo beds" />
        <character name="habitat" value="open sun" modifier="in" />
        <character name="habitat" value="shrubs" modifier="or under" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; nw Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="nw Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Lyrocarpa coulteri, which is known from Pima and Yuma counties in Arizona and Imperial and San Diego counties in California, is extremely variable in nearly every aspect. The leaves range from relatively large, thin, and sparsely pubescent to smaller, thicker, and almost canescent; their shapes and margins are highly variable also. The flowers are strongly scented at night and appear to be moth-pollinated; their color varies a great deal. Fruit shape and size depend on the original number of ovules and how many of them mature into seeds, and one sometimes finds on the same plant fruits ranging from broadly obcordate to panduriform. Finally, the plants occupy a wide array of habitats; they can be tall, slender, and broad- and thin-leaved when grown in the shade of larger shrubs, whereas those growing in direct sunlight can be shorter, stout, and small- and thick-leaved.</discussion>
  <discussion>With the above in mind, it is easy to understand why the two varieties recognized by R. C. Rollins (1941b, 1993) do not merit recognition. Variety apiculata was based on a fragmentary specimen with narrow petals and broad leaves apiculate at lobe apices and teeth. However, these two characters are uncorrelated. Similarly, var. palmeri was distinguished from var. coulteri by having broadly obcordate versus pandurifom fruits and 6–10 ovules, instead of 8–16 ovules, per ovary. As indicated above, fruit shape largely depends not on the number of ovules but on how many of them mature into seeds.</discussion>
  
</bio:treatment>