<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="treatment_page">375</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="Windham &amp; Al-Shehbaz" date="2007" rank="species">elkoensis</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 263. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species elkoensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094561</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>apomictic;</text>
      <biological_entity id="o25331" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex often woody.</text>
      <biological_entity id="o25332" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems 1–5 per caudex branch, arising from center or margin of rosette near ground surface, 1–3 dm, sparsely pubescent proximally, trichomes subsessile, 2–5-rayed, 0.1–0.4 mm, glabrous or sparsely pubescent distally.</text>
      <biological_entity id="o25333" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per caudex branch" constraintid="o25334" from="1" name="quantity" src="d0_s4" to="5" />
        <character constraint="from margin" constraintid="o25336" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o25334" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o25335" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o25336" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o25337" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o25338" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o25339" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o25340" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o25336" id="r1729" name="part_of" negation="false" src="d0_s4" to="o25337" />
      <relation from="o25336" id="r1730" name="near" negation="false" src="d0_s4" to="o25338" />
      <relation from="o25336" id="r1731" name="near" negation="false" src="d0_s4" to="o25339" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade narrowly oblanceolate, 2–6 mm wide, margins entire, ciliate at petiole base, trichomes (simple), to 0.4 mm, surfaces moderately pubescent, trichomes sessile or subsessile, 2–5-rayed, 0.1–0.4 mm.</text>
      <biological_entity constraint="basal" id="o25341" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o25342" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25343" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="at petiole base" constraintid="o25344" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o25344" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o25345" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25346" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25347" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 3–6, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o25348" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o25349" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o25348" id="r1732" name="concealing" negation="true" src="d0_s6" to="o25349" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles absent or 0.2–0.5 mm, surfaces of distalmost leaves glabrous, or sparsely pubescent along margins.</text>
      <biological_entity constraint="cauline" id="o25350" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o25351" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" value="0.2-0.5 mm" value_original="0.2-0.5 mm" />
      </biological_entity>
      <biological_entity id="o25352" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="along margins" constraintid="o25354" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o25353" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o25354" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <relation from="o25352" id="r1733" name="part_of" negation="false" src="d0_s7" to="o25353" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 4–11-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels erect to ascending, straight, 4–8 mm, usually glabrous, rarely sparsely pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o25355" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="4-11-flowered" value_original="4-11-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o25356" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o25357" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="ascending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o25358" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o25355" id="r1734" name="fruiting" negation="false" src="d0_s9" to="o25356" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers erect to ascending at anthesis;</text>
      <biological_entity id="o25359" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="at anthesis" from="erect" name="orientation" src="d0_s10" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals glabrous;</text>
      <biological_entity id="o25360" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals usually lavender, rarely whitish, 4–7 × 1.2–2 mm, glabrous;</text>
      <biological_entity id="o25361" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_odor" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen spheroid.</text>
      <biological_entity id="o25362" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits erect to ascending, often appressed to rachis, not secund, straight, edges often slightly undulate (not parallel), 3–7 cm × 2–3 mm;</text>
      <biological_entity id="o25363" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s14" to="ascending" />
        <character constraint="to rachis" constraintid="o25364" is_modifier="false" modifier="often" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o25364" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o25365" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="often slightly" name="shape" src="d0_s14" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s14" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o25366" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 26–42 per ovary;</text>
      <biological_entity id="o25367" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o25368" from="26" name="quantity" src="d0_s16" to="42" />
      </biological_entity>
      <biological_entity id="o25368" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.05–0.3 mm.</text>
      <biological_entity id="o25369" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds uniseriate, 2.5–3.5 × 1.8–2.5 mm;</text>
      <biological_entity id="o25370" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s18" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s18" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 0.4–0.9 mm wide.</text>
      <biological_entity id="o25371" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s19" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly soil among rocks in open forests and subalpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly soil" constraint="among rocks in open forests and subalpine meadows" />
        <character name="habitat" value="rocks" constraint="in open forests and subalpine meadows" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="subalpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion>Morphological evidence suggests that Boechera elkoensis is an apomictic species that arose through hybridization between B. platysperma and B. stricta (see M. D. Windham and I. A. Al-Shehbaz 2007 for detailed comparison).</discussion>
  
</bio:treatment>