<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">729</other_info_on_meta>
    <other_info_on_meta type="treatment_page">731</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="1895" rank="species">flexuosum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 175. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species flexuosum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094953</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex woody, covered with persistent petiolar remains);</text>
    </statement>
    <statement id="d0_s2">
      <text>somewhat glaucous, glabrous throughout.</text>
      <biological_entity id="o31425" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems (often subdecumbent), branched basally, (flexuous), 1.5–5.6 (–8.5) dm.</text>
      <biological_entity id="o31426" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="5.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="8.5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="5.6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: petiole 1–6.5 (–13) cm;</text>
      <biological_entity constraint="basal" id="o31427" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31428" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade often lanceolate, sometimes oblong or oblanceolate, (2–) 3.5–16.5 (–20.5) cm × (5–) 10–25 (–45) mm, margins entire.</text>
      <biological_entity constraint="basal" id="o31429" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o31430" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s5" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="20.5" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s5" to="16.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31431" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (ascending);</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o31432" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade lanceolate to linear, 1–7 (–11) cm × 2–7 (–14) mm, (base sagittate to somewhat amplexicaul), margins entire.</text>
      <biological_entity id="o31433" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="11" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="7" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31434" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (few-flowered, corymbose), elongated in fruit, (flower buds oblong).</text>
      <biological_entity id="o31436" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels usually horizontal to divaricate, rarely divaricate-ascending, straight or slightly curved upward, slender, (2.5–) 4–9 (–16) mm, slightly flattened at base.</text>
      <biological_entity id="o31435" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o31436" is_modifier="false" name="length" src="d0_s9" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o31437" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o31438" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o31435" id="r2103" name="fruiting" negation="false" src="d0_s10" to="o31437" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect, oblong, 3–4 (–4.5) × 1–1.5 (–1.7) mm;</text>
      <biological_entity id="o31439" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o31440" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals lavender or white, often spatulate, sometimes oblanceolate or obovate, 6–9 (–10) × (1.5–) 2–3 (–3.5) mm, margins not crisped, claw strongly differentiated from blade, (slender, 2–3.5 (–4) mm, narrowest at base);</text>
      <biological_entity id="o31441" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31442" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31443" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o31444" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o31445" is_modifier="false" modifier="strongly" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="size" notes="" src="d0_s12" value="slender" value_original="slender" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character constraint="at base" constraintid="o31446" is_modifier="false" name="width" src="d0_s12" value="narrowest" value_original="narrowest" />
      </biological_entity>
      <biological_entity id="o31445" name="blade" name_original="blade" src="d0_s12" type="structure" />
      <biological_entity id="o31446" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>nectar glands lateral, median glands absent;</text>
      <biological_entity id="o31447" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="nectar" id="o31448" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity constraint="median" id="o31449" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments tetradynamous, median pairs 3–4 (–5) mm, lateral pair 2–3.5 (–4) mm;</text>
      <biological_entity id="o31450" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o31451" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="tetradynamous" value_original="tetradynamous" />
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers included, oblong, 1–2 (–2.5) mm, not circinately coiled;</text>
      <biological_entity id="o31452" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o31453" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="included" value_original="included" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not circinately" name="architecture" src="d0_s15" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore (stout), 0.5–1 mm.</text>
      <biological_entity id="o31454" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31455" name="gynophore" name_original="gynophore" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits erect to ascending, torulose, slightly incurved or straight, terete, 1–2.5 (–4.2) cm × 0.8–1 (–1.5) mm;</text>
      <biological_entity id="o31456" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s17" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s17" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s17" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s17" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 12–30 per ovary;</text>
      <biological_entity id="o31457" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31458" from="12" name="quantity" src="d0_s18" to="30" />
      </biological_entity>
      <biological_entity id="o31458" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style cylindrical, (0.3–) 1–2 (–3) mm.</text>
      <biological_entity id="o31459" name="style" name_original="style" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds (1–) 1.3–1.5 × 0.5–8 (–1) mm.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 26.</text>
      <biological_entity id="o31460" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s20" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s20" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="average_width" src="d0_s20" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s20" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31461" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Often tangled among woody shrubs in moderately to strongly alkaline sandy loam or clay, open deserts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woody shrubs" modifier="often tangled among" constraint="in moderately to strongly alkaline sandy loam or clay , open deserts" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="to alkaline sandy loam" modifier="moderately strongly" />
        <character name="habitat" value="open deserts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  
</bio:treatment>