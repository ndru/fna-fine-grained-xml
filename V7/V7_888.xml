<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">554</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">euclidieae</taxon_name>
    <taxon_name authority="Boissier" date="1854" rank="genus">strigosella</taxon_name>
    <taxon_name authority="(Linnaeus) Botschantzev" date="1972" rank="species">africana</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>57: 1038. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe euclidieae;genus strigosella;species africana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242350689</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hesperis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">africana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 663. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hesperis;species africana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malcolmia</taxon_name>
    <taxon_name authority="(Linnaeus) W. T. Aiton" date="unknown" rank="species">africana</taxon_name>
    <taxon_hierarchy>genus Malcolmia;species africana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually sparsely to densely pubescent, rarely glabrescent, trichomes short-stalked, forked or subdendritic, these, sometimes, with simple, subsetiform ones.</text>
      <biological_entity id="o39247" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o39248" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="shape" src="d0_s0" value="forked" value_original="forked" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="subdendritic" value_original="subdendritic" />
      </biological_entity>
      <biological_entity id="o39249" name="one" name_original="ones" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="true" name="shape" src="d0_s0" value="subsetiform" value_original="subsetiform" />
      </biological_entity>
      <relation from="o39248" id="r2653" modifier="sometimes" name="with" negation="false" src="d0_s0" to="o39249" />
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or branched proximally, (0.4–) 1.5–3 (–5) dm, pubescent.</text>
      <biological_entity id="o39250" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves soon withered.</text>
      <biological_entity constraint="basal" id="o39251" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="soon" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves petiolate or (distal) subsessile;</text>
      <biological_entity constraint="cauline" id="o39252" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0.1–) 0.6–2 (–3) cm;</text>
      <biological_entity id="o39253" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="0.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, oblanceolate, or oblong, (0.5–) 1.5–6 (–10) cm × (3–) 10–25 (–35) mm (smaller distally), base cuneate, apex acute.</text>
      <biological_entity id="o39254" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39255" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o39256" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes: rachis straight or slightly flexuous.</text>
      <biological_entity id="o39257" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels 0.5–2 (–4) mm.</text>
      <biological_entity id="o39258" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39259" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o39258" id="r2654" name="fruiting" negation="false" src="d0_s7" to="o39259" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals sometimes persistent, (3.5–) 4–5 × 0.5–0.7 mm;</text>
      <biological_entity id="o39260" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o39261" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals narrowly oblanceolate, (6.5–) 8–10 (–12) × 1–2 mm;</text>
      <biological_entity id="o39262" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o39263" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments distinct, 2.5–5 mm;</text>
      <biological_entity id="o39264" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o39265" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers narrowly oblong, 0.9–1.1 mm.</text>
      <biological_entity id="o39266" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o39267" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits divaricate-ascending, straight, (2.5–) 3.5–5.5 (–7) cm × 1–1.3 mm;</text>
      <biological_entity id="o39268" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s12" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s12" to="7" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s12" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves usually pubescent, rarely glabrous, trichomes coarse and forked, these mixed with smaller, forked, subdendritic, or simple, subsetiform ones;</text>
      <biological_entity id="o39269" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o39270" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="shape" src="d0_s13" value="forked" value_original="forked" />
        <character constraint="with ones" constraintid="o39271" is_modifier="false" name="arrangement" src="d0_s13" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o39271" name="one" name_original="ones" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="shape" src="d0_s13" value="forked" value_original="forked" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="subdendritic" value_original="subdendritic" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="simple" value_original="simple" />
        <character is_modifier="true" name="shape" src="d0_s13" value="subsetiform" value_original="subsetiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma to 1 mm.</text>
      <biological_entity id="o39272" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1–1.2 × 0.5–0.6 mm. 2n = 14, 28.</text>
      <biological_entity id="o39273" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s15" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39274" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, disturbed areas, roadsides, deserts, sandy flats, vacant lots, sagebrush and greasewood areas, grasslands, railroad tracks, shale outcrops, alkaline flats, juniper woodlands, plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="deserts" />
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="vacant lots" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="greasewood areas" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="railroad tracks" />
        <character name="habitat" value="shale outcrops" />
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="juniper woodlands" />
        <character name="habitat" value="plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ariz., Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.; Europe; Asia; n Africa; introduced also in South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in South America (Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>