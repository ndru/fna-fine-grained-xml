<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">168</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">moringaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">MORINGA</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Pl.</publication_title>
      <place_in_publication>2: 318, 579. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family moringaceae;genus MORINGA</taxon_hierarchy>
    <other_info_on_name type="etymology">Tamil murungai, twisted pod, alluding to young fruit</other_info_on_name>
    <other_info_on_name type="fna_id">121194</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs, [massive pachycauls, baobab-like with water-storing trunk], slender-trunked.</text>
      <biological_entity id="o1835" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="slender-trunked" value_original="slender-trunked" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="slender-trunked" value_original="slender-trunked" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules with nectaries at growing tip;</text>
      <biological_entity id="o1837" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1838" name="stipule" name_original="stipules" src="d0_s1" type="structure" />
      <biological_entity id="o1839" name="nectary" name_original="nectaries" src="d0_s1" type="structure" />
      <biological_entity id="o1840" name="tip" name_original="tip" src="d0_s1" type="structure" />
      <relation from="o1838" id="r165" name="with" negation="false" src="d0_s1" to="o1839" />
      <relation from="o1839" id="r166" name="at" negation="false" src="d0_s1" to="o1840" />
    </statement>
    <statement id="d0_s2">
      <text>rachis articulation with stalked glands;</text>
      <biological_entity id="o1841" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="rachis" id="o1842" name="articulation" name_original="articulation" src="d0_s2" type="structure" />
      <biological_entity id="o1843" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o1842" id="r167" name="with" negation="false" src="d0_s2" to="o1843" />
    </statement>
    <statement id="d0_s3">
      <text>[1-pinnate] (2–) 3–4 (–5) -pinnate;</text>
      <biological_entity id="o1844" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="(2-)3-4(-5)-pinnate" value_original="(2-)3-4(-5)-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflet blade membranous [subcoriaceous], [lanceolate, oblanceolate, linear] round or oval, venation sometimes conspicuous abaxially, apex glandular, surfaces [pubescent] puberulent or glabrous.</text>
      <biological_entity id="o1845" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="leaflet" id="o1846" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oval" value_original="oval" />
        <character is_modifier="false" modifier="sometimes; abaxially" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o1847" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o1848" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: parts usually with hairs forming a barrier distal to the nectariferous hypanthium;</text>
      <biological_entity id="o1849" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o1850" name="part" name_original="parts" src="d0_s5" type="structure">
        <character constraint="to nectariferous hypanthium" constraintid="o1853" is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o1851" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o1852" name="barrier" name_original="barrier" src="d0_s5" type="structure" />
      <biological_entity constraint="nectariferous" id="o1853" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure" />
      <relation from="o1850" id="r168" name="with" negation="false" src="d0_s5" to="o1851" />
      <relation from="o1851" id="r169" name="forming a" negation="false" src="d0_s5" to="o1852" />
    </statement>
    <statement id="d0_s6">
      <text>1 sporangium initiated in anther ontogeny.</text>
      <biological_entity id="o1854" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1855" name="sporangium" name_original="sporangium" src="d0_s6" type="structure" />
      <biological_entity id="o1856" name="anther" name_original="anther" src="d0_s6" type="structure" />
      <relation from="o1855" id="r170" name="initiated in" negation="false" src="d0_s6" to="o1856" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules 2-valved, often constricted between seeds.</text>
      <biological_entity id="o1857" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-valved" value_original="2-valved" />
        <character constraint="between seeds" constraintid="o1858" is_modifier="false" modifier="often" name="size" src="d0_s7" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o1858" name="seed" name_original="seeds" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds [1] 2–3 cm, winged [not winged], sometimes with spongy seed-coat, shed by gravity.</text>
      <biological_entity id="o1860" name="seed-coat" name_original="seed-coat" src="d0_s8" type="structure">
        <character is_modifier="true" name="texture" src="d0_s8" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o1861" name="gravity" name_original="gravity" src="d0_s8" type="structure" />
      <relation from="o1859" id="r171" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o1860" />
      <relation from="o1859" id="r172" name="shed by" negation="false" src="d0_s8" to="o1861" />
    </statement>
    <statement id="d0_s9">
      <text>x = 11.</text>
      <biological_entity id="o1859" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o1862" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia (Bangladesh, India, Oman, Pakistan, Saudi Arabia, Yemen), sw, ne Africa, Indian Ocean Islands (Madagascar); introduced also pantropically.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia (Bangladesh)" establishment_means="introduced" />
        <character name="distribution" value="Asia (India)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Oman)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Pakistan)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Saudi Arabia)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Yemen)" establishment_means="introduced" />
        <character name="distribution" value="sw" establishment_means="introduced" />
        <character name="distribution" value="ne Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="also pantropically" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Species 13 (1 in the flora).</discussion>
  <discussion>Little is known about breeding systems in Moringa; M. longituba Engler appears incapable of self-pollination, and flowers with sterile anthers have been reported in M. concanensis Nimmo ex Dalzell &amp; Gibson. All species are used medicinally locally; M. stenopetala (Baker f.) Cufodontis is used as a leaf vegetable in northwestern Kenya and southwestern Ethiopia.</discussion>
  
</bio:treatment>