<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">215</other_info_on_meta>
    <other_info_on_meta type="illustration_page">211</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="genus">oxystylis</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="species">lutea</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont, Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>313. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus oxystylis;species lutea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220009752</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 (–150) cm.</text>
      <biological_entity id="o15957" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules crinkled;</text>
      <biological_entity id="o15958" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15959" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="crinkled" value_original="crinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1.4–7 cm;</text>
      <biological_entity id="o15960" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15961" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolules 2–4 mm;</text>
      <biological_entity id="o15962" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15963" name="petiolule" name_original="petiolules" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflet blade elliptic to elliptic-obovate, 1.5–5 × 0.5–2.5 cm, margins entire, apex obtuse.</text>
      <biological_entity id="o15964" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="leaflet" id="o15965" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="elliptic-obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15966" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15967" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes dense, ca. 1 cm (2–3 cm in fruit);</text>
      <biological_entity id="o15968" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character name="some_measurement" src="d0_s5" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts unifoliate, 2–4 mm.</text>
      <biological_entity id="o15969" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels sparsely glandular, 1–2 mm.</text>
      <biological_entity id="o15970" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals green, lanceolate, 1.4–1.7 × 0.2–0.3 mm, margins entire, glabrous;</text>
      <biological_entity id="o15971" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15972" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s8" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15973" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals yellow, oblong to ovate, 2–4 × 1–1.6 mm;</text>
      <biological_entity id="o15974" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15975" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens yellow, 4–4.5 mm;</text>
      <biological_entity id="o15976" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15977" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1–1.2 mm;</text>
      <biological_entity id="o15978" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15979" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>gynophore 0.1–0.2 mm in fruit;</text>
      <biological_entity id="o15980" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15981" name="gynophore" name_original="gynophore" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o15982" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15982" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 0.3 mm;</text>
      <biological_entity id="o15983" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15984" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style 0.8–1 mm.</text>
      <biological_entity id="o15985" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15986" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Schizocarps 1 × 0.6 mm, smooth.</text>
      <biological_entity id="o15987" name="schizocarp" name_original="schizocarps" src="d0_s15" type="structure">
        <character name="length" src="d0_s15" unit="mm" value="1" value_original="1" />
        <character name="width" src="d0_s15" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 0.5 × 0.3 mm. 2n = 20, 40.</text>
      <biological_entity id="o15988" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character name="length" src="d0_s16" unit="mm" value="0.5" value_original="0.5" />
        <character name="width" src="d0_s16" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15989" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy, gravelly desert flats, scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="dry" />
        <character name="habitat" value="gravelly desert flats" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  
</bio:treatment>