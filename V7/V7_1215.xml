<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">703</other_info_on_meta>
    <other_info_on_meta type="mention_page">723</other_info_on_meta>
    <other_info_on_meta type="treatment_page">706</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="F. W. Hoffman" date="1952" rank="species">brachiatus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>11: 230, fig. 5. 1952</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species brachiatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">brachiatus</taxon_name>
    <taxon_name authority="R. W. Dolan &amp; LaPré" date="unknown" rank="subspecies">hoffmanii</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species brachiatus;subspecies hoffmanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(somewhat glaucous), glabrous throughout.</text>
      <biological_entity id="o25974" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually branched basally, rarely unbranched, 1.4–5 (–6) dm.</text>
      <biological_entity id="o25975" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually; basally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="dm" />
        <character char_type="range_value" from="1.4" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o25976" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (fleshy, mottled), broadly obovate to suborbicular, 1.5–4 cm, margins coarsely dentate.</text>
      <biological_entity id="o25977" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s6" to="suborbicular" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25978" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade ovate to cordate, 0.7–3.7cm × 3–15 mm (smaller distally), base auriculate to amplexicaul, margins serrate-dentate or entire.</text>
      <biological_entity constraint="cauline" id="o25979" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o25980" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="cordate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s7" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25981" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="auriculate to amplexicaul" value_original="auriculate to amplexicaul" />
      </biological_entity>
      <biological_entity id="o25982" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrate-dentate" value_original="serrate-dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (lax, sometimes secund).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 1–2 mm.</text>
      <biological_entity id="o25983" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25984" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o25983" id="r1760" name="fruiting" negation="false" src="d0_s9" to="o25984" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx urceolate;</text>
      <biological_entity id="o25985" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25986" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (erect), rose-purple (to yellowish at base), (ovate), 5–7 mm, keeled, (apex recurved);</text>
      <biological_entity id="o25987" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="rose-purple" value_original="rose-purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o25988" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals whitish, (abaxial pair with purple spot, adaxial pair faintly purple-veined), 7–10 mm, blade 1.5–3 × 1–1.5 mm, margins not crisped, claw 5–7 mm, as wide as or wider than blade;</text>
      <biological_entity id="o25989" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o25990" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25991" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25992" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o25993" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25995" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character is_modifier="true" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o25994" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character is_modifier="true" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <relation from="o25993" id="r1761" name="as wide as" negation="false" src="d0_s12" to="o25995" />
    </statement>
    <statement id="d0_s13">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o25996" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o25997" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o25998" name="pair" name_original="pairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o25997" id="r1762" name="in" negation="false" src="d0_s13" to="o25998" />
    </statement>
    <statement id="d0_s14">
      <text>filaments: abaxial pair (connate to middle), 4–7 mm, lateral pair 2–4 mm, adaxial pair (completely connate, usually recurved), 8–10 mm;</text>
      <biological_entity id="o25999" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial" id="o26000" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26001" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers: abaxial and lateral pairs fertile, 2–2.5 mm, adaxial pair sterile, 0.7–1.2 mm;</text>
      <biological_entity id="o26002" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26003" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.3–0.7 mm.</text>
      <biological_entity id="o26004" name="anther" name_original="anthers" src="d0_s16" type="structure" />
      <biological_entity id="o26005" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits divaricate-ascending, torulose, nearly straight, flattened, 4–6 cm × 1–1.3 mm;</text>
      <biological_entity id="o26006" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s17" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s17" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with obscure midvein;</text>
      <biological_entity id="o26007" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o26008" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o26007" id="r1763" name="with" negation="false" src="d0_s18" to="o26008" />
    </statement>
    <statement id="d0_s19">
      <text>replum constricted between seeds;</text>
      <biological_entity id="o26009" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character constraint="between seeds" constraintid="o26010" is_modifier="false" name="size" src="d0_s19" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o26010" name="seed" name_original="seeds" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>ovules 22–30 per ovary;</text>
      <biological_entity id="o26011" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o26012" from="22" name="quantity" src="d0_s20" to="30" />
      </biological_entity>
      <biological_entity id="o26012" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.1–0.4 mm;</text>
      <biological_entity id="o26013" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s21" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma subentire.</text>
      <biological_entity id="o26014" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 1.8–2.5 × 0.7–0.9 mm;</text>
      <biological_entity id="o26015" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s23" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s23" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing 0–0.1 mm wide distally.</text>
    </statement>
    <statement id="d0_s25">
      <text>2n = 28.</text>
      <biological_entity id="o26016" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" modifier="distally" name="width" src="d0_s24" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26017" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine barrens in chaparral, pine-oak or cypress woodland openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine barrens" constraint="in chaparral , pine-oak or cypress woodland openings" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="pine-oak" />
        <character name="habitat" value="cypress woodland openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus brachiatus is known from Lake and Sonoma counties. At the time that R. W. Dolan and L. F. LaPré (1989) studied it, infraspecific taxa based on differences in sepal color and pubescence seemed distinct (R. E. Buck et al. 1993). Populations discovered since then do not accord with the putative differences.</discussion>
  
</bio:treatment>