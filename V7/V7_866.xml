<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="treatment_page">545</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="Eastwood" date="1938" rank="species">teretifolium</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 144. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species teretifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095022</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">filifolium</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>2: 73. 1838,</place_in_publication>
      <other_info_on_pub>not F. Mueller 1852</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Erysimum;species filifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived, caudex often woody).</text>
      <biological_entity id="o23646" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes of leaves 2-rayed, mixed with 3-rayed ones.</text>
      <biological_entity id="o23648" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character constraint="with ones" constraintid="o23650" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o23649" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23650" name="one" name_original="ones" src="d0_s2" type="structure" />
      <relation from="o23648" id="r1609" name="part_of" negation="false" src="d0_s2" to="o23649" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched, (1.4–) 2.5–8 (–10) dm.</text>
      <biological_entity id="o23651" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1.4" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s3" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade filiform to narrowly linear, 5–17 cm × 0.4–3 mm, (somewhat revolute, appearing terete), base attenuate, margins denticulate, apex subacuminate.</text>
      <biological_entity constraint="basal" id="o23652" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23653" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s4" to="narrowly linear" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23654" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o23655" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o23656" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="subacuminate" value_original="subacuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves sessile;</text>
      <biological_entity constraint="cauline" id="o23657" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade margins entire or remotely denticulate.</text>
      <biological_entity constraint="blade" id="o23658" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o23660" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels divaricate to ascending, slender, narrower than fruit, 5–14 mm.</text>
      <biological_entity id="o23659" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o23660" is_modifier="false" modifier="considerably" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o23662" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o23661" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o23662" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
      <relation from="o23659" id="r1610" name="fruiting" negation="false" src="d0_s8" to="o23661" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong to linear-oblong, 7–11 mm, lateral pair saccate basally;</text>
      <biological_entity id="o23663" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23664" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="linear-oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals orange-yellow to yellow, broadly obovate to suborbicular, 15–20 (–25) × 5–10 mm, claw 6–13 mm, apex rounded;</text>
      <biological_entity id="o23665" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23666" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="orange-yellow" name="coloration" src="d0_s10" to="yellow" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s10" to="suborbicular" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23667" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23668" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filaments 7–14 mm;</text>
      <biological_entity id="o23669" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o23670" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 2.3–3.5 mm.</text>
      <biological_entity id="o23671" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23672" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits widely spreading to ascending, narrowly linear, curved or slightly twisted, somewhat torulose, (4–) 7–12 (–15) cm × 1.2–2.2 (–2.5) mm, slightly latiseptate, not striped;</text>
      <biological_entity id="o23673" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="widely spreading" name="orientation" src="d0_s13" to="ascending" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s13" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s13" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="15" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s13" to="12" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s13" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves with prominent midvein, pubescent outside, trichomes 2 and 3 (or 4) -rayed, glabrous inside;</text>
      <biological_entity id="o23674" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23675" name="midvein" name_original="midvein" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o23676" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o23674" id="r1611" name="with" negation="false" src="d0_s14" to="o23675" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 40–72 per ovary;</text>
      <biological_entity id="o23677" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o23678" from="40" name="quantity" src="d0_s15" to="72" />
      </biological_entity>
      <biological_entity id="o23678" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style cylindrical, slender, 0.5–2 (–2.5) mm, sparsely pubescent;</text>
      <biological_entity id="o23679" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma subentire to slightly 2-lobed, lobes as long as wide.</text>
      <biological_entity id="o23680" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s17" to="slightly 2-lobed" />
      </biological_entity>
      <biological_entity id="o23681" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ovoid, 1.5–2.3 (–2.7) × 0.9–1.5 mm;</text>
      <biological_entity id="o23682" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing appendage-like, distal.</text>
      <biological_entity id="o23683" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="appendage-like" value_original="appendage-like" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity constraint="distal" id="o23684" name="wing" name_original="wing" src="d0_s19" type="structure" />
      <biological_entity constraint="2n" id="o23685" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy areas bordering sage scrub or chaparral, sand deposits derived from sandstone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="sage scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="sand deposits" />
        <character name="habitat" value="sandstone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erysimum teretifolium is a highly endangered species known only from Santa Cruz County.</discussion>
  
</bio:treatment>