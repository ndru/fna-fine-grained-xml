<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">742</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">warea</taxon_name>
    <taxon_name authority="(Muhlenberg ex Nuttall) Nuttall" date="1834" rank="species">cuneifolia</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 84. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus warea;species cuneifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094947</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Muhlenberg ex Nuttall" date="unknown" rank="species">cuneifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 73. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cleome;species cuneifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stanleya</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>genus Stanleya;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (2–) 3–6.5 (–8) dm.</text>
      <biological_entity id="o22904" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves petiolate (petiole (0.05–) 0.1–0.2 (–0.3) cm proximally, obsolete distally);</text>
      <biological_entity constraint="cauline" id="o22905" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o22906" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.05" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="0.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" modifier="proximally" name="some_measurement" src="d0_s1" to="0.2" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="prominence" src="d0_s1" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade usually linear-oblanceolate to oblanceolate, rarely linear, (0.7–) 1–3 (–4) cm × 1.5–6 (–8) mm, base cuneate, apex rounded to retuse.</text>
      <biological_entity id="o22907" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually linear-oblanceolate" name="shape" src="d0_s2" to="oblanceolate" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s2" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22908" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22909" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes 0.3–2 (–3) cm in fruit.</text>
      <biological_entity id="o22911" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels (4–) 5–9 (–11) mm.</text>
      <biological_entity id="o22910" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o22911" from="0.3" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22912" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o22910" id="r1583" name="fruiting" negation="false" src="d0_s4" to="o22912" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals white or purplish, spreading or reflexed, 3–5 (–7) × 0.2–0.3 mm;</text>
      <biological_entity id="o22913" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o22914" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s5" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white or pink, broadly obovate to spatulate, 4–9 mm, blade 2–5 × 1.5–3 mm, claw 2–4 mm, nearly smooth or obscurely papillate, margins entire;</text>
      <biological_entity id="o22915" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22916" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s6" to="spatulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22917" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22918" name="claw" name_original="claw" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o22919" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 6–8 (–10) mm;</text>
      <biological_entity id="o22920" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22921" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1–1.5 mm;</text>
      <biological_entity id="o22922" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22923" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>gynophore slender, (5–) 7–11 mm.</text>
      <biological_entity id="o22924" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o22925" name="gynophore" name_original="gynophore" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits 2–4 (–5) cm × 0.7–1 mm;</text>
      <biological_entity id="o22926" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 32–54 per ovary;</text>
      <biological_entity id="o22927" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o22928" from="32" name="quantity" src="d0_s11" to="54" />
      </biological_entity>
      <biological_entity id="o22928" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style rarely to 0.1 mm.</text>
      <biological_entity id="o22929" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 0.6–0.8 × 0.4–0.5 mm.</text>
      <biological_entity id="o22930" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s13" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy areas, scrublands, sand hills, fields, open banks, oak-pinyon woods, roadside embankments</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="scrublands" />
        <character name="habitat" value="sand hills" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="open banks" />
        <character name="habitat" value="oak-pinyon woods" />
        <character name="habitat" value="roadside embankments" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Although Warea cuneifolia is fairly widespread in Georgia and South Carolina, it is known in Alabama only from Pike County, in Florida from Gadsden and Liberty counties, and in North Carolina from Harnett and Hoke counties.</discussion>
  
</bio:treatment>