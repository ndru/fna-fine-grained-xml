<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">304</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Nuttall ex Torrey &amp; A. Gray" date="1838" rank="species">cuneifolia</taxon_name>
    <taxon_name authority="S. Watson" date="1888" rank="variety">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>23: 256. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species cuneifolia;variety integrifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095296</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">integrifolia</taxon_name>
    <taxon_hierarchy>genus Draba;species integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">sonorae</taxon_name>
    <taxon_name authority="(S. Watson) O. E. Schulz" date="unknown" rank="variety">integrifolia</taxon_name>
    <taxon_hierarchy>genus Draba;species sonorae;variety integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems pubescent with 3-rayed or 4-rayed trichomes.</text>
      <biological_entity id="o40022" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="with trichomes" constraintid="o40023" is_modifier="false" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o40023" name="trichome" name_original="trichomes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Racemes to 3/4 of scape.</text>
      <biological_entity id="o40024" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="of scape" constraintid="o40025" from="0" name="quantity" src="d0_s1" to="3/4" />
      </biological_entity>
      <biological_entity id="o40025" name="scape" name_original="scape" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Fruits oblong to linear or lanceolate, (5–) 7–12 mm;</text>
      <biological_entity id="o40026" name="fruit" name_original="fruits" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="linear or lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>valves: trichomes stalked and 4-rayed, sometimes with 2-rayed or 3-rayed ones;</text>
      <biological_entity id="o40027" name="valve" name_original="valves" src="d0_s3" type="structure" />
      <biological_entity id="o40028" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ovules 20–44 per ovary.</text>
      <biological_entity id="o40029" name="valve" name_original="valves" src="d0_s4" type="structure" />
      <biological_entity id="o40030" name="ovule" name_original="ovules" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o40031" from="20" name="quantity" src="d0_s4" to="44" />
      </biological_entity>
      <biological_entity id="o40031" name="ovary" name_original="ovary" src="d0_s4" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Mar.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and gravelly soil in desert scrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="desert scrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Tex., Utah; Mexico (Baja California, Nuevo León, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31b.</number>
  
</bio:treatment>