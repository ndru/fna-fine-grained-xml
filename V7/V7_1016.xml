<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="treatment_page">626</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Wormskjöld ex Hornemann) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">arctica</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 321. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species arctica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alyssum</taxon_name>
    <taxon_name authority="Wormskjöld ex Hornemann" date="unknown" rank="species">arcticum</taxon_name>
    <place_of_publication>
      <publication_title>in G. C. Oeder et al., Fl. Dan.</publication_title>
      <place_in_publication>9(26): 3, plate 1520. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Alyssum;species arcticum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Wormskjöld ex Hornemann) S. Watson" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Wormskjöld ex Hornemann) Richardson" date="unknown" rank="species">arctica</taxon_name>
    <taxon_name authority="(S.Watson) A. E. Porsild" date="unknown" rank="subspecies">purshii</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species arctica;subspecies purshii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="(Trautvetter) N. Busch" date="unknown" rank="species">arctica</taxon_name>
    <taxon_hierarchy>genus Vesicaria;species arctica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">leiocarpa</taxon_name>
    <taxon_hierarchy>genus Vesicaria;species leiocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o29471" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched, (woody, cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>± densely pubescent, trichomes (sessile or short-stalked), several-rayed, rays distinct, furcate or bifurcate, (somewhat umbonate, finely tuberculate to ± smooth).</text>
      <biological_entity id="o29472" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29473" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="several-rayed" value_original="several-rayed" />
      </biological_entity>
      <biological_entity id="o29474" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few to several from base, erect to spreading or prostrate, 0.5–2 (–3) dm.</text>
      <biological_entity id="o29475" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s3" value="few to several" value_original="few to several" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s3" to="spreading or prostrate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o29476" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o29475" id="r1983" name="from" negation="false" src="d0_s3" to="o29476" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (usually ± rosulate);</text>
      <biological_entity constraint="basal" id="o29477" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade obovate to oblanceolate, (1–) 2–6 (–15) cm, margins entire.</text>
      <biological_entity id="o29478" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29479" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (sessile or shortly petiolate);</text>
      <biological_entity constraint="cauline" id="o29480" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate or lingulate, 0.5–1.5 (–3) cm, margins entire.</text>
      <biological_entity id="o29481" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o29482" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes loose.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels (erect to divaricate or ascending), (5–) 10–20 (–40) mm, (stout).</text>
      <biological_entity id="o29483" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29484" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o29483" id="r1984" name="fruiting" negation="false" src="d0_s9" to="o29484" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ovate to elliptic, (3–) 4–5 (–6) mm, (median pair often thickened apically, cucullate);</text>
      <biological_entity id="o29485" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29486" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals spatulate, 5–6 (–7) mm, (blade gradually narrowed to claw).</text>
      <biological_entity id="o29487" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o29488" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits subglobose to ellipsoid, uncompressed, 4–6 (–9) mm;</text>
      <biological_entity id="o29489" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s12" to="ellipsoid" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves (not retaining seeds after dehiscence), glabrous or sparsely pubescent outside, trichomes sessile;</text>
      <biological_entity id="o29490" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29491" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o29492" name="replum" name_original="replum" src="d0_s14" type="structure">
        <character constraint="than fruit" constraintid="o29493" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o29493" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovules (8–) 10–14 (–16) per ovary;</text>
      <biological_entity id="o29494" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s15" to="10" to_inclusive="false" />
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="16" />
        <character char_type="range_value" constraint="per ovary" constraintid="o29495" from="10" name="quantity" src="d0_s15" to="14" />
      </biological_entity>
      <biological_entity id="o29495" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 1–2.5 (–4) mm.</text>
      <biological_entity id="o29496" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds plump.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 60.</text>
      <biological_entity id="o29497" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="plump" value_original="plump" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29498" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Typically on sand and gravel from calcareous bedrock, river bars and terraces, cliff ledges, scree and talus slopes, often growing after disturbance</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sand" modifier="typically on" />
        <character name="habitat" value="calcareous bedrock" />
        <character name="habitat" value="river bars" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="gravel from calcareous bedrock" />
        <character name="habitat" value="cliff ledges" />
        <character name="habitat" value="scree" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="disturbance" modifier="slopes often growing after" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska; circumarctic (except n Europe, ne Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="circumarctic (except n Europe)" establishment_means="native" />
        <character name="distribution" value="circumarctic (ne Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Arctic bladderpod</other_name>
  
</bio:treatment>