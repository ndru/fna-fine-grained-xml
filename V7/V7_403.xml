<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="mention_page">316</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Wulfen in N. J. Jacquin" date="1778" rank="species">fladnizensis</taxon_name>
    <place_of_publication>
      <publication_title>in N. J. Jacquin, Misc. Austriac.</publication_title>
      <place_in_publication>1: 147, plate 17, fig. 1. 1778</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species fladnizensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">200009446</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="species">fladnizensis</taxon_name>
    <taxon_name authority="(O. E. Schulz) Rollins" date="unknown" rank="variety">pattersonii</taxon_name>
    <taxon_hierarchy>genus Draba;species fladnizensis;variety pattersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Hartman" date="unknown" rank="species">pattersonii</taxon_name>
    <taxon_hierarchy>genus Draba;species pattersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pattersonii</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">hirticaulis</taxon_name>
    <taxon_hierarchy>genus Draba;species pattersonii;variety hirticaulis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wahlenbergii</taxon_name>
    <taxon_hierarchy>genus Draba;species wahlenbergii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(sometimes cespitose);</text>
      <biological_entity id="o9344" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple or branched (with persistent leaf-bases);</text>
    </statement>
    <statement id="d0_s3">
      <text>usually scapose.</text>
      <biological_entity id="o9345" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.2–) 0.3–1 (–1.3) dm, glabrous.</text>
      <biological_entity id="o9346" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="1.3" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s4" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o9347" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole (obscure), margin ciliate, (trichomes simple or 2-rayed, 0.25–0.6 mm);</text>
      <biological_entity id="o9348" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o9349" name="margin" name_original="margin" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade linear to oblanceolate or narrowly obovate, (0.3–) 0.4–1.2 (–1.6) cm × 1–3 (–4) mm, margins usually entire, rarely toothed, surfaces abaxially pubescent or glabrous, trichomes simple, sometimes with fewer, short-stalked, 2-rayed ones, (midvein prominent), adaxially often glabrous.</text>
      <biological_entity id="o9350" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="oblanceolate or narrowly obovate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s7" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s7" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9351" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9352" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9353" name="trichome" name_original="trichomes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="with fewer , short-stalked , 2-rayed ones; adaxially often" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 0–2;</text>
    </statement>
    <statement id="d0_s9">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o9354" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade oblong to ovate, margins entire, (ciliate).</text>
      <biological_entity id="o9355" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="ovate" />
      </biological_entity>
      <biological_entity id="o9356" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Racemes (2 or) 3–11 (–14) -flowered, usually ebracteate, rarely proximalmost flowers bracteate, elongated in fruit;</text>
      <biological_entity id="o9357" name="raceme" name_original="racemes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-11(-14)-flowered" value_original="3-11(-14)-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o9358" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s11" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o9359" is_modifier="false" name="length" src="d0_s11" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o9359" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rachis not flexuous, glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate-ascending, often straight, (1–) 2–5 (–6) mm, glabrous.</text>
      <biological_entity id="o9360" name="rachis" name_original="rachis" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9361" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o9360" id="r671" name="fruiting" negation="false" src="d0_s13" to="o9361" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals (green or purplish), ovate, 1.2–2.2 mm, glabrous or pubescent, (trichomes simple);</text>
      <biological_entity id="o9362" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9363" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>petals white, spatulate, 2–2.5 × 0.8–1.5 mm;</text>
      <biological_entity id="o9364" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o9365" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers ovate, 0.2–0.25 mm.</text>
      <biological_entity id="o9366" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o9367" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits elliptic-lanceolate to oblong, plane, flattened, 3–8 (–9) × 1.5–2 mm;</text>
      <biological_entity id="o9368" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s17" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s17" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves glabrous;</text>
      <biological_entity id="o9369" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 12–24 per ovary;</text>
      <biological_entity id="o9370" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o9371" from="12" name="quantity" src="d0_s19" to="24" />
      </biological_entity>
      <biological_entity id="o9371" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.05–0.2 (–0.3) mm.</text>
      <biological_entity id="o9372" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s20" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s20" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds oblong to elliptic, 0.8–1 × 0.5–0.6 mm. 2n = 16.</text>
      <biological_entity id="o9373" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s21" to="elliptic" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s21" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s21" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9374" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops and talus, alpine meadows, sandy gravel</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="sandy gravel" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m at higher latitudes, 3000-3800 m at lower latitudes</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" constraint=" at higher latitudes" />
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="3000" from_unit="m" constraint=" at lower latitudes" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T., Nunavut, Que., Yukon; Alaska, Colo., Utah, Wyo.; c, s Europe; Asia; circumpolar and high alpine areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="circumpolar and high alpine areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <discussion>Rollins reduced Draba pattersonii to a variety of D. fladnizensis and separated the two primarily on plant size and minor differences in fruit shape. Examination of D. fladnizensis specimens collected throughout Europe and North America reveals that the alleged differences between the two taxa are artificial. The type material of D. pattersonii, which was collected in Colorado, is a mixture of plants highly variable in their type of indumentum. The specimens have no flowers, but the habit, fruits, and leaves are nearly indistinguishable from those of D. fladnizensis from higher latitudes.</discussion>
  <discussion>In the absence of flowers, the white-flowered Draba fladnizensis (2n = 16) is often confused with the yellow-flowered D. crassifolia (2n = 40). The latter is an annual or short-lived perennial that rarely forms a well-developed caudex, whereas D. fladnizensis almost always produces a distinct caudex. Although most individuals of both species are scapose, they occasionally produce one or two cauline leaves. The cauline leaves are usually glabrous in D. crassifolia and ciliate in D. fladnizensis; in the latter, the distalmost cauline leaf usually subtends the proximalmost flower. Finally, the seeds in D. fladnizensis are slightly larger (0.8–0.1 × 0.5–0.6 versus 0.7–0.8 × 0.4–0.5 mm) than those of D. crassifolia, though the reliability of this distinction needs to be examined in greater detail. N. H. Holmgren (2005b) reported D. fladnizensis from central Nevada, but we have not seen any material of the species from that state.</discussion>
  
</bio:treatment>