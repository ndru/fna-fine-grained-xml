<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Harvey" date="1845" rank="genus">dithyrea</taxon_name>
    <taxon_name authority="(Davidson) Davidson in A. Davidson and G. L. Moxley" date="unknown" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>in A. Davidson and G. L. Moxley, Fl. S. Calif.,</publication_title>
      <place_in_publication>151. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus dithyrea;species maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095156</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Biscutella</taxon_name>
    <taxon_name authority="(Harvey) Bentham &amp; Hooker f. ex Brewer &amp; S. Watson" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="Davidson" date="unknown" rank="variety">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 179. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Biscutella;species californica;variety maritima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="Harvey" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="Davidson ex B. L. Robinson" date="unknown" rank="variety">maritima</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species californica;variety maritima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(rhizomatous).</text>
      <biological_entity id="o21108" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems several from base, decumbent, unbranched or branched (few) distally, 1–2.1 dm.</text>
      <biological_entity id="o21109" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o21110" is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="2.1" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o21110" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole 1–6 (–8) cm;</text>
      <biological_entity constraint="basal" id="o21111" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21112" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate to suborbicular or oblanceolate, 1–5 (–6) cm × 7–40 mm, base cuneate, margins dentate, sometimes repand.</text>
      <biological_entity constraint="basal" id="o21113" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21114" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="suborbicular or oblanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21115" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o21116" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) shortly petiolate to subsessile;</text>
      <biological_entity constraint="cauline" id="o21117" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="shortly petiolate" name="architecture" src="d0_s5" to="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate or oblong, 1–3 cm × 8–20 mm, base cuneate, margins entire, sometimes repand.</text>
      <biological_entity id="o21118" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21119" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o21120" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes not elongated in fruit.</text>
      <biological_entity id="o21122" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels 1.5–2.5 mm.</text>
      <biological_entity id="o21121" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o21122" is_modifier="false" modifier="not" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21123" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o21121" id="r1460" name="fruiting" negation="false" src="d0_s8" to="o21123" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals lavender, oblong-linear, (6–) 7–10 × 0.8–1.2 mm;</text>
      <biological_entity id="o21124" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21125" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s9" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white to purplish, 12–15 × 2.5–3.5 mm;</text>
      <biological_entity id="o21126" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21127" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="purplish" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 5.5–7.5 mm;</text>
      <biological_entity id="o21128" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21129" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 2–3 mm.</text>
      <biological_entity id="o21130" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21131" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits: valves suborbicular, 8–11 × 8–10 mm, base and apex rounded, pubescent, trichomes stellate, sessile, sometimes mixed with basally forked, or, rarely, simple ones (clavate papillae absent);</text>
      <biological_entity id="o21132" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity id="o21133" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21134" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21135" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21136" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character constraint="with basally forked" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s13" value="mixed" value_original="mixed" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style 0.3–1 mm.</text>
      <biological_entity id="o21137" name="fruit" name_original="fruits" src="d0_s14" type="structure" />
      <biological_entity id="o21138" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 3–4 × 2–2.5 mm. 2n = 60.</text>
      <biological_entity id="o21139" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21140" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seashores, coastal sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seashores" />
        <character name="habitat" value="coastal sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">Dithyraea</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>