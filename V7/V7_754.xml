<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Torrey" date="1837" rank="genus">leavenworthia</taxon_name>
    <taxon_name authority="Mahler" date="1987" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>12: 239, fig. 1. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus leavenworthia;species texana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094621</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leavenworthia</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">aurea</taxon_name>
    <taxon_name authority="(Mahler) Rollins" date="unknown" rank="variety">texana</taxon_name>
    <taxon_hierarchy>genus Leavenworthia;species aurea;variety texana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (when present) 1–2 dm.</text>
      <biological_entity id="o3911" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole 1.5–4 cm;</text>
      <biological_entity constraint="basal" id="o3912" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o3913" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 3.5–5 cm, lobes 0–4 on each side, margins shallowly dentate, terminal lobe transversely broadly oblong, 0.6–0.7 cm × 8–11 mm, (distinctly shorter than wide), considerably larger than lateral lobes, margins slightly lobed or shallowly dentate.</text>
      <biological_entity constraint="basal" id="o3914" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o3915" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3916" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o3917" from="0" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o3917" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o3918" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o3919" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="transversely broadly" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s2" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="11" to_unit="mm" />
        <character constraint="than lateral lobes" constraintid="o3920" is_modifier="false" name="size" src="d0_s2" value="considerably larger" value_original="considerably larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o3920" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity id="o3921" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting pedicels: solitary flowers 30–70 mm;</text>
      <biological_entity id="o3923" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <biological_entity id="o3924" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>racemes 20–40 mm.</text>
      <biological_entity id="o3926" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o3927" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals widely spreading, oblong-linear, 3.9–4.5 × 1.4–1.8 mm;</text>
      <biological_entity id="o3928" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3929" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s5" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals spreading, bright-yellow, narrowly obovate, 7.3–9 × 2–3 mm, claw dark yellow, 2.6–3.3 mm, apex shallowly emarginate, apical notch 0.1–0.3 mm deep;</text>
      <biological_entity id="o3930" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3931" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="7.3" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3932" name="claw" name_original="claw" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark yellow" value_original="dark yellow" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s6" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3933" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s6" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3934" name="notch" name_original="notch" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s6" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments: median 4–4.5 mm, lateral 2.2–2.5 mm;</text>
      <biological_entity id="o3935" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="median" value_original="median" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.8–1.3 mm.</text>
      <biological_entity id="o3936" name="filament" name_original="filaments" src="d0_s8" type="structure" />
      <biological_entity id="o3937" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits oblong, 1.5–2.3 cm × 4–5 mm, smooth, latiseptate, (margined);</text>
      <biological_entity id="o3938" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s9" to="2.3" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves thin;</text>
      <biological_entity id="o3939" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 4–14 per ovary;</text>
      <biological_entity id="o3940" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o3941" from="4" name="quantity" src="d0_s11" to="14" />
      </biological_entity>
      <biological_entity id="o3941" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style 1.7–3 mm.</text>
      <biological_entity id="o3942" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 3–3.5 mm diam.;</text>
      <biological_entity id="o3943" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>wing 0.2–0.4 mm wide;</text>
      <biological_entity id="o3944" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryo straight.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 22.</text>
      <biological_entity id="o3945" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3946" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pastures, seepage areas of rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pastures" />
        <character name="habitat" value="seepage areas" constraint="of rock outcrops" />
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Leavenworthia texana is known only from San Augustine County. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <discussion>Rollins reduced Leavenworthia texana to a variety of L. aurea. The differences in petal color, shape and margin of the terminal lobe, and chromosome number clearly support recognition as distinct species.</discussion>
  
</bio:treatment>