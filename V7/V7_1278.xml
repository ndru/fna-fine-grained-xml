<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">728</other_info_on_meta>
    <other_info_on_meta type="mention_page">729</other_info_on_meta>
    <other_info_on_meta type="treatment_page">732</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="(Nuttall) Endlicher in W. G. Walpers" date="1842" rank="species">integrifolium</taxon_name>
    <place_of_publication>
      <publication_title>in W. G. Walpers, Repert. Bot. Syst.</publication_title>
      <place_in_publication>1: 172. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species integrifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094934</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pachypodium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">integrifolium</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 96. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pachypodium;species integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pleurophragma</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="unknown" rank="species">integrifolium</taxon_name>
    <taxon_hierarchy>genus Pleurophragma;species integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>somewhat glaucous throughout, glabrous.</text>
      <biological_entity id="o33408" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="somewhat; throughout" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems paniculately branched distally, (2–) 4.5–17 (–28) dm.</text>
      <biological_entity id="o33409" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="paniculately; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="28" to_unit="dm" />
        <character char_type="range_value" from="4.5" from_unit="dm" name="some_measurement" src="d0_s2" to="17" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole (0.5–) 1.2–10 (–15) cm;</text>
      <biological_entity constraint="basal" id="o33410" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o33411" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually oblong to lanceolate, or oblanceolate to spatulate, rarely ovate or obovate, (3.7–) 5–31 (–54) cm × (12–) 16–78 (–140) mm, margins usually entire, rarely dentate or repand.</text>
      <biological_entity constraint="basal" id="o33412" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o33413" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually oblong" name="shape" src="d0_s4" to="lanceolate or oblanceolate" />
        <character char_type="range_value" from="usually oblong" name="shape" src="d0_s4" to="lanceolate or oblanceolate" />
        <character char_type="range_value" from="usually oblong" name="shape" src="d0_s4" to="lanceolate or oblanceolate" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="31" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="54" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="31" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_width" src="d0_s4" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="78" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="140" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s4" to="78" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33414" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (ascending, not appressed to stem);</text>
    </statement>
    <statement id="d0_s6">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o33415" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade linear to linear-lanceolate, (1–) 2.2–8.3 (–19.5) cm × (2–) 3–11 (–25) mm, (base cuneate to attenuate, not auriculate), margins entire.</text>
      <biological_entity id="o33416" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s7" to="2.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="19.5" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="length" src="d0_s7" to="8.3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33417" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes dense, slightly or considerably elongated in fruit, (flower buds narrowly oblong).</text>
      <biological_entity id="o33419" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels usually horizontal to divaricate, rarely ascending or reflexed, usually straight, rarely incurved, slender or stout, (2–) 3–9 (–13) mm, often strongly flattened, rarely not flattened at base.</text>
      <biological_entity id="o33418" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character constraint="in fruit" constraintid="o33419" is_modifier="false" modifier="slightly; considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o33420" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o33421" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s9" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="often strongly" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="rarely not" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o33418" id="r2251" name="fruiting" negation="false" src="d0_s9" to="o33420" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect, linear to linear-oblong, (3–) 3.5–5.5 (–7.5) × (0.5–) 0.8–1 (–1.5) mm;</text>
      <biological_entity id="o33422" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33423" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="linear-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s10" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s10" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white or lavender to purple, spatulate to oblanceolate, (4.5–) 6–9 (–13) × (0.5–) 0.8–1.5 (–2.3) mm, margins not crisped, claw differentiated from blade, [slender, (2–) 3–4.5 (–7) mm, narrowest at base];</text>
      <biological_entity id="o33424" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33425" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s11" to="purple" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s11" to="oblanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_width" src="d0_s11" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33426" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o33427" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o33428" is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o33428" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>nectar glands lateral, flat or toothlike, median glands absent;</text>
      <biological_entity id="o33429" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="nectar" id="o33430" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s12" value="toothlike" value_original="toothlike" />
      </biological_entity>
      <biological_entity constraint="median" id="o33431" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments subequal to slightly tetradynamous, (3–) 4.5–7.5 (–12.5) mm;</text>
      <biological_entity id="o33432" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33433" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="12.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s13" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers exserted, linear to narrowly oblong, (1–) 1.5–2.5 (–4.5) mm, not circinately coiled;</text>
      <biological_entity id="o33434" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o33435" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s14" to="narrowly oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="not circinately" name="architecture" src="d0_s14" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>gynophore 0.5–2 (–5.5) mm.</text>
      <biological_entity id="o33436" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o33437" name="gynophore" name_original="gynophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits usually horizontal to divaricate-ascending, or ascending, rarely reflexed, usually torulose, rarely submoniliform, usually strongly incurved, sometimes straight, rarely arcuate, terete, (0.8–) 1.4–6.5 (–8) cm × 1–1.3 (–2) mm;</text>
      <biological_entity id="o33438" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="usually horizontal" name="orientation" src="d0_s16" to="divaricate-ascending or ascending" />
        <character char_type="range_value" from="usually horizontal" name="orientation" src="d0_s16" to="divaricate-ascending or ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s16" value="submoniliform" value_original="submoniliform" />
        <character is_modifier="false" modifier="usually strongly" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course_or_shape" src="d0_s16" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s16" to="1.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s16" to="8" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s16" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s16" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 14–40 per ovary;</text>
      <biological_entity id="o33439" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o33440" from="14" name="quantity" src="d0_s17" to="40" />
      </biological_entity>
      <biological_entity id="o33440" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style usually cylindrical, rarely subclavate, 0.5–1.3 (–3.2) mm.</text>
      <biological_entity id="o33441" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s18" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s18" value="subclavate" value_original="subclavate" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds (1–) 1.2–2 (–2.3) × 0.7–1 (–1.3) mm.</text>
      <biological_entity id="o33442" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s19" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s19" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s19" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., N.Dak., N.Mex., Nebr., Nev., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals white; fruiting pedicels whitish, stout, (5-) 6-11(-13) mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually lavender to purple, rarely white; fruiting pedicels not whitish, slender and 2-10 (-13) mm, or stout and 2-5(-6) mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals (5-)6-9(-10) mm; fruiting pedicels horizontal to divaricate, (5-)6-9(-13) mm; fruits (1.4-)1.9-3.7(-4.5) cm; gynophores (0.5-)1-3 mm; se California, s Nevada, sw Utah.</description>
      <determination>6b Thelypodium integrifolium subsp. affine</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals (7.5-)8-11(-13) mm; fruiting pedicels usually horizontal to reflexed, rarely divaricate, (6-)7-11(-13) mm; fruits (2.2-) 3.5-6.5(-8) cm; gynophores (1-)1.5-4 (-5.5) mm; Grand Canyon, Colorado River, Arizona.</description>
      <determination>6e Thelypodium integrifolium subsp. longicarpum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting pedicels often slender, sometimes stout, not or slightly flattened at base, (4-)6- 10 (-13) mm.</description>
      <determination>6a Thelypodium integrifolium subsp. integrifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting pedicels stout, strongly flattened at base, 2-5(-6) mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Racemes often strongly congested, central rachises (0.6-)1.2-7(-9) cm; gynophores stout, 0.5-1(-3) mm; e California, Nevada, s Oregon, nw Utah.</description>
      <determination>6c Thelypodium integrifolium subsp. complanatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Racemes somewhat congested, central rachises (3-)8-20(-28) cm; gynophores slender, (0.5-)0.8-3(-4) mm; ne Arizona, w Colorado, nw New Mexico, e Utah.</description>
      <determination>6d Thelypodium integrifolium subsp. gracilipes</determination>
    </key_statement>
  </key>
</bio:treatment>