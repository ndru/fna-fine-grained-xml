<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">KOEBERLINIACEAE</taxon_name>
    <taxon_hierarchy>family KOEBERLINIACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10466</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees (deciduous);</text>
      <biological_entity id="o42295" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>(thorny) spines absent: glabrous or glabrate.</text>
      <biological_entity id="o42297" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect;</text>
    </statement>
    <statement id="d0_s3">
      <text>profusely branched.</text>
      <biological_entity id="o42298" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="profusely" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves alternate, spirally arranged, (minute, scalelike);</text>
    </statement>
    <statement id="d0_s5">
      <text>venation pinnate;</text>
      <biological_entity id="o42299" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules present (caducous, scalelike);</text>
      <biological_entity id="o42300" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole absent.</text>
      <biological_entity id="o42301" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary, racemose, corymbose, or flowers solitary;</text>
      <biological_entity id="o42302" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="corymbose" value_original="corymbose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedunculate;</text>
      <biological_entity id="o42303" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bud-scales usually persistent;</text>
      <biological_entity id="o42304" name="bud-scale" name_original="bud-scales" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts usually absent, sometimes present.</text>
      <biological_entity id="o42305" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels present.</text>
      <biological_entity id="o42306" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers bisexual, actinomorphic, rotate to crateriform, campanulate, or urceolate;</text>
      <biological_entity id="o42307" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="actinomorphic" value_original="actinomorphic" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s13" to="crateriform campanulate or urceolate" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s13" to="crateriform campanulate or urceolate" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s13" to="crateriform campanulate or urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o42308" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o42309" name="androecium" name_original="androecium" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals persistent, 4, distinct or connate basally;</text>
      <biological_entity id="o42310" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 4, attached directly to receptacle, imbricate, distinct, equal;</text>
      <biological_entity id="o42311" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
        <character constraint="to receptacle" constraintid="o42312" is_modifier="false" name="fixation" src="d0_s16" value="attached" value_original="attached" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s16" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s16" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o42312" name="receptacle" name_original="receptacle" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>intrastaminal nectary-discs or glands usually present;</text>
      <biological_entity constraint="intrastaminal" id="o42313" name="nectary-disc" name_original="nectary-discs" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="intrastaminal" id="o42314" name="gland" name_original="glands" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stamens 8;</text>
      <biological_entity id="o42315" name="stamen" name_original="stamens" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>filaments distinct, glabrous or pubescent;</text>
      <biological_entity id="o42316" name="filament" name_original="filaments" src="d0_s19" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers dehiscing bylongitudinal slits;</text>
      <biological_entity id="o42317" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s20" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o42318" name="slit" name_original="slits" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>pistil 1;</text>
      <biological_entity id="o42319" name="pistil" name_original="pistil" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovary 1-carpellate, 2-locular;</text>
    </statement>
    <statement id="d0_s23">
      <text>placentation axile;</text>
      <biological_entity id="o42320" name="ovary" name_original="ovary" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="1-carpellate" value_original="1-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s22" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s23" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>ovules anatropous, bitegmic;</text>
      <biological_entity id="o42321" name="ovule" name_original="ovules" src="d0_s24" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s24" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="bitegmic" value_original="bitegmic" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>style 1 (straight, short and thick);</text>
      <biological_entity id="o42322" name="style" name_original="style" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>stigma 1, capitate, unlobed.</text>
      <biological_entity id="o42323" name="stigma" name_original="stigma" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s26" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s26" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Fruits: berries, indehiscent (spheroidal).</text>
      <biological_entity id="o42324" name="fruit" name_original="fruits" src="d0_s27" type="structure">
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s27" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity id="o42325" name="berry" name_original="berries" src="d0_s27" type="structure" />
    </statement>
    <statement id="d0_s28">
      <text>Seeds (1 or) 2–4, blackish;</text>
    </statement>
    <statement id="d0_s29">
      <text>not arillate;</text>
      <biological_entity id="o42326" name="seed" name_original="seeds" src="d0_s28" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s28" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s28" value="blackish" value_original="blackish" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s29" value="arillate" value_original="arillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>endosperm scanty or absent, sometimes a persistent perisperm present;</text>
      <biological_entity id="o42327" name="endosperm" name_original="endosperm" src="d0_s30" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s30" value="scanty" value_original="scanty" />
        <character is_modifier="false" name="presence" src="d0_s30" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o42328" name="perisperm" name_original="perisperm" src="d0_s30" type="structure">
        <character is_modifier="true" name="duration" src="d0_s30" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="presence" src="d0_s30" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s31">
      <text>cotyledons incumbent.</text>
    </statement>
    <statement id="d0_s32">
      <text>x = 11.</text>
      <biological_entity id="o42329" name="cotyledon" name_original="cotyledons" src="d0_s31" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s31" value="incumbent" value_original="incumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o42330" name="chromosome" name_original="" src="d0_s32" type="structure">
        <character name="quantity" src="d0_s32" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw North America, n Mexico, South America (Bolivia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>0</number>
  <other_name type="common_name">Allthorn Family</other_name>
  <discussion>Genus 1, species 2 (1 in the flora).</discussion>
  <discussion>Koeberlinia has been included in the traditional Capparaceae by some authors, including A. Cronquist (1981). Recent molecular studies support its removal from Capparaceae and a relationship with Bataceae and Salvadoraceae (P. F. Stevens, www.mobot.org/MOBOT/research/APweb).</discussion>
  <references>
    <reference>Holmes, W. C., K. L. Yip, and A. E. Rushing. 2008. Taxonomy of Koeberlinia (Koeberliniaceae). Brittonia 60: 171–184.</reference>
  </references>
  
</bio:treatment>