<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">739</other_info_on_meta>
    <other_info_on_meta type="mention_page">741</other_info_on_meta>
    <other_info_on_meta type="treatment_page">740</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Hooker" date="1830" rank="genus">thysanocarpus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1838" rank="species">laciniatus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 118. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thysanocarpus;species laciniatus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094950</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–6 dm.</text>
      <biological_entity id="o39777" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blade oblanceolate to elliptic, 1–6 cm, margins often pinnatifid with narrow lobes (lobes 0.5–1.5 mm), sometimes sinuate-dentate or subentire, surfaces usually glabrous, rarely sparsely hirsute, trichomes whitish, 0.3–0.4 mm.</text>
      <biological_entity constraint="basal" id="o39778" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o39779" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39780" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character constraint="with lobes" constraintid="o39781" is_modifier="false" modifier="often" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o39781" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o39782" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o39783" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s1" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: blade linear to narrowly elliptic, widest near middle or equally wide throughout, base not auriculate or with small, inconspicuous auricles (not extending around stem).</text>
      <biological_entity constraint="cauline" id="o39784" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o39785" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s2" to="narrowly elliptic" />
        <character constraint="near " constraintid="o39787" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity constraint="middle" id="o39786" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o39787" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="equally" name="width" src="d0_s2" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="middle" id="o39788" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o39789" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="equally" name="width" src="d0_s2" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="with small , inconspicuous auricles" />
      </biological_entity>
      <biological_entity id="o39790" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="true" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o39787" id="r2686" name="near" negation="false" src="d0_s2" to="o39788" />
      <relation from="o39787" id="r2687" name="near" negation="false" src="d0_s2" to="o39789" />
      <relation from="o39785" id="r2688" name="with" negation="false" src="d0_s2" to="o39790" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes: internodes (1.5–) 2–4.5 mm in fruit.</text>
      <biological_entity id="o39791" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o39793" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels smoothly recurved or straight and stiffly spreading, (proximal) 3–6 (–10) mm.</text>
      <biological_entity id="o39792" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o39793" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="smoothly" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39794" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o39792" id="r2689" name="fruiting" negation="false" src="d0_s4" to="o39794" />
    </statement>
    <statement id="d0_s5">
      <text>Fruits flat or planoconvex, obovate to nearly orbicular, (2.5–5 mm wide);</text>
      <biological_entity id="o39795" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="planoconvex obovate" name="shape" src="d0_s5" to="nearly orbicular" />
        <character char_type="range_value" from="planoconvex obovate" name="shape" src="d0_s5" to="nearly orbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>valves often glabrous, sometimes pubescent, trichomes clavate, 0.05–0.4 mm;</text>
      <biological_entity id="o39796" name="valve" name_original="valves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39797" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>wing entire or deeply crenate, rays absent or indistinct.</text>
      <biological_entity id="o39798" name="wing" name_original="wing" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o39799" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Thysanocarpus laciniatus presents some of the same problems as does T. curvipes. Variety laciniatus contains both diploids and tetraploids (M. D. Windham, unpubl.) and varies in fruit characters, pubescence, and basal leaf shape. Specimens with sinuate-dentate basal leaf margins and small auricles on cauline leaves can be difficult to distinguish from T. curvipes. Preliminary molecular phylogenetic analyses support the distinction between T. curvipes and T. laciniatus var. laciniatus, but suggest that tetraploid populations of the latter may have arisen through hybridization between T. curvipes and a diploid member of the T. laciniatus clade (P. Alexander, unpubl.). Varieties hitchcockii and rigidus are distinctive diploids (Windham, unpubl.) with restricted ranges and may deserve specific rank. Variety rigidus (known to us from only four collections) can be difficult to distinguish from the more purplish specimens of var. laciniatus, but the latter have at least some recurved pedicels and often have pinnatifid leaves.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruit valves pubescent, trichomes 0.05-0.1 mm.</description>
      <determination>3b Thysanocarpus laciniatus var. hitchcockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruit valves usually glabrous, or trichomes 0.2-0.4 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Foliage usually greenish throughout, sometimes purplish basally; basal leaf blade margins pinnatifid or sinuate-dentate; fruiting pedicels smoothly recurved.</description>
      <determination>3a Thysanocarpus laciniatus var. laciniatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Foliage purplish throughout; basal leaf blade margins subentire to sinuate-dentate; fruiting pedicels straight or nearly so.</description>
      <determination>3c Thysanocarpus laciniatus var. rigidus</determination>
    </key_statement>
  </key>
</bio:treatment>