<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">620</other_info_on_meta>
    <other_info_on_meta type="treatment_page">626</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Richardson) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">arenosa</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 321. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species arenosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095115</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="Richardson" date="unknown" rank="species">arenosa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Franklin, Narr. Journey Polar Sea,</publication_title>
      <place_in_publication>743. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vesicaria;species arenosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Richardson) Rydberg" date="unknown" rank="species">arenosa</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species arenosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">argentea</taxon_name>
    <taxon_name authority="(Richardson) Rydberg" date="unknown" rank="variety">arenosa</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species argentea;variety arenosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ludoviciana</taxon_name>
    <taxon_name authority="(Richardson) S. Watson" date="unknown" rank="variety">arenosa</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species ludoviciana;variety arenosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or, rarely, annuals;</text>
      <biological_entity id="o9611" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>± densely pubescent, trichomes (sessile or short-stalked), few-rayed, rays (usually spreading), distinct or slightly fused at base, furcate or bifurcate, (tuberculate).</text>
      <biological_entity id="o9613" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9614" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="few-rayed" value_original="few-rayed" />
      </biological_entity>
      <biological_entity id="o9615" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o9616" is_modifier="false" modifier="slightly" name="fusion" src="d0_s2" value="fused" value_original="fused" />
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
      <biological_entity id="o9616" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few from base, prostrate or straggling to erect, (sometimes purplish, usually unbranched), (0.5–) 1–2 (–3) dm.</text>
      <biological_entity id="o9617" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o9618" is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s3" value="prostrate" value_original="prostrate" />
        <character name="growth_form_or_orientation" src="d0_s3" value="straggling to erect" value_original="straggling to erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o9618" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade oblanceolate, 1.5–5 (–7) cm, margins entire or shallowly dentate, (flat).</text>
      <biological_entity constraint="basal" id="o9619" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9620" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9621" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves: blade elliptic to linear, (0.5–) 1–2.5 (–3) cm, margins usually entire.</text>
      <biological_entity constraint="cauline" id="o9622" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9623" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic to linear" value_original="elliptic to linear" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9624" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes (secund), dense, (elongated in fruit).</text>
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels (usually sharply recurved, sometimes divaricate-spreading or nearly horizontal), 5–15 (–20) mm, (stout).</text>
      <biological_entity id="o9625" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9626" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o9625" id="r680" name="fruiting" negation="false" src="d0_s7" to="o9626" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals elliptic or oblong, 4–6 (–7) mm, (lateral pair subsaccate, median pair thickened apically, cucullate);</text>
      <biological_entity id="o9627" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o9628" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (often red or lavender when dried), obovate, 6–8.5 (–9.5) mm, (narrowing to broad claw).</text>
      <biological_entity id="o9629" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9630" name="petal" name_original="petals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruits subglobose, obovoid, or broadly ellipsoid, slightly inflated, (3.5–) 4–5.5 (–6.5) mm;</text>
      <biological_entity id="o9631" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves densely pubescent outside, trichomes spreading or closely appressed, rarely sparsely pubescent inside;</text>
      <biological_entity id="o9632" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9633" name="trichome" name_original="trichomes" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="closely" name="orientation" src="d0_s11" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules (4–) 8 (–10) per ovary;</text>
      <biological_entity id="o9634" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="10" />
        <character constraint="per ovary" constraintid="o9635" name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o9635" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style (slender), 3–5.5 (–6.5) mm.</text>
      <biological_entity id="o9636" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds slightly flattened.</text>
      <biological_entity id="o9637" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Colo., Mont., N.Dak., Nebr., S.Dak., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Great Plains bladderpod</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials or, rarely, annuals, short-lived; fruit valves: trichomes spreading.</description>
      <determination>6a Physaria arenosa subsp. arenosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials, long-lived; fruit valves: trichomes closely appressed.</description>
      <determination>6b Physaria arenosa subsp. argillosa</determination>
    </key_statement>
  </key>
</bio:treatment>