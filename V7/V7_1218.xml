<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">702</other_info_on_meta>
    <other_info_on_meta type="treatment_page">707</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="J. L. Morrison" date="1938" rank="species">callistus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>4: 205, plate 31, figs. 1–10. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species callistus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095137</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>(somewhat glaucous), hirsute.</text>
      <biological_entity id="o40992" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or branched, (often bristly proximally), 0.2–0.9 dm.</text>
      <biological_entity id="o40993" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="0.9" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>not rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o40994" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblong-orbicular to obovate, 0.7–1.5 cm, margins dentate.</text>
      <biological_entity id="o40995" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong-orbicular" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o40996" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade broadly ovate to obovate, 0.8–1.7 cm × 4–13 mm (smaller distally), base amplexicaul, margins dentate.</text>
      <biological_entity constraint="cauline" id="o40997" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o40998" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s7" to="obovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s7" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40999" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o41000" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (with a terminal cluster of sterile flowers).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate, 2–3 mm.</text>
      <biological_entity id="o41001" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41002" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o41001" id="r2779" name="fruiting" negation="false" src="d0_s9" to="o41002" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx narrowly campanulate;</text>
      <biological_entity id="o41003" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o41004" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals green to purplish or (sterile flowers) lilac-purple, (narrowly ovate, sterile flowers linear-lanceolate), 3–5 mm, (8–13 mm in sterile flowers), keeled basally (not keeled in sterile flowers; sparsely hirsute in fertile flowers, glabrous in sterile flowers);</text>
      <biological_entity id="o41005" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o41006" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s11" to="purplish or lilac-purple" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals purple (with darker purple veins), 8–11 mm, blade 4–6 × 2.5–3.5 mm, margins not crisped, (flaring), claw 4–5 mm, narrower than blade;</text>
      <biological_entity id="o41007" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o41008" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41009" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41010" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o41011" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character constraint="than blade" constraintid="o41012" is_modifier="false" name="width" src="d0_s12" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o41012" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens in 3 unequal pairs, (purple);</text>
      <biological_entity id="o41013" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o41014" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o41015" name="pair" name_original="pairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o41014" id="r2780" name="in" negation="false" src="d0_s13" to="o41015" />
    </statement>
    <statement id="d0_s14">
      <text>filaments: abaxial pair (connate), 4–5 mm, lateral pair 2–3 mm, adaxial pair (connate nearly to apex), 5–6.5 mm;</text>
      <biological_entity id="o41016" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial" id="o41017" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41018" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers: abaxial and lateral pairs fertile, 1.4–1.8 mm, adaxial pair sterile, 0.4–0.8 mm;</text>
      <biological_entity id="o41019" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41020" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.1–0.3 mm.</text>
      <biological_entity id="o41021" name="anther" name_original="anthers" src="d0_s16" type="structure" />
      <biological_entity id="o41022" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits divaricate, smooth, curved upward, slightly flattened, 1.3–2.5 cm × 2.5–3.5 mm;</text>
      <biological_entity id="o41023" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s17" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s17" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein, (hispid, trichomes setiform, 0.5–0.8 mm);</text>
      <biological_entity id="o41024" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o41025" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o41024" id="r2781" name="with" negation="false" src="d0_s18" to="o41025" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o41026" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 46–60 per ovary;</text>
      <biological_entity id="o41027" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o41028" from="46" name="quantity" src="d0_s20" to="60" />
      </biological_entity>
      <biological_entity id="o41028" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.2–0.5 mm;</text>
      <biological_entity id="o41029" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o41030" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds ovoid (plump), 1.2–1.5 × 0.6–0.8 mm;</text>
      <biological_entity id="o41031" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s23" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s23" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing absent.</text>
    </statement>
    <statement id="d0_s25">
      <text>2n = 28.</text>
      <biological_entity id="o41032" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character is_modifier="false" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o41033" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly sedimentary scree and lag-barrens in chaparral-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral-oak woodlands" modifier="gravelly sedimentary scree and lag-barrens in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus callistus is known from the Mount Hamilton Range of Santa Clara County and is considered to be the most endangered species in the genus.</discussion>
  
</bio:treatment>