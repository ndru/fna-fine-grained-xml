<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="treatment_page">182</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">limnanthaceae</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="genus">limnanthes</taxon_name>
    <taxon_name authority="C. T. Mason" date="1952" rank="section">Inflexae</taxon_name>
    <taxon_name authority="Howell" date="1897" rank="species">floccosa</taxon_name>
    <taxon_name authority="(M. Peck) C. T. Mason" date="1952" rank="subspecies">bellingeriana</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>25: 501. 1952</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family limnanthaceae;genus limnanthes;section inflexae;species floccosa;subspecies bellingeriana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095174</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limnanthes</taxon_name>
    <taxon_name authority="M. Peck" date="unknown" rank="species">bellingeriana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>50: 93. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Limnanthes;species bellingeriana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage glabrous.</text>
      <biological_entity id="o633" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o634" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers urn-shaped;</text>
      <biological_entity id="o635" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="urn--shaped" value_original="urn--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals accrescent, narrowly ovate, 5.5–8 mm, abaxially and adaxially glabrous or sparsely hairy;</text>
      <biological_entity id="o636" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially; adaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals oblong, 5.5–7.5 mm, apex retuse, (without marginal hairs basally);</text>
      <biological_entity id="o637" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s4" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o638" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments 2–3.5 mm;</text>
      <biological_entity id="o639" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers ± 0.5 mm, usually dehiscing introrsely;</text>
      <biological_entity id="o640" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="usually; introrsely" name="dehiscence" src="d0_s6" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 2.3–2.5 mm.</text>
      <biological_entity id="o641" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Nutlets 1 or 2 (3), tubercles platelike.</text>
      <biological_entity id="o642" name="nutlet" name_original="nutlets" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o643" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plate-like" value_original="platelike" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernal pool edges in shallow soil of rocky meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vernal pool edges" constraint="in shallow soil of rocky meadows" />
        <character name="habitat" value="shallow soil" constraint="of rocky meadows" />
        <character name="habitat" value="rocky meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7b.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>D. Southworth and J. Seevers (1997) compared pollen of subspp. bellingeriana and floccosa. Pollen of Limnanthes floccosa has a striated zone perpendicular to the apertural band; in subsp. bellingeriana, the striations are criss-crossed and in subsp. floccosa the striations are parallel. Southworth and Seevers reported that the two subspecies could be easily distinguished in the field in mixed populations based on the density of hairs on the sepals. In subsp. floccosa, the hairs are so dense that they prevent the sepals from fully opening; in subsp. bellingeriana, the sepal hairs are relatively short (50 µm) and sparse. Based on the absence of intermediates in these characters, Southworth and Seevers recommended elevating subsp. bellingeriana to species level. Allozyme studies (C. I. McNeill and S. K. Jain 1983) group subspp. bellingeriana and floccosa together, and subspp. californica, grandiflora, and pumila together. Also, in ITS analysis (M. S. Plotkin 1998), subsp. bellingeriana is nested within the floccosa clade. Crosses between subspp. bellingeriana and floccosa resulted in viable hybrids (R. Ornduff 1971). Subspecies bellingeriana is found in shaded, damp edges of meadows; sympatric subsp. floccosa occurs on the upper, dry, exposed edges of meadows (M. T. K. Arroyo 1973). On balance, I prefer to retain this taxon as a subspecies within L. floccosa. Subspecies bellingeriana is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <references>
    <reference>Southworth, D. and J. Seevers. 1997. Taxonomic status of Limnanthes floccosa subsp. bellingeriana (Limnanthaceae). In: T. N. Kaye et al., eds. 1997. Conservation of Native Plants and Fungi. Corvallis. Pp. 147–152.</reference>
  </references>
  
</bio:treatment>