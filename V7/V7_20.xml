<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="treatment_page">32</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="Kimura" date="1928" rank="subgenus">protitea</taxon_name>
    <taxon_name authority="Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">Humboldtianae</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>16(2): 199. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus protitea;section Humboldtianae</taxon_hierarchy>
    <other_info_on_name type="fna_id">317493</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1–30 m.</text>
      <biological_entity id="o37903" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branches somewhat brittle, highly brittle, or flexible at base.</text>
      <biological_entity id="o37905" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character is_modifier="false" modifier="highly" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="highly" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
        <character constraint="at base" constraintid="o37906" is_modifier="false" name="fragility" src="d0_s1" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity id="o37906" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole with pairs or clusters of spherical glands distally;</text>
      <biological_entity id="o37907" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o37908" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o37909" name="pair" name_original="pairs" src="d0_s2" type="structure" />
      <biological_entity id="o37910" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="spherical" value_original="spherical" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="cluster" value_original="cluster" />
      </biological_entity>
      <relation from="o37908" id="r2558" name="with" negation="false" src="d0_s2" to="o37909" />
      <relation from="o37908" id="r2559" name="with" negation="false" src="d0_s2" to="o37910" />
    </statement>
    <statement id="d0_s3">
      <text>largest medial blade relatively narrow (6–37 mm wide), margins flat, (glands marginal), abaxial surface glabrous, glabrescent, puberulent, pubescent, or pilose.</text>
      <biological_entity id="o37911" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="largest medial" id="o37912" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="relatively" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o37913" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o37914" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate catkins slender, (5–16 mm wide).</text>
      <biological_entity id="o37915" name="catkin" name_original="catkins" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers: ovary glabrous or villous.</text>
      <biological_entity id="o37916" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o37917" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a2.</number>
  <discussion>Species 15 (6 in the flora).</discussion>
  
</bio:treatment>