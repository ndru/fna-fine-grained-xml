<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">667</other_info_on_meta>
    <other_info_on_meta type="treatment_page">670</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">sisymbrieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisymbrium</taxon_name>
    <taxon_name authority="(Linnaeus) Scopoli" date="1772" rank="species">officinale</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carniol. ed.</publication_title>
      <place_in_publication>2, 2: 26. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe sisymbrieae;genus sisymbrium;species officinale</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200009684</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">officinale</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 660. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erysimum;species officinale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">officinale</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="variety">leiocarpum</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species officinale;variety leiocarpum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous or pubescent.</text>
      <biological_entity id="o6831" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, branched distally, 2.5–7.5 (–11) dm, usually sparsely to densely hirsute, (trichomes retrorse), rarely glabrate distally.</text>
      <biological_entity id="o6832" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="11" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s2" to="7.5" to_unit="dm" />
        <character is_modifier="false" modifier="usually sparsely; sparsely to densely" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="rarely; distally" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves usually rosulate;</text>
      <biological_entity constraint="basal" id="o6833" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (1–) 2–7 (–10) cm;</text>
      <biological_entity id="o6834" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade broadly oblanceolate or oblong-obovate (in outline), (2–) 3–10 (–15) cm × (10–) 20–50 (–80) mm, margins lyrate-pinnatifid, pinnatisect, or runcinate;</text>
      <biological_entity id="o6835" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-obovate" value_original="oblong-obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6836" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="lyrate-pinnatifid" value_original="lyrate-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="runcinate" value_original="runcinate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="runcinate" value_original="runcinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lobes (2) 3 or 4 (5) on each side, oblong or lanceolate, smaller than terminal lobe, margins entire, dentate, or lobed, (terminal lobe suborbicular or deltate, margins dentate).</text>
      <biological_entity id="o6837" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character name="atypical_quantity" src="d0_s6" value="2" value_original="2" />
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character constraint="on side" constraintid="o6838" name="atypical_quantity" src="d0_s6" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character constraint="than terminal lobe" constraintid="o6839" is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o6838" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity constraint="terminal" id="o6839" name="lobe" name_original="lobe" src="d0_s6" type="structure" />
      <biological_entity id="o6840" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves similar to basal;</text>
      <biological_entity constraint="cauline" id="o6841" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o6842" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o6841" id="r498" name="to" negation="false" src="d0_s7" to="o6842" />
    </statement>
    <statement id="d0_s8">
      <text>blade with lobe margins dentate or subentire.</text>
      <biological_entity constraint="lobe" id="o6844" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subentire" value_original="subentire" />
      </biological_entity>
      <relation from="o6843" id="r499" name="with" negation="false" src="d0_s8" to="o6844" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels erect, (appressed to rachis), stout, narrower than fruit, 1.5–3 (–4) mm.</text>
      <biological_entity id="o6843" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character constraint="than fruit" constraintid="o6846" is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o6845" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o6846" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o6843" id="r500" name="fruiting" negation="false" src="d0_s9" to="o6845" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect, oblong-ovate, 2–2.5 × ca. 1 mm;</text>
      <biological_entity id="o6847" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6848" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals spatulate, 2.5–4 × 1–2 mm, claw 1–2 mm;</text>
      <biological_entity id="o6849" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6850" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6851" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments (erect, yellowish), 2–3 mm;</text>
      <biological_entity id="o6852" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6853" name="filament" name_original="filaments" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers ovate, 0.3–0.5 mm.</text>
      <biological_entity id="o6854" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6855" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits (erect), subulate-linear, straight, slightly torulose or smooth, stout, (0.7–) 1–1.4 (–1.8) cm × 1–1.5 mm;</text>
      <biological_entity id="o6856" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="subulate-linear" value_original="subulate-linear" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s14" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s14" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s14" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous or pubescent;</text>
      <biological_entity id="o6857" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 10–20 per ovary;</text>
      <biological_entity id="o6858" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o6859" from="10" name="quantity" src="d0_s16" to="20" />
      </biological_entity>
      <biological_entity id="o6859" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style (0.8–) 1–1.5 (–2) mm;</text>
      <biological_entity id="o6860" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o6861" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1–1.3 × 0.5–0.6 mm. 2n = 14.</text>
      <biological_entity id="o6862" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6863" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr-late Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, fields, pastures, waste grounds, deserts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="waste grounds" />
        <character name="habitat" value="deserts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Yukon; Ala., Alaska, Ark., Calif., Conn., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis.; Europe; Asia; n Africa; introduced also in Central America, South America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  
</bio:treatment>