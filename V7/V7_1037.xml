<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">621</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">651</other_info_on_meta>
    <other_info_on_meta type="treatment_page">632</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(A. Nelson) Grady &amp; O’Kane" date="2007" rank="species">curvipes</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>17: 183. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species curvipes</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095121</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">curvipes</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 205. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species curvipes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o10633" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes (often wavy, closely appressed to blade surfaces), 4–5-rayed, rays furcate or bifurcate, slightly fused near base, (tuberculate throughout).</text>
      <biological_entity id="o10634" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10635" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o10636" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
        <character constraint="near base" constraintid="o10637" is_modifier="false" modifier="slightly" name="fusion" src="d0_s2" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o10637" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base, loosely spreading, usually decumbent, (well exserted from basal leaves, often reddish purple), 0.8–2.4 dm.</text>
      <biological_entity id="o10638" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o10639" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="loosely" name="orientation" notes="" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s3" to="2.4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o10639" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade (erect), spatulate to nearly rhombic, 2.5–5 (–9) cm, (base gradually narrowed to petiole), margins entire, (flat).</text>
      <biological_entity constraint="basal" id="o10640" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" notes="" src="d0_s4" to="nearly rhombic" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10641" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o10642" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves: blade spatulate, similar to basal, margins entire.</text>
      <biological_entity constraint="cauline" id="o10643" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o10644" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10645" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o10646" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o10644" id="r743" name="to" negation="false" src="d0_s5" to="o10645" />
    </statement>
    <statement id="d0_s6">
      <text>Racemes loose, (elongated, exceeding basal leaves).</text>
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels (ascending, curved or sigmoid), 4–7 mm.</text>
      <biological_entity id="o10647" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10648" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o10647" id="r744" name="fruiting" negation="false" src="d0_s7" to="o10648" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals (pale-yellow), lingulate to spatulate, 3.5–4 mm;</text>
      <biological_entity id="o10649" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="lingulate" name="shape" notes="" src="d0_s8" to="spatulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10650" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals narrowly oblanceolate, 4–6 mm.</text>
      <biological_entity id="o10651" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10652" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits ellipsoid, not inflated (strongly latiseptate, more so at apex), (3–) 5–9 mm;</text>
      <biological_entity id="o10653" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves pubescent, trichomes closely appressed to surface;</text>
      <biological_entity id="o10654" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10655" name="trichome" name_original="trichomes" src="d0_s11" type="structure">
        <character constraint="to surface" constraintid="o10656" is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o10656" name="surface" name_original="surface" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>ovules 4–8 per ovary;</text>
      <biological_entity id="o10657" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10658" from="4" name="quantity" src="d0_s12" to="8" />
      </biological_entity>
      <biological_entity id="o10658" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style 2.5–4.5 mm (never more than 1/2 fruit length).</text>
      <biological_entity id="o10659" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds plump.</text>
      <biological_entity id="o10660" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Curved bladderpod</other_name>
  <discussion>Physaria curvipes is known from the Big Horn Mountains.</discussion>
  
</bio:treatment>