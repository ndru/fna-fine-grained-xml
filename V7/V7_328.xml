<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="treatment_page">252</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">alysseae</taxon_name>
    <taxon_name authority="de Candolle" date="1821" rank="genus">BERTEROA</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Mus. Hist. Nat.</publication_title>
      <place_in_publication>7: 232: 290. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe alysseae;genus BERTEROA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Carlo Giuseppe Bertero, 1789–1831, Italian physician and botanist who settled in Chile</other_info_on_name>
    <other_info_on_name type="fna_id">103860</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials [perennials];</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>pubescent, trichomes stellate, mixed with simple ones.</text>
      <biological_entity id="o15679" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o15681" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
        <character constraint="with ones" constraintid="o15682" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o15682" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect [ascending], usually branched distally.</text>
      <biological_entity id="o15683" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o15684" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal not rosulate, petiolate, blade margins entire or repand [dentate, sinuate];</text>
      <biological_entity constraint="basal" id="o15685" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o15686" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline (middle and distal) sessile.</text>
      <biological_entity constraint="cauline" id="o15687" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose, several-flowered, dense), considerably elongated in fruit.</text>
      <biological_entity id="o15689" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels erect or divaricate, slender.</text>
      <biological_entity id="o15688" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o15689" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o15690" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o15688" id="r1095" name="fruiting" negation="false" src="d0_s9" to="o15690" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect-ascending [suberect, spreading], oblong, lateral pair not saccate basally;</text>
      <biological_entity id="o15691" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15692" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals usually white, rarely yellow, obcordate, apex deeply 2-fid;</text>
      <biological_entity id="o15693" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15694" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obcordate" value_original="obcordate" />
      </biological_entity>
      <biological_entity id="o15695" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o15696" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15697" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments: median pair flattened basally, unappendaged, [laterally 1-toothed], lateral pair with basal toothlike appendage;</text>
      <biological_entity id="o15698" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="median" value_original="median" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="unappendaged" value_original="unappendaged" />
        <character constraint="with basal appendage" constraintid="o15699" is_modifier="false" name="position" src="d0_s13" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15699" name="appendage" name_original="appendage" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="toothlike" value_original="toothlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers oblong, (apex obtuse);</text>
      <biological_entity id="o15700" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity id="o15701" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands (4), lateral, 1 on each side of lateral stamen.</text>
      <biological_entity id="o15702" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o15703" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o15704" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15704" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity constraint="lateral" id="o15705" name="stamen" name_original="stamen" src="d0_s15" type="structure" />
      <relation from="o15704" id="r1096" name="part_of" negation="false" src="d0_s15" to="o15705" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits sessile, oblong, or elliptic [ovate, obovate, or orbicular], smooth, slightly inflated [or not inflated], latiseptate;</text>
      <biological_entity id="o15706" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each not veined or with obscure midvein, stellate-hairy [glabrous];</text>
      <biological_entity id="o15707" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="veined" value_original="veined" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="with obscure midvein" value_original="with obscure midvein" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o15708" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s17" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o15707" id="r1097" name="with" negation="false" src="d0_s17" to="o15708" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o15709" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete, (membranous);</text>
      <biological_entity id="o15710" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 4–16 per ovary;</text>
      <biological_entity id="o15711" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o15712" from="4" name="quantity" src="d0_s20" to="16" />
      </biological_entity>
      <biological_entity id="o15712" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>stigma capitate, obscurely 2-lobed.</text>
      <biological_entity id="o15713" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s21" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds biseriate, flattened [plump], margined [winged or not], lenticular or ovoid-lenticular [suborbicular];</text>
      <biological_entity id="o15714" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s22" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="margined" value_original="margined" />
        <character is_modifier="false" name="shape" src="d0_s22" value="lenticular" value_original="lenticular" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid-lenticular" value_original="ovoid-lenticular" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat (minutely reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o15715" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 8.</text>
      <biological_entity id="o15716" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s24" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o15717" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Hoary-alyssum</other_name>
  <discussion>Species 5 (1 in the flora).</discussion>
  <discussion>Berteroa mutabilis (Ventenat) de Candolle, native to northeastern Italy, the Balkan Peninsula, and northern Turkey, is known in North America only from a handful of collections almost all made more than a century ago as garden escapes in Kansas and Massachusetts. Although it was included by R. C. Rollins (1993), who indicated that it had not been collected for 60 years, the species apparently did not become naturalized in North America and, therefore, is not included here. From B. incana, B. mutabilis is easily distinguished by having winged instead of margined seeds, and flat and glabrous versus inflated and pubescent fruits. As indicated by I. A. Al-Shehbaz (1987), the record of B. obliqua (Smith) de Candolle from the Catskill region, New York, was based on misidentified plants of B. incana.</discussion>
  
</bio:treatment>