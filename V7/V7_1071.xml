<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">643</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Munz) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">hitchcockii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">hitchcockii</taxon_name>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species hitchcockii;subspecies hitchcockii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095202</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming tufts;</text>
      <biological_entity id="o25909" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o25910" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <relation from="o25909" id="r1758" name="forming" negation="false" src="d0_s0" to="o25910" />
    </statement>
    <statement id="d0_s1">
      <text>caudex not elongated, not elastic.</text>
      <biological_entity id="o25911" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="length" src="d0_s1" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="elastic" value_original="elastic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole differentiated from blade (sometimes weakly);</text>
      <biological_entity constraint="basal" id="o25912" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o25913" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character constraint="from blade" constraintid="o25914" is_modifier="false" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o25914" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate to obovate.</text>
      <biological_entity constraint="basal" id="o25915" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o25916" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Anthers 1.4–1.8 (–2) mm.</text>
      <biological_entity id="o25917" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s4" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits 2.6–3.8 mm wide.</text>
      <biological_entity id="o25918" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s5" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly or rocky limestone at or above timberline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky limestone" />
        <character name="habitat" value="timberline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2300-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40a.</number>
  <discussion>It is possible that populations of subsp. hitchcockii on the Table Cliff Plateau, Utah, are consubspecific with the nearby subsp. rubicundula. The leaf blades are indistinguishable from the material from Nevada and the plants do not form elongated, elastic caudices.</discussion>
  <discussion>Subspecies hitchcockii is found in the Sheep Range and Spring Mountains (Charleston Mountain), Nevada, and on the Table Cliff Plateau, Utah, where it is limited to the white member of the limestone Wasatch (Claron) Formation.</discussion>
  
</bio:treatment>