<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
    <other_info_on_meta type="illustration_page">508</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Ledebour_ C. A. Meyer &amp; Bunge in C. F. von Ledebour" date="unknown" rank="tribe">chorisporeae</taxon_name>
    <taxon_name authority="R. Brown ex de Candolle" date="unknown" rank="genus">chorispora</taxon_name>
    <taxon_name authority="(Pallas) de Candolle" date="1821" rank="species">tenella</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 435. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe chorisporeae;genus chorispora;species tenella</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009368</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Raphanus</taxon_name>
    <taxon_name authority="Pallas" date="unknown" rank="species">tenellus</taxon_name>
    <place_of_publication>
      <publication_title>Reise Russ. Reich.</publication_title>
      <place_in_publication>3: 741. 1776</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Raphanus;species tenellus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chorispermum</taxon_name>
    <taxon_name authority="(Pallas) R. Brown" date="unknown" rank="species">tenellum</taxon_name>
    <taxon_hierarchy>genus Chorispermum;species tenellum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants papillose, sometimes pubescent, papillae sometimes mixed with simple trichomes.</text>
      <biological_entity id="o19715" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="relief" src="d0_s0" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19716" name="papilla" name_original="papillae" src="d0_s0" type="structure">
        <character constraint="with trichomes" constraintid="o19717" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s0" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o19717" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (0.5–) 1–4 (–5.6) dm.</text>
      <biological_entity id="o19718" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="5.6" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves (often withered by flowering);</text>
      <biological_entity constraint="basal" id="o19719" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (0.5–) 1–2 (–4) cm;</text>
      <biological_entity id="o19720" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate or oblong, (1.5–) 2.5–8 (–13) cm × (4–) 8–20 (–30) mm, base cuneate or attenuate, apex acute, surfaces glandular.</text>
      <biological_entity id="o19721" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s4" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19722" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o19723" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o19724" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves similar to basal, distalmost subsessile, blades smaller distally.</text>
      <biological_entity constraint="cauline" id="o19725" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o19726" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="distalmost" id="o19727" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <relation from="o19725" id="r1341" name="to" negation="false" src="d0_s5" to="o19726" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels (2–) 3–5 mm, glandular.</text>
      <biological_entity id="o19728" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19729" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o19728" id="r1342" name="fruiting" negation="false" src="d0_s6" to="o19729" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals purplish, (3–) 4–5 (–6) × 0.5–0.7 mm;</text>
      <biological_entity id="o19730" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19731" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s7" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals 8–10 (–12) × 1–2 mm, claw 6–7 mm;</text>
      <biological_entity id="o19732" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19733" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19734" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 4–6 (–7) mm;</text>
      <biological_entity id="o19735" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19736" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers ca. 1.5 mm.</text>
      <biological_entity id="o19737" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19738" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits slightly curved-ascending, (1.4–) 1.8–2.5 (–3) cm × 1.5–2 mm, with 8–12 constrictions on each side;</text>
      <biological_entity id="o19739" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="curved-ascending" value_original="curved-ascending" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="atypical_length" src="d0_s11" to="1.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s11" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="length" src="d0_s11" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19740" name="constriction" name_original="constrictions" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
      <biological_entity id="o19741" name="side" name_original="side" src="d0_s11" type="structure" />
      <relation from="o19739" id="r1343" name="with" negation="false" src="d0_s11" to="o19740" />
      <relation from="o19740" id="r1344" name="on" negation="false" src="d0_s11" to="o19741" />
    </statement>
    <statement id="d0_s12">
      <text>style (6–) 10–18 (–22) mm.</text>
      <biological_entity id="o19742" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="22" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, 1–1.4 × 0.8–1 mm. 2n = 14.</text>
      <biological_entity id="o19743" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19744" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, pastures, fields, roadsides, railroad embankments, grassy slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad embankments" />
        <character name="habitat" value="grassy slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Sask.; Ariz., Calif., Colo., Idaho, Ill., Ind., Iowa, Kans., La., Mass., Mich., Mo., Mont., Nebr., Nev., N.Mex., N.Y., N.Dak., Ohio, Okla., Oreg., S.Dak., Tenn., Tex., Utah, Wash., Wyo.; Europe; Asia; n Africa; introduced also in South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Chorispora tenella appears to be most widely distributed in Colorado, Nevada, and Wyoming, of all the provinces and states listed above.</discussion>
  
</bio:treatment>