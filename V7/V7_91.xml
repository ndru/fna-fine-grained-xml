<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="(Borrer) Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">myrtilloides</taxon_name>
    <taxon_name authority="Argus" date="1974" rank="species">raupii</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>52: 1303, plate 1. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section myrtilloides;species raupii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242445848</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1.2–1.8 m, not clonal.</text>
      <biological_entity id="o13469" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.2" from_unit="m" name="some_measurement" src="d0_s0" to="1.8" to_unit="m" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o13470" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches gray-brown, glabrous;</text>
      <biological_entity id="o13471" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown, glabrous.</text>
      <biological_entity id="o13472" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules foliaceous;</text>
      <biological_entity id="o13473" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13474" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 5–9 mm;</text>
      <biological_entity id="o13475" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o13476" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade narrowly elliptic, 32–58 × 12–19 mm, 2–3.3 times as long as wide, base cuneate or convex, margins slightly revolute, entire, apex acute to acuminate, abaxial surface glabrous, adaxial slightly glossy, glabrous;</text>
      <biological_entity id="o13477" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o13478" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="32" from_unit="mm" name="length" src="d0_s6" to="58" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s6" to="19" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="2-3.3" value_original="2-3.3" />
      </biological_entity>
      <biological_entity id="o13479" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o13480" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13481" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13482" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13483" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins shallowly serrulate;</text>
      <biological_entity id="o13484" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o13485" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade glabrous.</text>
      <biological_entity id="o13486" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o13487" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 17.5–42 × 5–13 mm, flowering branchlet 6–7 mm;</text>
      <biological_entity id="o13488" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="17.5" from_unit="mm" name="length" src="d0_s9" to="42" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13489" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate moderately densely flowered, stout, 20–40 × 6–12 mm, flowering branchlet 4–7 mm;</text>
      <biological_entity id="o13490" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="40" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13491" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract tawny or bicolor, 1.3–2.5 mm, apex rounded, entire, abaxially glabrous.</text>
      <biological_entity id="o13492" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o13493" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13494" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary 0.3–0.8 mm, adaxial nectary narrowly oblong, 0.6–1 mm, nectaries distinct;</text>
      <biological_entity id="o13495" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13496" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13497" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13498" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous;</text>
      <biological_entity id="o13499" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o13500" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ellipsoid, shortly cylindrical, or globose, 0.4–0.7 mm.</text>
      <biological_entity id="o13501" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o13502" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: abaxial nectary absent, adaxial nectary narrowly oblong or oblong, 0.5–1.1 mm, equal to or longer than stipe;</text>
      <biological_entity id="o13503" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13504" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o13505" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s15" value="equal" value_original="equal" />
        <character constraint="than stipe" constraintid="o13506" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o13506" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.4–1.2 mm;</text>
      <biological_entity id="o13507" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13508" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, usually glabrous, rarely puberulent, beak slightly bulged below styles;</text>
      <biological_entity id="o13509" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13510" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o13511" name="beak" name_original="beak" src="d0_s17" type="structure" />
      <biological_entity id="o13512" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o13511" id="r937" modifier="below" name="bulged" negation="false" src="d0_s17" to="o13512" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 12 per ovary;</text>
      <biological_entity id="o13513" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13514" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character constraint="per ovary" constraintid="o13515" name="quantity" src="d0_s18" value="12" value_original="12" />
      </biological_entity>
      <biological_entity id="o13515" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.6–0.8 mm;</text>
      <biological_entity id="o13516" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13517" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially not papillate with rounded tip, or broadly cylindrical, 0.3–0.5 mm.</text>
      <biological_entity id="o13518" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o13519" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o13520" is_modifier="false" modifier="abaxially not" name="relief" src="d0_s20" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s20" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13520" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 4.4–8 mm.</text>
      <biological_entity id="o13521" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s21" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jun" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets in moist, open forests, gravel floodplains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" constraint="in moist" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="gravel floodplains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <other_name type="common_name">Raup willow</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Salix raupii resembles glabrous S. glauca var. villosa. Thin-layer chromatography of leaf phenolics in S. raupii revealed a pattern similar to those of S. glauca vars. villosa and acutifolia and S. athabascensis (G. W. Argus, unpubl.). Based on overall similarity, its nearest neighbors are S. glauca, in a broad sense, and S. athabascensis (Argus 1997). The sectional placement of this species is uncertain. It is placed here in sect. Myrtilloides because it clusters with S. athabascensis, but it is evidently close to S. glauca and may be a species of intersectional hybrid origin.</discussion>
  
</bio:treatment>