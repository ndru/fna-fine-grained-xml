<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">645</other_info_on_meta>
    <other_info_on_meta type="treatment_page">646</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(S. Watson) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">kingii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">kingii</taxon_name>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species kingii;subspecies kingii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095199</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate and straggling to erect;</text>
      <biological_entity id="o7804" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="straggling" name="orientation" src="d0_s0" to="erect" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trichomes tuberculate throughout, center low-mounded.</text>
      <biological_entity id="o7805" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="throughout" name="relief" src="d0_s1" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o7806" name="center" name_original="center" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="low-mounded" value_original="low-mounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: blade margins ± entire, (sometimes slightly lobed or widened at base).</text>
      <biological_entity constraint="basal" id="o7807" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="blade" id="o7808" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes not secund, loose and elongated in fruit, or dense in alpine forms.</text>
      <biological_entity id="o7810" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
      <biological_entity id="o7811" name="form" name_original="forms" src="d0_s3" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s3" value="alpine" value_original="alpine" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels usually sigmoid.</text>
      <biological_entity id="o7809" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="secund" value_original="secund" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character constraint="in fruit" constraintid="o7810" is_modifier="false" name="length" src="d0_s3" value="elongated" value_original="elongated" />
        <character constraint="in forms" constraintid="o7811" is_modifier="false" name="density" notes="" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="usually" name="course_or_shape" src="d0_s4" value="sigmoid" value_original="sigmoid" />
      </biological_entity>
      <biological_entity id="o7812" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o7809" id="r570" name="fruiting" negation="false" src="d0_s4" to="o7812" />
    </statement>
    <statement id="d0_s5">
      <text>Petals yellow.</text>
      <biological_entity id="o7813" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits slightly wider than long, apex truncate or retuse;</text>
      <biological_entity id="o7814" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="slightly wider than long" value_original="slightly wider than long" />
      </biological_entity>
      <biological_entity id="o7815" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>valves pubescent inside;</text>
      <biological_entity id="o7816" name="valve" name_original="valves" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>septum ± perforate;</text>
      <biological_entity id="o7817" name="septum" name_original="septum" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovules 4 (–8) per ovary;</text>
      <biological_entity id="o7818" name="ovule" name_original="ovules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character constraint="per ovary" constraintid="o7819" name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o7819" name="ovary" name_original="ovary" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>style to 7 (–9) mm.</text>
      <biological_entity id="o7820" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granitic ridges, quartz and limestone chip, stream gravels, dry slopes, calcareous soils, sagebrush hillsides, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granitic ridges" />
        <character name="habitat" value="quartz" />
        <character name="habitat" value="limestone chip" />
        <character name="habitat" value="stream gravels" />
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="calcareous soils" />
        <character name="habitat" value="sagebrush hillsides" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44a.</number>
  
</bio:treatment>