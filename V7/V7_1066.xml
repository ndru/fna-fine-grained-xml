<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="treatment_page">642</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="C. V. Morton" date="1937" rank="species">grahamii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Carnegie Mus.</publication_title>
      <place_in_publication>26: 220. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species grahamii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094987</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physaria</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">acutifolia</taxon_name>
    <taxon_name authority="S. L. Welsh &amp; Reveal" date="unknown" rank="variety">purpurea</taxon_name>
    <taxon_hierarchy>genus Physaria;species acutifolia;variety purpurea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physaria</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">acutifolia</taxon_name>
    <taxon_name authority="(Rollins) S. L. Welsh" date="unknown" rank="variety">repanda</taxon_name>
    <taxon_hierarchy>genus Physaria;species acutifolia;variety repanda;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">repanda</taxon_name>
    <taxon_hierarchy>genus Physaria;species repanda;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o21187" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched, (thick, cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes rays (appressed on leaves, ascending on pedicels and fruits), distinct, furcate or bifurcate.</text>
      <biological_entity id="o21188" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="trichomes" id="o21189" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems several from base, decumbent to erect or ascending (unbranched), 1–2.5 dm.</text>
      <biological_entity id="o21190" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o21191" is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="decumbent" name="orientation" notes="" src="d0_s3" to="erect or ascending" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o21191" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: (outer ones spreading, inner erect or ascending);</text>
      <biological_entity constraint="basal" id="o21192" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate, often broadly so, 4–7 cm, margins repand to lyrate-lobed.</text>
      <biological_entity constraint="basal" id="o21193" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21194" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="often broadly; broadly" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21195" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="repand" name="shape" src="d0_s5" to="lyrate-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves similar to basal, blade oblanceolate or narrowly oblong, reduced in size, (base gibbous).</text>
      <biological_entity constraint="cauline" id="o21196" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o21197" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o21198" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o21196" id="r1467" name="to" negation="false" src="d0_s6" to="o21197" />
    </statement>
    <statement id="d0_s7">
      <text>Racemes loose, (elongated).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (ascending to divaricate-ascending, sigmoid to nearly straight), 10–17 mm.</text>
      <biological_entity id="o21199" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s7" value="loose" value_original="loose" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21200" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o21199" id="r1468" name="fruiting" negation="false" src="d0_s8" to="o21200" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals lanceolate or narrowly oblong, 5.8–7.2 mm;</text>
      <biological_entity id="o21201" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o21202" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="5.8" from_unit="mm" name="some_measurement" src="d0_s9" to="7.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (erect, sometime purplish or drying purple), narrowly oblong to oblanceolate, 7–10 mm, (not or weakly clawed).</text>
      <biological_entity id="o21203" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" notes="" src="d0_s10" to="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21204" name="petal" name_original="petals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits didymous, globose or subglobose, inflated, 10–13 mm, (papery, basal and apical sinuses deep);</text>
      <biological_entity id="o21205" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s11" value="didymous" value_original="didymous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves (retaining seeds after dehiscence), pubescent, trichomes ascending, appearing fuzzy;</text>
      <biological_entity id="o21206" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21207" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="fuzzy" value_original="fuzzy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum oblong to oblanceolate, as wide as or wider than fruit, apex obtuse;</text>
      <biological_entity id="o21208" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o21210" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o21209" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o21211" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o21208" id="r1469" name="as wide as" negation="false" src="d0_s13" to="o21210" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o21212" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character constraint="per ovary" constraintid="o21213" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o21213" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style (4–) 5–7 mm.</text>
      <biological_entity id="o21214" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds plump, (suborbicular).</text>
      <biological_entity id="o21215" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush, pinyon-juniper, ponderosa pine, Douglas-fir, limber pine communities on clay, or a mixture of shale fragments and clay</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="douglas-fir" constraint="on clay , or a mixture of shale fragments and clay" />
        <character name="habitat" value="limber pine communities" constraint="on clay , or a mixture of shale fragments and clay" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="a mixture" constraint="of shale fragments and clay" />
        <character name="habitat" value="shale fragments" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Graham’s twinpod</other_name>
  <discussion>Physaria grahamii is difficult to evaluate due to the paucity of collections. The tentative recognition by N. H. Holmgren (2005b) is followed here.</discussion>
  
</bio:treatment>