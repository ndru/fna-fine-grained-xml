<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">salix</taxon_name>
    <taxon_name authority="Dumortier" date="1826" rank="section">triandrae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">triandra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1016. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus salix;section triandrae;species triandra</taxon_hierarchy>
    <other_info_on_name type="fna_id">200006057</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">amygdalina</taxon_name>
    <taxon_hierarchy>genus Salix;species amygdalina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">amygdalina</taxon_name>
    <taxon_name authority="Wimmer &amp; Grabowski" date="unknown" rank="variety">discolor</taxon_name>
    <taxon_hierarchy>genus Salix;species amygdalina;variety discolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">triandra</taxon_name>
    <taxon_name authority="(Wimmer &amp; Grabowski) Arcangeli" date="unknown" rank="subspecies">discolor</taxon_name>
    <taxon_hierarchy>genus Salix;species triandra;subspecies discolor;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branches glabrous or glabrescent;</text>
      <biological_entity id="o24668" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o24669" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets yellowbrown, redbrown, or brownish, usually glabrous, rarely pilose.</text>
      <biological_entity id="o24670" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24671" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules rudimentary to foliaceous on early ones (absent on proximal ones);</text>
      <biological_entity id="o24672" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24673" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="rudimentary" value_original="rudimentary" />
        <character constraint="on early ones" is_modifier="false" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole deeply grooved adaxially, margins covering groove, 4–26 mm, pubescent or puberulent to glabrescent adaxially;</text>
      <biological_entity id="o24674" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24675" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o24676" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="26" to_unit="mm" />
        <character char_type="range_value" from="puberulent" modifier="adaxially" name="pubescence" src="d0_s3" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o24677" name="groove" name_original="groove" src="d0_s3" type="structure" />
      <relation from="o24676" id="r1679" name="covering" negation="false" src="d0_s3" to="o24677" />
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade oblong, narrowly oblong, narrowly elliptic, elliptic, or lanceolate to obovate, 53–114 × 14–35 mm, 2.7–6.3 times as long as wide, base convex or cuneate, margins flat or slightly revolute, crenate or serrulate, apex acuminate, acute, or ± caudate, abaxial surface glabrous or glabrescent, adaxial dull or slightly glossy, glabrous or glabrescent;</text>
      <biological_entity id="o24678" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o24679" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="53" from_unit="mm" name="length" src="d0_s4" to="114" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="2.7-6.3" value_original="2.7-6.3" />
      </biological_entity>
      <biological_entity id="o24680" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24681" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o24682" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="caudate" value_original="caudate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24683" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24684" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal blade margins crenate or crenulate;</text>
      <biological_entity id="o24685" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="blade" id="o24686" name="margin" name_original="margins" src="d0_s5" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>juvenile blade reddish.</text>
      <biological_entity id="o24687" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o24688" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Catkins: staminate 20–60 × 5.5–10 mm, flowering branchlet 3–17 mm;</text>
      <biological_entity id="o24689" name="catkin" name_original="catkins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s7" to="60" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24690" name="branchlet" name_original="branchlet" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate moderately to very densely flowered, slender to stout, 20–60 × 5–8 mm, flowering branchlet 5–31 mm;</text>
      <biological_entity id="o24691" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="to branchlet" constraintid="o24692" is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24692" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="very densely" name="architecture" src="d0_s8" value="flowered" value_original="flowered" />
        <character char_type="range_value" from="slender" is_modifier="true" name="size" src="d0_s8" to="stout" />
        <character char_type="range_value" from="20" from_unit="mm" is_modifier="true" name="length" src="d0_s8" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="width" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="31" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>floral bract 1–2.3 mm, apex rounded or acute, abaxially hairy (mainly proximally), hairs wavy.</text>
      <biological_entity id="o24693" name="catkin" name_original="catkins" src="d0_s9" type="structure" />
      <biological_entity constraint="floral" id="o24694" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24695" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24696" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: abaxial nectary 0.2–1.1 mm, adaxial nectary oblong, square, or ovate, 0.2–0.6 mm, distinct;</text>
      <biological_entity id="o24697" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24698" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24699" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments distinct, hairy on proximal 1/2;</text>
      <biological_entity id="o24700" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24701" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character constraint="on proximal 1/2" constraintid="o24702" is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24702" name="1/2" name_original="1/2" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>anthers ellipsoid.</text>
      <biological_entity id="o24703" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24704" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pistillate flowers: adaxial nectary obovate to square, 0.3–0.5 mm;</text>
      <biological_entity id="o24705" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24706" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s13" to="square" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary pyriform, beak gradually tapering to or slightly bulged below styles;</text>
      <biological_entity id="o24707" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24708" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o24709" name="beak" name_original="beak" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="gradually; slightly" name="shape" src="d0_s14" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o24710" name="style" name_original="styles" src="d0_s14" type="structure" />
      <relation from="o24709" id="r1680" name="bulged" negation="false" src="d0_s14" to="o24710" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 30–36 per ovary;</text>
      <biological_entity id="o24711" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24712" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o24713" from="30" name="quantity" src="d0_s15" to="36" />
      </biological_entity>
      <biological_entity id="o24713" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>styles distinct 1/2 their lengths, 0.2–0.3 mm;</text>
      <biological_entity id="o24714" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24715" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character name="lengths" src="d0_s16" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas flat, abaxially non-papillate with rounded tip, 0.1–0.2 mm.</text>
      <biological_entity id="o24716" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24717" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s17" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o24718" is_modifier="false" modifier="abaxially" name="relief" src="d0_s17" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24718" name="tip" name_original="tip" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules 3–6 mm. 2n = 38 (44), 57, or 88.</text>
      <biological_entity id="o24719" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24720" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="38" value_original="38" />
        <character name="atypical_quantity" src="d0_s18" value="44" value_original="44" />
        <character name="quantity" src="d0_s18" value="57" value_original="57" />
        <character name="quantity" src="d0_s18" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-40 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="40" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.S., Ont.; D.C., Maine, Ohio, Va.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Almond leaf willow</other_name>
  <discussion>Salix triandra usually has been overlooked in North American floras. At one time, it was a very important basket willow and probably was introduced into North America for that purpose. Some authors treat the glaucous and nonglaucous forms as subspecies (F. Martini and P. Paiero 1988; K. H. Rechinger 1993); A. K. Skvortsov (1999) noted that, although the two have somewhat distinct ranges, both kinds occur throughout the species and sometimes can be found in the same population. His suggestion that genetic inheritance of this character should be studied has not been taken up. The species is characterized by bark that is dark gray, smooth, and flaking in large irregular patches, as in Platanus ×acerifolia. The ovary-style transition is so indistinct that styles are often described as absent, but there are two, distinct styles, each terminating in a short stigma. A color change, later in the season, between the styles and ovary suggests that the tip of the ovary and the two distinct styles are both stylar tissues. In general, it appears that the styles are connate proximally and distinct distally.</discussion>
  <discussion>Collections of Salix triandra made in 1934–35 by H. Hyland along the Penobscot River, Orono, Maine, were labeled by him as “introduced,” but they could have spread from cultivation or have been naturalized (A. Haines, pers. comm.). Recent naturalized occurrences are known from Toronto, Ontario, and Wolfville, Nova Scotia. Specimens identified as S. triandra by C. R. Ball are from Virginia and the District of Columbia. Salix triandra is reported to occur in Ohio (T. D. Sydnor and W. F. Cowen 2000) but voucher specimens were not found.</discussion>
  
</bio:treatment>