<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Dumortier" date="1826" rank="section">Chamaetia</taxon_name>
    <place_of_publication>
      <publication_title>Bijdr. Natuurk. Wetensch.</publication_title>
      <place_in_publication>1: 56. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section Chamaetia</taxon_hierarchy>
    <other_info_on_name type="fna_id">317502</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.01–1.5 m, sometimes clonal by layering or rhizomes.</text>
      <biological_entity id="o41324" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.01" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character constraint="by rhizomes" constraintid="o41325" is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o41325" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="layering" value_original="layering" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Largest medial blades hypostomatous, amphistomatous, or hemiamphistomatous, abaxial surface glaucous.</text>
      <biological_entity constraint="largest medial" id="o41326" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="hypostomatous" value_original="hypostomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="amphistomatous" value_original="amphistomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="hemiamphistomatous" value_original="hemiamphistomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="amphistomatous" value_original="amphistomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="hemiamphistomatous" value_original="hemiamphistomatous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o41327" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Catkins from subterminal buds.</text>
      <biological_entity id="o41328" name="catkin" name_original="catkins" src="d0_s2" type="structure" />
      <biological_entity constraint="subterminal" id="o41329" name="bud" name_original="buds" src="d0_s2" type="structure" />
      <relation from="o41328" id="r2792" name="from" negation="false" src="d0_s2" to="o41329" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers: filaments glabrous or hairy.</text>
      <biological_entity id="o41330" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o41331" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate flowers: abaxial nectary sometimes present, then distinct, or connate to adaxial and forming a cup;</text>
      <biological_entity id="o41332" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o41333" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character constraint="to adaxial nectary" constraintid="o41334" is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o41334" name="nectary" name_original="nectary" src="d0_s4" type="structure" />
      <biological_entity id="o41335" name="cup" name_original="cup" src="d0_s4" type="structure" />
      <relation from="o41333" id="r2793" name="forming a" negation="false" src="d0_s4" to="o41335" />
    </statement>
    <statement id="d0_s5">
      <text>ovary not glaucous, sparsely to very densely short-silky, hairs white or white and ferruginous, cylindrical or flattened.</text>
      <biological_entity id="o41336" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o41337" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o41338" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="very densely" name="pubescence" src="d0_s5" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white and ferruginous" value_original="white and ferruginous" />
      </biological_entity>
      <relation from="o41337" id="r2794" modifier="sparsely" name="to" negation="false" src="d0_s5" to="o41338" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2d1.</number>
  <discussion>Species 4 (3 in the flora).</discussion>
  <discussion>The branchlets in all members of sect. Chamaetia rarely elongate to produce late (neoformed) leaves. Flowering branchlets usually are not differentiated from vegetative branchlets and often have the same number of leaves.</discussion>
  
</bio:treatment>