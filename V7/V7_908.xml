<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">563</other_info_on_meta>
    <other_info_on_meta type="treatment_page">564</other_info_on_meta>
    <other_info_on_meta type="illustration_page">561</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="tribe">iberideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iberis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">sempervirens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 648. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe iberideae;genus iberis;species sempervirens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095044</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs;</text>
    </statement>
    <statement id="d0_s1">
      <text>(evergreen, with sterile shoots);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous.</text>
      <biological_entity id="o39222" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o39223" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent, branched basally and distally, (0.5–) 1–2.5 dm.</text>
      <biological_entity id="o39224" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.5" from_unit="dm" modifier="distally" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" modifier="distally" name="some_measurement" src="d0_s3" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (of sterile shoots) sessile;</text>
      <biological_entity constraint="basal" id="o39225" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-oblanceolate, 1–3 (–5) cm × 2–5 mm.</text>
      <biological_entity id="o39226" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (of flowering shoots) similar to basal, blade smaller, margins entire.</text>
      <biological_entity constraint="cauline" id="o39227" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o39228" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o39229" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o39230" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o39227" id="r2649" name="to" negation="false" src="d0_s6" to="o39228" />
    </statement>
    <statement id="d0_s7">
      <text>Racemes slightly elongated in fruit.</text>
      <biological_entity id="o39232" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels divaricate-ascending, 4–11 mm.</text>
      <biological_entity id="o39231" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o39232" is_modifier="false" modifier="slightly" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39233" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o39231" id="r2650" name="fruiting" negation="false" src="d0_s8" to="o39233" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong or ovate, 2–4 mm;</text>
      <biological_entity id="o39234" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o39235" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white or sometimes pink, abaxial pair 5–13 × 2–7 mm, adaxial pair 3–6 × 1–3 mm.</text>
      <biological_entity id="o39236" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o39237" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39238" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39239" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits suborbicular to broadly ovate, 6–8 × 5–6 mm, apically notched;</text>
      <biological_entity id="o39240" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s11" to="broadly ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s11" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves extending into subacute wing;</text>
      <biological_entity id="o39241" name="valve" name_original="valves" src="d0_s12" type="structure" />
      <biological_entity id="o39242" name="wing" name_original="wing" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="subacute" value_original="subacute" />
      </biological_entity>
      <relation from="o39241" id="r2651" name="extending into" negation="false" src="d0_s12" to="o39242" />
    </statement>
    <statement id="d0_s13">
      <text>style 1–3 mm, exserted beyond apical notch.</text>
      <biological_entity id="o39243" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o39244" name="notch" name_original="notch" src="d0_s13" type="structure" />
      <relation from="o39243" id="r2652" name="exserted beyond" negation="false" src="d0_s13" to="o39244" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds narrowly winged, 2–3 mm. 2n = 22.</text>
      <biological_entity id="o39245" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s14" value="winged" value_original="winged" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39246" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Escape from cultivation, abandoned gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cultivation" modifier="escape from" />
        <character name="habitat" value="abandoned gardens" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Mich., N.Y.; s Europe; introduced also in South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Evergreen candytuft</other_name>
  
</bio:treatment>