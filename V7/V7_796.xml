<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">506</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="mention_page">509</other_info_on_meta>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">selenia</taxon_name>
    <taxon_name authority="Cory" date="1931" rank="species">jonesii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>33: 142. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus selenia;species jonesii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095060</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Selenia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">jonesii</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">obovata</taxon_name>
    <taxon_hierarchy>genus Selenia;species jonesii;variety obovata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annuals, (not or rarely subacaulescent).</text>
      <biological_entity constraint="plants" id="o35017" name="winter-annual" name_original="winter-annuals" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (not inflated into crown), ascending or subdecumbent, (0.5–) 1–3 (–4) dm.</text>
      <biological_entity id="o35018" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character name="orientation" src="d0_s1" value="semidecumbent" value_original="subdecumbent" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o35019" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–2.5 cm;</text>
      <biological_entity id="o35020" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins 2-pinnatisect, 4–8 cm;</text>
      <biological_entity constraint="blade" id="o35021" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-pinnatisect" value_original="2-pinnatisect" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes 4–11 on each side, (smaller than terminal);</text>
      <biological_entity id="o35022" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o35023" from="4" name="quantity" src="d0_s5" to="11" />
      </biological_entity>
      <biological_entity id="o35023" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>apical segment linear to oblong or ovate, 1–6 × 0.5–2.5 mm, margins entire.</text>
      <biological_entity constraint="apical" id="o35024" name="segment" name_original="segment" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="oblong or ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35025" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (and bracts) similar to basal, smaller distally.</text>
      <biological_entity constraint="cauline" id="o35026" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o35027" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o35026" id="r2348" name="to" negation="false" src="d0_s7" to="o35027" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels: some from basal leaf-axils, (10–) 20–40 (–50) mm.</text>
      <biological_entity id="o35029" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o35030" name="leaf-axil" name_original="leaf-axils" src="d0_s8" type="structure" />
      <relation from="o35029" id="r2349" name="from" negation="false" src="d0_s8" to="o35030" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals spreading, oblong, 4–6 (–7) × 1.5–2.5 mm, apex appendage developed, 0.5–0.8 mm;</text>
      <biological_entity id="o35031" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35032" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apex" id="o35033" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="development" src="d0_s9" value="developed" value_original="developed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spatulate, 4–7 × 2.5–4 mm, apex rounded or emarginate;</text>
      <biological_entity id="o35034" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o35035" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35036" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filament pairs 3–4 mm, not dilated basally;</text>
      <biological_entity id="o35037" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o35038" name="filament" name_original="filament" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s11" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers ovate, 1–1.5 mm;</text>
      <biological_entity id="o35039" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o35040" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore 1–3 mm.</text>
      <biological_entity id="o35041" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o35042" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits globose or, rarely, obovoid, terete, 0.8–1.5 cm × 8–14 mm, (not fleshy, papery), base usually obtuse, rarely cuneate, apex obtuse;</text>
      <biological_entity id="o35043" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character name="shape" src="d0_s14" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s14" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s14" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35044" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s14" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o35045" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves obscurely reticulate-veined;</text>
      <biological_entity id="o35046" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s15" value="reticulate-veined" value_original="reticulate-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>replum not flattened;</text>
      <biological_entity id="o35047" name="replum" name_original="replum" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>septum complete;</text>
      <biological_entity id="o35048" name="septum" name_original="septum" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–14 per ovary;</text>
      <biological_entity id="o35049" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o35050" from="8" name="quantity" src="d0_s18" to="14" />
      </biological_entity>
      <biological_entity id="o35050" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 1–2 (–3) mm, not flattened basally.</text>
      <biological_entity id="o35051" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 4–5 mm diam.;</text>
      <biological_entity id="o35052" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s20" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>wing 1–1.5 mm. 2n = 24.</text>
      <biological_entity id="o35053" name="wing" name_original="wing" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35054" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry lake beds, draws, moist swales, prairie plateaus, playa lakes, buffalo wallows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry lake beds" />
        <character name="habitat" value="moist swales" modifier="draws" />
        <character name="habitat" value="prairie plateaus" />
        <character name="habitat" value="playa lakes" />
        <character name="habitat" value="buffalo wallows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Selenia jonesii is endemic to six counties in the western Edwards Plateau. Variety obovata is reduced herein to synonymy because the alleged difference in fruit shape does not hold; it appears to be an artifact of pressing inflated fruit.</discussion>
  
</bio:treatment>