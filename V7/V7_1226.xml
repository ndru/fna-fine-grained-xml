<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">701</other_info_on_meta>
    <other_info_on_meta type="treatment_page">709</other_info_on_meta>
    <other_info_on_meta type="illustration_page">710</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Cory" date="1943" rank="species">cutleri</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>45: 259. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species cutleri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094997</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous throughout.</text>
      <biological_entity id="o38446" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (simple from base), usually branched distally, 2–7 dm.</text>
      <biological_entity id="o38447" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves subrosulate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o38448" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="subrosulate" value_original="subrosulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate, 2.5–20 cm, margins runcinate-pinnatifid.</text>
      <biological_entity id="o38449" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o38450" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="runcinate-pinnatifid" value_original="runcinate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (petiolate);</text>
      <biological_entity constraint="cauline" id="o38451" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate, 4–20 cm × 10–70 mm, (smaller distally), base not auriculate, margins runcinate-pinnatifid (undivided, usually entire distally).</text>
      <biological_entity id="o38452" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38453" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o38454" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="runcinate-pinnatifid" value_original="runcinate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (lax).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (5–) 10–27 mm.</text>
      <biological_entity id="o38455" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38456" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o38455" id="r2591" name="fruiting" negation="false" src="d0_s9" to="o38456" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers (strongly zygomorphic);</text>
      <biological_entity id="o38457" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>calyx campanulate;</text>
      <biological_entity id="o38458" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals (ascending), dark purple, 8–12 mm, not keeled;</text>
      <biological_entity id="o38459" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals lavender to purple-lavender, (in 2 unequal pairs), abaxial pair undifferentiated into blade and claw, 5–6 mm, margins crisped, adaxial pair 15–26 mm, (not crisped), claw 7–11 mm, crisped, narrower than blade;</text>
      <biological_entity id="o38460" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="lavender" name="coloration_or_odor" src="d0_s13" to="purple-lavender" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o38461" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character constraint="into claw" constraintid="o38463" is_modifier="false" name="prominence" src="d0_s13" value="undifferentiated" value_original="undifferentiated" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38462" name="blade" name_original="blade" src="d0_s13" type="structure" />
      <biological_entity id="o38463" name="claw" name_original="claw" src="d0_s13" type="structure" />
      <biological_entity id="o38464" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o38465" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s13" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38466" name="claw" name_original="claw" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="crisped" value_original="crisped" />
        <character constraint="than blade" constraintid="o38467" is_modifier="false" name="width" src="d0_s13" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o38467" name="blade" name_original="blade" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o38468" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <biological_entity id="o38469" name="pair" name_original="pairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s14" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o38468" id="r2592" name="in" negation="false" src="d0_s14" to="o38469" />
    </statement>
    <statement id="d0_s15">
      <text>filaments distinct: abaxial pair 6–7 mm, lateral pair 3–5 mm, adaxial pair 8–12 mm;</text>
      <biological_entity id="o38470" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o38471" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o38472" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers: abaxial and lateral pairs fertile, 4–5 mm, adaxial pair fertile, 3–3.5 mm;</text>
      <biological_entity id="o38473" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="reproduction" src="d0_s16" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o38474" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>gynophore 0.2–0.5 mm.</text>
      <biological_entity id="o38475" name="anther" name_original="anthers" src="d0_s17" type="structure" />
      <biological_entity id="o38476" name="gynophore" name_original="gynophore" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits ascending, smooth, straight, strongly flattened, 3.2–7.5 cm × 4–5 mm;</text>
      <biological_entity id="o38477" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="length" src="d0_s18" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o38478" name="valve" name_original="valves" src="d0_s19" type="structure" />
      <biological_entity id="o38479" name="midvein" name_original="midvein" src="d0_s19" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s19" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o38478" id="r2593" name="with" negation="false" src="d0_s19" to="o38479" />
    </statement>
    <statement id="d0_s20">
      <text>replum straight;</text>
      <biological_entity id="o38480" name="replum" name_original="replum" src="d0_s20" type="structure">
        <character is_modifier="false" name="course" src="d0_s20" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 30–46 per ovary;</text>
      <biological_entity id="o38481" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o38482" from="30" name="quantity" src="d0_s21" to="46" />
      </biological_entity>
      <biological_entity id="o38482" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style 0.2–0.5 mm;</text>
      <biological_entity id="o38483" name="style" name_original="style" src="d0_s22" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s22" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma 2-lobed.</text>
      <biological_entity id="o38484" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds orbicular, 3–4 mm diam.;</text>
      <biological_entity id="o38485" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s24" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>wing 0.5–1 mm wide, continuous.</text>
      <biological_entity id="o38486" name="wing" name_original="wing" src="d0_s25" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s25" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s25" value="continuous" value_original="continuous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus slopes, rocky hillsides, gravelly and dry stream beds, sand flats, limestone slopes, open scrub or woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="dry stream beds" />
        <character name="habitat" value="sand flats" />
        <character name="habitat" value="limestone slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>In Texas, Streptanthus cutleri is restricted to the Big Bend area in Brewster County.</discussion>
  
</bio:treatment>