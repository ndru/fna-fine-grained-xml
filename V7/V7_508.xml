<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Greene) Windham &amp; Al-Shehbaz" date="2007" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>12: 237. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species covillei</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094782</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">covillei</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>5: 243. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species covillei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">leibergii</taxon_name>
    <taxon_hierarchy>genus Arabis;species leibergii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>apomictic;</text>
      <biological_entity id="o8386" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex woody.</text>
      <biological_entity id="o8387" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 0.5–2.5 dm, glabrous throughout.</text>
      <biological_entity id="o8388" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="per caudex branch" constraintid="o8389" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from center" constraintid="o8390" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="2.5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o8389" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o8390" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o8391" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o8392" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o8393" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <relation from="o8390" id="r603" name="part_of" negation="false" src="d0_s4" to="o8391" />
      <relation from="o8390" id="r604" name="near" negation="false" src="d0_s4" to="o8392" />
      <relation from="o8390" id="r605" name="near" negation="false" src="d0_s4" to="o8393" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade oblanceolate, 1.5–5 mm wide, margins entire, ciliate proximally, trichomes (simple), to 0.4 mm, and distally, trichomes short-stalked, 2 or 3 (or 4) -rayed, 0.15–0.4 mm, surfaces glabrous.</text>
      <biological_entity constraint="basal" id="o8394" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o8395" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8396" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o8397" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8398" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" value="3-rayed" value_original="3-rayed" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8399" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 2–7, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o8400" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity id="o8401" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o8400" id="r606" name="concealing" negation="true" src="d0_s6" to="o8401" />
    </statement>
    <statement id="d0_s7">
      <text>blade with auricles 0.2–1 mm, auricles rarely absent, surfaces of distalmost leaves glabrous.</text>
      <biological_entity constraint="cauline" id="o8402" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o8403" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o8404" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8405" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8406" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o8407" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o8403" id="r607" name="with" negation="false" src="d0_s7" to="o8404" />
      <relation from="o8406" id="r608" name="part_of" negation="false" src="d0_s7" to="o8407" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 2–9-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending to suberect, straight, 6–20 mm, glabrous.</text>
      <biological_entity id="o8408" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-9-flowered" value_original="2-9-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="suberect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8409" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o8408" id="r609" name="fruiting" negation="false" src="d0_s9" to="o8409" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o8410" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals glabrous;</text>
      <biological_entity id="o8411" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals lavender to purplish, 5–6 × 1–2 mm, glabrous;</text>
      <biological_entity id="o8412" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s12" to="purplish" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen spheroid.</text>
      <biological_entity id="o8413" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits ascending to suberect, rarely appressed to rachis, not secund, straight, edges slightly undulate (rarely parallel), 3.5–5 cm × 3–5 mm;</text>
      <biological_entity id="o8414" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s14" to="suberect" />
        <character constraint="to rachis" constraintid="o8415" is_modifier="false" modifier="rarely" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o8415" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o8416" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s14" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o8417" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 18–30 per ovary;</text>
      <biological_entity id="o8418" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o8419" from="18" name="quantity" src="d0_s16" to="30" />
      </biological_entity>
      <biological_entity id="o8419" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.2–1 mm.</text>
      <biological_entity id="o8420" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds uniseriate, 4–5 × 3–4 mm;</text>
      <biological_entity id="o8421" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s18" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 1–1.5 mm wide.</text>
      <biological_entity id="o8422" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s19" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes in alpine meadows and open coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="in alpine meadows and open coniferous forests" />
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="open coniferous forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <discussion>Morphological evidence suggests that Boechera covillei is an apomictic species that arose through hybridization between B. howellii and B. lyallii (see M. D. Windham and I. A. Al-Shehbaz 2007b for detailed comparison).</discussion>
  
</bio:treatment>