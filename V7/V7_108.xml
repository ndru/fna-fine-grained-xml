<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="Bebb in J. T. Rothrock" date="1879" rank="species">wolfii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">wolfii</taxon_name>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species wolfii;variety wolfii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445898</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–2 m.</text>
      <biological_entity id="o17672" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches redbrown or violet, pubescent to glabrescent;</text>
      <biological_entity id="o17673" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o17674" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="violet" value_original="violet" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellowbrown or yellowish, sparsely pubescent or densely long-silky, hairs straight, wavy, curved, or crinkled.</text>
      <biological_entity id="o17675" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o17676" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o17677" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crinkled" value_original="crinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole convex to flat, or shallowly grooved adaxially, 3–5.6 mm, pubescent or long-silky adaxially;</text>
      <biological_entity id="o17678" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17679" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s3" to="flat" />
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="long-silky" value_original="long-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade narrowly oblong, narrowly elliptic, elliptic, or oblanceolate, apex acute, acuminate, or convex, abaxial surface pubescent, short-silky, or villous, adaxial sparsely to densely short to long-silky, or villous;</text>
      <biological_entity id="o17680" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o17681" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o17682" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17683" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17684" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>juvenile blade short to long-silky or villous abaxially.</text>
      <biological_entity id="o17685" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o17686" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Catkins: pistillate moderately or very densely flowered, subglobose or globose, 8.5–19 × 7–12 mm, flowering branchlet 1–7 mm;</text>
      <biological_entity id="o17687" name="catkin" name_original="catkins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="very densely; very densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="globose" value_original="globose" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="length" src="d0_s6" to="19" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17688" name="branchlet" name_original="branchlet" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bract 0.8–1.6 mm.</text>
      <biological_entity id="o17689" name="catkin" name_original="catkins" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o17690" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s7" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: abaxial nectary (0–) 0.1–0.2 mm, adaxial nectary 0.4–0.5 mm.</text>
      <biological_entity id="o17691" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17692" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="0.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17693" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers: adaxial nectary oblong, 0.4–0.8 mm, shorter to longer than stipe;</text>
      <biological_entity id="o17694" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17695" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
        <character constraint="than stipe" constraintid="o17696" is_modifier="false" name="size_or_length" src="d0_s9" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o17696" name="stipe" name_original="stipe" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stipe 0.2–0.9 mm;</text>
      <biological_entity id="o17697" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17698" name="stipe" name_original="stipe" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary glabrous;</text>
      <biological_entity id="o17699" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17700" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 10–12 per ovary;</text>
      <biological_entity id="o17701" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17702" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17703" from="10" name="quantity" src="d0_s12" to="12" />
      </biological_entity>
      <biological_entity id="o17703" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas flat, abaxially non-papillate with rounded or pointed tip.</text>
      <biological_entity id="o17704" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17706" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s13" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = 38.</text>
      <biological_entity id="o17705" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o17706" is_modifier="false" modifier="abaxially" name="relief" src="d0_s13" value="non-papillate" value_original="non-papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17707" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early-mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jun" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, springs, wet meadows, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54a.</number>
  <discussion>Hybrids:</discussion>
  <discussion>Variety wolfii forms natural hybrids with Salix boothii.</discussion>
  
</bio:treatment>