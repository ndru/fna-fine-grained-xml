<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martinov" date="unknown" rank="family">resedaceae</taxon_name>
    <taxon_name authority="Cambessèdes in V. Jacquemont" date="unknown" rank="genus">OLIGOMERIS</taxon_name>
    <place_of_publication>
      <publication_title>in V. Jacquemont, Voy. Inde</publication_title>
      <place_in_publication>4(Bot.): 23. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family resedaceae;genus OLIGOMERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oligos, few, and meros, part, alluding to fewer stamens and petals than in other genera of family</other_info_on_name>
    <other_info_on_name type="fna_id">122801</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual [biennial], or perennial.</text>
      <biological_entity id="o2277" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, branched basally.</text>
      <biological_entity id="o2278" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not rosulate;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate (sometimes fasciculate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate, petiole shorter than blade, winged;</text>
      <biological_entity id="o2279" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o2280" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="than blade" constraintid="o2281" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o2281" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade margins usually entire, sometimes toothed near base, teeth 1–2, hyaline.</text>
      <biological_entity constraint="blade" id="o2282" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="near base" constraintid="o2283" is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o2283" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o2284" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences usually dense spikes (or spikelke racemes).</text>
      <biological_entity id="o2285" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o2286" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="usually" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels absent or nearly so.</text>
      <biological_entity id="o2287" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals persistent, 2–4 (–5), distinct, equal;</text>
      <biological_entity id="o2288" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2289" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="4" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 2–3 [–5], distinct or basally connate, margins entire or shallowly incised;</text>
      <biological_entity id="o2290" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2291" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="5" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o2292" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s9" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>intrastaminal nectary-discs absent;</text>
      <biological_entity id="o2293" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="intrastaminal" id="o2294" name="nectary-disc" name_original="nectary-discs" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 3–4 [–10];</text>
      <biological_entity id="o2295" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2296" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments persistent, distinct or basally connate;</text>
      <biological_entity id="o2297" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2298" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovaries 4 (–5) -carpelled.</text>
      <biological_entity id="o2299" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2300" name="ovary" name_original="ovaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="4(-5)-carpelled" value_original="4(-5)-carpelled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules erect, angled, subglobose to obovoid, walls membranous.</text>
      <biological_entity id="o2301" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s14" value="angled" value_original="angled" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s14" to="obovoid" />
      </biological_entity>
      <biological_entity id="o2302" name="wall" name_original="walls" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds smooth (glossy).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 15.</text>
      <biological_entity id="o2303" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="x" id="o2304" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw North America, n Mexico, sw, c Asia, n, s Africa, n Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="c Asia" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="n Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Whitepuff</other_name>
  <discussion>Species 3 (1 in the flora).</discussion>
  <discussion>Molecular research has shown that Oligomeris is a monophyletic group nested within Reseda in tribe Resedeae and sister to Reseda sect. Glaucoreseda (S. Martín-Bravo et al. 2007). Nonetheless, it seems preferable to maintain the genus; it has clear-cut diagnostic morphological characteristics. Hyaline teeth at the base of the leaves may be vestiges of a laminar segmentation of the leaves (F. Weberling 1968); some have interpreted the teeth as stipular.</discussion>
  
</bio:treatment>