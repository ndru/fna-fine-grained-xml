<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="treatment_page">625</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 321. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species angustifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095116</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="Nuttall ex Torrey &amp; A. Gray" date="unknown" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer</publication_title>
      <place_in_publication>1: 101. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vesicaria;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) S. Watson" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Cory" date="unknown" rank="species">longifolia</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species longifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>with a fine taproot;</text>
      <biological_entity id="o35240" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o35239" id="r2362" name="with" negation="false" src="d0_s1" to="o35240" />
    </statement>
    <statement id="d0_s2">
      <text>± densely pubescent, trichomes several-rayed, rays distinct or fused at base, bifurcate, (prominently tuberculate throughout).</text>
      <biological_entity id="o35239" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o35241" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="several-rayed" value_original="several-rayed" />
      </biological_entity>
      <biological_entity id="o35242" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o35243" is_modifier="false" name="fusion" src="d0_s2" value="fused" value_original="fused" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
      <biological_entity id="o35243" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few to several from base, erect, (sometimes branched), to 4 dm.</text>
      <biological_entity id="o35244" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s3" value="few to several" value_original="few to several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o35245" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o35244" id="r2363" name="from" negation="false" src="d0_s3" to="o35245" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade elliptic to rhombic, 3–8 cm, (base narrowing gradually to petiole), margins entire, repand, coarsely toothed, or pinnatifid.</text>
      <biological_entity constraint="basal" id="o35246" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o35247" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="rhombic" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o35248" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves: (proximal often shortly petiolate, distal sessile);</text>
      <biological_entity constraint="cauline" id="o35249" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade linear or narrowly obovate, 1.5–6 (–10) cm, margins entire, repand, or shallowly toothed.</text>
      <biological_entity constraint="cauline" id="o35250" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o35251" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o35252" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes usually loose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (usually divaricate, sometimes horizontal, straight or slightly curved), 8–20 mm.</text>
      <biological_entity id="o35253" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_fragility" src="d0_s7" value="loose" value_original="loose" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35254" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o35253" id="r2364" name="fruiting" negation="false" src="d0_s8" to="o35254" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals elliptic or ovate, 4–6 mm, (lateral pair usually subsaccate);</text>
      <biological_entity id="o35255" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o35256" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals obovate to obdeltate, 6–10 mm, (apex often emarginate).</text>
      <biological_entity id="o35257" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o35258" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="obdeltate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits not didymous, ± globose, slightly inflated, 4–6 mm;</text>
      <biological_entity id="o35259" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement" src="d0_s11" value="didymous" value_original="didymous" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves (not retaining seeds after dehiscence), glabrous throughout;</text>
      <biological_entity id="o35260" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o35261" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character constraint="than fruit" constraintid="o35262" is_modifier="false" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o35262" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o35263" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character constraint="per ovary" constraintid="o35264" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o35264" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 2–3.5 mm;</text>
    </statement>
    <statement id="d0_s16">
      <text>(stigma expanded).</text>
      <biological_entity id="o35265" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds flattened, (margined).</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 10.</text>
      <biological_entity id="o35266" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35267" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr(-May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow limestone-derived soils, sometimes spreading to disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow limestone-derived soils" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>90-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="90" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Threadleaf bladderpod</other_name>
  
</bio:treatment>