<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">739</other_info_on_meta>
    <other_info_on_meta type="treatment_page">740</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Hooker" date="1830" rank="genus">thysanocarpus</taxon_name>
    <taxon_name authority="Greene" date="1886" rank="species">conchuliferus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>13: 218. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thysanocarpus;species conchuliferus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094952</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thysanocarpus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">conchuliferus</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">planiusculus</taxon_name>
    <taxon_hierarchy>genus Thysanocarpus;species conchuliferus;variety planiusculus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thysanocarpus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laciniatus</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">conchuliferus</taxon_name>
    <taxon_hierarchy>genus Thysanocarpus;species laciniatus;variety conchuliferus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.5–1.5 dm.</text>
      <biological_entity id="o15936" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blade oblanceolate to elliptic, 1–2.5 (–3.5) cm, margins often pinnatifid, sometimes sinuate-dentate, surfaces glabrous.</text>
      <biological_entity constraint="basal" id="o15937" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o15938" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s1" to="elliptic" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15939" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="sinuate-dentate" value_original="sinuate-dentate" />
      </biological_entity>
      <biological_entity id="o15940" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: blade lanceolate to narrowly elliptic, or nearly linear, widest near base or middle, base auriculate-clasping, auricles extending around stem (at least some leaves).</text>
      <biological_entity constraint="cauline" id="o15941" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15942" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character constraint="to " constraintid="o15944" is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o15943" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="narrowly" name="arrangement" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="nearly" name="arrangement" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="true" name="width" src="d0_s2" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity constraint="middle" id="o15944" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o15945" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s2" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity id="o15946" name="auricle" name_original="auricles" src="d0_s2" type="structure" />
      <biological_entity id="o15947" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o15946" id="r1107" name="extending" negation="false" src="d0_s2" to="o15947" />
    </statement>
    <statement id="d0_s3">
      <text>Racemes: internodes 0.7–1.5 (–2) mm in fruit.</text>
      <biological_entity id="o15948" name="raceme" name_original="racemes" src="d0_s3" type="structure" />
      <biological_entity id="o15950" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels stiffly divaricate-ascending to slightly recurved, (proximal) 3.5–6.5 mm.</text>
      <biological_entity id="o15949" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o15950" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="stiffly divaricate-ascending" name="orientation" src="d0_s4" to="slightly recurved" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s4" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15951" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o15949" id="r1108" name="fruiting" negation="false" src="d0_s4" to="o15951" />
    </statement>
    <statement id="d0_s5">
      <text>Fruits cymbiform, (wings strongly incurved toward flat side of fruit);</text>
      <biological_entity id="o15952" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cymbiform" value_original="cymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>valves glabrous;</text>
      <biological_entity id="o15953" name="valve" name_original="valves" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>wing with spatulate lobes (these sometimes joined distally, 0.25–0.4 mm wide at narrowest), rays absent or indistinct.</text>
      <biological_entity id="o15954" name="wing" name_original="wing" src="d0_s7" type="structure" />
      <biological_entity id="o15955" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o15956" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <relation from="o15954" id="r1109" name="with" negation="false" src="d0_s7" to="o15955" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ridges, slopes, cliffs, canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Thysanocarpus conchuliferus is known from Santa Cruz Island, where T. laciniatus and T. curvipes also occur. Rarely, specimens of those two species will have slightly incurved wings, or fruits folded in pressing. In such cases, the very dense, short inflorescences of T. conchuliferus provide a useful distinguishing feature.</discussion>
  <discussion>Thysanocarpus conchuliferus is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>