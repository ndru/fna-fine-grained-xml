<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">38</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="mention_page">42</other_info_on_meta>
    <other_info_on_meta type="treatment_page">40</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">salix</taxon_name>
    <taxon_name authority="Koidzumi" date="1913" rank="section">subalbae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">babylonica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1017. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus salix;section subalbae;species babylonica</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200005760</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branches yellowbrown to redbrown;</text>
      <biological_entity id="o30060" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o30061" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s0" to="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets sparsely to moderately densely tomentose, especially at nodes.</text>
      <biological_entity id="o30062" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o30063" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o30064" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o30063" id="r2016" modifier="especially" name="at" negation="false" src="d0_s1" to="o30064" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules absent or rudimentary on early ones;</text>
      <biological_entity id="o30065" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o30066" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character constraint="on early ones" is_modifier="false" name="prominence" src="d0_s2" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole convex to flat or shallowly to deeply grooved adaxially, 7–9 mm, tomentose abaxially;</text>
      <biological_entity id="o30067" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o30068" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s3" to="flat" />
        <character is_modifier="false" modifier="shallowly to deeply; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade lanceolate, narrowly oblong, or narrowly elliptic, 90–160 × 5–20 mm, 5.5–10.5 times as long as wide, base cuneate, margins flat, spinulose-serrulate or serrulate, apex acuminate, caudate, or acute, surfaces glabrous or sparsely short-silky, hairs straight, dull adaxially;</text>
      <biological_entity id="o30069" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o30070" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="90" from_unit="mm" name="length" src="d0_s4" to="160" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="5.5-10.5" value_original="5.5-10.5" />
      </biological_entity>
      <biological_entity id="o30071" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30072" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose-serrulate" value_original="spinulose-serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o30073" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o30074" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="short-silky" value_original="short-silky" />
      </biological_entity>
      <biological_entity id="o30075" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o30076" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="blade" id="o30077" name="margin" name_original="margins" src="d0_s5" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>juvenile blade reddish or yellowish green.</text>
      <biological_entity id="o30078" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o30079" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Catkins (flowering just before leaves emerge);</text>
    </statement>
    <statement id="d0_s8">
      <text>staminate 13–35 mm, flowering branchlet 1–6 mm;</text>
      <biological_entity id="o30080" name="catkin" name_original="catkins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate densely flowered, stout or subglobose, 9–27 × 2.5–7 mm, flowering branchlet (0–) 2–4 mm;</text>
      <biological_entity id="o30081" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30082" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s9" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>floral bract 1.1–1.8 mm, apex acute, rounded, or truncate, entire, abaxially sparsely hairy throughout or proximally, hairs wavy.</text>
      <biological_entity constraint="floral" id="o30083" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30084" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially sparsely; throughout; throughout; proximally" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o30085" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: abaxial nectary 0.2–0.6 mm, adaxial nectary oblong or ovate, 0.4–0.7 mm, nectaries distinct or connate and cupshaped;</text>
      <biological_entity id="o30086" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30087" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30088" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30089" name="nectary" name_original="nectaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct, hairy on proximal 1/2 or basally;</text>
      <biological_entity id="o30090" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o30091" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character constraint="on proximal 1/2" constraintid="o30092" is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o30092" name="1/2" name_original="1/2" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (sometimes reddish turning yellow), ellipsoid or globose.</text>
      <biological_entity id="o30093" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o30094" name="anther" name_original="anthers" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: adaxial nectary oblong, square, ovate, or obovate, 0.4–0.8 mm;</text>
      <biological_entity id="o30095" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30096" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s14" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary ovoid or obturbinate, beak (sometimes pilose proximally), slightly bulged below or abruptly tapering to styles;</text>
      <biological_entity id="o30097" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30098" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obturbinate" value_original="obturbinate" />
      </biological_entity>
      <biological_entity id="o30099" name="beak" name_original="beak" src="d0_s15" type="structure" />
      <biological_entity id="o30100" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="abruptly" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o30099" id="r2017" modifier="slightly" name="bulged below" negation="false" src="d0_s15" to="o30100" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 2–4 per ovary;</text>
      <biological_entity id="o30101" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30102" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o30103" from="2" name="quantity" src="d0_s16" to="4" />
      </biological_entity>
      <biological_entity id="o30103" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles distinct or connate 1/2 their lengths, 0.2–0.3 mm;</text>
      <biological_entity id="o30104" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30105" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="connate" value_original="connate" />
        <character name="lengths" src="d0_s17" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or 2 plump lobes (almost capitate), 0.2–0.3 mm.</text>
      <biological_entity id="o30106" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30107" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s18" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o30108" is_modifier="false" modifier="abaxially" name="relief" src="d0_s18" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s18" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30108" name="tip" name_original="tip" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o30109" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character is_modifier="true" name="size" src="d0_s18" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Capsules 2–2.7 mm. 2n = 76.</text>
      <biological_entity id="o30110" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s19" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30111" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Around settlements</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="settlements" modifier="around" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Calif., Del., D.C., Fla., Ga., Ky., La., Md., N.C., S.C., Tenn., Va.; Asia; introduced also in Mexico (Mexico City), South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also in Mexico (Mexico City)" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Weeping willow</other_name>
  <discussion>Little is known about the origin of the strongly weeping cultivar of Salix babylonica. It was described by Linnaeus (1737[1738]) based on young garden specimens (W. J. Bean 1970–1988, vol. 4). It is thought to have originated in China, although it no longer occurs in the wild and its origin is uncertain. Selections are thought to have been transported to Europe along the trade route from China. In Tajikistan, there are three cultivated clones, one of which is staminate (A. K. Skvortsov 1999). Taxonomic treatments of S. babylonica are variable. Some botanists recognize a single species, including both pendulous and non-pendulous forms (Skvortsov), while others recognize four species: S. babylonica, with a weeping habit, S. capitata Y. L. Chou &amp; Skvortsov, S. pseudolasiogyne H. Léveillé, and the commonly cultivated S. matsudana Koidzumi (Fang Z. F. et al. 1999), with an erect or spreading habit. Here, S. babylonica is treated in a narrow sense, including only weeping forms.</discussion>
  <discussion>Salix babylonica is not cold tolerant and is not commonly grown in Europe (R. D. Meikle 1984) or in northern North America. In the flora area, cultivated trees with strongly pendulous branches and branchlets have been identified as S. babylonica (G. W. Argus 1985, 1986, 1993), but many are hybrids with S. alba (S. ×sepulcralis) or S. euxina (S. ×pendulina). Salix ×sepulcralis, especially nothovar. chrysocoma, with bright yellow branchlets, is the most commonly grown of these hybrids. All reported occurrences of S. babylonica need verification.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix ×sepulcralis Simonkai: Weeping willow, S. alba × S. babylonica, is introduced from Europe and widely naturalized throughout the world. Synonyms include S. ×salamonii Carrière ex Henry and S. ×sepulcralis nothovar. chrysocoma (Dode) Meikle. It is characterized by: trees, to 12 m, stems pendulous; branches somewhat to highly brittle at base, yellowish, yellow-green, or yellow-brown; branchlets yellowish, yellow-green, or golden; stipules rudimentary or foliaceous on late leaves; petiole not glandular or with pairs or clusters of spherical glands distally or scattered throughout, short-silky adaxially; largest medial blade amphistomatous or hemiamphistomatous, narrowly elliptic to very narrowly so, margins finely serrulate or spinulose-serrulate, abaxial surface glaucous, adaxial glaucous, sparsely long-silky to glabrescent, hairs white or white and ferruginous, adaxial surface slightly glossy; catkins on distinct flowering branchlet 3–14 mm; staminate moderately densely flowered, slender, 23–53 × 3–9 mm; pistillate moderately densely to loosely flowered, slender to stout, 18–30 × 3–8 mm, flowering branchlet 3–14 mm; pistillate bracts persistent after flowering; staminate abaxial and adaxial nectaries distinct; stamens 2; anthers 0.5–0.8 mm; pistillate nectary longer than stipe; stipe 0–0.2 mm; ovaries gradually tapering to styles; ovules 4 per ovary; styles 0.15–2 mm; capsules 1–2 mm. In the flora area, it occurs in: British Columbia, New Brunswick, Nova Scotia, Ontario, Quebec; Alaska, Arizona, Arkansas, California, Connecticut, District of Columbia, Illinois, Iowa, Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Missouri, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina, Ohio, Oregon, Pennsylvania, Tennessee, Utah, Virginia, and West Virginia.</discussion>
  <discussion>The most commonly cultivated, and sometimes escaped, weeping willow with golden or yellow-green branchlets is Salix ×sepulcralis nothovar. chrysocoma (Dode) Meikle. It probably originated as S. alba var. vitellina × S. babylonica (R. D. Meikle 1984). According to F. S. Santamour Jr. and A. J. McArdle (1988), S. ×sepulcralis cv. Salamonii has a broadly pyramidal crown and is only slightly pendulous. It is not clear just how this cultivar differs from S. ×pendulina. For a discussion of the taxonomy of these and other weeping willows see J. Chmela (1983).</discussion>
  <discussion>Salix ×pendulina Wenderoth: Weeping willow, S. babylonica × S. euxina, is introduced from Europe and grown throughout the world. It is characterized by: trees, 2.5–12 m, stems pendulous; branches highly brittle at base, yellow-brown, gray-brown, or red-brown; branchlets yellowish to brownish; stipules foliaceous on late leaves; petioles glabrous, pilose, or velvety to glabrescent adaxially; largest medial blade amphistomatous or hypostomatous, very narrowly elliptic to lanceolate, or linear, margins serrulate, irregularly so, or spinulose-serrulate, abaxial surface glaucous, adaxial slightly glossy or dull; catkins on distinct flowering branchlet, 3–14 mm; staminate loosely flowered, stout, 16–34 × 7–11 mm; pistillate densely or moderately densely flowered, slender or stout, 20–36 × 3.5–11 mm; pistillate bract persistent after flowering; staminate abaxial and adaxial nectaries connate and shallowly cup-shaped; stamens 2; anthers 0.5–0.6 mm; pistillate nectary longer than stipe; stipe 0 mm; styles 0.2–0.6 mm; ovules 4–8 per ovary; capsules 1.8–3.5 mm. In the flora area, it occurs in: Ontario; California, Connecticut, District of Columbia, Georgia, Illinois, Indiana, Maine, Maryland, Massachusetts, Michigan, Missouri, Nebraska, New Jersey, New Mexico, New York, North Carolina, Ohio, Oregon, Pennsylvania, Texas, Virginia, Washington, and West Virginia.</discussion>
  <discussion>Reports of this hybrid in British Columbia and California are undocumented. Plants of Salix ×pendulina with prominent, caudate stipules are var. blanda (Andersson) Meikle; those with ovaries with patchy or streaky hairiness are var. elegantissima (K. Koch) Meikle.</discussion>
  
</bio:treatment>