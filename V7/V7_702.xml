<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="P. Gaertner" date="unknown" rank="genus">ARMORACIA</taxon_name>
    <place_of_publication>
      <publication_title>Oekon. Fl. Wetterau</publication_title>
      <place_in_publication>2: 426. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus ARMORACIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Ancient Greek name for horseradish, or perhaps Celtic ar, near, mor, sea, and rich, against, alluding to habitat</other_info_on_name>
    <other_info_on_name type="fna_id">102627</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(aquatic or of mesic habitats, with rootstocks);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous.</text>
      <biological_entity id="o10076" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, branched distally.</text>
      <biological_entity id="o10077" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate and sessile;</text>
      <biological_entity id="o10078" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate, long-petiolate, blade margins crenate or pinnatifid [entire];</text>
      <biological_entity constraint="basal" id="o10079" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="long-petiolate" value_original="long-petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o10080" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline petiolate or sessile distally, blade margins crenate, serrate, pinnatifid, pinnatisect [laciniate].</text>
      <biological_entity constraint="cauline" id="o10081" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o10082" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes ([often corymbose-paniculate], several-flowered), considerably elongated in fruit.</text>
      <biological_entity id="o10084" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending, divaricate, or slightly reflexed, slender.</text>
      <biological_entity id="o10083" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o10084" is_modifier="false" modifier="considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o10085" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o10083" id="r702" name="fruiting" negation="false" src="d0_s10" to="o10085" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals spreading or ascending, ovate or oblong, lateral pair not saccate basally, (glabrous);</text>
      <biological_entity id="o10086" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10087" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, obovate, or oblanceolate [spatulate, oblong], claw somewhat differentiated from blade (relatively short, apex obtuse);</text>
      <biological_entity id="o10088" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10089" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o10090" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o10091" is_modifier="false" modifier="somewhat" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o10091" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens slightly tetradynamous;</text>
      <biological_entity id="o10092" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10093" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments slightly dilated basally;</text>
      <biological_entity id="o10094" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o10095" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate [oblong or linear], (apex obtuse);</text>
      <biological_entity id="o10096" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o10097" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands confluent, subtending bases of stamens, median glands present.</text>
      <biological_entity id="o10098" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o10099" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o10100" name="base" name_original="bases" src="d0_s16" type="structure" />
      <biological_entity id="o10101" name="stamen" name_original="stamens" src="d0_s16" type="structure" />
      <biological_entity constraint="median" id="o10102" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o10099" id="r703" name="subtending" negation="false" src="d0_s16" to="o10100" />
      <relation from="o10099" id="r704" name="part_of" negation="false" src="d0_s16" to="o10101" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits: silicles, sessile, oblong, ovate, elliptic, or orbicular, angustiseptate;</text>
      <biological_entity id="o10103" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s17" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s17" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
      <biological_entity id="o10104" name="silicle" name_original="silicles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>valves each not veined;</text>
      <biological_entity id="o10105" name="fruit" name_original="fruits" src="d0_s18" type="structure" />
      <biological_entity id="o10106" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o10107" name="fruit" name_original="fruits" src="d0_s19" type="structure" />
      <biological_entity id="o10108" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum perforated or reduced to a rim;</text>
      <biological_entity id="o10109" name="fruit" name_original="fruits" src="d0_s20" type="structure" />
      <biological_entity id="o10110" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="perforated" value_original="perforated" />
        <character constraint="to rim" constraintid="o10111" is_modifier="false" name="size" src="d0_s20" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10111" name="rim" name_original="rim" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>ovules 8–12 [–20] per ovary;</text>
      <biological_entity id="o10112" name="fruit" name_original="fruits" src="d0_s21" type="structure" />
      <biological_entity id="o10113" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="20" />
        <character char_type="range_value" constraint="per ovary" constraintid="o10114" from="8" name="quantity" src="d0_s21" to="12" />
      </biological_entity>
      <biological_entity id="o10114" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style obsolete or distinct;</text>
      <biological_entity id="o10115" name="fruit" name_original="fruits" src="d0_s22" type="structure" />
      <biological_entity id="o10116" name="style" name_original="style" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" name="fusion" src="d0_s22" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate, (sometimes 2-lobed).</text>
      <biological_entity id="o10117" name="fruit" name_original="fruits" src="d0_s23" type="structure" />
      <biological_entity id="o10118" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds biseriate, plump, not winged, ovate [oblong];</text>
      <biological_entity id="o10119" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s24" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="size" src="d0_s24" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s24" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (punctate) not mucilaginous when wetted;</text>
      <biological_entity id="o10120" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons accumbent.</text>
      <biological_entity id="o10121" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s26" value="accumbent" value_original="accumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; c, s Europe, Asia (Russian Far East, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Horseradish</other_name>
  <discussion>Species 3 (1 in the flora).</discussion>
  <references>
    <reference>Fosberg, F. R. 1965. Nomenclature of the horseradish (Cruciferae). Baileya 13: 1–4.</reference>
    <reference>Lawrence, G. H. M. 1971. The horseradish Armoracia rusticana. Herbarist 37: 17–19.</reference>
  </references>
  
</bio:treatment>