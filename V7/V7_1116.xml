<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">620</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="treatment_page">658</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 327. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species pinetorum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094906</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 126. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o38108" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes (sessile or short-stalked), 6–8-rayed, rays furcate or bifurcate, (tuberculate, less so on outer layers).</text>
      <biological_entity id="o38109" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o38110" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o38111" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few from base, ascending to erect, (0.5–) 1–2 (–3.5) dm.</text>
      <biological_entity id="o38112" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o38113" is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s3" to="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o38113" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: (petiole tapering to blade);</text>
      <biological_entity constraint="basal" id="o38114" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade rhombic to elliptic and irregularly angular, sometimes spatulate to oblanceolate, 1.5–7.5 (–10) cm, margins entire.</text>
      <biological_entity constraint="basal" id="o38115" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o38116" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic to elliptic" value_original="rhombic to elliptic" />
        <character is_modifier="false" modifier="irregularly" name="arrangement_or_shape" src="d0_s5" value="angular" value_original="angular" />
        <character char_type="range_value" from="spatulate" modifier="sometimes" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="7.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o38117" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: (not or loosely overlapping, petiolate or distal sessile);</text>
      <biological_entity constraint="cauline" id="o38118" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade spatulate to oblanceolate, 1–4 cm, margins entire.</text>
      <biological_entity constraint="cauline" id="o38119" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o38120" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o38121" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes crowded, elongated.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels (ascending, curved or sigmoid), 6–12 (–20) mm.</text>
      <biological_entity id="o38122" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38123" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o38122" id="r2567" name="fruiting" negation="false" src="d0_s9" to="o38123" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ovate, oblong, or elliptic 4–7.5 mm, (median pair thickened apically, cucullate);</text>
      <biological_entity id="o38124" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38125" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals spatulate or broadly cuneate, 6–13 mm, (claw slightly expanded at base).</text>
      <biological_entity id="o38126" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38127" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits (substipitate), globose or obovoid to ellipsoid, sometimes slightly obcompressed, 4–9 mm;</text>
      <biological_entity id="o38128" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s12" to="ellipsoid" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s12" value="obcompressed" value_original="obcompressed" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves (not retaining seeds after dehiscence), glabrous throughout;</text>
      <biological_entity id="o38129" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o38130" name="replum" name_original="replum" src="d0_s14" type="structure">
        <character constraint="than fruit" constraintid="o38131" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o38131" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 4–24 per ovary;</text>
      <biological_entity id="o38132" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o38133" from="4" name="quantity" src="d0_s15" to="24" />
      </biological_entity>
      <biological_entity id="o38133" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style (2–) 4–7 mm.</text>
      <biological_entity id="o38134" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds flattened.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 10.</text>
      <biological_entity id="o38135" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o38136" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Scrub oak, pinyon-juniper woodland, open ponderosa pine forests, these sometimes mixed with Douglas fir, white pine, white fir, Engelmann spruce, or Gambel oak, on limestone-derived or otherwise basic soils, often in rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scrub oak" />
        <character name="habitat" value="pinyon-juniper woodland" />
        <character name="habitat" value="open ponderosa pine forests" />
        <character name="habitat" value="douglas" />
        <character name="habitat" value="these mixed with douglas fir" modifier="sometimes" />
        <character name="habitat" value="white pine" />
        <character name="habitat" value="white fir" />
        <character name="habitat" value="engelmann" />
        <character name="habitat" value="gambel oak" modifier="or" />
        <character name="habitat" value="limestone-derived" modifier="on" />
        <character name="habitat" value="basic soils" modifier="otherwise" />
        <character name="habitat" value="rock crevices" modifier="often in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2900 (-3400) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1400" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3400" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>68.</number>
  <other_name type="common_name">White Mountain bladderpod</other_name>
  <discussion>Physaria pinetorum with reduced forms are found at high elevations; in disturbed, moist soils plants can become quite large, as in the Manzano Mountains. Densely cespitose plants with crowded racemes not exceeding the basal leaves are found at the crest (3200–3400 m) of the Sandia Mountains, New Mexico. These probably represent an undescribed taxon.</discussion>
  
</bio:treatment>