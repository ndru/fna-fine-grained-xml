<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">697</other_info_on_meta>
    <other_info_on_meta type="treatment_page">698</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1818" rank="genus">stanleya</taxon_name>
    <taxon_name authority="(Pursh) Britton" date="1889" rank="species">pinnata</taxon_name>
    <taxon_name authority="(E. James) Rollins" date="1939" rank="variety">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Lloydia</publication_title>
      <place_in_publication>2: 118. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus stanleya;species pinnata;variety integrifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095319</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stanleya</taxon_name>
    <taxon_name authority="E. James" date="unknown" rank="species">integrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Account Exped. Pittsburgh</publication_title>
      <place_in_publication>2: 17. 1823 (as Stanleyea)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stanleya;species integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stanleya</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">glauca</taxon_name>
    <taxon_name authority="Cockerell" date="unknown" rank="variety">latifolia</taxon_name>
    <taxon_hierarchy>genus Stanleya;species glauca;variety latifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stanleya</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">pinnatifida</taxon_name>
    <taxon_name authority="(E. James) B. L. Robinson" date="unknown" rank="variety">integrifolia</taxon_name>
    <taxon_hierarchy>genus Stanleya;species pinnatifida;variety integrifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Cauline leaves: blade broadly ovate to lanceolate, margins usually entire, rarely proximalmost dentate.</text>
      <biological_entity constraint="cauline" id="o30316" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o30317" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s0" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o30318" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30319" name="blade" name_original="blade" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s0" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: sepals 11–16 mm;</text>
      <biological_entity id="o30320" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o30321" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s1" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petals 11–16 mm, claw 6–9 mm;</text>
      <biological_entity id="o30322" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o30323" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s2" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30324" name="claw" name_original="claw" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>gynophore 12–20 mm, pubescent basally.</text>
      <biological_entity id="o30325" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o30326" name="gynophore" name_original="gynophore" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruits 3–8 cm;</text>
      <biological_entity id="o30327" name="fruit" name_original="fruits" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ovules 26–34 per ovary.</text>
      <biological_entity id="o30328" name="ovule" name_original="ovules" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o30329" from="26" name="quantity" src="d0_s5" to="34" />
      </biological_entity>
      <biological_entity id="o30329" name="ovary" name_original="ovary" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds 2.5–4.5 mm. 2n = 56.</text>
      <biological_entity id="o30330" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30331" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Barren rocky hillsides, gravelly knolls, badlands, dry prairies, arroyos, dry washes, shaley sandstone, sagebrush and pinyon-juniper communities, shale slopes and cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren rocky hillsides" />
        <character name="habitat" value="gravelly knolls" />
        <character name="habitat" value="badlands" />
        <character name="habitat" value="dry prairies" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="dry washes" />
        <character name="habitat" value="shaley sandstone" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper communities" />
        <character name="habitat" value="shale slopes" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b.</number>
  
</bio:treatment>