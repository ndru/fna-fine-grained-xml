<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="treatment_page">347</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="J. F. Macbride &amp; Payson" date="1917" rank="genus">ANELSONIA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>64: 81. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus ANELSONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Aven Nelson, 1859–1952, American botanist who studied the flora of Wyoming and neighboring states</other_info_on_name>
    <other_info_on_name type="fna_id">101721</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, deep-rooted);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>pubescent, trichomes short-stalked, dendritic or irregularly forked, (soft).</text>
      <biological_entity id="o9772" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o9773" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="dendritic" value_original="dendritic" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, unbranched.</text>
      <biological_entity id="o9774" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves (persistent) basal;</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>petiolate;</text>
      <biological_entity id="o9775" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade margins entire.</text>
      <biological_entity constraint="blade" id="o9776" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, few to several-flowered), not or slightly elongated in fruit.</text>
      <biological_entity id="o9778" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending to suberect, slender.</text>
      <biological_entity id="o9777" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o9778" is_modifier="false" modifier="not; slightly" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="suberect" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o9779" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o9777" id="r689" name="fruiting" negation="false" src="d0_s10" to="o9779" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (early caducous, erect), oblong, (pubescent), base of lateral pair not saccate;</text>
      <biological_entity id="o9780" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o9781" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o9782" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9783" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <relation from="o9782" id="r690" name="part_of" negation="false" src="d0_s11" to="o9783" />
    </statement>
    <statement id="d0_s12">
      <text>petals white to purplish, oblanceolate, (slightly longer than sepals);</text>
      <biological_entity id="o9784" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9785" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="purplish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o9786" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o9787" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o9788" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o9789" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate;</text>
      <biological_entity id="o9790" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o9791" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands confluent, subtending bases of stamens.</text>
      <biological_entity id="o9792" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o9793" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o9794" name="base" name_original="bases" src="d0_s16" type="structure" />
      <biological_entity id="o9795" name="stamen" name_original="stamens" src="d0_s16" type="structure" />
      <relation from="o9793" id="r691" name="subtending" negation="false" src="d0_s16" to="o9794" />
      <relation from="o9793" id="r692" name="part_of" negation="false" src="d0_s16" to="o9795" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits (erect, siliques or silicles), sessile or short-stipitate, lanceolate, broadly oblong to narrowly ovate, not torulose, latiseptate;</text>
      <biological_entity id="o9796" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="short-stipitate" value_original="short-stipitate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s17" to="narrowly ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein and somewhat anastomosing lateral-veins, glabrous;</text>
      <biological_entity id="o9797" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9798" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o9799" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="true" modifier="somewhat" name="architecture" src="d0_s18" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <relation from="o9797" id="r693" name="with" negation="false" src="d0_s18" to="o9798" />
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o9800" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o9801" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 10–24 per ovary;</text>
      <biological_entity id="o9802" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o9803" from="10" name="quantity" src="d0_s21" to="24" />
      </biological_entity>
      <biological_entity id="o9803" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate.</text>
      <biological_entity id="o9804" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds biseriate, somewhat flattened, not winged, oblong to ovoid;</text>
      <biological_entity id="o9805" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s23" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s23" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (silvery, papillate), not mucilaginous when wetted;</text>
      <biological_entity id="o9806" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 7.</text>
      <biological_entity id="o9807" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s25" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o9808" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Species 1.</discussion>
  <discussion>Anelsonia is most closely related to Boechera and Phoenicaulis, from which it is readily distinguished by its scapose habit and papillate seeds.</discussion>
  
</bio:treatment>