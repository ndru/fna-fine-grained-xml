<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="treatment_page">261</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arabis</taxon_name>
    <taxon_name authority="R. M. Harper" date="1903" rank="species">georgiana</taxon_name>
    <place_of_publication>
      <publication_title>Torreya</publication_title>
      <place_in_publication>3: 88. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus arabis;species georgiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094732</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to moderately hirsute (at least basally), trichomes simple, mixed with fewer, short-stalked, forked ones, subsessile cruciform or 3-rayed stellate trichomes commonly on abaxial blade surfaces, sometimes plants glabrous distally.</text>
      <biological_entity id="o34825" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o34826" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="with ones" constraintid="o34827" is_modifier="false" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cruciform" value_original="cruciform" />
        <character name="shape" src="d0_s1" value="3-rayed" value_original="3-rayed" />
      </biological_entity>
      <biological_entity id="o34827" name="one" name_original="ones" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="true" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o34828" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o34829" name="surface" name_original="surfaces" src="d0_s1" type="structure" constraint_original="abaxial blade" />
      <biological_entity id="o34830" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <relation from="o34828" id="r2335" name="on" negation="false" src="d0_s1" to="o34829" />
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or few from base (rosette), erect, unbranched or branched (few) distally, 3–7 dm, (hirsute basally, glabrous distally).</text>
      <biological_entity id="o34831" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o34832" is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o34832" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole 0.5–2 cm, (ciliate or not);</text>
      <biological_entity constraint="basal" id="o34833" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o34834" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spatulate, oblanceolate, or obovate, 1.5–6 cm × 5–15 mm, margins dentate, apex obtuse or acute, abaxial surface moderately to sparsely pubescent, trichomes subsessile stellate, adaxial surface subglabrate or sparsely stellate.</text>
      <biological_entity constraint="basal" id="o34835" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o34836" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34837" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o34838" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o34839" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately to sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o34840" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34841" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="subglabrate" value_original="subglabrate" />
        <character is_modifier="false" modifier="sparsely" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 7–26;</text>
      <biological_entity constraint="cauline" id="o34842" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="26" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblong, lanceolate, or linear-lanceolate, 1.5–7 cm × 3–18 mm, base auriculate to subamplexicaul, margins dentate or entire, apex acute or obtuse, pubescent as basal leaves except distalmost leaves often glabrous.</text>
      <biological_entity id="o34843" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34844" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subamplexicaul" value_original="subamplexicaul" />
      </biological_entity>
      <biological_entity id="o34845" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o34846" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character constraint="as basal leaves" constraintid="o34847" is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o34847" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distalmost" id="o34848" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o34847" id="r2336" name="except" negation="false" src="d0_s6" to="o34848" />
    </statement>
    <statement id="d0_s7">
      <text>Racemes often simple.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels erect to erect-ascending, 7–16 mm, (glabrous).</text>
      <biological_entity id="o34849" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="erect-ascending" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34850" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o34849" id="r2337" name="fruiting" negation="false" src="d0_s8" to="o34850" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong, 2.5–4.5 × 1–1.5 mm, lateral pair subsaccate basally;</text>
      <biological_entity id="o34851" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34852" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s9" value="subsaccate" value_original="subsaccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, narrowly spatulate or oblanceolate, 6–9 × 1–1.5 mm, apex obtuse;</text>
      <biological_entity id="o34853" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34854" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34855" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 3–4.5 mm;</text>
      <biological_entity id="o34856" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34857" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers oblong, 0.8–1 mm.</text>
      <biological_entity id="o34858" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34859" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits erect to erect-ascending, (often subappressed to rachis), smooth, 4–7 cm × 0.7–0.8 mm;</text>
      <biological_entity id="o34860" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s13" to="erect-ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s13" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves each with midvein extending full length or to middle;</text>
      <biological_entity id="o34861" name="valve" name_original="valves" src="d0_s14" type="structure" />
      <biological_entity id="o34862" name="midvein" name_original="midvein" src="d0_s14" type="structure" />
      <relation from="o34861" id="r2338" name="with" negation="false" src="d0_s14" to="o34862" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 38–44 per ovary;</text>
      <biological_entity id="o34863" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o34864" from="38" name="quantity" src="d0_s15" to="44" />
      </biological_entity>
      <biological_entity id="o34864" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 0.7–1.8 mm.</text>
      <biological_entity id="o34865" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds narrowly winged throughout, oblong, 0.9–1.9 × 0.5–0.7 mm;</text>
      <biological_entity id="o34866" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="narrowly; throughout" name="architecture" src="d0_s17" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s17" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s17" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>wing to 0.1 mm wide distally.</text>
      <biological_entity id="o34867" name="wing" name_original="wing" src="d0_s18" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" modifier="distally" name="width" src="d0_s18" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Arabis georgiana is most closely related to A. pycnocarpa, from which it is easily distinguished by having narrower fruits, longer petals, and subsessile cruciform or 3-rayed trichomes on abaxial surfaces of basal leaves. It is known only in Alabama from Bibb and Elmore counties and in Georgia from Stewart County.</discussion>
  
</bio:treatment>