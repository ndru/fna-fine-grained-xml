<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="treatment_page">412</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Rollins" date="1988" rank="genus">CUSICKIELLA</taxon_name>
    <place_of_publication>
      <publication_title>J. Jap. Bot.</publication_title>
      <place_in_publication>63: 68. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus CUSICKIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">For William C. Cusick, 1842–1922, Oregon plant collector</other_info_on_name>
    <other_info_on_name type="fna_id">108732</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose or pulvinate, taprooted, caudex much-branched, woody, with persistent leaf remains);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose or subscapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous or pubescent, trichomes simple, often mixed with stalked, forked, or subdendritic ones.</text>
      <biological_entity id="o1572" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subscapose" value_original="subscapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o1573" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="with stalked , forked , or subdendritic ones" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, unbranched.</text>
      <biological_entity id="o1574" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and sometimes cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>sessile;</text>
      <biological_entity id="o1575" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal (terminating caudex branches, thickened at base, persistent, erect), rosulate, blade margins entire;</text>
      <biological_entity constraint="basal" id="o1576" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o1577" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline (absent or few as bracts), blade (base not auriculate), margins entire.</text>
      <biological_entity constraint="cauline" id="o1578" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o1579" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o1580" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, proximal flowers sometimes bracteate), elongated in fruit.</text>
      <biological_entity id="o1582" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate or ascending, slender.</text>
      <biological_entity id="o1581" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o1582" is_modifier="false" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o1583" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o1581" id="r117" name="fruiting" negation="false" src="d0_s10" to="o1583" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (erect to ascending), oblong;</text>
      <biological_entity id="o1584" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o1585" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals white or yellowish, spatulate or oblanceolate, (slightly longer than sepals, base attenuate to short claw);</text>
      <biological_entity id="o1586" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1587" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens slightly tetradynamous;</text>
      <biological_entity id="o1588" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1589" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments dilated basally;</text>
      <biological_entity id="o1590" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1591" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate;</text>
      <biological_entity id="o1592" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o1593" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands: lateral annular, median glands confluent with lateral.</text>
      <biological_entity constraint="nectar" id="o1594" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s16" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity constraint="median" id="o1595" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character constraint="with lateral" constraintid="o1596" is_modifier="false" name="arrangement" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o1596" name="lateral" name_original="lateral" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits sessile or minutely stipitate, ovoid or ellipsoid, not torulose, terete or 4-angled, (thick and leathery);</text>
      <biological_entity id="o1597" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent or obscure midvein, (sometimes keeled), puberulent or glabrous;</text>
      <biological_entity id="o1598" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1599" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o1598" id="r118" name="with" negation="false" src="d0_s18" to="o1599" />
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o1600" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o1601" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o1602" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character constraint="per ovary" constraintid="o1603" name="quantity" src="d0_s21" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o1603" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate.</text>
      <biological_entity id="o1604" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds aseriate, plump, not winged, ovoid to oblong;</text>
      <biological_entity id="o1605" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="aseriate" value_original="aseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s23" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (smooth), not mucilaginous when wetted;</text>
      <biological_entity id="o1606" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent or obliquely so.</text>
      <biological_entity id="o1607" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
        <character name="arrangement_or_orientation" src="d0_s25" value="obliquely" value_original="obliquely" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Species of Cusickiella superficially resemble those of Draba, but the two genera are clearly unrelated. Cusickiella is easily distinguished from Draba by having incumbent cotyledons and four seeds per fruit, whereas Draba has accumbent cotyledons and more seeds per fruit.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes ebracteate; fruit valves rounded on back, each with obscure midvein; leaf blades (0.3-)0.5-1.2(-1.4) cm, trichomes on surfaces not setiform; petals white.</description>
      <determination>1 Cusickiella douglasii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Racemes bracteate basally; fruit valves keeled on back, each with prominent midvein; leaf blades 0.2-0.5 cm, trichomes on surfaces setiform; petals pale yellow.</description>
      <determination>2 Cusickiella quadricostata</determination>
    </key_statement>
  </key>
</bio:treatment>