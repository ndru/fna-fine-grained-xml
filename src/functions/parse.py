from lxml import etree
import re


def build_rank_dict(taxon_identification):
    rank_dict = {
        "family": "",
        "subfamily": "",
        "tribe": "",
        "subtribe": "",
        "genus": "",
        "subgenus": "",
        "section": "",
        "subsection": "",
        "series": "",
        "subseries": "",
        "species": "",
        "subspecies": "",
        "variety": ""
    }

    for taxon_name in taxon_identification.findall('./taxon_name'):
        for rank_name in rank_dict:
            if taxon_name.get('rank') == rank_name:
                rank_dict[rank_name] = taxon_name.text
    return(rank_dict)


def format_name(taxon_identification):
    rank_dict = build_rank_dict(taxon_identification)
    abbrev = {
        "subfamily": "subfam.",
        "tribe": "tribe",
        "subtribe": "subtribe",
        "subgenus": "subg.",
        "section": "sect.",
        "subsection": "subsect.",
        "series": "ser.",
        "subseries": "subser.",
        "subspecies": "subsp.",
        "variety": "var."
    }
    treat_name = ''

    for taxon_name in taxon_identification.xpath('taxon_name[last()]'):
        rank_name = taxon_name.get('rank')
        if rank_name == 'family' or rank_name == 'genus':
            treat_name = rank_dict[rank_name]
        elif rank_name == 'subfamily' or rank_name == 'tribe':
            treat_name = (rank_dict['family'] + ' ' +
                          abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subtribe':
            treat_name = (rank_dict['family'] + ' ' + abbrev['tribe'] + ' ' + rank_dict['tribe']
                          + abbrev['subtribe'] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subgenus' or rank_name == 'section':
            treat_name = (rank_dict['genus'] + ' ' +
                          abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subsection':
            treat_name = (rank_dict['genus'] + ' ' + rank_dict['section']
                          + abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'series':
            treat_name = (rank_dict['genus'] + ' '
                          + '(' + abbrev['section'] + ' ' +
                          rank_dict['section'] + ')'
                          + ' ' + abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subseries':
            treat_name = (rank_dict['genus'] + ' ' + '(' + abbrev['section'] + ' ' + rank_dict['section'] + ')'
                          + ' ' + abbrev['series'] + ' ' + rank_dict['series']
                          + ' ' + abbrev['subseries'] + ' ' + rank_dict[rank_name])
        elif rank_name == 'species':
            treat_name = rank_dict['genus'] + ' ' + rank_dict[rank_name]
        elif rank_name == 'subspecies' or rank_name == 'variety':
            treat_name = rank_dict['genus'] + ' ' + rank_dict['species'] + \
                ' ' + abbrev[rank_name] + ' ' + rank_dict[rank_name]
    return(treat_name)


def clean_semicolon(taxon_name):
    if taxon_name.endswith(";"):
        taxon_name = taxon_name[:-1]
    return(taxon_name)


def authority(acceptednode):
    speciesauth = acceptednode.xpath('taxon_name[last()]')[
        0].attrib['authority']
    return(speciesauth)


def print_vol(file_path):
    vol_text = re.search('\_\d*\.xml', file_path).start()
    vol_num = file_path[vol_text - 2:vol_text]
    vol_reg = re.search('\d{1,}', vol_num)
    return(vol_reg.group())


def clean_other_names(names):
    formatted = []
    for name in names:  # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
        if authority(name) is not None:
            formatted.append(format_name(name) + ' ' + authority(name))
        else:
            formatted.append(format_name(name))
    # print(formatted)
    formatted = ';'.join(formatted)
    return(formatted)


def format_distribution(tree):
    distribution = tree.find("//description[@type='distribution']")
    if distribution is not None:
        distribution_text = ''.join(distribution.itertext()).strip()
        # print(''.join(distribution.itertext()))
    else:
        distribution_text = ''
    return(distribution_text)


def collect_data(file_path):
    #    file_path = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V6/V6_242.xml'
    tree = etree.parse(file_path)

    accepted_node = tree.find("//taxon_identification[@status='ACCEPTED']")
    ranks = build_rank_dict(accepted_node)
    taxon = format_name(accepted_node)
    # print(taxon)

    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']")
    basionyms = tree.findall("//taxon_identification[@status='BASIONYM']")
    distribution = format_distribution(tree)

    taxon_hierarchy = accepted_node.find('./taxon_hierarchy').text

    row = ([file_path, print_vol(file_path), ranks['family'], ranks['genus'],
            taxon_hierarchy, taxon, authority(accepted_node),
            clean_other_names(basionyms), clean_other_names(synonyms), distribution])
    return(row)


def collect_entities(tree):
    traits = tree.findall("//biological_entity")
    for trait in traits:
        trait = trait.attrib['name']
        print(trait)
