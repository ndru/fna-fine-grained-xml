<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="unknown" rank="subgenus">Selaginella</taxon_name>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus Selaginella</taxon_hierarchy>
    <other_info_on_name type="fna_id">302113</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems radially symmetric, not articulate;</text>
      <biological_entity id="o2600" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s0" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="articulate" value_original="articulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>creeping stems with closely spaced, dichotomous forks;</text>
      <biological_entity id="o2601" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o2602" name="fork" name_original="forks" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="closely" name="arrangement" src="d0_s1" value="spaced" value_original="spaced" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="dichotomous" value_original="dichotomous" />
      </biological_entity>
      <relation from="o2601" id="r573" name="with" negation="false" src="d0_s1" to="o2602" />
    </statement>
    <statement id="d0_s2">
      <text>upright stems unbranched;</text>
      <biological_entity id="o2603" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="upright" value_original="upright" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>vessel elements absent.</text>
      <biological_entity constraint="vessel" id="o2604" name="element" name_original="elements" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Rhizophores absent.</text>
      <biological_entity id="o2605" name="rhizophore" name_original="rhizophores" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves loosely spirally arranged, monomorphic, lanceolate to lanceolate-triangular, thin, soft;</text>
      <biological_entity id="o2606" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="loosely spirally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="lanceolate-triangular" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins short-spiny;</text>
      <biological_entity id="o2607" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="short-spiny" value_original="short-spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stomates scattered throughout abaxial surface;</text>
      <biological_entity id="o2608" name="stomate" name_original="stomates" src="d0_s7" type="structure">
        <character constraint="throughout abaxial surface" constraintid="o2609" is_modifier="false" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o2609" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="throughout" name="position" src="d0_s7" value="abaxial" value_original="abaxial" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary leaves absent.</text>
      <biological_entity constraint="axillary" id="o2610" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Strobili solitary, cylindric.</text>
      <biological_entity id="o2611" name="strobilus" name_original="strobili" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sporophylls loosely spirally arranged, larger and slightly different shape from vegetative leaves, most sporophylls fertile, megasporophylls and microsporophylls same size, auricles absent.</text>
      <biological_entity id="o2612" name="sporophyll" name_original="sporophylls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="loosely spirally" name="arrangement" src="d0_s10" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
        <character constraint="from leaves" constraintid="o2613" is_modifier="false" modifier="slightly" name="character" src="d0_s10" value="shape" value_original="shape" />
      </biological_entity>
      <biological_entity id="o2613" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o2614" name="sporophyll" name_original="sporophylls" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o2615" name="megasporophyll" name_original="megasporophylls" src="d0_s10" type="structure" />
      <biological_entity id="o2616" name="microsporophyll" name_original="microsporophylls" src="d0_s10" type="structure" />
      <biological_entity id="o2617" name="auricle" name_original="auricles" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Circumboreal and Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Circumboreal and Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <references>
    <reference>Karrfalt, E. E. 1981. The comparative and developmental morphology of the root system of Selaginella selaginoides  (L.) Link. Amer. J. Bot. 68: 224–253.</reference>
    <reference>Page, C. N. 1989. Compression and slingshot megaspore ejection in Selaginella selaginoides —A new phenomenon in pteridophytes. Fern Gaz. 13: 267–275.</reference>
  </references>
  
</bio:treatment>