<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="M. G. Price" date="1983" rank="genus">pecluma</taxon_name>
    <taxon_name authority="(Humboldt &amp; Bonpland ex Willdenow) M. G. Price" date="1983" rank="species">plumula</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>73: 115. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus pecluma;species plumula</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500868</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Humboldt &amp; Bonpland ex Willdenow" date="unknown" rank="species">plumula</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>5(1): 178. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species plumula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ca. 5 mm diam.;</text>
      <biological_entity id="o14520" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character name="diameter" src="d0_s0" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales dark-brown, linear-lanceolate.</text>
      <biological_entity id="o14521" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves pendent, sometimes erect in small plants.</text>
      <biological_entity id="o14522" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character constraint="in plants" constraintid="o14523" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o14523" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole black, 1/10–1/5 length of blade, hairs few, unbranched;</text>
      <biological_entity id="o14524" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character char_type="range_value" from="1/10 length of blade" name="length" src="d0_s3" to="1/5 length of blade" />
      </biological_entity>
      <biological_entity id="o14525" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales ovate to cordate, conspicuous, inflated.</text>
      <biological_entity id="o14526" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="cordate" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade linear-elliptic, 15–50 × 3–7 cm;</text>
      <biological_entity id="o14527" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="linear-elliptic" value_original="linear-elliptic" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="50" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base abruptly cuneate;</text>
      <biological_entity id="o14528" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex tapering to acute.</text>
      <biological_entity id="o14529" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="tapering" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Segments linear, 1.5–2.5 mm wide, sparsely short-pubescent;</text>
      <biological_entity id="o14530" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>segments at base of blade abruptly reduced to lobes, not deflexed.</text>
      <biological_entity id="o14531" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" notes="" src="d0_s9" value="deflexed" value_original="deflexed" />
      </biological_entity>
      <biological_entity id="o14532" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="to lobes" constraintid="o14534" is_modifier="false" modifier="abruptly" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o14533" name="blade" name_original="blade" src="d0_s9" type="structure" />
      <biological_entity id="o14534" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <relation from="o14531" id="r3173" name="at" negation="false" src="d0_s9" to="o14532" />
      <relation from="o14532" id="r3174" name="part_of" negation="false" src="d0_s9" to="o14533" />
    </statement>
    <statement id="d0_s10">
      <text>Veins 1-forked.</text>
      <biological_entity id="o14535" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="1-forked" value_original="1-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori round;</text>
      <biological_entity id="o14536" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sporangia with 64 spores.</text>
      <biological_entity id="o14538" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="64" value_original="64" />
      </biological_entity>
      <relation from="o14537" id="r3175" name="with" negation="false" src="d0_s12" to="o14538" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 148.</text>
      <biological_entity id="o14537" name="sporangium" name_original="sporangia" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o14539" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="148" value_original="148" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic or occasionally on rock, wet woods, river banks, hammocks, and limesinks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="epiphytic or occasionally on" />
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="river banks" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="limesinks" />
        <character name="habitat" value="epiphytic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America to s Brazil.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America to s Brazil" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Plume polypody</other_name>
  
</bio:treatment>