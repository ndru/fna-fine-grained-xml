<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Presl" date="unknown" rank="family">blechnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">blechnum</taxon_name>
    <taxon_name authority="(Linnaeus) Roth" date="1794" rank="species">spicant</taxon_name>
    <place_of_publication>
      <place_in_publication>5: 411. 1793</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family blechnaceae;genus blechnum;species spicant</taxon_hierarchy>
    <other_info_on_name type="fna_id">200004244</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Osmunda</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">spicant</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1066. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Osmunda;species spicant;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Struthiopteris</taxon_name>
    <taxon_name authority="(Linnaeus) Weis" date="unknown" rank="species">spicant</taxon_name>
    <taxon_hierarchy>genus Struthiopteris;species spicant;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems slender, short-creeping or ascending, not climbing.</text>
      <biological_entity id="o8993" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves dimorphic, cespitose, erect or spreading, fertile leaves more erect and longer than sterile leaves.</text>
      <biological_entity id="o8994" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o8995" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="than sterile leaves" constraintid="o8996" is_modifier="false" name="length_or_size" src="d0_s1" value="more erect and longer" value_original="more erect and longer" />
      </biological_entity>
      <biological_entity id="o8996" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petioles of fertile leaves reddish-brown to purple-black, 15-60 cm, coarsely scaly proximally.</text>
      <biological_entity id="o8997" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s2" to="purple-black" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="coarsely; proximally" name="architecture_or_pubescence" src="d0_s2" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o8998" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o8997" id="r1917" name="part_of" negation="false" src="d0_s2" to="o8998" />
    </statement>
    <statement id="d0_s3">
      <text>Petioles of sterile leaves reddish-brown, 2-30 cm, coarsely scaly proximally.</text>
      <biological_entity id="o8999" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
        <character is_modifier="false" modifier="coarsely; proximally" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o9000" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o8999" id="r1918" name="part_of" negation="false" src="d0_s3" to="o9000" />
    </statement>
    <statement id="d0_s4">
      <text>Fertile blades erect, narrowly rhombic, 1-pinnate, without conform terminal pinna, 25-65 × 3-15 cm, glabrous.</text>
      <biological_entity id="o9001" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" notes="" src="d0_s4" to="65" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o9002" name="pinna" name_original="pinna" src="d0_s4" type="structure" />
      <relation from="o9001" id="r1919" name="without" negation="false" src="d0_s4" to="o9002" />
    </statement>
    <statement id="d0_s5">
      <text>Sterile blades spreading, narrowly oblanceolate to linear-lanceolate, pinnatifid to apex and so without conform terminal pinna, 20-75 × 3-14 cm, tapering at base, glabrous.</text>
      <biological_entity id="o9003" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character constraint="to apex" constraintid="o9004" is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" notes="" src="d0_s5" to="75" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s5" to="14" to_unit="cm" />
        <character constraint="at base" constraintid="o9006" is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9004" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o9005" name="pinna" name_original="pinna" src="d0_s5" type="structure" />
      <biological_entity id="o9006" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o9003" id="r1920" name="without" negation="false" src="d0_s5" to="o9005" />
    </statement>
    <statement id="d0_s6">
      <text>Rachises of fertile and sterile leaves with indument of filiform, spreading scales abaxially.</text>
      <biological_entity id="o9007" name="rachis" name_original="rachises" src="d0_s6" type="structure" />
      <biological_entity id="o9008" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o9009" name="indument" name_original="indument" src="d0_s6" type="structure" />
      <biological_entity id="o9010" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o9007" id="r1921" name="part_of" negation="false" src="d0_s6" to="o9008" />
      <relation from="o9007" id="r1922" name="with" negation="false" src="d0_s6" to="o9009" />
      <relation from="o9009" id="r1923" name="part_of" negation="false" src="d0_s6" to="o9010" />
    </statement>
    <statement id="d0_s7">
      <text>Fertile pinnae not articulate to rachis, sessile, decurrent and surcurrent to rachis;</text>
      <biological_entity id="o9011" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character constraint="to rachis" constraintid="o9012" is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
        <character constraint="to rachis" constraintid="o9013" is_modifier="false" name="orientation" src="d0_s7" value="surcurrent" value_original="surcurrent" />
      </biological_entity>
      <biological_entity id="o9012" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o9013" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>larger pinnae slightly curved, linear, 25-32 × 1.5-2 mm;</text>
      <biological_entity constraint="larger" id="o9014" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s8" to="32" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>margins entire, slightly to moderately revolute;</text>
      <biological_entity id="o9015" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly to moderately" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costae with indument of a few small scales abaxially, often concealed by sori.</text>
      <biological_entity id="o9016" name="costa" name_original="costae" src="d0_s10" type="structure">
        <character constraint="by sori" constraintid="o9019" is_modifier="false" modifier="often" name="prominence" notes="" src="d0_s10" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o9017" name="indument" name_original="indument" src="d0_s10" type="structure" />
      <biological_entity id="o9018" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o9019" name="sorus" name_original="sori" src="d0_s10" type="structure" />
      <relation from="o9016" id="r1924" name="with" negation="false" src="d0_s10" to="o9017" />
      <relation from="o9017" id="r1925" name="part_of" negation="false" src="d0_s10" to="o9018" />
    </statement>
    <statement id="d0_s11">
      <text>Sterile pinnae closely spaced, not articulate to rachis, base fully adnate;</text>
      <biological_entity id="o9020" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s11" value="spaced" value_original="spaced" />
        <character constraint="to rachis" constraintid="o9021" is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o9021" name="rachis" name_original="rachis" src="d0_s11" type="structure" />
      <biological_entity id="o9022" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="fully" name="fusion" src="d0_s11" value="adnate" value_original="adnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>larger pinnae curved, linear to oblong-linear or barely wider beyond middle, 15-35 × 3.5-5 mm;</text>
      <biological_entity constraint="larger" id="o9023" name="pinna" name_original="pinnae" src="d0_s12" type="structure">
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s12" to="oblong-linear" />
        <character constraint="beyond middle pinnae" constraintid="o9024" is_modifier="false" modifier="barely" name="width" src="d0_s12" value="wider" value_original="wider" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" notes="" src="d0_s12" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o9024" name="pinna" name_original="pinnae" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>margins entire.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 68.</text>
      <biological_entity id="o9025" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9026" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet coniferous woods and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet coniferous" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Deer fern</other_name>
  <discussion>Blechnum spicant is found primarily along the coast and in coastal mountain ranges.</discussion>
  
</bio:treatment>