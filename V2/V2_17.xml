<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Michael D. Windham</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">Woodsia</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>158. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus Woodsia</taxon_hierarchy>
    <other_info_on_name type="etymology">for English botanist Joseph Woods</other_info_on_name>
    <other_info_on_name type="fna_id">134973</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually on rock.</text>
      <biological_entity id="o786" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o787" name="rock" name_original="rock" src="d0_s0" type="structure" />
      <relation from="o786" id="r173" name="on" negation="false" src="d0_s0" to="o787" />
    </statement>
    <statement id="d0_s1">
      <text>Stems compact to creeping;</text>
    </statement>
    <statement id="d0_s2">
      <text>ascending or erect (rarely horizontal), stolons absent.</text>
      <biological_entity id="o788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o789" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic, dying back over winter or sometimes persistent into the next season.</text>
      <biological_entity id="o790" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character constraint="over " constraintid="o791" is_modifier="false" name="condition" src="d0_s3" value="dying" value_original="dying" />
      </biological_entity>
      <biological_entity constraint="next" id="o791" name="season" name_original="season" src="d0_s3" type="structure">
        <character is_modifier="true" name="season" src="d0_s3" value="winter" value_original="winter" />
        <character is_modifier="true" modifier="sometimes" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole 1/5–3/4 length of blade, base not conspicuously swollen;</text>
      <biological_entity id="o792" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1/5 length of blade" name="length" src="d0_s4" to="3/4 length of blade" />
      </biological_entity>
      <biological_entity id="o793" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not conspicuously" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>vascular-bundles 2, arranged laterally, ± round or oblong in cross-section.</text>
      <biological_entity id="o794" name="vascular-bundle" name_original="vascular-bundles" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" modifier="laterally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="round" value_original="round" />
        <character constraint="in cross-section" constraintid="o795" is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o795" name="cross-section" name_original="cross-section" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Blade linear to lanceolate or ovate, 1–2-pinnate-pinnatifid, gradually reduced distally to pinnatifid apex, herbaceous.</text>
      <biological_entity id="o796" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate or ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="1-2-pinnate-pinnatifid" value_original="1-2-pinnate-pinnatifid" />
        <character constraint="to apex" constraintid="o797" is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="growth_form_or_texture" notes="" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o797" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate to rachis, segment margins entire to dentate, not spiny;</text>
      <biological_entity id="o798" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character constraint="to rachis" constraintid="o799" is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o799" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity constraint="segment" id="o800" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s7" to="dentate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s7" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal pinnae somewhat reduced, sessile, bases usually ± equilateral;</text>
      <biological_entity constraint="proximal" id="o801" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o802" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="architecture_or_shape" src="d0_s8" value="equilateral" value_original="equilateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costae often shallowly grooved adaxially, grooves ± continuous from rachis to costae;</text>
      <biological_entity id="o803" name="costa" name_original="costae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often shallowly; adaxially" name="architecture" src="d0_s9" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o804" name="groove" name_original="grooves" src="d0_s9" type="structure">
        <character constraint="from rachis" constraintid="o805" is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o805" name="rachis" name_original="rachis" src="d0_s9" type="structure" />
      <biological_entity id="o806" name="costa" name_original="costae" src="d0_s9" type="structure" />
      <relation from="o805" id="r174" name="to" negation="false" src="d0_s9" to="o806" />
    </statement>
    <statement id="d0_s10">
      <text>indument of glandular (occasionally nonglandular) hairs on both surfaces, rarely absent.</text>
      <biological_entity id="o807" name="indument" name_original="indument" src="d0_s10" type="structure" constraint="hair" constraint_original="hair; hair">
        <character is_modifier="false" modifier="rarely" name="presence" notes="" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o808" name="hair" name_original="hairs" src="d0_s10" type="structure" />
      <biological_entity id="o809" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <relation from="o807" id="r175" name="part_of" negation="false" src="d0_s10" to="o808" />
      <relation from="o807" id="r176" name="on" negation="false" src="d0_s10" to="o809" />
    </statement>
    <statement id="d0_s11">
      <text>Veins free, simple or forked.</text>
      <biological_entity id="o810" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori in 1 row between midrib and margin on ultimate segments, round;</text>
      <biological_entity id="o811" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="between midrib and margin" constraintid="o813-o814" id="o812" name="row" name_original="row" src="d0_s12" type="structure" constraint_original="between  midrib and  margin, ">
        <character is_modifier="true" name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o813" name="midrib" name_original="midrib" src="d0_s12" type="structure" />
      <biological_entity id="o814" name="margin" name_original="margin" src="d0_s12" type="structure" />
      <biological_entity constraint="ultimate" id="o815" name="segment" name_original="segments" src="d0_s12" type="structure" />
      <relation from="o811" id="r177" name="in" negation="false" src="d0_s12" to="o812" />
      <relation from="o812" id="r178" name="on" negation="false" src="d0_s12" to="o815" />
    </statement>
    <statement id="d0_s13">
      <text>indusia basal, dissected into several to numerous filamentous or scalelike segments encircling sorus, persistent but often obscure in mature sori.</text>
      <biological_entity id="o816" name="indusium" name_original="indusia" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="basal" value_original="basal" />
        <character constraint="into segments" constraintid="o817" is_modifier="false" name="shape" src="d0_s13" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character constraint="in sori" constraintid="o819" is_modifier="false" modifier="often" name="prominence" src="d0_s13" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o817" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character char_type="range_value" from="several" is_modifier="true" name="quantity" src="d0_s13" to="numerous" />
        <character is_modifier="true" name="texture" src="d0_s13" value="filamentous" value_original="filamentous" />
        <character is_modifier="true" name="shape" src="d0_s13" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o818" name="sorus" name_original="sorus" src="d0_s13" type="structure" />
      <biological_entity id="o819" name="sorus" name_original="sori" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o817" id="r179" name="encircling" negation="false" src="d0_s13" to="o818" />
    </statement>
    <statement id="d0_s14">
      <text>Spores brownish, cristate, rarely rugose.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 38, 39, 41.</text>
      <biological_entity id="o820" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cristate" value_original="cristate" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="x" id="o821" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
        <character name="quantity" src="d0_s15" value="39" value_original="39" />
        <character name="quantity" src="d0_s15" value="41" value_original="41" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly north temperate regions and higher elevations in the tropics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly north temperate regions and higher elevations in the tropics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Cliff fern</other_name>
  <discussion>Woodsia is a well-marked genus; its morphology and chromosome base number (x = 41) provide evidence of relationships to the dryopteroid ferns. Most authors consider Cystopteris to be its closest ally, and the two genera are often confused in herbarium collections. The resemblance is superficial in many ways, however, and Woodsia is easily distinguished from Cystopteris by its persistent petiole bases, multilobed indusia, and obscure veins that end in hydathodes before reaching the leaf margin. The North American species of Woodsia fall into two natural groups that might be recognized as subgenera. Woodsia ilvensis, W. glabella, and W. alpina have articulate petioles, indusial segments that are uniseriate throughout and composed of cells that are much longer than wide, entire or crenate pinnules, strictly concolored stem scales, and chromosome base numbers of 39–41. They are circumboreal in distribution and show clear affinities to species found only in Eurasia. The remainder of the North American taxa have petioles that are not articulate, indusial segments that are multiseriate at the base and composed of cells that are isodiametric or slightly longer than wide, dentate pinnules, often bicolored stem scales, and a chromosome base number of 38. All of these species are endemic to the New World and probably represent a distinct lineage within the genus. Hybridization is common within these natural groups, but intergroup hybrids are relatively rare.</discussion>
  <discussion>Species ca. 30 (10 in the flora).</discussion>
  <references>
    <reference>Brown, D. F. M. 1964. A monographic study of the fern genus Woodsia. Nova Hedwigia 16: 1–154.</reference>
    <reference>Taylor, T. M. C. 1947. New species and combinations in Woodsia section Perrinia. Amer. Fern J. 37: 84–88.</reference>
    <reference>Wagner, F. S. 1987. Evidence for the origin of the hybrid cliff fern,  Woodsia  × abbeae  (Aspleniaceae: Athyrioideae). Syst. Bot. 12: 116–124.</reference>
    <reference>Windham, M. D. 1987b. Chromosomal and electrophoretic studies of the genus Woodsia in North America. Amer. J. Bot. 74: 715.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades and rachises completely glabrous except for occasional sessile glands; proximal pinnae distinctly fan-shaped, usually wider than long; leaves 0.5-1.2 cm wide, mature petioles green or straw-colored throughout.</description>
      <determination>1 Woodsia glabella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades and/or rachises with scattered hairs, scales, or stalked glands, rarely glabrescent; proximal pinnae ovate-lanceolate to deltate, usually longer than wide; leaves 1.2-12 cm wide or, if less, then proximal portion of mature petioles reddish brown or dark purple.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petioles articulate well above base, abscission zone visible as swollen node; indusial segments uniseriate throughout, composed of cells that are many times longer than wide; pinnules entire or crenate, without acute teeth on margins.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petioles not articulate above base, swollen abscission zone absent; indusial segments usually multiseriate at base, composed of cells that are isodiametric or slightly longer than wide; pinnules dentate, with acute teeth on margins.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Linear-lanceolate scales absent or very rare on abaxial pinnae surfaces; rachises with widely scattered hairs and scales, sometimes nearly glabrous; largest pinnae with 1-3 pairs of pinnules.</description>
      <determination>2 Woodsia alpina</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Linear-lanceolate scales common on abaxial pinnae surfaces; rachises with abundant hairs and scales; largest pinnae with 4-9 pairs of pinnules.</description>
      <determination>3 Woodsia ilvensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pinnae with flattened, multicellular hairs concentrated along midrib on both surfaces; mature petioles usually reddish brown or dark purple, relatively brittle and easily shattered.</description>
      <determination>4 Woodsia scopulina</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pinnae lacking flattened, multicellular hairs along midrib; mature petioles light brown to straw-colored or, if reddish brown/dark purple (in Woodsia plummerae and W. oregana), somewhat pliable and resistant to shattering.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Indusia composed of relatively broad segments, these multiseriate for most of length but often branched or divided distally.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Indusia composed of narrow, usually filamentous segments, these uniseriate for most of length.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Proximal portion of mature petioles reddish brown or dark purple; blades densely glandular, often somewhat viscid; vein tips not enlarged, barely visible on adaxial surface.</description>
      <determination>5 Woodsia plummerae</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Proximal portion of mature petioles light brown or straw-colored (sometimes darker at very base); blades sparsely to moderately glandular, rarely viscid; vein tips usually enlarged to form whitish hydathodes visible on adaxial surface.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Indusial segments branched or divided distally to form narrow, filamentous lobes; glandular hairs of blade with thin stalks and slightly expanded tips; pinnule margins usually thickened, lustrous on adaxial surface.</description>
      <determination>6 Woodsia cochisensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Indusial segments often glandular along distal edge but otherwise nearly entire, not divided into narrow, filamentous lobes; many glandular hairs of blade with thick stalks and distinctly bulbous tips; pinnule margins not noticeably thickened or lustrous.</description>
      <determination>7 Woodsia obtusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Pinnule margins (viewed from abaxial surface) smooth to somewhat ragged but usually lacking translucent projections or filaments; proximal portion of mature petioles reddish brown or dark purple; indusial filaments generally inconspicuous, concealed by or slightly surpassing mature sporangia.</description>
      <determination>8 Woodsia oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Pinnule margins (viewed from abaxial surface) with translucent projections or filaments on teeth; proximal portion of mature petioles usually light brown or straw-colored (sometimes darker at very base); indusial filaments generally apparent, often greatly surpassing mature sporangia.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Translucent projections on pinnule margins mostly 1-2-celled, occasionally filamentous; spores averaging 44-52 µm; largest pinnae divided into 3-7 pairs of closely spaced pinnules, pinna apices usually abruptly tapered to rounded.</description>
      <determination>9 Woodsia neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Translucent projections on pinnule margins mostly multicellular, often prolonged to form twisted filaments; spores averaging 37-44 µm; largest pinnae with 7-18 pairs of discrete, widely spaced pinnules, pinna apices often attenuate to narrowly acute.</description>
      <determination>10 Woodsia phillipsii</determination>
    </key_statement>
  </key>
</bio:treatment>