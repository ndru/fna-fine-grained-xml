<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="Douglas ex Lawson &amp; C. Lawson" date="1836" rank="species">ponderosa</taxon_name>
    <taxon_name authority="Engelmann in S. Watson" date="1880" rank="variety">scopulorum</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson,Bot. California</publication_title>
      <place_in_publication>2: 126. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species ponderosa;variety scopulorum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500948</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="(Engelmann) Lemmon" date="unknown" rank="species">brachyptera</taxon_name>
    <taxon_hierarchy>genus Pinus;species brachyptera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scopulorum</taxon_name>
    <taxon_hierarchy>genus Pinus;species scopulorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 24m;</text>
      <biological_entity id="o6489" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="24" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 1.5m diam.</text>
      <biological_entity id="o6490" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Twigs mostly redbrown, rarely glaucous.</text>
      <biological_entity id="o6491" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mainly 2–3 per fascicle, (7–) 10–17cm × (1.2–) 1.4–2mm.</text>
      <biological_entity id="o6492" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per fascicle" constraintid="o6493" from="2" name="quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_length" notes="" src="d0_s3" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s3" to="17" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_width" notes="" src="d0_s3" to="1.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" notes="" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6493" name="fascicle" name_original="fascicle" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pollen cones yellow.</text>
      <biological_entity constraint="pollen" id="o6494" name="cone" name_original="cones" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seed-cones mostly symmetric, 5–10cm;</text>
      <biological_entity id="o6495" name="cone-seed" name_original="seed-cones" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s5" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="5" from_unit="cm" name="distance" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apophyses of fertile scales moderately raised;</text>
      <biological_entity id="o6496" name="apophysis" name_original="apophyses" src="d0_s6" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s6" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity id="o6497" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o6496" id="r1370" name="part_of" negation="false" src="d0_s6" to="o6497" />
    </statement>
    <statement id="d0_s7">
      <text>umbo low pyramidal, narrowing acuminately to a stout-based prickle or short sharp spur.</text>
      <biological_entity id="o6498" name="umbo" name_original="umbo" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
        <character constraint="to spur" constraintid="o6500" is_modifier="false" name="width" src="d0_s7" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o6499" name="prickle" name_original="prickle" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="stout-based" value_original="stout-based" />
      </biological_entity>
      <biological_entity id="o6500" name="spur" name_original="spur" src="d0_s7" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s7" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seed body 3–4mm;</text>
      <biological_entity constraint="seed" id="o6501" name="body" name_original="body" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>wing to 15mm.</text>
      <biological_entity id="o6502" name="wing" name_original="wing" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tablelands, canyon slopes and rims, and foothills, western Great Plains, Rocky Mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tablelands" />
        <character name="habitat" value="canyon slopes" />
        <character name="habitat" value="rims" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–3000m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Colo., Idaho, Mont., Nebr., Nev., N.Mex., N.Dak., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25c</number>
  <other_name type="common_name">Rocky Mountain ponderosa pine</other_name>
  <discussion>The most important timber pine of the Rocky Mountains is Pinus ponderosa var. scopulorum. It intergrades with P. ponderosa var. ponderosa in Idaho, Montana, and Washington, and with P. ponderosa var. arizonica in Arizona, New Mexico, and Mexico.</discussion>
  
</bio:treatment>