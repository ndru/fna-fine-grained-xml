<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">dryopteris</taxon_name>
    <taxon_name authority="(W. Palmer) Knowlton" date="1900" rank="species">celsa</taxon_name>
    <place_of_publication>
      <publication_title>W. Palmer, &amp; Pollard, Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>13: 202. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus dryopteris;species celsa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500592</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Hooker ex Goldie) A. Gray" date="unknown" rank="species">goldiana</taxon_name>
    <taxon_name authority="W. Palmer" date="unknown" rank="subspecies">celsa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>13: 65. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dryopteris;species goldiana;subspecies celsa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves monomorphic, dying back in winter, 65–120 × 15–30 cm.</text>
      <biological_entity id="o2731" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="monomorphic" value_original="monomorphic" />
        <character constraint="in winter" is_modifier="false" name="condition" src="d0_s0" value="dying" value_original="dying" />
        <character char_type="range_value" from="65" from_unit="cm" name="length" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petiole 1/3 length of leaf, scaly at least at base;</text>
      <biological_entity id="o2732" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character name="length" src="d0_s1" value="1/3 length of leaf" value_original="1/3 length of leaf" />
        <character constraint="at base" constraintid="o2733" is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o2733" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>scales scattered, dark-brown or tan with dark central stripe.</text>
      <biological_entity id="o2734" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character constraint="with central stripe" constraintid="o2735" is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="central" id="o2735" name="stripe" name_original="stripe" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade green, ovatelanceolate, gradually tapering to tip, pinnate-pinnatifid, herbaceous, not glandular.</text>
      <biological_entity id="o2736" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="to tip" constraintid="o2737" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2737" name="tip" name_original="tip" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pinnae ± in plane of blade, lanceolate-ovate;</text>
      <biological_entity id="o2738" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate-ovate" value_original="lanceolate-ovate" />
      </biological_entity>
      <biological_entity id="o2739" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <relation from="o2738" id="r602" modifier="in plane" name="part_of" negation="false" src="d0_s4" to="o2739" />
    </statement>
    <statement id="d0_s5">
      <text>basal pinnae linear-oblong, much reduced, basal pinnules longer than adjacent pinnules, basal basiscopic pinnule and basal acroscopic pinnule equal;</text>
      <biological_entity constraint="basal" id="o2740" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2741" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character constraint="than adjacent pinnules , basal basiscopic pinnule and basal acroscopic pinnule" constraintid="o2742, o2744" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2742" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o2744" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pinnule margins crenately toothed.</text>
      <biological_entity constraint="pinnule" id="o2745" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="crenately" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sori midway between midvein and margin of segments.</text>
      <biological_entity id="o2746" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character constraint="between midvein, margin" constraintid="o2747, o2748" is_modifier="false" name="position" src="d0_s7" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity id="o2747" name="midvein" name_original="midvein" src="d0_s7" type="structure" />
      <biological_entity id="o2748" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o2749" name="segment" name_original="segments" src="d0_s7" type="structure" />
      <relation from="o2747" id="r603" name="part_of" negation="false" src="d0_s7" to="o2749" />
      <relation from="o2748" id="r604" name="part_of" negation="false" src="d0_s7" to="o2749" />
    </statement>
    <statement id="d0_s8">
      <text>Indusia lacking glands.</text>
      <biological_entity id="o2751" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 164.</text>
      <biological_entity id="o2750" name="indusium" name_original="indusia" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o2752" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="164" value_original="164" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seepage slopes, hammocks and logs in swamps, mostly on the Piedmont and Coastal Plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="seepage" />
        <character name="habitat" value="swamps" modifier="slopes hammocks and logs in" />
        <character name="habitat" value="coastal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Ga., Ill., Ky., La., Md., Mich., Mo., N.J., N.Y., N.C., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Log fern</other_name>
  <discussion>Dryopteris celsa is a fertile allotetraploid derived from hybridization between D. goldieana and D. ludoviciana. Dryopteris celsa hybridizes with six species; hybrids can usually be identified by the dark-striped scales.</discussion>
  
</bio:treatment>