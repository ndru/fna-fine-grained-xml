<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="Humboldt &amp; Bonpland ex Willdenow" date="1810" rank="genus">pleopeltis</taxon_name>
    <taxon_name authority="(T. Wendt) E. G. Andrews &amp; Windham in Windham" date="1993" rank="species">riograndensis</taxon_name>
    <place_of_publication>
      <publication_title>in Windham,Contr. Univ. Michigan Herb.</publication_title>
      <place_in_publication>19: 46. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus pleopeltis;species riograndensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500971</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="A. Braun ex Klotzsch" date="unknown" rank="species">thyssanolepis</taxon_name>
    <taxon_name authority="T. Wendt" date="unknown" rank="variety">riograndense</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>70: 6. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species thyssanolepis;variety riograndense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, sparingly branched, 2–3 mm diam.;</text>
      <biological_entity id="o7525" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales subulate to lanceolate-acuminate, centrally clathrate with cell luminae large and clear, surfaces glabrous, margins lacerate-ciliate.</text>
      <biological_entity id="o7526" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s1" to="lanceolate-acuminate" />
        <character constraint="with cell luminae" constraintid="o7527" is_modifier="false" modifier="centrally" name="architecture" src="d0_s1" value="clathrate" value_original="clathrate" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity constraint="cell" id="o7527" name="lumina" name_original="luminae" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o7528" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7529" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s1" value="lacerate-ciliate" value_original="lacerate-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves to 20 cm, strongly hygroscopic.</text>
      <biological_entity id="o7530" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="to 20 cm; strongly" name="architecture" src="d0_s2" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole grooved, otherwise round in cross-section, sparsely scaly;</text>
      <biological_entity id="o7531" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character constraint="in cross-section" constraintid="o7532" is_modifier="false" modifier="otherwise" name="shape" src="d0_s3" value="round" value_original="round" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" notes="" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o7532" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>scales rarely overlapping, margins denticulate to ciliate.</text>
      <biological_entity id="o7533" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o7534" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s4" to="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade triangular-oblong to ovate, deeply pinnatifid, to 5 cm wide, moderately scaly abaxially, glabrous adaxially;</text>
      <biological_entity id="o7535" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="triangular-oblong" name="shape" src="d0_s5" to="ovate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="moderately; abaxially" name="architecture_or_pubescence" src="d0_s5" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales concolored to obscurely bicolored, usually dark reddish-brown throughout, broadly ovatelanceolate, clathrate, more than 0.5 mm wide, margins fringed-ciliate.</text>
      <biological_entity id="o7536" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character char_type="range_value" from="concolored" name="coloration" src="d0_s6" to="obscurely bicolored" />
        <character is_modifier="false" modifier="usually; throughout" name="coloration" src="d0_s6" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="clathrate" value_original="clathrate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Venation mostly free with occasional areoles, never more than 1 included veinlet in fertile areoles.</text>
      <biological_entity id="o7537" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="fringed-ciliate" value_original="fringed-ciliate" />
        <character constraint="with areoles" constraintid="o7538" is_modifier="false" modifier="mostly" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o7538" name="areole" name_original="areoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="occasional" value_original="occasional" />
        <character char_type="range_value" from="1" modifier="never" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7539" name="veinlet" name_original="veinlet" src="d0_s7" type="structure" />
      <biological_entity id="o7540" name="areole" name_original="areoles" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o7539" id="r1569" name="in" negation="false" src="d0_s7" to="o7540" />
    </statement>
    <statement id="d0_s8">
      <text>Sori round, discrete, surficial to shallowly embossed, soral scales attached at periphery of receptacle.</text>
      <biological_entity id="o7541" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="discrete" value_original="discrete" />
        <character is_modifier="false" name="position" src="d0_s8" value="surficial" value_original="surficial" />
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s8" value="embossed" value_original="embossed" />
      </biological_entity>
      <biological_entity constraint="soral" id="o7542" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character constraint="at periphery" constraintid="o7543" is_modifier="false" name="fixation" src="d0_s8" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o7543" name="periphery" name_original="periphery" src="d0_s8" type="structure" />
      <biological_entity id="o7544" name="receptacle" name_original="receptacle" src="d0_s8" type="structure" />
      <relation from="o7543" id="r1570" name="part_of" negation="false" src="d0_s8" to="o7544" />
    </statement>
    <statement id="d0_s9">
      <text>Spores smooth with scattered spheric deposits on surface, 60–74 µm. 2n = 148.</text>
      <biological_entity id="o7545" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character constraint="with deposits" constraintid="o7546" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="60" from_unit="um" name="some_measurement" notes="" src="d0_s9" to="74" to_unit="um" />
      </biological_entity>
      <biological_entity id="o7546" name="deposit" name_original="deposits" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="shape" src="d0_s9" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o7547" name="surface" name_original="surface" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o7548" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="148" value_original="148" />
      </biological_entity>
      <relation from="o7546" id="r1571" name="on" negation="false" src="d0_s9" to="o7547" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Growing on rocky slopes and ledges, and in crevices, usually in moist, shaded canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="growing on" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="crevices" modifier="and in" />
        <character name="habitat" value="moist" modifier="usually in" />
        <character name="habitat" value="shaded canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Rio Grande scaly polypody</other_name>
  <discussion>In the past Pleopeltis riograndensis has been treated as a variety of P. thyssanolepis, but it differs from the latter species in that the petiole and leaf are only sparsely scaly rather than densely so, the blade scales are mostly ovate or ovate-lanceolate rather than nearly spheric, the venation is mostly free rather than mostly areolate, and the basal segments of the blade are alternate rather than opposite to nearly opposite.</discussion>
  
</bio:treatment>