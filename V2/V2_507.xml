<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">dryopteris</taxon_name>
    <taxon_name authority="(Villars) H. P. Fuchs" date="1959" rank="species">carthusiana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Bot. France</publication_title>
      <place_in_publication>105: 339. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus dryopteris;species carthusiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500591</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Villars" date="unknown" rank="species">carthusianum</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Pl. Dauphiné</publication_title>
      <place_in_publication>1: 292. 1786</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species carthusianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Jacquin) Schinz &amp; Thellung" date="unknown" rank="species">austriaca</taxon_name>
    <taxon_name authority="(O. F. Mueller) Fiori" date="unknown" rank="variety">spinulosa</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species austriaca;variety spinulosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(O. F. Mueller) Watt" date="unknown" rank="species">spinulosa</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species spinulosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="O. F. Mueller" date="unknown" rank="species">spinulosum</taxon_name>
    <taxon_hierarchy>genus Polypodium;species spinulosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves monomorphic, dying in winter, 15–75 × 10–30 cm.</text>
      <biological_entity id="o16495" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="monomorphic" value_original="monomorphic" />
        <character constraint="in winter" is_modifier="false" name="condition" src="d0_s0" value="dying" value_original="dying" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s0" to="75" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petiole 1/4–1/3 length of leaf, scaly at least at base;</text>
      <biological_entity id="o16496" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1/4 length of leaf" name="length" src="d0_s1" to="1/3 length of leaf" />
        <character constraint="at base" constraintid="o16497" is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o16497" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>scales scattered, tan.</text>
      <biological_entity id="o16498" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade light green, ovatelanceolate, 2–3-pinnate-pinnatifid, herbaceous, not glandular.</text>
      <biological_entity id="o16499" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-3-pinnate-pinnatifid" value_original="2-3-pinnate-pinnatifid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae ± in plane of blade, lance-oblong;</text>
      <biological_entity id="o16500" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lance-oblong" value_original="lance-oblong" />
      </biological_entity>
      <biological_entity id="o16501" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <relation from="o16500" id="r3624" modifier="in plane" name="part_of" negation="false" src="d0_s4" to="o16501" />
    </statement>
    <statement id="d0_s5">
      <text>basal pinnae lanceolate-deltate, slightly reduced, basal pinnules usually longer than adjacent pinnules, basal basiscopic pinnule longer than basal acroscopic pinnule;</text>
      <biological_entity constraint="basal" id="o16502" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-deltate" value_original="lanceolate-deltate" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16503" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character constraint="than adjacent pinnules" constraintid="o16504" is_modifier="false" name="length_or_size" src="d0_s5" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o16504" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16505" name="pinnule" name_original="pinnules" src="d0_s5" type="structure" />
      <biological_entity id="o16506" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="basiscopic" value_original="basiscopic" />
        <character constraint="than basal acroscopic pinnule" constraintid="o16507" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16507" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pinnule margins serrate, teeth spiny.</text>
      <biological_entity constraint="pinnule" id="o16508" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o16509" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sori midway between midvein and margin of segments.</text>
      <biological_entity id="o16510" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character constraint="between midvein, margin" constraintid="o16511, o16512" is_modifier="false" name="position" src="d0_s7" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity id="o16511" name="midvein" name_original="midvein" src="d0_s7" type="structure" />
      <biological_entity id="o16512" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o16513" name="segment" name_original="segments" src="d0_s7" type="structure" />
      <relation from="o16511" id="r3625" name="part_of" negation="false" src="d0_s7" to="o16513" />
      <relation from="o16512" id="r3626" name="part_of" negation="false" src="d0_s7" to="o16513" />
    </statement>
    <statement id="d0_s8">
      <text>Indusia lacking glands.</text>
      <biological_entity id="o16515" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2n = 164.</text>
      <biological_entity id="o16514" name="indusium" name_original="indusia" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o16516" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="164" value_original="164" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swampy woods, moist wooded slopes, stream banks, and conifer plantations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swampy woods" />
        <character name="habitat" value="moist wooded slopes" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="conifer plantations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Ark., Conn., Del., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., Wash., W.Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12</number>
  <other_name type="common_name">Spinulose wood fern</other_name>
  <other_name type="common_name">toothed wood fern</other_name>
  <other_name type="common_name">dryoptère de cartheuser</other_name>
  <discussion>Dryopteris carthusiana is tetraploid. Dryopteris intermedia is one parent, as indicated by chromosome pairing in their hybrid D. × triploidea Wherry. The other parent is the hypothetical missing ancestral species " D. semicristata " (see discussion for D. cristata). Dryopteris carthusiana hybridizes with five species; hybrids can be separated from D. intermedia by the lack of glandular hairs and by having 2-pinnate leaves.</discussion>
  
</bio:treatment>