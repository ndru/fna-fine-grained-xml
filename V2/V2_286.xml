<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Yatskievych" date="1990" rank="genus">pentagramma</taxon_name>
    <taxon_name authority="(Kaulfuss) Yatskievych" date="1990" rank="species">triangularis</taxon_name>
    <taxon_name authority="(Kaulfuss) Yatskievych" date="unknown" rank="subspecies">triangularis</taxon_name>
    <taxon_hierarchy>family pteridaceae;genus pentagramma;species triangularis;subspecies triangularis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500892</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Petiole glabrous, not viscid-glandular.</text>
      <biological_entity id="o10971" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s0" value="viscid-glandular" value_original="viscid-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blade thin and herbaceous to somewhat thick and leathery, not viscid-glandular, abaxially densely pale to bright-yellow, adaxially glabrous.</text>
      <biological_entity id="o10972" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="somewhat" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="viscid-glandular" value_original="viscid-glandular" />
        <character char_type="range_value" from="abaxially densely pale" name="coloration" src="d0_s1" to="bright-yellow" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Distal pinnae mostly regularly lobed.</text>
      <biological_entity constraint="distal" id="o10973" name="pinna" name_original="pinnae" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly regularly" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Proximal basiscopic lobes of basal pinnae pinnatifid, often deeply so.</text>
      <biological_entity constraint="basal" id="o10975" name="pinna" name_original="pinnae" src="d0_s3" type="structure" />
      <relation from="o10974" id="r2361" name="part_of" negation="false" src="d0_s3" to="o10975" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 60, ca. 90, 120.</text>
      <biological_entity constraint="pinna" id="o10974" name="lobe" name_original="lobes" src="d0_s3" type="structure" constraint_original="proximal pinna; pinna">
        <character is_modifier="true" name="orientation" src="d0_s3" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10976" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="60" value_original="60" />
        <character name="quantity" src="d0_s4" value="90" value_original="90" />
        <character name="quantity" src="d0_s4" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, pine and oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral, pine and oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Oreg., Wash.; Mexico in Baja California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico in Baja California" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2c</number>
  <discussion>We here restrict Pentagramma triangularis subsp. triangularis to plants with yellow farina and glabrous adaxial leaf surfaces occurring throughout a large region in westernmost North America. This subspecies comprises a complex of morphological, cytological, and phytochemical variants, at least some of which may deserve formal taxonomic recognition, following more detailed studies. Plants with yellow farina reported from Arizona, Nevada, and Utah may represent tetraploid hybrids between P. triangularis subsp. triangularis and P. triangularis subsp. maxonii and are not mapped herein.</discussion>
  
</bio:treatment>