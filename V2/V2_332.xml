<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Swartz" date="1806" rank="genus">cheilanthes</taxon_name>
    <taxon_name authority="(Swartz) Swartz" date="1806" rank="species">microphylla</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fil.</publication_title>
      <place_in_publication>127. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus cheilanthes;species microphylla</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500361</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Adiantum</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">microphyllum</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>135. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Adiantum;species microphyllum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems long-creeping, 1–3 mm diam.;</text>
      <biological_entity id="o11765" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="long-creeping" value_original="long-creeping" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales uniformly brown or slightly darker at base, linear-lanceolate, straight to slightly contorted, loosely appressed, persistent.</text>
      <biological_entity id="o11766" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="at base" constraintid="o11767" is_modifier="false" modifier="slightly" name="coloration" src="d0_s1" value="darker" value_original="darker" />
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11767" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually scattered, 8–40 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>vernation noncircinate.</text>
      <biological_entity id="o11768" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole black, rounded adaxially.</text>
      <biological_entity id="o11769" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="black" value_original="black" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate to linear-oblong, 2-pinnate-pinnatifid to 3-pinnate at base, 1.5–6 cm wide;</text>
      <biological_entity id="o11770" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear-oblong 2-pinnate-pinnatifid" />
        <character char_type="range_value" constraint="at base" constraintid="o11771" from="lanceolate" name="shape" src="d0_s5" to="linear-oblong 2-pinnate-pinnatifid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" notes="" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11771" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis rounded adaxially, lacking scales, with dimorphic pubescence, abaxially sparsely hirsute, adaxially covered with tortuous, appressed hairs.</text>
      <biological_entity id="o11772" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11773" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o11774" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="course" src="d0_s6" value="tortuous" value_original="tortuous" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o11773" id="r2542" modifier="adaxially" name="covered with" negation="false" src="d0_s6" to="o11774" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate, dark color of stalk continuing into pinna base, basal pair often slightly larger than adjacent pair, ± equilateral, appearing glabrous or sparsely pubescent adaxially.</text>
      <biological_entity id="o11775" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
        <character constraint="of stalk" constraintid="o11776" is_modifier="false" name="coloration" src="d0_s7" value="dark color" value_original="dark color" />
      </biological_entity>
      <biological_entity id="o11776" name="stalk" name_original="stalk" src="d0_s7" type="structure" />
      <biological_entity constraint="pinna" id="o11777" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o11778" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="often slightly larger than adjacent pair" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="equilateral" value_original="equilateral" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; adaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o11776" id="r2543" name="continuing into" negation="false" src="d0_s7" to="o11777" />
    </statement>
    <statement id="d0_s8">
      <text>Costae black adaxially for most of length;</text>
      <biological_entity id="o11779" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial scales absent.</text>
      <biological_entity constraint="abaxial" id="o11780" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ultimate segments narrowly elliptic to elongate-deltate, not beadlike, the largest 3–7 mm, abaxially and adaxially sparsely hirsute to glabrescent.</text>
      <biological_entity constraint="ultimate" id="o11781" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s10" to="elongate-deltate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="beadlike" value_original="beadlike" />
        <character is_modifier="false" name="size" src="d0_s10" value="largest" value_original="largest" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="adaxially sparsely hirsute" modifier="abaxially" name="pubescence" src="d0_s10" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>False indusia marginal to obscurely inframarginal, somewhat differentiated, 0.1–0.4 mm wide.</text>
      <biological_entity constraint="false" id="o11782" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character constraint="to obscurely inframarginal , somewhat differentiated , 0.1-0.4 mm" is_modifier="false" name="width" src="d0_s11" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori somewhat discontinuous, often concentrated on interrupted lateral lobes.</text>
      <biological_entity id="o11783" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s12" value="discontinuous" value_original="discontinuous" />
        <character constraint="on lateral lobes" constraintid="o11784" is_modifier="false" modifier="often" name="arrangement_or_density" src="d0_s12" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11784" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Sporangia containing 64 spores.</text>
      <biological_entity id="o11786" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="64" value_original="64" />
      </biological_entity>
      <relation from="o11785" id="r2544" name="containing" negation="false" src="d0_s13" to="o11786" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 116.</text>
      <biological_entity id="o11785" name="sporangium" name_original="sporangia" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o11787" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="116" value_original="116" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous rock outcrops and shell mounds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous rock outcrops" />
        <character name="habitat" value="shell mounds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18</number>
  <other_name type="common_name">Southern lip fern</other_name>
  <discussion>In the flora, the primarily Caribbean Cheilanthes microphylla is known from a small number of localities on the Florida peninsula. This restricted distribution, combined with its smaller stems, mostly black costae, and 64-spored sporangia, helps to separate Cheilanthes microphylla from the closely related C. alabamensis.</discussion>
  
</bio:treatment>