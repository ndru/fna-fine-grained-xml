<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="unknown" rank="genus">huperzia</taxon_name>
    <taxon_name authority="(Clute) Kartesz &amp; Gandhi" date="1991" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 201. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus huperzia;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500683</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Michaux forma occidentale Clute" date="unknown" rank="species">lucidulum</taxon_name>
    <place_of_publication>
      <publication_title>Fern Bull.</publication_title>
      <place_in_publication>11: 13. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lycopodium;species lucidulum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">lucidulum</taxon_name>
    <taxon_name authority="(Clute) L. R. Wilson" date="unknown" rank="variety">occidentale</taxon_name>
    <taxon_hierarchy>genus Lycopodium;species lucidulum;variety occidentale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shoots erect, indeterminate, 12–20 cm, becoming long-decumbent, 4–20 cm;</text>
      <biological_entity id="o7852" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="development" src="d0_s0" value="indeterminate" value_original="indeterminate" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="becoming" name="growth_form_or_orientation" src="d0_s0" value="long-decumbent" value_original="long-decumbent" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>leaves in mature portion slightly smaller than in juvenile portion;</text>
      <biological_entity id="o7853" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o7854" name="portion" name_original="portion" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o7853" id="r1638" name="in" negation="false" src="d0_s1" to="o7854" />
    </statement>
    <statement id="d0_s2">
      <text>distinct annual constrictions present;</text>
      <biological_entity id="o7855" name="constriction" name_original="constrictions" src="d0_s2" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>juvenile growth erect.</text>
      <biological_entity id="o7856" name="growth" name_original="growth" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves reflexed (juvenile portion) or spreading to reflexed (mature portion), light green to whitish green, lustrous;</text>
      <biological_entity id="o7857" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="reflexed" />
        <character char_type="range_value" from="light green" name="coloration" src="d0_s4" to="whitish green" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest leaves oblanceolate, broadest at 1/2–3/4 length, 6–10 mm;</text>
      <biological_entity constraint="largest" id="o7858" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="length" src="d0_s5" value="broadest" value_original="broadest" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>smallest leaves narrowly triangular, broadest at base, 4–7 mm;</text>
      <biological_entity constraint="smallest" id="o7859" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character constraint="at base" constraintid="o7860" is_modifier="false" name="width" src="d0_s6" value="broadest" value_original="broadest" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7860" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>margins with small papillae;</text>
      <biological_entity id="o7861" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o7862" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="small" value_original="small" />
      </biological_entity>
      <relation from="o7861" id="r1639" name="with" negation="false" src="d0_s7" to="o7862" />
    </statement>
    <statement id="d0_s8">
      <text>stomates present on both surfaces, numerous (36–80 per 1/2 leaf) on adaxial surface.</text>
      <biological_entity id="o7863" name="stomate" name_original="stomates" src="d0_s8" type="structure" />
      <biological_entity id="o7864" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o7865" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o7863" id="r1640" name="present on both" negation="false" src="d0_s8" to="o7864" />
      <relation from="o7863" id="r1641" name="present on both" negation="false" src="d0_s8" to="o7865" />
    </statement>
    <statement id="d0_s9">
      <text>Gemmiferous branchlets produced in 1 pseudowhorl at end of annual growth;</text>
      <biological_entity id="o7866" name="branchlet" name_original="branchlets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="gemmiferous" value_original="gemmiferous" />
      </biological_entity>
      <biological_entity id="o7867" name="pseudowhorl" name_original="pseudowhorl" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7868" name="end" name_original="end" src="d0_s9" type="structure" />
      <biological_entity id="o7869" name="growth" name_original="growth" src="d0_s9" type="structure">
        <character is_modifier="true" name="duration" src="d0_s9" value="annual" value_original="annual" />
      </biological_entity>
      <relation from="o7866" id="r1642" name="produced in" negation="false" src="d0_s9" to="o7867" />
      <relation from="o7867" id="r1643" name="at" negation="false" src="d0_s9" to="o7868" />
      <relation from="o7868" id="r1644" name="part_of" negation="false" src="d0_s9" to="o7869" />
    </statement>
    <statement id="d0_s10">
      <text>gemmae 4–4.5 × 3.5–4 mm;</text>
      <biological_entity id="o7870" name="gemma" name_original="gemmae" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lateral leaves broadly obtuse, widest above middle, 1.25–1.5 mm wide.</text>
      <biological_entity constraint="lateral" id="o7871" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="width" src="d0_s11" value="widest" value_original="widest" />
        <character constraint="above middle , 1.25-1.5 mm" is_modifier="false" name="width" src="d0_s11" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores 30–38 µm.</text>
      <biological_entity id="o7872" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s12" to="38" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial in shaded conifer forests and swamps, often along streams and in marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded conifer forests" modifier="terrestrial in" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="marshes" modifier="and in" />
        <character name="habitat" value="terrestrial" modifier="often along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1000(–2000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska, Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <discussion>Huperzia occidentalis is similar to the eastern H. lucidula and occupies similar habitats.</discussion>
  
</bio:treatment>