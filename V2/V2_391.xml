<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Michaux ex DeCandolle" date="unknown" rank="family">equisetaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">equisetum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Equisetum</taxon_name>
    <taxon_name authority="Clute" date="1904" rank="species">×ferrissii</taxon_name>
    <place_of_publication>
      <publication_title>Fern Bull.</publication_title>
      <place_in_publication>12: 22. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family equisetaceae;genus equisetum;subgenus equisetum;species ×ferrissii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500612</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Equisetum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">hyemale</taxon_name>
    <taxon_name authority="(Engelmann) C.V. Morton" date="unknown" rank="variety">elatum</taxon_name>
    <taxon_hierarchy>genus Equisetum;species hyemale;variety elatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Equisetum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hyemale</taxon_name>
    <taxon_name authority="A.A. Eaton" date="unknown" rank="variety">intermedium</taxon_name>
    <taxon_hierarchy>genus Equisetum;species hyemale;variety intermedium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial stems having basal part persisting over winter, unbranched, 20–180 cm;</text>
      <biological_entity id="o15186" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character constraint="over winter" is_modifier="false" name="duration" src="d0_s0" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="180" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15187" name="part" name_original="part" src="d0_s0" type="structure" />
      <relation from="o15186" id="r3316" name="having" negation="false" src="d0_s0" to="o15187" />
    </statement>
    <statement id="d0_s1">
      <text>lines of stomates single;</text>
      <biological_entity id="o15188" name="line" name_original="lines" src="d0_s1" type="structure" />
      <biological_entity id="o15189" name="stomate" name_original="stomates" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
      </biological_entity>
      <relation from="o15188" id="r3317" name="consist_of" negation="false" src="d0_s1" to="o15189" />
    </statement>
    <statement id="d0_s2">
      <text>ridges 14–32.</text>
      <biological_entity id="o15190" name="ridge" name_original="ridges" src="d0_s2" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s2" to="32" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths elongate, 7–17 × 3–12 mm, becoming dark-girdled with age;</text>
      <biological_entity id="o15191" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="17" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
        <character constraint="with age" constraintid="o15192" is_modifier="false" modifier="becoming" name="coloration" src="d0_s3" value="dark-girdled" value_original="dark-girdled" />
      </biological_entity>
      <biological_entity id="o15192" name="age" name_original="age" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>teeth 14–32, articulate and promptly shed or persistent.</text>
      <biological_entity id="o15193" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s4" to="32" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="promptly" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cone apex pointed;</text>
      <biological_entity constraint="cone" id="o15194" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spores white, misshapen.</text>
      <biological_entity id="o15195" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Cones maturing in late spring–early summer but spores not shed.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="cones maturing time" char_type="range_value" modifier="but spores not shed" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist lakeshores, riverbanks, roadsides, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist lakeshores" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont., Que., Sask.; Ariz., Ark., Calif., Colo., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Kans., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; n Mexico including Baja California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico including Baja California" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11</number>
  <discussion>The hybrid between Equisetum hyemale and E. laevigatum, E. × ferrissii, was mistaken for E. laevigatum by Schaffner and some subsequent authors. Although sterile, it exists outside the range of E. laevigatum, and apparently it is dispersed vegetatively (R.L. Hauke 1963). Perhaps it has persisted in some areas from a time when the parents were both there. Equisetum × ferrissii has been reported from Maine, New Hampshire, Rhode Island, South Dakota, and Vermont, but I have not seen specimens from those states.</discussion>
  
</bio:treatment>