<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1805" rank="genus">cystopteris</taxon_name>
    <taxon_name authority="Lellinger" date="1981" rank="species">reevesiana</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>71: 92. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus cystopteris;species reevesiana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500464</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cystopteris</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="unknown" rank="species">fragilis</taxon_name>
    <taxon_name authority="Clute" date="unknown" rank="subspecies">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Cystopteris;species fragilis;subspecies tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, not cordlike, internodes usually long, with scattered persistent petiole bases, hairs absent;</text>
      <biological_entity id="o6684" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
      </biological_entity>
      <biological_entity id="o6685" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o6686" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o6687" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o6685" id="r1397" name="with" negation="false" src="d0_s0" to="o6686" />
    </statement>
    <statement id="d0_s1">
      <text>scales tan to brown, ovate to lanceolate, radial walls thin, luminae tan.</text>
      <biological_entity id="o6688" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s1" to="brown" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o6689" name="wall" name_original="walls" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="radial" value_original="radial" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o6690" name="lumina" name_original="luminae" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, clustered at stem apex, to 45 cm, bearing sori throughout year.</text>
      <biological_entity id="o6691" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character constraint="at stem apex" constraintid="o6692" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="45" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o6692" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o6693" name="sorus" name_original="sori" src="d0_s2" type="structure" />
      <biological_entity id="o6694" name="year" name_original="year" src="d0_s2" type="structure" />
      <relation from="o6691" id="r1398" name="bearing" negation="false" src="d0_s2" to="o6693" />
      <relation from="o6691" id="r1399" name="throughout" negation="false" src="d0_s2" to="o6694" />
    </statement>
    <statement id="d0_s3">
      <text>Petiole highly variable in color, from uniformly dark purple to uniformly straw-colored, but mostly dark purple at base, grading to straw-colored at junction with blade, shorter than blade, base sparsely scaly.</text>
      <biological_entity id="o6695" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="highly" name="coloration" src="d0_s3" value="variable" value_original="variable" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="coloration" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
      </biological_entity>
      <biological_entity id="o6700" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="uniformly dark purple" is_modifier="true" name="coloration" src="d0_s3" to="uniformly straw-colored" />
        <character is_modifier="true" modifier="mostly" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="coloration" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o6701" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="uniformly dark purple" is_modifier="true" name="coloration" src="d0_s3" to="uniformly straw-colored" />
        <character is_modifier="true" modifier="mostly" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="coloration" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
        <character constraint="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" constraintid="o6697, o6698" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="from uniformly dark purple to uniformly straw-colored , but mostly dark purple at base , grading to straw-colored at junction with blade , shorter than blade , base" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o6702" name="junction" name_original="junction" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o6703" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o6697" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="uniformly dark purple" is_modifier="true" name="coloration" src="d0_s3" to="uniformly straw-colored" />
        <character is_modifier="true" modifier="mostly" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o6698" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="uniformly dark purple" is_modifier="true" name="coloration" src="d0_s3" to="uniformly straw-colored" />
        <character is_modifier="true" modifier="mostly" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <relation from="o6697" id="r1400" name="from" negation="false" src="d0_s3" to="o6700" />
      <relation from="o6697" id="r1401" name="from" negation="false" src="d0_s3" to="o6701" />
      <relation from="o6698" id="r1402" name="from" negation="false" src="d0_s3" to="o6700" />
      <relation from="o6698" id="r1403" name="from" negation="false" src="d0_s3" to="o6701" />
      <relation from="o6700" id="r1404" name="at" negation="false" src="d0_s3" to="o6702" />
      <relation from="o6701" id="r1405" name="at" negation="false" src="d0_s3" to="o6702" />
      <relation from="o6700" id="r1406" name="with" negation="false" src="d0_s3" to="o6703" />
      <relation from="o6701" id="r1407" name="with" negation="false" src="d0_s3" to="o6703" />
    </statement>
    <statement id="d0_s4">
      <text>Blade ovate to elliptic, 2–3-pinnate, widest at or just below middle, apex short-attenuate;</text>
      <biological_entity id="o6704" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character constraint="at middle" constraintid="o6705" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o6705" name="middle" name_original="middle" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="just below" name="position" src="d0_s4" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o6706" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-attenuate" value_original="short-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis and costae lacking gland-tipped hairs or bulblets;</text>
      <biological_entity id="o6707" name="rachis" name_original="rachis" src="d0_s5" type="structure" />
      <biological_entity id="o6708" name="costa" name_original="costae" src="d0_s5" type="structure" />
      <biological_entity id="o6709" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o6710" name="bulblet" name_original="bulblets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axils of pinnae with occasional multicellular, gland-tipped hairs.</text>
      <biological_entity id="o6711" name="axil" name_original="axils" src="d0_s6" type="structure" constraint="pinna" constraint_original="pinna; pinna" />
      <biological_entity id="o6712" name="pinna" name_original="pinnae" src="d0_s6" type="structure" />
      <biological_entity id="o6713" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="occasional" value_original="occasional" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o6711" id="r1408" name="part_of" negation="false" src="d0_s6" to="o6712" />
      <relation from="o6711" id="r1409" name="with" negation="false" src="d0_s6" to="o6713" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae usually perpendicular to rachis, not curving toward blade apex, margins dentate to crenate;</text>
      <biological_entity id="o6714" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character constraint="to rachis" constraintid="o6715" is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
        <character constraint="toward blade apex" constraintid="o6716" is_modifier="false" modifier="not" name="course" notes="" src="d0_s7" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o6715" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o6716" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o6717" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s7" to="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal pinnae pinnate-pinnatifid to 2-pinnate, ± equilateral, basiscopic pinnules not enlarged;</text>
      <biological_entity constraint="proximal" id="o6718" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnate-pinnatifid to 2-pinnate" value_original="pinnate-pinnatifid to 2-pinnate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s8" value="equilateral" value_original="equilateral" />
      </biological_entity>
      <biological_entity id="o6719" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal basiscopic pinnules mostly short-stalked, base truncate to obtuse, distal pinnae deltate to ovate.</text>
      <biological_entity constraint="basal" id="o6720" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o6721" name="pinnule" name_original="pinnules" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s9" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
      <biological_entity id="o6722" name="base" name_original="base" src="d0_s9" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s9" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6723" name="pinna" name_original="pinnae" src="d0_s9" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins directed into teeth and notches.</text>
      <biological_entity id="o6724" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character constraint="into notches" constraintid="o6726" is_modifier="false" name="orientation" src="d0_s10" value="directed" value_original="directed" />
      </biological_entity>
      <biological_entity id="o6725" name="tooth" name_original="teeth" src="d0_s10" type="structure" />
      <biological_entity id="o6726" name="notch" name_original="notches" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Indusia cupshaped to lanceolate, gland-tipped hairs absent.</text>
      <biological_entity id="o6727" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character char_type="range_value" from="cupshaped" name="shape" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o6728" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores spiny, usually averaging 33–41 µm. 2n = 84.</text>
      <biological_entity id="o6729" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6730" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial or on rock on variety of substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="terrestrial or on" constraint="on variety of substrates" />
        <character name="habitat" value="variety" constraint="of substrates" />
        <character name="habitat" value="substrates" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex., Utah; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Southwestern brittle fern</other_name>
  <discussion>The finely dissected leaves, dark petioles, creeping stems, smaller spores, and terrestrial habit distinguish Cystopteris reevesiana from C. fragilis in the southwest. On rock and at high elevations, however, C. reevesiana can have stems with short internodes and leaves that are reduced in size and dissection (resembling C. fragilis). In southern Colorado, the two species are sympatric in some areas and form triploid hybrids. Cystopteris reevesiana and C. bulbifera are the diploid progenitors of C. utahensis, which occasionally crosses with C. reevesiana to produce sterile triploid hybrids of intermediate morphology.</discussion>
  
</bio:treatment>