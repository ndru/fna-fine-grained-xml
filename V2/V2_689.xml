<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="Fée" date="1850" rank="genus">neurodium</taxon_name>
    <taxon_name authority="(Linnaeus) Fée" date="1852" rank="species">lanceolatum</taxon_name>
    <place_of_publication>
      <place_in_publication>3: 28. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus neurodium;species lanceolatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500800</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pteris</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">lanceolata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1073. 1753 Paltonium lanceolatum (Linnaeus) C. Presl</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pteris;species lanceolata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots brown, in dense mass often covering stems.</text>
      <biological_entity id="o17079" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o17080" name="mass" name_original="mass" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o17081" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <relation from="o17079" id="r3746" name="in" negation="false" src="d0_s0" to="o17080" />
      <relation from="o17080" id="r3747" modifier="often" name="covering" negation="false" src="d0_s0" to="o17081" />
    </statement>
    <statement id="d0_s1">
      <text>Stem scales black to dark-brown, triangular-acuminate to lanceolate-acuminate, clathrate, papillate-ciliolate.</text>
      <biological_entity constraint="stem" id="o17082" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s1" to="dark-brown" />
        <character char_type="range_value" from="triangular-acuminate" name="shape" src="d0_s1" to="lanceolate-acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="clathrate" value_original="clathrate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="papillate-ciliolate" value_original="papillate-ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-arching, 1–4.5 dm.</text>
      <biological_entity id="o17083" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-arching" value_original="erect-arching" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="4.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole less than 3.5 cm.</text>
      <biological_entity id="o17084" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blade 20 × 1.3 cm, leathery;</text>
      <biological_entity id="o17085" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="20" value_original="20" />
        <character name="width" src="d0_s4" unit="cm" value="1.3" value_original="1.3" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base narrowly cuneate to attenuate;</text>
      <biological_entity id="o17086" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly cuneate" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute to rounded.</text>
      <biological_entity id="o17087" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sori to 6 cm, ending ca. 0.5 cm below apex of blade.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 74.</text>
      <biological_entity id="o17088" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17089" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="74" value_original="74" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic in hammocks and mangrove swamps, substrate subacidic to circumneutral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="mangrove" />
        <character name="habitat" value="epiphytic in hammocks" />
        <character name="habitat" value="mangrove swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Ribbon fern</other_name>
  
</bio:treatment>