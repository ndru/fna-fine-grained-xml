<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hooker" date="unknown" rank="family">parkeriaceae</taxon_name>
    <taxon_name authority="Brongniart" date="1821" rank="genus">ceratopteris</taxon_name>
    <taxon_name authority="(Hooker) Hieronymus" date="1905" rank="species">pteridoides</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>34: 561. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family parkeriaceae;genus ceratopteris;species pteridoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200003557</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parkeria</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">pteridoides</taxon_name>
    <place_of_publication>
      <publication_title>Exot. Fl.</publication_title>
      <place_in_publication>2: plate 147. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Parkeria;species pteridoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ceratopteris</taxon_name>
    <taxon_name authority="(Hooker &amp; Greville) Kunze" date="unknown" rank="species">lockhartii</taxon_name>
    <taxon_hierarchy>genus Ceratopteris;species lockhartii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants floating or rooted.</text>
      <biological_entity id="o16409" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_location" src="d0_s0" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rooted" value_original="rooted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sterile leaves deltate to cordate to ovate.</text>
      <biological_entity id="o16410" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s1" to="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole of sterile leaf 1–19 cm, usually inflated, in some near base, but in most inflated nearer blades.</text>
      <biological_entity id="o16411" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="19" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o16412" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o16413" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o16414" name="nearer" name_original="nearer" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o16415" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o16411" id="r3598" name="part_of" negation="false" src="d0_s2" to="o16412" />
      <relation from="o16411" id="r3599" name="in" negation="false" src="d0_s2" to="o16413" />
      <relation from="o16411" id="r3600" name="in" negation="false" src="d0_s2" to="o16414" />
      <relation from="o16411" id="r3601" name="in" negation="false" src="d0_s2" to="o16415" />
    </statement>
    <statement id="d0_s3">
      <text>Blade of sterile leaf 2–4-pinnate, 5–33 × 4–29 cm, simple and palmately 3-lobed (ternate), or pinnately 5-lobed or pinnate near base;</text>
      <biological_entity id="o16416" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="2-4-pinnate" value_original="2-4-pinnate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="33" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s3" to="29" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s3" value="5-lobed" value_original="5-lobed" />
        <character constraint="near base" constraintid="o16418" is_modifier="false" name="shape" src="d0_s3" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o16417" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o16418" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o16416" id="r3602" name="part_of" negation="false" src="d0_s3" to="o16417" />
    </statement>
    <statement id="d0_s4">
      <text>proximal pinnae or veins of lobes usually opposite.</text>
      <biological_entity constraint="lobe" id="o16419" name="pinna" name_original="pinnae" src="d0_s4" type="structure" constraint_original="lobe proximal; lobe">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16420" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o16421" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <relation from="o16419" id="r3603" name="part_of" negation="false" src="d0_s4" to="o16421" />
      <relation from="o16420" id="r3604" name="part_of" negation="false" src="d0_s4" to="o16421" />
    </statement>
    <statement id="d0_s5">
      <text>Fertile leaves deltate to cordate to reniform, 9–50 × 8–36 (–50) cm.</text>
      <biological_entity id="o16422" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s5" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s5" to="cordate" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s5" to="50" to_unit="cm" />
        <character char_type="range_value" from="36" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="50" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s5" to="36" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Petiole of fertile leaf 4–25 cm.</text>
      <biological_entity id="o16423" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16424" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o16423" id="r3605" name="part_of" negation="false" src="d0_s6" to="o16424" />
    </statement>
    <statement id="d0_s7">
      <text>Blade of fertile leaf 1–4-pinnate;</text>
      <biological_entity id="o16425" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="1-4-pinnate" value_original="1-4-pinnate" />
      </biological_entity>
      <biological_entity id="o16426" name="leaf" name_original="leaf" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o16425" id="r3606" name="part_of" negation="false" src="d0_s7" to="o16426" />
    </statement>
    <statement id="d0_s8">
      <text>terminal segments narrow, linear.</text>
      <biological_entity constraint="terminal" id="o16427" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sporangia usually crowded between segment midvein and revolute margin, with 0–10 (–40) indurate annulus cells.</text>
      <biological_entity id="o16428" name="sporangium" name_original="sporangia" src="d0_s9" type="structure">
        <character constraint="between segment midvein, margin" constraintid="o16429, o16430" is_modifier="false" modifier="usually" name="arrangement" src="d0_s9" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity constraint="segment" id="o16429" name="midvein" name_original="midvein" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="segment" id="o16430" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="annulus" id="o16431" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s9" to="40" />
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s9" to="10" />
        <character is_modifier="true" name="texture" src="d0_s9" value="indurate" value_original="indurate" />
      </biological_entity>
      <relation from="o16428" id="r3607" name="with" negation="false" src="d0_s9" to="o16431" />
    </statement>
    <statement id="d0_s10">
      <text>Spores 32 per sporangium, 70–100 µm diam. 2n = 78.</text>
      <biological_entity id="o16432" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character constraint="per sporangium" constraintid="o16433" name="quantity" src="d0_s10" value="32" value_original="32" />
        <character char_type="range_value" from="70" from_unit="um" name="diameter" notes="" src="d0_s10" to="100" to_unit="um" />
      </biological_entity>
      <biological_entity id="o16433" name="sporangium" name_original="sporangium" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o16434" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="78" value_original="78" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Aquatic to semiaquatic, in swamps, bogs, canals, ponds, lakes, ditches, marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="aquatic to semiaquatic" constraint="in swamps" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="canals" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–25 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="25" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La.; West Indies; Central America; South America; se Asia in Vietnam.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="se Asia in Vietnam" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <discussion>Ceratopteris pteridoides is usually easily recognized by its sterile leaf morphology, which varies considerably with habitat. Leaves intermediate between sterile and fertile are fairly common, with various degrees of laminar development of the fertile segments. Some fertile leaves have quite broad segments with rows of sporangia along the margins only. Ceratopteris pteridoides is sexual and diploid and is incompletely reproductively isolated from the diploid C. richardii. Hybrids synthesized by L. G. Hickok (1977) result in 40% viable spores.</discussion>
  
</bio:treatment>