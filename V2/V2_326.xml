<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">aspleniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">asplenium</taxon_name>
    <taxon_name authority="D. C. Eaton" date="1873" rank="species">bradleyi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>4: 11. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aspleniaceae;genus asplenium;species bradleyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200004104</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asplenium</taxon_name>
    <taxon_name authority="Wherry" date="unknown" rank="species">stotleri</taxon_name>
    <taxon_hierarchy>genus Asplenium;species stotleri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots not proliferous.</text>
      <biological_entity id="o3034" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="proliferous" value_original="proliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short-creeping to ascending, occasionally branched;</text>
      <biological_entity id="o3035" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="short-creeping" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales dark reddish to brown throughout, narrowly deltate, (2–) 3–5 × 0.2–0.4 mm, margins entire or shallowly dentate.</text>
      <biological_entity id="o3036" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark reddish" modifier="throughout" name="coloration" src="d0_s2" to="brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s2" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3037" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic.</text>
      <biological_entity id="o3038" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole reddish or purplish brown throughout, lustrous, 1–10 (–13) cm, 1/3–3/4 length of blade;</text>
      <biological_entity id="o3039" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s4" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="lustrous" value_original="lustrous" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1/3 length of blade" name="length" src="d0_s4" to="3/4 length of blade" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>indument of brown, narrowly lanceolate scales at base, grading into hairs.</text>
      <biological_entity id="o3040" name="indument" name_original="indument" src="d0_s5" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity id="o3041" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o3042" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o3043" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o3040" id="r649" name="part_of" negation="false" src="d0_s5" to="o3041" />
      <relation from="o3040" id="r650" name="at" negation="false" src="d0_s5" to="o3042" />
      <relation from="o3040" id="r651" name="into" negation="false" src="d0_s5" to="o3043" />
    </statement>
    <statement id="d0_s6">
      <text>Blade narrowly oblong to lanceolate, pinnate-pinnatifid to 2-pinnate, 2–17 (–20) × 1–6 cm, thin to moderately thick, sparsely pubescent;</text>
      <biological_entity id="o3044" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s6" to="lanceolate pinnate-pinnatifid" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s6" to="lanceolate pinnate-pinnatifid" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="17" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="thin" name="width" src="d0_s6" to="moderately thick" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base truncate or obtuse;</text>
      <biological_entity id="o3045" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute, not rooting.</text>
      <biological_entity id="o3046" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Rachis reddish or purplish brown proximally, fading to green in distal 1/3–2/3, lustrous, sparsely pubescent.</text>
      <biological_entity id="o3047" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in distal 1/3-2/3" constraintid="o3048" from="fading" name="coloration" src="d0_s9" to="green" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s9" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3048" name="1/3-2/3" name_original="1/3-2/3" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pinnae in (3–) 5–15 (–25) pairs, ovate, obovate to lanceolate or deltate-lanceolate;</text>
      <biological_entity id="o3049" name="pinna" name_original="pinnae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="lanceolate or deltate-lanceolate" />
      </biological_entity>
      <biological_entity id="o3050" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="25" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="15" />
      </biological_entity>
      <relation from="o3049" id="r652" name="in" negation="false" src="d0_s10" to="o3050" />
    </statement>
    <statement id="d0_s11">
      <text>medial pinnae 6–40 × 3–10 mm;</text>
      <biological_entity constraint="medial" id="o3051" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>base truncate to obliquely obtuse;</text>
      <biological_entity id="o3052" name="base" name_original="base" src="d0_s12" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s12" to="obliquely obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>margins dentate to denticulate;</text>
      <biological_entity id="o3053" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s13" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apex acute or rounded.</text>
      <biological_entity id="o3054" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Veins free, barely evident.</text>
      <biological_entity id="o3055" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" modifier="barely" name="prominence" src="d0_s15" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Sori 3 to numerous pairs per pinna, on both basiscopic and acroscopic sides.</text>
      <biological_entity id="o3056" name="sorus" name_original="sori" src="d0_s16" type="structure">
        <character constraint="to pairs" constraintid="o3057" name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o3057" name="pair" name_original="pairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o3058" name="pinna" name_original="pinna" src="d0_s16" type="structure" />
      <biological_entity id="o3059" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s16" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o3057" id="r653" name="per" negation="false" src="d0_s16" to="o3058" />
      <relation from="o3056" id="r654" name="on" negation="false" src="d0_s16" to="o3059" />
    </statement>
    <statement id="d0_s17">
      <text>Spores 64 per sporangium.</text>
      <biological_entity id="o3061" name="sporangium" name_original="sporangium" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 144.</text>
      <biological_entity id="o3060" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character constraint="per sporangium" constraintid="o3061" name="quantity" src="d0_s17" value="64" value_original="64" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3062" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="144" value_original="144" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic rocks, usually on steep ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic rocks" />
        <character name="habitat" value="steep ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ill., Ky., Md., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24</number>
  <other_name type="common_name">Bradley's spleenwort</other_name>
  <discussion>Asplenium bradleyi is a morphologically variable species, the allotetraploid derivative of A. montanum × platyneuron (W. H. Wagner Jr. 1954; D. M. Smith and D. A. Levin 1963; C. R. Werth et al. 1985). The sterile diploid form of A. bradleyi has been collected twice in nature (W. H. Wagner Jr. et al. 1973; A. M. Evans 1988), and isozyme studies indicate that the allotetraploid has had a polytopic origin (C. R. Werth et al. 1985b). Occurring rarely to locally in the Appalachian region, A. bradleyi overlaps with both progenitor taxa, but it is fairly frequent in the Ozark and Ouachita region where A. montanum is absent. Sterile hybrids with A. pinnatifidum (A. × gravesii), A. montanum (A. × wherryi D. M. Smith et al.), and A. platyneuron are known from nature.</discussion>
  
</bio:treatment>