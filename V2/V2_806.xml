<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1805" rank="genus">cystopteris</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="1805" rank="species">fragilis</taxon_name>
    <place_of_publication>
      <publication_title>Neues J. Bot.</publication_title>
      <place_in_publication>1(2): 27, plate 2, fig. 9. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus cystopteris;species fragilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">200003869</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">fragile</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1091. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species fragile;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cystopteris</taxon_name>
    <taxon_name authority="Sim" date="unknown" rank="species">dickieana</taxon_name>
    <taxon_hierarchy>genus Cystopteris;species dickieana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cystopteris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fragilis</taxon_name>
    <taxon_name authority="(Sim) Hylander" date="unknown" rank="subspecies">dickieana</taxon_name>
    <taxon_hierarchy>genus Cystopteris;species fragilis;subspecies dickieana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, not cordlike, internodes short, beset with old petiole bases, hairs absent;</text>
      <biological_entity id="o4841" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
      </biological_entity>
      <biological_entity id="o4842" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o4843" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o4844" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o4842" id="r1049" name="beset with" negation="false" src="d0_s0" to="o4843" />
    </statement>
    <statement id="d0_s1">
      <text>scales tan to light-brown, lanceolate, radial walls thin, luminae tan.</text>
      <biological_entity id="o4845" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s1" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o4846" name="wall" name_original="walls" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="radial" value_original="radial" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o4847" name="lumina" name_original="luminae" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, clustered at stem apex, to 40 cm, nearly all bearing sori.</text>
      <biological_entity id="o4848" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character constraint="at stem apex" constraintid="o4849" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o4849" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o4850" name="sorus" name_original="sori" src="d0_s2" type="structure" />
      <relation from="o4848" id="r1050" modifier="nearly" name="bearing" negation="false" src="d0_s2" to="o4850" />
    </statement>
    <statement id="d0_s3">
      <text>Petiole dark at base, mostly green to straw-colored distally, shorter than or nearly equaling blade, base sparsely scaly.</text>
      <biological_entity id="o4851" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o4852" is_modifier="false" name="coloration" src="d0_s3" value="dark" value_original="dark" />
        <character char_type="range_value" from="mostly green" modifier="distally" name="coloration" notes="" src="d0_s3" to="straw-colored" />
        <character constraint="than or nearly equaling blade" constraintid="o4853" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4852" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o4853" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o4854" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blade lanceolate to narrowly elliptic, 1 (–2) -pinnate-pinnatifid, widest at or just below middle, apex acute;</text>
      <biological_entity id="o4855" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="1(-2)-pinnate-pinnatifid" value_original="1(-2)-pinnate-pinnatifid" />
        <character constraint="at middle" constraintid="o4856" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o4856" name="middle" name_original="middle" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="just below" name="position" src="d0_s4" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o4857" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis and costae lacking gland-tipped hairs or bulblets;</text>
      <biological_entity id="o4858" name="rachis" name_original="rachis" src="d0_s5" type="structure" />
      <biological_entity id="o4859" name="costa" name_original="costae" src="d0_s5" type="structure" />
      <biological_entity id="o4860" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o4861" name="bulblet" name_original="bulblets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axils of pinnae lacking multicellular, gland-tipped hairs.</text>
      <biological_entity id="o4862" name="axil" name_original="axils" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o4863" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o4864" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o4862" id="r1051" name="consist_of" negation="false" src="d0_s6" to="o4863" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae usually perpendicular to rachis, not curving toward blade apex, margins serrate to sharply dentate;</text>
      <biological_entity id="o4865" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character constraint="to rachis" constraintid="o4866" is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
        <character constraint="toward blade apex" constraintid="o4867" is_modifier="false" modifier="not" name="course" notes="" src="d0_s7" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o4866" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o4867" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o4868" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s7" to="sharply dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal pinnae pinnatifid to pinnate-pinnatifid, ± equilateral, basiscopic pinnules not enlarged;</text>
      <biological_entity constraint="proximal" id="o4869" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s8" to="pinnate-pinnatifid" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s8" value="equilateral" value_original="equilateral" />
      </biological_entity>
      <biological_entity id="o4870" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal basiscopic pinnules sessile, base truncate to obtuse, distal pinnae deltate to ovate.</text>
      <biological_entity constraint="basal" id="o4871" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o4872" name="pinnule" name_original="pinnules" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o4873" name="base" name_original="base" src="d0_s9" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s9" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4874" name="pinna" name_original="pinnae" src="d0_s9" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s9" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins directed mostly into teeth.</text>
      <biological_entity id="o4875" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character constraint="into teeth" constraintid="o4876" is_modifier="false" name="orientation" src="d0_s10" value="directed" value_original="directed" />
      </biological_entity>
      <biological_entity id="o4876" name="tooth" name_original="teeth" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Indusia ovate to lanceolate, without gland-tipped hairs.</text>
      <biological_entity id="o4877" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o4878" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o4877" id="r1052" name="without" negation="false" src="d0_s11" to="o4878" />
    </statement>
    <statement id="d0_s12">
      <text>Spores spiny or verrucate, usually 39–60 µm. 2n = 168, 252.</text>
      <biological_entity id="o4879" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="relief" src="d0_s12" value="verrucate" value_original="verrucate" />
        <character char_type="range_value" from="39" from_unit="um" modifier="usually" name="some_measurement" src="d0_s12" to="60" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4880" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="168" value_original="168" />
        <character name="quantity" src="d0_s12" value="252" value_original="252" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly on cliff faces, also in thin soil over rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliff" modifier="mostly on" />
        <character name="habitat" value="thin soil" modifier="faces also in" constraint="over rock" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–4500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Mont., Nebr., Nev., N.H., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., S.Dak., Tex., Utah, Vt., Wash., Wis., Wyo.; worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Brittle fern</other_name>
  <other_name type="common_name">fragile fern</other_name>
  <other_name type="common_name">cystoptère fragile</other_name>
  <discussion>Cystopteris fragilis is most often confused with C. tenuis in the east and C. reevesiana in the southwest. Habitat and geography, as well as the morphologic features discussed in the key, usually serve to separate these taxa. For instance, C. fragilis is more likely to be found on cliffs whereas the other species prefer boulders and soil (C. fragilis occurs at higher elevations and/or latitudes than the other species). These distinctions can be confounded when C. fragilis forms hybrids with sympatric species. Sterile pentaploid plants have been discovered where C. fragilis overlaps with C. laurentiana, tetraploid hybrids are likely where C. fragilis occurs with C. tenuis, and triploids may form where C. fragilis is found with C. reevesiana. Even after segregating relatively distinct elements such as Cystopteris protrusa, C. reevesiana, and C. tenuis, and identifying sterile hybrids, C. fragilis still remains a polymorphic and complex taxon that probably contains a number of natural, cryptic evolutionary units. For example, morphologically distinct hexaploid cytotypes have been reported (C. H. Haufler and M. D. Windham 1991). These occur as isolated and disjunct populations in Ontario, Alaska, Arizona, Colorado, Montana, and Wyoming. Isozymic profiles of each of these populations indicate that the hexaploids are polyphyletic and should not be accorded species status.</discussion>
  <discussion>The presence of verrucate spores (as opposed to the normal spiny spores) has been used to circumscribe Cystopteris dickieana. Although genetic analyses have not been undertaken, we think the verrucate spore is probably a recessive feature controlled by one or a few genes. While present at low frequency in much of the range of C. fragilis, verrucate spores are particularly prominent in the Great Plains. Perhaps in this region the genetic combinations specifying verrucate spores have been fixed. Following R. F. Blasdell (1963), C. dickieana is also considered here to be conspecific with C. fragilis because (1) early stages in the development of spiny spores can appear verrucate (A. C. Jermy and L. Harper 1971), (2) the hexaploid cytotypes discussed above always have verrucate spores, regardless of their parentage, (3) individuals with verrucate spores can be found in populations that are otherwise uniformly spiny-spored, and (4) individuals and populations that have verrucate spores are not otherwise (morphologically, ecologically, or genetically) distinct from those that have spiny spores.</discussion>
  <discussion>Especially in the western portion of its North American range (British Columbia, Washington, Montana, Idaho, Oregon, California), Cystopteris fragilis appears to be developing morphologically and ecologically distinctive variants. Hybrid individuals with aborted spores have been discovered, and plants from these areas increasingly tend to grow on both soil and rock and to have slightly different morphologies on the two substrates. These variants intergrade, however, and are not sufficiently distinct to warrant species status. This polymorphic polyploid is probably actively speciating at the tetraploid level, perhaps through gene silencing (C. R. Werth and M. D. Windham 1991).</discussion>
  
</bio:treatment>