<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">dryopteris</taxon_name>
    <taxon_name authority="(Cavanilles) C. Christensen" date="1911" rank="species">cinnamomea</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>1: 95. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus dryopteris;species cinnamomea</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500593</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tectaria</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">cinnamomea</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Pl.,</publication_title>
      <place_in_publication>252. 1802</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tectaria;species cinnamomea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves monomorphic, green through winter, 22–50 × 6–12 cm.</text>
      <biological_entity id="o15648" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="monomorphic" value_original="monomorphic" />
        <character constraint="through winter" is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="22" from_unit="cm" name="length" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s0" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petiole 1/4 length of leaf, scaly at least at base;</text>
      <biological_entity id="o15649" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character name="length" src="d0_s1" value="1/4 length of leaf" value_original="1/4 length of leaf" />
        <character constraint="at base" constraintid="o15650" is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o15650" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>scales scattered, cinnamon-colored.</text>
      <biological_entity id="o15651" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="cinnamon-colored" value_original="cinnamon-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade light green, deltate-ovate, 3-pinnate-pinnatifid, herbaceous, not or sparsely glandular.</text>
      <biological_entity id="o15652" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-pinnate-pinnatifid" value_original="3-pinnate-pinnatifid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="not; sparsely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae in plane of blade, narrowly deltate-lanceolate to deltate-oblong, narrowed to elongate, serrate tip;</text>
      <biological_entity id="o15653" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly deltate-lanceolate" name="shape" src="d0_s4" to="deltate-oblong narrowed" />
        <character char_type="range_value" from="narrowly deltate-lanceolate" name="shape" src="d0_s4" to="deltate-oblong narrowed" />
      </biological_entity>
      <biological_entity id="o15654" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o15655" name="tip" name_original="tip" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <relation from="o15653" id="r3411" modifier="in plane" name="part_of" negation="false" src="d0_s4" to="o15654" />
    </statement>
    <statement id="d0_s5">
      <text>basal pinnae deltate-oblong, somewhat reduced, basal pinnules shorter than adjacent pinnules, basal basiscopic pinnule longer than basal acroscopic pinnule;</text>
      <biological_entity constraint="basal" id="o15656" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate-oblong" value_original="deltate-oblong" />
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15657" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character constraint="than adjacent pinnules" constraintid="o15658" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15658" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15659" name="pinnule" name_original="pinnules" src="d0_s5" type="structure" />
      <biological_entity id="o15660" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="basiscopic" value_original="basiscopic" />
        <character constraint="than basal acroscopic pinnule" constraintid="o15661" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15661" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pinnule margins serrate.</text>
      <biological_entity constraint="pinnule" id="o15662" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sori near sinus.</text>
      <biological_entity id="o15663" name="sorus" name_original="sori" src="d0_s7" type="structure" />
      <biological_entity id="o15664" name="sinus" name_original="sinus" src="d0_s7" type="structure" />
      <relation from="o15663" id="r3412" name="near" negation="false" src="d0_s7" to="o15664" />
    </statement>
    <statement id="d0_s8">
      <text>Indusia lacking glands.</text>
      <biological_entity id="o15665" name="indusium" name_original="indusia" src="d0_s8" type="structure" />
      <biological_entity id="o15666" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <other_name type="common_name">Cinnamon wood fern</other_name>
  <discussion>Dryopteris cinnamomea belongs to the D. patula complex of Mexico and Central America, which is poorly understood. Arizona material of D. cinnamomea has been misidentified as D. patula, according to J. T. Mickel and J. M. Beitel (1988).</discussion>
  
</bio:treatment>