<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">woodsia</taxon_name>
    <taxon_name authority="Windham" date="1993" rank="species">phillipsii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Univ. Michigan Herb.</publication_title>
      <place_in_publication>19: 50. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus woodsia;species phillipsii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501352</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact to short-creeping, erect to horizontal, with few-to-many persistent petiole bases of unequal lengths;</text>
      <biological_entity id="o3084" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="horizontal" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o3085" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few-to-many" value_original="few-to-many" />
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o3084" id="r661" modifier="of unequal lengths" name="with" negation="false" src="d0_s0" to="o3085" />
    </statement>
    <statement id="d0_s1">
      <text>scales mostly uniformly brown but at least some bicolored with dark central stripe and pale-brown margins, narrowly lanceolate.</text>
      <biological_entity id="o3086" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="with margins" constraintid="o3088" is_modifier="false" modifier="at-least" name="coloration" src="d0_s1" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="central" id="o3087" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o3088" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 5–35 × 1.5–6 cm.</text>
      <biological_entity id="o3089" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole light-brown or straw-colored when mature, occasionally darker at very base, not articulate above base, relatively brittle and easily shattered.</text>
      <biological_entity id="o3090" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s3" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s3" value="straw-colored" value_original="straw-colored" />
        <character constraint="at base" constraintid="o3091" is_modifier="false" modifier="occasionally" name="coloration" src="d0_s3" value="darker" value_original="darker" />
        <character constraint="above base" constraintid="o3092" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s3" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="relatively; easily" name="fragility" notes="" src="d0_s3" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity id="o3091" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o3092" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Blade lanceolate, usually 2-pinnate proximally, sparsely to moderately glandular, never viscid;</text>
      <biological_entity id="o3093" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="usually; proximally" name="architecture_or_shape" src="d0_s4" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="never" name="coating" src="d0_s4" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>glandular-hairs with thin stalks and slightly expanded tips;</text>
      <biological_entity id="o3094" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure" />
      <biological_entity id="o3095" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o3096" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
      <relation from="o3094" id="r662" name="with" negation="false" src="d0_s5" to="o3095" />
      <relation from="o3094" id="r663" name="with" negation="false" src="d0_s5" to="o3096" />
    </statement>
    <statement id="d0_s6">
      <text>rachis with scattered glandular-hairs and hairlike scales.</text>
      <biological_entity id="o3097" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o3098" name="glandular-hair" name_original="glandular-hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o3099" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o3097" id="r664" name="with" negation="false" src="d0_s6" to="o3098" />
      <relation from="o3097" id="r665" name="with" negation="false" src="d0_s6" to="o3099" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae elongate-deltate to elliptic, longer than wide, often attenuate to a narrowly acute apex;</text>
      <biological_entity id="o3100" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="elongate-deltate" name="shape" src="d0_s7" to="elliptic" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer than wide" value_original="longer than wide" />
        <character constraint="to apex" constraintid="o3101" is_modifier="false" modifier="often" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o3101" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>largest pinnae with 7–18 pairs of widely spaced pinnules;</text>
      <biological_entity constraint="largest" id="o3102" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o3103" name="pair" name_original="pairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
      <biological_entity id="o3104" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="widely" name="arrangement" src="d0_s8" value="spaced" value_original="spaced" />
      </biological_entity>
      <relation from="o3102" id="r666" name="with" negation="false" src="d0_s8" to="o3103" />
      <relation from="o3103" id="r667" name="part_of" negation="false" src="d0_s8" to="o3104" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial and adaxial surfaces somewhat glandular, lacking nonglandular hairs or scales.</text>
      <biological_entity constraint="abaxial and adaxial" id="o3105" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o3106" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o3107" name="scale" name_original="scales" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pinnules dentate, often shallowly lobed;</text>
      <biological_entity id="o3108" name="pinnule" name_original="pinnules" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="often shallowly" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>margins often lustrous adaxially, somewhat thickened, with occasional glands, appearing ciliate due to presence of multicellular translucent projections on teeth that often prolonged to form twisted filaments.</text>
      <biological_entity id="o3109" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often; adaxially" name="reflectance" src="d0_s11" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o3110" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="occasional" value_original="occasional" />
      </biological_entity>
      <biological_entity id="o3111" name="projection" name_original="projections" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o3112" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often" name="due-to-presence" src="d0_s11" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o3113" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
      </biological_entity>
      <relation from="o3109" id="r668" name="with" negation="false" src="d0_s11" to="o3110" />
      <relation from="o3111" id="r669" name="on" negation="false" src="d0_s11" to="o3112" />
    </statement>
    <statement id="d0_s12">
      <text>Vein tips usually enlarged to form whitish hydathodes visible adaxially.</text>
      <biological_entity constraint="vein" id="o3114" name="tip" name_original="tips" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o3115" name="hydathode" name_original="hydathodes" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="adaxially" name="prominence" src="d0_s12" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Indusia of narrow, filamentous segments, these uniseriate for most of length, composed of ± isodiametric cells, often greatly surpassing mature sporangia.</text>
      <biological_entity id="o3116" name="indusium" name_original="indusia" src="d0_s13" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s13" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
      <biological_entity id="o3117" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s13" value="filamentous" value_original="filamentous" />
      </biological_entity>
      <biological_entity id="o3118" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s13" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o3119" name="sporangium" name_original="sporangia" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o3116" id="r670" name="part_of" negation="false" src="d0_s13" to="o3117" />
      <relation from="o3116" id="r671" name="composed of" negation="false" src="d0_s13" to="o3118" />
      <relation from="o3116" id="r672" modifier="often greatly" name="surpassing" negation="false" src="d0_s13" to="o3119" />
    </statement>
    <statement id="d0_s14">
      <text>Spores averaging 37–44 µm. 2n = 76.</text>
      <biological_entity id="o3120" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="37" from_unit="um" name="some_measurement" src="d0_s14" to="44" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3121" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and rocky slopes, usually on granitic or volcanic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="granitic" />
        <character name="habitat" value="volcanic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <other_name type="common_name">Phillips's cliff fern</other_name>
  <discussion>Woodsia phillipsii traditionally has been identified as W. mexicana. It differs from typical W. mexicana, however, in having completely filamentous indusial segments, multicellular (often filamentous) translucent projections on the pinnule margins, a greater number of pinnules per pinna, and a diploid chromosome number. Woodsia phillipsii is the only diploid species currently recognized in the W. mexicana complex, and it was probably involved in the hybrid origins of both W. mexicana and W. neomexicana. Some individuals of the latter species are difficult to distinguish from W. phillipsii (see comments under W. neomexicana), and the two taxa occasionally hybridize to produce sterile triploids of intermediate morphology. Woodsia phillipsii is also known to hybridize with W. plummerae (see comments under that species) and W. cochisensis.</discussion>
  
</bio:treatment>