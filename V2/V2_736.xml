<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="C. Presl" date="1836" rank="genus">campyloneurum</taxon_name>
    <taxon_name authority="(Kunze) C. Presl" date="1836" rank="species">costatum</taxon_name>
    <place_of_publication>
      <publication_title>Tent. Pterid.</publication_title>
      <place_in_publication>190, plate 7. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus campyloneurum;species costatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500306</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Kunze" date="unknown" rank="species">costatum</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>9: 38. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species costatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, 2–5 mm diam.</text>
      <biological_entity id="o15328" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s0" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves few, arching to pendent, stiff.</text>
      <biological_entity id="o15329" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character char_type="range_value" from="arching" name="orientation" src="d0_s1" to="pendent" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole 4–15 cm.</text>
      <biological_entity id="o15330" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade dark green, linear-elliptic or linear-oblanceolate, 20–40 × 2.5–6 cm, leathery;</text>
      <biological_entity id="o15331" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base acuminate;</text>
      <biological_entity id="o15332" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins shallowly sinuate to undulate;</text>
      <biological_entity id="o15333" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="shallowly sinuate" name="shape" src="d0_s5" to="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex abruptly caudate-acuminate.</text>
      <biological_entity id="o15334" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="caudate-acuminate" value_original="caudate-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Veins obscure, primary-veins obscure, slightly to strongly curved, areoles in 4–8 series between costa and margin, with usually 2 free included veinlets per areole.</text>
      <biological_entity id="o15335" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o15336" name="primary-vein" name_original="primary-veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="slightly to strongly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o15337" name="areole" name_original="areoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="between costa and margin" constraintid="o15339-o15340" id="o15338" name="series" name_original="series" src="d0_s7" type="structure" constraint_original="between  costa and  margin, ">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="8" />
        <character modifier="usually" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15339" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <biological_entity id="o15340" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o15341" name="veinlet" name_original="veinlets" src="d0_s7" type="structure" />
      <biological_entity id="o15342" name="areole" name_original="areole" src="d0_s7" type="structure" />
      <relation from="o15337" id="r3342" name="in" negation="false" src="d0_s7" to="o15338" />
      <relation from="o15337" id="r3343" name="included" negation="false" src="d0_s7" to="o15341" />
      <relation from="o15337" id="r3344" name="per" negation="false" src="d0_s7" to="o15342" />
    </statement>
    <statement id="d0_s8">
      <text>Sori in 1–several rows on each side of costa.</text>
      <biological_entity id="o15344" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="several" />
      </biological_entity>
      <biological_entity id="o15345" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o15346" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <relation from="o15343" id="r3345" name="in" negation="false" src="d0_s8" to="o15344" />
      <relation from="o15344" id="r3346" name="on" negation="false" src="d0_s8" to="o15345" />
      <relation from="o15345" id="r3347" name="part_of" negation="false" src="d0_s8" to="o15346" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 74.</text>
      <biological_entity id="o15343" name="sorus" name_original="sori" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o15347" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="74" value_original="74" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic in swamps, on various rough-barked trees, substrate subacid</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="epiphytic" constraint="in swamps" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="various rough-barked trees" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies; Central America; South America in Venezuela, Colombia, Ecuador.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America in Venezuela" establishment_means="native" />
        <character name="distribution" value="Colombia" establishment_means="native" />
        <character name="distribution" value="Ecuador" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Tailed strap fern</other_name>
  <discussion>In the flora Campyloneurum costatum is currently known only from Collier County, Florida. It had formerly been reported from Dade County, where it now may have been extirpated.</discussion>
  
</bio:treatment>