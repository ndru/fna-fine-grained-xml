<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="(R. Brown) J. Smith" date="1841" rank="genus">phlebodium</taxon_name>
    <taxon_name authority="(Linnaeus) J. Smith" date="1841" rank="species">aureum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>4: 59. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus phlebodium;species aureum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500905</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">aureum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1087. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species aureum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, ca. 8–15 (–30) mm diam., densely scaly;</text>
      <biological_entity id="o907" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s0" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s0" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales reddish to golden, long-attenuate, 10–20 mm.</text>
      <biological_entity id="o908" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s1" to="golden" />
        <character is_modifier="false" name="shape" src="d0_s1" value="long-attenuate" value_original="long-attenuate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves bright green or glaucous, arching to pendent, scattered, 3–13 dm.</text>
      <biological_entity id="o909" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="arching" name="orientation" src="d0_s2" to="pendent" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="13" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1.5–5 dm, smooth, with a few scales near base.</text>
      <biological_entity id="o910" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="5" to_unit="dm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o911" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o912" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o910" id="r207" name="with" negation="false" src="d0_s3" to="o911" />
      <relation from="o911" id="r208" name="near" negation="false" src="d0_s3" to="o912" />
    </statement>
    <statement id="d0_s4">
      <text>Blade pinnately and deeply lobed, 3–8 × 1–5 dm, glabrous, terminal segment conform.</text>
      <biological_entity id="o913" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s4" to="8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="width" src="d0_s4" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o914" name="segment" name_original="segment" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Segments lanceolate to elliptic, or linear-lanceolate to linear, 6–20 × 1–4 cm, margins entire or sometimes undulate.</text>
      <biological_entity id="o915" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic or linear-lanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic or linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o916" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sori in 1 line on each side of costae, occasionally 2d row present, sori terminal or at junction of free included veinlets.</text>
      <biological_entity id="o918" name="line" name_original="line" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o919" name="junction" name_original="junction" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o920" name="veinlet" name_original="veinlets" src="d0_s6" type="structure" />
      <relation from="o917" id="r209" name="in" negation="false" src="d0_s6" to="o918" />
      <relation from="o918" id="r210" name="on each side of costae , occasionally 2d row present , sori terminal or at" negation="false" src="d0_s6" to="o919" />
      <relation from="o919" id="r211" name="included" negation="false" src="d0_s6" to="o920" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 148.</text>
      <biological_entity id="o917" name="sorus" name_original="sori" src="d0_s6" type="structure" />
      <biological_entity constraint="2n" id="o921" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="148" value_original="148" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic on a variety of trees or on logs, dense piles of humus, but most commonly among old leaf bases of Sabal palmetto Loddiges, in various habitats from hammocks to swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="a variety" modifier="epiphytic on" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="dense piles" modifier="or on logs" constraint="of humus" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="old leaf bases" modifier="but most commonly among" constraint="of sabal palmetto loddiges , in various habitats from hammocks to swamps" />
        <character name="habitat" value="various habitats" constraint="from hammocks to swamps" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="epiphytic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Goldfoot fern</other_name>
  <other_name type="common_name">golden polypody</other_name>
  <discussion>Phlebodium aureum occurs north to Dixie and Nassau counties in Florida, and it is disjunct in Franklin County. It is also found in Georgia (W. H. Duncan 1954; L. H. Snyder Jr. and J. G. Bruce 1986). Two varieties (or subspecies) have been recognized, Phlebodium aureum var. aureum and P. aureum var. areolatum (Humboldt &amp; Bonpland ex Willdenow) Farwell. The latter is now often elevated to species rank and given the name P. pseudoaureum (Cavanilles) Lellinger. Phlebodium pseudoaureum is widespread in Central America and South America (D. B. Lellinger 1987) and has been reported as rare in Florida by G. R. Proctor (1985). I have not seen specimens that could be convincingly referred to P. pseudoaureum.</discussion>
  <discussion>Phlebodium aureum, a tetraploid species, is believed to have arisen through allopolyploidy following hybridization between P. pseudoaureum and P. decumanum (Willdenow) J. Smith, a widespread species in tropical America.</discussion>
  
</bio:treatment>