<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">thelypteris</taxon_name>
    <taxon_name authority="(Hooker) Alston" date="1958" rank="subgenus">Lastrea</taxon_name>
    <taxon_name authority="(H. Christ) Ching" date="1936" rank="species">quelpaertensis</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Fan Mem. Inst. Biol.</publication_title>
      <place_in_publication>6: 328. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus thelypteris;subgenus lastrea;species quelpaertensis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233501298</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="H. Christ" date="unknown" rank="species">quelpaertensis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Léveillé, Bull. Acad. Int. Géogr. Bot.</publication_title>
      <place_in_publication>20: 7. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dryopteris;species quelpaertensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oreopteris</taxon_name>
    <taxon_name authority="(H. Christ) Holub" date="unknown" rank="species">quelpaertensis</taxon_name>
    <taxon_hierarchy>genus Oreopteris;species quelpaertensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping to suberect, 5–10 mm diam.</text>
      <biological_entity id="o15688" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="short-creeping" name="orientation" src="d0_s0" to="suberect" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, dying back in winter, crowded, (15–) 25–100 cm.</text>
      <biological_entity id="o15689" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character constraint="in winter" is_modifier="false" name="condition" src="d0_s1" value="dying" value_original="dying" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole straw-colored to tan above base, 3–20 cm × 2–5 mm, scales on petioles and rachises tan to straw-colored, persistent, ovate to lanceolate.</text>
      <biological_entity id="o15690" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="above base" constraintid="o15691" from="straw-colored" name="coloration" src="d0_s2" to="tan" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" notes="" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15691" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o15692" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" notes="" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o15693" name="petiole" name_original="petioles" src="d0_s2" type="structure" />
      <biological_entity id="o15694" name="rachis" name_original="rachises" src="d0_s2" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s2" to="straw-colored" />
      </biological_entity>
      <relation from="o15692" id="r3415" name="on" negation="false" src="d0_s2" to="o15693" />
      <relation from="o15692" id="r3416" name="on" negation="false" src="d0_s2" to="o15694" />
    </statement>
    <statement id="d0_s3">
      <text>Blade elliptic, 25–80 cm, 5–10 pairs of proximal pinnae gradually smaller toward base, lowest pinnae ca. 1 cm, blade tapering gradually to pinnatifid apex.</text>
      <biological_entity id="o15695" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s3" to="80" to_unit="cm" />
        <character char_type="range_value" constraint="of proximal pinnae" constraintid="o15696" from="5" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15696" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character constraint="toward base" constraintid="o15697" is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o15697" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="lowest" id="o15698" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character name="some_measurement" src="d0_s3" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15699" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o15700" is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o15700" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae deeply pinnatifid to ca. 1 mm or less from costa, 3–12 × 1–2 cm;</text>
      <biological_entity id="o15701" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character name="some_measurement" src="d0_s4" value="less" value_original="less" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" notes="" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15702" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <relation from="o15701" id="r3417" name="from" negation="false" src="d0_s4" to="o15702" />
    </statement>
    <statement id="d0_s5">
      <text>segments linear to oblong, somewhat oblique and often somewhat curved, entire or crenulate, basal segments of proximal pinnae more often crenulate;</text>
      <biological_entity id="o15703" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblong" />
        <character is_modifier="false" modifier="somewhat" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
        <character is_modifier="false" modifier="often somewhat" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15704" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15705" name="pinna" name_original="pinnae" src="d0_s5" type="structure" />
      <relation from="o15704" id="r3418" name="part_of" negation="false" src="d0_s5" to="o15705" />
    </statement>
    <statement id="d0_s6">
      <text>proximal pair of veins from adjacent segments meeting margin above sinus.</text>
      <biological_entity constraint="vein" id="o15706" name="segment" name_original="segments" src="d0_s6" type="structure" constraint_original="vein proximal; vein" />
      <biological_entity id="o15707" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o15708" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o15709" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <biological_entity id="o15710" name="sinus" name_original="sinus" src="d0_s6" type="structure" />
      <relation from="o15706" id="r3419" name="part_of" negation="false" src="d0_s6" to="o15707" />
      <relation from="o15706" id="r3420" name="from" negation="false" src="d0_s6" to="o15708" />
      <relation from="o15708" id="r3421" name="meeting" negation="false" src="d0_s6" to="o15709" />
      <relation from="o15706" id="r3422" name="above" negation="false" src="d0_s6" to="o15710" />
    </statement>
    <statement id="d0_s7">
      <text>Indument abaxially of tan to whitish linear scales along costae, hairs lacking or sparse along costae, blade tissue lacking glands or sparsely glandular.</text>
      <biological_entity id="o15711" name="indument" name_original="indument" src="d0_s7" type="structure" />
      <biological_entity id="o15712" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character char_type="range_value" from="tan" is_modifier="true" name="coloration" src="d0_s7" to="whitish" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o15713" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity id="o15714" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="lacking" value_original="lacking" />
        <character constraint="along costae" constraintid="o15715" is_modifier="false" name="quantity" src="d0_s7" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o15715" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o15716" name="tissue" name_original="tissue" src="d0_s7" type="structure" />
      <biological_entity id="o15717" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o15711" id="r3423" name="part_of" negation="false" src="d0_s7" to="o15712" />
      <relation from="o15711" id="r3424" name="along" negation="false" src="d0_s7" to="o15713" />
    </statement>
    <statement id="d0_s8">
      <text>Sori round, submarginal;</text>
      <biological_entity id="o15718" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" name="position" src="d0_s8" value="submarginal" value_original="submarginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>indusia tan, glabrous;</text>
      <biological_entity id="o15719" name="indusium" name_original="indusia" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="tan" value_original="tan" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sporangia glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 68.</text>
      <biological_entity id="o15720" name="sporangium" name_original="sporangia" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15721" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial in open, rocky woods and subalpine meadows in acid soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="terrestrial in" />
        <character name="habitat" value="rocky woods" />
        <character name="habitat" value="subalpine meadows" constraint="in acid soils" />
        <character name="habitat" value="acid soils" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Nfld.; Alaska, Wash.; e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <discussion>Although the name Thelypteris limbosperma (Allioni) H. P. Fuchs, type from Europe, has usually been applied to plants in the flora, specimens from western North America match more closely those from eastern Asia; therefore, a name based on a Korean type is used here. The single collection from the coast of Newfoundland (reported by A. Bouchard and S. G. Hay 1976) is remarkably disjunct but matches collections from western North America rather than those of the European species.</discussion>
  
</bio:treatment>