<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">dryopteris</taxon_name>
    <taxon_name authority="(D. C. Eaton) Dowell" date="1906" rank="species">clintoniana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Staten Island Assoc. Arts</publication_title>
      <place_in_publication>1: 64. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus dryopteris;species clintoniana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500594</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aspidium</taxon_name>
    <taxon_name authority="(Linnaeus) Swartz" date="unknown" rank="species">cristatum</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="variety">clintonianum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>5, 665. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aspidium;species cristatum;variety clintonianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Linnaeus) A. Gray" date="unknown" rank="species">cristata</taxon_name>
    <taxon_name authority="(D. C. Eaton) L. Underwood" date="unknown" rank="variety">clintoniana</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species cristata;variety clintoniana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves dimorphic, 45–100 × 12–20 cm;</text>
      <biological_entity id="o8216" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="45" from_unit="cm" name="length" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="width" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>fertile leaves dying back in winter;</text>
      <biological_entity id="o8217" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character constraint="in winter" is_modifier="false" name="condition" src="d0_s1" value="dying" value_original="dying" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sterile leaves 1–several, smaller, green through winter.</text>
      <biological_entity id="o8218" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="size" src="d0_s2" value="smaller" value_original="smaller" />
        <character constraint="through winter" is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/4–1/3 length of leaf, scaly at least at base;</text>
      <biological_entity id="o8219" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/4 length of leaf" name="length" src="d0_s3" to="1/3 length of leaf" />
        <character constraint="at base" constraintid="o8220" is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o8220" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>scales scattered, tan, sometimes with dark-brown center.</text>
      <biological_entity id="o8221" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity id="o8222" name="center" name_original="center" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <relation from="o8221" id="r1728" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o8222" />
    </statement>
    <statement id="d0_s5">
      <text>Blade green, lanceolate, with nearly parallel sides, pinnate-pinnatifid, herbaceous, not glandular.</text>
      <biological_entity id="o8223" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o8224" name="side" name_original="sides" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="nearly" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
      <relation from="o8223" id="r1729" name="with" negation="false" src="d0_s5" to="o8224" />
    </statement>
    <statement id="d0_s6">
      <text>Pinnae of fertile leaves twisted out of plane of blade but not fully perpendicular to it, narrowly elongate-deltate;</text>
      <biological_entity id="o8225" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character constraint="of blade" constraintid="o8227" is_modifier="false" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s6" value="elongate-deltate" value_original="elongate-deltate" />
      </biological_entity>
      <biological_entity id="o8226" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o8227" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not fully" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <relation from="o8225" id="r1730" name="part_of" negation="false" src="d0_s6" to="o8226" />
    </statement>
    <statement id="d0_s7">
      <text>basal pinnae narrowly elongate-deltate, much reduced;</text>
      <biological_entity constraint="basal" id="o8228" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="elongate-deltate" value_original="elongate-deltate" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>basal pinnules longer than or equal to adjacent pinnules, basal basiscopic pinnule and basal acroscopic pinnule equal;</text>
      <biological_entity constraint="basal" id="o8229" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character constraint="than or equal to adjacent pinnules , basal basiscopic pinnule and basal acroscopic pinnule" constraintid="o8230, o8232" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8230" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o8232" name="pinnule" name_original="pinnule" src="d0_s8" type="structure">
        <character is_modifier="true" name="variability" src="d0_s8" value="equal" value_original="equal" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pinnule margins serrate or biserrate, with spiny teeth.</text>
      <biological_entity constraint="pinnule" id="o8233" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="biserrate" value_original="biserrate" />
      </biological_entity>
      <biological_entity id="o8234" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="spiny" value_original="spiny" />
      </biological_entity>
      <relation from="o8233" id="r1731" name="with" negation="false" src="d0_s9" to="o8234" />
    </statement>
    <statement id="d0_s10">
      <text>Sori midway between midvein and margin of segments.</text>
      <biological_entity id="o8235" name="sorus" name_original="sori" src="d0_s10" type="structure">
        <character constraint="between midvein, margin" constraintid="o8236, o8237" is_modifier="false" name="position" src="d0_s10" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity id="o8236" name="midvein" name_original="midvein" src="d0_s10" type="structure" />
      <biological_entity id="o8237" name="margin" name_original="margin" src="d0_s10" type="structure" />
      <biological_entity id="o8238" name="segment" name_original="segments" src="d0_s10" type="structure" />
      <relation from="o8236" id="r1732" name="part_of" negation="false" src="d0_s10" to="o8238" />
      <relation from="o8237" id="r1733" name="part_of" negation="false" src="d0_s10" to="o8238" />
    </statement>
    <statement id="d0_s11">
      <text>Indusia lacking glands.</text>
      <biological_entity id="o8240" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 246.</text>
      <biological_entity id="o8239" name="indusium" name_original="indusia" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o8241" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="246" value_original="246" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swampy woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swampy woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Conn., Ind., Maine, Mass., Mich., N.H., N.J., N.Y., Ohio, Pa., R.I., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Clinton's wood fern</other_name>
  <other_name type="common_name">dryoptère de clinton</other_name>
  <discussion>Dryopteris clintoniana is a North American endemic and an allohexaploid derived from D. cristata and D. goldieana. Dryopteris clintoniana hybridizes with six species. Hybrids can be identified by the fairly narrow blades and elongate-deltate proximal pinnae.</discussion>
  
</bio:treatment>