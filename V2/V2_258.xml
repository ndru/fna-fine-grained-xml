<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bartlett" date="unknown" rank="family">cupressaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juniperus</taxon_name>
    <taxon_name authority="Spach" date="1841" rank="section">Sabina</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginiana</taxon_name>
    <taxon_name authority="(Small) E. Murray" date="1983" rank="variety">silicicola</taxon_name>
    <place_of_publication>
      <publication_title>Kalmia</publication_title>
      <place_in_publication>13: 8. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cupressaceae;genus juniperus;section sabina;species virginiana;variety silicicola;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500740</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sabina</taxon_name>
    <taxon_name authority="(Small) L. H. Bailey" date="unknown" rank="species">silicicola</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>24: 5. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sabina;species silicicola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juniperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">silicicola</taxon_name>
    <taxon_hierarchy>genus Juniperus;species silicicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 10 m;</text>
      <biological_entity id="o13879" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>crown flattened or conic (when young and protected or crowded).</text>
      <biological_entity id="o13880" name="crown" name_original="crown" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s1" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Bark cinnamon reddish.</text>
      <biological_entity id="o13881" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="cinnamon reddish" value_original="cinnamon reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Branches spreading to pendulous.</text>
      <biological_entity id="o13882" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Scalelike leaves bluntly obtuse to acute at apex.</text>
      <biological_entity id="o13883" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" constraint="at apex" constraintid="o13884" from="bluntly obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o13884" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pollen cones 4–5 mm.</text>
      <biological_entity constraint="pollen" id="o13885" name="cone" name_original="cones" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seed-cones 3–4 mm.</text>
      <biological_entity id="o13886" name="cone-seed" name_original="seed-cones" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="distance" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds 1.5–3 mm.</text>
      <biological_entity id="o13887" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal foredunes and coastal river sandbanks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal foredunes" />
        <character name="habitat" value="coastal river sandbanks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–15 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="15" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5b</number>
  <other_name type="common_name">Southern redcedar</other_name>
  <other_name type="common_name">coastal redcedar</other_name>
  <discussion>This southern variety of Juniperus virginiana appears to be restricted to coastal foredunes but differs little in morphology or leaf terpenoids from upland J. virginiana and appears to intergrade with that variety in Georgia (R. P. Adams 1986). These taxa are distinct from the Caribbean junipers J. lucayana Britton of the Bahamas, Jamaica, and Cuba, and J. bermudiana Linnaeus of Bermuda (R. P. Adams et al. 1987). Reports of J. virginiana var. silicicola from west of Florida are questionable.</discussion>
  
</bio:treatment>