<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1805" rank="genus">cystopteris</taxon_name>
    <taxon_name authority="(Michaux) Desvaux" date="1827" rank="species">tenuis</taxon_name>
    <place_of_publication>
      <place_in_publication>6: 264. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus cystopteris;species tenuis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500466</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nephrodium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">tenue</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 269. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Nephrodium;species tenue;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cystopteris</taxon_name>
    <taxon_name authority="(Linnaeus) Bernhardi" date="unknown" rank="species">fragilis</taxon_name>
    <taxon_name authority="G. Lawson" date="unknown" rank="variety">mackayi</taxon_name>
    <taxon_hierarchy>genus Cystopteris;species fragilis;variety mackayi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, not cordlike, internodes short, beset with old petiole bases, hairs absent;</text>
      <biological_entity id="o12560" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
      </biological_entity>
      <biological_entity id="o12561" name="internode" name_original="internodes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o12562" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o12563" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o12561" id="r2740" name="beset with" negation="false" src="d0_s0" to="o12562" />
    </statement>
    <statement id="d0_s1">
      <text>scales tan to light-brown, lanceolate, radial walls thin, luminae tan.</text>
      <biological_entity id="o12564" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s1" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o12565" name="wall" name_original="walls" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="radial" value_original="radial" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o12566" name="lumina" name_original="luminae" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, clustered at stem apex, to 40 cm, nearly all bearing sori.</text>
      <biological_entity id="o12567" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character constraint="at stem apex" constraintid="o12568" is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o12568" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o12569" name="sorus" name_original="sori" src="d0_s2" type="structure" />
      <relation from="o12567" id="r2741" modifier="nearly" name="bearing" negation="false" src="d0_s2" to="o12569" />
    </statement>
    <statement id="d0_s3">
      <text>Petiole dark at base, mostly green to straw-colored distally, shorter than or nearly equaling blade, base sparsely scaly.</text>
      <biological_entity id="o12570" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o12571" is_modifier="false" name="coloration" src="d0_s3" value="dark" value_original="dark" />
        <character char_type="range_value" from="mostly green" modifier="distally" name="coloration" notes="" src="d0_s3" to="straw-colored" />
        <character constraint="than or nearly equaling blade" constraintid="o12572" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o12571" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o12572" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12573" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blade lanceolate to narrowly elliptic, 1 (–2) -pinnate-pinnatifid, widest at or just below middle, apex short-attenuate;</text>
      <biological_entity id="o12574" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="narrowly elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="1(-2)-pinnate-pinnatifid" value_original="1(-2)-pinnate-pinnatifid" />
        <character constraint="at middle" constraintid="o12575" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o12575" name="middle" name_original="middle" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="just below" name="position" src="d0_s4" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o12576" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-attenuate" value_original="short-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis and costae lacking gland-tipped hairs or bulblets;</text>
      <biological_entity id="o12577" name="rachis" name_original="rachis" src="d0_s5" type="structure" />
      <biological_entity id="o12578" name="costa" name_original="costae" src="d0_s5" type="structure" />
      <biological_entity id="o12579" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o12580" name="bulblet" name_original="bulblets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>axils of pinnae lacking multicellular, gland-tipped hairs.</text>
      <biological_entity id="o12581" name="axil" name_original="axils" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o12582" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o12583" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o12581" id="r2742" name="consist_of" negation="false" src="d0_s6" to="o12582" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae typically at acute angle to rachis, often curving toward blade apex, margins crenulate;</text>
      <biological_entity id="o12584" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character constraint="toward blade apex" constraintid="o12587" is_modifier="false" modifier="often" name="course" notes="" src="d0_s7" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o12585" name="angle" name_original="angle" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o12586" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o12587" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o12588" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <relation from="o12584" id="r2743" name="at" negation="false" src="d0_s7" to="o12585" />
      <relation from="o12585" id="r2744" name="to" negation="false" src="d0_s7" to="o12586" />
    </statement>
    <statement id="d0_s8">
      <text>proximal pinnae pinnatifid to pinnate-pinnatifid, ± equilateral, basiscopic pinnules not enlarged;</text>
      <biological_entity constraint="proximal" id="o12589" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s8" to="pinnate-pinnatifid" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s8" value="equilateral" value_original="equilateral" />
      </biological_entity>
      <biological_entity id="o12590" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s8" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal basiscopic pinnules sessile, base cuneate to obtuse, distal pinnae ovate to narrowly elliptic.</text>
      <biological_entity constraint="basal" id="o12591" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o12592" name="pinnule" name_original="pinnules" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o12593" name="base" name_original="base" src="d0_s9" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s9" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12594" name="pinna" name_original="pinnae" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="narrowly elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins directed into teeth and notches.</text>
      <biological_entity id="o12595" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character constraint="into notches" constraintid="o12597" is_modifier="false" name="orientation" src="d0_s10" value="directed" value_original="directed" />
      </biological_entity>
      <biological_entity id="o12596" name="tooth" name_original="teeth" src="d0_s10" type="structure" />
      <biological_entity id="o12597" name="notch" name_original="notches" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Indusia ovate to cupshaped, without gland-tipped hairs.</text>
      <biological_entity id="o12598" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="cupshaped" />
      </biological_entity>
      <biological_entity id="o12599" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o12598" id="r2745" name="without" negation="false" src="d0_s11" to="o12599" />
    </statement>
    <statement id="d0_s12">
      <text>Spores spiny, usually 39–50 µm. 2n = 168.</text>
      <biological_entity id="o12600" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="spiny" value_original="spiny" />
        <character char_type="range_value" from="39" from_unit="um" modifier="usually" name="some_measurement" src="d0_s12" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12601" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="168" value_original="168" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly on shaded rock and cliff faces but also occasionally on forest floors</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded rock" modifier="mostly on" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="forest floors" modifier="faces but also occasionally on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ark., Ariz., Conn., Del., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., Nev., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., Tenn., Utah, Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Mackay's brittle fern</other_name>
  <other_name type="common_name">cystoptère ténue</other_name>
  <discussion>Long recognized as Cystopteris fragilis var. mackayi, C. tenuis was returned to species status by R. C. Moran (1983b). It is probably an allotetraploid originating from C. protrusa and an extinct diploid related to C. fragilis (C. H. Haufler 1985; C. H. Haufler and M. D. Windham 1991).</discussion>
  <discussion>Cystopteris tenuis is common in eastern North America and less frequent at the northern and western perimeter of its range. In the center of its distribution (Minnesota, Iowa, Illinois, Wisconsin, Indiana, Ohio, Pennsylvania), the narrow, elliptic pinnae angled toward the blade apex and the rounded teeth make C. tenuis relatively distinct from C. fragilis and C. protrusa (although the early season, sterile leaves of C. protrusa often resemble those of C. tenuis). In the west and especially in the northeast, C. tenuis and C. fragilis are difficult to distinguish. For the most part, C. fragilis is confined to higher latitudes and elevations than C. tenuis, but the two species can be sympatric and occasionally form sterile tetraploid hybrids. Cystopteris protrusa and C. tenuis are infrequently sympatric, but where they are, sterile triploid hybrids can occur. Hybrids between C. tenuis and C. tennesseensis are recognized as C. × wagneri (R. C. Moran 1983). Hybridization between C. tenuis and C. bulbifera has also been reported (R. C. Moran 1982b). This hybrid, C. × illinoensis R. C. Moran, is known only from the type and needs to be studied further.</discussion>
  
</bio:treatment>