<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Hooker" date="unknown" rank="family">parkeriaceae</taxon_name>
    <taxon_name authority="Brongniart" date="1821" rank="genus">Ceratopteris</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>sér. 3, 8: 186. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family parkeriaceae;genus Ceratopteris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek cerato, horned, and pteris, fern, referring to the antlerlike fertile leaf</other_info_on_name>
    <other_info_on_name type="fna_id">106195</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Hooker" date="unknown" rank="genus">Parkeria</taxon_name>
    <place_of_publication>
      <publication_title>Exot. Fl.</publication_title>
      <place_in_publication>2: plate 147. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Parkeria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–120 cm.</text>
      <biological_entity id="o10097" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10 mm diam., sparsely scaly.</text>
      <biological_entity id="o10098" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect.</text>
      <biological_entity id="o10099" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Proximal leaves broad, simple and lobed to 4-pinnate, glabrous;</text>
      <biological_entity constraint="proximal" id="o10100" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="lobed" name="shape" src="d0_s3" to="4-pinnate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal leaves finely dissected with linear ultimate segments.</text>
      <biological_entity constraint="distal" id="o10101" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="with ultimate segments" constraintid="o10102" is_modifier="false" modifier="finely" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o10102" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sporangia nearly sessile, usually densely packed, globose.</text>
      <biological_entity id="o10103" name="sporangium" name_original="sporangia" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="usually densely" name="arrangement" src="d0_s5" value="packed" value_original="packed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spores tetrahedral.</text>
      <biological_entity id="o10104" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="tetrahedral" value_original="tetrahedral" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical, subtropical, and warm temperate regions worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical" establishment_means="native" />
        <character name="distribution" value="subtropical" establishment_means="native" />
        <character name="distribution" value="and warm temperate regions worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Antler ferns</other_name>
  <other_name type="common_name">water ferns</other_name>
  <other_name type="common_name">floating ferns</other_name>
  <discussion>Species 3–4 (3 in the flora).</discussion>
  <references>
    <reference>DeVol, C. E. 1957. The geographical distribution of Ceratopteris pteridoides. Amer. Fern J. 47: 67–72.</reference>
    <reference>Hickok, L. G. 1977. Cytological relationships between three diploid species of the fern genus Ceratopteris Brongn. Canad. J. Bot. 55: 1660–1667.</reference>
    <reference>Hickok, L. G. and E. J. Klekowski Jr. 1974. Inchoate speciation in Ceratopteris: An analysis of the synthesized hybrid C. richardii  ×  C. pteridoides. Evolution 28: 439–446.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sterile leaves simple, palmately or pinnately lobed, sometimes 2–4-pinnate, deltate to cordate to ovate; proximal pinnae or veins of lobes usually opposite; petioles frequently inflated. Fertile leaves usually deltate to cordate to reniform; sporangia lacking annulus or annulus well developed, indurate cells 0–10(–40).</description>
      <determination>2 Ceratopteris pteridoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sterile leaves (1–)2–3-pinnate, lanceolate, lance-ovate, ovate to deltate; proximal pinnae usually alternate; petioles usually not inflated. Fertile leaves lanceolate, sometimes ovate, deltate, or cordate; sporangia with well-developed annulus, indurate cells 13–71.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sporangia with 32 spores.</description>
      <determination>1 Ceratopteris thalictroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sporangia with 16 spores.</description>
      <determination>3 Ceratopteris richardii</determination>
    </key_statement>
  </key>
</bio:treatment>