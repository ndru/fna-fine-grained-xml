<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="H. Mason &amp; Stockwell" date="1945" rank="species">washoensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>8: 62. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species washoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">233500958</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 60m;</text>
      <biological_entity id="o18367" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="60" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 1m diam., straight;</text>
      <biological_entity id="o18368" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="1" to_unit="m" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown pyramidal.</text>
      <biological_entity id="o18369" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark yellowbrown to reddish, fissured, plates scaly.</text>
      <biological_entity id="o18370" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s3" to="reddish" />
        <character is_modifier="false" name="relief" src="d0_s3" value="fissured" value_original="fissured" />
      </biological_entity>
      <biological_entity id="o18371" name="plate" name_original="plates" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches spreading-ascending;</text>
      <biological_entity id="o18372" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading-ascending" value_original="spreading-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>twigs stout, orangish, aging gray, rough.</text>
      <biological_entity id="o18373" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s5" value="stout" value_original="stout" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orangish" value_original="orangish" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds ovoid, redbrown, 1.5–2cm, not resinous;</text>
      <biological_entity id="o18374" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scale margins fringed.</text>
      <biological_entity constraint="scale" id="o18375" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="fringed" value_original="fringed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Leaves (2–) 3 per fascicle, spreading-ascending, persisting (2–) 4–6 (–7) years, 10–15cm × ca. 1.5mm, slightly twisted, gray-green, all surfaces with stomatal lines, margins finely serrulate, apex acuminate;</text>
      <biological_entity id="o18376" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character constraint="per fascicle" constraintid="o18377" name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="7" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="6" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o18377" name="fascicle" name_original="fascicle" src="d0_s8" type="structure" />
      <biological_entity id="o18378" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o18379" name="stomatal" name_original="stomatal" src="d0_s8" type="structure" />
      <biological_entity id="o18380" name="line" name_original="lines" src="d0_s8" type="structure" />
      <biological_entity id="o18381" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s8" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o18382" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o18378" id="r4028" name="with" negation="false" src="d0_s8" to="o18379" />
      <relation from="o18378" id="r4029" name="with" negation="false" src="d0_s8" to="o18380" />
    </statement>
    <statement id="d0_s9">
      <text>sheath 1–2cm, base persistent.</text>
      <biological_entity id="o18383" name="sheath" name_original="sheath" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18384" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pollen cones cylindric, 10–20mm, red-purple.</text>
      <biological_entity constraint="pollen" id="o18385" name="cone" name_original="cones" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="red-purple" value_original="red-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seed-cones maturing in 2 years, shedding seeds soon thereafter, not persistent, spreading, slightly asymmetric, ovoid-conic before opening, broadly ovoid when open, 7–10cm, tan or pale redbrown, sessile, abaxial surface of scales darker and sharply contrasting in color with adaxial surface;</text>
      <biological_entity id="o18386" name="cone-seed" name_original="seed-cones" src="d0_s11" type="structure">
        <character constraint="in years" constraintid="o18387" is_modifier="false" name="life_cycle" src="d0_s11" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="not" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s11" value="asymmetric" value_original="asymmetric" />
        <character constraint="before opening" constraintid="o18389" is_modifier="false" name="shape" src="d0_s11" value="ovoid-conic" value_original="ovoid-conic" />
        <character is_modifier="false" modifier="when open" name="shape" notes="" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="cm" name="distance" src="d0_s11" to="10" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale redbrown" value_original="pale redbrown" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o18387" name="year" name_original="years" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18388" name="seed" name_original="seeds" src="d0_s11" type="structure" />
      <biological_entity id="o18389" name="opening" name_original="opening" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o18390" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o18391" name="scale" name_original="scales" src="d0_s11" type="structure" />
      <biological_entity constraint="adaxial" id="o18392" name="surface" name_original="surface" src="d0_s11" type="structure" />
      <relation from="o18386" id="r4030" modifier="soon thereafter; thereafter" name="shedding" negation="false" src="d0_s11" to="o18388" />
      <relation from="o18390" id="r4031" name="part_of" negation="false" src="d0_s11" to="o18391" />
      <relation from="o18390" id="r4032" modifier="sharply" name="with" negation="false" src="d0_s11" to="o18392" />
    </statement>
    <statement id="d0_s12">
      <text>apophyses slightly raised, low pyramidal;</text>
      <biological_entity id="o18393" name="apophysis" name_original="apophyses" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s12" value="raised" value_original="raised" />
        <character is_modifier="false" name="position" src="d0_s12" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s12" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>umbo central, narrowly pyramidal, tapering into short, reflexed, fine prickle.</text>
      <biological_entity id="o18394" name="umbo" name_original="umbo" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="central" value_original="central" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="pyramidal" value_original="pyramidal" />
        <character constraint="into prickle" constraintid="o18395" is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18395" name="prickle" name_original="prickle" src="d0_s13" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="width" src="d0_s13" value="fine" value_original="fine" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds ellipsoid;</text>
      <biological_entity id="o18396" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>body ca. 0.8cm, gray-brown;</text>
      <biological_entity id="o18397" name="body" name_original="body" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="cm" value="0.8" value_original="0.8" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="gray-brown" value_original="gray-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>wing to 16mm.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n =24.</text>
      <biological_entity id="o18398" name="wing" name_original="wing" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18399" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry montane forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry montane forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–2500m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26</number>
  <other_name type="common_name">Washoe pine</other_name>
  <discussion>Pinus washoensis often occurs in large stands and resembles P. jeffreyi. The number and posture of seed-cone scales fall within the ranges given for P. jeffreyi. The abaxial surface of these scales has a significantly darker pigmentation, however; such a color contrast is not apparent in P. jeffreyi. Forest geneticists have developed hybrids between P. washoensis and related yellow pines, but no natural hybrids have been observed. Some workers regard P. washoensis as closely related to—or even conspecific with--- P. ponderosa.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>