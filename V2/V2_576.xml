<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">abies</taxon_name>
    <taxon_name authority="A. Murray bis" date="1863" rank="species">bifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Roy. Hort. Soc. London</publication_title>
      <place_in_publication>3: 320. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus abies;species bifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500003</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Abies</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">subalpina</taxon_name>
    <taxon_hierarchy>genus Abies;species subalpina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 30m;</text>
      <biological_entity id="o7738" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 0.45m diam.;</text>
      <biological_entity id="o7739" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="0.45" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown spirelike.</text>
      <biological_entity id="o7740" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spirelike" value_original="spirelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark gray, thin, smooth, with age somewhat furrowed and scaly (toward southern end of range bark corky [corkbark fir]).</text>
      <biological_entity id="o7741" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o7742" name="age" name_original="age" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s3" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <relation from="o7741" id="r1613" name="with" negation="false" src="d0_s3" to="o7742" />
    </statement>
    <statement id="d0_s4">
      <text>Branches diverging from trunk at right angles, stout, stiff;</text>
      <biological_entity id="o7743" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character constraint="from trunk" constraintid="o7744" is_modifier="false" name="orientation" src="d0_s4" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="fragility_or_size" notes="" src="d0_s4" value="stout" value_original="stout" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o7744" name="trunk" name_original="trunk" src="d0_s4" type="structure" />
      <biological_entity id="o7745" name="angle" name_original="angles" src="d0_s4" type="structure" />
      <relation from="o7744" id="r1614" name="at" negation="false" src="d0_s4" to="o7745" />
    </statement>
    <statement id="d0_s5">
      <text>twigs opposite to whorled, grayish, pubescence sparse, light-brown;</text>
      <biological_entity id="o7746" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character char_type="range_value" from="opposite" name="arrangement" src="d0_s5" to="whorled" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>fresh leaf-scars with light-brown periderm.</text>
      <biological_entity id="o7747" name="leaf-scar" name_original="leaf-scars" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition" src="d0_s6" value="fresh" value_original="fresh" />
      </biological_entity>
      <biological_entity id="o7748" name="periderm" name_original="periderm" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <relation from="o7747" id="r1615" name="with" negation="false" src="d0_s6" to="o7748" />
    </statement>
    <statement id="d0_s7">
      <text>Buds exposed, brown, globose, small, resinous, apex rounded;</text>
      <biological_entity id="o7749" name="bud" name_original="buds" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="exposed" value_original="exposed" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s7" value="globose" value_original="globose" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="coating" src="d0_s7" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o7750" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>basal scales long, narrow, isosceles triangular to spatulate, glabrous, resinous or not resinous, margins entire to rarely crenate, apex sharp-pointed or rounded.</text>
      <biological_entity constraint="basal" id="o7751" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s8" to="spatulate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o7752" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s8" to="rarely crenate" />
      </biological_entity>
      <biological_entity id="o7753" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="sharp-pointed" value_original="sharp-pointed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Leaves 1.1–2.5cm × 1.25–1.5mm, spiraled and turned upward, flexible;</text>
      <biological_entity id="o7754" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.1" from_unit="cm" name="length" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.25" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="turned" value_original="turned" />
        <character is_modifier="false" name="fragility" src="d0_s9" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cross-section flat, grooved adaxially, sometimes only slightly so;</text>
    </statement>
    <statement id="d0_s11">
      <text>odor camphorlike;</text>
      <biological_entity id="o7755" name="cross-section" name_original="cross-section" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s10" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="odor" src="d0_s11" value="camphorlike" value_original="camphorlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>abaxial surface with 3–5 stomatal rows on each side of midrib;</text>
      <biological_entity constraint="abaxial" id="o7756" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o7757" name="stomatal" name_original="stomatal" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
      <biological_entity id="o7758" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
      <biological_entity id="o7759" name="side" name_original="side" src="d0_s12" type="structure" />
      <biological_entity id="o7760" name="midrib" name_original="midrib" src="d0_s12" type="structure" />
      <relation from="o7756" id="r1616" name="with" negation="false" src="d0_s12" to="o7757" />
      <relation from="o7756" id="r1617" name="with" negation="false" src="d0_s12" to="o7758" />
      <relation from="o7757" id="r1618" name="on" negation="false" src="d0_s12" to="o7759" />
      <relation from="o7758" id="r1619" name="on" negation="false" src="d0_s12" to="o7759" />
      <relation from="o7759" id="r1620" name="part_of" negation="false" src="d0_s12" to="o7760" />
    </statement>
    <statement id="d0_s13">
      <text>adaxial surface light green to bluish green, usually glaucous, with 3–6 stomatal rows at midleaf, rows usually continuous to leaf base, usually more numerous toward leaf apex;</text>
      <biological_entity constraint="adaxial" id="o7761" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s13" to="bluish green" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s13" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o7762" name="stomatal" name_original="stomatal" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
      <biological_entity id="o7763" name="row" name_original="rows" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
      <biological_entity id="o7764" name="midleaf" name_original="midleaf" src="d0_s13" type="structure" />
      <biological_entity id="o7765" name="row" name_original="rows" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="continuous" value_original="continuous" />
        <character constraint="toward leaf apex" constraintid="o7767" is_modifier="false" modifier="usually" name="quantity" notes="" src="d0_s13" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o7766" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity constraint="leaf" id="o7767" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <relation from="o7761" id="r1621" name="with" negation="false" src="d0_s13" to="o7762" />
      <relation from="o7761" id="r1622" name="with" negation="false" src="d0_s13" to="o7763" />
      <relation from="o7762" id="r1623" name="at" negation="false" src="d0_s13" to="o7764" />
      <relation from="o7763" id="r1624" name="at" negation="false" src="d0_s13" to="o7764" />
    </statement>
    <statement id="d0_s14">
      <text>apex slightly notched to rounded;</text>
      <biological_entity id="o7768" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="slightly notched" name="shape" src="d0_s14" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>resin canals large, ± median, away from margins and midway between abaxial and adaxial epidermal layers.</text>
      <biological_entity constraint="resin" id="o7769" name="canal" name_original="canals" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="large" value_original="large" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s15" value="median" value_original="median" />
      </biological_entity>
      <biological_entity id="o7770" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character constraint="between abaxial adaxial epidermal, layers" constraintid="o7771, o7772" is_modifier="false" name="position" src="d0_s15" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity constraint="and abaxial adaxial" id="o7771" name="epidermal" name_original="epidermal" src="d0_s15" type="structure" />
      <biological_entity constraint="and abaxial adaxial" id="o7772" name="layer" name_original="layers" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Pollen cones at pollination purplish.</text>
      <biological_entity constraint="pollen" id="o7773" name="cone" name_original="cones" src="d0_s16" type="structure" />
      <biological_entity id="o7774" name="pollination" name_original="pollination" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="purplish" value_original="purplish" />
      </biological_entity>
      <relation from="o7773" id="r1625" name="at" negation="false" src="d0_s16" to="o7774" />
    </statement>
    <statement id="d0_s17">
      <text>Seed-cones cylindric, 5–10 × 3–3.5cm, dark purple-blue to grayish purple, sessile, apex rounded;</text>
      <biological_entity id="o7775" name="seed-cone" name_original="seed-cones" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s17" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s17" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="dark purple-blue" name="coloration" src="d0_s17" to="grayish purple" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o7776" name="apex" name_original="apex" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>scales ca. 1.5 × 2.5cm, densely pubescent;</text>
      <biological_entity id="o7777" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character name="length" src="d0_s18" unit="cm" value="1.5" value_original="1.5" />
        <character name="width" src="d0_s18" unit="cm" value="2.5" value_original="2.5" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>bracts included.</text>
      <biological_entity id="o7778" name="bract" name_original="bracts" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 5–7 × 2–3mm, body brown;</text>
      <biological_entity id="o7779" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s20" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s20" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7780" name="body" name_original="body" src="d0_s20" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s20" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>wing about 1.5 times as long as body, grayish brown;</text>
      <biological_entity id="o7781" name="wing" name_original="wing" src="d0_s21" type="structure">
        <character constraint="body" constraintid="o7782" is_modifier="false" name="length" src="d0_s21" value="1.5 times as long as body" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="grayish brown" value_original="grayish brown" />
      </biological_entity>
      <biological_entity id="o7782" name="body" name_original="body" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>cotyledons 3–6.</text>
      <biological_entity id="o7783" name="cotyledon" name_original="cotyledons" src="d0_s22" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s22" to="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Continental, subalpine coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine" modifier="continental" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–3600m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Ariz., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Rocky Mountain alpine fir</other_name>
  <other_name type="common_name">Rocky Mountain subalpine fir</other_name>
  <other_name type="common_name">corkbark fir</other_name>
  <discussion>Abies bifolia has been—and by many workers still is—included in synonymy under A. lasiocarpa or A. subalpina since about 1890, and A. subalpina under A. lasiocarpa since about the 1920s. Abies bifolia is distinct from A. lasiocarpa, however, in chemical tests on wood (H.S. Fraser and E.P. Swan 1972), lack of crystals in the ray parenchyma (R.W. Kennedy et al. 1968), lack of lasiocarpenonol (J.F. Manville and A.S. Tracey 1989), and distinct terpene patterns (R.S. Hunt and E.von Rudloff 1979). Abies bifolia also tends to have slightly shorter and fewer prominently notched leaves than A. lasiocarpa. The two are clearly separated by the color of their periderm and by the shape of their basal bud scales. These firs may be more distinct than the pairs A. balsamea -- A. fraseri and A. procera -- A. magnifica. A north-south transect, however, from south central Yukon to northern Washington yielded introgressed trees possessing characteristics of both A. lasiocarpa and A. bifolia, recalling the interior spruce (Canadian Forestry Service 1983), which has characteristics of both Picea glauca and P. engelmannii. These trees can similarly be called interior subalpine fir, i.e., A. bifolia × lasiocarpa. Both A. lasiocarpa and A. bifolia need comparative morphologic studies.</discussion>
  <discussion>Isolated southern populations of Abies bifolia may also have unique characteristics. The taxonomy of corkbark fir, treated by some as A. lasiocarpa var. arizonica (Merriam) Lemmon, is uncertain. This taxon should probably be a segregate of A. bifolia, not A. lasiocarpa, a disposition that requires a thorough morphologic and chemical reappraisal, especially since the work of E.Zavarin et al. (1970) suggested that populations south of Wyoming may have unique terpene patterns. In north central Alberta, A. bifolia introgresses with A. balsamea (R.S. Hunt and E.von Rudloff 1974; E.H. Moss 1953).</discussion>
  
</bio:treatment>