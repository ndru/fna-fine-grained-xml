<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard P. Wunderlin</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bartlett" date="unknown" rank="family">cupressaceae</taxon_name>
    <taxon_name authority="Ventenat" date="1808" rank="genus">Callitris</taxon_name>
    <place_of_publication>
      <publication_title>Dec. Gen. Nov.</publication_title>
      <place_in_publication>10. 1808</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cupressaceae;genus Callitris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek callos, beautiful, and treis, three, referring to the beauty of the plants and the three-whorled leaves and cone scales</other_info_on_name>
    <other_info_on_name type="fna_id">105146</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees evergreen.</text>
      <biological_entity id="o11824" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets angled or furrowed-cylindrical, variously oriented.</text>
      <biological_entity id="o11826" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="furrowed-cylindrical" value_original="furrowed-cylindrical" />
        <character is_modifier="false" modifier="variously" name="orientation" src="d0_s1" value="oriented" value_original="oriented" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in whorls of 3–5.</text>
      <biological_entity id="o11827" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11828" name="whorl" name_original="whorls" src="d0_s2" type="structure" />
      <relation from="o11827" id="r2548" modifier="of 3-5" name="in" negation="false" src="d0_s2" to="o11828" />
    </statement>
    <statement id="d0_s3">
      <text>Adult leaves scalelike, appressed, abaxial surface keeled or rounded, free portion to 1 mm, abaxial glands absent.</text>
      <biological_entity id="o11829" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="adult" value_original="adult" />
        <character is_modifier="false" name="shape" src="d0_s3" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11830" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11831" name="portion" name_original="portion" src="d0_s3" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11832" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pollen cones solitary or in small clusters, with 4–15 whorls of sporophylls, each with 2–4 pollen-sacs.</text>
      <biological_entity constraint="pollen" id="o11833" name="cone" name_original="cones" src="d0_s4" type="structure">
        <character constraint="in small clusters" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="in small clusters" value_original="in small clusters" />
      </biological_entity>
      <biological_entity id="o11834" name="whorl" name_original="whorls" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o11835" name="sporophyll" name_original="sporophylls" src="d0_s4" type="structure" />
      <biological_entity id="o11836" name="pollen-sac" name_original="pollen-sacs" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <relation from="o11833" id="r2549" name="with" negation="false" src="d0_s4" to="o11834" />
      <relation from="o11834" id="r2550" name="part_of" negation="false" src="d0_s4" to="o11835" />
      <relation from="o11833" id="r2551" name="with" negation="false" src="d0_s4" to="o11836" />
    </statement>
    <statement id="d0_s5">
      <text>Seed-cones maturing in 1–2 years, of 1–2 sizes, often remaining unopened for many years, ovoid or globose, 1–3.5 cm;</text>
      <biological_entity id="o11837" name="cone-seed" name_original="seed-cones" src="d0_s5" type="structure">
        <character constraint="in years" constraintid="o11838" is_modifier="false" name="life_cycle" src="d0_s5" value="maturing" value_original="maturing" />
        <character constraint="for years" constraintid="o11839" is_modifier="false" modifier="of 1-2 sizes; often" name="architecture" notes="" src="d0_s5" value="unopened" value_original="unopened" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11838" name="year" name_original="years" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o11839" name="year" name_original="years" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scales persistent, in 2 equally inserted whorls of 3 (–4), valvate, rhombic-deltate, basifixed, thick and woody.</text>
      <biological_entity id="o11840" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="of 3(-4)" name="arrangement_or_dehiscence" notes="" src="d0_s6" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rhombic-deltate" value_original="rhombic-deltate" />
        <character is_modifier="false" name="fixation" src="d0_s6" value="basifixed" value_original="basifixed" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s6" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o11841" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" modifier="equally" name="position" src="d0_s6" value="inserted" value_original="inserted" />
      </biological_entity>
      <relation from="o11840" id="r2552" name="in" negation="false" src="d0_s6" to="o11841" />
    </statement>
    <statement id="d0_s7">
      <text>Seeds 2–9 per scale, round or 3-angled, broadly 1–3-winged;</text>
      <biological_entity id="o11842" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per scale" constraintid="o11843" from="2" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" name="shape" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s7" value="1-3-winged" value_original="1-3-winged" />
      </biological_entity>
      <biological_entity id="o11843" name="scale" name_original="scale" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>cotyledons 2.</text>
      <biological_entity id="o11844" name="cotyledon" name_original="cotyledons" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Australia, and New Caledonia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="and New Caledonia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Cypress-pine</other_name>
  <discussion>Species 16 (1 naturalized in the flora).</discussion>
  <references>
    <reference>Blake, S. T. 1959. New or noteworthy plants, chiefly from Queensland, l. Proc. Roy. Soc. Queensland. 70(6): 33–46.</reference>
    <reference>Garden, J. 1957. A revision of the genus Callitris Vent. Contr. New South Wales Natl. Herb. 2(5): 363–392.</reference>
    <reference>Thompson, J. 1961. Cupressaceae. Contr. New South Wales Natl. Herb., Fl. Ser. 1/18: 46–55.</reference>
    <reference>Thompson, J. and L. A. S. Johnson. 1986. Callitris glaucophylla, Australia's 'white cypress pine'—A new name for an old species. Telopea 2: 731–736.</reference>
  </references>
  
</bio:treatment>