<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>R. David Whetstone, T. A. Atkinson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. S. Presl" date="unknown" rank="family">osmundaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Osmunda</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1063. 1753; Gen. Pl. ed. 5, 484, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family osmundaceae;genus Osmunda</taxon_hierarchy>
    <other_info_on_name type="etymology">Saxon, Osmunder, name for Thor, god of war</other_info_on_name>
    <other_info_on_name type="fna_id">123343</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial.</text>
      <biological_entity id="o3776" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping;</text>
      <biological_entity id="o3777" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tips often somewhat erect.</text>
      <biological_entity id="o3778" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often somewhat" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves dimorphic;</text>
      <biological_entity id="o3779" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s3" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>fertile leaves erect, often notably smaller than sterile leaves in length and width.</text>
      <biological_entity id="o3780" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character constraint="than sterile leaves" constraintid="o3781" is_modifier="false" name="length" src="d0_s4" value="in" value_original="in" />
      </biological_entity>
      <biological_entity id="o3781" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blades 1–2-pinnate;</text>
      <biological_entity id="o3782" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-2-pinnate" value_original="1-2-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pinnae monomorphic to dimorphic, pinnatifid or pinnate.</text>
      <biological_entity id="o3783" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="growth_form" src="d0_s6" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, tropical and temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="tropical and temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Species 10 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile leaves with pinnae monomorphic, all spore-bearing; tuft of hairs persistent on abaxial surface of pinnae near base.</description>
      <determination>1 Osmunda cinnamomea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile leaves with pinnae dimorphic, some spore-bearing, some not; tuft of hairs absent on abaxial surface of pinnae near base.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sterile leaves 2-pinnate with pinnules sessile.</description>
      <determination>4 Osmunda ruggii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sterile leaves pinnate-pinnatifid or 2-pinnate with pinnules stalked.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fertile pinnae apical, sterile leaves 2-pinnate.</description>
      <determination>3a Osmunda regalis var. spectabilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fertile pinnae medial, sterile leaves pinnate-pinnatifid.</description>
      <determination>2 Osmunda claytoniana</determination>
    </key_statement>
  </key>
</bio:treatment>