<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Blume" date="1828" rank="genus">Arachniodes</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl. Javae</publication_title>
      <place_in_publication>2: 241. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus Arachniodes</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek arachnion, spider's web, and -odes, having the form or nature of; it has been suggested that Blume saw fungal hyphae or spider webs on his original material</other_info_on_name>
    <other_info_on_name type="fna_id">102399</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial.</text>
      <biological_entity id="o8153" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems moderately long to short-creeping, stolons absent.</text>
      <biological_entity id="o8154" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
      </biological_entity>
      <biological_entity id="o8155" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, evergreen.</text>
      <biological_entity id="o8156" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s2" value="evergreen" value_original="evergreen" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole ± as long as blade, base not swollen;</text>
      <biological_entity id="o8157" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o8158" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o8159" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o8157" id="r1712" name="as long as" negation="false" src="d0_s3" to="o8158" />
    </statement>
    <statement id="d0_s4">
      <text>vascular-bundles more than 3, arranged in an arc, ± round in cross-section.</text>
      <biological_entity id="o8160" name="vascular-bundle" name_original="vascular-bundles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" upper_restricted="false" />
        <character constraint="in arc" constraintid="o8161" is_modifier="false" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character constraint="in cross-section" constraintid="o8162" is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o8161" name="arc" name_original="arc" src="d0_s4" type="structure" />
      <biological_entity id="o8162" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly deltate or pentagonal, 2–3-pinnate-pinnatifid, gradually to abruptly reduced distally to pinnate or pinnatifid apex, papery to somewhat leathery.</text>
      <biological_entity id="o8163" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-3-pinnate-pinnatifid" value_original="2-3-pinnate-pinnatifid" />
        <character constraint="to apex" constraintid="o8164" is_modifier="false" modifier="gradually to abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="texture" notes="" src="d0_s5" value="papery to somewhat" value_original="papery to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o8164" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="pinnate" value_original="pinnate" />
        <character is_modifier="true" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae not articulate to rachis, segment margins and especially apex spinulose;</text>
      <biological_entity id="o8165" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character constraint="to rachis" constraintid="o8166" is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o8166" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity constraint="segment" id="o8167" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spinulose" value_original="spinulose" />
      </biological_entity>
      <biological_entity constraint="segment" id="o8168" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spinulose" value_original="spinulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal pinnae largest, petiolulate, inequilateral with basal basiscopic pinnule much larger and more elongate than more distal pinnules;</text>
      <biological_entity constraint="proximal" id="o8169" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="largest" value_original="largest" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolulate" value_original="petiolulate" />
        <character constraint="with basal pinnae" constraintid="o8170" is_modifier="false" name="shape" src="d0_s7" value="inequilateral" value_original="inequilateral" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8170" name="pinna" name_original="pinnae" src="d0_s7" type="structure" />
      <biological_entity id="o8171" name="pinnule" name_original="pinnule" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s7" value="larger" value_original="larger" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8172" name="pinnule" name_original="pinnules" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>costae adaxially grooved, grooves continuous from rachis to costae to costules;</text>
      <biological_entity id="o8173" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s8" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o8174" name="groove" name_original="grooves" src="d0_s8" type="structure">
        <character constraint="from rachis" constraintid="o8175" is_modifier="false" name="architecture" src="d0_s8" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o8175" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o8176" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <biological_entity id="o8177" name="costule" name_original="costules" src="d0_s8" type="structure" />
      <relation from="o8175" id="r1713" name="to" negation="false" src="d0_s8" to="o8176" />
      <relation from="o8176" id="r1714" name="to" negation="false" src="d0_s8" to="o8177" />
    </statement>
    <statement id="d0_s9">
      <text>indument of hairlike scales abaxially, absent adaxially.</text>
      <biological_entity id="o8178" name="indument" name_original="indument" src="d0_s9" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="false" modifier="adaxially" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8179" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o8178" id="r1715" name="part_of" negation="false" src="d0_s9" to="o8179" />
    </statement>
    <statement id="d0_s10">
      <text>Veins free, forked.</text>
      <biological_entity id="o8180" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s10" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori in 1 row between midrib and margin, round;</text>
      <biological_entity id="o8181" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="between midrib and margin" constraintid="o8183-o8184" id="o8182" name="row" name_original="row" src="d0_s11" type="structure" constraint_original="between  midrib and  margin, ">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8183" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <biological_entity id="o8184" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <relation from="o8181" id="r1716" name="in" negation="false" src="d0_s11" to="o8182" />
    </statement>
    <statement id="d0_s12">
      <text>indusia round-reniform, attached at narrow sinus, persistent.</text>
      <biological_entity id="o8185" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="round-reniform" value_original="round-reniform" />
        <character constraint="at sinus" constraintid="o8186" is_modifier="false" name="fixation" src="d0_s12" value="attached" value_original="attached" />
        <character is_modifier="false" name="duration" notes="" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o8186" name="sinus" name_original="sinus" src="d0_s12" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores brownish, rugate or tuberculate, sometimes spiny.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 41.</text>
      <biological_entity id="o8187" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="relief" src="d0_s13" value="rugate" value_original="rugate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s13" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity constraint="x" id="o8188" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="41" value_original="41" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropics and subtropics, mostly in e Asia and Pacific Islands, a few in Africa, ca. 4 in Mexico, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropics and subtropics" establishment_means="native" />
        <character name="distribution" value="mostly in e Asia and Pacific Islands" establishment_means="native" />
        <character name="distribution" value="a few in Africa" establishment_means="native" />
        <character name="distribution" value="ca. 4 in Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <other_name type="common_name">East Indian holly fern</other_name>
  <discussion>Species ca. 50 (1 in the flora naturalized from Asia).</discussion>
  <references>
    <reference>Ching, R. C. 1934. A revision of the compound leaved Polysticha and other related species in the continental Asia including Japan and Formosa. Sinensia 5: 23–91.</reference>
    <reference>Gordon, J. E. 1981. Arachniodes simplicior new to South Carolina and the United States. Amer. Fern J. 71: 65–68.</reference>
    <reference>Tindale, M. D. 1960. Pteridophyta of south eastern Australia. Contr. New South Wales Natl. Herb., Fl. Ser. 211: 47–78.</reference>
  </references>
  
</bio:treatment>