<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Lycopodium</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1100. 1753; Gen. Pl. ed. 5, 486, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus Lycopodium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lykos, wolf, and pous, podes, foot; in reference to the resemblance of the branch tips to a wolf's paw</other_info_on_name>
    <other_info_on_name type="fna_id">119159</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mainly trailing on ground.</text>
      <biological_entity id="o18265" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="on ground" constraintid="o18266" is_modifier="false" modifier="mainly" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18266" name="ground" name_original="ground" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots emerging from point of origin on underside of main-stems.</text>
      <biological_entity id="o18267" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o18268" name="point" name_original="point" src="d0_s1" type="structure" />
      <biological_entity id="o18269" name="underside" name_original="underside" src="d0_s1" type="structure" />
      <biological_entity id="o18270" name="main-stem" name_original="main-stems" src="d0_s1" type="structure" />
      <relation from="o18267" id="r4008" name="emerging from" negation="false" src="d0_s1" to="o18268" />
      <relation from="o18267" id="r4009" name="emerging from" negation="false" src="d0_s1" to="o18269" />
      <relation from="o18267" id="r4010" name="emerging from" negation="false" src="d0_s1" to="o18270" />
    </statement>
    <statement id="d0_s2">
      <text>Horizontal stems on substrate surface or subterranean, long-creeping.</text>
      <biological_entity id="o18271" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="location" src="d0_s2" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
      <biological_entity id="o18272" name="surface" name_original="surface" src="d0_s2" type="structure" />
      <relation from="o18271" id="r4011" name="on" negation="false" src="d0_s2" to="o18272" />
    </statement>
    <statement id="d0_s3">
      <text>Upright shoots scattered along horizontal stem, 5–16 mm diam., round or flat in cross-section, unbranched or with 1–4 lateral branchlets.</text>
      <biological_entity id="o18273" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="upright" value_original="upright" />
        <character constraint="along stem" constraintid="o18274" is_modifier="false" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" notes="" src="d0_s3" to="16" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character constraint="in cross-section" constraintid="o18275" is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="with 1-4 lateral branchlets" value_original="with 1-4 lateral branchlets" />
      </biological_entity>
      <biological_entity id="o18274" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o18275" name="cross-section" name_original="cross-section" src="d0_s3" type="structure" />
      <biological_entity constraint="lateral" id="o18276" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <relation from="o18273" id="r4012" name="with" negation="false" src="d0_s3" to="o18276" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves not imbricate, linear to linear-lanceolate;</text>
      <biological_entity id="o18277" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaves on horizontal stems scattered, appressed, membranous;</text>
      <biological_entity id="o18278" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o18279" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o18278" id="r4013" name="on" negation="false" src="d0_s5" to="o18279" />
    </statement>
    <statement id="d0_s6">
      <text>leaves on lateral branchlets mostly 6-ranked or more, monomorphic with few exceptions, appressed, ascending to spreading, margins entire to dentate.</text>
      <biological_entity id="o18280" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="with exceptions" constraintid="o18282" is_modifier="false" name="architecture" notes="" src="d0_s6" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s6" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18281" name="branchlet" name_original="branchlets" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s6" value="6-ranked" value_original="6-ranked" />
      </biological_entity>
      <biological_entity id="o18282" name="exception" name_original="exceptions" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o18283" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s6" to="dentate" />
      </biological_entity>
      <relation from="o18280" id="r4014" name="on" negation="false" src="d0_s6" to="o18281" />
    </statement>
    <statement id="d0_s7">
      <text>Gemmiferous branchlets and gemmae absent.</text>
      <biological_entity id="o18284" name="branchlet" name_original="branchlets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="gemmiferous" value_original="gemmiferous" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18285" name="gemma" name_original="gemmae" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="gemmiferous" value_original="gemmiferous" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Strobili single and sessile or multiple and pedunculate, apex blunt to acute;</text>
      <biological_entity id="o18286" name="strobilus" name_original="strobili" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity id="o18287" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peduncle, when present, conspicuously leafy;</text>
      <biological_entity id="o18288" name="peduncle" name_original="peduncle" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="when present; conspicuously" name="architecture" src="d0_s9" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sporophylls extremely reduced, much shorter than peduncle or stem-leaves.</text>
      <biological_entity id="o18289" name="sporophyll" name_original="sporophylls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="extremely" name="size" src="d0_s10" value="reduced" value_original="reduced" />
        <character constraint="than peduncle or stem-leaves" constraintid="o18290, o18291" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o18290" name="peduncle" name_original="peduncle" src="d0_s10" type="structure" />
      <biological_entity id="o18291" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Sporangia reniform.</text>
      <biological_entity id="o18292" name="sporangium" name_original="sporangia" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="reniform" value_original="reniform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores reticulate, sides at equator convex, angles acute.</text>
      <biological_entity id="o18293" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s12" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o18294" name="side" name_original="sides" src="d0_s12" type="structure">
        <character constraint="at equator" is_modifier="false" name="shape" src="d0_s12" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o18295" name="angle" name_original="angles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Gametophytes nonphotosynthetic, mycorrhizal, subterranean, flat and irregularly button-shaped, with ring meristem around circumference.</text>
      <biological_entity constraint="ring" id="o18297" name="meristem" name_original="meristem" src="d0_s13" type="structure" />
      <relation from="o18296" id="r4015" name="with" negation="false" src="d0_s13" to="o18297" />
    </statement>
    <statement id="d0_s14">
      <text>x = 34.</text>
      <biological_entity id="o18296" name="gametophyte" name_original="gametophytes" src="d0_s13" type="structure">
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s13" value="mycorrhizal" value_original="mycorrhizal" />
        <character is_modifier="false" name="location" src="d0_s13" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s13" value="button--shaped" value_original="button--shaped" />
      </biological_entity>
      <biological_entity constraint="x" id="o18298" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mainly temperate and subarctic.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mainly temperate and subarctic" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Club-moss</other_name>
  <discussion>In striking contrast to Diphasiastrum, Huperzia, and Lycopodiella, interspecific hybridization is practically unknown in Lycopodium. Many of the species now recognized in Lycopodium have been segregated from Lycopodium clavatum, L. annotinum, and L. jussiaei Desvaux ex Poiret. The three groups given in the key below should probably be treated as subgenera.</discussion>
  <discussion>Species 15–25 (6 in the flora).</discussion>
  <references>
    <reference>Hickey, R. J. 1977. The Lycopodium obscurum complex in North America. Amer. Fern J. 67: 45–49.</reference>
    <reference>Wagner, W. H. Jr., J. M. Beitel, and R. C. Moran. 1989. Lycopodium hickeyi: A new species of North American clubmoss. Amer. Fern J. 79: 119–121.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Strobili pedunculate; upright shoots with 2–5 branches, not treelike; leaves with hair tips 1–4 mm (these may fall off early, but remain at shoot apices) (L. clavatum group).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Strobili sessile; upright shoots either unbranched or much branched to produce treelike habit; leaves lacking hair tips.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Strobili mostly solitary on peduncle, if paired then nearly lacking pedicels; leaves 3–5 mm, ascending to appressed; branches 2–3(–4), mostly upright.</description>
      <determination>2 Lycopodium lagopus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Strobili 2–5, borne on loosely alternate pedicels, 0.5–0.8 cm; leaves 4–6 mm, spreading to somewhat ascending; branches 3–6, mostly oblique or spreading.</description>
      <determination>3 Lycopodium clavatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Strobili single at top of upright shoot; shoot unbranched or branched 1–2 times; horizontal stems on substrate surface (L. annotinum group).</description>
      <determination>1 Lycopodium annotinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Strobili 1–7 at top of many-branched, upright, treelike shoot; horizontal stems subterranean (L. dendroideum group).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lateral shoots flat in cross section, leaves unequal in size, lateral leaves spreading and twisted, adaxial surfaces facing upward, proximal leaves much reduced; leaves on main axis dark green, tightly appressed.</description>
      <determination>6 Lycopodium obscurum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lateral shoots round in cross section, leaves equal in size, none twisted, adaxial leaf surfaces all facing stem, proximal leaves not reduced; leaves on main axis light or dark green, spreading or appressed.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf ranks 1 on upperside of lateral branch, 2 on each side, and 1 on underside; leaves of main axis below branches dark green, tightly appressed, soft to touch.</description>
      <determination>5 Lycopodium hickeyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf ranks 2 on top of lateral branch, 1 on each side, and 2 on underside; leaves of main axis below branches pale green, spreading, prickly to touch.</description>
      <determination>4 Lycopodium dendroideum</determination>
    </key_statement>
  </key>
</bio:treatment>