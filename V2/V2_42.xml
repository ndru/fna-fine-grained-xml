<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Newman" date="1851" rank="genus">gymnocarpium</taxon_name>
    <taxon_name authority="(Hoffmann) Newman" date="1851" rank="species">robertianum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologist</publication_title>
      <place_in_publication>4: app. 24. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus gymnocarpium;species robertianum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200003907</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Hoffmann" date="unknown" rank="species">robertianum</taxon_name>
    <place_of_publication>
      <publication_title>Deutschl. Fl.</publication_title>
      <place_in_publication>2: add. et emend. 10. 1795</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species robertianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Hoffmann) C. Christensen" date="unknown" rank="species">robertiana</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species robertiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phegopteris</taxon_name>
    <taxon_name authority="(Hoffmann) Fée" date="unknown" rank="species">robertianum</taxon_name>
    <taxon_hierarchy>genus Phegopteris;species robertianum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="(Hoffmann) Slosson" date="unknown" rank="species">robertiana</taxon_name>
    <taxon_hierarchy>genus Thelypteris;species robertiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–2 mm diam.;</text>
      <biological_entity id="o17000" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 2–4 mm.</text>
      <biological_entity id="o17001" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fertile leaves usually 10–52 cm.</text>
      <biological_entity id="o17002" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="52" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 5–33 cm, with numerous glandular-hairs distally;</text>
      <biological_entity id="o17003" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="33" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17004" name="glandular-hair" name_original="glandular-hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o17003" id="r3734" name="with" negation="false" src="d0_s3" to="o17004" />
    </statement>
    <statement id="d0_s4">
      <text>scales 2–6 mm.</text>
      <biological_entity id="o17005" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly deltate, 2–3-pinnate-pinnatifid, 5–19 cm, usually firm and robust, abaxial surface moderately to densely glandular, rachis densely glandular, adaxial surface moderately glandular.</text>
      <biological_entity id="o17006" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-3-pinnate-pinnatifid" value_original="2-3-pinnate-pinnatifid" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="19" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s5" value="firm" value_original="firm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17007" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o17008" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17009" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinna apex acute.</text>
      <biological_entity constraint="pinna" id="o17010" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Proximal pinnae 3–13 cm, ± perpendicular to rachis, basiscopic pinnules ± perpendicular to costa;</text>
      <biological_entity constraint="proximal" id="o17011" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="13" to_unit="cm" />
        <character constraint="to rachis" constraintid="o17012" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o17012" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o17013" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character constraint="to costa" constraintid="o17014" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o17014" name="costa" name_original="costa" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>basal basiscopic pinnules either sessile or stalked, pinnate-pinnatifid or pinnatifid, if sessile then with basal basiscopic pinnulet usually shorter than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o17015" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o17016" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnatifid" value_original="pinnatifid" />
        <character constraint="with basal pinnae" constraintid="o17017" is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17017" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o17018" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnulet" constraintid="o17019" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o17019" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d basal basiscopic pinnule sometimes stalked, if sessile then with basal basiscopic pinnulet shorter than or equaling adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o17020" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o17021" name="pinnule" name_original="pinnule" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17022" is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17022" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o17023" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o17024" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17024" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>basal acroscopic pinnule sometimes stalked, if sessile then with basal basiscopic pinnulet shorter than or equaling adjacent pinnulet.</text>
      <biological_entity constraint="basal" id="o17025" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o17026" name="pinnule" name_original="pinnule" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17027" is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17027" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o17028" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o17029" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17029" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pinnae of 2d pair usually stalked, if sessile then with basal basiscopic pinnule usually shorter than adjacent pinnule and equaling basal acroscopic pinnule;</text>
      <biological_entity id="o17030" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17032" is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17031" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <biological_entity constraint="basal" id="o17032" name="pinna" name_original="pinnae" src="d0_s11" type="structure" />
      <biological_entity id="o17033" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnule and equaling basal acroscopic pinnule ;" constraintid="o17035" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o17035" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o17030" id="r3735" name="part_of" negation="false" src="d0_s11" to="o17031" />
    </statement>
    <statement id="d0_s12">
      <text>basal acroscopic pinnule shorter than adjacent pinnule, apex often entire, rounded.</text>
      <biological_entity constraint="basal" id="o17036" name="pinna" name_original="pinnae" src="d0_s12" type="structure" />
      <biological_entity id="o17037" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
        <character constraint="than adjacent pinnule" constraintid="o17038" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17038" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o17039" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pinnae of 3d pair usually sessile with basal basiscopic pinnule shorter than adjacent pinnule and equaling basal acroscopic pinnule;</text>
      <biological_entity id="o17040" name="pinna" name_original="pinnae" src="d0_s13" type="structure">
        <character constraint="with basal pinnae" constraintid="o17042" is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17041" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o17042" name="pinna" name_original="pinnae" src="d0_s13" type="structure" />
      <biological_entity id="o17043" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnule and equaling basal acroscopic pinnule" constraintid="o17045" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17045" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o17040" id="r3736" name="part_of" negation="false" src="d0_s13" to="o17041" />
    </statement>
    <statement id="d0_s14">
      <text>basal acroscopic pinnule equaling or shorter than adjacent pinnule.</text>
      <biological_entity constraint="basal" id="o17046" name="pinna" name_original="pinnae" src="d0_s14" type="structure" />
      <biological_entity id="o17047" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o17048" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17048" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ultimate segments of proximal pinnae oblong, entire to slightly crenate, apex entire, rounded.</text>
      <biological_entity constraint="ultimate" id="o17049" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s15" to="slightly crenate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17050" name="pinna" name_original="pinnae" src="d0_s15" type="structure" />
      <biological_entity id="o17051" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o17049" id="r3737" name="part_of" negation="false" src="d0_s15" to="o17050" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 34–39 µm. 2n = 160.</text>
      <biological_entity id="o17052" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="34" from_unit="um" name="some_measurement" src="d0_s16" to="39" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17053" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="160" value_original="160" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous substrates, limestone pavement, outcrops, and cliffs, Thuja swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="substrates" modifier="calcareous" />
        <character name="habitat" value="limestone pavement" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="thuja swamps" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr. (Nfld.), Ont., Que.; Iowa, Mich., Minn., Wis.; Europe; Asia in Caucasus Mountains.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia in Caucasus Mountains" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Limestone oak fern</other_name>
  <other_name type="common_name">gymnocarpe de robert</other_name>
  <discussion>Gymnocarpium robertianum occurs in numerous localities in eastern Canada, especially in Ontario and Quebec where it is widely distributed; populations are small. Hybrids with G. robertianum are extremely rare. Gymnocarpium × heterosporum W. H. Wagner, a putative triploid hybrid between G. robertianum and G. appalachianum, is known only from one county in Pennsylvania (plants now extirpated, K. M. Pryer 1992). Gymnocarpium × achriosporum Sarvela, a putative tetraploid hybrid between G. robertianum and G. dryopteris, is known only from Sweden and two localities in Quebec. Both hybrids resemble G. robertianum in their leaf morphology and dense glandularity but have black, malformed spores.</discussion>
  
</bio:treatment>