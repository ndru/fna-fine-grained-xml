<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Cathy A. Paris</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">Adiantum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1094. 1753; Gen. Pl. ed 5, 485. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus Adiantum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek adiantos, unwetted, for the glabrous leaves, which shed raindrops</other_info_on_name>
    <other_info_on_name type="fna_id">100603</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial or on rock.</text>
      <biological_entity id="o16728" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="on rock" is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="on rock" value_original="on rock" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short to long-creeping or suberect, branched;</text>
      <biological_entity id="o16729" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="long-creeping" value_original="long-creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales deep tawny yellow to dark reddish-brown [black], concolored or bicolored, linear-lanceolate to lanceolate, margins entire, erose-ciliate, or minutely dentate.</text>
      <biological_entity id="o16730" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="deep" value_original="deep" />
        <character char_type="range_value" from="tawny yellow" name="coloration" src="d0_s2" to="dark reddish-brown concolored or bicolored" />
        <character char_type="range_value" from="tawny yellow" name="coloration" src="d0_s2" to="dark reddish-brown concolored or bicolored" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o16731" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="erose-ciliate" value_original="erose-ciliate" />
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="erose-ciliate" value_original="erose-ciliate" />
        <character is_modifier="false" modifier="minutely" name="architecture" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic to somewhat dimorphic, densely clustered to closely spaced [distant], 15–110 cm.</text>
      <biological_entity id="o16732" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="somewhat" name="growth_form" src="d0_s3" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="densely clustered" name="arrangement" src="d0_s3" to="closely spaced" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="110" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole chestnut-brown to dark purple or blackish, with single groove adaxially, glabrous, hispid, or strigose, with 1 or 2 vascular-bundles.</text>
      <biological_entity id="o16733" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="chestnut-brown" name="coloration" src="d0_s4" to="dark purple or blackish" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o16734" name="groove" name_original="groove" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
      <relation from="o16733" id="r3668" name="with" negation="false" src="d0_s4" to="o16734" />
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate, ovate, trowel-shaped, or fan-shaped, 1–4 (–9) -pinnate proximally, membranaceous to papery, both surfaces commonly glabrous (2 species with scattered hairs), adaxially dull or shiny, not striate;</text>
      <biological_entity id="o16735" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="trowel--shaped" value_original="trowel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="trowel--shaped" value_original="trowel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s5" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s5" value="1-4(-9)-pinnate" value_original="1-4(-9)-pinnate" />
        <character char_type="range_value" from="membranaceous" name="texture" src="d0_s5" to="papery" />
      </biological_entity>
      <biological_entity id="o16736" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="commonly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s5" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis straight or flexuous.</text>
      <biological_entity id="o16737" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s6" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ultimate segments subsessile to short-stalked (stalks terminating in cupulelike swelling at base of pinna in A. tenerum), round, fan-shaped, rhombic, or oblong, 3–29 mm wide;</text>
      <biological_entity constraint="ultimate" id="o16738" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="subsessile" name="architecture" src="d0_s7" to="short-stalked" />
        <character is_modifier="false" name="shape" src="d0_s7" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s7" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>base truncate to cuneate, free from costa;</text>
      <biological_entity id="o16739" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s8" to="cuneate" />
        <character constraint="from costa" constraintid="o16740" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o16740" name="costa" name_original="costa" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>stalk dark, often lustrous;</text>
      <biological_entity id="o16741" name="stalk" name_original="stalk" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark" value_original="dark" />
        <character is_modifier="false" modifier="often" name="reflectance" src="d0_s9" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>fertile segments with marginal lobes recurved to form false indusia.</text>
      <biological_entity id="o16742" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o16743" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="false" id="o16744" name="indusium" name_original="indusia" src="d0_s10" type="structure" />
      <relation from="o16742" id="r3669" name="with" negation="false" src="d0_s10" to="o16743" />
    </statement>
    <statement id="d0_s11">
      <text>Veins of ultimate segments conspicuous, free, ± dichotomously forking near base and well above segment base [anastomosing in a few tropical species], parallel distally.</text>
      <biological_entity id="o16745" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character constraint="near base" constraintid="o16747" is_modifier="false" modifier="more or less dichotomously" name="architecture_or_shape" src="d0_s11" value="forking" value_original="forking" />
        <character is_modifier="false" modifier="distally" name="arrangement" notes="" src="d0_s11" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o16746" name="segment" name_original="segments" src="d0_s11" type="structure" />
      <biological_entity id="o16747" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity constraint="segment" id="o16748" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o16745" id="r3670" name="part_of" negation="false" src="d0_s11" to="o16746" />
      <relation from="o16745" id="r3671" modifier="well" name="above" negation="false" src="d0_s11" to="o16748" />
    </statement>
    <statement id="d0_s12">
      <text>False indusia light gray-green or brown to dark-brown, narrow, 0.6–1 mm wide, marginal, concealing sporangia until sporangia dehisce.</text>
      <biological_entity constraint="false" id="o16749" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s12" to="dark-brown" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="marginal" value_original="marginal" />
      </biological_entity>
      <biological_entity id="o16750" name="sporangium" name_original="sporangia" src="d0_s12" type="structure" />
      <biological_entity id="o16751" name="sporangium" name_original="sporangia" src="d0_s12" type="structure" />
      <relation from="o16749" id="r3672" name="concealing" negation="false" src="d0_s12" to="o16750" />
      <relation from="o16749" id="r3673" name="until" negation="false" src="d0_s12" to="o16751" />
    </statement>
    <statement id="d0_s13">
      <text>Sporangia submarginal, borne along or sometimes also between veins on abaxial surface of false indusium, paraphyses and glands absent.</text>
      <biological_entity id="o16752" name="sporangium" name_original="sporangia" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="submarginal" value_original="submarginal" />
      </biological_entity>
      <biological_entity id="o16753" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity constraint="abaxial" id="o16754" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="false" id="o16755" name="indusium" name_original="indusium" src="d0_s13" type="structure" />
      <biological_entity constraint="false" id="o16756" name="paraphyse" name_original="paraphyses" src="d0_s13" type="structure" />
      <biological_entity id="o16757" name="gland" name_original="glands" src="d0_s13" type="structure" />
      <relation from="o16752" id="r3674" name="borne along" negation="false" src="d0_s13" to="o16753" />
      <relation from="o16752" id="r3675" name="on" negation="false" src="d0_s13" to="o16754" />
      <relation from="o16754" id="r3676" name="part_of" negation="false" src="d0_s13" to="o16755" />
      <relation from="o16754" id="r3677" name="part_of" negation="false" src="d0_s13" to="o16756" />
      <relation from="o16754" id="r3678" name="part_of" negation="false" src="d0_s13" to="o16757" />
    </statement>
    <statement id="d0_s14">
      <text>Spores yellow or yellowish-brown, tetrahedral-globose, trilete, rugulate to rugose or tuberculate, equatorial-ridge absent.</text>
      <biological_entity id="o16758" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="tetrahedral-globose" value_original="tetrahedral-globose" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="trilete" value_original="trilete" />
        <character char_type="range_value" from="rugulate" name="relief" src="d0_s14" to="rugose or tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>x = 29, 30.</text>
      <biological_entity id="o16759" name="equatorial-ridge" name_original="equatorial-ridge" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o16760" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="29" value_original="29" />
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide except at latitudes greater than 60°.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide except at latitudes greater than 60°" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Maidenhair fern</other_name>
  <discussion>Most diverse in Andean South America, Adiantum is primarily a tropical genus; of the nine species occurring in the flora, A. melanoleucum, A. tenerum, and A. tricholepis are strictly subtropical. Adiantum hispidulum occurs only as an escape from cultivation. The genus is absent from dry areas in the interior of the continent.</discussion>
  <discussion>Adiantum is a very clearly circumscribed genus of ferns, the character state "sporangia borne on abaxial surface of false indusium" being both necessary and sufficient to define it. Within this large and widespread genus, however, species relationships are mostly unknown. An evolutionary classification of the group is indeed much needed (R. M. Tryon and A. F. Tryon 1982).</discussion>
  <discussion>Species ca. 150–200 (9 in the flora).</discussion>
  <references>
    <reference>Fernald, M. L. 1950b. Adiantum capillus-veneris in the United States. Rhodora 52: 201–208.</reference>
    <reference>Paris, C. A. 1991. Adiantum viridimontanum, a new maidenhair fern in eastern North America. Rhodora 93: 105–122.</reference>
    <reference>Paris, C. A. and M. D. Windham. 1988. A biosystematic investigation of the Adiantum pedatum complex in eastern North America. Syst. Bot. 13: 240–255.</reference>
    <reference>Wagner, W. H. Jr. 1956. A natural hybrid, × Adiantum   tracyi C. C. Hall. Madroño 13: 195–205.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions of blades ± fan-shaped, rhombic, transversely oblong, or nearly round, about as long as broad.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions of blades ± oblong or long-triangular, at least 2 times as long as broad (rarely, reniform).</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Dark color of stalks extending into base of ultimate segments.</description>
      <determination>1 Adiantum capillus-veneris</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Dark color of stalks ending ± abruptly at base of ultimate segments.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Segment stalks terminating in small, cupulelike swelling at base of ultimate segments.</description>
      <determination>2 Adiantum tenerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Segment stalks not terminating in small, cupulelike swelling at base of ultimate segments.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ultimate segments glabrous.</description>
      <determination>3 Adiantum jordanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Ultimate segments hirsute.</description>
      <determination>4 Adiantum tricholepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Rachises hispid or strigose; blades pinnate (occasionally pseudopedate in Adiantum hispidulum).</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Rachises glabrous; blades pseudopedate.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Ultimate segments with scattered multicelled hairs; rachises hispid; false indusia ± round.</description>
      <determination>5 Adiantum hispidulum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Ultimate segments glabrous; rachises strigose; false indusia crescent-shaped.</description>
      <determination>6 Adiantum melanoleucum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions of blades ± oblong; leaves lax-arching, blades fan-shaped.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions ± long-triangular or reniform; leaves arching to stiffly erect, blades fan-shaped to funnel-shaped.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions of blades generally less than 3.2 times as long as broad, apices with rounded, crenulate or crenate-denticulate lobes, lobes separated by shallow sinuses 0.1–2(–3.7) mm, segment stalks ca. 0.6–0.9 mm.</description>
      <determination>7 Adiantum pedatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Segments at middle of penultimate divisions usually more than 3.2 times as long as broad, apices with sharply denticulate, angular lobes, lobes separated by deep sinuses 0.6–4 mm, segment stalks to 0.6 mm.</description>
      <determination>8 Adiantum aleuticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Central ultimate segments on stalks less than 0.9 mm; false indusia mostly less than 3.5 mm.</description>
      <determination>8 Adiantum aleuticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Central ultimate segments on stalks generally greater than 0.9 mm; false indusia mostly exceeding 3.5 mm.</description>
      <determination>9 Adiantum viridimontanum</determination>
    </key_statement>
  </key>
</bio:treatment>