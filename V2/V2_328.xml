<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="unknown" rank="genus">huperzia</taxon_name>
    <taxon_name authority="(Michaux) Trevisan" date="1875" rank="species">lucidula</taxon_name>
    <place_of_publication>
      <publication_title>Atti Soc. Ital. Sci. Nat.</publication_title>
      <place_in_publication>17: 248. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus huperzia;species lucidula</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002697</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">lucidulum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 284. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lycopodium;species lucidulum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Urostachys</taxon_name>
    <taxon_name authority="(Michaux) Nessel" date="unknown" rank="species">lucidulus</taxon_name>
    <taxon_hierarchy>genus Urostachys;species lucidulus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shoots erect, indeterminate, 14–20 (–100) cm, becoming long-decumbent, with long, trailing, senescent portion turning brown;</text>
      <biological_entity id="o9336" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="development" src="d0_s0" value="indeterminate" value_original="indeterminate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="becoming" name="growth_form_or_orientation" src="d0_s0" value="long-decumbent" value_original="long-decumbent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o9337" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="senescent" value_original="senescent" />
      </biological_entity>
      <relation from="o9336" id="r1986" name="with" negation="false" src="d0_s0" to="o9337" />
    </statement>
    <statement id="d0_s1">
      <text>juvenile and mature portions similar with strong annual constrictions due to formation of winter bud;</text>
      <biological_entity id="o9338" name="portion" name_original="portions" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="juvenile" value_original="juvenile" />
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity id="o9339" name="constriction" name_original="constrictions" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s1" value="strong" value_original="strong" />
        <character is_modifier="true" name="duration" src="d0_s1" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o9340" name="bud" name_original="bud" src="d0_s1" type="structure">
        <character is_modifier="true" name="season" src="d0_s1" value="winter" value_original="winter" />
      </biological_entity>
      <relation from="o9338" id="r1987" name="with" negation="false" src="d0_s1" to="o9339" />
      <relation from="o9339" id="r1988" name="part_of" negation="false" src="d0_s1" to="o9340" />
    </statement>
    <statement id="d0_s2">
      <text>juvenile growth erect.</text>
      <biological_entity id="o9341" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves spreading to reflexed, dark green, lustrous;</text>
      <biological_entity id="o9342" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest leaves narrowly obovate, leaves broadest at or above middle, 7–11 mm, margins papillate, teeth 1–8, irregular, large;</text>
      <biological_entity constraint="largest" id="o9343" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o9344" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broadest" value_original="broadest" />
        <character is_modifier="false" name="width" src="d0_s4" value="above middle leaves" value_original="above middle leaves" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o9345" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9346" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o9347" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="8" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s4" value="irregular" value_original="irregular" />
        <character is_modifier="false" name="size" src="d0_s4" value="large" value_original="large" />
      </biological_entity>
      <relation from="o9344" id="r1989" name="above" negation="false" src="d0_s4" to="o9345" />
    </statement>
    <statement id="d0_s5">
      <text>smallest leaves (at annual constrictions) narrowly lanceolate, 3–6 mm;</text>
      <biological_entity constraint="smallest" id="o9348" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stomates on abaxial surface only.</text>
      <biological_entity id="o9349" name="stomate" name_original="stomates" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o9350" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <relation from="o9349" id="r1990" name="on" negation="false" src="d0_s6" to="o9350" />
    </statement>
    <statement id="d0_s7">
      <text>Gemmiferous branchlets produced in 1 pseudowhorl at end of each annual growth cycle;</text>
      <biological_entity id="o9351" name="branchlet" name_original="branchlets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="gemmiferous" value_original="gemmiferous" />
      </biological_entity>
      <biological_entity id="o9352" name="pseudowhorl" name_original="pseudowhorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9353" name="end" name_original="end" src="d0_s7" type="structure" />
      <biological_entity constraint="growth" id="o9354" name="cycle" name_original="cycle" src="d0_s7" type="structure">
        <character is_modifier="true" name="duration" src="d0_s7" value="annual" value_original="annual" />
      </biological_entity>
      <relation from="o9351" id="r1991" name="produced in" negation="false" src="d0_s7" to="o9352" />
      <relation from="o9352" id="r1992" name="at" negation="false" src="d0_s7" to="o9353" />
      <relation from="o9353" id="r1993" name="part_of" negation="false" src="d0_s7" to="o9354" />
    </statement>
    <statement id="d0_s8">
      <text>gemmae 4–6 × 3–6 mm, lateral leaves 1.5–2.5 mm wide, broadly obtuse with distinct mucro.</text>
      <biological_entity id="o9355" name="gemma" name_original="gemmae" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9356" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
        <character constraint="with mucro" constraintid="o9357" is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o9357" name="mucro" name_original="mucro" src="d0_s8" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 23–29 µm. 2n = 134.</text>
      <biological_entity id="o9358" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="23" from_unit="um" name="some_measurement" src="d0_s9" to="29" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9359" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="134" value_original="134" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial in shaded conifer forests and mixed hardwoods, rarely on rock on shady mossy acidic sandstone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded conifer forests" />
        <character name="habitat" value="mixed hardwoods" />
        <character name="habitat" value="rock" constraint="on shady mossy" />
        <character name="habitat" value="shady mossy" />
        <character name="habitat" value="terrestrial in shaded conifer forests" />
        <character name="habitat" value="on rock on shady mossy acidic sandstone" modifier="rarely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Shining fir-moss</other_name>
  <other_name type="common_name">huperzie brillant</other_name>
  <discussion>Huperzia × bartleyi (Cusick) Kartesz &amp; Gandhi, a sterile hybrid between H. lucidula and H. porophila, occurs throughout the range of H. porophila and is discussed under that species. Huperzia × buttersii (Abbe) Kartesz &amp; Gandhi is a hybrid between H. lucidula and H. selago.</discussion>
  
</bio:treatment>