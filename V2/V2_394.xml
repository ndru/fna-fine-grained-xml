<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="Jermy" date="1986" rank="subgenus">tetragonostachys</taxon_name>
    <taxon_name authority="(Linnaeus) Spring" date="1838" rank="species">rupestris</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>21: 182. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus tetragonostachys;species rupestris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200002816</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rupestris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1101. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lycopodium;species rupestris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants on rock or terrestrial, forming long or spreading mats or rarely cushionlike mats.</text>
      <biological_entity id="o9457" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9458" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o9459" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="rarely" name="shape" src="d0_s0" value="cushionlike" value_original="cushionlike" />
      </biological_entity>
      <relation from="o9457" id="r2012" name="forming" negation="false" src="d0_s0" to="o9458" />
    </statement>
    <statement id="d0_s1">
      <text>Stems radially symmetric, long to moderately short-creeping to decumbent, not readily fragmenting, irregularly forked, without budlike arrested branches, tips straight;</text>
      <biological_entity id="o9460" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s1" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="moderately short-creeping" name="growth_form_or_orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" modifier="not readily; readily; irregularly" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o9461" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="budlike" value_original="budlike" />
      </biological_entity>
      <biological_entity id="o9462" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o9460" id="r2013" name="without" negation="false" src="d0_s1" to="o9461" />
    </statement>
    <statement id="d0_s2">
      <text>main-stem indeterminate, lateral branches conspicuously or inconspicuously determinate, sometimes ascending, 1–3-forked.</text>
      <biological_entity id="o9463" name="main-stem" name_original="main-stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="development" src="d0_s2" value="indeterminate" value_original="indeterminate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o9464" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="development" src="d0_s2" value="determinate" value_original="determinate" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-3-forked" value_original="1-3-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Rhizophores borne on upperside of stems, throughout stem length, 0.25–0.45 mm diam.</text>
      <biological_entity id="o9465" name="rhizophore" name_original="rhizophores" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="diameter" notes="" src="d0_s3" to="0.45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9466" name="upperside" name_original="upperside" src="d0_s3" type="structure" />
      <biological_entity id="o9467" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o9468" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o9465" id="r2014" name="borne on" negation="false" src="d0_s3" to="o9466" />
      <relation from="o9465" id="r2015" name="borne on" negation="false" src="d0_s3" to="o9467" />
      <relation from="o9465" id="r2016" name="throughout" negation="false" src="d0_s3" to="o9468" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves monomorphic, in alternate pseudowhorls of 6 (on main-stem) to 4 (on lateral branches), tightly appressed, ascending, green, occasionally reddish, linear or linear-lanceolate, 2.5–4 (–4.5) X 0.45–0.6 mm;</text>
      <biological_entity id="o9469" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="of 6-4; tightly" name="fixation_or_orientation" notes="" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.45" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9470" name="pseudowhorl" name_original="pseudowhorls" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
      </biological_entity>
      <relation from="o9469" id="r2017" name="in" negation="false" src="d0_s4" to="o9470" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial ridges well defined;</text>
      <biological_entity constraint="abaxial" id="o9471" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s5" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base cuneate and decurrent on underside to rounded and adnate on upperside, pubescent or glabrous;</text>
      <biological_entity id="o9472" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character constraint="on underside" constraintid="o9473" is_modifier="false" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
        <character constraint="on upperside" constraintid="o9474" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9473" name="underside" name_original="underside" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9474" name="upperside" name_original="upperside" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>margins long-ciliate, cilia transparent, spreading, (0.05–) 0.07–0.17 mm;</text>
      <biological_entity id="o9475" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o9476" name="cilium" name_original="cilia" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="0.07" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.07" from_unit="mm" name="some_measurement" src="d0_s7" to="0.17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex slightly keeled, mostly attenuate;</text>
      <biological_entity id="o9477" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bristle white, whitish, or transparent, puberulent, 0.45–1 (–1.5) mm.</text>
      <biological_entity id="o9478" name="bristle" name_original="bristle" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.45" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Strobili solitary, 0.5–3.5 cm;</text>
      <biological_entity id="o9479" name="strobilus" name_original="strobili" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporophylls deltate-ovate to ovatelanceolate, strongly tapering or not toward apex, abaxial ridges well defined, base glabrous, margins ciliate to slightly dentate, apex slightly keeled, not truncate in profile, long-bristled.</text>
      <biological_entity id="o9480" name="sporophyll" name_original="sporophylls" src="d0_s11" type="structure">
        <character char_type="range_value" from="deltate-ovate" name="shape" src="d0_s11" to="ovatelanceolate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
        <character name="shape" src="d0_s11" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o9481" name="apex" name_original="apex" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o9482" name="ridge" name_original="ridges" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s11" value="defined" value_original="defined" />
      </biological_entity>
      <biological_entity id="o9483" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9484" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s11" value="ciliate to slightly" value_original="ciliate to slightly" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s11" value="dentate" value_original="dentate" />
      </biological_entity>
      <relation from="o9480" id="r2018" name="toward" negation="false" src="d0_s11" to="o9481" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o9485" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="not; in profile" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="long-bristled" value_original="long-bristled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9486" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry ledges, sea cliffs, limestone, open fire-barrens, sandstone, granite outcrops, exposed rock, rock crevices, sandy or gravelly soil or grassy meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry ledges" />
        <character name="habitat" value="sea cliffs" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="open fire-barrens" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="granite outcrops" />
        <character name="habitat" value="exposed rock" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="grassy meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., Man., N.B., N.S., Ont., Que., Sask.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Iowa., Ky., Kans., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio., Okla., Pa., R.I., S.C., S.Dak., Tenn., Vt., Va., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15</number>
  <other_name type="common_name">Rock spike-moss</other_name>
  <other_name type="common_name">dwarf spike-moss</other_name>
  <other_name type="common_name">sélaginelle des rochers</other_name>
  <other_name type="common_name">sélaginelle rupestre</other_name>
  <discussion>Selaginella rupestris has the widest range of any selaginella in the flora. It is variable in many characteristics, e.g., the hairiness of the margins (which sometimes are not hairy), leaf base pubescence, and shape of sporophylls. The variation in sporangial distribution pattern in the strobili and the number of megaspores per megasporangium are important for understanding reproduction and relationships in this species. Very often the strobili are wholly megasporangiate, with only 1–2 megaspores per megasporangium, suggesting asexual reproduction. In other cases, both types of sporangia are present in a strobilus, suggesting sexual reproduction. R. M. Tryon (1971) correlated sporangial and spore distribution patterns in S. rupestris with distributional ranges and concluded that there are four races. Race A has 4 megaspores per megasporangium, has microsporangia, is sexual, and is distributed from southeastern Pennsylvania south to Georgia and Alabama. Race B has 1–2(–4) megaspores per megasporangium, has microsporangia, has an unknown type of reproduction, and has the same range as Race A, but it extends into New York. Race C has 1–2 megaspores per megasporangium, has microsporangia, is probably asexual, and is distributed throughout the species range. Race D has 1–2 megaspores per megasporangium, lacks microsporangia, is therefore asexual, and is found throughout the species range except where Race A occurs. These patterns suggest the presence of more than one species within S. rupestris in the broad sense. More studies, especially cytologic, are needed, as well as fieldwork, in order to understand these relationships and the variability in S. rupestris. Among the species in the flora, S. rupestris seems to be most closely related to S. sibirica (see discussion) and may also be allied to the S. arenicola complex (see discussion).</discussion>
  
</bio:treatment>