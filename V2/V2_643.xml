<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>George Yatskievych, Michael D. Windham</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Link" date="1833" rank="genus">Pityrogramma</taxon_name>
    <place_of_publication>
      <publication_title>Handbuch</publication_title>
      <place_in_publication>3: 19. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus Pityrogramma</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pityros, bran, and gramma, lines (as in written characters), referring to the farina covering the abaxial leaf blade surface</other_info_on_name>
    <other_info_on_name type="fna_id">125649</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial [or on rock].</text>
      <biological_entity id="o4290" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or nearly so, unbranched;</text>
      <biological_entity id="o4291" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character name="orientation" src="d0_s1" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales brown, concolored or nearly so, lanceolate, margins entire.</text>
      <biological_entity id="o4292" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="concolored" value_original="concolored" />
        <character name="coloration" src="d0_s2" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o4293" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic, closely spaced, 25–150 cm.</text>
      <biological_entity id="o4294" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s3" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole black to purplish black or reddish-brown, longitudinally grooved adaxially, glabrous, with 2 (–3) vascular-bundles.</text>
      <biological_entity id="o4295" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s4" to="purplish black or reddish-brown" />
        <character is_modifier="false" modifier="longitudinally; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4296" name="vascular-bundle" name_original="vascular-bundles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4295" id="r917" name="with" negation="false" src="d0_s4" to="o4296" />
    </statement>
    <statement id="d0_s5">
      <text>Blade linear-lanceolate to ovate or elongate-triangular, 1–4-pinnate, herbaceous to leathery, abaxially usually farinose, farina white or yellow, sometimes partially or completely replaced by trichomes, adaxially glabrous, dull, not striate;</text>
      <biological_entity id="o4297" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="ovate or elongate-triangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-4-pinnate" value_original="1-4-pinnate" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s5" to="leathery" />
        <character is_modifier="false" modifier="abaxially usually" name="pubescence" src="d0_s5" value="farinose" value_original="farinose" />
      </biological_entity>
      <biological_entity id="o4298" name="farina" name_original="farina" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes partially; partially; completely; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s5" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o4299" name="trichome" name_original="trichomes" src="d0_s5" type="structure" />
      <relation from="o4298" id="r918" modifier="sometimes partially; partially; completely" name="replaced by" negation="false" src="d0_s5" to="o4299" />
    </statement>
    <statement id="d0_s6">
      <text>rachis straight.</text>
      <biological_entity id="o4300" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ultimate segments stalked or sessile, free from costa or partially adnate to it, narrowly triangular to linear, entire or lobed;</text>
      <biological_entity constraint="ultimate" id="o4301" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character constraint="from costa" constraintid="o4302" is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s7" to="linear entire or lobed" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s7" to="linear entire or lobed" />
      </biological_entity>
      <biological_entity id="o4302" name="costa" name_original="costa" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>base narrowly cuneate, stalks when present green, not lustrous;</text>
      <biological_entity id="o4303" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4304" name="stalk" name_original="stalks" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when present" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s8" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>margins not recurved to form false indusia.</text>
      <biological_entity id="o4305" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="false" id="o4306" name="indusium" name_original="indusia" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Veins of ultimate segments free, obscure, pinnately branched and divergent distally.</text>
      <biological_entity id="o4307" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="prominence" src="d0_s10" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s10" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o4308" name="segment" name_original="segments" src="d0_s10" type="structure" />
      <relation from="o4307" id="r919" name="part_of" negation="false" src="d0_s10" to="o4308" />
    </statement>
    <statement id="d0_s11">
      <text>False indusia absent.</text>
      <biological_entity constraint="false" id="o4309" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sporangia scattered along veins, containing 32 or 64 spores, intermixed with farina-producing glands.</text>
      <biological_entity id="o4310" name="sporangium" name_original="sporangia" src="d0_s12" type="structure">
        <character constraint="along veins" constraintid="o4311" is_modifier="false" name="arrangement" src="d0_s12" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o4311" name="vein" name_original="veins" src="d0_s12" type="structure" />
      <biological_entity id="o4312" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
      <relation from="o4310" id="r920" name="containing" negation="false" src="d0_s12" to="o4312" />
    </statement>
    <statement id="d0_s13">
      <text>Spores tan with dark-brown ridges, tetrahedral-globose, perispore usually reticulate, with equatorial flange (except in Pityrogramma trifoliata).</text>
      <biological_entity id="o4313" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character constraint="with ridges" constraintid="o4314" is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="tetrahedral-globose" value_original="tetrahedral-globose" />
      </biological_entity>
      <biological_entity id="o4314" name="ridge" name_original="ridges" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o4315" name="perispore" name_original="perispore" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity constraint="equatorial" id="o4316" name="flange" name_original="flange" src="d0_s13" type="structure" />
      <relation from="o4315" id="r921" name="with" negation="false" src="d0_s13" to="o4316" />
    </statement>
    <statement id="d0_s14">
      <text>Gametophytes glabrous.</text>
      <biological_entity id="o4317" name="gametophyte" name_original="gametophytes" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Primarily neotropical, some in Africa, introduced elsewhere in Eastern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Primarily neotropical" establishment_means="introduced" />
        <character name="distribution" value="some in Africa" establishment_means="introduced" />
        <character name="distribution" value="elsewhere in Eastern Hemisphere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Silverback fern</other_name>
  <other_name type="common_name">goldback fern</other_name>
  <discussion>The name Nesoris bicolor Rafinesque was based on a yellow-farinose plant from Florida and presumably represents a species of Pityrogramma. A type has not been located, and C. S. Rafinesque's (1836[–1838]b, part 4) description is not precise enough to permit equation of this name with modern nomenclature. No yellow-farinose Pityrogramma has since been reported from Florida.</discussion>
  <discussion>Pentagramma has been segregated from Pityrogramma (G. Yatskievych et al. 1990) and comprises what was the Pityrogramma triangularis (Kaulfuss) Maxon complex.</discussion>
  <discussion>Species ca. 15 (2 in the flora).</discussion>
  <references>
    <reference>Tryon, R. M. 1962. Taxonomic fern notes. II. Pityrogramma  (including Trismeria) and Anogramma. Contr. Gray Herb. 189: 52–76.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal pinnae pinnately lobed or divided, narrowly triangular.</description>
      <determination>1a Pityrogramma calomelanos var. calomelanos</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distal pinnae entire or serrulate, linear to narrowly lanceolate.</description>
      <determination>2 Pityrogramma trifoliata</determination>
    </key_statement>
  </key>
</bio:treatment>