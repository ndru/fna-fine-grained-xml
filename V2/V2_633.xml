<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Iván A. Valdespino</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">Selaginella</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Aethéogam.</publication_title>
      <place_in_publication>101. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus Selaginella</taxon_hierarchy>
    <other_info_on_name type="etymology">from Selago, an ancient name for Lycopodium, a genus resembling Selaginella, and Latin, -ella, diminutive suffix</other_info_on_name>
    <other_info_on_name type="fna_id">130007</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, on rock, or rarely hemiepiphytic (initially terrestrial, becoming epiphytic) or epiphytic (in S. oregana).</text>
      <biological_entity id="o1203" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" modifier="rarely" name="habitat" src="d0_s0" value="hemiepiphytic" value_original="hemiepiphytic" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1204" name="rock" name_original="rock" src="d0_s0" type="structure" />
      <relation from="o1203" id="r278" name="on" negation="false" src="d0_s0" to="o1204" />
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, creeping, decumbent, cespitose, climbing, or fully erect, articulate or not, slightly to greatly branched.</text>
      <biological_entity id="o1205" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="climbing" value_original="climbing" />
        <character is_modifier="false" modifier="fully" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="articulate" value_original="articulate" />
        <character name="architecture" src="d0_s1" value="not" value_original="not" />
        <character is_modifier="false" modifier="slightly to greatly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizophores usually present, stout or filiform.</text>
      <biological_entity id="o1206" name="rhizophore" name_original="rhizophores" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s2" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Roots branching several times dichotomously from rhizophore tips.</text>
      <biological_entity id="o1207" name="root" name_original="roots" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character constraint="dichotomously from rhizophore tips" constraintid="o1208" is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="rhizophore" id="o1208" name="tip" name_original="tips" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves on aerial stems dimorphic or monomorphic;</text>
      <biological_entity id="o1210" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="true" name="location" src="d0_s4" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="growth_form" src="d0_s4" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <relation from="o1209" id="r279" name="on" negation="false" src="d0_s4" to="o1210" />
    </statement>
    <statement id="d0_s5">
      <text>if monomorphic, then linear to narrowly lanceolate, highly overlapping, spirally arranged;</text>
    </statement>
    <statement id="d0_s6">
      <text>if leaves on aerial stem dimorphic, then round or oblong to lanceolate, arranged in 4 ranks, 2 ranks of larger spreading lateral leaves and 2 ranks of smaller, appressed, and ascending median leaves, often with axillary leaf at base of each branching dichotomy.</text>
      <biological_entity id="o1209" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="highly" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o1211" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s6" to="lanceolate" />
        <character constraint="in ranks" constraintid="o1213" is_modifier="false" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o1212" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="true" name="location" src="d0_s6" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="growth_form" src="d0_s6" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o1213" name="rank" name_original="ranks" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1214" name="rank" name_original="ranks" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="larger lateral" id="o1215" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o1216" name="rank" name_original="ranks" src="d0_s6" type="structure" />
      <biological_entity constraint="median" id="o1217" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o1218" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o1219" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o1220" name="dichotomy" name_original="dichotomy" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="branching" value_original="branching" />
      </biological_entity>
      <relation from="o1211" id="r280" name="on" negation="false" src="d0_s6" to="o1212" />
      <relation from="o1214" id="r281" name="part_of" negation="false" src="d0_s6" to="o1215" />
      <relation from="o1216" id="r282" name="part_of" negation="false" src="d0_s6" to="o1217" />
      <relation from="o1216" id="r283" modifier="often" name="with" negation="false" src="d0_s6" to="o1218" />
      <relation from="o1218" id="r284" name="at" negation="false" src="d0_s6" to="o1219" />
      <relation from="o1219" id="r285" name="part_of" negation="false" src="d0_s6" to="o1220" />
    </statement>
    <statement id="d0_s7">
      <text>Megasporangia lobed to ovoid;</text>
      <biological_entity id="o1221" name="megasporangium" name_original="megasporangia" src="d0_s7" type="structure">
        <character char_type="range_value" from="lobed" name="shape" src="d0_s7" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>microsporangia reniform to ovoid.</text>
      <biological_entity id="o1222" name="microsporangium" name_original="microsporangia" src="d0_s8" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s8" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Megaspores tetrahedral, ovoid, or globose, variously sculptured, (127–) 200–1360 µm diam.;</text>
      <biological_entity id="o1223" name="megaspore" name_original="megaspores" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="tetrahedral" value_original="tetrahedral" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="variously" name="relief" src="d0_s9" value="sculptured" value_original="sculptured" />
        <character char_type="range_value" from="127" from_unit="um" name="diameter" src="d0_s9" to="200" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="200" from_unit="um" name="diameter" src="d0_s9" to="1360" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>microspores tetrahedral, variously sculptured, 20–75 µm diam. x = 7, 8, 9, 10, 11, 12.</text>
      <biological_entity id="o1224" name="microspore" name_original="microspores" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="tetrahedral" value_original="tetrahedral" />
        <character is_modifier="false" modifier="variously" name="relief" src="d0_s10" value="sculptured" value_original="sculptured" />
        <character char_type="range_value" from="20" from_unit="um" name="diameter" src="d0_s10" to="75" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="x" id="o1225" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="7" value_original="7" />
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" value="9" value_original="9" />
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
        <character name="quantity" src="d0_s10" value="11" value_original="11" />
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, mainly tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="mainly tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Spike-moss</other_name>
  <other_name type="common_name">sélaginelle</other_name>
  <discussion>The generic and infrageneric classification of Selaginella is controversial, and more than one genus may be recognized (see R. E. G. Pichi-Sermolli 1971 for information on generic synonyms). A. C. Jermy (1986, 1990b) proposed a subgeneric classification similar to that of J. G. Baker (1883, 1887). Despite some reservations, I consider Jermy's system useful for our purpose; therefore it is followed here. Three of the five subgenera proposed by Jermy occur in the flora area: subg. Selaginella, subg. Tetragonostachys, and subg. Stachygynandrum. One of the species in the flora, Selaginella eatonii (see discussion), may eventually prove to be best classified within a fourth, subg. Heterostachys Baker.</discussion>
  <discussion>Some characteristics used in the keys and descriptions are best observed in fresh specimens or by soaking a sample of a dried specimen in water, using material at branch forks or buds. This is particularly true for members of subg. Tetragonostachys. Use a minimum of 20X (40–60X better) magnification and take measurements of both young and old leaves. Measurements of leaf length include the bristle and the most basal portion.</discussion>
  <discussion>Selaginella subg. Tetragonostachys has a tendency for stem and leaves close to the substrate surface to be morphologically different from those on the side away from the substrate. In this case, the leaves on the side of the axis away from the surface are called upperside leaves, and those on the side toward the surface are called underside leaves. Otherwise, the leaves are designated only as leaves. In the subg. Stachygynandrum, however, which has complete structural differentiation between stem sides, the upper leaves are called median leaves, and the lower ones are called lateral leaves.</discussion>
  <discussion>Species probably more than 700 (38 in the flora).</discussion>
  <references>
    <reference>Alston, A. H. G. 1955. The heterophyllous Selaginella of continental North America. Bull. Brit. Mus. (Nat. Hist.), Bot. 1(8): 219–274.</reference>
    <reference>Baker, J. G. 1883. A synopsis of the genus Selaginella, pt. 1. J. Bot. 21: 1–5.</reference>
    <reference>Horner, H. T. Jr. and H. J. Arnott. 1963. Sporangial arrangement in North American species of Selaginella. Bot. Gaz. 124: 371–383.</reference>
    <reference>Jermy, A. C. 1986. Subgeneric names in Selaginella. Fern Gaz. 13: 117–118.</reference>
    <reference>Koller, A. L. and S. E. Scheckler. 1986. Variation in microsporangia and microspore dispersal in Selaginella. Amer. J. Bot. 73: 1274–1288.</reference>
    <reference>Tryon, A. F. 1949. Spores of the genus Selaginella in North America, north of Mexico. Ann. Missouri Bot. Gard. 36: 413–431.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves on aerial stems dimorphic, arranged in 4 ranks (2 median, 2 lateral), axillary leaves present at branching points; rhizophores present.</description>
      <determination>1c Subg. Stachygynandrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves on aerial stems monomorphic, not in distinct ranks, axillary leaves absent at branching points; rhizophores present or absent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Strobili cylindric; sporophylls spreading; leaves thin, soft, margins short-spiny; stomates throughout abaxial surface of leaf; rhizophores absent.</description>
      <determination>1a Subg. Selaginella</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Strobili quadrangular; sporophylls usually appressed; leaves thick or fleshy (seldom thin), firm, margins dentate, serrate, or ciliate (never spiny); stomates in abaxial groove; rhizophores present.</description>
      <determination>1b Subg. Tetragonostachys</determination>
    </key_statement>
  </key>
</bio:treatment>