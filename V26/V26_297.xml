<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">166</other_info_on_meta>
    <other_info_on_meta type="treatment_page">171</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fritillaria</taxon_name>
    <taxon_name authority="Eastwood" date="1902" rank="species">purdyi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 75, plate 6. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus fritillaria;species purdyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101629</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb scales: large 2–8;</text>
      <biological_entity constraint="bulb" id="o28588" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>small 0–3.</text>
      <biological_entity constraint="bulb" id="o28589" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem 1–4 dm.</text>
      <biological_entity id="o28590" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2–10, alternate, ± crowded near ground, 2.5–10 cm;</text>
      <biological_entity id="o28591" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="10" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character constraint="near ground" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate, occasionally a few sickle-shaped.</text>
      <biological_entity id="o28592" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="occasionally" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sickle--shaped" value_original="sickle--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers horizontal or nodding;</text>
      <biological_entity id="o28593" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals white, with purple spots or lines and pink shading, 1.5–3 cm, apex often slightly recurved;</text>
      <biological_entity id="o28594" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="pink shading" value_original="pink shading" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28595" name="line" name_original="lines" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="purple spots" value_original="purple spots" />
      </biological_entity>
      <biological_entity id="o28596" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often slightly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o28594" id="r3860" name="with" negation="false" src="d0_s6" to="o28595" />
    </statement>
    <statement id="d0_s7">
      <text>nectaries obscure, colored like tepals, broadly linear, ± equaling tepal length;</text>
      <biological_entity id="o28597" name="nectary" name_original="nectaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
        <character constraint="like tepals" constraintid="o28598" is_modifier="false" name="coloration" src="d0_s7" value="colored" value_original="colored" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_course_or_shape" notes="" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o28598" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity id="o28599" name="tepal" name_original="tepal" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style obviously branched for 1/2 its length, branches longer than 1.5 mm.</text>
      <biological_entity id="o28600" name="style" name_original="style" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="obviously" name="length" src="d0_s8" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o28601" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character modifier="longer than" name="some_measurement" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules acutely angled.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24.</text>
      <biological_entity id="o28602" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28603" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry ridges, generally on serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry ridges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Purdy’s fritillary</other_name>
  
</bio:treatment>