<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="S. Watson" date="1879" rank="genus">hastingsia</taxon_name>
    <taxon_name authority="Becking" date="1986" rank="species">atropurpurea</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>33: 175, figs. 1, 2. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hastingsia;species atropurpurea</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101651</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb ellipsoid or infrequently ellipsoid-ovoid, 28–54 × 18–30 mm, usually with blackish, fibrous tunic.</text>
      <biological_entity id="o18543" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="infrequently" name="shape" src="d0_s0" value="ellipsoid-ovoid" value_original="ellipsoid-ovoid" />
        <character char_type="range_value" from="28" from_unit="mm" name="length" src="d0_s0" to="54" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="width" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18544" name="tunic" name_original="tunic" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="texture" src="d0_s0" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <relation from="o18543" id="r2568" modifier="usually" name="with" negation="false" src="d0_s0" to="o18544" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves 25–55 cm × 6–12 mm, mature plants with blackish, shriveled foliage at base of scape;</text>
      <biological_entity id="o18545" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s1" to="55" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s1" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18546" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="mature" value_original="mature" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18547" name="foliage" name_original="foliage" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="shape" src="d0_s1" value="shriveled" value_original="shriveled" />
      </biological_entity>
      <biological_entity id="o18548" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o18549" name="scape" name_original="scape" src="d0_s1" type="structure" />
      <relation from="o18546" id="r2569" name="with" negation="false" src="d0_s1" to="o18547" />
      <relation from="o18547" id="r2570" name="at" negation="false" src="d0_s1" to="o18548" />
      <relation from="o18548" id="r2571" name="part_of" negation="false" src="d0_s1" to="o18549" />
    </statement>
    <statement id="d0_s2">
      <text>blade bluish green.</text>
      <biological_entity id="o18550" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish green" value_original="bluish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Scape 71–99 cm, often with 2–5 ascending branches, 4–6 mm thick at base.</text>
      <biological_entity id="o18551" name="scape" name_original="scape" src="d0_s3" type="structure">
        <character char_type="range_value" from="71" from_unit="cm" name="some_measurement" src="d0_s3" to="99" to_unit="cm" />
        <character char_type="range_value" constraint="at base" constraintid="o18553" from="4" from_unit="mm" name="thickness" notes="" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18552" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o18553" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o18551" id="r2572" modifier="often" name="with" negation="false" src="d0_s3" to="o18552" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: terminal raceme (8–) 12–35 (–49) cm;</text>
      <biological_entity id="o18554" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="terminal" id="o18555" name="raceme" name_original="raceme" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="49" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s4" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>flowers (20–) 30–36 (–46) per 10 cm of raceme.</text>
      <biological_entity id="o18556" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o18557" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s5" to="30" to_inclusive="false" />
        <character char_type="range_value" from="36" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="46" />
        <character char_type="range_value" constraint="of raceme" constraintid="o18558" from="30" name="quantity" src="d0_s5" to="36" />
      </biological_entity>
      <biological_entity id="o18558" name="raceme" name_original="raceme" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals purplish black with pale green central vein, oblong-lanceolate, 9–12 × 2 mm;</text>
      <biological_entity id="o18559" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18560" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character constraint="with central vein" constraintid="o18561" is_modifier="false" name="coloration" src="d0_s6" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="central" id="o18561" name="vein" name_original="vein" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens seldom exposed at anthesis;</text>
      <biological_entity id="o18562" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18563" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="seldom" name="prominence" src="d0_s7" value="exposed" value_original="exposed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 5–8 mm;</text>
      <biological_entity id="o18564" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18565" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers purple.</text>
      <biological_entity id="o18566" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o18567" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules ellipsoid to ellipsoid-ovoid, 7–11 × 5–8 mm.</text>
      <biological_entity id="o18568" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s10" to="ellipsoid-ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds black, 5 mm.</text>
      <biological_entity id="o18569" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul, fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ultramafic river-beds that have year-round water in rooting horizon and wet, open, sunny bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ultramafic river-beds" />
        <character name="habitat" value="year-round water" constraint="in rooting horizon and wet , open , sunny bogs" />
        <character name="habitat" value="horizon" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="open" />
        <character name="habitat" value="sunny bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>