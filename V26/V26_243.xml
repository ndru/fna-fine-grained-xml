<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1818" rank="genus">CLINTONIA</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>2: 266. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus CLINTONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">for De Witt Clinton (1769–1828), statesman and several-times governor of New York</other_info_on_name>
    <other_info_on_name type="fna_id">107362</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Salisbury" date="unknown" rank="genus">Xeniatrum</taxon_name>
    <taxon_hierarchy>genus Xeniatrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, 1.5–8 dm;</text>
      <biological_entity id="o2266" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes bearing thin, fibrous-roots.</text>
      <biological_entity id="o2267" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <biological_entity id="o2268" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o2267" id="r294" name="bearing" negation="false" src="d0_s1" to="o2268" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 2–6, arising from rhizome crown, sessile, sheathing, hyaline, chaffy in age;</text>
      <biological_entity id="o2269" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2270" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="6" />
        <character constraint="from rhizome crown" constraintid="o2271" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="hyaline" value_original="hyaline" />
        <character constraint="in age" constraintid="o2272" is_modifier="false" name="pubescence" src="d0_s2" value="chaffy" value_original="chaffy" />
      </biological_entity>
      <biological_entity constraint="rhizome" id="o2271" name="crown" name_original="crown" src="d0_s2" type="structure" />
      <biological_entity id="o2272" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline petiolate, blade with central vein prominent basally, obovate to oblanceolate, weakly villous-pubescent, base attenuate to cuneate, margins entire, apex acute to abruptly short-acuminate, often mucronate.</text>
      <biological_entity id="o2273" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o2274" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o2275" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" modifier="weakly" name="pubescence" src="d0_s3" value="villous" value_original="villous-pubescent" />
      </biological_entity>
      <biological_entity constraint="central" id="o2276" name="vein" name_original="vein" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o2277" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s3" to="cuneate" />
      </biological_entity>
      <biological_entity id="o2278" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2279" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="abruptly short-acuminate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <relation from="o2275" id="r295" name="with" negation="false" src="d0_s3" to="o2276" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal, in short racemes or umbellike clusters, 1–45-flowered, bracteate;</text>
      <biological_entity id="o2280" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s4" value="umbel-like" value_original="umbellike" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-45-flowered" value_original="1-45-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o2281" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <relation from="o2280" id="r296" name="in" negation="false" src="d0_s4" to="o2281" />
    </statement>
    <statement id="d0_s5">
      <text>bracts foliaceous or linear.</text>
      <biological_entity id="o2282" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: tepals caducous, 6, distinct, obovate to narrowly oblanceolate, nectaries present, sometimes obscure;</text>
      <biological_entity id="o2283" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2284" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="narrowly oblanceolate" />
      </biological_entity>
      <biological_entity id="o2285" name="nectary" name_original="nectaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens inserted at perianth base;</text>
      <biological_entity id="o2286" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2287" name="stamen" name_original="stamens" src="d0_s7" type="structure" />
      <biological_entity constraint="perianth" id="o2288" name="base" name_original="base" src="d0_s7" type="structure" />
      <relation from="o2287" id="r297" name="inserted at" negation="false" src="d0_s7" to="o2288" />
    </statement>
    <statement id="d0_s8">
      <text>filaments dilated, basally pubescent;</text>
      <biological_entity id="o2289" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2290" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers oblong-obovate to oblong-linear, semiversatile, extrorse;</text>
      <biological_entity id="o2291" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2292" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s9" to="oblong-linear" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="semiversatile" value_original="semiversatile" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s9" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary superior, 2-locular [3-locular], ovoid to subcylindrical, glabrous, ovules 2–10 per locule;</text>
      <biological_entity id="o2293" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2294" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="2-locular" value_original="2-locular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="subcylindrical" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2295" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o2296" from="2" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <biological_entity id="o2296" name="locule" name_original="locule" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>style columnar, compressed laterally;</text>
      <biological_entity id="o2297" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2298" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas weakly 2-lobed [3-lobed];</text>
      <biological_entity id="o2299" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2300" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s12" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicels unequal, elongate and ascending with age.</text>
      <biological_entity id="o2301" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2302" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
        <character constraint="with age" constraintid="o2303" is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o2303" name="age" name_original="age" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits baccate, metallic blue to black, 4–30-seeded, ellipsoid to ovoid, smooth.</text>
      <biological_entity id="o2304" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="baccate" value_original="baccate" />
        <character char_type="range_value" from="metallic blue" name="coloration" src="d0_s14" to="black" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="4-30-seeded" value_original="4-30-seeded" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s14" to="ovoid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds shiny brown, round abaxially, angled with 2 or 3 faces.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 7.</text>
      <biological_entity id="o2305" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s15" value="round" value_original="round" />
        <character constraint="with 2 or 3faces" is_modifier="false" name="shape" src="d0_s15" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity constraint="x" id="o2306" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Temperate North America, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Temperate North America" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Clintonia</other_name>
  <discussion>Species 5 (4 in the flora).</discussion>
  <discussion>Eastern and western Native Americans used Clintonia as an eye and heart medicine, as well as a dermatological and gynecological aid (D. E. Moerman 1986).</discussion>
  <discussion>The North American Clintonia species have bicarpellate ovaries by reduction (F. H. Utech 1973) and reticulate pollen, in contrast to the typical liliaceous tricarpellate ovaries and gemmate pollen that occur in the eastern Asian C. udensis (M. Takahashi and K. Sohma 1982; V. N. Kosenko 1991). The genus has a unique type of megasporogenesis, the “Clintonia-type” (A. N. Pahuja and V. Kumar 1971; F. H. Smith 1943; R. W. Smith 1911; R. I. Walker 1944), in which there is a selective elimination of genomes (heterotypic vs. homotypic divisions) before double fertilization, resulting in genetically identical diploid embryo and diploid endosperm nuclei. Such derived megasporogenesis is equivalent to female self-fertilization (pseudogamy).</discussion>
  <discussion>Counts of 2n = 28 are known in all Clintonia species except for a recently discovered sympatric diploid cytotype (2n = 14) in C. udensis (Li S. F. and Chang Z. Y. 1996; Li S. F. et al. 1996). It appears therefore that the Clintonia karyotype has an ancient allopolyploid origin and that the generic base number should be viewed as x = 7 and not x = 14. Based on these karyological data, an early eastern Asian and North American divergence with speciation and polyploidization occurring in North America is postulated.</discussion>
  <discussion>M. N. Tamura (1998c) included Medeola and Clintonia in the tribe Medeoloideae within a narrowly defined Liliaceae, and molecular analysis supports the association of those two genera (K. Hayashi et al. 1998, 2001; M. F. Fay and M. W. Chase 2000).</discussion>
  <references>
    <reference>Yoshida, F. H. Utech, and S. Kawano. 2001. Molecular systematics in the genus Clintonia and related taxa based on rbcL and matK gene sequence data. Pl. Spec. Biol. 16: 119–137.  </reference>
    <reference>Utech, F. H. 1973. A Biosystematic Study of the Genus Clintonia Raf. (Liliaceae: Polygonatae). Ph.D. thesis. Washington University.  </reference>
    <reference>Utech, F. H. 1975. Biosystematic studies in Clintonia (Liliaceae-Polygonatae) III. Cytogeography, chromosome numbers and chromosomal morphology of the North American species of Clintonia. Cytologia 40: 765–786.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences 1–8(–10)-flowered, in short, terminal racemes.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences 10–45-flowered, in terminal and/or lateral, umbel-like clusters.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 1.5–2.5 dm; inflorescences 1- or 2-flowered; tepals creamy white, 18–25 mm; anthers 3.5–5.5 mm; w North America.</description>
      <determination>1 Clintonia uniflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 2–5 dm; inflorescences 3–8(–10)-flowered, in short, terminal racemes; tepals yellow to yellowish green, 12–16 mm; anthers 2–3.5 mm; e North America.</description>
      <determination>2 Clintonia borealis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences 20–45-flowered, lateral clusters usually present; tepals claret red, narrowly oblong to oblanceolate, 10–18 mm; berries blue to bluish black, 10–30-seeded; California, Oregon.</description>
      <determination>3 Clintonia andrewsiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences 10–25(–30)-flowered, in terminal clusters only; tepals white, often spotted purplish brown or green distally, ovoid-obovate, 5.5–8 mm; berries black, 2–4-seeded; e North America.</description>
      <determination>4 Clintonia umbellulata</determination>
    </key_statement>
  </key>
</bio:treatment>