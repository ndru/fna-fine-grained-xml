<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Hexagonae</taxon_name>
    <place_of_publication>
      <publication_title>Gentes Herb.</publication_title>
      <place_in_publication>8: 362. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series Hexagonae</taxon_hierarchy>
    <other_info_on_name type="fna_id">300152</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="Diels" date="unknown" rank="subsection">Hexagonae</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 15a: 502. 1930</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Iris;subsection Hexagonae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences with leaflike spathes.</text>
      <biological_entity id="o26242" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure" />
      <biological_entity id="o26243" name="spathe" name_original="spathes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s0" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <relation from="o26242" id="r3572" name="with" negation="false" src="d0_s0" to="o26243" />
    </statement>
    <statement id="d0_s1">
      <text>Capsules 6-ribbed or lobed.</text>
      <biological_entity id="o26244" name="capsule" name_original="capsules" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="6-ribbed" value_original="6-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Seeds large, corky.</text>
      <biological_entity id="o26245" name="seed" name_original="seeds" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="large" value_original="large" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="corky" value_original="corky" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se, sc United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4b.2g.</number>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>Series Hexagonae has had a long and interesting taxonomic history, yet is still one of the most confusing and controversial groups of North American irises. Iris hexagona, I. fulva, and I. brevicaulis were the only ones of the series described prior to 1924, when J. K. Small began to study the group along the eastern and Gulf coasts. He described a number of new species, including I. giganticaerulea from Louisiana, which is one of the most interesting of the series. J. K. Small and E. J. Alexander (1931) then proposed an additional 76 species from the Gulf States, all within this series. This created considerable interest among botanists and resulted in a large number of papers with different interpretations, many of which are still in question. P. Viosca (1935) stated, “A whole new series of hybrids has sprung up. Thus, theoretically, the number of possibilities is infinite, and one could just as well describe 500 or a thousand as a hundred species.”</discussion>
  <discussion>It is quite possible that many of the 76 “new species” that Small and Alexander listed may actually be a part of the population of one of the primary hybrids. If that can be demonstrated, the pertinent names should be moved into synonymy under the particular hybrid. Tentatively, they are here placed in Iris ×louisianica N. C. Henderson, including: (published by Small) I. alticristata, I. atroenantha, I. auralata, I. bifurcata, I. brevipes, I. chlorolopha, I. chrysolopha, I. citricristata, I. citriviola, I. cyanochrysea, I. dewinkeleri, I. fluviatilis, I. fumifulva, I. fuscaurea, I. fuscirosea, I. fuscivenosa, I. iochroma, I. iocyanea, I. ludoviciana, I. moricolor, I. oenantha, I. parvirosea, I. phoenicis, I. pseudocristata, I. purpurissata, I. regalis, I. rhodochrysea, I. rosiflora, I. rubricunda, I. salmonicolor, I. subfulva, I. thomasii, I. tyriana, I. violivenosa, I. wherryana; (published by Alexander) I. albilinea, I. amnicola, I. aurilinea, I. callirhodea, I. cerasioides, I. crocinubia, I. cyanantha, I. ecristata, I. fumiflora, I. fuscisanguinea, I. gentilliana, I. ianthina, I. iodantha, I. ioleuca, I. iophaea, I. lancipetala, I. lilacinaurea, I. marplei, I. oölopha, I. pallidirosea, I. paludicola, I. parvicaerulea, I. pyrrholopha, I. regifulva, I. rhodantha, I. rosilutea, I. rosipurpurea, I. rubea, I. rubrolilacina, I. schizolopha, I. venulosa, I. violilutea, I. viridis.</discussion>
  <references>
    <reference>Small, J. K. and E. J. Alexander. 1931. Botanical interpretations of the iridaceous plants of the Gulf States. Contr. New York Bot. Gard. 327: 325–358.</reference>
    <reference>Viosca, P. 1935. The irises of southeastern Louisiana, a taxonomic and ecological interpretation. Bull. Amer. Iris Soc. 57: 3–56.</reference>
  </references>
  
</bio:treatment>