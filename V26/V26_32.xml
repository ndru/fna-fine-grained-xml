<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="treatment_page">63</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="(Nuttall) Baker" date="1879" rank="genus">triantha</taxon_name>
    <taxon_name authority="(S. Watson) R. R. Gates" date="1918" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>44: 137. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus triantha;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101980</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tofieldia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>14: 283. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tofieldia;species occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tofieldia</taxon_name>
    <taxon_name authority="(Michaux) Persoon" date="unknown" rank="species">glutinosa</taxon_name>
    <taxon_name authority="(S. Watson) C. L. Hitchcock" date="unknown" rank="variety">occidentalis</taxon_name>
    <taxon_hierarchy>genus Tofieldia;species glutinosa;variety occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems leafless, or with 1–3 leaves towards base, 10–80 cm, variously glandular-hairy or only glandular below inflorescence, glands uniformly 4–6 times longer than wide.</text>
      <biological_entity id="o20700" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="leafless" value_original="leafless" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s0" value="glandular-hairy" value_original="glandular-hairy" />
        <character constraint="below inflorescence" constraintid="o20703" is_modifier="false" modifier="only" name="pubescence" src="d0_s0" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o20701" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s0" to="3" />
      </biological_entity>
      <biological_entity id="o20702" name="base" name_original="base" src="d0_s0" type="structure" />
      <biological_entity id="o20703" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
      <biological_entity id="o20704" name="gland" name_original="glands" src="d0_s0" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s0" value="4-6" value_original="4-6" />
      </biological_entity>
      <relation from="o20700" id="r2830" name="with" negation="false" src="d0_s0" to="o20701" />
      <relation from="o20701" id="r2831" name="towards" negation="false" src="d0_s0" to="o20702" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades to 50 cm × 8 mm.</text>
      <biological_entity id="o20705" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="50" to_unit="cm" />
        <character name="width" src="d0_s1" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences forming globose or cylindric-ovoid, spikelike heads, 3–45-flowered, sometimes interrupted or open, 1–8 cm, glandular-pubescent;</text>
      <biological_entity id="o20706" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-45-flowered" value_original="3-45-flowered" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="open" value_original="open" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o20707" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="globose" value_original="globose" />
        <character is_modifier="true" name="shape" src="d0_s2" value="cylindric-ovoid" value_original="cylindric-ovoid" />
        <character is_modifier="true" name="shape" src="d0_s2" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <relation from="o20706" id="r2832" name="forming" negation="false" src="d0_s2" to="o20707" />
    </statement>
    <statement id="d0_s3">
      <text>bracts subtending pedicel in cluster;</text>
      <biological_entity id="o20708" name="bract" name_original="bracts" src="d0_s3" type="structure" />
      <biological_entity id="o20709" name="pedicel" name_original="pedicel" src="d0_s3" type="structure" />
      <relation from="o20708" id="r2833" name="subtending" negation="false" src="d0_s3" to="o20709" />
    </statement>
    <statement id="d0_s4">
      <text>bracteoles shallowly and symmetrically 3-lobed to cleft from proximal 1/3 to base, lobes rounded to acute, often markedly unequal.</text>
      <biological_entity id="o20710" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="from proximal 1/3" constraintid="o20711" from="symmetrically 3-lobed" name="shape" src="d0_s4" to="cleft" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20711" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o20712" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o20713" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" modifier="often markedly" name="size" src="d0_s4" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o20711" id="r2834" name="to" negation="false" src="d0_s4" to="o20712" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers usually borne in clusters of 3, proximal sometimes remote;</text>
      <biological_entity id="o20714" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o20715" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_density" src="d0_s5" value="remote" value_original="remote" />
      </biological_entity>
      <relation from="o20714" id="r2835" name="consist_of" negation="false" src="d0_s5" to="o20715" />
    </statement>
    <statement id="d0_s6">
      <text>perianth white or yellowish;</text>
      <biological_entity id="o20716" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals 3–7 mm, inner series somewhat longer and narrower;</text>
      <biological_entity id="o20717" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o20718" name="series" name_original="series" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="somewhat" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" name="width" src="d0_s7" value="narrower" value_original="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 3–6 mm;</text>
      <biological_entity id="o20719" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary ellipsoid, tapering gradually to style base;</text>
      <biological_entity id="o20720" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character constraint="to style base" constraintid="o20721" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="style" id="o20721" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>styles distinct, 0.6–3 mm;</text>
      <biological_entity id="o20722" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicel 1–12 mm.</text>
      <biological_entity id="o20723" name="pedicel" name_original="pedicel" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules ovoid to broadly ellipsoid, 4–9 mm, clearly longer than tepals and not enclosed by them, chartaceous, easily ruptured.</text>
      <biological_entity id="o20724" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="broadly ellipsoid" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character constraint="than tepals" constraintid="o20725" is_modifier="false" name="length_or_size" src="d0_s12" value="clearly longer" value_original="clearly longer" />
        <character constraint="by them" is_modifier="false" modifier="not" name="position" notes="" src="d0_s12" value="enclosed" value_original="enclosed" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="easily" name="condition" src="d0_s12" value="ruptured" value_original="ruptured" />
      </biological_entity>
      <biological_entity id="o20725" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds reddish-brown, ca. 1 mm;</text>
      <biological_entity id="o20726" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>appendages 1 or 2 with one at each end, rarely absent;</text>
      <biological_entity id="o20727" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or with" value="1" value_original="1" />
        <character constraint="at end" constraintid="o20728" name="quantity" src="d0_s14" unit="or with" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o20728" name="end" name_original="end" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" notes="" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>coat white, inflated, reticulate.</text>
      <biological_entity id="o20729" name="coat" name_original="coat" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s15" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w Canada, nw United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w Canada" establishment_means="native" />
        <character name="distribution" value="nw United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Subspecies 3 in the flora.</discussion>
  <discussion>The subspecies of Triantha occidentalis recognized here are for the most part readily distinguishable from one another. Only in the area of southwest Oregon where subsp. occidentalis and subsp. brevistyla make contact might it be said that some intergradation occurs, as was previously observed by C. L. Hitchcock (1944). It should also be noted that some specimens of subsp. occidentalis from Del Norte County in northern California and the adjacent Josephine County in Oregon are not entirely typical, being very robust with large, more elongate inflorescences.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seed coat not strongly inflated; seeds 3–4 times longer than wide; stems coarsely glandular-pubescent with cylindrical hairs below inflorescence, hairs 4–6 times longer than wide, or glands absent</description>
      <determination>2b Triantha occidentalis subsp. montana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seed coat strongly inflated; seeds usually 1–2(–3) times longer than wide; stems glandular-pubescent below inflorescence, with glands 1/2–2 times longer than wide, or pubescent with cylindrical hairs 2–4 times longer than wide, sometimes glands and hairs intermixed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 0.6–1.5 mm; inflorescences usually cylindrical-ovoid; Oregon to Alaska, occasionally Idaho and Alberta</description>
      <determination>2a Triantha occidentalis subsp. brevistyla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 1.3–3 mm; inflorescences usually globose; California, Oregon.</description>
      <determination>2c Triantha occidentalis subsp. occidentalis</determination>
    </key_statement>
  </key>
</bio:treatment>