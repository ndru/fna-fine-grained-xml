<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="treatment_page">436</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">neomexicana</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 115. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species neomexicana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102070</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="species">harrimaniae</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) Reveal" date="unknown" rank="variety">neomexicana</taxon_name>
    <taxon_hierarchy>genus Yucca;species harrimaniae;variety neomexicana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, forming single or open colonies, each separated by 35–60 cm, acaulescent or rarely caulescent;</text>
      <biological_entity id="o11139" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="separated" value_original="separated" />
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rosettes usually small, mostly asymmetrical.</text>
      <biological_entity id="o11140" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s1" value="small" value_original="small" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s1" value="asymmetrical" value_original="asymmetrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems, when present, primarily rhizomatous, shorter than 1 m.</text>
      <biological_entity id="o11141" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when present; primarily" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character modifier="shorter than" name="some_measurement" src="d0_s2" unit="m" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade spreading, including distal leaves, spatulate-lanceolate, concavo-convex, thin, widest near middle, 15–46 × 0.7–2 cm, flexible, rather glaucous, margins entire, filiferous, brown or straw-colored.</text>
      <biological_entity id="o11142" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate-lanceolate" value_original="spatulate-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concavo-convex" value_original="concavo-convex" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character constraint="near middle leaves" constraintid="o11144" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" notes="" src="d0_s3" to="46" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="rather" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11143" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="middle" id="o11144" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" notes="" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11145" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
      <relation from="o11142" id="r1567" name="including" negation="false" src="d0_s3" to="o11143" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemose, rarely paniculate proximally, arising within or more often 0–20 cm beyond rosettes, 4–7 dm;</text>
      <biological_entity id="o11146" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="rarely; proximally" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" constraint="beyond rosettes" constraintid="o11147" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o11147" name="rosette" name_original="rosettes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>branches, when present, few, short;</text>
      <biological_entity id="o11148" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when present" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts reflexed when mature, purplish, narrowly triangular, proximal to 10 cm, distal 2–3 cm, tapering to pungent apex;</text>
      <biological_entity id="o11149" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="when mature" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11150" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11151" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
        <character char_type="range_value" from="tapering" name="shape" src="d0_s6" to="pungent" />
      </biological_entity>
      <biological_entity id="o11152" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>peduncle sometimes scapelike, 0.1–0.4 m.</text>
      <biological_entity id="o11153" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s7" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s7" to="0.4" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent;</text>
      <biological_entity id="o11154" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth campanulate;</text>
      <biological_entity id="o11155" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals distinct, white to somewhat greenish white, usually tinged pink or purple, broadly lanceolate, 3–4.7 × 1.5–3 cm;</text>
      <biological_entity id="o11156" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="somewhat" name="coloration" src="d0_s10" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="tinged pink" value_original="tinged pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="4.7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1.3–1.7 cm;</text>
      <biological_entity id="o11157" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s11" to="1.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistil 2.4–3.2 cm × 0.5–0.8 cm;</text>
      <biological_entity id="o11158" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s12" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s12" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style pale green or rarely white, 9–13 mm;</text>
      <biological_entity id="o11159" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas lobed.</text>
      <biological_entity id="o11160" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits erect, capsular, dehiscent, cylindrical, deeply constricted near middle, 3–4.2 × 2–2.7 cm, dehiscence septicidal.</text>
      <biological_entity id="o11161" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindrical" value_original="cylindrical" />
        <character constraint="near middle" constraintid="o11162" is_modifier="false" modifier="deeply" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3" from_unit="cm" name="dehiscence" notes="" src="d0_s15" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="dehiscence" notes="" src="d0_s15" to="2.7" to_unit="cm" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="septicidal" value_original="septicidal" />
      </biological_entity>
      <biological_entity id="o11162" name="middle" name_original="middle" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds dull black, thin, 6–8 × 5–8 mm.</text>
      <biological_entity id="o11163" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s16" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s16" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed rocky ledges in woodlands and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ledges" modifier="exposed" constraint="in woodlands and grasslands" />
        <character name="habitat" value="woodlands" modifier="in" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>Although J. L. Reveal (1977c) reduced Yucca neomexicana to a variety of Y. harrimaniae, the two taxa are morphologically and geographically distinct, and K. H. Clary’s (1997) DNA evidence supports their recognition as separate species.</discussion>
  
</bio:treatment>