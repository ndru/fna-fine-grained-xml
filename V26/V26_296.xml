<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="treatment_page">170</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fritillaria</taxon_name>
    <taxon_name authority="(Pursh) Sprengel" date="1825" rank="species">pudica</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>2: 64. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus fritillaria;species pudica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101628</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lilium</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">pudicum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 228, plate 8. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lilium;species pudicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb scales: large 4–5;</text>
      <biological_entity constraint="bulb" id="o23914" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s0" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>small 85–125.</text>
      <biological_entity constraint="bulb" id="o23915" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="85" name="quantity" src="d0_s1" to="125" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem 0.7–3 dm.</text>
      <biological_entity id="o23916" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2–8, subopposite to scattered, 3–20 cm;</text>
      <biological_entity id="o23917" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="8" />
        <character char_type="range_value" from="subopposite" name="arrangement" src="d0_s3" to="scattered" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to lanceolate.</text>
      <biological_entity id="o23918" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers nodding;</text>
      <biological_entity id="o23919" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals yellow to orange, some lined brown, aging to brick-red, 0.8–2.2 cm;</text>
      <biological_entity id="o23920" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="orange" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="lined brown" value_original="lined brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brick-red" value_original="brick-red" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s6" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>nectaries near base of tepals, green, elliptic to round;</text>
      <biological_entity id="o23921" name="nectary" name_original="nectaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="round" />
      </biological_entity>
      <biological_entity id="o23922" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o23923" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <relation from="o23921" id="r3247" name="near" negation="false" src="d0_s7" to="o23922" />
      <relation from="o23922" id="r3248" name="part_of" negation="false" src="d0_s7" to="o23923" />
    </statement>
    <statement id="d0_s8">
      <text>style unbranched.</text>
      <biological_entity id="o23924" name="style" name_original="style" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules angled.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 24, 26.</text>
      <biological_entity id="o23925" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23926" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
        <character name="quantity" src="d0_s10" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy, shrubby, or wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded slopes" modifier="grassy shrubby or" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Yellow fritillary</other_name>
  <discussion>Fritillaria pudica is highly variable and has one of the widest distributions of all the North American species of the genus. It was commonly used as food by Native Americans. The small bulbs were often eaten raw, and the larger ones were either dried or cooked in various ways. The Okanogan-Colville tribe used the appearance of F. pudica flowers as a sign that spring had arrived, and the Shuswap tribe used them in bouquets.</discussion>
  
</bio:treatment>