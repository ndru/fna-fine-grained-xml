<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">633</other_info_on_meta>
    <other_info_on_meta type="mention_page">636</other_info_on_meta>
    <other_info_on_meta type="treatment_page">635</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="(Willdenow) Poiret" date="1818" rank="species">odontorhiza</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 10: 375. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species odontorhiza;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101535</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cymbidium</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">odontorhizon</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4(1): 110. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cymbidium;species odontorhizon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">micrantha</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species micrantha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">pringlei</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species pringlei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems yellow, greenish, or purplish brown, base strongly thickened, bulbous.</text>
      <biological_entity id="o25768" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="purplish brown" value_original="purplish brown" />
      </biological_entity>
      <biological_entity id="o25769" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="strongly" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: racemes lax, 11–46 × 1–2.5 cm.</text>
      <biological_entity id="o25770" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o25771" name="raceme" name_original="racemes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="lax" value_original="lax" />
        <character char_type="range_value" from="11" from_unit="cm" name="length" src="d0_s1" to="46" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 5–26, inconspicuous;</text>
      <biological_entity id="o25772" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="26" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>perianth open or closed;</text>
      <biological_entity id="o25773" name="perianth" name_original="perianth" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="condition" src="d0_s3" value="closed" value_original="closed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals and petals reddish purple to brown, often suffused with green, broadly lanceolate, 1–2-veined;</text>
      <biological_entity id="o25774" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="reddish purple" name="coloration" src="d0_s4" to="brown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="suffused with green" value_original="suffused with green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2-veined" value_original="1-2-veined" />
      </biological_entity>
      <biological_entity id="o25775" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="reddish purple" name="coloration" src="d0_s4" to="brown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="suffused with green" value_original="suffused with green" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2-veined" value_original="1-2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral sepals strongly curved upward and directed forward, 3–4.5 mm;</text>
      <biological_entity constraint="lateral" id="o25776" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="forward" name="orientation" src="d0_s5" value="directed" value_original="directed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 2.5–3.8 mm, connivent with dorsal sepal to form hood over column, or sepals and petals connivent to produce closed flower;</text>
      <biological_entity id="o25777" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.8" to_unit="mm" />
        <character constraint="with dorsal sepal" constraintid="o25778" is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o25778" name="sepal" name_original="sepal" src="d0_s6" type="structure" />
      <biological_entity id="o25779" name="form" name_original="form" src="d0_s6" type="structure" />
      <biological_entity id="o25780" name="hood" name_original="hood" src="d0_s6" type="structure" />
      <biological_entity id="o25781" name="column" name_original="column" src="d0_s6" type="structure" />
      <biological_entity id="o25782" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o25783" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o25784" name="flower" name_original="flower" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition" src="d0_s6" value="closed" value_original="closed" />
      </biological_entity>
      <relation from="o25778" id="r3493" name="to" negation="false" src="d0_s6" to="o25779" />
      <relation from="o25778" id="r3494" name="to" negation="false" src="d0_s6" to="o25780" />
      <relation from="o25779" id="r3495" name="over" negation="false" src="d0_s6" to="o25781" />
      <relation from="o25780" id="r3496" name="over" negation="false" src="d0_s6" to="o25781" />
      <relation from="o25782" id="r3497" name="produce" negation="false" src="d0_s6" to="o25784" />
      <relation from="o25783" id="r3498" name="produce" negation="false" src="d0_s6" to="o25784" />
    </statement>
    <statement id="d0_s7">
      <text>lip white, often spotted with purple, ovate to orbiculate-quadrangular, unlobed, 2.7–4.6 ×1.8–3.7 mm, thin, with 2 distinct basal lamellae, margins entire to erose-fringed;</text>
      <biological_entity id="o25785" name="lip" name_original="lip" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="spotted with purple" value_original="spotted with purple" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="orbiculate-quadrangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s7" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s7" to="3.7" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="basal" id="o25786" name="lamella" name_original="lamellae" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o25787" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s7" to="erose-fringed" />
      </biological_entity>
      <relation from="o25785" id="r3499" name="with" negation="false" src="d0_s7" to="o25786" />
    </statement>
    <statement id="d0_s8">
      <text>column white basally, often purple apically, 1.9–2.5 mm, straight or curved toward lip, with 2 distinct auricles in open-flowered plants;</text>
      <biological_entity id="o25788" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="often; apically" name="coloration_or_density" src="d0_s8" value="purple" value_original="purple" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character constraint="toward lip" constraintid="o25789" is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o25789" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <biological_entity id="o25790" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o25791" name="plant" name_original="plants" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="open-flowered" value_original="open-flowered" />
      </biological_entity>
      <relation from="o25788" id="r3500" name="with" negation="false" src="d0_s8" to="o25790" />
      <relation from="o25790" id="r3501" name="in" negation="false" src="d0_s8" to="o25791" />
    </statement>
    <statement id="d0_s9">
      <text>ovary 2.5–5 mm;</text>
      <biological_entity id="o25792" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>mentum inconspicuous.</text>
      <biological_entity id="o25793" name="mentum" name_original="mentum" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules broadly ellipsoid, 5.5–8 × 3.5–5 mm.</text>
      <biological_entity id="o25794" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Fall coral-root</other_name>
  <other_name type="common_name">small-flowered coral-root</other_name>
  <other_name type="common_name">corallorhize d’automne</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth tending to be closed, column without or with only poorly developed auricles at base on adaxial surface; stigmatic surface 0.3–0.5 mm wide; lip 1.7–2.2(–3) mm wide.</description>
      <determination>3a Corallorhiza odontorhiza var. odontorhiza</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perianth open, column with 2 prominent auricles at base on adaxial surface; stigmatic surface 0.7–1 mm wide; lip 2–3.7 mm wide.</description>
      <determination>3b Corallorhiza odontorhiza var. pringlei</determination>
    </key_statement>
  </key>
</bio:treatment>