<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">616</other_info_on_meta>
    <other_info_on_meta type="treatment_page">617</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Kunth" date="1815" rank="tribe">Epidendreae</taxon_name>
    <taxon_name authority="Lindley" date="1830" rank="subtribe">PLEUROTHALLIDINAE</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">pleurothallis</taxon_name>
    <taxon_name authority="Lindley" date="1841" rank="species">gelida</taxon_name>
    <place_of_publication>
      <publication_title>Edwards’s Bot. Reg.</publication_title>
      <place_in_publication>27: Misc. 91. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe epidendreae;subtribe pleurothallidinae;genus pleurothallis;species gelida;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101858</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, 10–45 cm.</text>
      <biological_entity id="o19715" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots coarse.</text>
      <biological_entity id="o19716" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="relief" src="d0_s1" value="coarse" value_original="coarse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 10–25 cm;</text>
      <biological_entity id="o19717" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths 2–3, loose, inflated.</text>
      <biological_entity id="o19718" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 1–4 cm;</text>
      <biological_entity id="o19719" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o19720" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly elliptic to oblanceolate, 10–20 × 2.5–10 cm, base cuneate, apex obtuse.</text>
      <biological_entity id="o19721" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o19722" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19723" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o19724" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences apical, many-flowered racemes, simultaneous, 10–25 cm;</text>
      <biological_entity id="o19725" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o19726" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="many-flowered" value_original="many-flowered" />
        <character is_modifier="false" name="development" src="d0_s6" value="simultaneous" value_original="simultaneous" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spathe 1–2 cm;</text>
      <biological_entity id="o19727" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 2–3 mm;</text>
      <biological_entity id="o19728" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2–4 mm.</text>
      <biological_entity id="o19729" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals distinct, light yellow, white-pubescent, apex obtuse;</text>
      <biological_entity id="o19730" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19731" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="light yellow" value_original="light yellow" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="white-pubescent" value_original="white-pubescent" />
      </biological_entity>
      <biological_entity id="o19732" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>dorsal sepal oblong to obovate, concave, 5–8 × 2.5–3 mm;</text>
      <biological_entity id="o19733" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="dorsal" id="o19734" name="sepal" name_original="sepal" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="concave" value_original="concave" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lateral sepals connate near base to form cup, oblong, ovate, oblique, 5–7.5 × 1.5–2.5 mm, apex obtuse;</text>
      <biological_entity id="o19735" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="lateral" id="o19736" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character constraint="near base" constraintid="o19737" is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="orientation_or_shape" src="d0_s12" value="oblique" value_original="oblique" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19737" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o19738" name="form" name_original="form" src="d0_s12" type="structure" />
      <biological_entity id="o19739" name="cup" name_original="cup" src="d0_s12" type="structure" />
      <biological_entity id="o19740" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o19737" id="r2705" name="to" negation="false" src="d0_s12" to="o19738" />
      <relation from="o19737" id="r2706" name="to" negation="false" src="d0_s12" to="o19739" />
    </statement>
    <statement id="d0_s13">
      <text>petals translucent white, 3-veined, oblong to obovate, 3–3.5 × 1.75–2 mm, apex obtuse, erose or notched;</text>
      <biological_entity id="o19741" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19742" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="translucent white" value_original="translucent white" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.75" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19743" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lip greenish white, 3-veined, oblong to nearly pandurate, curved, 2–2.5 × 0.75–2.25 mm, apex rounded to truncate;</text>
      <biological_entity id="o19744" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19745" name="lip" name_original="lip" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="nearly pandurate" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s14" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="width" src="d0_s14" to="2.25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19746" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s14" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>disc with pair of lamellae near middle;</text>
      <biological_entity id="o19747" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19748" name="disc" name_original="disc" src="d0_s15" type="structure" />
      <biological_entity id="o19749" name="pair" name_original="pair" src="d0_s15" type="structure" />
      <biological_entity id="o19750" name="lamella" name_original="lamellae" src="d0_s15" type="structure" />
      <biological_entity constraint="middle" id="o19751" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <relation from="o19748" id="r2707" name="with" negation="false" src="d0_s15" to="o19749" />
      <relation from="o19749" id="r2708" name="part_of" negation="false" src="d0_s15" to="o19750" />
      <relation from="o19749" id="r2709" name="near" negation="false" src="d0_s15" to="o19751" />
    </statement>
    <statement id="d0_s16">
      <text>column 2.5 mm, hooded;</text>
      <biological_entity id="o19752" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o19753" name="column" name_original="column" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="hooded" value_original="hooded" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary 2 mm.</text>
      <biological_entity id="o19754" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o19755" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character name="some_measurement" src="d0_s17" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter and spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–60 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="60" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Greater Antilles); Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Greater Antilles)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Pleurothallis gelida is recognized by a loosely sheathed, stout stem with a solitary, obtuse leaf; several long racemes of small, yellowish flowers that are pubescent within; and fused lateral sepals. It is uncommon in the Fahkahatchee Swamp in Florida.</discussion>
  
</bio:treatment>