<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">59</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Hudson" date="1778" rank="genus">tofieldia</taxon_name>
    <taxon_name authority="(Michaux) Persoon" date="1805" rank="species">pusilla</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 399. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus tofieldia;species pusilla</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242101977</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Narthecium</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">pusillum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 209. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Narthecium;species pusillum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1.5–30 cm.</text>
      <biological_entity id="o32013" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades to 8 cm × 3 mm.</text>
      <biological_entity id="o32014" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character name="width" src="d0_s1" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences open, 3–35-flowered, 0.3–5 cm;</text>
      <biological_entity id="o32015" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-35-flowered" value_original="3-35-flowered" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bracts whitish, margins deeply 3-lobed;</text>
      <biological_entity id="o32016" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o32017" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracteoles absent.</text>
      <biological_entity id="o32018" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: tepals white to yellowish green, 1.5–3 mm, inner tepals slightly longer than outer;</text>
      <biological_entity id="o32019" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o32020" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s5" to="yellowish green" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o32021" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character constraint="than outer tepals" constraintid="o32022" is_modifier="false" name="length_or_size" src="d0_s5" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="outer" id="o32022" name="tepal" name_original="tepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stamens ± equaling tepals;</text>
      <biological_entity id="o32023" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o32024" name="stamen" name_original="stamens" src="d0_s6" type="structure" />
      <biological_entity id="o32025" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary ellipsoid;</text>
      <biological_entity id="o32026" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o32027" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles 0.3–0.4 mm;</text>
      <biological_entity id="o32028" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o32029" name="style" name_original="styles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel 1–3 mm.</text>
      <biological_entity id="o32030" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32031" name="pedicel" name_original="pedicel" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules subglobose, 2.5–3 mm.</text>
      <biological_entity id="o32032" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 0.6–0.8 mm. 2n = 30.</text>
      <biological_entity id="o32033" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32034" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, open areas, often calcareous</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="calcareous" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Mich., Minn., Mont.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Scotch false asphodel</other_name>
  <discussion>What appear to be hybrids between Tofieldia pusilla and T. coccinea occasionally occur in Greenland and the Northwest Territories. They are characterized by a leafy bract at or near the middle of the stem and enlarged, crimson bracts below the pedicels. In some suspected hybrids, the bracts are absent, and fused bracteoles, trunctate or lobed, sheathe the pedicels well below the flowers.</discussion>
  
</bio:treatment>