<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">604</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="treatment_page">605</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="Rafinesque" date="1825" rank="genus">hexalectris</taxon_name>
    <taxon_name authority="L. O. Williams" date="1944" rank="species">nitida</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>25: 81. 1944</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus hexalectris;species nitida;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101664</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems pinkish, pale-red, or brown-purple, 10–32 cm;</text>
      <biological_entity id="o10858" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown-purple" value_original="brown-purple" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-red" value_original="pale-red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown-purple" value_original="brown-purple" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="32" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheathing bracts 3–5.</text>
      <biological_entity id="o10859" name="bract" name_original="bracts" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: floral bracts ovate-oblong to lanceolate, 3–7 × 3 mm.</text>
      <biological_entity id="o10860" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="floral" id="o10861" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="7" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 6–24, pedicellate;</text>
      <biological_entity id="o10862" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s3" to="24" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals and petals spreading and recurved, cleistogamous to chasmogamous;</text>
      <biological_entity id="o10863" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="cleistogamous" name="reproduction" src="d0_s4" to="chasmogamous" />
      </biological_entity>
      <biological_entity id="o10864" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="cleistogamous" name="reproduction" src="d0_s4" to="chasmogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal narrowly oblongelliptic, 8–13 × 3–4.5 mm, apex obtuse;</text>
      <biological_entity constraint="dorsal" id="o10865" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10866" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals obliquely elliptic to oblanceolate, rounded, slightly falcate, 7–12 × 3–4.5 mm;</text>
      <biological_entity constraint="lateral" id="o10867" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="obliquely elliptic" name="shape" src="d0_s6" to="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals obovate to oblanceolate, falcate, 8–11 × 2–3.5 mm, apically rounded;</text>
      <biological_entity id="o10868" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip shallowly 3-lobed, 7–11 × 4–8 mm, middle lobe suborbiculate, pink to purple, lateral lobes 1/3 length of middle lobe;</text>
      <biological_entity id="o10869" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s8" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o10870" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s8" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>minimal lamellae or slightly raised veins 5 or 7, near base of middle lobe, mostly obscured apically;</text>
      <biological_entity constraint="lateral" id="o10871" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" value="1/3 length of middle lobe" />
      </biological_entity>
      <biological_entity id="o10872" name="lamella" name_original="lamellae" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="5" value_original="5" />
        <character name="quantity" src="d0_s9" unit="or" value="7" value_original="7" />
        <character is_modifier="false" modifier="mostly; apically" name="prominence" notes="" src="d0_s9" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o10873" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o10874" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="middle" id="o10875" name="lobe" name_original="lobe" src="d0_s9" type="structure" />
      <relation from="o10872" id="r1540" modifier="slightly" name="raised" negation="false" src="d0_s9" to="o10873" />
      <relation from="o10872" id="r1541" name="near" negation="false" src="d0_s9" to="o10874" />
      <relation from="o10874" id="r1542" name="part_of" negation="false" src="d0_s9" to="o10875" />
    </statement>
    <statement id="d0_s10">
      <text>column white, 6–8 mm;</text>
      <biological_entity id="o10876" name="column" name_original="column" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anther yellow.</text>
      <biological_entity id="o10877" name="anther" name_original="anther" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 15 × 5 mm.</text>
      <biological_entity id="o10878" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="15" value_original="15" />
        <character name="width" src="d0_s12" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist canyons in oak-juniper-pinyon pine woodlands growing in humus, often in decaying juniper needle litter</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist canyons" constraint="in oak-juniper-pinyon pine" />
        <character name="habitat" value="oak-juniper-pinyon pine" />
        <character name="habitat" value="humus" modifier="woodlands growing in" />
        <character name="habitat" value="juniper needle litter" modifier="often in decaying" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Glass Mountain coral-root</other_name>
  <other_name type="common_name">shining cock’s-comb</other_name>
  <discussion>The Texas plants occur in the Chisos and Glass mountains, on the eastern and southern Edwards Plateau, and in the Dallas area. Most of the plants on the Edwards Plateau appear to have cleistogamous flowers, and only occasionally display open flowers (J. Liggio and A. Liggio 1999). Plants of Hexalectris nitida examined from the United States have been self-pollinating because of a reduced rostellum. In central Texas, in particular, flowers of this species are cleistogamous. They also are found under Juniperus in that area (V. S. Engel 1987) and flower well after ample late–spring rain.</discussion>
  
</bio:treatment>