<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak,Paul M. Catling,Susan J. Meades,Richard M. Bateman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">579</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="treatment_page">577</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Necker ex Nevski" date="unknown" rank="genus">DACTYLORHIZA</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Bot. Inst. S.S.S.R., Ser. 1, Fl. Sist. Vyssh. Rast.</publication_title>
      <place_in_publication>4: 332. 1937</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus dactylorhiza;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek dactylos, finger, and rhiza, root, in reference to the fingerlike tuberoids of the more primitive species</other_info_on_name>
    <other_info_on_name type="fna_id">109214</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial, rather succulent, glabrous.</text>
      <biological_entity id="o16608" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots from base of stem fascicled tuberoids, usually palmately divided with 2–5 lobes, fleshy.</text>
      <biological_entity id="o16609" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="with lobes" constraintid="o16613" is_modifier="false" modifier="usually palmately" name="shape" notes="" src="d0_s1" value="divided" value_original="divided" />
        <character is_modifier="false" name="texture" notes="" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o16610" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o16611" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o16612" name="tuberoid" name_original="tuberoids" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
      </biological_entity>
      <biological_entity id="o16613" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
      <relation from="o16609" id="r2269" name="from" negation="false" src="d0_s1" to="o16610" />
      <relation from="o16609" id="r2270" name="from" negation="false" src="d0_s1" to="o16611" />
      <relation from="o16609" id="r2271" name="from" negation="false" src="d0_s1" to="o16612" />
    </statement>
    <statement id="d0_s2">
      <text>Stems leafy.</text>
      <biological_entity id="o16614" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves several, ascending to recurved, not enfolded around spike, with or without purplish spots;</text>
      <biological_entity id="o16615" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="recurved" />
      </biological_entity>
      <biological_entity id="o16616" name="spike" name_original="spike" src="d0_s3" type="structure" />
      <relation from="o16615" id="r2272" modifier="around" name="enfolded" negation="true" src="d0_s3" to="o16616" />
    </statement>
    <statement id="d0_s4">
      <text>base sheathing in proximal leaves, distal leaves bractlike, not sheathing.</text>
      <biological_entity id="o16617" name="base" name_original="base" src="d0_s4" type="structure">
        <character constraint="in proximal leaves" constraintid="o16618" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16618" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o16619" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, spikes;</text>
      <biological_entity id="o16620" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o16621" name="spike" name_original="spikes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>floral bracts foliaceous, prominent.</text>
      <biological_entity constraint="floral" id="o16622" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers few-to-many, resupinate;</text>
      <biological_entity id="o16623" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s7" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>dorsal sepal, sometimes lateral sepals, and petals connivent, forming hood distal to lip;</text>
      <biological_entity constraint="dorsal" id="o16624" name="sepal" name_original="sepal" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="connivent" value_original="connivent" />
        <character constraint="to lip" constraintid="o16628" is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o16625" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="connivent" value_original="connivent" />
        <character constraint="to lip" constraintid="o16628" is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o16626" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="connivent" value_original="connivent" />
        <character constraint="to lip" constraintid="o16628" is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o16627" name="hood" name_original="hood" src="d0_s8" type="structure" />
      <biological_entity id="o16628" name="lip" name_original="lip" src="d0_s8" type="structure" />
      <relation from="o16624" id="r2273" name="forming" negation="false" src="d0_s8" to="o16627" />
      <relation from="o16625" id="r2274" name="forming" negation="false" src="d0_s8" to="o16627" />
      <relation from="o16626" id="r2275" name="forming" negation="false" src="d0_s8" to="o16627" />
    </statement>
    <statement id="d0_s9">
      <text>petals ± obliquely dilated basally;</text>
      <biological_entity id="o16629" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less obliquely; basally" name="shape" src="d0_s9" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip 3-lobed, base spurred, margins occasionally entire, nectarless;</text>
      <biological_entity id="o16630" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o16631" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o16632" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="nectarless" value_original="nectarless" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinaria 2, each with 1 pollen mass;</text>
      <biological_entity id="o16633" name="pollinarium" name_original="pollinaria" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="pollen" id="o16634" name="mass" name_original="mass" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o16633" id="r2276" name="with" negation="false" src="d0_s11" to="o16634" />
    </statement>
    <statement id="d0_s12">
      <text>viscidia within single 2-lobed bursicle;</text>
      <biological_entity id="o16635" name="viscidium" name_original="viscidia" src="d0_s12" type="structure" />
      <biological_entity id="o16636" name="bursicle" name_original="bursicle" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character is_modifier="true" name="shape" src="d0_s12" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <relation from="o16635" id="r2277" name="within" negation="false" src="d0_s12" to="o16636" />
    </statement>
    <statement id="d0_s13">
      <text>stigma reniform or obcordate, concave with median ridge, hidden behind bursicle.</text>
      <biological_entity id="o16637" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obcordate" value_original="obcordate" />
        <character constraint="with median ridge" constraintid="o16638" is_modifier="false" name="shape" src="d0_s13" value="concave" value_original="concave" />
        <character constraint="behind bursicle" constraintid="o16639" is_modifier="false" name="prominence" notes="" src="d0_s13" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity constraint="median" id="o16638" name="ridge" name_original="ridge" src="d0_s13" type="structure" />
      <biological_entity id="o16639" name="bursicle" name_original="bursicle" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, ascending, ellipsoid.</text>
      <biological_entity constraint="fruits" id="o16640" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska, Canada, mostly Eurasian.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Canada" establishment_means="native" />
        <character name="distribution" value="mostly Eurasian" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Orchis</other_name>
  <discussion>Species ca. 75 (2 in the flora).</discussion>
  <discussion>Dactylorhiza is a taxonomically complex genus in which closely related species have been combined into species aggregates (P. Vermeulen 1947; R. M. Bateman et al. 1997). Recognition of the aggregate taxa alone reduces the number of species by more than half. Recent research synthesizing morphometric and allozyme data to circumscribe species (R. M. Bateman and I. Denholm 1983, 1985, 1989; M. Hédren 1996), and DNA sequences and chromosome studies to determine the relationships of those species (R. M. Bateman et al. 1997; A. M. Pridgeon et al. 1997), is shedding much light on the evolution of the genus. The diploid lineage (2n = 40) appears to have evolved in Asia, migrating and speciating to both the west and northeast. Several alloploidy events (hybridization followed by chromosome doubling) occurred recently in Europe, apparently between the distinct diploids D. fuchsii and D. incarnata. That generated a highly complex suite of poorly distinguishable “prospecies” of 2n = 80, treated as a single species by some authorities and as many species by others (L. V. Averyanov 1990). Of the two North American species, D. aristata is a native diploid originating during the northeasterly migration, and D. majalis is an allotetraploid that originated in Europe and presumably is naturalized in North America (H. J. Clase and S. J. Meades 1996).</discussion>
  <discussion>Despite an extensive literature, much taxonomic work still remains to be done.</discussion>
  <references>
    <reference>Averyanov, L. V. 1990. A review of the genus Dactylorhiza. In: J. Arditti, ed. 1977+. Orchid Biology: Reviews and Perspectives. 7+ vols. Ithaca, N.Y. and New York. Vol. 5, pp. 159–206.  </reference>
    <reference>Bateman, R. M. and I. Denholm. 1983. A reappraisal of the British and Irish dactylorchids: 1. The tetraploid marsh orchids. Watsonia 14: 347–376.  </reference>
    <reference>Bateman, R. M. and I. Denholm. 1985. A reappraisal of the British and Irish dactylorchids: 2. The diploid marsh orchids. Watsonia 15: 321–355.  </reference>
    <reference>Bateman, R. M. and I. Denholm. 1989. A reappraisal of the British and Irish dactylorchids: 3. The spotted orchids. Watsonia 17: 319–349.  </reference>
    <reference>Bateman, R. M., A. M. Pridgeon, and M. W. Chase. 1997. Phylogenetics of subtribe Orchidinae (Orchidoideae, Orchidaceae) based on nuclear ITS sequences: 2. Infrageneric relationships and taxonomic revision to achieve monophyly of Orchis sensu stricto. Lindleyana 12: 113–141.   </reference>
    <reference>Hédren, M. 1996. Genetic differentiation, polyploidization and hybridization in northern European Dactylorhiza (Orchidaceae): Evidence from allozyme markers. Pl. Syst. Evol. 201: 31–55. </reference>
    <reference>Pridgeon, A. M. et al. 1997. Phylogenetics of subtribe Orchidinae (Orchidoideae, Orchidaceae) based on nuclear ITS sequences: 1. Intergeneric relationships and polyphyly of Orchis sensu lato. Lindleyana 12: 89–109.  </reference>
    <reference>Stace, C. A. 1991. New Flora of the British Isles. Cambridge and New York. Pp. 1166–1173.  </reference>
    <reference>Tyteca, D. and J.-L. Gathoye. 1993. On the morphological variability of Dactylorhiza praetermissa (Druce) Soó (Orchidaceae). Belg. J. Bot. 126: 81–99.  </reference>
    <reference>Vermeulen, P. 1947. Studies on Dactylorchids. Utrecht.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and petals awned; lip often magenta- to red-spotted.</description>
      <determination>1 Dactylorhiza aristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and petals obtuse, hoodlike, or acute, not awned; lip with red-violet loops (continuous or discontinuous), dashes, and spots.</description>
      <determination>2 Dactylorhiza majalis</determination>
    </key_statement>
  </key>
</bio:treatment>