<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="treatment_page">625</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Malaxideae</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">liparis</taxon_name>
    <taxon_name authority="(Linnaeus) Richard ex Lindley" date="1825" rank="species">liliifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Reg.</publication_title>
      <place_in_publication>11: plate 882. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe malaxideae;genus liparis;species liliifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101748</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophrys</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">liliifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 946. 1753 (as lilifolia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ophrys;species liliifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptorchis</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="unknown" rank="species">liliifolia</taxon_name>
    <taxon_hierarchy>genus Leptorchis;species liliifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malaxis</taxon_name>
    <taxon_name authority="(Linnaeus) Swartz" date="unknown" rank="species">liliifolia</taxon_name>
    <taxon_hierarchy>genus Malaxis;species liliifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophrys</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">trifolia</taxon_name>
    <taxon_hierarchy>genus Ophrys;species trifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 9–25 cm.</text>
      <biological_entity id="o16949" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pseudobulbs ovoid, 20 ×10 mm, sheathed by bracts and persistent leaf-bases, previous year’s pseudobulb usually present, connected by short rhizome.</text>
      <biological_entity id="o16950" name="pseudobulb" name_original="pseudobulbs" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovoid" value_original="ovoid" />
        <character name="length" src="d0_s1" unit="mm" value="20" value_original="20" />
        <character name="width" src="d0_s1" unit="mm" value="10" value_original="10" />
        <character constraint="by leaf-bases" constraintid="o16952" is_modifier="false" name="architecture" src="d0_s1" value="sheathed" value_original="sheathed" />
        <character is_modifier="false" name="growth_order" notes="" src="d0_s1" value="previous" value_original="previous" />
      </biological_entity>
      <biological_entity id="o16951" name="bract" name_original="bracts" src="d0_s1" type="structure" />
      <biological_entity id="o16952" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o16953" name="pseudobulb" name_original="pseudobulb" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character constraint="by rhizome" constraintid="o16954" is_modifier="false" name="fusion" src="d0_s1" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity id="o16954" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems bright green, with or without purplish brown tinge, angled, sometimes obscurely winged or fluted distally.</text>
      <biological_entity id="o16955" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character is_modifier="false" modifier="without with or purplish brown tinge" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="sometimes obscurely" name="architecture" src="d0_s2" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="fluted" value_original="fluted" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2;</text>
      <biological_entity id="o16956" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade conduplicate, dark green, glossy, ovate-elliptic, keeled abaxially, 4–18 × 2–8.5 cm, succulent, apex obtuse to acute.</text>
      <biological_entity id="o16957" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s4" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="18" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="8.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o16958" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 4–15 cm;</text>
      <biological_entity id="o16959" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bracts subulate, 2 × 1 mm, apex acute;</text>
      <biological_entity constraint="floral" id="o16960" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
        <character name="length" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character name="width" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16961" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels slender, 5–7 mm.</text>
      <biological_entity id="o16962" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 5–31, mauve-purple and green;</text>
      <biological_entity id="o16963" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="31" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="mauve-purple and green" value_original="mauve-purple and green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>dorsal sepal oblong-lanceolate to narrowly lanceolate, 8–11.5 × 1.2–2 mm, apex obtuse to acute;</text>
      <biological_entity constraint="dorsal" id="o16964" name="sepal" name_original="sepal" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s9" to="narrowly lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16965" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral sepals oblong-lanceolate to narrowly lanceolate, 8–11.5 × 1.2–2 mm, apex obtuse to acute;</text>
      <biological_entity constraint="lateral" id="o16966" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s10" to="narrowly lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="11.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16967" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pendent, curved, tubular, narrowly linear to filiform, 8.5–12 × 0.2–0.3 mm, margins strongly revolute;</text>
      <biological_entity id="o16968" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character char_type="range_value" from="narrowly linear" name="shape" src="d0_s11" to="filiform" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16969" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s11" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lip translucent, mauve to pale-purple or rarely green with prominent purplish veining, cuneate-obovate to suborbiculate, 8–12 × 6–10 mm, base slightly auriculate, apical margin erose-serrulate, apex subtruncate, mucronate;</text>
      <biological_entity id="o16970" name="lip" name_original="lip" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="translucent" value_original="translucent" />
        <character char_type="range_value" constraint="with base, margin, apex" constraintid="o16971, o16972, o16973" from="mauve" name="coloration" src="d0_s12" to="pale-purple or rarely green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o16971" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="cuneate-obovate" is_modifier="true" name="shape" src="d0_s12" to="suborbiculate" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" is_modifier="true" name="width" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="erose-serrulate" value_original="erose-serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o16972" name="margin" name_original="margin" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="cuneate-obovate" is_modifier="true" name="shape" src="d0_s12" to="suborbiculate" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" is_modifier="true" name="width" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="erose-serrulate" value_original="erose-serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o16973" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="cuneate-obovate" is_modifier="true" name="shape" src="d0_s12" to="suborbiculate" />
        <character char_type="range_value" from="8" from_unit="mm" is_modifier="true" name="length" src="d0_s12" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" is_modifier="true" name="width" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="erose-serrulate" value_original="erose-serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o16974" name="base" name_original="base" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>column winged apically, 3–4 × 1–1.5 mm, with 2 blunt tubercles on inner surface near base;</text>
      <biological_entity id="o16975" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16976" name="tubercle" name_original="tubercles" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s13" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity constraint="inner" id="o16977" name="surface" name_original="surface" src="d0_s13" type="structure" />
      <biological_entity id="o16978" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o16975" id="r2332" name="with" negation="false" src="d0_s13" to="o16976" />
      <relation from="o16976" id="r2333" name="on" negation="false" src="d0_s13" to="o16977" />
      <relation from="o16977" id="r2334" name="near" negation="false" src="d0_s13" to="o16978" />
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow;</text>
      <biological_entity id="o16979" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollinia yellow.</text>
      <biological_entity id="o16980" name="pollinium" name_original="pollinia" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules: pedicel 11–18 mm;</text>
      <biological_entity id="o16981" name="capsule" name_original="capsules" src="d0_s16" type="structure" />
      <biological_entity id="o16982" name="pedicel" name_original="pedicel" src="d0_s16" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s16" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>body ellipsoid, 15 × 5 mm, veins often slightly winged.</text>
      <biological_entity id="o16983" name="capsule" name_original="capsules" src="d0_s17" type="structure" />
      <biological_entity id="o16984" name="body" name_original="body" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s17" unit="mm" value="15" value_original="15" />
        <character name="width" src="d0_s17" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o16985" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="often slightly" name="architecture" src="d0_s17" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul (north).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="north" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mature mesic to moist deciduous forests, pine woods, rich moist humus, often colonizing previously open, disturbed habitats during early and middle stages of reforestation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mature mesic to moist deciduous forests" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="rich moist humus" />
        <character name="habitat" value="open" modifier="previously" constraint="during early and middle stages of reforestation" />
        <character name="habitat" value="disturbed habitats" constraint="during early and middle stages of reforestation" />
        <character name="habitat" value="early" constraint="of reforestation" />
        <character name="habitat" value="middle stages" constraint="of reforestation" />
        <character name="habitat" value="reforestation" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Iowa, Ky., Md., Mass., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Lily-leaved twayblade</other_name>
  <other_name type="common_name">large twayblade</other_name>
  <other_name type="common_name">mauve sleekwort</other_name>
  <discussion>Liparis liliifolia has what appear to be extremely similar relatives in Japan [L. japonica (Miquel) Maximowicz and L. makinoana Schlechter] and China (L. pauliana Handel-Mazzetti and L. cathcartii Hooker f.). Further work is needed to clarify the relationships among these five species. It could be a case of either divergent evolution from a common ancestor or convergent evolution from two or more ancestors.</discussion>
  
</bio:treatment>