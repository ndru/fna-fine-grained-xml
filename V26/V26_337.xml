<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gerald B. Straley†,Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TULIPA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 305. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 145. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus TULIPA</taxon_hierarchy>
    <other_info_on_name type="etymology">Persian thoulyban or Turkish tulbend, turban, alluding to the shape of the just-opening perianth</other_info_on_name>
    <other_info_on_name type="fna_id">133974</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, scapose or subscapose, bulbose;</text>
      <biological_entity id="o23045" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="bulbose" value_original="bulbose" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulbs often stoloniferous, tunicate, papery to coriaceous;</text>
      <biological_entity id="o23046" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tunicate" value_original="tunicate" />
        <character char_type="range_value" from="papery" name="texture" src="d0_s1" to="coriaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tunics variously hairy or glabrous adaxially.</text>
      <biological_entity id="o23047" name="tunic" name_original="tunics" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2–6 (–12), cauline, alternate, reduced distally;</text>
      <biological_entity id="o23048" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="12" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o23049" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to oblong, somewhat fleshy.</text>
      <biological_entity id="o23050" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="oblong" />
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1 (–4) -flowered, bracts usually absent.</text>
      <biological_entity id="o23051" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1(-4)-flowered" value_original="1(-4)-flowered" />
      </biological_entity>
      <biological_entity id="o23052" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: perianth campanulate to cupshaped;</text>
      <biological_entity id="o23053" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o23054" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="cupshaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals caducous, 6, distinct, often blotched near base, petaloid, ± equal;</text>
      <biological_entity id="o23055" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o23056" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s7" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="near base" constraintid="o23057" is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="blotched" value_original="blotched" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o23057" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>nectaries absent;</text>
      <biological_entity id="o23058" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23059" name="nectary" name_original="nectaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 6, distinct;</text>
      <biological_entity id="o23060" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23061" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments shorter than tepals, basally dilated;</text>
      <biological_entity id="o23062" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23063" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character constraint="than tepals" constraintid="o23064" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s10" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o23064" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers basifixed, linear to narrowly elliptic, introrse;</text>
      <biological_entity id="o23065" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23066" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s11" value="basifixed" value_original="basifixed" />
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s11" to="narrowly elliptic" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, 3-locular;</text>
      <biological_entity id="o23067" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23068" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style very short or absent;</text>
      <biological_entity id="o23069" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23070" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma prominently 3-lobed.</text>
      <biological_entity id="o23071" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o23072" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s14" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsular, ellipsoid to subglobose, 3-angled, leathery, dehiscence loculicidal.</text>
      <biological_entity id="o23073" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s15" to="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds many, in 2 rows per locule, flat.</text>
      <biological_entity id="o23075" name="row" name_original="rows" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23076" name="locule" name_original="locule" src="d0_s16" type="structure" />
      <relation from="o23074" id="r3125" name="in" negation="false" src="d0_s16" to="o23075" />
      <relation from="o23075" id="r3126" name="per" negation="false" src="d0_s16" to="o23076" />
    </statement>
    <statement id="d0_s17">
      <text>x = 12.</text>
      <biological_entity id="o23074" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="many" value_original="many" />
        <character is_modifier="false" name="prominence_or_shape" notes="" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="x" id="o23077" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; North America, temperate Eurasia (especially c, w Asia), n Africa, cultivated worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="introduced" />
        <character name="distribution" value="temperate Eurasia (especially c)" establishment_means="introduced" />
        <character name="distribution" value="temperate Eurasia (w Asia)" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="cultivated worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Tulip</other_name>
  <other_name type="common_name">tulipe</other_name>
  <other_name type="common_name">tulipán</other_name>
  <discussion>Species ca. 150 (1 in the flora).</discussion>
  <discussion>The common garden tulip (Tulipa gesneriana Linnaeus) and a number of other species (T. bakeri Hall, T. clusiana de Candolle, T. fosteriana Hoog ex W. Irving, T. kaufmanniana Regel, T. tarda Stapf), as well as a vast array of complex hybrid cultivars, are commonly planted for their spring flowers. Over 3500 names applied to tulips are currently listed (J. van Scheepen 1996). While some of these species or cultivars may persist for a short time, they rarely become truly naturalized in the flora. Taxonomic difficulties abound in Tulipa due to their long-established cultivation, hybridization, and selection.</discussion>
  <discussion>Viral infection of tulips results in odd, yet often attractive, colored streaks in the flowers. In the early 1600s these variants, called “broken” tulips, became prized in the Netherlands, widely sought, and worth considerable money. The ensuing “tulipomania” lead to widespread trading, speculation, and then, as with most similar fads, a sudden market collapse in 1637 (W. Blunt 1950; M. Dash 1999; F. A. Stafleu 1963).</discussion>
  <references>
    <reference>Botschantzeva, Z. P. 1982. Tulips: Taxonomy, Morphology, Cytology, Phytogeography and Physiology, transl. and ed. H. Q. Varekamp. Rotterdam.  </reference>
    <reference>Hall, A. D. 1940. The Genus Tulipa. London.  </reference>
    <reference>Pavord, A. 1999. The Tulip. New York, London, and Bloomsbury.</reference>
  </references>
  
</bio:treatment>