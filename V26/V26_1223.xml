<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">598</other_info_on_meta>
    <other_info_on_meta type="mention_page">600</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="treatment_page">599</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="tribe">ARETHUSEAE</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Bletiinae</taxon_name>
    <taxon_name authority="R. Brown" date="1813" rank="genus">calopogon</taxon_name>
    <taxon_name authority="(Walter) Ames" date="1908" rank="species">barbatus</taxon_name>
    <place_of_publication>
      <publication_title>Orchidaceae</publication_title>
      <place_in_publication>2: 272. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe arethuseae;subtribe bletiinae;genus calopogon;species barbatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101507</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophrys</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">barbata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>221. 1788, name conserved</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ophrys;species barbata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calo</taxon_name>
    <taxon_name authority="parviflorus Lindley" date="unknown" rank="species">pogon</taxon_name>
    <taxon_hierarchy>genus Calo;species pogon;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calo</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">parviflorus</taxon_name>
    <taxon_hierarchy>genus Calo;species parviflorus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calo</taxon_name>
    <taxon_name authority="(Salisbury) R. Brown" date="unknown" rank="species">pulchellus</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="variety">graminifolius</taxon_name>
    <taxon_hierarchy>genus Calo;species pulchellus;variety graminifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helleborine</taxon_name>
    <taxon_name authority="folia (Elliott) Kuntze" date="unknown" rank="species">gramini</taxon_name>
    <taxon_hierarchy>genus Helleborine;species gramini;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="(Elliott) Small" date="unknown" rank="species">graminifolium</taxon_name>
    <taxon_hierarchy>genus Limodorum;species graminifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limodorum</taxon_name>
    <taxon_name authority="(Lindley) Nash" date="unknown" rank="species">parviflorum</taxon_name>
    <taxon_hierarchy>genus Limodorum;species parviflorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 11–34 cm at flowering, 13–57 cm at senescence.</text>
      <biological_entity id="o22910" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" constraint="at flowering , 13-57 cm; at senescence" from="11" from_unit="cm" name="some_measurement" src="d0_s0" to="34" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose to elongate, rarely forked, 13–26 mm.</text>
      <biological_entity id="o22911" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="elongate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="forked" value_original="forked" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s1" to="26" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves closely appressed to inflorescence at flowering;</text>
      <biological_entity id="o22912" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to inflorescence" constraintid="o22913" is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o22913" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear, 7–22 cm × 2–5.5 mm, later elongating to 12–54 cm.</text>
      <biological_entity id="o22914" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s3" to="22" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5.5" to_unit="mm" />
        <character constraint="to 12-54 cm" is_modifier="false" modifier="later" name="length" src="d0_s3" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences green or slightly purple at base, rarely entirely purple, becoming entirely green after flowering, 11–34 cm, later elongating to 13–57 cm;</text>
      <biological_entity id="o22915" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character constraint="at base" constraintid="o22916" is_modifier="false" modifier="slightly" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely entirely" name="coloration_or_density" notes="" src="d0_s4" value="purple" value_original="purple" />
        <character constraint="after flowering , 11-34 cm" is_modifier="false" modifier="becoming entirely" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="later" name="length" src="d0_s4" value="elongating" value_original="elongating" />
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s4" to="57" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22916" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>floral bracts 2–4.5 mm.</text>
      <biological_entity constraint="floral" id="o22917" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1–12, less than 1 cm apart, opening nearly simultaneously, magenta, pink, or rarely white, without fragrance;</text>
      <biological_entity id="o22918" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="12" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="apart" value_original="apart" />
        <character is_modifier="false" modifier="nearly simultaneously; simultaneously" name="coloration" src="d0_s6" value="magenta" value_original="magenta" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>dorsal sepal obovate to oblanceolate, 12–20 × 4–9 mm, apex acuminate;</text>
      <biological_entity constraint="dorsal" id="o22919" name="sepal" name_original="sepal" src="d0_s7" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s7" to="oblanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22920" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral sepals distally reflexed to straight, ovate to moderately lanceolate, weakly falcate to straight, 11–16 × 5–9 mm, apex apiculate to acuminate;</text>
      <biological_entity constraint="lateral" id="o22921" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="moderately lanceolate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s8" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22922" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals obpandurate, 11–16 × 4–6 mm, apex obtuse;</text>
      <biological_entity id="o22923" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22924" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lip 7–13.5 mm, dilated apex of middle lobe triangular to quadrangular, broadly rounded, 7–11 mm wide, apex retuse or apiculate;</text>
      <biological_entity id="o22925" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="13.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22926" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="dilated" value_original="dilated" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s10" to="quadrangular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="middle" id="o22927" name="lobe" name_original="lobe" src="d0_s10" type="structure" />
      <biological_entity id="o22928" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <relation from="o22926" id="r3112" name="part_of" negation="false" src="d0_s10" to="o22927" />
    </statement>
    <statement id="d0_s11">
      <text>column 6–9 × 1.5–2 mm, distal end 5–8 mm wide;</text>
      <biological_entity id="o22929" name="column" name_original="column" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o22930" name="end" name_original="end" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rostellum present.</text>
      <biological_entity id="o22931" name="rostellum" name_original="rostellum" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid to ellipsoid, 12–17 × 4.5–6 mm. 2n = 40, 42.</text>
      <biological_entity id="o22932" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="ellipsoid" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s13" to="17" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22933" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring, seldom winter (s Fla), late spring in north (Jan–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="flowering time" char_type="range_value" modifier="s Fla" to="winter" from="winter" />
        <character name="flowering time" char_type="range_value" modifier="in north" to="late spring" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Jun" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, acidic, sandy pine savannas and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="acidic" />
        <character name="habitat" value="sandy pine" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Bearded grass-pink</other_name>
  <discussion>Calopogon barbatus can be distinguished from other Calopogon species by its narrow leaf closely appressed to the inflorescence at flowering, relatively closely spaced flowers that open nearly simultaneously, general lack of floral fragrance, and the significant elongation of leaves and aboveground stems over the growing season. Calopogon barbatus also flowers earlier than other species within its range except C. oklahomensis. Specimens of plants from Texas and most from western Louisiana that have been identified as C. barbatus are actually C. oklahomensis (D. H. Goldman 1995). Calopogon barbatus, however, is found in western Louisiana, although local and most infrequent (W. C. Holmes and P. S. Mathies 1980).</discussion>
  <discussion>The type specimen for the basionym of Calopogon barbatus, Ophrys barbata Walter, is actually C. multiflorus (D. H. Goldman 1998). The name Ophrys barbata, however, has been proposed for conservation (R. K. Brummitt 2000), in which case no nomenclatural change will be necessary.</discussion>
  
</bio:treatment>