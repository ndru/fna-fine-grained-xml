<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Peter Goldblatt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="mention_page">402</other_info_on_meta>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="genus">WATSONIA</taxon_name>
    <place_of_publication>
      <publication_title>Fig. Pl. Gard. Dict.,</publication_title>
      <place_in_publication>184, plate 276. 1758</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus WATSONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For William Watson, 1715–1787, British botanist</other_info_on_name>
    <other_info_on_name type="fna_id">134786</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, from corms.</text>
      <biological_entity id="o28979" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o28980" name="corm" name_original="corms" src="d0_s0" type="structure" />
      <relation from="o28979" id="r3912" name="from" negation="false" src="d0_s0" to="o28980" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branched.</text>
      <biological_entity id="o28981" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves several;</text>
      <biological_entity id="o28982" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade plane, lanceolate to linear, usually coarse, fibrotic.</text>
      <biological_entity id="o28983" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear" />
        <character is_modifier="false" modifier="usually" name="relief" src="d0_s3" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="fibrotic" value_original="fibrotic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences spicate, erect, many-flowered;</text>
      <biological_entity id="o28984" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="spicate" value_original="spicate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts green, often flushed with red, unequal, usually outer exceeding inner, apex acute, inner forked apically, firm to leathery.</text>
      <biological_entity id="o28985" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="flushed with red" value_original="flushed with red" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s5" value="outer" value_original="outer" />
        <character is_modifier="false" name="position_relational" src="d0_s5" value="exceeding" value_original="exceeding" />
        <character is_modifier="false" name="position" src="d0_s5" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o28986" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o28987" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s5" value="forked" value_original="forked" />
        <character char_type="range_value" from="firm" name="texture" src="d0_s5" to="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers short-lived, odorless [rarely fragrant], zygomorphic [actinomorphic], distichous;</text>
      <biological_entity id="o28988" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="odor" src="d0_s6" value="odorless" value_original="odorless" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="zygomorphic" value_original="zygomorphic" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals horizontal or suberect, connate into tube, orange, red, or purple [pink, rarely white], ± equal [equal];</text>
      <biological_entity id="o28989" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="suberect" value_original="suberect" />
        <character constraint="into tube" constraintid="o28990" is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o28990" name="tube" name_original="tube" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>perianth-tube funnel-shaped or elongate, expanded distally into wide, horizontal upper part;</text>
      <biological_entity id="o28991" name="perianth-tube" name_original="perianth-tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="funnel--shaped" value_original="funnel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character constraint="into upper part" constraintid="o28992" is_modifier="false" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="upper" id="o28992" name="part" name_original="part" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wide" value_original="wide" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens unilateral [symmetrical], arcuate [declinate], extended horizontally below dorsal tepal;</text>
      <biological_entity id="o28993" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_orientation_or_position" src="d0_s9" value="unilateral" value_original="unilateral" />
        <character is_modifier="false" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character constraint="horizontally below dorsal tepal" constraintid="o28994" is_modifier="false" name="size" src="d0_s9" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o28994" name="tepal" name_original="tepal" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="horizontally below" name="position" src="d0_s9" value="dorsal" value_original="dorsal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers parallel [diverging];</text>
      <biological_entity id="o28995" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style arching below or above filaments [central], dividing opposite to [beyond] anthers into 3 filiform branches each divided for ca. 1/2 their length, apically stigmatic.</text>
      <biological_entity id="o28996" name="style" name_original="style" src="d0_s11" type="structure">
        <character constraint="below filaments" constraintid="o28997" is_modifier="false" name="orientation" src="d0_s11" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="apically" name="structure_in_adjective_form" notes="" src="d0_s11" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o28997" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <biological_entity id="o28998" name="opposite-to-anther" name_original="opposite-to-anthers" src="d0_s11" type="structure" />
      <biological_entity id="o28999" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="true" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="length" src="d0_s11" value="divided" value_original="divided" />
      </biological_entity>
      <relation from="o28996" id="r3913" name="dividing" negation="false" src="d0_s11" to="o28998" />
      <relation from="o28996" id="r3914" name="into" negation="false" src="d0_s11" to="o28999" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules [globose to] oblong, wood-textured, rounded [acute or attenuate].</text>
      <biological_entity id="o29000" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="texture" src="d0_s12" value="wood-textured" value_original="wood-textured" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds several to many, angular, 1-winged or 2-winged [prismatic];</text>
      <biological_entity id="o29001" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="several" name="quantity" src="d0_s13" to="many" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s13" value="angular" value_original="angular" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-winged" value_original="1-winged" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-winged" value_original="2-winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>seed-coat light-brown.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o29002" name="seed-coat" name_original="seed-coat" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity constraint="x" id="o29003" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; s Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="s Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion>Species 52 (1 in the flora).</discussion>
  <discussion>Several species of Watsonia are cultivated in the flora area where the winters are mild, especially in California; only W. meriana is truly naturalized. The following have been recorded as persisting for some years around abandoned dwellings, in cemeteries and garbage dumps, and along roads and highways: W. borbonica (Pourret) Goldblatt (both pink- and white-flowered forms), W. fourcadei J. W. Mathews &amp; L. Bolus, and W. marginata (Linnaeus f.) Ker Gawler.</discussion>
  <references>
    <reference>  Goldblatt, P. 1989. The genus Watsonia. A systematic monograph. Ann. Kirstenbosch Bot. Gard. 17.</reference>
  </references>
  
</bio:treatment>