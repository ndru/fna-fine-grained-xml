<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frederick H. Utech</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Torrey" date="1857" rank="genus">SCOLIOPUS</taxon_name>
    <place_of_publication>
      <publication_title>Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 145, plate 22. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus SCOLIOPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek skolios, crooked, and - pous, footed, alluding to the tortuous, recurved pedicels</other_info_on_name>
    <other_info_on_name type="fna_id">129848</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, with short, knotty rhizomes;</text>
      <biological_entity id="o9730" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9731" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
      </biological_entity>
      <relation from="o9730" id="r1384" name="with" negation="false" src="d0_s0" to="o9731" />
    </statement>
    <statement id="d0_s1">
      <text>roots contractile.</text>
      <biological_entity id="o9732" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="contractile" value_original="contractile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems subterranean, simple, vertical, short.</text>
      <biological_entity id="o9733" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="location" src="d0_s2" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 2–3 (–4), petiolate or subsessile, sheathing at base;</text>
      <biological_entity id="o9734" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character constraint="at base" constraintid="o9735" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o9735" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade dark green distally, paler proximally, sometimes mottled with purple, elliptic to oblong, apex obtuse.</text>
      <biological_entity id="o9736" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s4" value="paler" value_original="paler" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s4" value="mottled with purple" value_original="mottled with purple" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="oblong" />
      </biological_entity>
      <biological_entity id="o9737" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences umbellate, with several axillary fascicles of elongate, twisting pedicels.</text>
      <biological_entity id="o9738" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o9739" name="fascicle" name_original="fascicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o9740" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="twisting" value_original="twisting" />
      </biological_entity>
      <relation from="o9738" id="r1385" name="with" negation="false" src="d0_s5" to="o9739" />
      <relation from="o9739" id="r1386" name="part_of" negation="false" src="d0_s5" to="o9740" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers showy, odor unpleasant;</text>
      <biological_entity id="o9741" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="showy" value_original="showy" />
        <character is_modifier="false" name="odor" src="d0_s6" value="unpleasant" value_original="unpleasant" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perianth of 2 dissimilar whorls of tepals, outer spreading or recurving, distinct, petaloid, ovate to lanceolate to oblanceolate, with oblong gland basally, inner erect, distinct, linear, converging over pistil;</text>
      <biological_entity id="o9742" name="perianth" name_original="perianth" src="d0_s7" type="structure" />
      <biological_entity id="o9743" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9744" name="tepal" name_original="tepals" src="d0_s7" type="structure" />
      <biological_entity constraint="outer" id="o9745" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurving" value_original="recurving" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9746" name="gland" name_original="gland" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9747" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character constraint="over pistil" constraintid="o9748" is_modifier="false" name="arrangement" src="d0_s7" value="converging" value_original="converging" />
      </biological_entity>
      <biological_entity id="o9748" name="pistil" name_original="pistil" src="d0_s7" type="structure" />
      <relation from="o9742" id="r1387" name="consist_of" negation="false" src="d0_s7" to="o9743" />
      <relation from="o9743" id="r1388" name="part_of" negation="false" src="d0_s7" to="o9744" />
      <relation from="o9745" id="r1389" name="with" negation="false" src="d0_s7" to="o9746" />
    </statement>
    <statement id="d0_s8">
      <text>stamens 3, opposite outer tepals;</text>
      <biological_entity id="o9749" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9750" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers versatile, 2-locular, oblong, extrorse;</text>
      <biological_entity id="o9751" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="dehiscence_or_orientation" src="d0_s9" value="extrorse" value_original="extrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary superior, 1-locular, strongly 3-angled, placentation parietal;</text>
      <biological_entity id="o9752" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s10" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="placentation" src="d0_s10" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style erect, short;</text>
      <biological_entity id="o9753" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas persistent, 3, spreading to recurved, linear, inner surface deeply channelled, ovules 20–40, in 2 rows on each placenta.</text>
      <biological_entity id="o9754" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="recurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9755" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o9756" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s12" to="40" />
      </biological_entity>
      <biological_entity id="o9757" name="row" name_original="rows" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9758" name="placenta" name_original="placenta" src="d0_s12" type="structure" />
      <relation from="o9756" id="r1390" name="in" negation="false" src="d0_s12" to="o9757" />
      <relation from="o9757" id="r1391" name="on" negation="false" src="d0_s12" to="o9758" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, brownish purple, oblong-lanceolate, strongly 3-angled, thin-walled, opening irregularly by decay, beaked by persistent style and stigmas.</text>
      <biological_entity id="o9759" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s13" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="thin-walled" value_original="thin-walled" />
        <character constraint="by stigmas" constraintid="o9762" is_modifier="false" name="architecture_or_shape" src="d0_s13" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o9760" name="decay" name_original="decay" src="d0_s13" type="structure" />
      <biological_entity id="o9761" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="true" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o9762" name="stigma" name_original="stigmas" src="d0_s13" type="structure" />
      <relation from="o9759" id="r1392" name="opening" negation="false" src="d0_s13" to="o9760" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds slightly curved, oblong, eliaosome present.</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 7, 8.</text>
      <biological_entity id="o9763" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o9764" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="7" value_original="7" />
        <character name="quantity" src="d0_s15" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Slink pod</other_name>
  <other_name type="common_name">fetid adder’s-tongue</other_name>
  <other_name type="common_name">slink-lily</other_name>
  <discussion>Species 2 (2 in the flora).</discussion>
  <references>
    <reference>Berg, R. Y. 1959. Seed dispersal, morphology and taxonomic position of Scoliopus, Liliaceae. Skr. Norske Vidensk.-Akad. Oslo, Mat.-Naturvidensk. Kl. 1959(4): 1–56.  </reference>
    <reference>Cave, M. S. 1966. The chromosomes of Scoliopus (Liliaceae). Madroño 18: 211–213.  </reference>
    <reference>Utech, F. H. 1979b. Floral vascular anatomy of Scoliopus bigelovii Torrey (Liliaceae-Parideae = Trilliaceae) and tribal note. Ann. Carnegie Mus. 48: 43–71.  </reference>
    <reference>Utech, F. H. 1992. Biology of Scoliopus (Liliaceae). I. Systematics and phytogeography. Ann. Missouri Bot. Gard. 79: 126–142.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 14–24 cm; pedicel 11–22 cm; outer tepals 12.5–19 mm; style 4.5–6 mm.</description>
      <determination>1 Scoliopus bigelovii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 8–14 cm; pedicel 6–10 cm; outer tepals 6.5–10 mm; style 2–2.5 mm.</description>
      <determination>2 Scoliopus hallii</determination>
    </key_statement>
  </key>
</bio:treatment>