<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="S. V. Fraser" date="1940" rank="species">perdulce</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Kansas Acad. Sci.</publication_title>
      <place_in_publication>42: 124. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species perdulce</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101389</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 2–20+, without basal bulbels, 1–2.5 × 1.2–2.8 cm;</text>
      <biological_entity id="o23862" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="20" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s0" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" notes="" src="d0_s0" to="2.8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o23863" name="bulbel" name_original="bulbels" src="d0_s0" type="structure" />
      <relation from="o23862" id="r3243" name="without" negation="false" src="d0_s0" to="o23863" />
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, dark-brown, reticulate, cells coarse-meshed, open, fibrous;</text>
      <biological_entity constraint="outer" id="o23864" name="coat" name_original="coats" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="coarse-meshed" value_original="coarse-meshed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="open" value_original="open" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o23865" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s1" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o23864" id="r3244" name="enclosing" negation="false" src="d0_s1" to="o23865" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats whitish, cells regular, vertically elongate, walls not sinuous.</text>
      <biological_entity constraint="inner" id="o23866" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o23867" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="regular" value_original="regular" />
        <character is_modifier="false" modifier="vertically" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o23868" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s2" value="sinuous" value_original="sinuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, green at anthesis, 3–5, sheathing;</text>
      <biological_entity id="o23869" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, channeled, 8–30 cm × 1–2 (–3) mm, margins entire.</text>
      <biological_entity id="o23870" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23871" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, ± terete, 10–20 cm × 1–3 mm.</text>
      <biological_entity id="o23872" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, loose, 5–25-flowered, hemispheric-globose, bulbils unknown;</text>
      <biological_entity id="o23873" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-25-flowered" value_original="5-25-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric-globose" value_original="hemispheric-globose" />
      </biological_entity>
      <biological_entity id="o23874" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 2–3, 3–7-veined, ovate, ± equal, apex acuminate.</text>
      <biological_entity constraint="spathe" id="o23875" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-7-veined" value_original="3-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o23876" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers urceolate, 7–10 mm;</text>
      <biological_entity id="o23877" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals erect, white or pale-pink with deep pink midribs to deep rose, lanceolate, ± equal, becoming callous-keeled and permanently investing capsule, margins entire, apex obtuse or acute;</text>
      <biological_entity id="o23878" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character constraint="with midribs" constraintid="o23879" is_modifier="false" name="coloration" src="d0_s9" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="rose" value_original="rose" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s9" value="callous-keeled" value_original="callous-keeled" />
      </biological_entity>
      <biological_entity id="o23879" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o23880" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
      <biological_entity id="o23881" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="permanently" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23882" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o23878" id="r3245" modifier="permanently" name="investing" negation="false" src="d0_s9" to="o23880" />
    </statement>
    <statement id="d0_s10">
      <text>stamens included;</text>
      <biological_entity id="o23883" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow or purple;</text>
      <biological_entity id="o23884" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow;</text>
      <biological_entity id="o23885" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crestless;</text>
      <biological_entity id="o23886" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="crestless" value_original="crestless" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style linear, ± equaling stamens;</text>
      <biological_entity id="o23887" name="style" name_original="style" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o23888" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate, unlobed or obscurely lobed;</text>
      <biological_entity id="o23889" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s15" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pedicel 3–17 mm, ± 2 times perianth at anthesis, elongating in fruit.</text>
      <biological_entity id="o23890" name="pedicel" name_original="pedicel" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="17" to_unit="mm" />
        <character constraint="at fruit" constraintid="o23892" is_modifier="false" name="size_or_quantity" src="d0_s16" value="2 times perianth" value_original="2 times perianth" />
      </biological_entity>
      <biological_entity id="o23891" name="perianth" name_original="perianth" src="d0_s16" type="structure" />
      <biological_entity id="o23892" name="fruit" name_original="fruit" src="d0_s16" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s16" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="length" src="d0_s16" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seed-coat dull or shining;</text>
      <biological_entity id="o23893" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s17" value="dull" value_original="dull" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>cells minutely roughened.</text>
      <biological_entity id="o23894" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief_or_texture" src="d0_s18" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals deep rose, aging to purple; n Texas and e New Mexico to se South Dakota and adjacent Iowa</description>
      <determination>14a Allium perdulce var. perdulce</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Tepals white to light pink, aging pink; trans-Pecos Texas.</description>
      <determination>14b Allium perdulce var. sperryi</determination>
    </key_statement>
  </key>
</bio:treatment>