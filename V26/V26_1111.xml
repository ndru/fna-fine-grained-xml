<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">544</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="Sheviak" date="1984" rank="species">diluvialis</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>36: 11, figs. 1C, D, 2A–C, F. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species diluvialis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101950</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="Chamisso" date="unknown" rank="species">romanzoffiana</taxon_name>
    <taxon_name authority="(Sheviak) S. L. Welsh" date="unknown" rank="variety">diluvialis</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species romanzoffiana;variety diluvialis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–62 cm.</text>
      <biological_entity id="o1721" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="62" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots few–several, horizontal to descending, slenderly tuberous, to 1 cm diam.</text>
      <biological_entity id="o1722" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s1" to="descending" />
        <character is_modifier="false" modifier="slenderly" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persisting through anthesis, usually restricted to base of stem, ascending, linearlanceolate, to 28 × 1.5 cm.</text>
      <biological_entity id="o1723" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="through anthesis" is_modifier="false" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="28" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1724" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o1725" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o1723" id="r229" modifier="usually" name="restricted to" negation="false" src="d0_s2" to="o1724" />
      <relation from="o1723" id="r230" modifier="usually" name="part_of" negation="false" src="d0_s2" to="o1725" />
    </statement>
    <statement id="d0_s3">
      <text>Spikes usually tightly spiraled, 3 flowers per cycle of spiral, rarely loosely spiraled with more than 4 flowers per cycle;</text>
      <biological_entity id="o1726" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually tightly" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1727" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character constraint="with flowers" constraintid="o1729" is_modifier="false" modifier="of spiral; rarely loosely" name="arrangement" notes="" src="d0_s3" value="spiraled" value_original="spiraled" />
      </biological_entity>
      <biological_entity id="o1728" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
      <biological_entity id="o1729" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1730" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
      <relation from="o1727" id="r231" name="per" negation="false" src="d0_s3" to="o1728" />
      <relation from="o1729" id="r232" name="per" negation="false" src="d0_s3" to="o1730" />
    </statement>
    <statement id="d0_s4">
      <text>rachis sparsely (rarely densely) pubescent, some trichomes capitate, glands obviously stalked (longest trichomes 0.2–0.4 mm).</text>
      <biological_entity id="o1731" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o1732" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o1733" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obviously" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers white or ivory, ascending, strongly gaping from near base (lip prominently diverging from sepals and petals);</text>
      <biological_entity id="o1734" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="ivory" value_original="ivory" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character constraint="from base" constraintid="o1735" is_modifier="false" modifier="strongly" name="architecture" src="d0_s5" value="gaping" value_original="gaping" />
      </biological_entity>
      <biological_entity id="o1735" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sepals distinct or connate at base, 7.5–15 mm;</text>
      <biological_entity id="o1736" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character constraint="at base" constraintid="o1737" is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1737" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>lateral sepals broadly spreading to loosely incurved, often raised above rest of flower or often appressed;</text>
      <biological_entity constraint="lateral" id="o1738" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly spreading" name="orientation" src="d0_s7" to="loosely incurved" />
        <character constraint="above rest" constraintid="o1739" is_modifier="false" modifier="often" name="prominence" src="d0_s7" value="raised" value_original="raised" />
        <character is_modifier="false" modifier="often" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o1739" name="rest" name_original="rest" src="d0_s7" type="structure" />
      <biological_entity id="o1740" name="flower" name_original="flower" src="d0_s7" type="structure" />
      <relation from="o1739" id="r233" name="part_of" negation="false" src="d0_s7" to="o1740" />
    </statement>
    <statement id="d0_s8">
      <text>petals linear, apex acuminate;</text>
      <biological_entity id="o1741" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o1742" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip often yellow centrally, ovate, lanceolate, or oblong, with variably evident median constriction, occasionally subpandurate, 7–12 × 2.5–6.8 mm, margins crisped, entire, or apically dentate, distal surface mostly glabrous adaxially;</text>
      <biological_entity id="o1743" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often; centrally" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="occasionally" name="shape" notes="" src="d0_s9" value="subpandurate" value_original="subpandurate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="6.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="median" id="o1744" name="constriction" name_original="constriction" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="variably" name="prominence" src="d0_s9" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o1745" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s9" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1746" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly; adaxially" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o1743" id="r234" name="with" negation="false" src="d0_s9" to="o1744" />
    </statement>
    <statement id="d0_s10">
      <text>basal calli prominent;</text>
      <biological_entity constraint="basal" id="o1747" name="callus" name_original="calli" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>veins few to several, branches parallel to widespreading;</text>
      <biological_entity id="o1748" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s11" to="several" />
      </biological_entity>
      <biological_entity id="o1749" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="widespreading" value_original="widespreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidia linear to linearlanceolate;</text>
      <biological_entity id="o1750" name="viscidium" name_original="viscidia" src="d0_s12" type="structure">
        <character constraint="to linearlanceolate" constraintid="o1751" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o1751" name="linearlanceolate" name_original="linearlanceolate" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary mostly 4–10 mm.</text>
      <biological_entity id="o1752" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds monoembryonic.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 74.</text>
      <biological_entity id="o1753" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monoembryonic" value_original="monoembryonic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1754" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="74" value_original="74" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic to wet riparian meadows, marshes, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet riparian" />
        <character name="habitat" value="mesic to wet riparian meadows" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Nebr., Nev., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Spiranthes diluvialis is an amphiploid product of hybridization of S. romanzoffiana and S. magnicamporum. It is correspondingly somewhat variable in individual characters, but specimens can be determined through consideration of a combination of features. The position of lateral sepals commonly varies within an inflorescence, and on the same flower one is often tightly appressed while the other is widespreading and ascending. Lip venation in particular can be helpful, as most branches are parallel, but often a few are wide-spreading.</discussion>
  <references>
    <reference>  Sheviak, C. J. 1984. Spiranthes diluvialis (Orchidaceae), a new species from the western United States. Brittonia 36: 8–14.</reference>
  </references>
  
</bio:treatment>