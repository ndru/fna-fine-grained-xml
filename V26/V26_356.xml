<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="F. H. Wiggers" date="unknown" rank="genus">maianthemum</taxon_name>
    <taxon_name authority="Desfontaines" date="1807" rank="species">canadense</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>9: 54. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus maianthemum;species canadense</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101757</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Maianthemum</taxon_name>
    <taxon_name authority="(Desfontaines) Pursh" date="unknown" rank="species">canadense</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">interius</taxon_name>
    <taxon_hierarchy>genus Maianthemum;species canadense;variety interius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smilacina</taxon_name>
    <taxon_name authority="(Desfontaines) Greene" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_hierarchy>genus Smilacina;species canadensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Unifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">canadense</taxon_name>
    <taxon_hierarchy>genus Unifolium;species canadense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, 10–25 cm.</text>
      <biological_entity id="o14146" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes sympodial, proliferatively branching, units 2–30 cm × 1–1.5 mm, roots restricted to nodes.</text>
      <biological_entity id="o14147" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sympodial" value_original="sympodial" />
        <character is_modifier="false" modifier="proliferatively" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o14148" name="unit" name_original="units" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14149" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o14150" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o14149" id="r1965" name="restricted to" negation="false" src="d0_s1" to="o14150" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, 1–1.8 dm × 1–2.5 mm.</text>
      <biological_entity id="o14151" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="length" src="d0_s2" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves solitary on sterile shoots, 2–3 on fertile shoots;</text>
      <biological_entity id="o14152" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="on shoots" constraintid="o14153" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o14153" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
        <character char_type="range_value" constraint="on shoots" constraintid="o14154" from="2" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o14154" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 4.5–7 (–9) × 3–4.5 (–5.5) cm, apex acute or short-caudate;</text>
      <biological_entity id="o14155" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14156" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="short-caudate" value_original="short-caudate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal leaves sessile, blade ovate, base with narrow sinus;</text>
      <biological_entity constraint="proximal" id="o14157" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o14158" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o14159" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o14160" name="sinus" name_original="sinus" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o14159" id="r1966" name="with" negation="false" src="d0_s5" to="o14160" />
    </statement>
    <statement id="d0_s6">
      <text>distal leaves petiolate, blade cordate, petiole 1–7 mm.</text>
      <biological_entity constraint="distal" id="o14161" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o14162" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o14163" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences racemose, complex, 12–25-flowered.</text>
      <biological_entity id="o14164" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="complex" value_original="complex" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="12-25-flowered" value_original="12-25-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (1–) 2 (–3) per node, 2-merous;</text>
      <biological_entity id="o14165" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s8" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="3" />
        <character constraint="per node" constraintid="o14166" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="2-merous" value_original="2-merous" />
      </biological_entity>
      <biological_entity id="o14166" name="node" name_original="node" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>tepals conspicuous, 1.5–2 × 0.8–1 mm;</text>
      <biological_entity id="o14167" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 1–1.5 mm;</text>
      <biological_entity id="o14168" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.2–0.4 mm;</text>
      <biological_entity id="o14169" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary globose, 0.8–1 mm wide;</text>
      <biological_entity id="o14170" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 0.5–0.8 mm;</text>
      <biological_entity id="o14171" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma distinctly 2-lobed;</text>
      <biological_entity id="o14172" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pedicel 3–7 × 0.2–0.5 mm.</text>
      <biological_entity id="o14173" name="pedicel" name_original="pedicel" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Berries green mottled red when young, maturing to deep translucent red, globose, 4–6 mm diam.</text>
      <biological_entity id="o14174" name="berry" name_original="berries" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="green mottled" value_original="green mottled" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s16" value="red" value_original="red" />
        <character is_modifier="false" name="life_cycle" src="d0_s16" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="translucent red" value_original="translucent red" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 1–2, globose, 3 mm. 2n = 36, 54, 72.</text>
      <biological_entity id="o14175" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="2" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14176" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="36" value_original="36" />
        <character name="quantity" src="d0_s17" value="54" value_original="54" />
        <character name="quantity" src="d0_s17" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous and coniferous forests, persisting in forest remnants and parks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous" />
        <character name="habitat" value="coniferous" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="forest remnants" />
        <character name="habitat" value="parks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Conn., Del., D.C., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Pa., R.I., S.C., S.Dak., Tenn., Vt., Va., W.Va., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">False lily-of-the-valley</other_name>
  <other_name type="common_name">two-leaved Solomon’s-seal</other_name>
  <other_name type="common_name">Canadian may-lily</other_name>
  <other_name type="common_name">maïanthème du Canada</other_name>
  <discussion>Pubescent specimens of Maianthemum canadense in the western half of the range with consistently larger leaves have been treated as var. interius.</discussion>
  
</bio:treatment>