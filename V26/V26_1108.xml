<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Richard" date="unknown" rank="genus">spiranthes</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="species">porrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Orchid. Pl.,</publication_title>
      <place_in_publication>467. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus spiranthes;species porrifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101963</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiranthes</taxon_name>
    <taxon_name authority="Chamisso" date="unknown" rank="species">romanzoffiana</taxon_name>
    <taxon_name authority="(Lindley) Ames &amp; Correll" date="unknown" rank="variety">porrifolia</taxon_name>
    <taxon_hierarchy>genus Spiranthes;species romanzoffiana;variety porrifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–60 cm.</text>
      <biological_entity id="o18511" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots few–several, spreading to descending, tuberous, mostly to 1 cm diam.</text>
      <biological_entity id="o18512" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s1" to="several" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="descending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character char_type="range_value" from="0" from_unit="cm" modifier="mostly" name="diameter" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves persisting through anthesis or fugaceous, basal, often on proximal portion of stem, ascending, linear to linearlanceolate, linear-oblanceolate, oblanceolate or elliptic, to 34 × 3.5 cm.</text>
      <biological_entity id="o18513" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="through anthesis basal leaves" constraintid="o18514" is_modifier="false" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="34" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18514" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="through" name="life_cycle" src="d0_s2" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="duration" src="d0_s2" value="fugaceous" value_original="fugaceous" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18515" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o18516" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o18513" id="r2560" modifier="often" name="on" negation="false" src="d0_s2" to="o18515" />
      <relation from="o18515" id="r2561" name="part_of" negation="false" src="d0_s2" to="o18516" />
    </statement>
    <statement id="d0_s3">
      <text>Spikes usually very tightly spiraled, rarely loosely spiraled, usually 3 flowers per cycle of spiral;</text>
      <biological_entity id="o18517" name="spike" name_original="spikes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually very tightly" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" modifier="rarely loosely" name="arrangement" src="d0_s3" value="spiraled" value_original="spiraled" />
        <character modifier="usually" name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o18518" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o18519" name="cycle" name_original="cycle" src="d0_s3" type="structure" />
      <relation from="o18518" id="r2562" modifier="of spiral" name="per" negation="false" src="d0_s3" to="o18519" />
    </statement>
    <statement id="d0_s4">
      <text>rachis glabrous to sparsely pubescent, trichomes less (usually much less) than 0.18 mm, capitate glands often sessile.</text>
      <biological_entity id="o18520" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity id="o18521" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18522" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers ascending, creamy to markedly yellowish, slenderly tubular;</text>
      <biological_entity id="o18523" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="creamy" name="coloration" src="d0_s5" to="markedly yellowish" />
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals connate at base, 4.6–11 mm, apex reflexed-spreading;</text>
      <biological_entity id="o18524" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="at base" constraintid="o18525" is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="4.6" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18525" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o18526" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed-spreading" value_original="reflexed-spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals linearlanceolate to linear-oblanceolate, apex reflexed-spreading, obtuse to subacute or subemarginate;</text>
      <biological_entity id="o18527" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-oblanceolate" value_original="linear-oblanceolate" />
      </biological_entity>
      <biological_entity id="o18528" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed-spreading" value_original="reflexed-spreading" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="subacute or subemarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lip lanceovate to lanceolate, 4–9.5 × 1.9–4.3 mm, apex recurved less than to scarcely more than apices of sepals and petals, only slightly or not at all dilated, with subapical dense cushion of peg-shaped trichomes on adaxial surface;</text>
      <biological_entity id="o18529" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="width" src="d0_s8" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18530" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o18531" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o18532" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
      <biological_entity id="o18533" name="petal" name_original="petals" src="d0_s8" type="structure" />
      <biological_entity constraint="subapical" id="o18534" name="cushion" name_original="cushion" src="d0_s8" type="structure">
        <character is_modifier="true" name="density" src="d0_s8" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o18535" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="peg--shaped" value_original="peg--shaped" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18536" name="surface" name_original="surface" src="d0_s8" type="structure" />
      <relation from="o18531" id="r2563" name="part_of" negation="false" src="d0_s8" to="o18532" />
      <relation from="o18531" id="r2564" name="part_of" negation="false" src="d0_s8" to="o18533" />
      <relation from="o18530" id="r2565" modifier="at all dilated" name="with" negation="false" src="d0_s8" to="o18534" />
      <relation from="o18534" id="r2566" name="part_of" negation="false" src="d0_s8" to="o18535" />
      <relation from="o18534" id="r2567" name="on" negation="false" src="d0_s8" to="o18536" />
    </statement>
    <statement id="d0_s9">
      <text>veins few to several, branches widespreading or often parallel;</text>
      <biological_entity id="o18537" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s9" to="several" />
      </biological_entity>
      <biological_entity id="o18538" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s9" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>viscidia linear to linear-elliptic;</text>
      <biological_entity id="o18539" name="viscidium" name_original="viscidia" src="d0_s10" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s10" to="linear-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary mostly 2–7.5 mm.</text>
      <biological_entity id="o18540" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds monoembryonic.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 44 [66].</text>
      <biological_entity id="o18541" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="monoembryonic" value_original="monoembryonic" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18542" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="44" value_original="44" />
        <character name="atypical_quantity" src="d0_s13" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet riparian meadows, stream banks, marshes, fens, seeping banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet riparian meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="seeping banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  
</bio:treatment>