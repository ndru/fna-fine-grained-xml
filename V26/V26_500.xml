<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Hooker" date="1838" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 184, plate 197. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species douglasii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101354</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Allium</taxon_name>
    <taxon_name authority="B. L. Robinson &amp; Seaton" date="unknown" rank="species">hendersonii</taxon_name>
    <taxon_hierarchy>genus Allium;species hendersonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs 1–4, not clustered on stout, primary rhizomes, ovoid, 1.2–3 × 1–2 cm;</text>
      <biological_entity id="o18469" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="4" />
        <character constraint="on primary rhizomes" constraintid="o18470" is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="shape" notes="" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s0" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o18470" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>outer coats enclosing 1 or more bulbs, light-brown, membranous, lacking cellular reticulation, or cells arranged in only 2–3 rows distal to roots, ± quadrate, without fibers;</text>
      <biological_entity constraint="outer" id="o18471" name="coat" name_original="coats" src="d0_s1" type="structure" />
      <biological_entity id="o18472" name="reticulation" name_original="reticulation" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
        <character is_modifier="true" name="texture" src="d0_s1" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="quantity" src="d0_s1" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="cellular" value_original="cellular" />
      </biological_entity>
      <biological_entity id="o18473" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character constraint="in rows" constraintid="o18474" is_modifier="false" name="arrangement" src="d0_s1" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s1" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o18474" name="row" name_original="rows" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" modifier="only" name="quantity" src="d0_s1" to="3" />
        <character constraint="to roots" constraintid="o18475" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o18475" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity id="o18476" name="fiber" name_original="fibers" src="d0_s1" type="structure" />
      <relation from="o18471" id="r2556" name="enclosing" negation="false" src="d0_s1" to="o18472" />
      <relation from="o18473" id="r2557" name="without" negation="false" src="d0_s1" to="o18476" />
    </statement>
    <statement id="d0_s2">
      <text>inner coats white, sometimes pink, cells obscure, quadrate or linear.</text>
      <biological_entity constraint="inner" id="o18477" name="coat" name_original="coats" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s2" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o18478" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" src="d0_s2" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually persistent, green at anthesis, 2, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o18479" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o18480" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o18481" name="soil" name_original="soil" src="d0_s3" type="structure" />
      <biological_entity id="o18482" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <relation from="o18480" id="r2558" name="extending" negation="false" src="d0_s3" to="o18481" />
      <relation from="o18480" id="r2559" name="extending" negation="false" src="d0_s3" to="o18482" />
    </statement>
    <statement id="d0_s4">
      <text>blade solid, flat, falcate, 9–28 cm × (2–) 5–15 mm, margins entire.</text>
      <biological_entity id="o18483" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="28" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18484" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Scape persistent, solitary, erect, solid, terete, not expanded proximal to inflorescence, (10–) 20–30 (–40) cm × 1–4 mm.</text>
      <biological_entity id="o18485" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="not" name="size" src="d0_s5" value="expanded" value_original="expanded" />
        <character constraint="to inflorescence" constraintid="o18486" is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o18486" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_length" notes="alterIDs:o18486" src="d0_s5" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o18486" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" notes="alterIDs:o18486" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="alterIDs:o18486" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Umbel persistent, erect, compact, 25–50-flowered, hemispheric to globose, bulbils unknown;</text>
      <biological_entity id="o18487" name="umbel" name_original="umbel" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="compact" value_original="compact" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="25-50-flowered" value_original="25-50-flowered" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s6" to="globose" />
      </biological_entity>
      <biological_entity id="o18488" name="bulbil" name_original="bulbils" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>spathe bracts persistent, 3, 4–6-veined, ovate, ± equal, apex acute.</text>
      <biological_entity constraint="spathe" id="o18489" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="4-6-veined" value_original="4-6-veined" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o18490" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers ± stellate, (6–) 7–8 (–10) mm;</text>
      <biological_entity id="o18491" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>tepals spreading, light pink to purple with prominent green midribs, narrowly lanceolate, ± equal, becoming papery in fruit, margins entire, apex acuminate;</text>
      <biological_entity id="o18492" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" constraint="with midribs" constraintid="o18493" from="light pink" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o18494" is_modifier="false" modifier="becoming" name="texture" src="d0_s9" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o18493" name="midrib" name_original="midribs" src="d0_s9" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o18494" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o18495" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18496" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens equaling tepals or exserted;</text>
      <biological_entity id="o18497" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o18498" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers blue-gray;</text>
      <biological_entity id="o18499" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="blue-gray" value_original="blue-gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollen white to light gray;</text>
      <biological_entity id="o18500" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="light gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovary crested;</text>
      <biological_entity id="o18501" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="crested" value_original="crested" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>processes 6, 2 per lobe, low, rounded, margins entire;</text>
      <biological_entity id="o18502" name="process" name_original="processes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="6" value_original="6" />
        <character constraint="per lobe" constraintid="o18503" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="position" notes="" src="d0_s14" value="low" value_original="low" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o18503" name="lobe" name_original="lobe" src="d0_s14" type="structure" />
      <biological_entity id="o18504" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style exserted, linear;</text>
      <biological_entity id="o18505" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma capitate, unlobed;</text>
      <biological_entity id="o18506" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pedicel 15–30 mm.</text>
      <biological_entity id="o18507" name="pedicel" name_original="pedicel" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s17" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seed-coat shining;</text>
      <biological_entity id="o18508" name="seed-coat" name_original="seed-coat" src="d0_s18" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s18" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o18509" name="cell" name_original="cells" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18510" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Winter-wet, shallow soils on rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow soils" modifier="winter-wet" constraint="on rock outcrops" />
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>75.</number>
  
</bio:treatment>