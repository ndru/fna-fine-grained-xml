<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="Harbison" date="1901" rank="species">ludovicianum</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 23. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species ludovicianum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101998</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes ± horizontal, brownish, short, thick, praemorse, not brittle.</text>
      <biological_entity id="o14909" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s0" value="praemorse" value_original="praemorse" />
        <character is_modifier="false" modifier="not" name="fragility" src="d0_s0" value="brittle" value_original="brittle" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–3, round in cross-section, 1.4–2.6 dm, ± slender, glabrous.</text>
      <biological_entity id="o14910" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character constraint="in cross-section" constraintid="o14911" is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character char_type="range_value" from="1.4" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="2.6" to_unit="dm" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14911" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts held well above ground, sessile;</text>
      <biological_entity id="o14912" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o14913" name="ground" name_original="ground" src="d0_s2" type="structure" />
      <relation from="o14912" id="r2036" name="held" negation="false" src="d0_s2" to="o14913" />
    </statement>
    <statement id="d0_s3">
      <text>blade strongly mottled in dark and bronzy green, often with central light strip, mottling becoming somewhat obscure with age, lanceolate-ovate, 5.3–9.5 × 2.3–5 cm, not glossy, margins of distal 1/3 convex-curved to apex, apex rounded-acute.</text>
      <biological_entity id="o14914" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="mottled in dark and bronzy green" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o14916" is_modifier="false" modifier="becoming somewhat" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="5.3" from_unit="cm" name="length" src="d0_s3" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s3" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity constraint="central" id="o14915" name="strip" name_original="strip" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="light" value_original="light" />
      </biological_entity>
      <biological_entity id="o14916" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o14917" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o14918" name="convex-curved" name_original="convex-curved" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="true" name="quantity" src="d0_s3" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o14919" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o14920" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded-acute" value_original="rounded-acute" />
      </biological_entity>
      <relation from="o14914" id="r2037" modifier="often" name="with" negation="false" src="d0_s3" to="o14915" />
      <relation from="o14917" id="r2038" name="consist_of" negation="false" src="d0_s3" to="o14918" />
      <relation from="o14918" id="r2039" name="to" negation="false" src="d0_s3" to="o14919" />
    </statement>
    <statement id="d0_s4">
      <text>Flower erect, odor of carrion;</text>
      <biological_entity id="o14921" name="flower" name_original="flower" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals displayed above bracts, spreading, green, lanceolate-oblanceolate, 19–35 × 2.7–4 mm, margins entire, apex rounded or acute to sometimes weakly reflexed;</text>
      <biological_entity id="o14922" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes weakly" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o14923" name="bract" name_original="bracts" src="d0_s5" type="structure" />
      <biological_entity id="o14924" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="true" name="shape" src="d0_s5" value="lanceolate-oblanceolate" value_original="lanceolate-oblanceolate" />
      </biological_entity>
      <biological_entity id="o14925" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="true" name="shape" src="d0_s5" value="lanceolate-oblanceolate" value_original="lanceolate-oblanceolate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o14922" id="r2040" name="displayed above" negation="false" src="d0_s5" to="o14923" />
      <relation from="o14922" id="r2041" name="displayed above" negation="false" src="d0_s5" to="o14924" />
      <relation from="o14922" id="r2042" name="displayed above" negation="false" src="d0_s5" to="o14925" />
    </statement>
    <statement id="d0_s6">
      <text>petals long-lasting, faintly introrsely curved-erect-spreading, weakly connivent, ± concealing stamens and ovary, dark maroon-brown, purplish, or dull greenish, or 2-colored, basal portion purple, distal portion grayish green, not spirally twisted, oblanceolate-linear, 3.5–5.5 × 0.4–0.8 cm, thick-textured, thickened and weakly clawed basally, margins entire, apex acute;</text>
      <biological_entity id="o14926" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="long-lasting" value_original="long-lasting" />
        <character is_modifier="false" modifier="faintly introrsely" name="orientation" src="d0_s6" value="curved-erect-spreading" value_original="curved-erect-spreading" />
        <character is_modifier="false" modifier="weakly" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o14927" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark maroon-brown" value_original="dark maroon-brown" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
      </biological_entity>
      <biological_entity id="o14928" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="position" src="d0_s6" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark maroon-brown" value_original="dark maroon-brown" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="2-colored" value_original="2-colored" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14929" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14930" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish green" value_original="grayish green" />
        <character is_modifier="false" modifier="not spirally" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="oblanceolate-linear" value_original="oblanceolate-linear" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thick-textured" value_original="thick-textured" />
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="weakly; basally" name="shape" src="d0_s6" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o14931" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14932" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens erect, straight, 10–18 mm;</text>
      <biological_entity id="o14933" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments olive-orange, 2–3 mm, widened basally;</text>
      <biological_entity id="o14934" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="olive-orange" value_original="olive-orange" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="width" src="d0_s8" value="widened" value_original="widened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers erect, straight, olive to orange, 7–20 mm, slender, dehiscence latrorse;</text>
      <biological_entity id="o14935" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="olive" name="coloration" src="d0_s9" to="orange" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="latrorse" value_original="latrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>connectives olive to orange, straight, scarcely extended beyond anther sac;</text>
      <biological_entity id="o14936" name="connectif" name_original="connectives" src="d0_s10" type="structure">
        <character char_type="range_value" from="olive" name="coloration" src="d0_s10" to="orange" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="beyond anther sac" constraintid="o14937" is_modifier="false" modifier="scarcely" name="size" src="d0_s10" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity constraint="anther" id="o14937" name="sac" name_original="sac" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary purple, ovoid, 6-angled, 8–9 mm;</text>
      <biological_entity id="o14938" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="6-angled" value_original="6-angled" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas erect, with spreading or coiled tips, distinct, pale-purple, subulate, 3–6 mm, ± fleshy.</text>
      <biological_entity id="o14939" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-purple" value_original="pale-purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o14940" name="tip" name_original="tips" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="coiled" value_original="coiled" />
      </biological_entity>
      <relation from="o14939" id="r2043" name="with" negation="false" src="d0_s12" to="o14940" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits dark purplish green, little or no odor, ovoid, 6-angled, pulpy.</text>
      <biological_entity id="o14941" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark purplish" value_original="dark purplish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="green" value_original="green" />
        <character is_modifier="false" name="odor" src="d0_s13" value="no" value_original="no" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="6-angled" value_original="6-angled" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="pulpy" value_original="pulpy" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–early spring (early Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early spring" from="late winter" />
        <character name="flowering time" char_type="range_value" to="Apr" from="early Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Low flatwoods, floodplains along streams, steep ravine slopes leading to floodplains, mixed pine-beech woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="low flatwoods" />
        <character name="habitat" value="floodplains" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="steep ravine slopes" />
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="mixed pine-beech woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="common_name">Louisiana trillium</other_name>
  <discussion>The range of Trillium ludovicianum is near to that of T. cuneatum in Mississippi, and the two appear to intergrade.</discussion>
  
</bio:treatment>