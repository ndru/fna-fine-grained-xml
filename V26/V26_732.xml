<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">352</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">angustifolium</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Sisyrinchium no. 2. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species angustifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">W1</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101890</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">graminoides</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species graminoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, dark olive green to bronze or blackish when dry, to 4.5 dm, not glaucous.</text>
      <biological_entity id="o1297" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark olive" value_original="dark olive" />
        <character char_type="range_value" from="green" modifier="when dry" name="coloration" src="d0_s0" to="bronze or blackish" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="4.5" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched, with 1–2 nodes, 2.3–5 mm wide, glabrous, margins often minutely denticulate especially basally, similar in color and texture to stem body;</text>
      <biological_entity id="o1298" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" notes="" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1299" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s1" to="2" />
      </biological_entity>
      <biological_entity constraint="stem" id="o1301" name="body" name_original="body" src="d0_s1" type="structure" />
      <relation from="o1298" id="r170" name="with" negation="false" src="d0_s1" to="o1299" />
      <relation from="o1300" id="r171" name="to" negation="false" src="d0_s1" to="o1301" />
    </statement>
    <statement id="d0_s2">
      <text>first internode 10–30 cm, usually longer than leaves;</text>
      <biological_entity id="o1300" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often minutely; especially basally; basally" name="shape" src="d0_s1" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o1302" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character constraint="than leaves" constraintid="o1303" is_modifier="false" name="length_or_size" src="d0_s2" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o1303" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>distalmost node with 1–3 branches.</text>
      <biological_entity constraint="distalmost" id="o1304" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o1305" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o1304" id="r172" name="with" negation="false" src="d0_s3" to="o1305" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o1306" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1307" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o1308" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o1308" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o1309" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes usually green, obviously wider than supporting branch, glabrous, keels denticulate to entire;</text>
      <biological_entity id="o1310" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than supporting branch , glabrous , keels" constraintid="o1311, o1312" is_modifier="false" name="width" src="d0_s6" value="obviously wider" value_original="obviously wider" />
      </biological_entity>
      <biological_entity id="o1311" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s6" to="entire" />
      </biological_entity>
      <biological_entity id="o1312" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
        <character char_type="range_value" from="denticulate" name="shape" src="d0_s6" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer 18–38 mm, 2–9.5 mm longer than inner, usually tapering evenly towards apex, margins basally connate 4–6 mm;</text>
      <biological_entity constraint="outer" id="o1313" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s7" to="38" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="9.5" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o1314" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o1315" is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1314" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o1315" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o1316" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved or straight, hyaline margins 0.1–0.3 mm wide, apex acuminate to acute, ending 0.2–0.7 mm proximal to green apex.</text>
      <biological_entity constraint="inner" id="o1317" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o1318" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o1319" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1320" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="acute" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o1321" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <relation from="o1317" id="r173" name="with" negation="false" src="d0_s8" to="o1318" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals pale blue to violet, occasionally white, bases yellow;</text>
      <biological_entity id="o1322" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1323" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale blue" name="coloration" src="d0_s9" to="violet" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1324" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer tepals 7.7–12.5 mm, apex rounded or emarginate, aristate;</text>
      <biological_entity id="o1325" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o1326" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="7.7" from_unit="mm" name="some_measurement" src="d0_s10" to="12.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1327" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o1328" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1329" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o1330" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1331" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o1332" name="foliage" name_original="foliage" src="d0_s12" type="structure" />
      <relation from="o1331" id="r174" name="to" negation="false" src="d0_s12" to="o1332" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules dark-brown or black, sometimes with purplish tinge, ± globose, 4–7 mm;</text>
      <biological_entity id="o1333" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" modifier="with purplish tinge; more or less" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel spreading or ascending.</text>
      <biological_entity id="o1334" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds globose to obconic, lacking obvious depression, 0.5–1.2 mm, rugulose.</text>
      <biological_entity id="o1335" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="obconic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 96.</text>
      <biological_entity id="o1336" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="prominence" src="d0_s15" value="obvious" value_original="obvious" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1337" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist meadows, stream banks, swamp edges, sandy meadows, moist open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="edges" modifier="swamp" />
        <character name="habitat" value="sandy meadows" />
        <character name="habitat" value="moist open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr. (Labr.), N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Bermudienne à feuilles étroites</other_name>
  <discussion>Sisyrinchium membranaceum E. P. Bicknell probably belongs here; Bicknell indicated that its relationship was “with S. graminoides” and his description falls within that of S. angustifolium, except for slightly shorter spathe bracts.</discussion>
  <discussion>In previous floras, Sisyrinchium angustifolium often has been confused with S. montanum, especially when S. graminoides was segregated. Branching seems to be the primary point of confusion. The original descriptions of S. angustifolium and S. graminoides clearly indicated branching while that of S. montanum indicates it to be single-stemmed. There is some slight similarity between S. montanum var. crebrum and S. angustifolium with respect to spathe connation and dry color, and chromosome counts indicate that both have 2n = 96, but there is some indication that breeding barriers may exist (D. B. Ward 1959).</discussion>
  
</bio:treatment>