<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="treatment_page">291</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Salisbury" date="1812" rank="genus">hymenocallis</taxon_name>
    <taxon_name authority="Traub" date="1962" rank="species">henryae</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>18: 71. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus hymenocallis;species henryae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101678</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulb nonrhizomatous, broadly ovoid, 4–7 (–9) × 4.5–6.5 (–8) cm;</text>
      <biological_entity id="o32305" name="bulb" name_original="bulb" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s0" value="nonrhizomatous" value_original="nonrhizomatous" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s0" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="9" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s0" to="7" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="width" src="d0_s0" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>basal plate 1–3 cm;</text>
      <biological_entity constraint="basal" id="o32306" name="plate" name_original="plate" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>neck 4–6 cm;</text>
      <biological_entity id="o32307" name="neck" name_original="neck" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tunic grayish brown.</text>
      <biological_entity id="o32308" name="tunic" name_original="tunic" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish brown" value_original="grayish brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, 3–9, erect, (2–) 3–6.7 dm × (0.8–) 1.5–3.2 cm, coriaceous;</text>
      <biological_entity id="o32309" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="9" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_length" src="d0_s4" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="length" src="d0_s4" to="6.7" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_width" src="d0_s4" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3.2" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade bluish green to deep green, narrowly liguliform, broadly channeled, margins hyaline, apex acute, glaucous to slightly glaucous, or not glaucous.</text>
      <biological_entity id="o32310" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="bluish green" name="coloration" src="d0_s5" to="deep green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="liguliform" value_original="liguliform" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity id="o32311" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o32312" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s5" to="slightly glaucous or not glaucous" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s5" to="slightly glaucous or not glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape (3.5–) 4.5–7.5 dm, suborbiculate, glaucous;</text>
      <biological_entity id="o32313" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="dm" name="atypical_some_measurement" src="d0_s6" to="4.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4.5" from_unit="dm" name="some_measurement" src="d0_s6" to="7.5" to_unit="dm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scape bracts 2, not enclosing flower buds, 4.5–5.5 × 1–1.5 cm;</text>
      <biological_entity constraint="scape" id="o32314" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s7" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="flower" id="o32315" name="bud" name_original="buds" src="d0_s7" type="structure" />
      <relation from="o32314" id="r4345" name="enclosing" negation="true" src="d0_s7" to="o32315" />
    </statement>
    <statement id="d0_s8">
      <text>subtending floral bracts 3.8–6.1 cm × 5–10 mm.</text>
      <biological_entity constraint="subtending floral" id="o32316" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.8" from_unit="cm" name="length" src="d0_s8" to="6.1" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 2 (–3 rarely), opening sequentially, mildly fragrant;</text>
      <biological_entity id="o32317" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" modifier="sequentially; mildly" name="odor" src="d0_s9" value="fragrant" value_original="fragrant" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth-tube green, robust, 6–10 (–12) cm;</text>
      <biological_entity id="o32318" name="perianth-tube" name_original="perianth-tube" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="robust" value_original="robust" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s10" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="distance" src="d0_s10" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals slightly ascending to long-spreading, greenish white, 8.8–16 cm × 4.5–9.5 (–11) mm;</text>
      <biological_entity id="o32319" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="slightly ascending" name="orientation" src="d0_s11" to="long-spreading" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="8.8" from_unit="cm" name="length" src="d0_s11" to="16" to_unit="cm" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s11" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corona white with faint green proximal eye, funnelform, gradually spreading in time, shortly tubulose proximally, 3–4 × 5–6 cm, margins between free portions of filaments irregularly dentate;</text>
      <biological_entity id="o32320" name="corona" name_original="corona" src="d0_s12" type="structure">
        <character constraint="with proximal eye" constraintid="o32321" is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character constraint="in time" constraintid="o32322" is_modifier="false" modifier="gradually" name="orientation" notes="" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="shortly; proximally" name="architecture_or_shape" notes="" src="d0_s12" value="tubulose" value_original="tubulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s12" to="4" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s12" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o32321" name="eye" name_original="eye" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="faint" value_original="faint" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o32322" name="time" name_original="time" src="d0_s12" type="structure" />
      <biological_entity constraint="between portions" constraintid="o32324" id="o32323" name="margin" name_original="margins" src="d0_s12" type="structure" constraint_original="between  portions, ">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s12" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o32324" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o32325" name="filament" name_original="filaments" src="d0_s12" type="structure" />
      <relation from="o32323" id="r4346" name="part_of" negation="false" src="d0_s12" to="o32325" />
    </statement>
    <statement id="d0_s13">
      <text>free portions of filaments inserted on flat sinal base, slightly incurved, white, 2.8–4.5 cm;</text>
      <biological_entity id="o32326" name="portion" name_original="portions" src="d0_s13" type="structure" constraint="filament" constraint_original="filament; filament">
        <character is_modifier="true" name="fusion" src="d0_s13" value="free" value_original="free" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s13" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="some_measurement" src="d0_s13" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32327" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity id="o32328" name="sinal" name_original="sinal" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o32329" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o32326" id="r4347" name="part_of" negation="false" src="d0_s13" to="o32327" />
      <relation from="o32326" id="r4348" name="inserted on" negation="false" src="d0_s13" to="o32328" />
      <relation from="o32326" id="r4349" name="inserted on" negation="false" src="d0_s13" to="o32329" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.5–2.5 cm, pollen yellow;</text>
      <biological_entity id="o32330" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s14" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32331" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary oblong to pyriform, 1.5–3 cm × 10 mm, ovules 4–8 per locule;</text>
      <biological_entity id="o32332" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="pyriform" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s15" to="3" to_unit="cm" />
        <character name="width" src="d0_s15" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o32333" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o32334" from="4" name="quantity" src="d0_s15" to="8" />
      </biological_entity>
      <biological_entity id="o32334" name="locule" name_original="locule" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style green in distal 1/2, fading to white proximally, (13–) 16–20 cm.</text>
      <biological_entity id="o32335" name="style" name_original="style" src="d0_s16" type="structure">
        <character constraint="in distal 1/2" constraintid="o32336" is_modifier="false" name="coloration" src="d0_s16" value="green" value_original="green" />
        <character char_type="range_value" from="fading" modifier="proximally" name="coloration" notes="" src="d0_s16" to="white" />
        <character char_type="range_value" from="13" from_unit="cm" name="atypical_some_measurement" src="d0_s16" to="16" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s16" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32336" name="1/2" name_original="1/2" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules subglobose, shortly beaked, 3.5–4.5 × 3–4 cm.</text>
      <biological_entity id="o32337" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s17" value="beaked" value_original="beaked" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s17" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s17" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds obovoid, 1.5–2.2 × 1.2–1.5 cm. 2n = 38.</text>
      <biological_entity id="o32338" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s18" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s18" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32339" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Green spider-lily</other_name>
  <other_name type="common_name">Henry’s spider-lily</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Hymenocallis henryae is being considered for federal protection. It has received the highest endangerment ranking by the Florida Natural Areas Inventory (Gary Knight, FNAI Director, pers. comm.) and is known only from scattered localities in Liberty, Gulf, Bay, and Walton counties. It is readily distinguished in the field by consistently having two flowers per plant and long, pale green tepals radiating from the base of a white, funnelform staminal corona. The erect, liguliform leaves vary in size and glaucousness over its range (G. L. Smith and J. N. Henry 1999).</discussion>
  <discussion>Hymenocallis henryae was designated “H. viridiflora” by J. K. Small (1933). This name is listed as number 10 in Small’s key, but as a result of a clerical error (C. V. Morton 1935), species number 10 was described by Small under the name H. rotatum Le Conte. Even if Small had applied the name H. viridiflora to the description, as he evidently had intended, that name still would be invalid, since it would have been a later homonym of H. viridiflora (Ruiz &amp; Pavón) R. W. Wallace from Peru.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades not glaucous or only slightly so; plants growing singly or loosely clumped.</description>
      <determination>14a Hymenocallis henryae var. henryae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades glaucous; plants often growing in dense clumps.</description>
      <determination>14b Hymenocallis henryae var. glaucifolia</determination>
    </key_statement>
  </key>
</bio:treatment>