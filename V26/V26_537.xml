<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">280</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">galanthus</taxon_name>
    <taxon_name authority="Hooker f." date="1875" rank="species">elwesii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>101: plate 6166. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus galanthus;species elwesii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101635</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 9–15 (–20) cm;</text>
      <biological_entity id="o29900" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bulbs 2–3 × 1.5–2 cm.</text>
      <biological_entity id="o29901" name="bulb" name_original="bulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: vernation convolute, remaining so within basal sheath;</text>
      <biological_entity id="o29902" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="vernation" src="d0_s2" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity constraint="basal" id="o29903" name="sheath" name_original="sheath" src="d0_s2" type="structure" />
      <relation from="o29902" id="r4030" name="within" negation="false" src="d0_s2" to="o29903" />
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly oblanceolate, 10–30 × 0.5–2.5 (–3.5) cm.</text>
      <biological_entity id="o29904" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o29905" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spathe 3–5 cm.</text>
      <biological_entity id="o29906" name="spathe" name_original="spathe" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: outer tepals white, elliptic to broadly obovate or nearly orbiculate, 2–2.5 × 1–2 cm;</text>
      <biological_entity id="o29907" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o29908" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="broadly obovate or nearly orbiculate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner tepals white with green blotch at base and apex, or with green stripe from base to apex, narrowly obovate, 10–12 × (4–) 6–7 mm;</text>
      <biological_entity id="o29909" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="inner" id="o29910" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character constraint="with blotch" constraintid="o29911" is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character constraint="from base" constraintid="o29914" is_modifier="false" name="coloration" notes="" src="d0_s6" value="green stripe" value_original="green stripe" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s6" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29911" name="blotch" name_original="blotch" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o29912" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o29913" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o29914" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o29915" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o29911" id="r4031" name="at" negation="false" src="d0_s6" to="o29912" />
      <relation from="o29911" id="r4032" name="at" negation="false" src="d0_s6" to="o29913" />
      <relation from="o29914" id="r4033" name="to" negation="false" src="d0_s6" to="o29915" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 4–6 mm;</text>
      <biological_entity id="o29916" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o29917" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary 5–8 × 4–5 mm;</text>
      <biological_entity id="o29918" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o29919" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 8–10 mm;</text>
      <biological_entity id="o29920" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29921" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicel (1.5–) 3–4 cm.</text>
      <biological_entity id="o29922" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29923" name="pedicel" name_original="pedicel" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, open forests, abandoned gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="gardens" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.Y., Pa.; se Europe; sw Asia (Turkey); expected elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="se Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia (Turkey)" establishment_means="native" />
        <character name="distribution" value="expected elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Giant snowdrop</other_name>
  
</bio:treatment>