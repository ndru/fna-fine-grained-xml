<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James D. Ackerman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">Cranichidinae</taxon_name>
    <taxon_name authority="Swartz" date="1788" rank="genus">CRANICHIS</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>8, 120. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe cranichidinae;genus cranichis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kranos, helmet, for helmetlike appearance of lip</other_info_on_name>
    <other_info_on_name type="fna_id">108247</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs terrestrial, sympodial.</text>
      <biological_entity id="o10125" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="sympodial" value_original="sympodial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots several, fasciculate, thick, villous.</text>
      <biological_entity id="o10126" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="several" value_original="several" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, rhizomatous.</text>
      <biological_entity id="o10127" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves 1–many, often forming basal rosette;</text>
      <biological_entity id="o10128" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s3" to="many" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10129" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o10128" id="r1448" modifier="often" name="forming" negation="false" src="d0_s3" to="o10129" />
    </statement>
    <statement id="d0_s4">
      <text>blade petiolate, not articulate, thin.</text>
      <biological_entity id="o10130" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, several to many-flowered racemes, erect;</text>
      <biological_entity id="o10131" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o10132" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scape with a few sheathing bracts.</text>
      <biological_entity id="o10133" name="scape" name_original="scape" src="d0_s6" type="structure" />
      <biological_entity id="o10134" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s6" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o10133" id="r1449" name="with" negation="false" src="d0_s6" to="o10134" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers not resupinate;</text>
      <biological_entity id="o10135" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals and petals spreading, distinct, thin;</text>
      <biological_entity id="o10136" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o10137" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip concave, fleshy;</text>
      <biological_entity id="o10138" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="concave" value_original="concave" />
        <character is_modifier="false" name="texture" src="d0_s9" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>column cylindric, short;</text>
      <biological_entity id="o10139" name="column" name_original="column" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anther erect, abaxial;</text>
      <biological_entity id="o10140" name="anther" name_original="anther" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s11" value="abaxial" value_original="abaxial" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pollinia 4, clavate, mealy, breaking apart in chunks, not powdery, attached to caudicle;</text>
      <biological_entity id="o10141" name="pollinium" name_original="pollinia" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="mealy" value_original="mealy" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s12" value="powdery" value_original="powdery" />
        <character constraint="to caudicle" constraintid="o10143" is_modifier="false" name="fixation" src="d0_s12" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o10142" name="chunk" name_original="chunks" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o10143" name="caudicle" name_original="caudicle" src="d0_s12" type="structure" />
      <relation from="o10141" id="r1450" name="breaking" negation="false" src="d0_s12" to="o10142" />
    </statement>
    <statement id="d0_s13">
      <text>stigma entire;</text>
      <biological_entity id="o10144" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rostellum apical, viscidium terminal;</text>
      <biological_entity id="o10145" name="rostellum" name_original="rostellum" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o10146" name="viscidium" name_original="viscidium" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary pedicellate.</text>
      <biological_entity id="o10147" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, erect.</text>
      <biological_entity constraint="fruits" id="o10148" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Subtropical and tropical regions, North America, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Subtropical and tropical regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Helmet orchid</other_name>
  <discussion>Species 60 (1 in the flora).</discussion>
  
</bio:treatment>