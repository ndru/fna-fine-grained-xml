<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul Martin Brown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">499</other_info_on_meta>
    <other_info_on_meta type="treatment_page">523</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="Endlicher" date="1842" rank="tribe">Cranichideae</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="subtribe">SPIRANTHINAE</taxon_name>
    <taxon_name authority="Schlechter" date="1920" rank="genus">DEIREGYNE</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>37: 426. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe cranichideae;subtribe spiranthinae;genus deiregyne;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek deire, neck, and gyne, pistil or woman, referring to sepals that sit on top of ovary and form a necklike extension</other_info_on_name>
    <other_info_on_name type="fna_id">109486</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, terrestrial.</text>
      <biological_entity id="o22282" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots tuberous, fascicled, fleshy.</text>
      <biological_entity id="o22283" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems with foliaceous sheaths.</text>
      <biological_entity id="o22284" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o22285" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o22284" id="r3028" name="with" negation="false" src="d0_s2" to="o22285" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves usually absent at anthesis, basal or cauline, base cuneate.</text>
      <biological_entity id="o22286" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o22287" is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22287" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="anthesis" value_original="anthesis" />
        <character is_modifier="true" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="true" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminally flowered spikes.</text>
      <biological_entity id="o22288" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o22289" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="terminally" name="architecture" src="d0_s4" value="flowered" value_original="flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers resupinate;</text>
      <biological_entity id="o22290" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth parts distinct and/or free;</text>
      <biological_entity constraint="perianth" id="o22291" name="part" name_original="parts" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals similar, basally connivent, forming inflated nectary;</text>
      <biological_entity id="o22292" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o22293" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o22292" id="r3029" name="forming" negation="false" src="d0_s7" to="o22293" />
    </statement>
    <statement id="d0_s8">
      <text>lip free, oblong-ovate, arcuate, base conduplicate, apex acute;</text>
      <biological_entity id="o22294" name="lip" name_original="lip" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity id="o22295" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s8" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity id="o22296" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>column partially adnate to dorsal sepal, not inflated adaxially at base;</text>
      <biological_entity id="o22297" name="column" name_original="column" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character constraint="at base" constraintid="o22299" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s9" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o22298" name="sepal" name_original="sepal" src="d0_s9" type="structure" />
      <biological_entity id="o22299" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anther ovate-cucullate, margins entire;</text>
      <biological_entity id="o22300" name="anther" name_original="anther" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-cucullate" value_original="ovate-cucullate" />
      </biological_entity>
      <biological_entity id="o22301" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>viscidium rounded;</text>
      <biological_entity id="o22302" name="viscidium" name_original="viscidium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rostellum longer than wide;</text>
      <biological_entity id="o22303" name="rostellum" name_original="rostellum" src="d0_s12" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigma 2-lobed at apex.</text>
      <biological_entity id="o22304" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character constraint="at apex" constraintid="o22305" is_modifier="false" name="shape" src="d0_s13" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o22305" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o22306" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical regions, United States, Mexico, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical regions" establishment_means="native" />
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>Species 14 (1 in the flora).</discussion>
  
</bio:treatment>