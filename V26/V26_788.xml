<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">385</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Californicae</taxon_name>
    <taxon_name authority="Eastwood" date="1897" rank="species">purdyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>3, 1: 78, plate 7, fig. 2. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series californicae;species purdyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101715</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">lansdaleana</taxon_name>
    <taxon_hierarchy>genus Iris;species lansdaleana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iris</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">macrosiphon</taxon_name>
    <taxon_name authority="(Eastwood) Jepson" date="unknown" rank="variety">purdyi</taxon_name>
    <taxon_hierarchy>genus Iris;species macrosiphon;variety purdyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes much branched, forming dense clumps, dark redbrown, very slender, 0.3–0.6 cm diam., covered with remains of old leaves;</text>
      <biological_entity id="o6754" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark redbrown" value_original="dark redbrown" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="diameter" src="d0_s0" to="0.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6755" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o6756" name="remains" name_original="remains" src="d0_s0" type="structure" />
      <biological_entity id="o6757" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <relation from="o6754" id="r969" name="forming" negation="false" src="d0_s0" to="o6755" />
      <relation from="o6754" id="r970" name="covered with" negation="false" src="d0_s0" to="o6756" />
      <relation from="o6754" id="r971" name="covered with" negation="false" src="d0_s0" to="o6757" />
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous.</text>
      <biological_entity id="o6758" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, solid, 1.5–2.5 dm.</text>
      <biological_entity id="o6759" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal few, laxly spreading, longer than stem, blade bright dark green adaxially, flushed pink basally, veins subprominent, linear, 2.8–4.8 dm × 0.5–0.8 cm, rather glaucous abaxially, margins thickened, apex acute;</text>
      <biological_entity id="o6760" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o6761" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" modifier="laxly" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character constraint="than stem" constraintid="o6762" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6762" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o6763" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="bright dark" value_original="bright dark" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s3" value="flushed pink" value_original="flushed pink" />
      </biological_entity>
      <biological_entity id="o6764" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="subprominent" value_original="subprominent" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.8" from_unit="dm" name="length" src="d0_s3" to="4.8" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" modifier="rather; abaxially" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o6765" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o6766" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline imbricated, sheathing, free only at tips, bracteiform, blade green edged with pink, strongly striate, inflated, apex acuminate.</text>
      <biological_entity id="o6767" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o6768" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricated" value_original="imbricated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character constraint="at tips" constraintid="o6769" is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="bracteiform" value_original="bracteiform" />
      </biological_entity>
      <biological_entity id="o6769" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity id="o6770" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character constraint="with apex" constraintid="o6771" is_modifier="false" name="architecture" src="d0_s4" value="edged" value_original="edged" />
      </biological_entity>
      <biological_entity id="o6771" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="pink" value_original="pink" />
        <character is_modifier="true" modifier="strongly" name="coloration_or_pubescence_or_relief" src="d0_s4" value="striate" value_original="striate" />
        <character is_modifier="true" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescence units 1–2-flowered;</text>
      <biological_entity constraint="inflorescence" id="o6772" name="unit" name_original="units" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green with prominent red margins, inflated, broadly lanceolate-ovate, 5.6–7 cm × 8–13 mm, unequal, outer shorter than inner, herbaceous, apex acuminate.</text>
      <biological_entity id="o6773" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character constraint="with margins" constraintid="o6774" is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character char_type="range_value" from="5.6" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="13" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o6774" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="outer" id="o6775" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="than inner margins" constraintid="o6776" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6776" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o6777" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: perianth pale creamy yellow flushed with pale lavender, with conspicuous brownish purple lines;</text>
      <biological_entity id="o6778" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6779" name="perianth" name_original="perianth" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale creamy" value_original="pale creamy" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow flushed with pale lavender" />
      </biological_entity>
      <biological_entity id="o6780" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="brownish purple" value_original="brownish purple" />
      </biological_entity>
      <relation from="o6779" id="r972" name="with" negation="false" src="d0_s7" to="o6780" />
    </statement>
    <statement id="d0_s8">
      <text>floral-tube linear, 3–5 cm, somewhat dilated apically;</text>
      <biological_entity id="o6781" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6782" name="floral-tube" name_original="floral-tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat; apically" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals widely spreading, veined and dotted with deeper purple on claw and limb, oblanceolate, 5.5–8.4 × 1.6–2.7 cm;</text>
      <biological_entity id="o6783" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6784" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dotted" value_original="dotted" />
        <character constraint="on limb" constraintid="o6786" is_modifier="false" name="coloration" src="d0_s9" value="deeper purple" value_original="deeper purple" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="length" src="d0_s9" to="8.4" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="width" src="d0_s9" to="2.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6785" name="claw" name_original="claw" src="d0_s9" type="structure" />
      <biological_entity id="o6786" name="limb" name_original="limb" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, lanceolate, 5–7 × 1–2 cm, margins sinuate;</text>
      <biological_entity id="o6787" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6788" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s10" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6789" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary trigonal in cross-section with groove along each angle, narrow, 1–1.5 cm;</text>
      <biological_entity id="o6790" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6791" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s11" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6792" name="cross-section" name_original="cross-section" src="d0_s11" type="structure" />
      <biological_entity id="o6793" name="groove" name_original="groove" src="d0_s11" type="structure" />
      <biological_entity id="o6794" name="angle" name_original="angle" src="d0_s11" type="structure" />
      <relation from="o6791" id="r973" name="in" negation="false" src="d0_s11" to="o6792" />
      <relation from="o6792" id="r974" name="with" negation="false" src="d0_s11" to="o6793" />
      <relation from="o6793" id="r975" name="along" negation="false" src="d0_s11" to="o6794" />
    </statement>
    <statement id="d0_s12">
      <text>style 2–3 cm, crests narrowly semiovate or nearly linear, laciniate, 1–2 cm;</text>
      <biological_entity id="o6795" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6796" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6797" name="crest" name_original="crests" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="semiovate" value_original="semiovate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="laciniate" value_original="laciniate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas rounded-truncate to 2-lobed, never triangular, margins minutely denticulate;</text>
      <biological_entity id="o6798" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6799" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="rounded-truncate" name="shape" src="d0_s13" to="2-lobed" />
        <character is_modifier="false" modifier="never" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o6800" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s13" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel 1–2 cm.</text>
      <biological_entity id="o6801" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o6802" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong-ovoid, trigonal, somewhat beaked, 2–3 cm.</text>
      <biological_entity id="o6803" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s15" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds light-brown, D-shaped, oblong-ovoid, thick, finely wrinkled.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o6804" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="d--shaped" value_original="d--shaped" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong-ovoid" value_original="oblong-ovoid" />
        <character is_modifier="false" name="width" src="d0_s16" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6805" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods of redwood region</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" constraint="of redwood region" />
        <character name="habitat" value="redwood region" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Purdy’s iris</other_name>
  <discussion>Iris purdyi hybridizes with I. bracteata, I. chrysophylla, I. douglasiana, I. innominata, I. macrosiphon, I. tenax, and I. tenuissima.</discussion>
  
</bio:treatment>