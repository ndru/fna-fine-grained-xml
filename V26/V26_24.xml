<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John G. Packer</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">PLEEA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 247. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus PLEEA</taxon_hierarchy>
    <other_info_on_name type="etymology">for Auguste Plée, 1787–1825, French traveller in the New World</other_info_on_name>
    <other_info_on_name type="fna_id">125891</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, glabrous.</text>
      <biological_entity id="o24653" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2-ranked, equitant, mostly basal;</text>
      <biological_entity id="o24654" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="2-ranked" value_original="2-ranked" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="equitant" value_original="equitant" />
      </biological_entity>
      <biological_entity id="o24655" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade linear.</text>
      <biological_entity id="o24656" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, racemose, open, bracteate, bracteolate;</text>
      <biological_entity id="o24657" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracteoles connate in epicalyx.</text>
      <biological_entity id="o24658" name="bracteole" name_original="bracteoles" src="d0_s4" type="structure">
        <character constraint="in epicalyx" constraintid="o24659" is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o24659" name="epicalyx" name_original="epicalyx" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers arising singly;</text>
      <biological_entity id="o24660" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="arising" value_original="arising" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>tepals persistent, 6, in 2 somewhat dissimiliar series, distinct;</text>
      <biological_entity id="o24661" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s6" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o24662" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" modifier="somewhat" name="variability" src="d0_s6" value="dissimiliar" value_original="dissimiliar" />
      </biological_entity>
      <relation from="o24661" id="r3353" name="in" negation="false" src="d0_s6" to="o24662" />
    </statement>
    <statement id="d0_s7">
      <text>stamens 9 (–10), 2 opposite each outer tepal, 1 opposite each inner;</text>
      <biological_entity id="o24663" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="10" />
        <character name="quantity" src="d0_s7" value="9" value_original="9" />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24664" name="tepal" name_original="tepal" src="d0_s7" type="structure">
        <character constraint="opposite inner tepal" constraintid="o24665" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="inner" id="o24665" name="tepal" name_original="tepal" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments dilated basally, flattened;</text>
      <biological_entity id="o24666" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers versatile, 2-locular, introrse, without appendages;</text>
      <biological_entity id="o24667" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s9" value="versatile" value_original="versatile" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
      <biological_entity id="o24668" name="appendage" name_original="appendages" src="d0_s9" type="structure" />
      <relation from="o24667" id="r3354" name="without" negation="false" src="d0_s9" to="o24668" />
    </statement>
    <statement id="d0_s10">
      <text>ovary superior, stipitate, apocarpous basally, glabrous;</text>
      <biological_entity id="o24669" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s10" value="apocarpous" value_original="apocarpous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>intercarpellary nectary present;</text>
      <biological_entity constraint="intercarpellary" id="o24670" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3.</text>
      <biological_entity id="o24671" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits capsular, ovoid to broadly ellipsoid, glabrous, dehiscence septicidal, then adaxially loculicidal.</text>
      <biological_entity id="o24672" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="capsular" value_original="capsular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s13" to="broadly ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="septicidal" value_original="septicidal" />
        <character is_modifier="false" modifier="adaxially" name="dehiscence" src="d0_s13" value="loculicidal" value_original="loculicidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds appendaged.</text>
    </statement>
    <statement id="d0_s15">
      <text>x =15.</text>
      <biological_entity id="o24673" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="appendaged" value_original="appendaged" />
      </biological_entity>
      <biological_entity constraint="x" id="o24674" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Rush-featherling</other_name>
  <discussion>Species 1.</discussion>
  <discussion>F. H. Utech (1978, 1979) clearly demonstrated the relationship of Pleea to Tofieldia sensu lato and reassigned the only species of the former to the latter. Morphologically, though, P. tenuifolia is a very distinctive species and, while it shares characteristics with both Tofieldia and Triantha, it is not in any way an intermediate.</discussion>
  <references>
    <reference>Utech, F. H. 1978. Floral vascular anatomy of Pleea tenuifolia Michx. (Liliaceae-Tofieldieae) and its reassignment to Tofieldia. Ann. Carnegie Mus. 47: 423–454.  </reference>
    <reference>Utech, F. H. 1979. Karyotype analysis, palynology, and external seed morphology of Tofieldia tenuifolia (Michx.) Utech (Liliaceae-Tofieldieae). Ann. Carnegie Mus. 48: 171–184.</reference>
  </references>
  
</bio:treatment>