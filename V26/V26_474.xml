<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">allium</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="species">rhizomatum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 114. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus allium;species rhizomatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101394</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Bulbs solitary, not basally clustered, replaced annually by new bulbs borne terminally on rhizome;</text>
      <biological_entity id="o11343" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="not basally" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o11344" name="bulb" name_original="bulbs" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="new" value_original="new" />
      </biological_entity>
      <biological_entity id="o11345" name="rhizome" name_original="rhizome" src="d0_s0" type="structure" />
      <relation from="o11343" id="r1596" modifier="annually" name="by" negation="false" src="d0_s0" to="o11344" />
      <relation from="o11344" id="r1597" name="borne" negation="false" src="d0_s0" to="o11345" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomes 1–3, conspicuous, slender, 2–3 cm, scaly;</text>
      <biological_entity id="o11346" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>parent bulbs persisting, often not collected, oblique-ovoid, 1–2.5 × 1 cm;</text>
      <biological_entity constraint="parent" id="o11347" name="bulb" name_original="bulbs" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persisting" value_original="persisting" />
        <character is_modifier="false" modifier="often not; not" name="shape" src="d0_s2" value="oblique-ovoid" value_original="oblique-ovoid" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="2.5" to_unit="cm" />
        <character name="width" src="d0_s2" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>outer coats enclosing parent bulbs, grayish, lacking cellular reticulation, membranous, without fibers;</text>
      <biological_entity constraint="outer" id="o11348" name="coat" name_original="coats" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity constraint="parent" id="o11349" name="bulb" name_original="bulbs" src="d0_s3" type="structure" />
      <biological_entity id="o11350" name="reticulation" name_original="reticulation" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="cellular" value_original="cellular" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o11351" name="fiber" name_original="fibers" src="d0_s3" type="structure" />
      <relation from="o11348" id="r1598" name="enclosing" negation="false" src="d0_s3" to="o11349" />
      <relation from="o11350" id="r1599" name="without" negation="false" src="d0_s3" to="o11351" />
    </statement>
    <statement id="d0_s4">
      <text>inner coats white or hyaline, cells obscure, ± quadrate.</text>
      <biological_entity constraint="inner" id="o11352" name="coat" name_original="coats" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o11353" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscure" value_original="obscure" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent, green at anthesis, 2–3, basally sheathing, sheaths not extending much above soil surface;</text>
      <biological_entity id="o11354" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character constraint="at anthesis" is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s5" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o11355" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o11356" name="soil" name_original="soil" src="d0_s5" type="structure" />
      <biological_entity id="o11357" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <relation from="o11355" id="r1600" name="extending" negation="false" src="d0_s5" to="o11356" />
      <relation from="o11355" id="r1601" name="extending" negation="false" src="d0_s5" to="o11357" />
    </statement>
    <statement id="d0_s6">
      <text>blade solid, flat, not falcate, 20–35 cm × 2–3 mm, margins entire.</text>
      <biological_entity id="o11358" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="solid" value_original="solid" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11359" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Scape solitary, erect, solid, terete, 20–30 cm × 1–3 mm.</text>
      <biological_entity id="o11360" name="scape" name_original="scape" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="terete" value_original="terete" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Umbel persistent, erect, loose, 5–15 (–22) -flowered, globose to hemispheric, bulbils unknown;</text>
      <biological_entity id="o11361" name="umbel" name_original="umbel" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-15(-22)-flowered" value_original="5-15(-22)-flowered" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s8" to="hemispheric" />
      </biological_entity>
      <biological_entity id="o11362" name="bulbil" name_original="bulbils" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>spathe bracts persistent, 2, 3-veined, ovate to lanceovate, ± equal, apex acute.</text>
      <biological_entity constraint="spathe" id="o11363" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceovate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11364" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers stellate, 6–9 mm;</text>
      <biological_entity id="o11365" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="stellate" value_original="stellate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals erect, pink with purplish or pinkish midveins, oblong to lanceolate, slightly carinate basally, ± equal, becoming papery in fruit, margins entire, apex acute to acuminate;</text>
      <biological_entity id="o11366" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character constraint="with midveins" constraintid="o11367" is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s11" to="lanceolate" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s11" value="carinate" value_original="carinate" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character constraint="in fruit" constraintid="o11368" is_modifier="false" modifier="becoming" name="texture" src="d0_s11" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o11367" name="midvein" name_original="midveins" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="pinkish" value_original="pinkish" />
      </biological_entity>
      <biological_entity id="o11368" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
      <biological_entity id="o11369" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11370" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens included;</text>
      <biological_entity id="o11371" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers yellow or pink;</text>
      <biological_entity id="o11372" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen yellow or white;</text>
      <biological_entity id="o11373" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary crestless, 3-grooved with thickened ridge on either side of groove;</text>
      <biological_entity id="o11374" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="crestless" value_original="crestless" />
        <character constraint="with ridge" constraintid="o11375" is_modifier="false" name="architecture" src="d0_s15" value="3-grooved" value_original="3-grooved" />
      </biological_entity>
      <biological_entity id="o11375" name="ridge" name_original="ridge" src="d0_s15" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o11376" name="side" name_original="side" src="d0_s15" type="structure" />
      <biological_entity id="o11377" name="groove" name_original="groove" src="d0_s15" type="structure" />
      <relation from="o11375" id="r1602" name="on" negation="false" src="d0_s15" to="o11376" />
      <relation from="o11376" id="r1603" name="part_of" negation="false" src="d0_s15" to="o11377" />
    </statement>
    <statement id="d0_s16">
      <text>style linear, shorter than stamens;</text>
      <biological_entity id="o11378" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character constraint="than stamens" constraintid="o11379" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11379" name="stamen" name_original="stamens" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>stigma capitate, scarcely thickened, unlobed;</text>
      <biological_entity id="o11380" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="scarcely" name="size_or_width" src="d0_s17" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s17" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pedicel 10–20 (–50) mm.</text>
      <biological_entity id="o11381" name="pedicel" name_original="pedicel" src="d0_s18" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s18" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seed-coat shining;</text>
      <biological_entity id="o11382" name="seed-coat" name_original="seed-coat" src="d0_s19" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s19" value="shining" value_original="shining" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>cells smooth.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 28.</text>
      <biological_entity id="o11383" name="cell" name_original="cells" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11384" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, usually grassy areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" modifier="dry usually" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <discussion>Allium rhizomatum has often been included within the Mexican species A. glandulosum. This reduction is unwarranted. The perianth of A. rhizomatum is pale, with the color mainly confined to the midribs. Additionally, the species can be distinguished by its 3-lobed, apically 3-grooved ovary and lack of sepal glands. Allium glandulosum has a red perianth, an apically rounded ovary, and sepal glands. The nectar produced from these glands does not show in herbarium specimens.</discussion>
  
</bio:treatment>