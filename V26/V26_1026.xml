<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="subfamily">Cypripedioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cypripedium</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 389. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily cypripedioideae;genus cypripedium;species californicum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101546</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect, 25–120 cm.</text>
      <biological_entity id="o4473" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5–10, along length of stem, alternate, ascending to spreading;</text>
      <biological_entity id="o4474" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="10" />
        <character is_modifier="false" name="length" notes="" src="d0_s1" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="spreading" />
      </biological_entity>
      <biological_entity id="o4475" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade elliptic-lanceolate to broadly elliptic, 5–16 × 1.5–6.5 cm.</text>
      <biological_entity id="o4476" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s2" to="broadly elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="16" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers 3–18 (–22);</text>
      <biological_entity id="o4477" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="22" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals yellow-green to pale brownish yellow;</text>
      <biological_entity id="o4478" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s4" to="pale brownish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>dorsal sepal elliptic, 14–20 × 7–13 mm;</text>
      <biological_entity constraint="dorsal" id="o4479" name="sepal" name_original="sepal" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral sepals connate almost to apex;</text>
      <biological_entity constraint="lateral" id="o4480" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="to apex" constraintid="o4481" is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o4481" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>synsepal 12–20 × 10–12 mm;</text>
      <biological_entity id="o4482" name="synsepal" name_original="synsepal" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals spreading, same color as sepals or more yellowish, linear-oblong to linearlanceolate, flat, 14–16 × 3–5 mm;</text>
      <biological_entity id="o4483" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4484" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o4483" id="r657" name="as" negation="false" src="d0_s8" to="o4484" />
    </statement>
    <statement id="d0_s9">
      <text>lip white, sometimes pinkish, obovoid, 15–20 mm;</text>
      <biological_entity id="o4485" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>orifice basal, 11–14 mm;</text>
      <biological_entity id="o4486" name="orifice" name_original="orifice" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="basal" value_original="basal" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminode suborbicular-subauriform.</text>
      <biological_entity id="o4487" name="staminode" name_original="staminode" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="suborbicular-subauriform" value_original="suborbicular-subauriform" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forest openings, especially on steep slopes, in seeps, springy marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="steep slopes" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="springy marshes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">California lady’s-slipper</other_name>
  
</bio:treatment>