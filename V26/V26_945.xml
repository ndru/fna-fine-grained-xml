<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">445</other_info_on_meta>
    <other_info_on_meta type="mention_page">455</other_info_on_meta>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">agave</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">neglecta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>289. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus agave;species neglecta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101312</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants short-stemmed, commonly suckering, without rhizomes, trunks 0.3–0.4 m;</text>
      <biological_entity id="o32383" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="short-stemmed" value_original="short-stemmed" />
        <character is_modifier="false" modifier="commonly" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o32384" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o32385" name="trunk" name_original="trunks" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="0.4" to_unit="m" />
      </biological_entity>
      <relation from="o32383" id="r4353" name="without" negation="false" src="d0_s0" to="o32384" />
    </statement>
    <statement id="d0_s1">
      <text>rosettes not cespitose, 13–15 × 15–20 dm.</text>
      <biological_entity id="o32386" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="13" from_unit="dm" name="length" src="d0_s1" to="15" to_unit="dm" />
        <character char_type="range_value" from="15" from_unit="dm" name="width" src="d0_s1" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending to arching or reflexed, 100–150 × 15–25 cm;</text>
      <biological_entity id="o32387" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="arching or reflexed" />
        <character char_type="range_value" from="100" from_unit="cm" name="length" src="d0_s2" to="150" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade light green, probably not cross-zoned, broadly lanceolate, firm, adaxially plane, abaxially slightly convex;</text>
      <biological_entity id="o32388" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="probably not" name="coloration" src="d0_s3" value="cross-zoned" value_original="cross-zoned" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="abaxially slightly" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins nearly straight, not fibrous, minutely armed near base, teeth single prickles, 1–2 mm, 2–3+ cm apart;</text>
      <biological_entity id="o32389" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
        <character constraint="near base" constraintid="o32390" is_modifier="false" modifier="minutely" name="architecture" src="d0_s4" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o32390" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o32391" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity id="o32392" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apical spine dark-brown, acicular, 2.5 cm.</text>
      <biological_entity constraint="apical" id="o32393" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character name="some_measurement" src="d0_s5" unit="cm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Scape 8–10 (–13) m.</text>
      <biological_entity id="o32394" name="scape" name_original="scape" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s6" to="13" to_unit="m" />
        <character char_type="range_value" from="8" from_unit="m" name="some_measurement" src="d0_s6" to="10" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, open, often bulbiferous;</text>
      <biological_entity id="o32395" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="bulbiferous" value_original="bulbiferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts persistent, triangular, 0.5–2 cm;</text>
      <biological_entity id="o32396" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 12–20, slightly ascending, comprising distal 1/3–1/2 of inflorescence, longer than 10 cm.</text>
      <biological_entity constraint="lateral" id="o32397" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="20" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character modifier="longer than" name="some_measurement" src="d0_s9" unit="cm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o32398" name="inflorescence" name_original="inflorescence" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <relation from="o32397" id="r4354" name="comprising" negation="false" src="d0_s9" to="o32398" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers 12–22 per cluster, erect, 5.5–6 cm;</text>
      <biological_entity id="o32399" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="per cluster , erect , 5.5-6 cm" from="12" name="quantity" src="d0_s10" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perianth greenish yellow, tube urceolate, 8–10 × 4–7 mm, limb lobes erect, equal, 20–30 mm;</text>
      <biological_entity id="o32400" name="perianth" name_original="perianth" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o32401" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="limb" id="o32402" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens long-exserted;</text>
      <biological_entity id="o32403" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted ca. mid perianth-tube or above, erect, yellow, 4–5 cm;</text>
      <biological_entity id="o32404" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="inserted" value_original="inserted" />
      </biological_entity>
      <biological_entity constraint="mid" id="o32405" name="perianth-tube" name_original="perianth-tube" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s13" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, 20–25 mm;</text>
      <biological_entity id="o32406" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary 2–3 cm, neck slightly constricted.</text>
      <biological_entity id="o32407" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s15" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o32408" name="neck" name_original="neck" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s15" value="constricted" value_original="constricted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules oblong to ovoid, 3 cm.</text>
      <biological_entity id="o32409" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s16" to="ovoid" />
        <character name="some_measurement" src="d0_s16" unit="cm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 5–6 mm.</text>
      <biological_entity id="o32410" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy places near beaches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy places" constraint="near beaches" />
        <character name="habitat" value="beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Small agave</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Agave neglecta continues to live up to its name, being known only from a series of photographs and incomplete, sterile herbarium specimens. Therefore, the above measurements are only an approximation, and all are subject to modification if and when fresh, reproductive material can be studied. Attempts to see this rare species in the field have been thwarted by development and hurricanes. H. S. Gentry (1982) suggested that it might be related to A. weberi, a freely suckering species that produces fiber of an excellent quality. Nothing is known about the uses of A. neglecta. The plant may well be a cultivar of A. sisalana or A. kewensis and represent an ancient introduction from Mexico.</discussion>
  
</bio:treatment>