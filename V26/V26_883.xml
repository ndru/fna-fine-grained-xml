<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="mention_page">431</other_info_on_meta>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Engelmann ex Trelease" date="1902" rank="species">rostrata</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>13: 68, plates 40–42, plate 84, fig. 3, plate 93, fig. 2. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species rostrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102073</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Yucca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rostrata</taxon_name>
    <taxon_name authority="Trelease" date="unknown" rank="variety">linearis</taxon_name>
    <taxon_hierarchy>genus Yucca;species rostrata;variety linearis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants solitary or forming colonies of rosettes, caulescent, arborescent, 2.5–3.6 m, not including inflorescence, 1.8–3.2 dm diam;</text>
      <biological_entity id="o22716" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="forming colonies" value_original="forming colonies" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character char_type="range_value" from="2.5" from_unit="m" name="some_measurement" src="d0_s0" to="3.6" to_unit="m" />
        <character char_type="range_value" from="1.8" from_unit="dm" modifier="not" name="diameter" src="d0_s0" to="3.2" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22717" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <biological_entity id="o22718" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
      <relation from="o22716" id="r3089" name="part_of" negation="false" src="d0_s0" to="o22717" />
      <relation from="o22716" id="r3090" name="including" negation="true" src="d0_s0" to="o22718" />
    </statement>
    <statement id="d0_s1">
      <text>rosettes each with more than 100 leaves.</text>
      <biological_entity id="o22719" name="rosette" name_original="rosettes" src="d0_s1" type="structure" />
      <biological_entity id="o22720" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="100" is_modifier="true" name="quantity" src="d0_s1" upper_restricted="false" />
      </biological_entity>
      <relation from="o22719" id="r3091" name="with" negation="false" src="d0_s1" to="o22720" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–several, erect, mostly simple, occasionally 1–3-branched.</text>
      <biological_entity id="o22721" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s2" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="1-3-branched" value_original="1-3-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blade linear, often twisted, flat to concavo-convex, widest considerably beyond middle, 25–60 × 1.2–1.7 cm, glaucous, smooth, margins minutely denticulate, lemon yellow, hyaline, apex spinose, spine-tipped.</text>
      <biological_entity id="o22722" name="leaf-blade" name_original="leaf-blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s3" to="concavo-convex" />
        <character constraint="beyond middle" constraintid="o22723" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" notes="" src="d0_s3" to="60" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o22723" name="middle" name_original="middle" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" notes="" src="d0_s3" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22724" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="lemon yellow" value_original="lemon yellow" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o22725" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spinose" value_original="spinose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences paniculate, arising just within or beyond rosettes, ovoid, 3–10 dm;</text>
      <biological_entity id="o22726" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" modifier="just" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="beyond rosettes" value_original="beyond rosettes" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s4" to="10" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o22727" name="rosette" name_original="rosettes" src="d0_s4" type="structure" />
      <relation from="o22726" id="r3092" name="beyond" negation="false" src="d0_s4" to="o22727" />
    </statement>
    <statement id="d0_s5">
      <text>branches up to 3.8 dm;</text>
      <biological_entity id="o22728" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s5" to="3.8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts erect;</text>
      <biological_entity id="o22729" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle sometimes scapelike, 0.3–1 m, less than 2.5 cm diam., glabrous or glabrescent.</text>
      <biological_entity id="o22730" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s7" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s7" to="1" to_unit="m" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pendent;</text>
      <biological_entity id="o22731" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth globose to campanulate;</text>
      <biological_entity id="o22732" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s9" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals distinct, white, narrowly ovate, 4.2–5.2 × 1.1–2 cm, apex sharply acuminate;</text>
      <biological_entity id="o22733" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4.2" from_unit="cm" name="length" src="d0_s10" to="5.2" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22734" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1.7–2 cm;</text>
      <biological_entity id="o22735" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistil 2.5–3.5 cm;</text>
      <biological_entity id="o22736" name="pistil" name_original="pistil" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s12" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style white, 6–14 mm;</text>
      <biological_entity id="o22737" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas lobed.</text>
      <biological_entity id="o22738" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits erect, capsular, dehiscent, ovoid to ellipsoid, rarely constricted, 4–7 × 1.8–2.5 cm, dehiscence septicidal.</text>
      <biological_entity id="o22739" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="ellipsoid" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s15" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4" from_unit="cm" name="dehiscence" src="d0_s15" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="dehiscence" src="d0_s15" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="septicidal" value_original="septicidal" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky mountain slopes, canyon bottoms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky mountain slopes" />
        <character name="habitat" value="canyon bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; n Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Beaked yucca</other_name>
  <discussion>In the United States, Yucca rostrata is restricted to Brewster County, Texas. It is closely related to Y. thompsoniana (K. H. Clary 1997), which is perhaps just a northern variant of this species.</discussion>
  
</bio:treatment>