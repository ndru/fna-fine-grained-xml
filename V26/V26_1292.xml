<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="tribe">Malaxideae</taxon_name>
    <taxon_name authority="Solander ex Swartz" date="1788" rank="genus">malaxis</taxon_name>
    <taxon_name authority="Salazar" date="1993" rank="species">wendtii</taxon_name>
    <place_of_publication>
      <publication_title>Orquidea (Mexico City)</publication_title>
      <place_in_publication>13: 281, fig. 1. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe malaxideae;genus malaxis;species wendtii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101768</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 16–45 cm.</text>
      <biological_entity id="o6242" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pseudobulbs 5–10 mm diam.</text>
      <biological_entity id="o6243" name="pseudobulb" name_original="pseudobulbs" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1, within proximal 1/3 of stem;</text>
      <biological_entity id="o6244" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6245" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o6246" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o6244" id="r903" name="within" negation="false" src="d0_s2" to="o6245" />
      <relation from="o6245" id="r904" name="part_of" negation="false" src="d0_s2" to="o6246" />
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate-ovate or ovatelanceolate, (3–) 4.1–8.5 (–10) × 0.2–0.45 (–0.65) cm, apex obtuse to acute.</text>
      <biological_entity id="o6247" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate-ovate" value_original="orbiculate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s3" to="4.1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" src="d0_s3" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="0.45" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.65" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.45" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6248" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemes, 6–25 cm;</text>
      <biological_entity constraint="inflorescences" id="o6249" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachis slightly angled;</text>
      <biological_entity id="o6250" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bracts triangular to lanceolate, 1–2 × 0.5 mm, apex acute;</text>
      <biological_entity constraint="floral" id="o6251" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o6252" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels not crowded, 2.5–5 mm.</text>
      <biological_entity id="o6253" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 16–133, resupinate, deep maroon or greenish maroon;</text>
      <biological_entity id="o6254" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s8" to="133" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish maroon" value_original="greenish maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals papillose, not glabrous;</text>
      <biological_entity id="o6255" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>dorsal sepal lanceolate, 1.8–2.4 × 0.8–1.2 mm, margins revolute, apex acute;</text>
      <biological_entity constraint="dorsal" id="o6256" name="sepal" name_original="sepal" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s10" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6257" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s10" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o6258" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lateral sepals lanceolate-elliptic, falcate, 1.8–2.4 × 0.8–1.2 mm, apex acute;</text>
      <biological_entity constraint="lateral" id="o6259" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s11" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6260" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals strongly recurved, linear to filiform, slightly falcate, 1.8–2.2 × 0.2–0.4 mm, apex obtuse;</text>
      <biological_entity id="o6261" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="filiform" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6262" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lip linear to linearlanceolate, 1.8–2.3 × 1.3–1.8 mm, base with auricles broad and diverging, apex narrowly acuminate;</text>
      <biological_entity id="o6263" name="lip" name_original="lip" src="d0_s13" type="structure">
        <character constraint="to apex" constraintid="o6265" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o6264" name="base" name_original="base" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" is_modifier="true" name="length" src="d0_s13" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" is_modifier="true" name="width" src="d0_s13" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s13" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o6265" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>column 0.5–0.8 × 0.5–0.8 mm;</text>
      <biological_entity id="o6266" name="column" name_original="column" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s14" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pollinia yellow.</text>
      <biological_entity id="o6267" name="pollinium" name_original="pollinia" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ascending, ellipsoid, 5 × 3 mm.</text>
      <biological_entity id="o6268" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character name="length" src="d0_s16" unit="mm" value="5" value_original="5" />
        <character name="width" src="d0_s16" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open coniferous and mixed forests on dry slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" modifier="open" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="dry slopes" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>The confusion among Malaxis porphyrea, M. ehrenbergii, and M. wendtii is discussed under 10. M. porphyrea.</discussion>
  
</bio:treatment>