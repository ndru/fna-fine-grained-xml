<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles J. Sheviak,Paul M. Catling</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Orchidoideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="tribe">Orchideae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subtribe">Orchidinae</taxon_name>
    <taxon_name authority="Hultén" date="1968" rank="genus">AMERORCHIS</taxon_name>
    <place_of_publication>
      <publication_title>Ark. Bot., n. s.</publication_title>
      <place_in_publication>7: 34. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus amerorchis;</taxon_hierarchy>
    <other_info_on_name type="etymology">America plus orchis, from the American distribution of this close relative of Eurasian Orchis</other_info_on_name>
    <other_info_on_name type="fna_id">101344</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rather succulent.</text>
      <biological_entity id="o33757" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots scattered along rhizome, slender, fleshy.</text>
      <biological_entity id="o33758" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="along rhizome" constraintid="o33759" is_modifier="false" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="size" notes="" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o33759" name="rhizome" name_original="rhizome" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Stems scapose.</text>
      <biological_entity id="o33760" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves solitary, basal, spreading, conduplicate;</text>
      <biological_entity id="o33761" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="basal" id="o33762" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles inconspicuous, sheathing or clasping stem;</text>
      <biological_entity id="o33763" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o33764" name="stem" name_original="stem" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade orbiculate to broadly elliptic, ovate, obovate, or lance or oblance-elliptic.</text>
      <biological_entity id="o33765" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="broadly elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="oblance-elliptic" value_original="oblance-elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, solitary spikes, lax;</text>
      <biological_entity id="o33766" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o33767" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bracts foliaceous.</text>
      <biological_entity constraint="floral" id="o33768" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers few to several, resupinate, showy;</text>
      <biological_entity id="o33769" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s8" to="several" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="showy" value_original="showy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lip spurred at base, deeply 3-lobed, middle lobe notched, margins entire to crenate or erose;</text>
      <biological_entity id="o33770" name="lip" name_original="lip" src="d0_s9" type="structure">
        <character constraint="at base" constraintid="o33771" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="spurred" value_original="spurred" />
        <character is_modifier="false" modifier="deeply" name="shape" notes="" src="d0_s9" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o33771" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="middle" id="o33772" name="lobe" name_original="lobe" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="notched" value_original="notched" />
      </biological_entity>
      <biological_entity id="o33773" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s9" to="crenate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollinaria 2;</text>
      <biological_entity id="o33774" name="pollinarium" name_original="pollinaria" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinia 2;</text>
      <biological_entity id="o33775" name="pollinium" name_original="pollinia" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>viscidia within single 2-lobed bursicle;</text>
      <biological_entity id="o33776" name="viscidium" name_original="viscidia" src="d0_s12" type="structure" />
      <biological_entity id="o33777" name="bursicle" name_original="bursicle" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character is_modifier="true" name="shape" src="d0_s12" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <relation from="o33776" id="r4550" name="within" negation="false" src="d0_s12" to="o33777" />
    </statement>
    <statement id="d0_s13">
      <text>stigma flat, narrowly reniform to obcordate, ends flanking bursicle.</text>
      <biological_entity id="o33778" name="stigma" name_original="stigma" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="flat" value_original="flat" />
        <character char_type="range_value" from="narrowly reniform" name="shape" src="d0_s13" to="obcordate" />
      </biological_entity>
      <biological_entity id="o33779" name="end" name_original="ends" src="d0_s13" type="structure" />
      <biological_entity id="o33780" name="bursicle" name_original="bursicle" src="d0_s13" type="structure" />
      <relation from="o33779" id="r4551" name="flanking" negation="false" src="d0_s13" to="o33780" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules.</text>
      <biological_entity constraint="fruits" id="o33781" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Round-leaved orchis</other_name>
  <other_name type="common_name">amérorchis</other_name>
  <other_name type="common_name">Amérorchis à feuille ronde</other_name>
  <discussion>Species 1.</discussion>
  
</bio:treatment>