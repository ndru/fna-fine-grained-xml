<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">623</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Dressler" date="1979" rank="tribe">Calypsoeae</taxon_name>
    <taxon_name authority="Salisbury" date="unknown" rank="genus">calypso</taxon_name>
    <taxon_name authority="(Linnaeus) Oakes" date="1842" rank="species">bulbosa</taxon_name>
    <taxon_name authority="(R. Brown) Luer" date="1975" rank="variety">americana</taxon_name>
    <place_of_publication>
      <publication_title>Native Orchids U.S. &amp; Canada,</publication_title>
      <place_in_publication>336. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe calypsoeae;genus calypso;species bulbosa;variety americana;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102207</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calypso</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>5: 208. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Calypso;species americana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Flowers with prominent tuft of contrasting yellow bristles at base of lamina;</text>
      <biological_entity id="o2368" name="flower" name_original="flowers" src="d0_s0" type="structure" />
      <biological_entity id="o2369" name="tuft" name_original="tuft" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o2370" name="bristle" name_original="bristles" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o2371" name="lamina" name_original="lamina" src="d0_s0" type="structure" />
      <relation from="o2368" id="r309" name="with" negation="false" src="d0_s0" to="o2369" />
      <relation from="o2369" id="r310" name="part_of" negation="false" src="d0_s0" to="o2370" />
      <relation from="o2370" id="r311" name="part_of" negation="false" src="d0_s0" to="o2371" />
    </statement>
    <statement id="d0_s1">
      <text>lamina white or suffused with pink, equaling or longer than conspicuous horns.</text>
      <biological_entity id="o2373" name="horn" name_original="horns" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>2n = 32.</text>
      <biological_entity id="o2372" name="lamina" name_original="lamina" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="suffused with pink" value_original="suffused with pink" />
        <character is_modifier="false" name="variability" src="d0_s1" value="equaling" value_original="equaling" />
        <character constraint="than conspicuous horns" constraintid="o2373" is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2374" name="chromosome" name_original="" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesic to wet coniferous forests, mixed forests, and bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mesic to wet coniferous" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Colo., Idaho, Maine, Mich., Minn., Mont., N.H., N.Mex., N.Y., S.Dak., Utah, Vt., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>