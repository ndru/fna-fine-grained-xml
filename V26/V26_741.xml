<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="treatment_page">364</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sisyrinchium</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="1901" rank="species">biforme</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 575. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus sisyrinchium;species biforme</taxon_hierarchy>
    <other_info_on_name type="fna_id">242101894</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisyrinchium</taxon_name>
    <taxon_name authority="R. L. Oliver" date="unknown" rank="species">dimorphum</taxon_name>
    <taxon_hierarchy>genus Sisyrinchium;species dimorphum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, pale to olive green or occasionally bronze when dry, to 4.7 dm, not glaucous;</text>
      <biological_entity id="o11164" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="pale" modifier="when dry" name="coloration" src="d0_s0" to="olive green or occasionally bronze" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="4.7" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes scarcely discernable.</text>
      <biological_entity id="o11165" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="scarcely" name="prominence" src="d0_s1" value="discernable" value_original="discernable" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or branched, with 1 node, 1.1–1.9 dm wide, glabrous, margins entire, usually same color and texture as stem body but occasionally drying lighter;</text>
      <biological_entity id="o11166" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.1" from_unit="dm" name="width" notes="" src="d0_s2" to="1.9" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11167" name="node" name_original="node" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11168" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="as stem body" constraintid="o11169" is_modifier="false" modifier="usually" name="texture" src="d0_s2" value="color" value_original="color" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="lighter" value_original="lighter" />
      </biological_entity>
      <biological_entity constraint="stem" id="o11169" name="body" name_original="body" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally" name="condition" src="d0_s2" value="drying" value_original="drying" />
      </biological_entity>
      <relation from="o11166" id="r1568" name="with" negation="false" src="d0_s2" to="o11167" />
    </statement>
    <statement id="d0_s3">
      <text>internode 17.2–21.3 cm, equaling or shorter than leaves, with 2 branches.</text>
      <biological_entity id="o11170" name="internode" name_original="internode" src="d0_s3" type="structure">
        <character char_type="range_value" from="17.2" from_unit="cm" name="some_measurement" src="d0_s3" to="21.3" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
        <character constraint="than leaves" constraintid="o11171" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11171" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11172" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <relation from="o11170" id="r1569" name="with" negation="false" src="d0_s3" to="o11172" />
    </statement>
    <statement id="d0_s4">
      <text>Leaf-blades glabrous, bases not persistent in fibrous tufts.</text>
      <biological_entity id="o11173" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11174" name="base" name_original="bases" src="d0_s4" type="structure">
        <character constraint="in tufts" constraintid="o11175" is_modifier="false" modifier="not" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11175" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences borne singly;</text>
      <biological_entity id="o11176" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes green, obviously wider than supporting branch, glabrous, keels entire;</text>
      <biological_entity id="o11177" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character constraint="than supporting branch" constraintid="o11178" is_modifier="false" name="width" src="d0_s6" value="obviously wider" value_original="obviously wider" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11178" name="branch" name_original="branch" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity id="o11179" name="keel" name_original="keels" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer 13–19.6 mm on branched stems, 19.3–25.6 mm on simple stems, 1.2 mm longer to 2 mm shorter than inner on branched stems, 1.5–4.2 mm longer than inner on simple stems, tapering evenly towards apex, margins basally connate 3.9–6.1 mm;</text>
      <biological_entity constraint="outer" id="o11180" name="spathe" name_original="spathe" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="on stems" constraintid="o11181" from="13" from_unit="mm" name="some_measurement" src="d0_s7" to="19.6" to_unit="mm" />
        <character char_type="range_value" constraint="on stems" constraintid="o11182" from="19.3" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="25.6" to_unit="mm" />
        <character name="some_measurement" notes="" src="d0_s7" unit="mm" value="1.2" value_original="1.2" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o11183" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="4.2" to_unit="mm" />
        <character constraint="than inner spathe" constraintid="o11185" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character constraint="towards apex" constraintid="o11187" is_modifier="false" name="shape" notes="" src="d0_s7" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o11181" name="stem" name_original="stems" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o11182" name="stem" name_original="stems" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11183" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o11184" name="stem" name_original="stems" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11185" name="spathe" name_original="spathe" src="d0_s7" type="structure" />
      <biological_entity id="o11186" name="stem" name_original="stems" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o11187" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o11188" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s7" to="6.1" to_unit="mm" />
      </biological_entity>
      <relation from="o11183" id="r1570" name="on" negation="false" src="d0_s7" to="o11184" />
      <relation from="o11185" id="r1571" name="on" negation="false" src="d0_s7" to="o11186" />
    </statement>
    <statement id="d0_s8">
      <text>inner with keel evenly curved, hyaline margins 0.2–0.5 mm wide, apex acuminate to acute, ending 0.2–0.8 mm proximal to green apex on branched stems, sometimes lobed and exceeding green apex on simple stems.</text>
      <biological_entity constraint="inner" id="o11189" name="spathe" name_original="spathe" src="d0_s8" type="structure" />
      <biological_entity id="o11190" name="keel" name_original="keel" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="evenly" name="course" src="d0_s8" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o11191" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11192" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="acute" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o11193" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o11194" name="stem" name_original="stems" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o11195" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o11196" name="stem" name_original="stems" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o11189" id="r1572" name="with" negation="false" src="d0_s8" to="o11190" />
      <relation from="o11193" id="r1573" name="on" negation="false" src="d0_s8" to="o11194" />
      <relation from="o11193" id="r1574" name="exceeding" negation="false" src="d0_s8" to="o11195" />
      <relation from="o11193" id="r1575" name="on" negation="false" src="d0_s8" to="o11196" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals light blue, bases yellow;</text>
      <biological_entity id="o11197" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11198" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light blue" value_original="light blue" />
      </biological_entity>
      <biological_entity id="o11199" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer tepals 5–13 mm, apex rounded, short-aristate;</text>
      <biological_entity id="o11200" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o11201" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11202" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="short-aristate" value_original="short-aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments connate ± entirely, stipitate-glandular basally;</text>
      <biological_entity id="o11203" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11204" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less entirely; entirely" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary similar in color to foliage.</text>
      <biological_entity id="o11205" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11206" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
      <biological_entity id="o11207" name="foliage" name_original="foliage" src="d0_s12" type="structure" />
      <relation from="o11206" id="r1576" name="to" negation="false" src="d0_s12" to="o11207" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules light to dark-brown, ± globose to ± fusiform, 5–7 mm;</text>
      <biological_entity id="o11208" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s13" to="dark-brown" />
        <character char_type="range_value" from="less globose" name="shape" src="d0_s13" to="more or less fusiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel usually erect.</text>
      <biological_entity id="o11209" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds globose to obconic, occasionally with slight depression, 1–1.3 mm, rugulose.</text>
      <biological_entity id="o11211" name="depression" name_original="depression" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="slight" value_original="slight" />
      </biological_entity>
      <relation from="o11210" id="r1577" modifier="occasionally" name="with" negation="false" src="d0_s15" to="o11211" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 96.</text>
      <biological_entity id="o11210" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s15" to="obconic" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11212" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="96" value_original="96" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows or swales within stabilized coastal dunes, sandy river banks inland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" constraint="within stabilized coastal dunes" />
        <character name="habitat" value="swales" constraint="within stabilized coastal dunes" />
        <character name="habitat" value="stabilized coastal dunes" />
        <character name="habitat" value="sandy river banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.; adjacent coastal n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="adjacent coastal n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  
</bio:treatment>