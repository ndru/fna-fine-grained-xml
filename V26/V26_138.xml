<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">trillium</taxon_name>
    <taxon_name authority="Rafinesque" date="1820" rank="subgenus">PHYLLANTHERUM</taxon_name>
    <taxon_name authority="J. D. Freeman" date="1975" rank="species">reliquum</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>27: 21, fig. 5. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus trillium;subgenus phyllantherum;species reliquum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242102008</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes short, stout, praemorse.</text>
      <biological_entity id="o16081" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s0" value="praemorse" value_original="praemorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scapes 1–2, semidecumbent, decumbent, or weakly erect (especially in cultivation), S-shaped, round in cross-section, 0.6–1.8 dm, glabrous.</text>
      <biological_entity id="o16082" name="scape" name_original="scapes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="semidecumbent" value_original="semidecumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s1" value="s--shaped" value_original="s--shaped" />
        <character constraint="in cross-section" constraintid="o16083" is_modifier="false" name="shape" src="d0_s1" value="round" value_original="round" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" notes="" src="d0_s1" to="1.8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16083" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Bracts resting on or near ground surface, horizontally spreading, sessile;</text>
      <biological_entity id="o16084" name="bract" name_original="bracts" src="d0_s2" type="structure" />
      <biological_entity id="o16085" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="horizontally" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o16085" id="r2189" name="resting on or near" negation="false" src="d0_s2" to="o16085" />
    </statement>
    <statement id="d0_s3">
      <text>blade strongly mottled on each side of central light green stripe in shades of light green, dark green, bronze green, and dark purple, mottling becoming obscure with age, ovate to elliptic, rounded-tapered ± equally from base to tip from widest point, 5–12 × 6–10 cm, apex rounded or weakly acute.</text>
      <biological_entity id="o16086" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="on side" constraintid="o16087" is_modifier="false" modifier="strongly" name="coloration" src="d0_s3" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="bronze green" value_original="bronze green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="mottling" value_original="mottling" />
        <character constraint="with age" constraintid="o16090" is_modifier="false" modifier="becoming" name="prominence" src="d0_s3" value="obscure" value_original="obscure" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="elliptic" />
        <character constraint="from base" constraintid="o16091" is_modifier="false" modifier="more or less equally" name="shape" src="d0_s3" value="rounded-tapered" value_original="rounded-tapered" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" notes="" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16087" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="central" id="o16088" name="stripe" name_original="stripe" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="light green" value_original="light green" />
      </biological_entity>
      <biological_entity id="o16089" name="shade-of" name_original="shades_of" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
      </biological_entity>
      <biological_entity id="o16090" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity id="o16091" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o16092" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <biological_entity constraint="widest" id="o16093" name="point" name_original="point" src="d0_s3" type="structure" />
      <biological_entity id="o16094" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o16087" id="r2190" name="part_of" negation="false" src="d0_s3" to="o16088" />
      <relation from="o16087" id="r2191" name="in" negation="false" src="d0_s3" to="o16089" />
      <relation from="o16091" id="r2192" name="to" negation="false" src="d0_s3" to="o16092" />
      <relation from="o16092" id="r2193" name="from" negation="false" src="d0_s3" to="o16093" />
    </statement>
    <statement id="d0_s4">
      <text>Flower borne directly on bracts, odor of putrid meat;</text>
      <biological_entity id="o16095" name="flower" name_original="flower" src="d0_s4" type="structure" />
      <biological_entity id="o16096" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity id="o16097" name="meat" name_original="meat" src="d0_s4" type="structure">
        <character is_modifier="true" name="odor" src="d0_s4" value="putrid" value_original="putrid" />
      </biological_entity>
      <relation from="o16095" id="r2194" name="borne" negation="false" src="d0_s4" to="o16096" />
    </statement>
    <statement id="d0_s5">
      <text>sepals divergent, somewhat recurved, green, maroon streaked, lanceolate, 17–42 × 5–9.5 mm, margins entire, flat, apex rounded-acute;</text>
      <biological_entity id="o16098" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="maroon streaked" value_original="maroon streaked" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s5" to="42" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="9.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16099" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o16100" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-acute" value_original="rounded-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals long-lasting, erect, ± connivent, ± concealing stamens and ovary, dark brownish maroon, greenish purple, or streaked with yellow, usually not spirally twisted, narrowly elliptic-lanceolate to oblanceolate, 2.5–5.5 × 0.6–1 cm, thick-textured, margins entire, ± flat, apex acute;</text>
      <biological_entity id="o16101" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="long-lasting" value_original="long-lasting" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="dark brownish" value_original="dark brownish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="streaked with yellow" value_original="streaked with yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish purple" value_original="greenish purple" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="streaked with yellow" value_original="streaked with yellow" />
        <character is_modifier="false" modifier="usually not spirally" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="narrowly elliptic-lanceolate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="thick-textured" value_original="thick-textured" />
      </biological_entity>
      <biological_entity id="o16102" name="stamen" name_original="stamens" src="d0_s6" type="structure" />
      <biological_entity id="o16103" name="ovary" name_original="ovary" src="d0_s6" type="structure" />
      <biological_entity id="o16104" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o16105" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o16101" id="r2195" name="more or less concealing" negation="false" src="d0_s6" to="o16102" />
    </statement>
    <statement id="d0_s7">
      <text>stamens erect, incurved, 12–20 mm;</text>
      <biological_entity id="o16106" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments ± straight, reddish-brown, 1–2 mm, slender;</text>
      <biological_entity id="o16107" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers ± straight, dark purple, 4–20 mm, ± thick, dehiscence introrse;</text>
      <biological_entity id="o16108" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="width" src="d0_s9" value="thick" value_original="thick" />
        <character is_modifier="false" name="dehiscence" src="d0_s9" value="introrse" value_original="introrse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>connectives brown-purple, extending 1–2.5 mm beyond anther sacs, apex acute;</text>
      <biological_entity id="o16109" name="connectif" name_original="connectives" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="brown-purple" value_original="brown-purple" />
      </biological_entity>
      <biological_entity id="o16110" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="beyond anther sacs" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary green-purple, ovoid, 6-angled, 5–10 mm;</text>
      <biological_entity id="o16111" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="green-purple" value_original="green-purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="6-angled" value_original="6-angled" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas erect, divergent-recurved, distinct, linear, 2–4 mm, uniformly thin.</text>
      <biological_entity id="o16112" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="divergent-recurved" value_original="divergent-recurved" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="uniformly" name="width" src="d0_s12" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits baccate, dark maroon-purple, fragrance unreported, ovoid, 6-winged or angled apically, 0.7–1 cm, pulpy, moist.</text>
      <biological_entity id="o16113" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="baccate" value_original="baccate" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark maroon-purple" value_original="dark maroon-purple" />
        <character is_modifier="false" name="fragrance" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="6-winged" value_original="6-winged" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s13" value="angled" value_original="angled" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s13" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s13" value="pulpy" value_original="pulpy" />
        <character is_modifier="false" name="texture" src="d0_s13" value="moist" value_original="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–spring (early Mar–Apr).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="late winter" />
        <character name="flowering time" char_type="range_value" to="Apr" from="early Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich mixed deciduous forested slopes, bluffs, stream-flats, lower slopes at edge of small stream floodplains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich mixed deciduous" />
        <character name="habitat" value="slopes" modifier="forested" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="stream-flats" />
        <character name="habitat" value="lower slopes" />
        <character name="habitat" value="edge" modifier="at" constraint="of small stream floodplains" />
        <character name="habitat" value="small stream floodplains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="common_name">Relict trillium</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Trillium reliquum recently has been reported from Alabama; I have not seen specimens from there. Otherwise, the species occurs in Clay, Early, and Richmond counties, Georgia, and in Aiken County, South Carolina. It is currently listed as an endangered species in the United States.</discussion>
  
</bio:treatment>