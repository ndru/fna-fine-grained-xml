<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">634</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="E. G. Camus, Bergon &amp; A. Camus" date="1908" rank="subtribe">Corallorhizinae</taxon_name>
    <taxon_name authority="Gagnebin" date="unknown" rank="genus">corallorhiza</taxon_name>
    <taxon_name authority="Lindley" date="1840" rank="species">striata</taxon_name>
    <taxon_name authority="(Rydberg) L. O. Williams" date="unknown" rank="variety">vreelandii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 343. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe corallorhizinae;genus corallorhiza;species striata;variety vreelandii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">242102231</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">vreelandii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 271. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Corallorhiza;species vreelandii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">bigelovii</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species bigelovii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">ochroleuca</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species ochroleuca;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Corallorhiza</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">striata</taxon_name>
    <taxon_name authority="Todsen &amp; T. A. Todsen" date="unknown" rank="variety">flavida</taxon_name>
    <taxon_hierarchy>genus Corallorhiza;species striata;variety flavida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Racemes 10–44 cm.</text>
      <biological_entity id="o24557" name="raceme" name_original="racemes" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="44" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers 2–28, conspicuous;</text>
      <biological_entity id="o24558" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="28" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sepals and petals often connivent around column, nodding;</text>
      <biological_entity id="o24559" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character constraint="around column" constraintid="o24561" is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o24560" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character constraint="around column" constraintid="o24561" is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="nodding" value_original="nodding" />
      </biological_entity>
      <biological_entity id="o24561" name="column" name_original="column" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>sepals 5–11 mm;</text>
      <biological_entity id="o24562" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lateral sepals often broadened at base, nearly auriculate;</text>
      <biological_entity constraint="lateral" id="o24563" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character constraint="at base" constraintid="o24564" is_modifier="false" modifier="often" name="width" src="d0_s4" value="broadened" value_original="broadened" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="" src="d0_s4" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o24564" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5–11 mm;</text>
      <biological_entity id="o24565" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lip 2–7 × 1.5–4.5 mm;</text>
      <biological_entity id="o24566" name="lip" name_original="lip" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>column 1.5–4 mm.</text>
      <biological_entity id="o24567" name="column" name_original="column" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 11–23 mm.</text>
      <biological_entity id="o24568" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s8" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coniferous woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Que.; Ariz., Calif., Colo., Idaho, Nev., N.Mex., Oreg., S.Dak., Utah, Wash., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <other_name type="past_name">Corallorrhiza</other_name>
  <discussion>The two varieties of Corallorhiza striata intergrade in California and Oregon, although they are generally distinct. Small specimens from Quebec will key to this variety; they are not thought to be closely related to populations from the southwest.</discussion>
  
</bio:treatment>