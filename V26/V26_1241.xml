<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Eric Hágsater</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">613</other_info_on_meta>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Kunth" date="1815" rank="tribe">Epidendreae</taxon_name>
    <taxon_name authority="Bentham" date="1881" rank="subtribe">Laeliinae</taxon_name>
    <taxon_name authority="Hooker" date="1828" rank="genus">ENCYCLIA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>55: plate 2831. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe epidendreae;subtribe laeliinae;genus encyclia;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek enkyklos, to encircle, referring to the lateral lobes of the lip, which encircle the column</other_info_on_name>
    <other_info_on_name type="fna_id">111611</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, epiphytic.</text>
      <biological_entity id="o7882" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fleshy, glabrous.</text>
      <biological_entity id="o7883" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems aggregate, erect, forming ovoid-pyriform pseudobulbs.</text>
      <biological_entity id="o7884" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="aggregate" value_original="aggregate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o7885" name="pseudobulb" name_original="pseudobulbs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ovoid-pyriform" value_original="ovoid-pyriform" />
      </biological_entity>
      <relation from="o7884" id="r1123" name="forming" negation="false" src="d0_s2" to="o7885" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves apical, sessile, articulate;</text>
      <biological_entity id="o7886" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="apical" value_original="apical" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear-elliptic, leathery.</text>
      <biological_entity id="o7887" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="linear-elliptic" value_original="linear-elliptic" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences apical, racemes to panicles, flowering only once;</text>
      <biological_entity id="o7888" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o7889" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="only once; once" name="life_cycle" notes="" src="d0_s5" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o7890" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <relation from="o7889" id="r1124" name="to" negation="false" src="d0_s5" to="o7890" />
    </statement>
    <statement id="d0_s6">
      <text>spathe absent.</text>
      <biological_entity id="o7891" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers resupinate, pedicellate, simultaneous;</text>
      <biological_entity id="o7892" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="resupinate" value_original="resupinate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="development" src="d0_s7" value="simultaneous" value_original="simultaneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals subequal;</text>
      <biological_entity id="o7893" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals similar;</text>
      <biological_entity id="o7894" name="petal" name_original="petals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lip appressed to column, adnate only at base, callus boatshaped;</text>
      <biological_entity id="o7895" name="lip" name_original="lip" src="d0_s10" type="structure">
        <character constraint="to column" constraintid="o7896" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character constraint="at base" constraintid="o7897" is_modifier="false" name="fusion" notes="" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o7896" name="column" name_original="column" src="d0_s10" type="structure" />
      <biological_entity id="o7897" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o7898" name="callus" name_original="callus" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="boat-shaped" value_original="boat-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollinia 4, obovoid, laterally compressed, subequal;</text>
      <biological_entity id="o7899" name="pollinium" name_original="pollinia" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>caudicles 4 in 2 pairs;</text>
      <biological_entity id="o7900" name="caudicle" name_original="caudicles" src="d0_s12" type="structure">
        <character constraint="in pairs" constraintid="o7901" name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o7901" name="pair" name_original="pairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>column winged;</text>
      <biological_entity id="o7902" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>rostellum semiorbiculate, entire, abaxially covered with viscous substance.</text>
      <biological_entity id="o7903" name="rostellum" name_original="rostellum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="semiorbiculate" value_original="semiorbiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7904" name="substance" name_original="substance" src="d0_s14" type="structure">
        <character is_modifier="true" name="texture" src="d0_s14" value="viscous" value_original="viscous" />
      </biological_entity>
      <relation from="o7903" id="r1125" modifier="abaxially" name="covered with" negation="false" src="d0_s14" to="o7904" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, ellipsoid, 3-ribbed, 1-locular.</text>
      <biological_entity constraint="fruits" id="o7905" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="3-ribbed" value_original="3-ribbed" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Neotropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Neotropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <discussion>Species over 750 (1 in the flora).</discussion>
  <discussion>Stems of the encyclias are variable outside the flora area: rarely they are canelike and do not form pseudobulbs; leaves are sometimes semiterete. The genus has been misrepresented as epidendrums having pseudobulbs (O. Ames et al. 1936), as most of them do; some epidendrums also do.</discussion>
  <discussion>The Florida species were segregated by J. K. Small (1933) into four genera: Encyclia, Hormidium, Anacheilium, and Epicladium. Most later authors recognized only Epidendrum or Epidendrum and Encyclia. Recent attempts for the reestablishment of Anacheilium and Hormidium (G. F. Pabst et al. 1981; R. P. Sauleda et al. 1984) were based mainly on Brazilian species and did not take Mexican species into consideration. Prosthechea has been recently recognized (W. E. Higgins 1997[1998]), thus leaving only one species of Encyclia in the flora area.</discussion>
  <discussion>Encyclia rufa (Lindley) Britton &amp; Millspaugh has been excluded from the flora because only one specimen has been recorded, and it was probably introduced (R. P. Sauleda 1983). The species is endemic to the Bahamas and Cuba.</discussion>
  <references>
    <reference>Dressler, R. L. and G. E. Pollard. 1976. The Genus Encyclia in Mexico, ed. 2. Mexico City.  </reference>
    <reference>Pabst, G. F., J. L. Moutinho, and A. V. Pinto. 1981. An attempt to establish the correct statement for genus Anacheilium Hoffmgg. and revision of the genus Hormidium Lindl. ex Heynh. Bradea 3: 173–186.  </reference>
    <reference>Sauleda, R. P. and R. M. Adams. 1983. The genus Encyclia Hook. (Orchidaceae) in the Bahama Archipelago. Rhodora 85: 127–174.  </reference>
  </references>
  
</bio:treatment>