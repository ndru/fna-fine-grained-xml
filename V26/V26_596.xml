<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="genus">camassia</taxon_name>
    <taxon_name authority="(Pursh) Greene" date="1894" rank="species">quamash</taxon_name>
    <taxon_name authority="Gould" date="1942" rank="subspecies">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>28: 736, figs. 5(5–7), 7, 9. 1942</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus camassia;species quamash;subspecies linearis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102214</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 6–15 mm wide, not glaucous.</text>
      <biological_entity id="o27677" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s0" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers slightly zygomorphic;</text>
      <biological_entity id="o27678" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s1" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>tepals connivent over capsules after anthesis, usually forced apart again by capsule growth, deep bluish violet, each 5-, 7-, or 9- veined, occasionally 3-veined in outer whorls, 20–35 × 5–8 mm;</text>
      <biological_entity id="o27679" name="tepal" name_original="tepals" src="d0_s2" type="structure">
        <character constraint="over capsules" constraintid="o27680" is_modifier="false" name="arrangement" src="d0_s2" value="connivent" value_original="connivent" />
        <character is_modifier="false" modifier="after anthesis; usually; after anthesis; usually" name="depth" src="d0_s2" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bluish violet" value_original="bluish violet" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="9-veined" value_original="9-veined" />
        <character constraint="in outer whorls" constraintid="o27682" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="3-veined" value_original="3-veined" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27680" name="capsule" name_original="capsules" src="d0_s2" type="structure" />
      <biological_entity constraint="capsule" id="o27681" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
      </biological_entity>
      <biological_entity constraint="outer" id="o27682" name="whorl" name_original="whorls" src="d0_s2" type="structure" />
      <relation from="o27679" id="r3749" modifier="after anthesis; usually" name="forced" negation="false" src="d0_s2" to="o27681" />
    </statement>
    <statement id="d0_s3">
      <text>anthers dull yellow to violet, 4–7 mm;</text>
    </statement>
    <statement id="d0_s4">
      <text>fruiting pedicel incurving-erect, 10–25 mm.</text>
      <biological_entity id="o27683" name="anther" name_original="anthers" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dull" value_original="dull" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s3" to="violet" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="incurving-erect" value_original="incurving-erect" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27684" name="pedicel" name_original="pedicel" src="d0_s4" type="structure" />
      <relation from="o27683" id="r3750" name="fruiting" negation="false" src="d0_s4" to="o27684" />
    </statement>
    <statement id="d0_s5">
      <text>Capsules 6–18 mm.</text>
      <biological_entity id="o27685" name="capsule" name_original="capsules" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seeds 5–10 per locule.</text>
      <biological_entity id="o27686" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o27687" from="5" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity id="o27687" name="locule" name_original="locule" src="d0_s6" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid–late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late spring" from="mid" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1f.</number>
  
</bio:treatment>