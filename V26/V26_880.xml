<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="treatment_page">429</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">agavaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">yucca</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">gloriosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 319. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family agavaceae;genus yucca;species gloriosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102065</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming colonies of rosettes, caulescent, arborescent, simple or more often branching.</text>
      <biological_entity id="o24876" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24877" name="colony" name_original="colonies" src="d0_s0" type="structure" />
      <biological_entity id="o24878" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <relation from="o24876" id="r3375" name="forming" negation="false" src="d0_s0" to="o24877" />
      <relation from="o24876" id="r3376" name="part_of" negation="false" src="d0_s0" to="o24878" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, to 5 m.</text>
      <biological_entity id="o24879" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blade erect or recurving, green or blue-green, lanceolate or sword-shaped, flattened, concave distally, thin, 40–100 × 3.5–6 cm, rigid or flexible, glaucous at least when young, margins entire or roughly and minutely denticulate, often becoming filiferous with straight fibers, yellow or brown, opaque.</text>
      <biological_entity id="o24880" name="leaf-blade" name_original="leaf-blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="recurving" value_original="recurving" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="blue-green" value_original="blue-green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="sword--shaped" value_original="sword--shaped" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character char_type="range_value" from="40" from_unit="cm" name="length" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o24881" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s2" value="roughly" value_original="roughly" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
        <character constraint="with fibers" constraintid="o24882" is_modifier="false" modifier="often becoming" name="architecture" src="d0_s2" value="filiferous" value_original="filiferous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="opaque" value_original="opaque" />
      </biological_entity>
      <biological_entity id="o24882" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences paniculate, arising partly within to well beyond rosettes, ovoid to ellipsoid, 5–12 × 4.5 dm, glabrous or pubescent;</text>
      <biological_entity id="o24883" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character constraint="well beyond rosettes" constraintid="o24884" is_modifier="false" name="orientation" src="d0_s3" value="arising" value_original="arising" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s3" to="ellipsoid" />
        <character char_type="range_value" from="5" from_unit="dm" name="length" src="d0_s3" to="12" to_unit="dm" />
        <character name="width" src="d0_s3" unit="dm" value="4.5" value_original="4.5" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24884" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>peduncle scapelike, 0.9–1.5 m.</text>
      <biological_entity id="o24885" name="peduncle" name_original="peduncle" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="scapelike" value_original="scapelike" />
        <character char_type="range_value" from="0.9" from_unit="m" name="some_measurement" src="d0_s4" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers pendent;</text>
      <biological_entity id="o24886" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>perianth globose to campanulate;</text>
      <biological_entity id="o24887" name="perianth" name_original="perianth" src="d0_s6" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s6" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tepals distinct, white to creamy white or greenish white, sometimes tinged with purple, elliptic to narrowly ovate, 4–5 × 2–2.5 cm;</text>
      <biological_entity id="o24888" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="creamy white or greenish white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="tinged with purple" value_original="tinged with purple" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s7" to="narrowly ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments ca. 2.6 cm, hispid or slightly papillose;</text>
      <biological_entity id="o24889" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="cm" value="2.6" value_original="2.6" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers ca. 3.5 mm;</text>
      <biological_entity id="o24890" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="3.5" value_original="3.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistil light green, ca. 3.6 cm;</text>
      <biological_entity id="o24891" name="pistil" name_original="pistil" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light green" value_original="light green" />
        <character name="some_measurement" src="d0_s10" unit="cm" value="3.6" value_original="3.6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary sessile, ca. 2.8 cm;</text>
      <biological_entity id="o24892" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="some_measurement" src="d0_s11" unit="cm" value="2.8" value_original="2.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style ca. 9 mm;</text>
      <biological_entity id="o24893" name="style" name_original="style" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="9" value_original="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas separate;</text>
      <biological_entity id="o24894" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicel to 2 cm, often arching.</text>
      <biological_entity id="o24895" name="pedicel" name_original="pedicel" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s14" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s14" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits erect or pendent, baccate, with core, indehiscent, 6-winged or 6-ribbed, elongate, 2.5–8 cm, leathery.</text>
      <biological_entity id="o24896" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="baccate" value_original="baccate" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s15" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="6-winged" value_original="6-winged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="6-ribbed" value_original="6-ribbed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s15" to="8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o24897" name="core" name_original="core" src="d0_s15" type="structure" />
      <relation from="o24896" id="r3377" name="with" negation="false" src="d0_s15" to="o24897" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds black, lustrous, ovate, thin, 5–8 mm diam. 2n = 50.</text>
      <biological_entity id="o24898" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24899" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="50" value_original="50" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Yucca gloriosa has a growth habit similar to that of Y. aloifolia, except that the former appears more moundlike due to the terminal branching mode, whereas the latter appears more open because the branching is more median on the trunk.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade mostly erect, rigid; inflorescences extending well beyond rosettes; berries pendent, 5.5–8 cm.</description>
      <determination>8a Yucca gloriosa var. gloriosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blade mostly recurving, flexible; inflorescences barely extending beyond rosettes; berries mostly erect, 2.5–4.5 cm.</description>
      <determination>8b Yucca gloriosa var. recurvifolia</determination>
    </key_statement>
  </key>
</bio:treatment>