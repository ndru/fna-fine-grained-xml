<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">liliaceae</taxon_name>
    <taxon_name authority="Douglas ex Lindley" date="1830" rank="genus">triteleia</taxon_name>
    <taxon_name authority="(W. T. Aiton) Greene" date="1886" rank="species">ixioides</taxon_name>
    <taxon_name authority="(Hoover) L. W. Lenz" date="1975" rank="subspecies">cookii</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>8: 237. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family liliaceae;genus triteleia;species ixioides;subspecies cookii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242102318</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Triteleia</taxon_name>
    <taxon_name authority="(Lindley) C. V. Morton" date="unknown" rank="species">ixioides</taxon_name>
    <taxon_name authority="Hoover" date="unknown" rank="variety">cookii</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Life</publication_title>
      <place_in_publication>1: 19, plate 2(left). 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Triteleia;species ixioides;variety cookii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brodiaea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lutea</taxon_name>
    <taxon_name authority="(Hoover) Munz" date="unknown" rank="variety">cookii</taxon_name>
    <taxon_hierarchy>genus Brodiaea;species lutea;variety cookii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves 1–2, 10–20 cm.</text>
      <biological_entity id="o5891" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="2" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Scape 20–25 cm, slightly scabrous.</text>
      <biological_entity id="o5892" name="scape" name_original="scape" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="pubescence_or_relief" src="d0_s1" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: perianth white to pale straw-colored, sometimes with darker midvein color extending into distal portion of tepal lobe, tube 6–10 mm, nearly equal to lobes, lobes strongly reflexed, 6–10 mm;</text>
      <biological_entity id="o5893" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o5894" name="perianth" name_original="perianth" src="d0_s2" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s2" to="pale straw-colored" />
      </biological_entity>
      <biological_entity id="o5895" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="darker" value_original="darker" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="extending" value_original="extending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5896" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity constraint="tepal" id="o5897" name="lobe" name_original="lobe" src="d0_s2" type="structure" />
      <biological_entity id="o5898" name="tube" name_original="tube" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character constraint="to lobes" constraintid="o5899" is_modifier="false" modifier="nearly" name="variability" src="d0_s2" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o5899" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity id="o5900" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <relation from="o5894" id="r850" modifier="sometimes" name="with" negation="false" src="d0_s2" to="o5895" />
      <relation from="o5895" id="r851" name="into" negation="false" src="d0_s2" to="o5896" />
      <relation from="o5896" id="r852" name="part_of" negation="false" src="d0_s2" to="o5897" />
    </statement>
    <statement id="d0_s3">
      <text>short filaments 3 mm, long filaments 4 mm, apical appendages straight or slightly recurved;</text>
      <biological_entity id="o5901" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o5902" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o5903" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character name="some_measurement" src="d0_s3" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="apical" id="o5904" name="appendage" name_original="appendages" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>anthers white;</text>
      <biological_entity id="o5905" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5906" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pedicel 1–12 cm. 2n = 14, 28.</text>
      <biological_entity id="o5907" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5908" name="pedicel" name_original="pedicel" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5909" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="14" value_original="14" />
        <character name="quantity" src="d0_s5" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream sides, wet ravines on serpentine, often near cypresses</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream sides" />
        <character name="habitat" value="wet ravines" constraint="on serpentine , often near cypresses" />
        <character name="habitat" value="serpentine" />
        <character name="habitat" value="near cypresses" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8c.</number>
  <other_name type="common_name">Cook’s triteleia</other_name>
  <discussion>Subspecies cookii is restricted to San Luis Obispo County and is recognized by its long pedicels and relatively long perianth tube in relation to the length of the perianth lobes.</discussion>
  
</bio:treatment>