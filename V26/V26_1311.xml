<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Edward W. Greenwood†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">495</other_info_on_meta>
    <other_info_on_meta type="mention_page">639</other_info_on_meta>
    <other_info_on_meta type="treatment_page">638</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">orchidaceae</taxon_name>
    <taxon_name authority="Lindley" date="1826" rank="subfamily">Epidendroideae</taxon_name>
    <taxon_name authority="Pfitzer" date="1887" rank="tribe">Cymbidieae</taxon_name>
    <taxon_name authority="Dressler" date="1990" rank="subtribe">Goveniinae</taxon_name>
    <taxon_name authority="Lindley" date="1832" rank="genus">GOVENIA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Orchid. Pl.,</publication_title>
      <place_in_publication>153. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily epidendroideae;tribe cymbidieae;subtribe goveniinae;genus govenia;</taxon_hierarchy>
    <other_info_on_name type="etymology">For J. R. Gowen, English collector in Assam</other_info_on_name>
    <other_info_on_name type="fna_id">113964</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, terrestrial.</text>
      <biological_entity id="o8930" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots spreading, slender, fleshy.</text>
      <biological_entity id="o8931" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems reduced to short, nearly ovoid pseudobulbs.</text>
      <biological_entity id="o8932" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="reduced" name="size" src="d0_s2" to="short" />
      </biological_entity>
      <biological_entity id="o8933" name="pseudobulb" name_original="pseudobulbs" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="nearly" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves petiolate, articulate, from multiple, concentric, basal sheaths;</text>
      <biological_entity id="o8934" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8935" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="multiple" value_original="multiple" />
        <character is_modifier="true" name="position" src="d0_s3" value="concentric" value_original="concentric" />
      </biological_entity>
      <relation from="o8934" id="r1285" name="from" negation="false" src="d0_s3" to="o8935" />
    </statement>
    <statement id="d0_s4">
      <text>blade pleated.</text>
      <biological_entity id="o8936" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="pleated" value_original="pleated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences lateral from within sheaths, racemes, erect, loose.</text>
      <biological_entity id="o8937" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="within sheaths" constraintid="o8938" is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o8938" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o8939" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers resupinate;</text>
      <biological_entity id="o8940" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="resupinate" value_original="resupinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>columns fleshy, arcuate 90o, prominently winged, foot prominent [absent];</text>
      <biological_entity id="o8941" name="column" name_original="columns" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="course_or_shape" src="d0_s7" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o8942" name="foot" name_original="foot" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers terminal;</text>
      <biological_entity id="o8943" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pollinaria with 2 pairs of pollinia, stipe, and viscidium.</text>
      <biological_entity id="o8944" name="pollinarium" name_original="pollinaria" src="d0_s9" type="structure" />
      <biological_entity id="o8945" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8946" name="pollinium" name_original="pollinia" src="d0_s9" type="structure" />
      <biological_entity id="o8947" name="stipe" name_original="stipe" src="d0_s9" type="structure" />
      <biological_entity id="o8948" name="viscidium" name_original="viscidium" src="d0_s9" type="structure" />
      <relation from="o8944" id="r1286" name="with" negation="false" src="d0_s9" to="o8945" />
      <relation from="o8945" id="r1287" name="part_of" negation="false" src="d0_s9" to="o8946" />
      <relation from="o8945" id="r1288" name="part_of" negation="false" src="d0_s9" to="o8947" />
      <relation from="o8945" id="r1289" name="part_of" negation="false" src="d0_s9" to="o8948" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical regions, North America, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>57.</number>
  <discussion>Species 30 or more (1 in the flora).</discussion>
  <references>
    <reference>  Brown, P. M. 2000d. Govenia floridana (Orchidaceae), a new species endemic to southern Florida, U.S.A. N. Amer. Native Orchid J. 6: 231–238.</reference>
    <reference>Correll, D. S. 1947. Revision of the genus Govenia. Lloydia 10: 218–278.  </reference>
    <reference>Fawcett, W. and A. B. Rendle. 1910–1936. Flora of Jamaica…. 5 vols. London. Vol. 1, p. 113.  </reference>
    <reference>Greenwood, E. W. 1982b. Govenia in Mexico, an introductory note. Orquidea (Mexico City) 8: 114–120.  </reference>
    <reference>Greenwood, E. W. 1991. The Florida Govenia. Amer. Orchid Soc. Bull. 69: 867–869. </reference>
  </references>
  
</bio:treatment>