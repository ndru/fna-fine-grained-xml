<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/07 01:27:53</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">379</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">iridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">iris</taxon_name>
    <taxon_name authority="(Tausch) Spach" date="1846" rank="subgenus">Limniris</taxon_name>
    <taxon_name authority="Tausch" date="1823" rank="section">Limniris</taxon_name>
    <taxon_name authority="Iris (subg. Limniris" date="1953" rank="series">Vernae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">verna</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 39. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family iridaceae;genus iris;subgenus limniris;section limniris;series vernae;species verna;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242101721</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neubeckia</taxon_name>
    <taxon_name authority="(Linnaeus) Small" date="unknown" rank="species">verna</taxon_name>
    <taxon_hierarchy>genus Neubeckia;species verna;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes heterogeneous, whitish, cordlike portions 0.1–1.5 dm × 2–4 mm, enlarging to 6–8 mm diam., densely covered with brown, scalelike leaves, roots absent, or torulose with roots borne along entire rhizome.</text>
      <biological_entity id="o7524" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="variability" src="d0_s0" value="heterogeneous" value_original="heterogeneous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o7525" name="portion" name_original="portions" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="cordlike" value_original="cordlike" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="length" src="d0_s0" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="4" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s0" value="enlarging" value_original="enlarging" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7526" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s0" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o7527" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character constraint="with roots" constraintid="o7528" is_modifier="false" name="shape" src="d0_s0" value="torulose" value_original="torulose" />
      </biological_entity>
      <biological_entity id="o7528" name="root" name_original="roots" src="d0_s0" type="structure" />
      <biological_entity id="o7529" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o7525" id="r1068" modifier="densely" name="covered with" negation="false" src="d0_s0" to="o7526" />
      <relation from="o7528" id="r1069" name="borne along" negation="false" src="d0_s0" to="o7529" />
    </statement>
    <statement id="d0_s1">
      <text>Stems simple, 5–15 cm.</text>
      <biological_entity id="o7530" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal with blade light green, ensiform, 0.3–1.5 dm × 0.3–1.3 cm, enlarging to 3.5 dm, glaucous;</text>
      <biological_entity id="o7531" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o7532" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="ensiform" value_original="ensiform" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s2" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s2" to="1.3" to_unit="cm" />
        <character constraint="to 3.5 dm" is_modifier="false" name="size" src="d0_s2" value="enlarging" value_original="enlarging" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o7533" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light green" value_original="light green" />
      </biological_entity>
      <relation from="o7532" id="r1070" name="with" negation="false" src="d0_s2" to="o7533" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 5–9, sheathing, imbricate, blade light green, obovate, 1.3–5 cm, increasing in length, proximal shortest, membranous, apex acute.</text>
      <biological_entity id="o7534" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o7535" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="9" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o7536" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="light green" value_original="light green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7537" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length" src="d0_s3" value="shortest" value_original="shortest" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o7538" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescence units 1–2-flowered;</text>
      <biological_entity constraint="inflorescence" id="o7539" name="unit" name_original="units" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>spathes divergent, exposing floral-tube, green, lanceolate, 2–2.5 cm, apex acuminate.</text>
      <biological_entity id="o7540" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7541" name="floral-tube" name_original="floral-tube" src="d0_s5" type="structure" />
      <biological_entity id="o7542" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o7540" id="r1071" name="exposing" negation="false" src="d0_s5" to="o7541" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: floral-tube pale violet, filiform, 2.5–6.5 cm, expanding somewhat distally to 2.5–4 cm diam.;</text>
      <biological_entity id="o7543" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7544" name="floral-tube" name_original="floral-tube" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale violet" value_original="pale violet" />
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6.5" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat distally; distally" name="size" src="d0_s6" value="expanding" value_original="expanding" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals widely spreading, blue to violet with yellow or orange longitudinal papillose band at base of blade, obovate, 2–6 × 0.8–2 cm, base gradually attenuate into claw, not crested;</text>
      <biological_entity id="o7545" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7546" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character char_type="range_value" constraint="with band" constraintid="o7547" from="blue" name="coloration" src="d0_s7" to="violet" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7547" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="true" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o7548" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o7549" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o7550" name="base" name_original="base" src="d0_s7" type="structure">
        <character constraint="into claw" constraintid="o7551" is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="not" name="shape" notes="" src="d0_s7" value="crested" value_original="crested" />
      </biological_entity>
      <biological_entity id="o7551" name="claw" name_original="claw" src="d0_s7" type="structure" />
      <relation from="o7547" id="r1072" name="at" negation="false" src="d0_s7" to="o7548" />
      <relation from="o7548" id="r1073" name="part_of" negation="false" src="d0_s7" to="o7549" />
    </statement>
    <statement id="d0_s8">
      <text>petals erect, arching inward at tip, spatulate, 2–7 × 1–2 cm, base abruptly attenuate into claw;</text>
      <biological_entity id="o7552" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o7553" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="at tip" constraintid="o7554" is_modifier="false" name="orientation" src="d0_s8" value="arching" value_original="arching" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7554" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o7555" name="base" name_original="base" src="d0_s8" type="structure">
        <character constraint="into claw" constraintid="o7556" is_modifier="false" modifier="abruptly" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o7556" name="claw" name_original="claw" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>ovary linear, to 1.3 cm;</text>
      <biological_entity id="o7557" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7558" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="1.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles pale violet, 2.5–4 cm, crests linear-acute, narrow, 0.7 cm;</text>
      <biological_entity id="o7559" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7560" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale violet" value_original="pale violet" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s10" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7561" name="crest" name_original="crests" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-acute" value_original="linear-acute" />
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character name="some_measurement" src="d0_s10" unit="cm" value="0.7" value_original="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas rounded, margins entire;</text>
      <biological_entity id="o7562" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7563" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7564" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicel 1–3 cm, increasing to 25 cm as capsule matures.</text>
      <biological_entity id="o7565" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7566" name="pedicel" name_original="pedicel" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7567" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s12" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o7566" id="r1074" name="as" negation="false" src="d0_s12" to="o7567" />
    </statement>
    <statement id="d0_s13">
      <text>Capsules 3-angled with single ridge at each angle, almost hidden in bases of spathes, 1–3.2 × 0.8–1.5 cm, tapering into beak consisting of dried remnant of floral-tube.</text>
      <biological_entity id="o7568" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character constraint="with ridge" constraintid="o7569" is_modifier="false" name="shape" src="d0_s13" value="3-angled" value_original="3-angled" />
        <character constraint="in bases" constraintid="o7571" is_modifier="false" modifier="almost" name="prominence" notes="" src="d0_s13" value="hidden" value_original="hidden" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s13" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" notes="" src="d0_s13" to="1.5" to_unit="cm" />
        <character constraint="into beak" constraintid="o7573" is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o7569" name="ridge" name_original="ridge" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o7570" name="angle" name_original="angle" src="d0_s13" type="structure" />
      <biological_entity id="o7571" name="base" name_original="bases" src="d0_s13" type="structure" />
      <biological_entity id="o7572" name="spathe" name_original="spathes" src="d0_s13" type="structure" />
      <biological_entity id="o7573" name="beak" name_original="beak" src="d0_s13" type="structure" />
      <biological_entity id="o7574" name="remnant" name_original="remnant" src="d0_s13" type="structure">
        <character is_modifier="true" name="condition" src="d0_s13" value="dried" value_original="dried" />
      </biological_entity>
      <biological_entity id="o7575" name="floral-tube" name_original="floral-tube" src="d0_s13" type="structure">
        <character is_modifier="true" name="condition" src="d0_s13" value="dried" value_original="dried" />
      </biological_entity>
      <relation from="o7569" id="r1075" name="at" negation="false" src="d0_s13" to="o7570" />
      <relation from="o7571" id="r1076" name="part_of" negation="false" src="d0_s13" to="o7572" />
      <relation from="o7573" id="r1077" name="consisting of" negation="false" src="d0_s13" to="o7574" />
      <relation from="o7573" id="r1078" name="consisting of" negation="false" src="d0_s13" to="o7575" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds dark-brown, ribbed, 2.8–3.2 mm, lustrous, with small, fleshy aril basally.</text>
      <biological_entity id="o7576" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="lustrous" value_original="lustrous" />
      </biological_entity>
      <biological_entity id="o7577" name="aril" name_original="aril" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="small" value_original="small" />
        <character is_modifier="true" name="texture" src="d0_s14" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o7576" id="r1079" name="with" negation="false" src="d0_s14" to="o7577" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Dwarf violet iris</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Rhizomes 5–15 cm between offshoots; leaf blades 0.3–0.8 cm wide; capsules 1.2–1.8 cm.</description>
      <determination>7a Iris verna var. verna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Rhizomes 1.3 cm between offshoots; leaf blades 5–13 cm wide; capsules 2–3.2 cm.</description>
      <determination>7b Iris verna var. smalliana</determination>
    </key_statement>
  </key>
</bio:treatment>